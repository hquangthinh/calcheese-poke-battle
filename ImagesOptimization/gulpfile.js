﻿const gulp = require('gulp');
const imagemin = require('gulp-imagemin');

const webAppImageSrcPath = 'WebApp/Content/images/**/*';
const adminWebAppImageSrcPath = 'AdminWebApp/Content/images/**/*';

gulp.task('default', () =>
	gulp.src(webAppImageSrcPath)
		.pipe(imagemin({
            verbose: true
        }))
		.pipe(gulp.dest('WebApp/dist/Content/images'))
);
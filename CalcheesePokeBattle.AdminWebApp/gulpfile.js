﻿// Required modules
var gulp = require('gulp');
var sass = require('gulp-sass');
var jade = require('gulp-jade');
var concat = require('gulp-concat');
var cssmin = require('gulp-cssmin');
var uglify = require('gulp-uglify');
var ngAnnotate = require('gulp-ng-annotate');
var sourcemaps = require('gulp-sourcemaps');
var rename = require('gulp-rename');

// Global variables
var appTheme = 'blue';
var bowerPath = 'Content/assets';
var customVendorPath = 'Content/vendor';
// Path to compile/concat/minify source files
var paths = {
    stylesBlue: {
        src: ['Content/scss/theme/blue/**/*.sass', 'Content/scss/theme/blue/**/*.scss'],
        dest: 'Content/styles/theme/blue'
    },
    jadeViews: {
        src: 'Scripts/app/views/**/*.jade',
        dest: 'Scripts/app/views/'
    },
    jadePartials: {
        src: 'Scripts/app/partials/*.jade',
        dest: 'Scripts/app/partials/'
    },
    jadeComponentViews: {
        src: 'Scripts/app/components/**/*.jade',
        dest: 'Scripts/app/components/'
    },
    stylesFinal: {
        vendor: [
            //bowerPath + '/jquery-ui/themes/base/jquery-ui.min.css',
            customVendorPath + '/bootstrap.css',
            bowerPath + '/angular-toastr/dist/angular-toastr.min.css',
            bowerPath + '/angular-loading-bar/build/loading-bar.min.css',
            bowerPath + '/perfect-scrollbar/css/perfect-scrollbar.min.css',
            bowerPath + '/sweetalert2/dist/sweetalert2.min.css',
            bowerPath + '/fontawesome/css/font-awesome.min.css',
            //bowerPath + '/Bootflat/bootflat/css/bootflat.min.css',
            'Content/styles/theme/' + appTheme + '/vendor.css',
            customVendorPath + '/animate.min.css',
            customVendorPath + '/vendor-override.css'
        ],
        app: [
            'Content/styles/theme/' + appTheme + '/main.css'
        ],
        dest: 'Content/styles/theme/' + appTheme + '/'
    },
    scriptsFinal: {
        vendor: [
            bowerPath + '/es6-promise/es6-promise.auto.min.js',
            bowerPath + '/jquery/dist/jquery.min.js',
            bowerPath + '/ramda/dist/ramda.min.js',
            bowerPath + '/bootstrap-sass-official/assets/javascripts/bootstrap.js',
            bowerPath + '/perfect-scrollbar/js/perfect-scrollbar.min.js',
            bowerPath + '/iCheck/icheck.js',
            bowerPath + '/sweetalert2/dist/sweetalert2.min.js',
            bowerPath + '/file-saver/FileSaver.min.js'
        ],
        angular: [
            bowerPath + '/angular/angular.min.js',
            bowerPath + '/ng-file-upload/angular-file-upload-shim.min.js',
            bowerPath + '/ng-file-upload/angular-file-upload.min.js',
            bowerPath + '/angular-resource/angular-resource.js',
            bowerPath + '/angular-animate/angular-animate.js',
            bowerPath + '/angular-sanitize/angular-sanitize.js',
            bowerPath + '/angular-bootstrap/ui-bootstrap-tpls.min.js',
            bowerPath + '/angular-ui-router/release/angular-ui-router.min.js',
            bowerPath + '/angular-loading-bar/build/loading-bar.js',
            bowerPath + '/angular-breadcrumb/dist/angular-breadcrumb.js',
            bowerPath + '/angular-input-masks/angular-input-masks-standalone.min.js',
            bowerPath + '/angular-toastr/dist/angular-toastr.tpls.min.js',
            bowerPath + '/ngstorage/ngStorage.js',
            bowerPath + '/moment/min/moment.min.js',
            bowerPath + '/angular-momentjs/angular-momentjs.min.js',
            bowerPath + '/angular-cookies/angular-cookies.min.js'
        ],
        app: [
            'Scripts/app/calcheese-admin.module.js',
            'Scripts/app/calcheese-admin.routes.js',
            'Scripts/app/config/*.js',
            'Scripts/app/services/*.js',
            'Scripts/app/helpers/*.js',
            'Scripts/app/filters/*.js',
            'Scripts/app/controllers/*.js',
            'Scripts/app/directives/*.js',
            'Scripts/app/components/**/*.js'
        ],
        dest: 'Scripts/'
    }
};

function parseSASS(src, dest) {
    return gulp.src(src)
        .pipe(sass({ errLogToConsole: true }))
        .pipe(gulp.dest(dest));
}

// [TASK] ----------------------------------- 
// Compile SASS to CSS
gulp.task('styles-blue', function () {
    return parseSASS(paths.stylesBlue.src, paths.stylesBlue.dest);
});
gulp.task('compile-styles-vendor', ['styles-blue'], function () {
    return gulp.src(paths.stylesFinal.vendor)
        .pipe(concat('lib.css'))
        .pipe(gulp.dest(paths.stylesFinal.dest))
        .pipe(cssmin())
        .pipe(rename('lib.min.css'))
        .pipe(gulp.dest(paths.stylesFinal.dest));
});
gulp.task('compile-styles-app', ['styles-blue'], function () {
    return gulp.src(paths.stylesFinal.app)
        .pipe(concat('app.css'))
        .pipe(gulp.dest(paths.stylesFinal.dest))
        .pipe(cssmin())
        .pipe(rename('app.min.css'))
        .pipe(gulp.dest(paths.stylesFinal.dest));
});
gulp.task('styles', ['styles-blue', 'compile-styles-vendor', 'compile-styles-app']);

// [TASK] ----------------------------------- 
// Compile javascripts
gulp.task('compile-scripts-vendor', function () {
    return gulp.src(paths.scriptsFinal.vendor)
        .pipe(concat('lib.js'))
        .pipe(gulp.dest(paths.scriptsFinal.dest))
        .pipe(uglify())
        .pipe(rename('lib.min.js'))
        .pipe(gulp.dest(paths.scriptsFinal.dest));
});
gulp.task('compile-scripts-angular', function () {
    return gulp.src(paths.scriptsFinal.angular)
        .pipe(concat('angular-modules.js'))
        .pipe(gulp.dest(paths.scriptsFinal.dest))
        .pipe(uglify())
        .pipe(rename('angular-modules.min.js'))
        .pipe(gulp.dest(paths.scriptsFinal.dest));
});
gulp.task('compile-scripts-app', function () {
    return gulp.src(paths.scriptsFinal.app)
        .pipe(concat('calcheese-admin-app.js'))
        .pipe(gulp.dest(paths.scriptsFinal.dest))
        .pipe(ngAnnotate())
        .pipe(uglify())
        .pipe(rename('calcheese-admin-app.min.js'))
        .pipe(gulp.dest(paths.scriptsFinal.dest));
});

gulp.task('scripts', ['compile-scripts-vendor', 'compile-scripts-angular', 'compile-scripts-app']);

// [TASK] ----------------------------------- 
// Parse jade template to HTML
gulp.task('jade-views', function () {
    return gulp.src(paths.jadeViews.src)
        .pipe(jade({
            locals: {},
            debug: false,
            pretty: true
        }))
        .pipe(gulp.dest(paths.jadeViews.dest));
});
gulp.task('jade-partials', function () {
    return gulp.src(paths.jadePartials.src)
        .pipe(jade({
            locals: {},
            debug: false,
            pretty: true
        }))
        .pipe(gulp.dest(paths.jadePartials.dest));
});
gulp.task('jade-components-views', function () {
    return gulp.src(paths.jadeComponentViews.src)
        .pipe(jade({
            locals: {},
            debug: false,
            pretty: true
        }))
        .pipe(gulp.dest(paths.jadeComponentViews.dest));
});

// [TASK] ----------------------------------- 
// Watch file changes
gulp.task('watch', function () {
    gulp.watch([paths.stylesBlue.src], ['styles']);
    gulp.watch([paths.jadeViews.src], ['jade-views']);
    gulp.watch([paths.jadePartials.src], ['jade-partials']);
    gulp.watch([paths.jadeComponentViews.src], ['jade-components-views']);
    gulp.watch([paths.scriptsFinal.app], ['compile-scripts-app']);
});

// Default tasks
gulp.task('build', ['styles', 'scripts', 'jade-views', 'jade-partials', 'jade-components-views']);
gulp.task('build-app', ['compile-styles-app', 'compile-scripts-app', 'jade-views', 'jade-partials', 'jade-components-views']);
gulp.task('watch-app', ['build-app', 'watch']);
gulp.task('default', ['build', 'watch']);
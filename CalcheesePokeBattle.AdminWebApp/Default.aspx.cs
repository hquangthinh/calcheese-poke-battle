﻿using System;
using System.Configuration;
using System.Text;

namespace CalcheesePokeBattle.AdminWebApp
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var appSettings = ConfigurationManager.AppSettings;
            ViewState["siteBaseUrl"] = appSettings["SiteBaseUrl"];
            ViewState["webBaseUrl"] = appSettings["WebBaseUrl"];
            ViewState["apiBaseUrl"] = appSettings["ApiBaseUrl"];
            ViewState["tokenAntiForgery"] = GetTokenHeaderValue();
        }

        protected string RenderJsScripts(string prefix, params String[] jsRelativePath)
        {
            var sb = new StringBuilder();
            foreach (var s in jsRelativePath)
            {
                sb.AppendFormat("<script type='text/javascript' src='{0}?{1}'></script>\r\n", ResolveUrl(s.StartsWith("~") ? s : prefix + s)
                    ,/* reset cache on new lauch*/
                     (s.StartsWith("~/Scripts") || "dev".Equals(Application[Global.MODE] as string, StringComparison.OrdinalIgnoreCase)
                    ? Application.Get(Global.INSTANCE_ID) : ""));
            }
            return sb.ToString();
        }

        protected string AssetScripts(params String[] jsRelativePath)
        {
            return RenderJsScripts("~/Content/assets", jsRelativePath);
        }

        private static string GetTokenHeaderValue()
        {
            string cookieToken, formToken;
            System.Web.Helpers.AntiForgery.GetTokens(null, out cookieToken, out formToken);
            return cookieToken + ":" + formToken;
        }
    }
}
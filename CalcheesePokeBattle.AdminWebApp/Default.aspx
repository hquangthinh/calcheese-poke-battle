﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="CalcheesePokeBattle.AdminWebApp.Default" %>
<%@ Import Namespace="System.Globalization" %>
<%@ Import Namespace="CalcheesePokeBattle.AdminWebApp" %>

<!DOCTYPE html>

<html lang="en" ng-app="CalcheeseAdmin"
    data-sitebaseurl="<%=ViewState["siteBaseUrl"]%>"
    data-webbaseurl="<%=ViewState["webBaseUrl"]%>"
    data-apibaseurl="<%=ViewState["apiBaseUrl"]%>"
    data-instanceid="<%=Application["__INSTANCE_ID__"]%>"
    data-mode="<%=Application["__MODE__"]%>"
    data-locale="<%=CultureInfo.CurrentCulture.ToString()%>">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Calcheese Pokemon Battle">
    <meta name="keywords" content="Calcheese, Pokemon">

    <base href="<%=ResolveUrl("~")%>"/>
    <title>Calcheese Pokemon Battle</title>

    <link rel="stylesheet" href="<%=ResolveUrl("~/Content/styles/theme/blue/lib.min.css") + "?" + Application.Get(Global.INSTANCE_ID)%>">
    <link rel="stylesheet" href="<%=ResolveUrl("~/Content/styles/theme/blue/app.min.css") + "?" + Application.Get(Global.INSTANCE_ID)%>">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <script src="https://oss.maxcdn.com/modernizr/2.8.3/modernizr.min.js"></script>
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
    <!--[if lt IE 10]>
      <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
    <div class="container-fluid page-content-wrapper">
        <div id="offcanvas" class="row row-offcanvas row-offcanvas-left">
            <!-- Section: Main -->
            <div class="col-sm-12 col-md-12 col-lg-12 page-content">
                <!-- Section: Top panel -->
                <page-top></page-top>

                <!-- Section: Main content -->
                <div class="page-main">
                    <!-- Component: Breadcrumb -->
                    <div ncy-breadcrumb></div>

                    <!-- Section: View -->
                    <div ui-view></div>
                </div>

                <!-- Section: Footer content -->
                <page-bottom></page-bottom>
            </div>
        </div>
    </div>

    <span id="token-af" value="<%=ViewState["tokenAntiForgery"]%>"></span>

    <%= AssetScripts(
        // Check [gulpfile.js] for details
        // bundle all vendor scripts,
         "~/Scripts/lib.min.js"
        // angular & reference modules
        //,"~/Scripts/angular-modules.min.js"
        , "~/Scripts/angular-modules.js"
        // application
        //,"~/Scripts/calcheese-admin-app.min.js"
        , "~/Scripts/calcheese-admin-app.js"
    )%>
</body>
</html>
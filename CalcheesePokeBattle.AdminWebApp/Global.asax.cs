﻿using System;
using System.Configuration;

namespace CalcheesePokeBattle.AdminWebApp
{
    public class Global : System.Web.HttpApplication
    {
        private const string DEFAULT_PAGE = "DefaultPage";
        public const string INSTANCE_ID = "__INSTANCE_ID__";
        public const string MODE = "__MODE__";

        protected void Application_Start(object sender, EventArgs e)
        {
            Application.Set(INSTANCE_ID, DateTime.Now.TimeOfDay.TotalSeconds);
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            if (Request.QueryString[MODE] == "dev") Application[MODE] = "dev";

            var appSettings = ConfigurationManager.AppSettings;

            //static file
            var url = Request.Url.LocalPath;
            if (!System.IO.File.Exists(Context.Server.MapPath(url)))
                Context.RewritePath(appSettings[DEFAULT_PAGE]);
        }
    }
}
﻿(function(angular) {
    'use strict';

    var app = angular
        .module('CalcheeseAdmin');

    app.filter('localDate', ['$filter', 'DateUtils', localDate]);

    app.filter('localDateTime', ['$filter', 'DateUtils', localDateTime]);

    function localDate($filter, DateUtils) {
        return function(input) {
            return formatDateTime(input, DateUtils.getDateFormat(), $filter);
        };
    }

    function localDateTime($filter, DateUtils) {
        return function(input) {
            var result = formatDateTime(input, DateUtils.getDateTimeFormat(), $filter);
            return result ? result.replace(" 00:00:00", "") : undefined;
        };
    }

    function formatDateTime(input, format, filter) {
        if (input == null || input == undefined) {
            return undefined;
        }
        var value = filter('date')(new Date(input), format);
        return value.toUpperCase();
    }

    // Range filter to loop number
    app.filter('range',
        function() {
            return function(array, total) {
                total = parseInt(total);
                for (var i = 1; i <= total; i++)
                    array.push(i);
                return array;
            };
        });

    //page lower and upper

    app.filter('pageLower',
        function() {
            return function(result) {
                if (!result) return 0;
                return result.ResultCount == 0 ? 0 : Math.max((result.PageNumber - 1) * result.PageSize + 1, 1);
            };
        }).filter('pageUpper',
        function() {
            return function(result) {
                if (!result) return 0;
                return Math.min(result.PageNumber * result.PageSize, result.ResultCount);
            };
        });
    app.filter('call',
        function() {
            return function(f) {
                console.log(f);
                return f();
            };
        });
    app.filter('yesno',
        function() {
            return function(boolValue, defaultValue) {
                return boolValue === true ? 'Yes' : boolValue == false ? 'No' : defaultValue || '';
            };
        });
    app.filter('yesnoIcon',
        function() {
            return function(boolValue, defaultValue) {
                return boolValue === true ? 'fa-check' : boolValue == false ? 'fa-times' : defaultValue || '';
            };
        });

    app.filter('encode',
        function() {
            return function(input) {
                if (!input) return null;
                // Base64 encode / decode
                // http://www.webtoolkit.info/
                //
                var _keyStr = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
                var output = "";
                var chr1, chr2, chr3, enc1, enc2, enc3, enc4;
                var i = 0;

                //input = Base64._utf8_encode(input);

                while (i < input.length) {

                    chr1 = input.charCodeAt(i++);
                    chr2 = input.charCodeAt(i++);
                    chr3 = input.charCodeAt(i++);

                    enc1 = chr1 >> 2;
                    enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
                    enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
                    enc4 = chr3 & 63;

                    if (isNaN(chr2)) {
                        enc3 = enc4 = 64;
                    } else if (isNaN(chr3)) {
                        enc4 = 64;
                    }

                    output = output +
                        _keyStr.charAt(enc1) +
                        _keyStr.charAt(enc2) +
                        _keyStr.charAt(enc3) +
                        _keyStr.charAt(enc4);

                }

                return output;
            };
        });

    // turn an array of string into display comma separator string
    app.filter('arrayForDisplay',
        function() {
            return function(input) {
                if (input === undefined || input === null || input.length === 0) {
                    return '';
                }
                if (angular.isArray(input)) {
                    return input.join(', ');
                }
                return input;
            };
        });

    // format file size from bytes to KB, MB, GB
    app.filter('fileSize',
        function() {
            return function(input) {
                if (input === undefined || input === null || input.length === 0 || isNaN(input)) {
                    return '';
                }

                function getReadableFileSizeString(fileSizeInBytes) {
                    if (fileSizeInBytes === 0) return '0 bytes';
                    var i = -1;
                    var byteUnits = [' KB', ' MB', ' GB', ' TB', 'PB', 'EB', 'ZB', 'YB'];
                    do {
                        fileSizeInBytes = fileSizeInBytes / 1024;
                        i++;
                    } while (fileSizeInBytes > 1024);

                    return Math.max(fileSizeInBytes, 0.1).toFixed(1) + byteUnits[i];
                };

                return getReadableFileSizeString(parseInt(input, 10));
            };
        });

})(window.angular);
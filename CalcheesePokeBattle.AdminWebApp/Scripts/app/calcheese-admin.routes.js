﻿(function (angular) {
    'use strict';

    angular
        .module('CalcheeseAdmin')
        .config([
            '$stateProvider', '$urlMatcherFactoryProvider', '$urlRouterProvider', '$locationProvider',
            '$breadcrumbProvider', 'toastrConfig', 'config', routerConfig
        ]);

    function routerConfig($stateProvider,
        $urlMatcherFactoryProvider,
        $urlRouterProvider,
        $locationProvider,
        $breadcrumbProvider,
        toastrConfig,
        config) {

        $urlMatcherFactoryProvider.caseInsensitive(true);
        $urlMatcherFactoryProvider.strictMode(false);

        // enable html5 mode
        $locationProvider.html5Mode(true);

        // Root of breadcrumbs
        $breadcrumbProvider.setOptions({
            // prefixStateName: 'dashboard',
            templateUrl: function() {
                return urlService().partial('/com-breadcrumb.tmpl.html');
            }
        });

        var basicResolve = {
            CurrentUser: [
                'UserService', function(UserService) {
                    return UserService.getCurrentUser();
                }
            ]
        };

        var defaultResolve = angular.extend({}, basicResolve);

        var noResolve = { Nothing: function() {} };

        function urlService() {
            return angular.element(document.body).injector().get('url');
        }

        // helper to make cleaner code
        // map route to controller and template
        function map(state, route, ctrl, tmpl, isDefaultRoute, label, parent, resolve, params) {
            if (isDefaultRoute === true) {
                $urlRouterProvider.otherwise(route);
            }

            $stateProvider.state(state,
            {
                url: route,
                controller: ctrl,
                templateUrl: function() {
                    return urlService().tmpl(tmpl);
                },
                ncyBreadcrumb: {
                    label: label,
                    parent: parent || null
                },
                resolve: resolve || defaultResolve,
                params: params
            });
        }

        function mapControllerAs(state, route, ctrl, tmpl, isDefaultRoute, label, parent, resolve, params) {
            map(state, route, ctrl ? ctrl + ' as ctrl' : null, tmpl, isDefaultRoute, label, parent, resolve, params);
        }

        // application routers
        map(config.ROUTER.WELCOME, '/', 'WelcomeCtrl', '/dashboard/welcome.html', false, 'Calcheese Admin', null, {});
        mapControllerAs(config.ROUTER.LOGIN, '/login?returnUrl', 'LoginCtrl', '/dashboard/login-v2.html', true, 'Calcheese Admin', null, {});
        map(config.ROUTER.LOGOUT, '/logout', 'LogoutCtrl', '/dashboard/logout.html', false, 'Calcheese Admin', null, {});
        mapControllerAs(config.ROUTER.ERRORS, '/errors?errorMsg', 'ErrorsCtrl', '/dashboard/errors.html', false, 'Calcheese Admin', null, {}, { ErrorResponse: null });
        mapControllerAs(config.ROUTER.DASHBOARD, '/dashboard', 'DashboardCtrl', '/dashboard/index.html', true, 'Dashboard', null, defaultResolve, {});

        // user routes
        mapControllerAs(config.ROUTER.USER_LIST, '/users', 'UserSearchCtrl', '/users/index.html', false, 'Users', config.ROUTER.DASHBOARD, defaultResolve, {});
        mapControllerAs(config.ROUTER.USER_DETAIL, '/users/:Id', 'UserDetailCtrl', '/users/detail.html', false, '{{ ctrl._params.Id === "new" ? "Add User" : "Edit User" }}', config.ROUTER.USER_LIST, defaultResolve, {});

        //gift item routes
        mapControllerAs(config.ROUTER.GIFT_ITEM_LIST, '/giftitems', 'GiftItemSearchCtrl', '/giftitems/index.html', false, 'GiftItem', config.ROUTER.DASHBOARD, defaultResolve, {});
        mapControllerAs(config.ROUTER.GIFT_ITEM_DETAIL, '/giftitems/:Id', 'GiftItemDetailCtrl', '/giftitems/detail.html', false, '{{ ctrl._params.Id === "new" ? "Add GiftItem" : "Edit GiftItem" }}', config.GIFT_ITEM_LIST, defaultResolve, {});

        //Poke Card
        mapControllerAs(config.ROUTER.POKE_CARD_LIST, '/pokecards', 'PokeCardSearchCtrl', '/pokecards/index.html', false, 'PokeCard', config.ROUTER.DASHBOARD, defaultResolve, {});
        mapControllerAs(config.ROUTER.POKE_CARD_DETAIL, '/pokecards/:Id', 'PokeCardDetailCtrl', '/pokecards/detail.html', false, '{{ ctrl._params.Id === "new" ? "Add PokeCard" : "Edit PokeCard" }}', config.POKE_CARD_LIST, defaultResolve, {});

        //prize_lucky_draw
        mapControllerAs(config.ROUTER.PRIZE_LUCKY_DRAW, '/prizeluckydraw', 'PrizeLuckyDrawCtrl', '/prizeluckydraw/index-v2.html', false, 'Lucky Draw Code', config.ROUTER.DASHBOARD, defaultResolve, {});

        //Upload Coupon file
        mapControllerAs(config.ROUTER.UPLOAD_COUPON, '/couponcode', 'CouponCodeCtrl', '/couponcode/index.html', false, 'Coupon Code', config.ROUTER.DASHBOARD, defaultResolve, {});

        //site_statistics
        mapControllerAs(config.ROUTER.SITE_STATISTICS, '/statistic', 'StatisticCtrl', '/statistic/index.html', false, 'statistic', config.ROUTER.DASHBOARD, defaultResolve, {});

        //lucky_draw_candidate_list_by_week
        mapControllerAs(config.ROUTER.LUCKY_DRAW_CANDIDATE_LIST_BY_WEEK, '/luckydraw', 'LuckyDrawCtrl', '/luckydraw/index.html', false, 'Lucky Draw', config.ROUTER.DASHBOARD, defaultResolve, {});

        //USED_CODE_CARD_DETAIL_LIST
        mapControllerAs(config.ROUTER.USED_CODE_CARD_DETAIL_LIST, '/usercoderelease', 'UserCodeReleaseCtrl', '/usercoderelease/index.html', false, 'Code', config.ROUTER.DASHBOARD, defaultResolve, {});

        //USED_CODE_CARD_SUMMARY_BY_DATE
        mapControllerAs(config.ROUTER.USED_CODE_CARD_SUMMARY_BY_DATE, '/usercoderelease-summary-by-date', 'UserCodeReleaseSummaryByDateCtrl', '/usercoderelease/used_code_card_summary_by_date.html', false, 'Code', config.ROUTER.USED_CODE_CARD_DETAIL_LIST, defaultResolve, {});

        //CARD_RELEASE_DETAIL_LIST
        mapControllerAs(config.ROUTER.CARD_RELEASE_DETAIL_LIST, '/cardrelease', 'CardReleaseCtrl', '/cardrelease/index.html', false, 'Card', config.ROUTER.DASHBOARD, defaultResolve, {});

        //PAGE_VIEW_REPORT
        mapControllerAs(config.ROUTER.PAGE_VIEW_REPORT, '/page-view-report', 'PageViewReportPageCtrl', '/reports/page-views.html', false, 'Page View Report', config.ROUTER.DASHBOARD, defaultResolve, {});

        //ALL_GAME_PAGE_VIEW
        mapControllerAs(config.ROUTER.ALL_GAME_PAGE_VIEW, '/all-game-page-views', 'AllGamePageViewsPageCtrl', '/reports/all-game-page-views.html', false, 'All Game Page Views', config.ROUTER.PAGE_VIEW_REPORT, defaultResolve, {});

        //GIFT_REDEEM_REPORT
        mapControllerAs(config.ROUTER.GIFT_REDEEM_REPORT, '/gift-redeem-report', 'GiftRedeemReportPageCtrl', '/reports/gift-redeem.html', false, 'Gift Redeem Report', config.ROUTER.DASHBOARD, defaultResolve, {});

        // Toast default settings
        angular.extend(toastrConfig,
        {
            positionClass: 'toast-bottom-full-width'
        });
    }

})(window.angular);
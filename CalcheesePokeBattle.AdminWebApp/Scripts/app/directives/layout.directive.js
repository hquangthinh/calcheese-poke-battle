﻿(function(angular) {
    'use strict';

    var app = angular
        .module('CalcheeseAdmin');

    // Top section: quick actions list and right actions
    app.directive('pageTop',
    [
        'url', function(url) {
            return {
                restrict: 'E',
                controller: 'LayoutCtrl',
                templateUrl: url.partial('/page-top.tmpl.html')
            };
        }
    ]);

    // Left section: sidebar to display main navigation
    app.directive('pageSidebar',
    [
        'url', function(url) {
            return {
                restrict: 'E',
                controller: 'LayoutCtrl',
                templateUrl: url.partial('/page-sidebar.tmpl.html')
            };
        }
    ]);

    // Bottom section: current user, role or credit
    app.directive('pageBottom',
    [
        'url', function(url) {
            return {
                restrict: 'E',
                controller: 'LayoutCtrl',
                templateUrl: url.partial('/page-bottom.tmpl.html')
            };
        }
    ]);


    // Overlay to display processing message
    app.directive('comOverlay',
    [
        'url', function(url) {
            return {
                restrict: 'E',
                scope: {
                    show: '='
                },
                transclude: true, // Insert custom content inside the directive
                link: function(scope, element, attrs) {
                    scope.dialogStyle = {};
                    if (attrs.width) {
                        scope.dialogStyle.width = attrs.width;
                    }
                    if (attrs.height) {
                        scope.dialogStyle.height = attrs.height;
                    }
                    angular.element('body').addClass('overlay-open');

                    scope.hideModal = function() {
                        scope.show = false;
                        angular.element('body').removeClass('overlay-open');
                    };
                },
                templateUrl: url.partial('/com-overlay.tmpl.html')
            };
        }
    ]);

    app.directive("loadingIndicator",
        function() {
            return {
                restrict: "A",
                template: "<div>Loading...</div>",
                link: function(scope, element, attrs) {
                    scope.$on("loading-started",
                        function(e) {
                            element.css({ "display": "" });
                        });
                    scope.$on("loading-complete",
                        function(e) {
                            element.css({ "display": "none" });
                        });
                }
            };
        });

    // Component: breadcrumbs
    app.directive('comBreadcrumb',
    [
        'url', function(url) {
            return {
                restrict: 'E',
                controller: 'LayoutCtrl',
                templateUrl: url.partial('/com-breadcrumb.tmpl.html')
            };
        }
    ]);

    // AntiForgery token
    app.directive('tokenAf',
    [
        '$http', function($http) {
            return function(scope, element, attrs) {
                $http.defaults.headers
                    .common['RequestVerificationToken'] = attrs.tokenAf || "no request verification token";
            };
        }
    ]);

    // Affix addin: add/remove class 
    app.directive('comAffix',
    [
        '$window', '$uibPosition', function($window, $uibPosition) {
            return {
                restrict: 'A',
                scope: { 'compareWith': '@' },
                link: function(scope, element, attrs) {
                    var _initialOffset = getOffset();
                    var _compareElement = $window.document.querySelector('#' + scope.compareWith);

                    angular.element($window).bind('scroll',
                        function() {
                            var newOffset = getOffset();
                            scope.$apply(function() {
                                if (affixNeeded(newOffset)) {
                                    angular.element(element).addClass('affix');
                                    element.css({ top: '0', width: newOffset.width + 'px' });
                                } else {
                                    angular.element(element).removeClass('affix');
                                    element.css({ top: 'auto', width: 'auto' });
                                }
                            });
                        });

                    function getOffset() {
                        return $uibPosition.offset(element[0]);
                    }

                    function affixNeeded(newOffset) {
                        if (_compareElement) {
                            var offsetCompare = $uibPosition.offset(_compareElement);
                            if (offsetCompare.height < newOffset.height)
                                return false;
                        }
                        return $window.pageYOffset > _initialOffset.top;
                    }
                }
            };
        }
    ]);

})(window.angular);
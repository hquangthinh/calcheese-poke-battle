﻿(function(angular) {
    'use strict';

    angular
        .module('CalcheeseAdmin')
        .constant('config',
        {
            VERSION: '0.0.1.e3b9152e672f3172e698b98db304fedcf85ec2c4',
            MODE: 'dev',
            PAGE_SIZE: 10,
            DEFAULT_SEARCH_MATCH_TYPE: 'contains',
            ALLOW_CHANGE_USER_PASSWORD: true,
            SITE_MODE: 'live',
            ROUTER: {
                DASHBOARD: "dashboard",
                WELCOME: 'welcome',
                LOGIN: "login",
                LOGOUT: "logout",
                ERRORS: "errors",
                USER_LIST: "user_list",
                USER_DETAIL: "user_detail",
                ADD_USER_DETAIL: "add_user_detail",
                EDIT_USER_DETAIL: "edit_user_detail",
                GIFT_ITEM_LIST: "gift_item_list",
                GIFT_ITEM_DETAIL: "gift_item_detail",
                POKE_CARD_LIST: "poke_card_list",
                POKE_CARD_DETAIL: "poke_card_detail",
                COUPON_CODE_LIST: "coupon_code_list",
                COUPON_CODE_DETAIL: "coupon_code_detail",
                PRIZE_LUCKY_DRAW: "prize_lucky_draw",
                SITE_STATISTICS: "site_statistics",
                UPLOAD_COUPON: "upload_coupon",
                CARD_RELEASE_DETAIL_LIST: "card_release_detail_list",
                LUCKY_DRAW_CANDIDATE_LIST_BY_WEEK: "lucky_draw_candidate_list_by_week",
                USED_CODE_CARD_DETAIL_LIST: "used_code_card_detail_list",
                USED_CODE_CARD_SUMMARY_BY_DATE: "used_code_card_summary_by_date",
                PAGE_VIEW_REPORT: "page_view_report",
                ALL_GAME_PAGE_VIEW: "all_game_page_views",
                GIFT_REDEEM_REPORT: "gift_redeem_report",
                REPORT_USER_REGISTRATION_BY_DATE: "report_user_registration_by_date",
                REPORT_POKEMON_GAME_BY_DATE: "report_pokemon_game_by_date"
            },
            MAX_FILE_SIZE:10000000
        });

})(window.angular);
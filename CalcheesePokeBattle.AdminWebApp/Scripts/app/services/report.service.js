﻿(function (angular) {
    'use strict';

    angular
        .module('CalcheeseAdmin')
        .service("ReportService", ReportService);

    ReportService.$inject = ["apis"];

    function ReportService(apis) {
        var service = this;
        var serviceApi = apis.report;
        var pageViewTrackingApi = apis.page_view_tracking;

        service.getAllImportJobStatus = function () {
            return serviceApi.path('coupon-import-job-status');
        }

        service.userCouponCodeStatistic = function () {
            return serviceApi.path('coupon-statistic');
        }

        service.getGiftSummaryByWeek = function (weekNumber) {
            return serviceApi.path('gift-summary-by-week', { queryString: 'WeekNumber=' + weekNumber })
                .then(function(res) {
                    return res.data;
                });
        }

        service.getGiftRedeemByWeek = function (weekNumber) {
            return serviceApi.path('gift-redeem-by-week', { queryString: 'WeekNumber=' + weekNumber })
                .then(function(res) {
                    return res.data;
                });
        }

        service.getPageViewReportDetail = function (pageTitle) {
            var query = { PageTitle: pageTitle };
            return pageViewTrackingApi.postToUri('/page-view-report-detail', query).then(function(res) {
                return res.data;
            });
        }

        service.getPageViewReportSummaryByPage = function () {
            return pageViewTrackingApi.postToUri('/page-view-report-summary-by-page').then(function (res) {
                return res.data;
            });
        }

        service.getPageViewReportSummaryByUser = function (weekNumber, pageTitle) {
            return pageViewTrackingApi.postToUri('/page-view-report-summary-by-username', { WeekNumber: weekNumber, PageTitle: pageTitle}).then(function (res) {
                return res.data;
            });
        }

        service.getAllGamePageViews = function (lastAccessType) {
            return pageViewTrackingApi.postToUri('/all-page-view', { LastAccessType: lastAccessType })
                .then(function(res) {
                    return res.data;
                });
        }
    }
})(window.angular);

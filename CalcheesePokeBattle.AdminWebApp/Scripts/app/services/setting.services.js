(function (angular) {
    'use strict';

    angular
        .module('CalcheeseAdmin')
        .service("SettingService", SettingService);

    SettingService.$inject = ["apis", "UserService", "config", "storage", "R"];

    function SettingService(apis, UserService, config, storage, R) {
        var service = this;
        var settingsApi = apis.settings;

        var Settings = {
            asAttachmentMaxFileSize: 82
        };

        /** @memberOf SettingService **/
        service.dataImportSupportedFileTypes = ["application/vnd.ms-excel", "text/csv"];
        /** @memberOf SettingService **/
        service.getSettingValue = getSettingValue;
        /** @memberOf SettingService **/
        service.getServerSettingValue = getServerSettingValue;
        /** @memberOf SettingService **/
        service.getPreferredLocale = getPreferredLocale;
        /** @memberOf SettingService **/
        service.saveSetting = saveSetting;
        /** @memberOf SettingService **/
        service.SETTINGS = Settings;
        /** @memberOf SettingService **/
        service.maxUploadFileSize = maxUploadFileSize;
        /** @memberOf SettingService **/
        service.maxUploadFileSizeMB = maxUploadFileSizeMB;

        var _settings = {};

        function maxUploadFileSize() {
            return maxUploadFileSizeMB() * 1024 * 1024;
        }

        function maxUploadFileSizeMB() {
            var settingValue = getSettingValue(Settings.asAttachmentMaxFileSize, 0);
            return settingValue === 0 ? config.MAX_FILE_SIZE : settingValue;
        }

        function getSettingValue(settingId, defaultValue) {
            // user must be logged in to get setting value
            if (!UserService.CurrentUser) return defaultValue;
            return _settings[settingId] || defaultValue;
        }

        function isSettingAffirmative(settingId) {
            var settingValue = getSettingValue(settingId);
            settingValue = settingValue ? settingValue.toUpperCase() : "";
            return settingValue === "1"
                || settingValue === "-1"
                || settingValue === "YES"
                || settingValue === "ENABLED"
                || settingValue === "ACTIVATED"
                || settingValue === "ON";
        }

        function getServerSettingValue(settingId, defaultValue) {
            return settingsApi.getFromUri(settingId, "/value").then(function (res) {
                return res.data || defaultValue;
            });
        }

        var serverLocale = R.replace('-', '_', angular.element('html').data('locale'));

        function getPreferredLocale() {
            return storage.get('currentLocale', true) || serverLocale;
        }

        function saveSetting(settingId, value) {
            return settingsApi.post({SettingID: settingId, SettingValue: value}).then(function () {
                _settings[settingId] = value;
            });
        }
    }
})(window.angular);
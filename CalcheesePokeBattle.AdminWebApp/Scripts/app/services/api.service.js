﻿(function (angular) {
    'use strict';

    angular
        .module('CalcheeseAdmin')
        .factory('apis', apis);

    apis.$inject = ['$http', '$q', 'apiBaseUrl', '$upload', 'R'];

    function apis($http, $q, apiBaseUrl, $upload, R) {
        /**
         * In case you have different base url for special api
         * you can set in its instance but not common base url
         * eg. var api= new API('/location'); //base url is default from global base url provided by apiBaseUrlProvider
         * eg. var api= new API('/location','http://someotherhost/api'); //want to call difference host than common host
         */
        function API(api, idField, opt_baseUrl) {
            var me = this;
            if (!api) throw "Api is null. Please provide api on creating ex: new API('/location','LocationCode')";
            if (!idField) throw "idField is null. Please provide api on creating ex: new API('/location','LocationCode')";

            // public functions
            this.api = api;
            this.idField = idField;
            this.baseUrl = opt_baseUrl || apiBaseUrl;
            this.apiUrl = apiBaseUrl + api;
            this.path = path;
            this.id = id;
            this.all = all;
            /**
             * Use all() to get result set. This is mapped as the same server api functions
             * @deprecated
             */
            this.get = all;
            this.getFromUri = getFromUri;
            this.post = post;
            this.postToUri = postToUri;
            this.form = form;
            this.delete = apiDelete;
            this.del = del;
            this.put = put;
            this.upload = upload;
            /**
             * Search by a field
             */
            this.search = search;
            this.searchActive = searchActive;
            this.subUri = subUri;

            function encodeId(id) {
                if (!R.is(String, id)) return id;
                return id
                    .replace('&', '___amp___')
                    .replace('<', '___lt___')
                    .replace('>', '___gt___')
                    .replace('*', '___star___')
                    .replace('%', '___perc___')
                    .replace(':', '___colon___')
                    .replace('/', '___slash___')
                    .replace('\\', '___bslsh___')
                    .replace('#', '___hash___')
                    .replace('?', '___quest___')
                    .replace('!', '___exclm___')
                    .replace('(', '___obrkt___')
                    .replace(')', '___cbrkt___')
                    .replace('+', '___plus___')
                    ;
            }

            // function declarations
            function path(urlPath, opt_config) {
                opt_config = opt_config || {};
                var url = me.baseUrl + me.api + '/' + urlPath;
                if (opt_config.queryString) {
                    url = url + '?' + opt_config.queryString;
                }
                return $http.get(url, opt_config);
            }

            function id(id, opt_config) {
                return path(encodeId(id), opt_config);
            }

            function all(opt_config) {
                return $http.get(me.baseUrl + me.api, opt_config || {});
            }

            function getUriPath(uri) {
                return R.head(uri) === '/' ? uri : '/' + uri;
            }

            function getFromUri(id, uri, opt_config) {
                return path(encodeId(id) + getUriPath(uri), opt_config);
            }

            function post(opt_data, opt_config) {
                return httpPost(me.baseUrl + me.api, opt_data, opt_config);
            }

            function postToUri(uri, opt_data, opt_config) {
                return httpPost(me.baseUrl + me.api + getUriPath(uri), opt_data, opt_config);
            }

            function subUri(uri) {
                return new API(me.api + getUriPath(uri), me.idField, me.baseUrl);
            }

            function form(opt_data) {
                return $http({
                    method: 'POST',
                    url: me.baseUrl + me.api,
                    data: $.param(opt_data),
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                });
            }

            function apiDelete(opt_config) {
                return httpDelete(me.baseUrl + me.api, opt_config);
            }

            function del(id, opt_config) {
                return httpDelete(me.baseUrl + me.api + '/' + encodeId(id), opt_config);
            }

            function put(opt_data, opt_config) {
                return $http.put(me.baseUrl + me.api, opt_data || {}, opt_config || {});
            }

            function upload(maxFileSize, file, action, opt_data) {
                if (file.size > maxFileSize) {
                    return $q(function (resolve, reject) {
                        var fileSizeInMbs = truncateDecimals(maxFileSize / 1048576, 2);
                        var errorMessage = "File size exceeds " + fileSizeInMbs + " MB";
                        console.error(errorMessage);
                        return reject(errorMessage);
                    });
                }
                return $upload.upload({
                    method: 'POST',
                    url: me.baseUrl + me.api + (action ? '/' + action : ''),
                    data: opt_data || {},
                    file: file
                });
            }

            function search(query, fieldName, matchType, companyId) {
                var params = createSearchParams(query, fieldName, matchType, companyId);
                return all({params: params}).then(function (res) {
                    return res.data.ResultSet;
                });
            }

            function searchActive(query, fieldName, matchType, companyId) {
                var params = createSearchParams(query, fieldName, matchType, companyId);
                params.Active = true;
                return all({ params: params }).then(function (res) {
                    return res.data.ResultSet;
                });
            }

            function createSearchParams(query, fieldName, matchType, companyId) {
                var params = {};
                angular.forEach(fieldName.split(/[,;\s]/), function (name) {
                    if (name === 'CompanyID' || name === 'CompanyCode') {
                        params[name] = companyId;
                    }
                    else {
                        params[name] = query;
                    }
                });
                params.SearchMatchType = matchType;

                return params;
            }

            function truncateDecimals(number, digits) {
                var multiplier = Math.pow(10, digits),
                    adjustedNum = number * multiplier,
                    truncatedNum = Math[adjustedNum < 0 ? 'ceil' : 'floor'](adjustedNum);

                return truncatedNum / multiplier;
            }

            function httpDelete(url, opt_config) {
                return $http['delete'](url, opt_config || {});
            }

            function httpPost(url, opt_data, opt_config) {
                return $http.post(url, opt_data || {}, opt_config || {});
            }
        }

        return {
            couponcode: new API('/CouponUpload', 'Unknown'),
            prizeluckydraw: new API('/prizeluckydraw', 'Unknown'),
            giftitems: new API('/giftitem', 'Id'),
            pokecards: new API('/pokecard', 'Id'),
            users: new API('/pikachu-secret', 'Id'),
            login: new API('/admin-auth/login', 'Unknown'),
            auth: new API('/admin-auth', 'Unknown'),
            report: new API('/reports', 'Unknown'),
            report_gift_redeem_by_week: new API('/reports/gift-redeem-by-week', 'Unknown'),
            usedcouponcodereport: new API('/usedcouponcodereport', 'Unknown'),
            usedcouponcodereport_summary_by_date: new API('/usedcouponcodereport/summary-by-date', 'Unknown'),
            page_view_tracking: new API('/page-view-tracking', 'Unknown')
        };
    }
})(window.angular);

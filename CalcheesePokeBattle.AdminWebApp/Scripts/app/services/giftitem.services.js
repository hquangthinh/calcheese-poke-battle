(function (angular) {
    'use strict';

    angular
        .module('CalcheeseAdmin')
        .service("GiftItemService", GiftItemService);

    GiftItemService.$inject = ["apis"];

    function GiftItemService(apis, $rootScope, Memoization) {
        var service = this;
        var giftItemApi = apis.giftitems;

        /** @memberOf GiftItem **/
        service.CurrentGiftItem = null;
        /** @memberOf GiftItem **/
        service.updateGiftItem = updateGiftItem;
        /** @memberOf GiftItem **/
        service.getGiftItem = getGiftItemDetail;
        /** @memberOf GiftItem **/
        service.deleteGiftItem = deleteGiftItem;
        /** @memberOf GiftItem **/
        service.searchGiftItems = searchGiftItems;
     

       

        function updateGiftItem(model) {
            var postedData = {};
            angular.forEach(model, function (item) {
                postedData[item.PropertyInternalName] = item.PropertyValue;
            });
            return giftItemApi.postToUri('/', postedData);
        }

        function getGiftItemDetail(itemID) {
            return giftItemApi.id(itemID).then(function(result) {
                return result.data;
            });
        }
       

        function deleteGiftItem() {
            delete service.CurrentGiftItem;
        }
        

        function searchGiftItems(active, query, findAll, permission) {
            var permissionId = permission ? permission : null;
            return giftItemApi.all({params: {
                Keyword: query,
                SearchMatchType: 'contains',
                FindAll: findAll === true
            }}).then(function (result) {
                return result.data.ResultSet;
            });
        }
      
    }
})(window.angular);

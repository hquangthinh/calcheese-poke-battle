﻿(function (angular) {
    'use strict';

    angular
        .module('CalcheeseAdmin')
        .service("CouponCodeService", CouponCodeService);

    CouponCodeService.$inject = ["apis"];

    function CouponCodeService(apis) {
        var service = this;
        var serviceApi = apis.couponcode;

        service.getAllUploadInfo = function() {
            return serviceApi.path('all-upload-info');
        }

        // trigger a message to import queue
        service.triggerImportQueueMessage = function() {
            return serviceApi.postToUri('trigger-queue');
        }

        service.deleteFile = function(fileInfo) {
            return serviceApi.postToUri('delete-file', fileInfo);
        }
    }
})(window.angular);

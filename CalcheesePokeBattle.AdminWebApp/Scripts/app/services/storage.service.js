﻿(function (angular) {
    'use strict';

    angular
        .module('CalcheeseAdmin')
        .factory('storage',
        [
            '$rootScope', '$localStorage', 'UserService', function($rootScope, $localStorage, UserService) {

                // Days to expired
                var expiredDuration = 30;
                // Check different of 2 dates by days
                var daysBetween = function(first, second) {
                    // Copy date parts of the timestamps, discarding the time parts.
                    var one = new Date(first.getFullYear(), first.getMonth(), first.getDate());
                    var two = new Date(second.getFullYear(), second.getMonth(), second.getDate());
                    // Do the math.
                    var millisecondsPerDay = 1000 * 60 * 60 * 24;
                    var millisBetween = two.getTime() - one.getTime();
                    var days = millisBetween / millisecondsPerDay;
                    // Round down.
                    return Math.floor(days);
                };
                // Store key
                var store = function(key, value, notExpired) {
                    if (notExpired) {
                        $localStorage[key] = value;
                    } else {
                        $localStorage[key] = {};
                        $localStorage[key].date = new Date();
                        $localStorage[key].value = value;
                        $localStorage[key].user = UserService.CurrentUser
                            ? UserService.CurrentUser.NTUserNameFull
                            : null;
                    }
                };
                // Get key
                var get = function(key, notExpired) {
                    if ($localStorage[key]) {
                        // Store key with expired time?
                        return notExpired
                            ? $localStorage[key]
                            : $localStorage[key].date != null &&
                            $localStorage[key].date != undefined &&
                            $localStorage[key].user != null
                            ? (daysBetween(new Date($localStorage[key].date), new Date()) <= expiredDuration) &&
                            $localStorage[key].user == UserService.CurrentUser.NTUserNameFull
                            ? $localStorage[key].value
                            : null
                            : null;

                    } else {
                        return null;
                    }
                };

                return {
                    get: function(key, notExpired) {
                        return get(key, notExpired);
                    },
                    set: function(key, value, notExpired) {
                        store(key, value, notExpired);
                    },
                    add: function(key, field, value, notExpired) {
                        var objValue = get(key, notExpired);
                        if (objValue) {
                            objValue[field] = value;
                            store(key, objValue, notExpired);
                        }
                    },
                    remove: function(key) {
                        delete $localStorage[key];
                    },
                    reset: function() {
                        $localStorage.$reset();
                    }
                }

            }
        ]);

})(window.angular);
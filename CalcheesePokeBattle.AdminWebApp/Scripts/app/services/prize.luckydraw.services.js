﻿(function (angular) {
    'use strict';

    angular
        .module('CalcheeseAdmin')
        .service("PrizeLuckyDrawService", PrizeLuckyDrawService);

    PrizeLuckyDrawService.$inject = ["apis"];

    function PrizeLuckyDrawService(apis, $rootScope, Memoization) {
        var service = this;
        var prizeLuckyDrawdApi = apis.prizeluckydraw;
        service.initCurrentWeek = initCurrentWeek;
        service.getPrizeHistory = getPrizeHistory;
        service.getWinnerUserList = getWinnerUserList;
        service.processPrize = processPrize;
        service.removePrize = removePrize;
        service.setPrize = setPrize;

        function initCurrentWeek() {
            return prizeLuckyDrawdApi.path("GetCurrentWeekData").then(function (result) {
                return result.data;
            });
        }

        function getPrizeHistory(WeekNumber) {
            return prizeLuckyDrawdApi.path("GetPrizeHistory", {
                params: {
                    WeekNumber: WeekNumber
                }
            }).then(function (result) {
                return result.data;
            });
        }

        function getWinnerUserList(WeekNumber, Prize) {
            return prizeLuckyDrawdApi.path("GetWinnerUserList", {
                params: {
                    WeekNumber: WeekNumber,
                    Prize: Prize
                }
            }).then(function (result) {
                return result.data;
            });
        }

        function processPrize(WeekNumber) {
            return prizeLuckyDrawdApi.path("ProcessPrize", {
                params: {
                    WeekNumber: WeekNumber
                }
            }).then(function (result) {
                return result.data;
            });
        }

        function setPrize(weeknumber, prize) {
            return prizeLuckyDrawdApi.postToUri("SetPrize", {
                    Weeknumber:weeknumber,
                    Prize: prize
            }).then(function (result) {
                return result.data;
            });
        }

        function removePrize(id) {
          return  apis.prizeluckydraw.delete({
                params: {
                    Id: id
                }
            }).then(function (result) {
                return result.data;
            });
        }

    }
})(window.angular);

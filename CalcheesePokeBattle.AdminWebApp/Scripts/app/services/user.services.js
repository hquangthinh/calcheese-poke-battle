(function (angular) {
    'use strict';

    angular
        .module('CalcheeseAdmin')
        .service("UserService", UserService);

    UserService.$inject = ["apis", "$rootScope", "Memoization"];

    function UserService(apis, $rootScope, Memoization) {
        var service = this;
        var userApi = apis.users;

        /** @memberOf UserService **/
        service.CurrentUser = null;
        /** @memberOf UserService **/
        service.getCurrentUser = getCurrentUser;
        /** @memberOf UserService **/
        service.changePassword = changePassword;
        /** @memberOf UserService **/
        service.getUserDetail = getUserDetail;
        /** @memberOf UserService **/
        service.updateCurrentUser = updateCurrentUser;
        /** @memberOf UserService **/
        service.deleteCurrentUser = deleteCurrentUser;
        /** @memberOf UserService **/
        service.searchUsers = searchUsers;
        /** @memberOf UserService **/
        service.getUserGameHistories = getUserGameHistories;
        /** @memberOf UserService **/
        service.loadUserActivityHistories = loadUserActivityHistories;
        /** @memberOf UserService **/
        service.getUserUsedCouponCodes = getUserUsedCouponCodes;

        var CURRENT_USER_CACHE_DURATION = 60000; // 1 minute
        var currentUserMemoized = Memoization.memoize(getCurrentUserInternal, CURRENT_USER_CACHE_DURATION);
        function getCurrentUserInternal() {
            return userApi.id('current').then(function (result) {
                return updateCurrentUser(result.data);
            });
        }

        function getCurrentUser() {
            return currentUserMemoized()();
        }

        function changePassword(userDetailModel) {
            return userApi.postToUri('/password', userDetailModel);
        }

        function populateUserFullName(user) {
            if (user) {
                user.FullName = (user.FirstName || '') + ' ' + (user.LastName || '');
            }
        }

        function getUserDetail(userId) {
            return userApi.id(userId).then(function(result) {
                return result.data;
            });
        }

        function updateCurrentUser(user) {
            populateUserFullName(user);
            $rootScope.CurrentUser = user;
            service.CurrentUser = user;
            return user;
        }

        function deleteCurrentUser() {
            delete $rootScope.CurrentUser;
            delete service.CurrentUser;
        }
        

        function searchUsers(active, query, findAll, permission, requisitionApprover) {
            var permissionId = permission ? permission : null;
            return userApi.all({params: {
                Keyword: query,
                Permission: permissionId,
                IsApprover: requisitionApprover,
                SearchMatchType: 'contains',
                Active: active,
                FindAll: findAll === true
            }}).then(function (result) {
                return result.data.ResultSet;
            });
        }

        function getUserGameHistories(userId) {
            return userApi.path(userId + '/games').then(function(res) {
                return res.data;
            });
        }

        function loadUserActivityHistories(userName) {
            return userApi.postToUri("/activity-histories", { UserName: userName })
                .then(function(res) {
                    return res.data;
                });
        }

        function getUserUsedCouponCodes(userId) {
            return userApi.postToUri("/used-coupon-codes", { UserId: userId })
                .then(function (res) {
                    return res.data;
                });
        }
    }
})(window.angular);

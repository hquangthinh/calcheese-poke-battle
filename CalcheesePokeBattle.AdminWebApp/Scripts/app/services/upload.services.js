﻿(function () {
    'use strict';

    angular
        .module("CalcheeseAdmin")
        .service("UploadService", UploadService);

    UploadService.$inject = ["$timeout", "webBaseUrl", "apiBaseUrl", "SettingService", "config", "toastr", "R"];

    function UploadService($timeout, webBaseUrl, apiBaseUrl, SettingService, config, toastr, R) {
        // Variables
        var service = this;

        // Exposed functions
        /** @memberOf UploadService **/
        service.options = {
            fileReaderSupported: false,
            uploadAction: "upload",
            supportFileTypes: []
        };

        /** @memberOf UploadService **/
        service.api = null;

        /** @memberOf UploadService **/
        service.uploadedCounter = 0;

        /** @memberOf UploadService **/
        service.onFileSelect = onFileSelect;

        /** @memberOf UploadService **/
        service.start = start;

        /** @memberOf UploadService **/
        service.uploadOptionData = uploadOptionData;

        /** @memberOf UploadService **/
        service.uploadCompleted = function () { };

        /** @memberOf UploadService **/
        service.imageUrl = imageUrl;

        /** @memberOf UploadService **/
        service.catalogueImageUrl = catalogueImageUrl;

        /** @memberOf UploadService **/
        service.download = download;

        /** @memberOf UploadService **/
        service.downloadCatalogueImage = downloadCatalogueImage;

        //--- Private        
        function download(typeFolder, typeId, fileName) {
            var api = apiBaseUrl + '/attachment/' + typeId + "/" + typeFolder;
            if (fileName) api = api + '?FileName=' + fileName;

            window.open(api);
        }

        function downloadCatalogueImage(attachmentId) {
            download(config.UPLOAD_FOLDER.Catalogue, attachmentId);
        }

        function imageUrl(typeFolder, typeId, fileName) {
            if ((typeFolder == null) || (typeId == null) || (fileName == null))
                return null;

            return webBaseUrl + "/" + config.UPLOAD_FOLDER.BaseFolder + "/" + typeFolder + "/" + typeId + "/" + fileName;
        }

        function catalogueImageUrl(imageFileName) {
            if (!imageFileName) return "";
            if (imageFileName.toLowerCase().startsWith("http"))
                return imageFileName;
            var date = new Date().getTime();
            return webBaseUrl + "/" + config.UPLOAD_FOLDER.BaseFolder + "/" + imageFileName + "?" + date;
        }

        function uploadOptionData() {
            return {}
        }

        function reset(model) {
            service.uploadedCounter = 0;
            model.selectedFiles = [];
            model.progress = []; // Import properties to support progress display
            model.progressData = [];
            if (model.upload && model.upload.length > 0) {
                for (var i = 0; i < model.upload.length; i++) {
                    if (model.upload[i] != null) {
                        model.upload[i].abort();
                    }
                }
            }
            model.upload = [];
            model.uploadResult = [];
            model.uploadInvalid = true;
            model.dataUrls = []; // Use to read image data
        }

        function onFileSelect(files, model) {
            reset(model);
            model.selectedFiles = files;
            if (files && files.length) {
                // Validate file size & mine type
                if (isValidFiles(files)) {
                    model.uploadInvalid = false;
                    for (var i = 0; i < files.length; i++) {
                        readData(i, files[i], model);
                        model.progress[i] = -1;
                    }
                } else {
                    toastr.error("File too large or invalid file types", 'Error', { closeButton: true, timeOut: 0 });
                }
            }
        }

        function isValidFiles(files) {
            return service.api
                && R.all(isValidFile, files);
        }

        function isValidFile(file) {
            return file.size <= SettingService.maxUploadFileSize()
                && R.contains(file.type, service.options.supportFileTypes);
        }

        function start(model) {
            for (var i = 0; i < model.selectedFiles.length; i++) {
                startUpload(i, model);
            }
        }

        function startUpload(index, model) {
            if (service.api == null) {
                toastr.error("System Error! Cannot upload file", 'Error', { closeButton: true, timeOut: 0 });
            } else {
                model.upload[index] = service.api().upload(SettingService.maxUploadFileSize(), model.selectedFiles[index], service.options.uploadAction, service.uploadOptionData());
                model.upload[index].then(function (response) {
                    // file is uploaded successfully
                    model.progressData[index] = response.data;
                    // check upload progress
                    service.uploadedCounter++;
                    if (service.uploadedCounter === model.selectedFiles.length)
                        service.uploadCompleted();
                }, function (response) {
                    // file failed to upload
                    console.log(response);
                }, function (evt) {
                    // Math.min is to fix IE which reports 200% sometimes
                    model.progress[index] = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
                });
            }
        }

        // Read local file data
        function readData(i, file, model) {
            service.options.fileReaderSupported = window.FileReader != null && (window.FileAPI == null || FileAPI.html5 != false);
            if (service.options.fileReaderSupported && file.type.indexOf('image') > -1) {
                var fileReader = new FileReader();
                fileReader.readAsDataURL(file);
                var loadFile = function (fileReader, index) {
                    fileReader.onload = function (e) {
                        $timeout(function () {
                            model.dataUrls[index] = e.target.result;
                        });
                    };
                };
                loadFile(fileReader, i);
            }
        }
    }
})();

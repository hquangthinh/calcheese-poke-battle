﻿(function (angular) {
    'use strict';

    angular
        .module('CalcheeseAdmin')
        .service('PageViewTrackingService', PageViewTrackingService);

    PageViewTrackingService.$inject = ['apis'];

    function PageViewTrackingService(apis) {
        var service = this;
        var serviceApi = apis.page_view_tracking;

        /** @memberOf PageViewTrackingService **/
        service.getPageViewReportDetail = getPageViewReportDetail;

        /** @memberOf PageViewTrackingService **/
        service.getPageViewReportSummaryByPage = getPageViewReportSummaryByPage;

        /** @memberOf PageViewTrackingService **/
        service.getPageViewReportSummaryByUser = getPageViewReportSummaryByUser;

        function getPageViewReportDetail(pagetitle) {
            return serviceApi.postToUri('page-view-report-detail', { PageTitle: pagetitle });
        }

        function getPageViewReportSummaryByPage() {
            return serviceApi.postToUri('page-view-report-summary-by-page');
        }

        function getPageViewReportSummaryByUser() {
            return serviceApi.postToUri('page-view-report-summary-by-username');
        }
    }
})(window.angular);

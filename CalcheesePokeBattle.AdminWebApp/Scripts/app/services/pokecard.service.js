﻿(function (angular) {
    'use strict';

    angular
        .module('CalcheeseAdmin')
        .service("PokeCardService", PokeCardService);

    PokeCardService.$inject = ["apis"];

    function PokeCardService(apis, $rootScope, Memoization) {
        var service = this;
        var pokecardApi = apis.pokecards;

        
        service.updatePokeCard = updatePokeCard;
        service.getPokeCardDetail = getPokeCardDetail;
        service.deletePokeCard = deletePokeCard;
        service.searchPokeCards = searchPokeCards;
        service.allCardRelease = allCardRelease;


        function allCardRelease() {
            return pokecardApi.path("allCardRelease").then(function (result) {
                return result.data;
            });
        }


        function updatePokeCard(model) {
            var postedData = {};
            angular.forEach(model, function (item) {
                postedData[item.PropertyInternalName] = item.PropertyValue;
            });
            return pokecardApi.postToUri('/', postedData);
        }

        function getPokeCardDetail(itemID) {
            return pokecardApi.id(itemID).then(function (result) {
                return result.data;
            });
        }


        function deletePokeCard() {
            delete service.CurrentGiftItem;
        }


        function searchPokeCards(active, query, findAll, permission) {
            var permissionId = permission ? permission : null;
            return pokecardApi.all({
                params: {
                    Keyword: query,
                    SearchMatchType: 'contains',
                    FindAll: findAll === true
                }
            }).then(function (result) {
                return result.data.ResultSet;
            });
        }

    }
})(window.angular);

﻿(function (angular) {
    'use strict';

    var app = angular
        .module('CalcheeseAdmin',
        [
            'ngResource',
            'ngAnimate',
            'ngSanitize',
            'ui.bootstrap',
            'ui.router',
            'ngStorage',
            'angular-loading-bar',
            'ncy-angular-breadcrumb',
            'angularFileUpload',
            'toastr',
            'angular-momentjs',
            'ngCookies'
        ]);

    // Ramda support
    app.constant('R', window.R);

    // Config
    app.config([
        '$httpProvider', '$uibTooltipProvider', 'config', 'apiBaseUrlProvider',
        '$uibModalProvider', '$compileProvider', '$qProvider',
        function($httpProvider,
            $tooltipProvider,
            config,
            apiBaseUrlProvider,
            $modalProvider,
            $compileProvider,
            $qProvider) {
            'use strict';
            // intercept for authorization
            var interceptor = [
                '$rootScope', 'config', '$q', 'R', function($scope, $constant, $q, R) {
                    return {
                        'request': function(config) {
                            config.timeout = 60000;
                            return config;
                        },

                        'response': function(response) {
                            if (response.status === 204) { // No content
                                $scope.$state.go($constant.ROUTER.DASHBOARD);
                            }
                            return response;
                        },

                        'responseError': function(response) {
                            console.error('http response error', response);
                            // Replace error icon
                            angular.element('.loading').toggleClass('error');

                            // connection error , can not connect to server
                            if (response.status === 0 || response.status === -1) {
                                var err = "Connection error";
                                var msg = "Cannot connect to server, please try again!";
                                swal({
                                    title: err,
                                    text: msg,
                                    type: "error"
                                });
                            }
                            // bad request
                            else if (response.status === 400 && !response.config.ignoreError) {
                                var errorDetails = "";
                                if (response.data.ModelState) {
                                    errorDetails = R.join('\n', R.values(response.data.ModelState));
                                } else {
                                    errorDetails =
                                        (response.data.Message ? response.data.Message : "") +
                                        (response.data.ExceptionMessage ? response.data.ExceptionMessage : "") +
                                        (response.data.InnerException && response.data.InnerException.ExceptionMessage
                                            ? response.data.InnerException.ExceptionMessage
                                            : "");
                                }
                                // alert to notify
                                swal({
                                    title: 'Stop',
                                    text: errorDetails,
                                    type: 'warning'
                                });
                            }
                            // unauthorized
                            else if (response.status === 401) {
                                swal({
                                        title: "Unauthorized",
                                        text: response.data.Message || "You are not authorised to perform this action.",
                                        type: "warning"
                                    },
                                    function() {
                                        $scope.$state.go($constant.ROUTER.LOGOUT);
                                    });
                            }
                            // forbidden
                            else if (response.status === 403) {
                                // redirect to login page
                                var returnUrl = window.location.href.indexOf('/logout') > 0
                                    ? null
                                    : window.location.href;
                                if ($scope.$state.current.name !== $constant.ROUTER.LOGIN) {
                                    $scope.$state.go($constant.ROUTER.LOGIN,
                                        { returnUrl: returnUrl },
                                        { reload: true });
                                }
                            }
                            // 409: resource conflict
                            else if (response.status === 409) {
                                errorDetails =
                                    (response.data.Message ? response.data.Message : "") +
                                    (response.data.ExceptionMessage ? response.data.ExceptionMessage : "") +
                                    (response.data.InnerException && response.data.InnerException.ExceptionMessage
                                        ? response.data.InnerException.ExceptionMessage
                                        : "");
                                // alert to notify
                                swal({
                                    title: response.statusText,
                                    text: errorDetails,
                                    type: "error"
                                });
                                $scope.$state.go($scope.$state.current.name, $scope.$stateParams, { reload: true });
                                $scope.$emit($scope.$state.current.name + '_resource_conflict',
                                    $scope.$state,
                                    $scope.$stateParams);
                            }
                            // internal server error || method not allow
                            else if (response.status >= 500 || response.status === 405) {
                                // redirect to errors page
                                $scope.$state.go($constant.ROUTER.ERRORS, { ErrorResponse: response });
                            }

                            return $q.reject(response);
                        }
                    };
                }
            ];

            $httpProvider.defaults.withCredentials = true;
            $httpProvider.interceptors.push(interceptor);

            // Tooltip config
            $tooltipProvider.options({
                popupDelay: 200,
                appendToBody: true
            });

            $modalProvider.options.animation = true;
            $modalProvider.options.size = "lg";

            // set to false if we don't want to see warning from framework
            $compileProvider.debugInfoEnabled(true);

            // turn off console error when a promise is rejected and there no handler for it
            //$qProvider.errorOnUnhandledRejections(false);
        }
    ]);

    // Run
    app.run([
        '$rootScope', '$uibModalStack', '$state', '$stateParams', '$http', 'webBaseUrl', 'UserService',
        function($rootScope,
            $modalStack,
            $state,
            $stateParams,
            $http,
            webBaseUrl,
            userService) {
            // Anti-forgery token
            $http.defaults.headers.common['__RequestVerificationToken'] = angular.element('span#token-af')
                .attr('value');

            $rootScope.$state = $state;
            $rootScope.$stateParams = $stateParams;
            $rootScope.overlay = {
                show: false,
                content: 'loading...'
            };

            if ($state.current.name === 'login') return;

            userService.getCurrentUser().then(function() {
                $rootScope._loggedIn = true;
            });

            // dismiss any active modal if current state changes
            $rootScope.$on('$stateChangeStart',
                function (event, toState, toParams, fromState, fromParams) {
                    var top = $modalStack.getTop();
                    if (top) $modalStack.dismiss(top.key);
                });

            // Base url to used in view
            $rootScope.webUrl = webBaseUrl;
        }
    ]);

})(window.angular);
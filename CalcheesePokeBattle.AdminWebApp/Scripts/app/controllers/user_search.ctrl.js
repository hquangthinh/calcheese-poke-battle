﻿(function (angular) {
    'use strict';

    angular
        .module('CalcheeseAdmin')
        .controller('UserSearchCtrl', UserSearchCtrl);

    UserSearchCtrl.$inject = ['$scope', 'apis', 'config', '$uibModal', 'url', 'BaseCtrlAs'];

    function UserSearchCtrl($scope, apis, config, $uibModal, url, BaseCtrlAs) {
        var ctrl = this;
        BaseCtrlAs.extend(ctrl, $scope, 'users');

        ctrl.changePassword = function() {
            $uibModal.open({
                templateUrl: url.tmpl('/users/change-password-modal.html'),
                controller: "ChangePasswordModalCtrl as modalCtrl",
                size: "md",
                resolve: {
                    UserCommand: {
                        UserName: ctrl.CurrentUser.UserName,
                        ChangePasswordForUser: false
                    }
                }
            });
        }

        ctrl.userCanSetPassword = function() {
            return config.ALLOW_CHANGE_USER_PASSWORD && ctrl.CurrentUser.UserPermission.IsSiteAdmin;
        }

        ctrl.setUserPassword = function(userName) {
            $uibModal.open({
                templateUrl: url.tmpl('/users/change-password-modal.html'),
                controller: "ChangePasswordModalCtrl as modalCtrl",
                size: "md",
                resolve: {
                    UserCommand: {
                        UserName: userName,
                        ChangePasswordForUser: true
                    }
                }
            });
        }

        ctrl.exportLuckyDrawUser = function () {
            $uibModal.open({
                templateUrl: url.tmpl('/users/user-export-options.html'),
                controller: "ExportUserOptionModalCtrl as modalCtrl",
                size: "md"
            }).result.then(function(week) {
                // call for export service
            });
        }

    }

    // controller for export-user-options.html
    angular
        .module('CalcheeseAdmin')
        .controller('ExportUserOptionModalCtrl', ExportUserOptionModalCtrl);

    ExportUserOptionModalCtrl.$inject = ['$uibModalInstance'];

    function ExportUserOptionModalCtrl($uibModalInstance) {

        var modalCtrl = this;

        modalCtrl.doExport = function() {
            $uibModalInstance.close(modalCtrl.selectedWeekNumber);
        }

        modalCtrl.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        }
    }

})(window.angular);
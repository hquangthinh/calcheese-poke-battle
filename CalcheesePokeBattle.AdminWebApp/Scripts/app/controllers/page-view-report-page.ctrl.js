﻿(function (angular) {

    'use strict';

    //PageViewReportPageCtrl

    angular
        .module('CalcheeseAdmin')
        .controller('PageViewReportPageCtrl', PageViewReportPageCtrl);

    PageViewReportPageCtrl.$inject = ['$scope', 'ReportService', 'apiBaseUrl'];

    function PageViewReportPageCtrl($scope, ReportService, apiBaseUrl) {

        var ctrl = this;

        ctrl.allPageTitles = [
            '', 'san-choi', 'tham-gia-san-choi', 'choi-game'
        ];

        ctrl.init = function () {
            ctrl.pageTitle = '';
            ReportService.getPageViewReportSummaryByUser(ctrl.weekNumber, ctrl.pageTitle).then(function(data) {
                ctrl.result = data;
                ctrl.loaded = true;
            });
        }

        $scope.$on('formSubmitted',
            function() {
                ReportService.getPageViewReportSummaryByUser(ctrl.weekNumber, ctrl.pageTitle).then(function (data) {
                    ctrl.result = data;
                });
            });

        ctrl.exportExcel = function() {
            var exportUrlReq = apiBaseUrl + '/page-view-tracking/export-page-view-report-summary-by-username';
            window.open(exportUrlReq);
        }
    }

    //AllGamePageViewsPageCtrl

    angular
        .module('CalcheeseAdmin')
        .controller('AllGamePageViewsPageCtrl', AllGamePageViewsPageCtrl);

    AllGamePageViewsPageCtrl.$inject = ['$scope', 'ReportService'];

    function AllGamePageViewsPageCtrl($scope, ReportService) {

        var ctrl = this;
        ctrl.allAccessTypes = ['24-hours', '1-week', '1-month'];
        ctrl.init = function () {
            ctrl.lastAccessType = '1-week';
            ReportService.getAllGamePageViews(ctrl.lastAccessType).then(function (data) {
                ctrl.result = data;
                ctrl.loaded = true;
            });
        }

        $scope.$on('formSubmitted',
            function() {
                ReportService.getAllGamePageViews(ctrl.lastAccessType).then(function(data) {
                    ctrl.result = data;
                    ctrl.loaded = true;
                });
            });
    }

})(window.angular);
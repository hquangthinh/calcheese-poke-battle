﻿(function (angular) {
    'use strict';

    angular
        .module('CalcheeseAdmin')
        .controller('GiftItemSearchCtrl', GiftItemSearchCtrl);

    GiftItemSearchCtrl.$inject = ['$scope', 'apis', 'BaseCtrlAs', 'GiftItemService'];

    function GiftItemSearchCtrl($scope, apis, BaseCtrlAs, GiftItemService) {
        var ctrl = this;
        BaseCtrlAs.extend(ctrl, $scope, 'giftitems');
    }

})(window.angular);
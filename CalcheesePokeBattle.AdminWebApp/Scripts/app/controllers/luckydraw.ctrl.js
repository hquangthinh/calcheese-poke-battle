﻿(function (document, angular, $, Ps) {
    'use strict';
    angular
        .module('CalcheeseAdmin')
        .controller('LuckyDrawCtrl', LuckyDrawCtrl);
    LuckyDrawCtrl.$inject = ['$scope', '$timeout', '$uibModal', 'apis', 'BaseCtrlAs', 'PrizeLuckyDrawService', 'url', 'config', 'R'];

    function LuckyDrawCtrl($scope, $timeout, $uibModal, apis, BaseCtrlAs, PrizeLuckyDrawService, url, config, R) {
        var ctrl = this;
        BaseCtrlAs.extend(ctrl, $scope, 'prizeluckydraw');

        var baseInit = ctrl.init;

        ctrl.init = function () {
            PrizeLuckyDrawService.initCurrentWeek().then(function (data) {
                ctrl.PrizeData = data;
                if (ctrl.request.params.WeekNumber == undefined) ctrl.request.params.WeekNumber = data.CuurentWeekDefinition.WeekNumber;
                baseInit();
            });
        }
      
    }


})(window.document, window.angular, window.$, window.Ps);
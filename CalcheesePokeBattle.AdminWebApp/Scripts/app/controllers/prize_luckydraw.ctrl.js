﻿(function (document, angular, $, Ps) {
    'use strict';
    angular
        .module('CalcheeseAdmin')
        .controller('PrizeLuckyDrawCtrl', PrizeLuckyDrawCtrl);
    PrizeLuckyDrawCtrl.$inject = ['$scope', '$timeout', '$uibModal', 'apis', 'apiBaseUrl', 'BaseCtrlAs', 'PrizeLuckyDrawService', 'url', 'config', 'R'];

    function PrizeLuckyDrawCtrl($scope, $timeout, $uibModal, apis, apiBaseUrl, BaseCtrlAs, PrizeLuckyDrawService, url, config, R) {

        var ctrl = this;
        ctrl.initScrollUserTable = false;
        ctrl.initScrollWinnerTable = false;
        ctrl.drawCodeResult = {};
        ctrl.winnerResult = [];
        ctrl.request = {};
        ctrl.isInTestMode = config.SITE_MODE === 'test';
        ctrl.currentWeek = 1;

        BaseCtrlAs.extend(ctrl, $scope, 'prizeluckydraw');

        //ctrl.$onInit = function () {
        //    angular.element('#logo-admin').addClass('lucky-draw-logo-custom');
        //}

        ctrl.request.params = {
            PageNumber: 1,
            PageSize: 50,
            SearchMatchType: "",
            WeekNumber: undefined
        };

        ctrl.doLoadAllUserLuckyDraws = doLoadAllUserLuckyDraws;
        ctrl.getUserLuckyDraws = getUserLuckyDraws;

        ctrl._initData = function () {
            PrizeLuckyDrawService.initCurrentWeek().then(function (data) {
                ctrl.PrizeData = data;
                ctrl.request.params.WeekNumber = data.CuurentWeekDefinition.WeekNumber;
                ctrl.currentWeek = data.CuurentWeekDefinition.WeekNumber;
                getWinnerUserList();
            });
        }

        function getWinnerUserList() {
            PrizeLuckyDrawService.getWinnerUserList(ctrl.request.params.WeekNumber, 0).then(function(data) {
                ctrl.winnerResult = data;
                if (!ctrl.initScrollWinnerTable) {
                    $timeout(function () {
                        initScrollbarWinnerTable();
                    },
                        500);
                    ctrl.initScrollWinnerTable = true;
                }
            });
        }

        function initScrollbarUserTable() {
            var classScroll = document.getElementsByClassName('user-table-container');
            for (var i = 0; i < classScroll.length; i++) {
                Ps.initialize(classScroll[i],
                    {
                        wheelSpeed: 2,
                        wheelPropagation: true,
                        maxScrollbarLength: 110
                    });
            };
        }

        function initScrollbarWinnerTable() {
            var classScroll = document.getElementsByClassName('winner-table-container');
            for (var i = 0; i < classScroll.length; i++) {
                Ps.initialize(classScroll[i],
                    {
                        wheelSpeed: 2,
                        wheelPropagation: true,
                        maxScrollbarLength: 110
                    });
            };
        }

        function doLoadAllUserLuckyDraws() {
            ctrl.request.params.Keyword = '';
            ctrl.getUserLuckyDraws();
        }

        function getUserLuckyDraws() {
            apis.prizeluckydraw.all(ctrl.request).then(function (result) {
                ctrl.drawCodeResult = result.data;
                ctrl.winnerResult = [];
                if (!ctrl.initScrollUserTable) {
                    $timeout(function() {
                            initScrollbarUserTable();
                        },
                        500);
                    ctrl.initScrollUserTable = true;
                }
            });
        }

        ctrl.hasFirstPrize = function() {
            var prizeMatch = function(item) {
                return item.PriceType === 'TraiHe';
            }
            var matchItems = R.filter(prizeMatch, ctrl.winnerResult);
            return matchItems && matchItems.length > 0;
        }

        ctrl.hasSecondPrize=function() {
            var prizeMatch = function (item) {
                return item.PriceType === 'XeDien' || item.PriceType === 'XeDap' || item.PriceType === 'VinID';
            }
            var matchItems = R.filter(prizeMatch, ctrl.winnerResult);
            return matchItems && matchItems.length > 0;
        }

        ctrl.hasThirdPrize = function() {
            var prizeMatch = function (item) {
                return item.PriceType === 'DoChoi';
            }
            var matchItems = R.filter(prizeMatch, ctrl.winnerResult);
            return matchItems && matchItems.length >= 30;
        }

        ctrl.exportData = function () {
            var currentWeek = ctrl.request.params.WeekNumber;
            var exportType = (ctrl.winnerResult && ctrl.winnerResult.length > 0) ? 'winner' : 'candidate';
            var exportUrlReq = apiBaseUrl + '/PrizeLuckyDraw/export-result-excel?WeekNumber=' + currentWeek + '&Type=' + exportType;
            if (exportType === 'candidate') {
                ctrl.drawCodeResult = {};
                ctrl.initScrollUserTable = false;
            }
            window.open(exportUrlReq);
        }

        ctrl.openAddPrize = function(prize) {
            $uibModal.open({
                    templateUrl: url.tmpl('/modals/prize_modal-v2.html'),
                    controller: "AddPrizeCtrl as addPrizeCtrl",
                    size: "lg",
                    backdrop: true,
                    windowTopClass: "modal-quay-so-window",
                    resolve: {
                        WeekNumber: ctrl.request.params.WeekNumber,
                        Prize: prize
                    }
                })
                .result.then(function() {
                        ctrl.drawCodeResult = {};
                        ctrl.initScrollUserTable = false;
                        ctrl.initScrollWinnerTable = false;
                        getWinnerUserList();
                    },
                    function() {
                        ctrl.drawCodeResult = {};
                        ctrl.initScrollUserTable = false;
                        ctrl.initScrollWinnerTable = false;
                        getWinnerUserList();
                    });
        }

        ctrl.changedWeek = function () {
            ctrl.drawCodeResult = {};
            ctrl.initScrollUserTable = false;
            ctrl.initScrollWinnerTable = false;
            getWinnerUserList();
        }
        
        ctrl.pageChanged = function () {
            ctrl.request.params.PageNumber = ctrl.drawCodeResult.PageNumber;
            getUserLuckyDraws();
        }

        ctrl.signOut = function() {
            apis.auth.postToUri('signout')
                .then(function () {
                    window.location.href = '/';
                });
        }

        // update setting flag and confirm to show prize result to front site
        ctrl.showPrizeResultToFrontSite = function() {
            return apis.prizeluckydraw.postToUri('/set-show-prize-result',
                    { WeekNumber: ctrl.request.params.WeekNumber })
                .then(function() {
                    ctrl._msg('Kết quả quay số may mắn tuần ' + ctrl.request.params.WeekNumber + ' đã được cập nhật lên web site calcheese.vn');
                });
        }

        // confirm prize result
        ctrl.confirmPrize = function (prizeItem) {
            var confirmPrizeCommand = {
                WeekNumber: ctrl.request.params.WeekNumber,
                WinnerListUserId: prizeItem.Id,
                ConfirmPrize: true,
                UserId: prizeItem.UserId,
                PrizeType: prizeItem.PriceType,
                LuckyDrawCode: prizeItem.LuckyDrawCode
            };
            return apis.prizeluckydraw.postToUri('/confirm-prize', confirmPrizeCommand).then(function (res) {
                var name = prizeItem.FullName || prizeItem.UserName;
                ctrl._msg('Đã xác nhận ' + prizeItem.PriceDescription + ' của ' + name);
                getWinnerUserList();
            });
        }

        // reset prize result
        ctrl.resetPrize = function (prizeItem) {
            var resetPrizeCommand = {
                WeekNumber: ctrl.request.params.WeekNumber,
                WinnerListUserId: prizeItem.Id,
                ResetPrize: true,
                UserId: prizeItem.UserId,
                PrizeType: prizeItem.PriceType,
                LuckyDrawCode: prizeItem.LuckyDrawCode
            };
            return apis.prizeluckydraw.postToUri('/reset-prize', resetPrizeCommand).then(function (res) {
                var name = prizeItem.FullName || prizeItem.UserName;
                ctrl._msg('Đã hủy ' + prizeItem.PriceDescription + ' của ' + name);
                getWinnerUserList();
            });
        }

        ctrl.allPrizeResultConfirmed = function () {
            var prizeConfirmed = function (item) {
                return item.HasConfirmedPrize;
            }
            return ctrl.winnerResult && ctrl.winnerResult.length === 32 && R.all(prizeConfirmed)(ctrl.winnerResult);
        }

        ctrl.doSearch = function () {
            if (ctrl.filterText && (!ctrl.winnerResult || ctrl.winnerResult.length === 0)) {
                ctrl.initScrollUserTable = false;
                ctrl.request.params.Keyword = ctrl.filterText;
                ctrl.request.params.PageNumber = 1;
                ctrl.getUserLuckyDraws();
            }
        }

    }

    angular
        .module('CalcheeseAdmin')
        .controller('AddPrizeCtrl', AddPrizeCtrl);
    AddPrizeCtrl.$inject = ['$scope', '$uibModalInstance', 'apis', 'PrizeLuckyDrawService', 'WeekNumber', 'Prize', '$interval', '$timeout'];
    function AddPrizeCtrl($scope, $uibModalInstance, apis, PrizeLuckyDrawService, WeekNumber, Prize, $interval, $timeout) {

        var ctrl = this;
        ctrl.showDebug = false;
        ctrl.prize = Prize;
        ctrl.weekNumber = WeekNumber;
        ctrl.isDisableRandomButton = false;
        ctrl.Timer = null;
        ctrl.cancel = cancel;
        ctrl.Result = [];
        ctrl.startRandomNumber = startRandomNumber;
        ctrl.countSetPrize = 1;
        var totalThirdPrize = 30;
        ctrl.thirdPrizeCountLeft = totalThirdPrize;

        ctrl.$onInit = function() {
            if (ctrl.prize === 1) {
                ctrl.prizeDescrition = 'Giải Nhất';
                ctrl.prizeTitleText = 'Học Bổng Trại Hè Quốc Tế';
                ctrl.prizeImageUrl = 'Content/images/lucky-draw-tool/lucky-draw-gift-sample.png';
            } else if (ctrl.prize === 2) {
                ctrl.prizeDescrition = 'Giải Nhì';
                if (WeekNumber % 2 === 0) {
                    ctrl.prizeTitleText = 'Xe Điện Cân Bằng';
                    ctrl.prizeImageUrl = 'Content/images/lucky-draw-tool/giai-nhi-xe-dien.png';
                } else {
                    ctrl.prizeTitleText = 'Xe Đạp Thể Thao';
                    ctrl.prizeImageUrl = 'Content/images/lucky-draw-tool/giai-nhi-xe-dap.png';
                }
            } else if (ctrl.prize === 3) {
                ctrl.prizeDescrition = 'Giải Ba';
                ctrl.prizeTitleText = 'Bộ Đồ Chơi Pokemon';
                ctrl.prizeImageUrl = 'Content/images/lucky-draw-tool/giai-ba-bo-xep-hinh.png';
            }
        }

        ctrl.model = {};

        ctrl.init = function () {
            PrizeLuckyDrawService.getWinnerUserList(WeekNumber, Prize).then(function (data) {
                ctrl.Result = data;
                ctrl.thirdPrizeCountLeft = totalThirdPrize - ctrl.Result.length; //init onload and remain unchanged
                //console.log('total prize issued -> ', ctrl.Result.length);
                ctrl.isDisableRandomButton = getRandomButtonState(ctrl.prize, ctrl.Result);
            }).finally(function() {
                initScrollbarForResultList();
            });
        }

        function getRandomButtonState(prize, result) {
            if (prize === 3)
                return (result && result.length >= 30);
            return (result && result.length > 0);
        }

        function initScrollbarForResultList() {
            var classScroll = document.getElementsByClassName('lucky-draw-winner-user-list-container');
            if (ctrl.scrollbarInitialized) {
                for (var j = 0; j < classScroll.length; j++) {
                    var container = classScroll[j];
                    container.scrollTop = 0;
                    Ps.update(container);
                }
            }
            else {
                for (var i = 0; i < classScroll.length; i++) {
                    Ps.initialize(classScroll[i],
                    {
                        wheelSpeed: 2,
                        wheelPropagation: true,
                        maxScrollbarLength: 110
                    });
                }
                ctrl.scrollbarInitialized = true;
            }
        }

        function randomNumber() {
            return Math.round((Math.random() * 9));
        }

         function startRandomNumber() {
             ctrl.isDisableRandomButton = true;
             ctrl.Timer = $interval(function() {
                     ctrl.model.Number1 = randomNumber();
                     ctrl.model.Number2 = randomNumber();
                     ctrl.model.Number3 = randomNumber();
                     ctrl.model.Number4 = randomNumber();
                     ctrl.model.Number5 = randomNumber();
                     ctrl.model.Number6 = randomNumber();
                 },
                 100);
            $timeout(function () { setPrize(); }, 3000);
        }

        //Timer stop function.
        ctrl.StopTimer = function () {
            //Cancel the Timer.
            if (angular.isDefined(ctrl.Timer)) {
                $interval.cancel(ctrl.Timer);
            }
        }

        function setPrize() {
            PrizeLuckyDrawService.setPrize(WeekNumber, Prize).then(function(data) {
                    ctrl.StopTimer();
                    setLuckyDrawCode(data, data.LuckyDrawCode, Prize);
                    if (Prize === 3) {
                        ctrl.countSetPrize += 1;
                        if (ctrl.countSetPrize > ctrl.thirdPrizeCountLeft) return;

                        $timeout(function() { startRandomNumber() }, 2000);
                    }
                })
                .catch(function(fallback) {
                })
                .finally(function() {
                    initScrollbarForResultList();
                    if (Prize === 3) {
                        $timeout(function() { angular.element('#userRowId_0').addClass('flash animated'); }, 300);
                    } else {
                        $timeout(function () { angular.element('#userRowId_0').addClass('flash animated'); }, 4000);
                    }
                });
        }

        function setLuckyDrawCode(winData, luckyDrawCode, prize) {

            var str = String(luckyDrawCode);
            if (prize === 3) {
                ctrl.Result.unshift(winData);
                ctrl.model.Number1 = str[0];
                ctrl.model.Number2 = str[1];
                ctrl.model.Number3 = str[2];
                ctrl.model.Number4 = str[3];
                ctrl.model.Number5 = str[4];
                ctrl.model.Number6 = str[5];
            } else {

                ctrl.model.Number1 = str[0];

                var number2Timer = $interval(function() {
                        ctrl.model.Number2 = randomNumber();
                    },
                    100);

                var number3Timer = $interval(function() {
                        ctrl.model.Number3 = randomNumber();
                    },
                    100);

                var number4Timer = $interval(function() {
                        ctrl.model.Number4 = randomNumber();
                    },
                    100);

                var number5Timer = $interval(function() {
                        ctrl.model.Number5 = randomNumber();
                    },
                    100);

                var number6Timer = $interval(function() {
                        ctrl.model.Number6 = randomNumber();
                    },
                    100);

                $timeout(function() {
                        if (angular.isDefined(number2Timer))
                            $interval.cancel(number2Timer);
                        ctrl.model.Number2 = str[1];
                    },
                    500);

                $timeout(function() {
                        if (angular.isDefined(number3Timer))
                            $interval.cancel(number3Timer);
                        ctrl.model.Number3 = str[2];
                    },
                    1000);

                $timeout(function() {
                        if (angular.isDefined(number4Timer))
                            $interval.cancel(number4Timer);
                        ctrl.model.Number4 = str[3];
                    },
                    1500);

                $timeout(function() {
                        if (angular.isDefined(number5Timer))
                            $interval.cancel(number5Timer);
                        ctrl.model.Number5 = str[4];
                    },
                    2000);

                $timeout(function() {
                        if (angular.isDefined(number6Timer))
                            $interval.cancel(number6Timer);
                        ctrl.model.Number6 = str[5];
                    },
                    2500);

                $timeout(function() {
                     ctrl.Result.unshift(winData);
                }, 3000);
            }
        }

        function cancel() {
            $uibModalInstance.dismiss('cancel');
        }
    }

})(window.document, window.angular, window.$, window.Ps);
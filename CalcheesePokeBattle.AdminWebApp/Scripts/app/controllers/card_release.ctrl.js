﻿(function (angular) {
    'use strict';
    angular
        .module('CalcheeseAdmin')
        .controller('CardReleaseCtrl', CardReleaseCtrl);

    CardReleaseCtrl.$inject = [ 'apis', 'PokeCardService'];

    function CardReleaseCtrl(apis, PokeCardService) {

        var ctrl = this;
        ctrl.init = function() {
            PokeCardService.allCardRelease().then(function(data) {
                ctrl.result = data;
            });
        }
    }

})(window.angular);
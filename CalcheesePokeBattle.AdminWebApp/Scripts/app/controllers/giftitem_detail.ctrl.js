﻿(function (angular) {
    'use strict';

    angular
        .module('CalcheeseAdmin')
        .controller('GiftItemDetailCtrl', GiftItemDetailCtrl);

    GiftItemDetailCtrl.$inject = ['$scope', 'BaseDetailCtrlAs', 'GiftItemService', 'R', 'SettingService', 'UploadService'];

    function GiftItemDetailCtrl($scope, BaseDetailCtrlAs, GiftItemService, R) {
        var ctrl = this;
        BaseDetailCtrlAs.extend(ctrl, $scope, 'giftitems');
        ctrl.backLink = ctrl._router.GIFT_ITEM_LIST;
        ctrl.isNewRecord = ctrl._params.Id === 'new';


        //$scope.supportFileTypes = ["image/jpeg", "image/gif", "image/png"];
        //$scope.onFileSelect = onFileSelect;
        //$scope.uploadSignature = uploadSignature;
        //$scope.downloadSignature = downloadSignature;

        // Signature
        //function signatureUrl() {
        //    var encodedUserName = $scope._params.Id;
        //    var imagePath = UploadService.imageUrl($scope.config.UPLOAD_FOLDER.Signature, encodedUserName, encodedUserName + ".thumb");
        //    var date = new Date().getTime();
        //    return imagePath + "?" + date;
        //}

        //function onFileSelect($files) {
        //    UploadService.onFileSelect($files, $scope.model.upload);
        //}

        //function uploadSignature() {
        //    // Options
        //    UploadService.uploadOptionData = function () {
        //        return {
        //            EncodedNTUserNameFull: $scope._params.Id,
        //            Description: $scope.model.upload.selectedFiles[0].name
        //        }
        //    };
        //    UploadService.start($scope.model.upload);
        //}

        //function downloadSignature() {
        //    UploadService.download($scope.config.UPLOAD_FOLDER.Signature, $scope._params.Id, $scope.model.SignatureFileName);
        //}
    }

})(window.angular);
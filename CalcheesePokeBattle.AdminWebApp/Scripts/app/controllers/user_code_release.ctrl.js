﻿(function (angular) {
    'use strict';

    angular
        .module('CalcheeseAdmin')
        .controller('UserCodeReleaseCtrl', UserCodeReleaseCtrl);

    UserCodeReleaseCtrl.$inject = ['$scope', 'apis', 'BaseCtrlAs'];

    function UserCodeReleaseCtrl($scope, apis, BaseCtrlAs) {
        var ctrl = this;
        BaseCtrlAs.extend(ctrl, $scope, 'usedcouponcodereport');
    }

    angular
        .module('CalcheeseAdmin')
        .controller('UserCodeReleaseSummaryByDateCtrl', UserCodeReleaseSummaryByDateCtrl);

    UserCodeReleaseSummaryByDateCtrl.$inject = ['$scope', 'apiBaseUrl', 'BaseCtrlAs'];

    function UserCodeReleaseSummaryByDateCtrl($scope, apiBaseUrl, BaseCtrlAs) {
        var ctrl = this;
        BaseCtrlAs.extend(ctrl, $scope, 'usedcouponcodereport_summary_by_date');

        ctrl.exportExcel = function () {
            var exportUrlReq = apiBaseUrl + '/usedcouponcodereport/export-card-release-summary-by-date';
            window.open(exportUrlReq);
        }
    }

})(window.angular);
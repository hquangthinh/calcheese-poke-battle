﻿
(function (angular) {
    'use strict';

    angular
        .module('CalcheeseAdmin')
        .controller('CouponCodeCtrl', CouponCodeCtrl);

    CouponCodeCtrl.$inject = ['$scope', 'BaseDetailCtrlAs', 'CouponCodeService', 'UploadService', 'apis', 'ReportService', 'R'];

    function CouponCodeCtrl($scope, BaseDetailCtrlAs, CouponCodeService, UploadService, apis, ReportService, R) {
        var ctrl = this;
        BaseDetailCtrlAs.extend(ctrl, $scope, 'couponcode');
        ctrl.backLink = ctrl._router.DASHBOARD;
        ctrl.isNewRecord = 'new';
        ctrl.supportFileTypes = ["application/vnd.ms-excel", "text/csv", "text/plain"];;
        ctrl.onFileSelect = onFileSelect;
        ctrl.uploadFile = uploadFile;
        ctrl.Pokecards = {};
        ctrl.currentCard = {};
        ctrl._init = function () {

            UploadService.api = function () {
                return apis.couponcode;
            }

            angular.extend(UploadService.options,
            {
                uploadAction: "upload",
                supportFileTypes: ctrl.supportFileTypes
                });

            UploadService.uploadCompleted = function () {
                fetchAllUploadInfo();
            };

            apis.pokecards.all({
                    params: {
                        SearchMatchType: 'contains',
                        FindAll: true
                    }
                })
                .then(function(result) {
                    ctrl.Pokecards = result.data.ResultSet;
                    ctrl.currentCard = ctrl.Pokecards[0];
                });

            fetchAllImportJobStatusInfo();

            fetchAllUploadInfo();
        };

        function fetchAllImportJobStatusInfo() {
            ReportService.getAllImportJobStatus().then(function(res) {
                ctrl.importJobStatus = res.data;
            });
        }

        function fetchAllUploadInfo() {
            CouponCodeService.getAllUploadInfo().then(function (res) {
                ctrl.allUploadInfo = res.data;
                var filePendingImport = function(fileInfo) {
                    return fileInfo.ImportStatus !== 'OK';
                }
                ctrl.totalPendingFileForProcessing = R.filter(filePendingImport, ctrl.allUploadInfo);
            });
        }

        function onFileSelect($files) {
            UploadService.onFileSelect($files, ctrl.model);
        }

        function uploadFile() {
            if (ctrl.currentCard.Id === "" || ctrl.currentCard.Id === undefined) return;
            // Options
            UploadService.uploadOptionData = function () {
                return {
                    PokeCardId: ctrl.currentCard.Id,
                    Description: ctrl.model.selectedFiles[0].name
                };
            }
            UploadService.start(ctrl.model);
        }

        ctrl.triggerImportAllData = function() {
            ctrl._confirm('Bạn có chắc muốn import tất cả dữ liệu bên dưới vào hệ thống?',
                function() {
                    CouponCodeService.triggerImportQueueMessage().then(function () {
                        ctrl._alert('Lệnh import đã ghi nhận, bạn có thể quay lại trang này để xem kết quả sau.');
                    });
                });
        }

        ctrl.canDeleteFile = function(fileInfo) {
            return fileInfo.ImportStatus !== 'OK' && (!ctrl.importJobStatus || ctrl.importJobStatus.length === 0);
        }

        ctrl.deleteFile = function(fileInfo) {
            CouponCodeService.deleteFile(fileInfo).then(function() {
                fetchAllUploadInfo();
            });
        }

        ctrl.close = function() {
            ctrl._go(ctrl._router.DASHBOARD);
        }
    }

})(window.angular);
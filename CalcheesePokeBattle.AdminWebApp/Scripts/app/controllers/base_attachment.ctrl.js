(function (angular) {
    'use strict';

    angular
        .module('CalcheeseAdmin')
        .factory('BaseAttachmentCtrlAs', ['BaseCtrlAs', 'apiBaseUrl', 'SettingService', '$timeout', 'R', BaseAttachmentCtrlAs]);

    angular
        .module('CalcheeseAdmin')
        .factory('BaseAttachmentCtrl', ['BaseAttachmentCtrlAs', BaseAttachmentCtrl]);
    
    angular
        .module('CalcheeseAdmin')
        .factory('BaseImportCtrlAs', ['BaseAttachmentCtrlAs', 'SettingService', BaseImportCtrlAs]);
    
    angular
        .module('CalcheeseAdmin')
        .factory('BaseImportCtrl', ['BaseImportCtrlAs', BaseImportCtrl]);

    function BaseAttachmentCtrlAs(BaseCtrlAs, apiBaseUrl, SettingService, $timeout, R) {
        var Base = function (ctrl, $scope, api) {
            BaseCtrlAs.extend(ctrl, $scope, api);

            ctrl.FileDescription = "";
            ctrl.onFileSelect = function (files) {
                if (files && files.length) {
                    if (files[0].size > SettingService.maxUploadFileSize()) {
                        ctrl._msgerr(ctrl.getLocalizedText("ClientMessage.FileIsTooLarge"));
                        files = null;
                    }
                    else {
                        ctrl.pendingFile = files[0];
                    }
                }
            };

            ctrl.uploadOptionData = function () {
                throw "Please override uploadOptionData";
            };

            ctrl.refreshRequested = false;
            ctrl.onUploaded = function () {
                ctrl.FileDescription = "";
                delete ctrl.pendingFile;

                // now signal angular to destroy the current file input element
                ctrl.refreshRequested = true;
                $scope.$applyAsync();

                $timeout(function () {
                    // once angular is done with its digest cycle, re-render the input element
                    ctrl.refreshRequested = false;
                }, 1, false);
            };

            ctrl.upload = function () {
                if (!ctrl.pendingFile) {
                    ctrl._msgerr('Select a file');
                    return;
                }
                ctrl._api()
                    .upload(SettingService.maxUploadFileSize(), ctrl.pendingFile, null, ctrl.uploadOptionData())
                    .then(function () {
                        // reload list
                        ctrl.init();
                        ctrl._msg('Uploaded');
                        ctrl.onUploaded();
                    });
            };

            ctrl.download = R.curry(function (attachmentType, attachmentId) {
                var api = apiBaseUrl + '/attachment/' + attachmentId + "/" + attachmentType;
                window.open(api);
            });

            ctrl.remove = function (attachmentId) {
                ctrl._confirm('You Are About To Remove Attachment', function () {
                    ctrl._api().del(attachmentId).then(function () {
                        ctrl.init();
                        ctrl._msg('Delete Successfully');
                    });
                });
            };
        };

        return {
            extend: function (ctrl, $scope, $api) {
                Base(ctrl, $scope, $api);
            }
        };
    }

    function BaseAttachmentCtrl(BaseAttachmentCtrlAs) {
        return {
            extend: function ($scope, $api) {
                BaseAttachmentCtrlAs.extend($scope, $scope, $api);
            }
        };
    }

    function BaseImportCtrlAs(BaseAttachmentCtrlAs, SettingService) {
        var Base = function(ctrl, $scope, api) {
            BaseAttachmentCtrlAs.extend(ctrl, $scope, api);

            // Abort all upload & reset variables
            ctrl.reset = function () {
                ctrl.selectedFiles = [];
                ctrl.progress = []; // Import properties to support progress display
                ctrl.progressData = [];
                if (ctrl.upload && ctrl.upload.length > 0) {
                    for (var i = 0; i < ctrl.upload.length; i++) {
                        if (ctrl.upload[i] != null) {
                            ctrl.upload[i].abort();
                        }
                    }
                }
                ctrl.upload = [];
                ctrl.uploadResult = [];
                ctrl.uploadInvalid = true;
                ctrl.dataUrls = []; // Use to read image data
            };

            ctrl.uploadAction = null; // import, upload, ...
            ctrl.supportFileTypes = function () {
                throw 'Please Override Support File Types'; // [ "", "", ... ]
            };

            // Override base attachment
            ctrl.onFileSelect = function (files) {
                ctrl.reset();
                ctrl.selectedFiles = files;

                if (files && files.length) {
                    // Validate file size & mine type
                    if (isValidFile(files)) {
                        ctrl.uploadInvalid = false;
                        for (var i = 0; i < files.length; i++) {
                            ctrl.progress[i] = -1;
                        }
                    } else {
                        ctrl._msgerr('File Too Large Or Invalid File Types');
                    }
                }
            };

            ctrl.import = function () {
                ctrl.processing = true;
                for (var i = 0; i < ctrl.selectedFiles.length; i++) {
                    startImport(i);
                }
            };

            ctrl.abortUpload = function (index) {
                ctrl.upload[index].abort();
            };

            ctrl.hasUploader = function (index) {
                return ctrl.upload[index] != null;
            };

            ctrl.onFileUploadSuccessfully = function(response) {
                ctrl.processing = false;
                //base class to override
            };
            
            ctrl.onFileUploadFailed = function (response) {
                //base class to override
            };

            // Private functions
            function isValidFile(files) {
                var isValid = true;
                angular.forEach(files, function (item) {
                    if ((item.size > SettingService.maxUploadFileSize()) || !isValidMineType(item.type))
                        isValid = false;
                });
                return isValid;
            }

            function isValidMineType(mineType) {
                if (mineType === "") return true;//client cannot detect the mime type so let server handle it
                var isValid = false;
                angular.forEach(ctrl.supportFileTypes(), function (item) {
                    if (mineType === item)
                        isValid = true;
                });

                return isValid;
            }

            function startImport(index) {
                ctrl.upload[index] = ctrl._api().upload(SettingService.maxUploadFileSize(), ctrl.selectedFiles[index], ctrl.uploadAction, ctrl.uploadOptionData());
                ctrl.upload[index].then(function (response) {
                    // file is uploaded successfully
                    ctrl.progressData[index] = response.data;
                    ctrl.onFileUploadSuccessfully(response);
                }, function(response) {
                    // file failed to upload
                    console.log(response);
                    ctrl.onFileUploadFailed(response);
                }, function(evt) {
                    // Math.min is to fix IE which reports 200% sometimes
                    ctrl.progress[index] = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
                });
            }

            // Read local file data
            function readData(file) {
                ctrl.fileReaderSupported = window.FileReader != null && (window.FileAPI == null || FileAPI.html5 != false);
                if (ctrl.fileReaderSupported && file.type.indexOf('image') > -1) {
                    var fileReader = new FileReader();
                    fileReader.readAsDataURL(file);
                    var loadFile = function(fileReader, index) {
                        fileReader.onload = function(e) {
                            $timeout(function() {
                                ctrl.dataUrls[index] = e.target.result;
                            });
                        };
                    }(fileReader, i);
                }
            }
        };

        return {
            extend: function (ctrl, $scope, $api) {
                Base(ctrl, $scope, $api);
            }
        };
    }
    
    function BaseImportCtrl(BaseImportCtrlAs) {
        
        return {
            extend: function ($scope, $api) {
                BaseImportCtrlAs.extend($scope, $scope, $api);
            }
        };
    }
})(window.angular);
﻿
(function (angular) {
    'use strict';
    angular
        .module('CalcheeseAdmin')
        .controller('StatisticCtrl', StatisticCtrl);

    StatisticCtrl.$inject = ['apis', 'ReportService'];

    function StatisticCtrl(apis, ReportService) {

        var ctrl = this;
        ctrl.init = function () {
            ReportService.userCouponCodeStatistic().then(function (result) {
                ctrl.result = result.data;
            });
        }
    }

})(window.angular);
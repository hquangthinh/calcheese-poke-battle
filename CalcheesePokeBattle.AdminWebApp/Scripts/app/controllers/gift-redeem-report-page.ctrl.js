﻿(function (angular) {

    'use strict';

    angular
        .module('CalcheeseAdmin')
        .controller('GiftRedeemReportPageCtrl', GiftRedeemReportPageCtrl);

    GiftRedeemReportPageCtrl.$inject = ['$scope', 'BaseCtrlAs', 'apiBaseUrl'];

    function GiftRedeemReportPageCtrl($scope, BaseCtrlAs, apiBaseUrl) {

        var ctrl = this;
        BaseCtrlAs.extend(ctrl, $scope, 'report_gift_redeem_by_week');
        ctrl.request.params.WeekNumber = 1;

        ctrl.exportExcel = function() {
            var currentWeek = ctrl.request.params.WeekNumber;
            var exportUrlReq = apiBaseUrl + '/reports/export-gift-redeem-list-excel?WeekNumber=' + currentWeek;
            window.open(exportUrlReq);
        }
    }

})(window.angular);
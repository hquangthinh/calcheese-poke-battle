﻿(function (angular) {
    'use strict';

    angular
        .module('CalcheeseAdmin')
        .controller('LayoutCtrl', LayoutCtrl);

    LayoutCtrl.$inject = ['$rootScope', '$scope', '$state', 'config', 'UserService'];

    function LayoutCtrl($rootScope, $scope, $state, config, UserService) {
        $scope.dateTimeFormat = 'EEEE, d MMMM yyyy - h:mm:ss a';
        $scope._router = config.ROUTER;
        $scope.$state = $state;
        $scope.appVersion = config.VERSION;
        $scope.webUrl = $rootScope.webUrl;

        // Current user
        $scope.CurrentUser = UserService.CurrentUser;
        $scope.isLoginPage = function() {
            return $state.current.name === config.ROUTER.LOGIN;
        }
    }
})(window.angular);
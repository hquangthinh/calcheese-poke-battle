﻿(function (angular) {
    'use strict';

    angular
        .module('CalcheeseAdmin')
        .controller('WelcomeCtrl',
        [
            '$rootScope', '$scope', 'config', function ($rootScope, $scope, config) {
                $scope.goDashboard = function() {
                    $rootScope.$watch('_loggedIn',
                        function(newVal, oldVal) {
                            if (newVal === true) $rootScope.$state.go(config.ROUTER.DASHBOARD);
                        });
                };
            }
        ]);

    angular
        .module('CalcheeseAdmin')
        .controller('ErrorsCtrl',
        [
            '$rootScope', '$stateParams', 'config', function ($rootScope, $stateParams, config) {
                var ctrl = this;
                ctrl.hideErrorDetails = false;
                ctrl.init = function() {
                    // Redirect to dashboard if there is not errors
                    if ($stateParams.ErrorResponse) {
                        ctrl.response = $stateParams.ErrorResponse;
                        ctrl.router = config.ROUTER;
                    } else if ($stateParams.errorMsg) {
                        ctrl.response = {
                            data: {
                                ExceptionMessage: 'Generic error found',
                                Message: $stateParams.errorMsg
                            }
                        }
                    } else {
                        $rootScope.$state.go(config.ROUTER.DASHBOARD);
                    }
                };

                ctrl.toggleErrorDetails = function() {
                    ctrl.hideErrorDetails = !ctrl.hideErrorDetails;
                }
            }
        ]);

    angular
        .module('CalcheeseAdmin')
        .controller('DashboardCtrl', DashboardCtrl);

    DashboardCtrl.$inject = ['$scope', '$uibModal', 'url', 'BaseCtrlAs', 'apis'];

    function DashboardCtrl($scope, $uibModal, url, BaseCtrlAs, $apis) {
        var ctrl = this;

        BaseCtrlAs.extend(ctrl, $scope, 'users');

        if (ctrl.CurrentUser.UserPermission.IsSiteAdmin) {
            ctrl.meta = [
                [
                    { text: 'Danh sách người chơi', state: ctrl._router.USER_LIST },
                    //{ text: 'Danh sách người chơi đăng ký theo ngày', state: ctrl._router.REPORT_USER_REGISTRATION_BY_DATE },
                    //{ text: 'Tải lên mã code', state: ctrl._router.UPLOAD_COUPON },
                    { text: 'Lọc danh sách trúng thưởng', state: ctrl._router.PRIZE_LUCKY_DRAW },
                    { text: 'Thông số thống kê', state: ctrl._router.SITE_STATISTICS },
                    { text: 'Thống kê thẻ đã sử dụng', state: ctrl._router.CARD_RELEASE_DETAIL_LIST },
                    { text: 'Danh sách ứng viên tham gia lucky draw theo tuần', state: ctrl._router.LUCKY_DRAW_CANDIDATE_LIST_BY_WEEK },
                    { text: 'Thông tin chi tiết thẻ đã sử dụng', state: ctrl._router.USED_CODE_CARD_DETAIL_LIST },
                    { text: 'Người chơi có vào sân chơi Pokemon', state: ctrl._router.PAGE_VIEW_REPORT },
                    { text: 'Danh sách người đổi quà', state: ctrl._router.GIFT_REDEEM_REPORT }
                    //{ text: 'Danh sách người chơi Pokemon theo ngày', state: ctrl._router.REPORT_POKEMON_GAME_BY_DATE }
                ]
            ];
        }
        else if (ctrl.CurrentUser.UserPermission.CanUserAccessLuckyDraw) {
            ctrl.meta = [
                [
                    { text: 'Lọc danh sách trúng thưởng', state: ctrl._router.PRIZE_LUCKY_DRAW }
                ]
            ];
            //ctrl._go(ctrl._router.PRIZE_LUCKY_DRAW, {}, { reload: false });
        }

        ctrl.signOut = function() {
            $apis.auth.postToUri('signout')
                .then(function () {
                    window.location.href = '/';
                });
        }

        ctrl.changePassword = function() {
            $uibModal.open({
                templateUrl: url.tmpl('/users/change-password-modal.html'),
                controller: "ChangePasswordModalCtrl as modalCtrl",
                size: "md",
                resolve: {
                    UserCommand: {
                        UserName: ctrl.CurrentUser.UserName,
                        ChangePasswordForUser: false
                    }
                }
            });
        }
    }

})(window.angular);
﻿(function (angular) {
    'use strict';

    angular
        .module('CalcheeseAdmin')
        .controller('PokeCardSearchCtrl', PokeCardSearchCtrl);

    PokeCardSearchCtrl.$inject = ['$scope', 'apis', 'BaseCtrlAs', 'PokeCardService'];

    function PokeCardSearchCtrl($scope, apis, BaseCtrlAs, PokeCardService) {
        var ctrl = this;
        BaseCtrlAs.extend(ctrl, $scope, 'pokecards');
    }

})(window.angular);
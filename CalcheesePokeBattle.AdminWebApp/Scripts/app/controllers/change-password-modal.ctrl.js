﻿(function (angular) {
    'use strict';

    angular
        .module('CalcheeseAdmin')
        .controller('ChangePasswordModalCtrl', ChangePasswordModalCtrl);

    ChangePasswordModalCtrl.$inject = ['$scope', '$state', '$uibModal', '$uibModalInstance', 'BaseCtrlAs', 'config', 'UserService', 'UserCommand'];

    function ChangePasswordModalCtrl($scope, $state, $uibModal, $uibModalInstance, BaseCtrlAs, config, UserService, UserCommand) {

        var modalCtrl = this;

        modalCtrl.$onInit = function () {

            BaseCtrlAs.extend(modalCtrl, $scope, 'users');

            modalCtrl.processing = false;
            modalCtrl.model = {
                UserNameToChangePassword: UserCommand.UserName,
                ChangePasswordForUser: UserCommand.ChangePasswordForUser
            };
        };

        modalCtrl.doChangePassword = doChangePassword;

        function doChangePassword() {

            UserService.changePassword(modalCtrl.model).then(function (changePwdRes) {
                if (changePwdRes && changePwdRes.data && changePwdRes.data.success) {
                    var message = 'Mật khẩu của ' + modalCtrl.model.UserNameToChangePassword + ' đã được đổi, vui lòng đăng nhập lại.';
                    modalCtrl._alertWithAction('Success', message, 'success', logoutAfterChangePassword);

                } else {

                    modalCtrl._alert('Error', changePwdRes.data.error.message, 'error');
                }
            });
        }

        function logoutAfterChangePassword() {
            $state.go(config.ROUTER.DEFAULT, {}, { reload: true });
        }

        modalCtrl.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        }
    }

})(window.angular);
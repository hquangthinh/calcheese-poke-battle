(function (angular) {
    'use strict';

    angular
        .module('CalcheeseAdmin')
        .controller('LoginCtrl', LoginCtrl);

    LoginCtrl.$inject = ['$scope', '$window', '$filter', 'config', 'webBaseUrl', 'apis', 'UserService'];

    function LoginCtrl($scope, $window, $filter, $constant, webBaseUrl, apis, UserService) {
        var ctrl = this;

        ctrl.focusOnUserName = true;
        ctrl.processing = true;
        //--- View actions
        ctrl.init = function () {
            // if current user is available, transit to dashboard
            // otherwise login is required
            UserService.getCurrentUser()
                .then(function () {
                    $scope.$state.go($constant.ROUTER.DASHBOARD, { reload: true });
                }).finally(function () {
                    ctrl.processing = false;
                });
        };

        // Validate input fields
        ctrl.canLogin = canLogin;
        ctrl.doLogin = doLogin;
        ctrl.windowsLogin = function () {
            $window.location.href = $scope.$stateParams.returnUrl || (webBaseUrl + '/dashboard');
        };

        function canLogin() {
            // Form model can not blank
            if (angular.isUndefined(ctrl.model)) return false;
            // Username can not be blank
            return ctrl.model.UserName && ctrl.model.Password;
        }

        // Form process
        function doLogin() {
            if (canLogin()) {
                ctrl.model.processing = true;
                apis.login.post(ctrl.model)
                    .then(function (result) {
                        var data = result.data;
                        if (data === 'ok') {
                            $scope.$emit('LoginSuccess');
                            $window.location.href = $scope.$stateParams.returnUrl || (webBaseUrl + '/dashboard');
                        } else {
                            ctrl.model.error = data;
                        }
                    })
                    .catch(function (error) {
                        ctrl.model.error = error;
                    })
                    .finally(function () {
                        ctrl.model.processing = false;
                    });
            } else {
                ctrl.model.error = 'Please Input Your Account And Password';
            }
        }
    }


    // Logout controller
    angular
        .module('CalcheeseAdmin')
        .controller('LogoutCtrl', LogoutCtrl);

    LogoutCtrl.$inject = ['$scope', '$timeout', '$state', 'config', 'apis', 'UserService'];

    function LogoutCtrl($scope, $timeout, $state, config, $apis, UserService) {
        // Call to dispose session

        $scope.init = function () {

            $apis.auth.postToUri('signout')
                .then(function () {
                    UserService.deleteCurrentUser();
                    $state.go(config.ROUTER.LOGIN, {}, { reload: true });
                });
        }
    }
})(window.angular);
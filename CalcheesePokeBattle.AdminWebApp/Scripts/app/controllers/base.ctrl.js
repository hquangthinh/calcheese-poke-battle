﻿(function (angular) {
    'use strict';

    angular
        .module('CalcheeseAdmin')
        .factory('BaseCtrlAs', ['$rootScope', '$filter', 'R', 'toastr', 'storage', 'config', 'url', 'apis', 'DateUtils', 'SettingService', 'UserService', BaseCtrlAs]);

    angular
        .module('CalcheeseAdmin')
        .factory('BaseCtrl', ['BaseCtrlAs', BaseCtrl]);

    function BaseCtrlAs($rootScope, $filter, R, toastr, storage, config, url, $apis, DateUtils, SettingService, UserService) {
        function getDefaultPageSize() {
            return config.PAGE_SIZE;
        }

        function resetLocalStorageIfVersionChange() {
            var appVersionStorageKey = "AppVersion";

            if (!storage.get(appVersionStorageKey)) {
                storage.reset();
                storage.set(appVersionStorageKey, config.VERSION);
                return;
            }

            var currentAppVersion = storage.get(appVersionStorageKey);
            if (config.VERSION !== currentAppVersion) {
                storage.reset();
                storage.set(appVersionStorageKey, config.VERSION);
            }
        }

        var Base = function ($scope, ctrl) {

            //Base properties and functions
            angular.extend(ctrl, {
                //do not override this, this is called on view ready <div class="page-list" ng-init="init()">
                init: function () {
                    ctrl.loading = true;
                    ctrl.resetLocalStorageIfVersionChange();

                    ctrl.request.params.SearchMatchType = config.DEFAULT_SEARCH_MATCH_TYPE;
                    ctrl.request.params.PageSize = getDefaultPageSize();

                    ctrl._overrideSearchParamsOnInit();
                    ctrl.momentDateFormat = DateUtils.getDateFormat().toUpperCase().replace(new RegExp('/','g'), '-');//momentjs only use format with dash (-)
                    ctrl.isAdvancedSearch = true;
                    if (ctrl._preGet()) {
                        ctrl.request.get().then(function () {
                            ctrl._init();
                            ctrl.loaded = true;
                        });
                    }
                },
                resetLocalStorageIfVersionChange: resetLocalStorageIfVersionChange,
                _overrideSearchParamsOnInit: angular.noop,
                _preGet: function () {
                    return true;
                },
                _extend: function (anotherScope) {
                    angular.extend(ctrl, anotherScope);
                },
                _go: function (state, params, options) {
                    return $rootScope.$state.go(state, params, options);
                },

                refresh: function() {
                    ctrl._go(ctrl._currentState, ctrl._params, {reload: true});
                },
                _currentState: $rootScope.$state.current.name,

                //$encode: $filter('encode'),
                //formatCurrency: $filter('currency'),
                 $apis: $apis,
                //used to inject templates
                url: url,
                //override for your logic
                _init: angular.noop,
                _useStorage: true, // To disabled local storage (for child listing)

                performAsync: function (action) {
                    ctrl.processing = true;
                    var doneProcessing = function (result) {
                        ctrl.processing = false;
                        return result;
                    };
                    var promise = action();
                    promise.then(doneProcessing, doneProcessing);
                    return promise;
                },

                rowNumOnPage: function (index, result) {
                    return result.PageSize
                        ? index + 1 + (result.PageNumber - 1)*result.PageSize
                        : index + 1;
                },

                //override if you use another lib
                _msg: function (message, type) {
                    if ('error' === type) {
                        console.error(message);
                        toastr.error(message, 'Error', {closeButton: true, timeOut: 0});
                    } else if (type === 'warning') {
                        toastr.warning(message, 'Warning');
                    } else {
                        toastr.success(message, 'Success');
                    }
                },
                _msgerr: function (message) {
                    ctrl._msg(message, 'error');
                },
                _alert: function (_title, _message, _type) {
                    swal({
                        title: _title,
                        type: _type,         // "warning", "error", "success" and "info"
                        html: _message
                    });
                },
                _alertWithAction: function (_title, _message, _type, action) {
                    swal({
                        title: _title,
                        type: _type,         // "warning", "error", "success" and "info"
                        html: _message
                    }).then(action);
                },
                _alertError: function (_message) {
                    ctrl._alert('Error', _message, "error");
                },
                _alertInfo: function (_title, _message) {
                    ctrl._alert(_title || "", _message, "info");
                },
                _confirm: function (message, action, html) {
                    swal({
                        title: "",
                        text: html ? null : message,
                        html: html ? message : '',
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Yes"
                    }).then(action);
                },
                _confirmSaveChanges: function (message, onConfirm, onCancel) {
                    swal({
                        title: "Are you sure?",
                        text: message ? message : 'You have un saved pending changes',
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        cancelButtonText: "Discard changes",
                        confirmButtonText: "Save changes"
                    }).then(onConfirm, onCancel);
                },
                _inputBox: function (title, message, allowsEmpty, onInputSuccessful) {
                    swal({
                        title: title,
                        text: message || 'Please type in the value',
                        input: 'text',
                        showCancelButton: true,
                        inputPlaceholder: 'Key in value here'
                    }).then(function (inputValue) {
                        if (inputValue === false) return false;
                        if (!allowsEmpty && (inputValue === "" || inputValue === undefined)) {
                            swal.showInputError('Input is required');
                            return false;
                        }
                        onInputSuccessful(inputValue);
                        return true;
                    });
                },

                //routes params
                _router: config.ROUTER,
                _params: $rootScope.$stateParams,
                //mode to capture after make request
                config: config,
                dateFormat: DateUtils.getDateFormat(),
                dateTimeFormat: DateUtils.getDateTimeFormat(),
                _mode: config.MODE,
                _currentMode: null,
                //current user
                CurrentUser: UserService.CurrentUser,
                //-- Common functions
                // Check changes of item
                isItemChanged: function (item, index) {
                    // Any special comparision
                    return !angular.equals(item, ctrl.OriginalResultSet[index]);
                },

                //override gor your function
                _api: function () {
                    throw "Please override _api(), and return API instance";
                },
                // Override search function for your controller,
                // user will input query string to serahc by configured fieldName
                // fieldName is fixed by html input(name=fieldName)
                // you can overide function of searching by field  _search_<fieldName>=function(query[,$scope]){}
                _search: function (query, fieldName) {
                    console.debug('Search for ', fieldName, ' with query', query);
                    var f = ctrl['_search_' + fieldName];

                    if (angular.isFunction(f)) {
                        return f.call(ctrl, query, ctrl);
                    }
                    //default use _api() to search
                    var params = {};
                    params[fieldName] = query;
                    return ctrl._api().get({params: params}).then(function (res) {
                        console.debug('Search', fieldName, 'with query', query, ' got result :', res);
                        return res.data.ResultSet;
                    });
                },

                /**
                 * Override to map result item to another structure.
                 * Default returns original item
                 */
                _resultMapper: function (item) {
                    return item;
                },
                _apiSuccessCallback: function (result) {
                    var data = result.data;
                    //result set response type
                    if (data.ResultSet) {
                        //remap to another structure if needing
                        angular.forEach(data.ResultSet, function (item, i) {
                            data.ResultSet[i] = ctrl._resultMapper(item);
                        });
                    }
                    ctrl.result = data;
                    ctrl.loading = false;
                    // clone result set to check changes
                    ctrl.OriginalResultSet = ctrl.result.ResultSet ? angular.copy(ctrl.result.ResultSet) : angular.copy(ctrl.result);
                    ctrl.isUnchanged = function (resultSet) {
                        return angular.equals(resultSet, ctrl.OriginalResultSet);
                    };

                    $scope.$emit('apiSuccess', data);
                    return data;
                },
                //override if you want another custom message
                onError: function (err) {
                    console.error('Error', err);
                    if (err.InnerExceptions !== undefined) {
                        var msg = "";
                        angular.forEach(err.InnerExceptions, function (e) {
                            msg += e.Message + " ([" + e.ExceptionType + ']:' + e.ExceptionMessage + ')' + "\n";
                            ctrl._msg(msg, 'error');
                        });
                    }
                    else {
                        ctrl._msgerr(err.Message);
                    }
                },
                _apiErrorCallback: function (err) {
                    if (!err) {
                        console.error('server no response', err);
                        return;
                    }
                    console.error('http request error', err);
                    //broadcast to do any stupid stuff you want :)
                    $scope.$emit('apiError', err);
                    ctrl.onError(err);
                    return err;
                },

                //sort dataset
                _sort: function (field, asc) {
                    var sort_asc = asc === undefined ? true : !asc;
                    ctrl.request.params.OrderBy = field;
                    ctrl.request.params.OrderAsc = sort_asc;
                    ctrl.request.get();
                },

                _handleRootScopeEvent: function (eventName, handler) {
                    $rootScope.$on(eventName, handler);
                },

                //do not override this
                request: {
                    //bind this with form model, see requisition/sidebar.html
                    params: {
                        PageSize: getDefaultPageSize(),
                        SearchMatchType: config.DEFAULT_SEARCH_MATCH_TYPE
                    },
                    // list of parameters that will be excluded when search filter is reset
                    paramsExcludedFromFilterReset: [],
                    get: function () {
                        ctrl.request.params.PageNumber = ctrl.request.params.PageNumber === 0 ? 1 : ctrl.request.params.PageNumber;
                        return ctrl._api()
                            .all({ params: ctrl.request.params })
                            .then(ctrl._apiSuccessCallback)
                            .catch(ctrl._apiErrorCallback);
                    },
                    id: function () {
                        if (!ctrl.request.params.id) {
                            throw "Invalid id in $scope.request.params.id";
                        }

                        ctrl._api()
                            .id(ctrl.request.params.id)
                            .then(ctrl._apiSuccessCallback)
                            .catch(ctrl._apiErrorCallback);
                    },
                    form: function () {
                        ctrl._api()
                            .form(ctrl.request.params)
                            .then(ctrl._apiSuccessCallback)
                            .catch(ctrl._apiErrorCallback);
                    }
                }
            });

            //use url request param as default if available
            angular.extend(ctrl.request.params, $rootScope.$stateParams);

            var resetDefaultParams = function () {
                //should reset by this because ng-model binding inner attributes, they are must reset too
                angular.forEach(ctrl.request.params, function (v, k) {
                    if (ctrl.request.paramsExcludedFromFilterReset.indexOf(k) > -1)
                        return;
                    ctrl.request.params[k] = (k.toLowerCase() === 'companyid')
                        ? ctrl.getDefaultCompany()
                        : null;
                });

                //default params
                angular.extend(ctrl.request.params, {
                    PageSize: getDefaultPageSize(),
                    PageNumber: 1,
                    SearchMatchType : config.DEFAULT_SEARCH_MATCH_TYPE
                }, $rootScope.$stateParams);

                //pagination bind to this attr
                ctrl.result = ctrl.result || {};
                ctrl.result.PageNumber = 1;
            };

            var doReloadData = function () {
                angular.extend(ctrl.request.params, {PageNumber: (ctrl.result || {PageNumber: 1}).PageNumber});
                ctrl._preGet();
                ctrl.request.get();
            };

            // Reset paging and reload data
            var doResetData = function () {
                resetDefaultParams();
                storage.remove(ctrl._currentState); //reset local storage
                ctrl._preGet();
                ctrl.request.get();
            };

            var defaultDataListener = {
                //this is fired on page change, see /partial/paging.html
                pageChanged: doReloadData,
                formSubmitted: doReloadData,
                formReset: doResetData,
                filterReset: doResetData
            };
            angular.forEach(defaultDataListener, function (v, k) {
                $scope.$on(k, v);
            });
        };

        return {
            extend: function (ctrl, $scope, $api) {
                Base($scope, ctrl);
                if (typeof $api == 'function') {
                    ctrl._api = $api;
                } else if (typeof $api == 'string') {
                    ctrl._api = function () {
                        return ctrl.$apis[$api];
                    };
                }
            }
        };
    }

    function BaseCtrl(BaseCtrlAs) {
        return {
            extend: function ($scope, $api) {
                BaseCtrlAs.extend($scope, $scope, $api);
            }
        };
    }
})(window.angular);

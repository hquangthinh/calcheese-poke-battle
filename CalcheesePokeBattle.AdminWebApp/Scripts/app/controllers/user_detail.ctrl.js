﻿(function (angular) {
    'use strict';

    angular
        .module('CalcheeseAdmin')
        .controller('UserDetailCtrl', UserDetailCtrl);

    UserDetailCtrl.$inject = ['$scope', 'BaseDetailCtrlAs', 'UserService', 'R'];

    function UserDetailCtrl($scope, BaseDetailCtrlAs, UserService, R) {
        var ctrl = this;
        BaseDetailCtrlAs.extend(ctrl, $scope, 'users');

        ctrl.backLink = ctrl._router.USER_LIST;
        ctrl.isNewRecord = ctrl._params.Id === 'new';
        ctrl.binding = {
            focusUserName: ctrl.isNewRecord
        };

        ctrl.loadUserGameHistories = function () {
            UserService.getUserGameHistories(ctrl._params.Id).then(function (gameHistoryData) {
                ctrl.userGameHistories = gameHistoryData;
            });
        }

        ctrl.loadUserActivityHistories = function() {
            UserService.loadUserActivityHistories(ctrl.model.UserName).then(function (historyData) {
                ctrl.userActivityHistories = historyData;
            });
        }

        ctrl.getUserUsedCouponCodes = function() {
            UserService.getUserUsedCouponCodes(ctrl._params.Id).then(function (usedCodeData) {
                ctrl.userAllUsedCouponCodes = usedCodeData;
            });
        }

        ctrl.addRole = function () {
            if (ctrl.binding.currentRole && ctrl.binding.currentRole !== '' && ctrl.binding.currentRole.trim().length > 0) {
                if (R.findIndex(R.propEq('Name', ctrl.binding.currentRole))(ctrl.model.UserRoles) === -1) {
                    ctrl.model.UserRoles.push({ Name: ctrl.binding.currentRole });
                    ctrl.binding.currentRole = undefined;
                }
            }
        }

        ctrl.removeRole = function(item) {
            var removeIndex = R.findIndex(R.propEq('Name', item.Name))(ctrl.model.UserRoles);
            if (removeIndex === -1)
                return;
            ctrl.model.UserRoles = R.remove(removeIndex, 1, ctrl.model.UserRoles);
        }
    }

})(window.angular);
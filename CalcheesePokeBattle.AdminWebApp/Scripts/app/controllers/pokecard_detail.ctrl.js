﻿(function (angular) {
    'use strict';

    angular
        .module('CalcheeseAdmin')
        .controller('PokeCardDetailCtrl', PokeCardDetailCtrl);

    PokeCardDetailCtrl.$inject = ['$scope', 'BaseDetailCtrlAs', 'PokeCardService', 'R', 'SettingService'];

    function PokeCardDetailCtrl($scope, BaseDetailCtrlAs, PokeCardService, R) {
        var ctrl = this;
        BaseDetailCtrlAs.extend(ctrl, $scope, 'pokecards');
        ctrl.backLink = ctrl._router.POKE_CARD_LIST;
        ctrl.isNewRecord = ctrl._params.Id === 'new';
    }

})(window.angular);
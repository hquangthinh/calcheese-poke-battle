(function (angular) {
    'use strict';

    var app = angular
        .module('CalcheeseAdmin',
        [
            'ngResource',
            'ngAnimate',
            'ngSanitize',
            'ui.bootstrap',
            'ui.router',
            'ngStorage',
            'angular-loading-bar',
            'ncy-angular-breadcrumb',
            'angularFileUpload',
            'toastr',
            'angular-momentjs',
            'ngCookies'
        ]);

    // Ramda support
    app.constant('R', window.R);

    // Config
    app.config([
        '$httpProvider', '$uibTooltipProvider', 'config', 'apiBaseUrlProvider',
        '$uibModalProvider', '$compileProvider', '$qProvider',
        function($httpProvider,
            $tooltipProvider,
            config,
            apiBaseUrlProvider,
            $modalProvider,
            $compileProvider,
            $qProvider) {
            'use strict';
            // intercept for authorization
            var interceptor = [
                '$rootScope', 'config', '$q', 'R', function($scope, $constant, $q, R) {
                    return {
                        'request': function(config) {
                            config.timeout = 60000;
                            return config;
                        },

                        'response': function(response) {
                            if (response.status === 204) { // No content
                                $scope.$state.go($constant.ROUTER.DASHBOARD);
                            }
                            return response;
                        },

                        'responseError': function(response) {
                            console.error('http response error', response);
                            // Replace error icon
                            angular.element('.loading').toggleClass('error');

                            // connection error , can not connect to server
                            if (response.status === 0 || response.status === -1) {
                                var err = "Connection error";
                                var msg = "Cannot connect to server, please try again!";
                                swal({
                                    title: err,
                                    text: msg,
                                    type: "error"
                                });
                            }
                            // bad request
                            else if (response.status === 400 && !response.config.ignoreError) {
                                var errorDetails = "";
                                if (response.data.ModelState) {
                                    errorDetails = R.join('\n', R.values(response.data.ModelState));
                                } else {
                                    errorDetails =
                                        (response.data.Message ? response.data.Message : "") +
                                        (response.data.ExceptionMessage ? response.data.ExceptionMessage : "") +
                                        (response.data.InnerException && response.data.InnerException.ExceptionMessage
                                            ? response.data.InnerException.ExceptionMessage
                                            : "");
                                }
                                // alert to notify
                                swal({
                                    title: 'Stop',
                                    text: errorDetails,
                                    type: 'warning'
                                });
                            }
                            // unauthorized
                            else if (response.status === 401) {
                                swal({
                                        title: "Unauthorized",
                                        text: response.data.Message || "You are not authorised to perform this action.",
                                        type: "warning"
                                    },
                                    function() {
                                        $scope.$state.go($constant.ROUTER.LOGOUT);
                                    });
                            }
                            // forbidden
                            else if (response.status === 403) {
                                // redirect to login page
                                var returnUrl = window.location.href.indexOf('/logout') > 0
                                    ? null
                                    : window.location.href;
                                if ($scope.$state.current.name !== $constant.ROUTER.LOGIN) {
                                    $scope.$state.go($constant.ROUTER.LOGIN,
                                        { returnUrl: returnUrl },
                                        { reload: true });
                                }
                            }
                            // 409: resource conflict
                            else if (response.status === 409) {
                                errorDetails =
                                    (response.data.Message ? response.data.Message : "") +
                                    (response.data.ExceptionMessage ? response.data.ExceptionMessage : "") +
                                    (response.data.InnerException && response.data.InnerException.ExceptionMessage
                                        ? response.data.InnerException.ExceptionMessage
                                        : "");
                                // alert to notify
                                swal({
                                    title: response.statusText,
                                    text: errorDetails,
                                    type: "error"
                                });
                                $scope.$state.go($scope.$state.current.name, $scope.$stateParams, { reload: true });
                                $scope.$emit($scope.$state.current.name + '_resource_conflict',
                                    $scope.$state,
                                    $scope.$stateParams);
                            }
                            // internal server error || method not allow
                            else if (response.status >= 500 || response.status === 405) {
                                // redirect to errors page
                                $scope.$state.go($constant.ROUTER.ERRORS, { ErrorResponse: response });
                            }

                            return $q.reject(response);
                        }
                    };
                }
            ];

            $httpProvider.defaults.withCredentials = true;
            $httpProvider.interceptors.push(interceptor);

            // Tooltip config
            $tooltipProvider.options({
                popupDelay: 200,
                appendToBody: true
            });

            $modalProvider.options.animation = true;
            $modalProvider.options.size = "lg";

            // set to false if we don't want to see warning from framework
            $compileProvider.debugInfoEnabled(true);

            // turn off console error when a promise is rejected and there no handler for it
            //$qProvider.errorOnUnhandledRejections(false);
        }
    ]);

    // Run
    app.run([
        '$rootScope', '$uibModalStack', '$state', '$stateParams', '$http', 'webBaseUrl', 'UserService',
        function($rootScope,
            $modalStack,
            $state,
            $stateParams,
            $http,
            webBaseUrl,
            userService) {
            // Anti-forgery token
            $http.defaults.headers.common['__RequestVerificationToken'] = angular.element('span#token-af')
                .attr('value');

            $rootScope.$state = $state;
            $rootScope.$stateParams = $stateParams;
            $rootScope.overlay = {
                show: false,
                content: 'loading...'
            };

            if ($state.current.name === 'login') return;

            userService.getCurrentUser().then(function() {
                $rootScope._loggedIn = true;
            });

            // dismiss any active modal if current state changes
            $rootScope.$on('$stateChangeStart',
                function (event, toState, toParams, fromState, fromParams) {
                    var top = $modalStack.getTop();
                    if (top) $modalStack.dismiss(top.key);
                });

            // Base url to used in view
            $rootScope.webUrl = webBaseUrl;
        }
    ]);

})(window.angular);
(function (angular) {
    'use strict';

    angular
        .module('CalcheeseAdmin')
        .config([
            '$stateProvider', '$urlMatcherFactoryProvider', '$urlRouterProvider', '$locationProvider',
            '$breadcrumbProvider', 'toastrConfig', 'config', routerConfig
        ]);

    function routerConfig($stateProvider,
        $urlMatcherFactoryProvider,
        $urlRouterProvider,
        $locationProvider,
        $breadcrumbProvider,
        toastrConfig,
        config) {

        $urlMatcherFactoryProvider.caseInsensitive(true);
        $urlMatcherFactoryProvider.strictMode(false);

        // enable html5 mode
        $locationProvider.html5Mode(true);

        // Root of breadcrumbs
        $breadcrumbProvider.setOptions({
            // prefixStateName: 'dashboard',
            templateUrl: function() {
                return urlService().partial('/com-breadcrumb.tmpl.html');
            }
        });

        var basicResolve = {
            CurrentUser: [
                'UserService', function(UserService) {
                    return UserService.getCurrentUser();
                }
            ]
        };

        var defaultResolve = angular.extend({}, basicResolve);

        var noResolve = { Nothing: function() {} };

        function urlService() {
            return angular.element(document.body).injector().get('url');
        }

        // helper to make cleaner code
        // map route to controller and template
        function map(state, route, ctrl, tmpl, isDefaultRoute, label, parent, resolve, params) {
            if (isDefaultRoute === true) {
                $urlRouterProvider.otherwise(route);
            }

            $stateProvider.state(state,
            {
                url: route,
                controller: ctrl,
                templateUrl: function() {
                    return urlService().tmpl(tmpl);
                },
                ncyBreadcrumb: {
                    label: label,
                    parent: parent || null
                },
                resolve: resolve || defaultResolve,
                params: params
            });
        }

        function mapControllerAs(state, route, ctrl, tmpl, isDefaultRoute, label, parent, resolve, params) {
            map(state, route, ctrl ? ctrl + ' as ctrl' : null, tmpl, isDefaultRoute, label, parent, resolve, params);
        }

        // application routers
        map(config.ROUTER.WELCOME, '/', 'WelcomeCtrl', '/dashboard/welcome.html', false, 'Calcheese Admin', null, {});
        mapControllerAs(config.ROUTER.LOGIN, '/login?returnUrl', 'LoginCtrl', '/dashboard/login-v2.html', true, 'Calcheese Admin', null, {});
        map(config.ROUTER.LOGOUT, '/logout', 'LogoutCtrl', '/dashboard/logout.html', false, 'Calcheese Admin', null, {});
        mapControllerAs(config.ROUTER.ERRORS, '/errors?errorMsg', 'ErrorsCtrl', '/dashboard/errors.html', false, 'Calcheese Admin', null, {}, { ErrorResponse: null });
        mapControllerAs(config.ROUTER.DASHBOARD, '/dashboard', 'DashboardCtrl', '/dashboard/index.html', true, 'Dashboard', null, defaultResolve, {});

        // user routes
        mapControllerAs(config.ROUTER.USER_LIST, '/users', 'UserSearchCtrl', '/users/index.html', false, 'Users', config.ROUTER.DASHBOARD, defaultResolve, {});
        mapControllerAs(config.ROUTER.USER_DETAIL, '/users/:Id', 'UserDetailCtrl', '/users/detail.html', false, '{{ ctrl._params.Id === "new" ? "Add User" : "Edit User" }}', config.ROUTER.USER_LIST, defaultResolve, {});

        //gift item routes
        mapControllerAs(config.ROUTER.GIFT_ITEM_LIST, '/giftitems', 'GiftItemSearchCtrl', '/giftitems/index.html', false, 'GiftItem', config.ROUTER.DASHBOARD, defaultResolve, {});
        mapControllerAs(config.ROUTER.GIFT_ITEM_DETAIL, '/giftitems/:Id', 'GiftItemDetailCtrl', '/giftitems/detail.html', false, '{{ ctrl._params.Id === "new" ? "Add GiftItem" : "Edit GiftItem" }}', config.GIFT_ITEM_LIST, defaultResolve, {});

        //Poke Card
        mapControllerAs(config.ROUTER.POKE_CARD_LIST, '/pokecards', 'PokeCardSearchCtrl', '/pokecards/index.html', false, 'PokeCard', config.ROUTER.DASHBOARD, defaultResolve, {});
        mapControllerAs(config.ROUTER.POKE_CARD_DETAIL, '/pokecards/:Id', 'PokeCardDetailCtrl', '/pokecards/detail.html', false, '{{ ctrl._params.Id === "new" ? "Add PokeCard" : "Edit PokeCard" }}', config.POKE_CARD_LIST, defaultResolve, {});

        //prize_lucky_draw
        mapControllerAs(config.ROUTER.PRIZE_LUCKY_DRAW, '/prizeluckydraw', 'PrizeLuckyDrawCtrl', '/prizeluckydraw/index-v2.html', false, 'Lucky Draw Code', config.ROUTER.DASHBOARD, defaultResolve, {});

        //Upload Coupon file
        mapControllerAs(config.ROUTER.UPLOAD_COUPON, '/couponcode', 'CouponCodeCtrl', '/couponcode/index.html', false, 'Coupon Code', config.ROUTER.DASHBOARD, defaultResolve, {});

        //site_statistics
        mapControllerAs(config.ROUTER.SITE_STATISTICS, '/statistic', 'StatisticCtrl', '/statistic/index.html', false, 'statistic', config.ROUTER.DASHBOARD, defaultResolve, {});

        //lucky_draw_candidate_list_by_week
        mapControllerAs(config.ROUTER.LUCKY_DRAW_CANDIDATE_LIST_BY_WEEK, '/luckydraw', 'LuckyDrawCtrl', '/luckydraw/index.html', false, 'Lucky Draw', config.ROUTER.DASHBOARD, defaultResolve, {});

        //USED_CODE_CARD_DETAIL_LIST
        mapControllerAs(config.ROUTER.USED_CODE_CARD_DETAIL_LIST, '/usercoderelease', 'UserCodeReleaseCtrl', '/usercoderelease/index.html', false, 'Code', config.ROUTER.DASHBOARD, defaultResolve, {});

        //USED_CODE_CARD_SUMMARY_BY_DATE
        mapControllerAs(config.ROUTER.USED_CODE_CARD_SUMMARY_BY_DATE, '/usercoderelease-summary-by-date', 'UserCodeReleaseSummaryByDateCtrl', '/usercoderelease/used_code_card_summary_by_date.html', false, 'Code', config.ROUTER.USED_CODE_CARD_DETAIL_LIST, defaultResolve, {});

        //CARD_RELEASE_DETAIL_LIST
        mapControllerAs(config.ROUTER.CARD_RELEASE_DETAIL_LIST, '/cardrelease', 'CardReleaseCtrl', '/cardrelease/index.html', false, 'Card', config.ROUTER.DASHBOARD, defaultResolve, {});

        //PAGE_VIEW_REPORT
        mapControllerAs(config.ROUTER.PAGE_VIEW_REPORT, '/page-view-report', 'PageViewReportPageCtrl', '/reports/page-views.html', false, 'Page View Report', config.ROUTER.DASHBOARD, defaultResolve, {});

        //ALL_GAME_PAGE_VIEW
        mapControllerAs(config.ROUTER.ALL_GAME_PAGE_VIEW, '/all-game-page-views', 'AllGamePageViewsPageCtrl', '/reports/all-game-page-views.html', false, 'All Game Page Views', config.ROUTER.PAGE_VIEW_REPORT, defaultResolve, {});

        //GIFT_REDEEM_REPORT
        mapControllerAs(config.ROUTER.GIFT_REDEEM_REPORT, '/gift-redeem-report', 'GiftRedeemReportPageCtrl', '/reports/gift-redeem.html', false, 'Gift Redeem Report', config.ROUTER.DASHBOARD, defaultResolve, {});

        // Toast default settings
        angular.extend(toastrConfig,
        {
            positionClass: 'toast-bottom-full-width'
        });
    }

})(window.angular);
(function(angular) {
    'use strict';

    angular
        .module('CalcheeseAdmin')
        .constant('config',
        {
            VERSION: '0.0.1.e3b9152e672f3172e698b98db304fedcf85ec2c4',
            MODE: 'dev',
            PAGE_SIZE: 10,
            DEFAULT_SEARCH_MATCH_TYPE: 'contains',
            ALLOW_CHANGE_USER_PASSWORD: true,
            SITE_MODE: 'live',
            ROUTER: {
                DASHBOARD: "dashboard",
                WELCOME: 'welcome',
                LOGIN: "login",
                LOGOUT: "logout",
                ERRORS: "errors",
                USER_LIST: "user_list",
                USER_DETAIL: "user_detail",
                ADD_USER_DETAIL: "add_user_detail",
                EDIT_USER_DETAIL: "edit_user_detail",
                GIFT_ITEM_LIST: "gift_item_list",
                GIFT_ITEM_DETAIL: "gift_item_detail",
                POKE_CARD_LIST: "poke_card_list",
                POKE_CARD_DETAIL: "poke_card_detail",
                COUPON_CODE_LIST: "coupon_code_list",
                COUPON_CODE_DETAIL: "coupon_code_detail",
                PRIZE_LUCKY_DRAW: "prize_lucky_draw",
                SITE_STATISTICS: "site_statistics",
                UPLOAD_COUPON: "upload_coupon",
                CARD_RELEASE_DETAIL_LIST: "card_release_detail_list",
                LUCKY_DRAW_CANDIDATE_LIST_BY_WEEK: "lucky_draw_candidate_list_by_week",
                USED_CODE_CARD_DETAIL_LIST: "used_code_card_detail_list",
                USED_CODE_CARD_SUMMARY_BY_DATE: "used_code_card_summary_by_date",
                PAGE_VIEW_REPORT: "page_view_report",
                ALL_GAME_PAGE_VIEW: "all_game_page_views",
                GIFT_REDEEM_REPORT: "gift_redeem_report",
                REPORT_USER_REGISTRATION_BY_DATE: "report_user_registration_by_date",
                REPORT_POKEMON_GAME_BY_DATE: "report_pokemon_game_by_date"
            },
            MAX_FILE_SIZE:10000000
        });

})(window.angular);
(function (angular) {
    'use strict';

    angular
        .module('CalcheeseAdmin')
        .factory('apis', apis);

    apis.$inject = ['$http', '$q', 'apiBaseUrl', '$upload', 'R'];

    function apis($http, $q, apiBaseUrl, $upload, R) {
        /**
         * In case you have different base url for special api
         * you can set in its instance but not common base url
         * eg. var api= new API('/location'); //base url is default from global base url provided by apiBaseUrlProvider
         * eg. var api= new API('/location','http://someotherhost/api'); //want to call difference host than common host
         */
        function API(api, idField, opt_baseUrl) {
            var me = this;
            if (!api) throw "Api is null. Please provide api on creating ex: new API('/location','LocationCode')";
            if (!idField) throw "idField is null. Please provide api on creating ex: new API('/location','LocationCode')";

            // public functions
            this.api = api;
            this.idField = idField;
            this.baseUrl = opt_baseUrl || apiBaseUrl;
            this.apiUrl = apiBaseUrl + api;
            this.path = path;
            this.id = id;
            this.all = all;
            /**
             * Use all() to get result set. This is mapped as the same server api functions
             * @deprecated
             */
            this.get = all;
            this.getFromUri = getFromUri;
            this.post = post;
            this.postToUri = postToUri;
            this.form = form;
            this.delete = apiDelete;
            this.del = del;
            this.put = put;
            this.upload = upload;
            /**
             * Search by a field
             */
            this.search = search;
            this.searchActive = searchActive;
            this.subUri = subUri;

            function encodeId(id) {
                if (!R.is(String, id)) return id;
                return id
                    .replace('&', '___amp___')
                    .replace('<', '___lt___')
                    .replace('>', '___gt___')
                    .replace('*', '___star___')
                    .replace('%', '___perc___')
                    .replace(':', '___colon___')
                    .replace('/', '___slash___')
                    .replace('\\', '___bslsh___')
                    .replace('#', '___hash___')
                    .replace('?', '___quest___')
                    .replace('!', '___exclm___')
                    .replace('(', '___obrkt___')
                    .replace(')', '___cbrkt___')
                    .replace('+', '___plus___')
                    ;
            }

            // function declarations
            function path(urlPath, opt_config) {
                opt_config = opt_config || {};
                var url = me.baseUrl + me.api + '/' + urlPath;
                if (opt_config.queryString) {
                    url = url + '?' + opt_config.queryString;
                }
                return $http.get(url, opt_config);
            }

            function id(id, opt_config) {
                return path(encodeId(id), opt_config);
            }

            function all(opt_config) {
                return $http.get(me.baseUrl + me.api, opt_config || {});
            }

            function getUriPath(uri) {
                return R.head(uri) === '/' ? uri : '/' + uri;
            }

            function getFromUri(id, uri, opt_config) {
                return path(encodeId(id) + getUriPath(uri), opt_config);
            }

            function post(opt_data, opt_config) {
                return httpPost(me.baseUrl + me.api, opt_data, opt_config);
            }

            function postToUri(uri, opt_data, opt_config) {
                return httpPost(me.baseUrl + me.api + getUriPath(uri), opt_data, opt_config);
            }

            function subUri(uri) {
                return new API(me.api + getUriPath(uri), me.idField, me.baseUrl);
            }

            function form(opt_data) {
                return $http({
                    method: 'POST',
                    url: me.baseUrl + me.api,
                    data: $.param(opt_data),
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                });
            }

            function apiDelete(opt_config) {
                return httpDelete(me.baseUrl + me.api, opt_config);
            }

            function del(id, opt_config) {
                return httpDelete(me.baseUrl + me.api + '/' + encodeId(id), opt_config);
            }

            function put(opt_data, opt_config) {
                return $http.put(me.baseUrl + me.api, opt_data || {}, opt_config || {});
            }

            function upload(maxFileSize, file, action, opt_data) {
                if (file.size > maxFileSize) {
                    return $q(function (resolve, reject) {
                        var fileSizeInMbs = truncateDecimals(maxFileSize / 1048576, 2);
                        var errorMessage = "File size exceeds " + fileSizeInMbs + " MB";
                        console.error(errorMessage);
                        return reject(errorMessage);
                    });
                }
                return $upload.upload({
                    method: 'POST',
                    url: me.baseUrl + me.api + (action ? '/' + action : ''),
                    data: opt_data || {},
                    file: file
                });
            }

            function search(query, fieldName, matchType, companyId) {
                var params = createSearchParams(query, fieldName, matchType, companyId);
                return all({params: params}).then(function (res) {
                    return res.data.ResultSet;
                });
            }

            function searchActive(query, fieldName, matchType, companyId) {
                var params = createSearchParams(query, fieldName, matchType, companyId);
                params.Active = true;
                return all({ params: params }).then(function (res) {
                    return res.data.ResultSet;
                });
            }

            function createSearchParams(query, fieldName, matchType, companyId) {
                var params = {};
                angular.forEach(fieldName.split(/[,;\s]/), function (name) {
                    if (name === 'CompanyID' || name === 'CompanyCode') {
                        params[name] = companyId;
                    }
                    else {
                        params[name] = query;
                    }
                });
                params.SearchMatchType = matchType;

                return params;
            }

            function truncateDecimals(number, digits) {
                var multiplier = Math.pow(10, digits),
                    adjustedNum = number * multiplier,
                    truncatedNum = Math[adjustedNum < 0 ? 'ceil' : 'floor'](adjustedNum);

                return truncatedNum / multiplier;
            }

            function httpDelete(url, opt_config) {
                return $http['delete'](url, opt_config || {});
            }

            function httpPost(url, opt_data, opt_config) {
                return $http.post(url, opt_data || {}, opt_config || {});
            }
        }

        return {
            couponcode: new API('/CouponUpload', 'Unknown'),
            prizeluckydraw: new API('/prizeluckydraw', 'Unknown'),
            giftitems: new API('/giftitem', 'Id'),
            pokecards: new API('/pokecard', 'Id'),
            users: new API('/pikachu-secret', 'Id'),
            login: new API('/admin-auth/login', 'Unknown'),
            auth: new API('/admin-auth', 'Unknown'),
            report: new API('/reports', 'Unknown'),
            report_gift_redeem_by_week: new API('/reports/gift-redeem-by-week', 'Unknown'),
            usedcouponcodereport: new API('/usedcouponcodereport', 'Unknown'),
            usedcouponcodereport_summary_by_date: new API('/usedcouponcodereport/summary-by-date', 'Unknown'),
            page_view_tracking: new API('/page-view-tracking', 'Unknown')
        };
    }
})(window.angular);

(function (angular) {
    'use strict';

    angular
        .module('CalcheeseAdmin')
        .service("CouponCodeService", CouponCodeService);

    CouponCodeService.$inject = ["apis"];

    function CouponCodeService(apis) {
        var service = this;
        var serviceApi = apis.couponcode;

        service.getAllUploadInfo = function() {
            return serviceApi.path('all-upload-info');
        }

        // trigger a message to import queue
        service.triggerImportQueueMessage = function() {
            return serviceApi.postToUri('trigger-queue');
        }

        service.deleteFile = function(fileInfo) {
            return serviceApi.postToUri('delete-file', fileInfo);
        }
    }
})(window.angular);

(function (angular) {
    'use strict';

    angular
        .module('CalcheeseAdmin')
        .service("GiftItemService", GiftItemService);

    GiftItemService.$inject = ["apis"];

    function GiftItemService(apis, $rootScope, Memoization) {
        var service = this;
        var giftItemApi = apis.giftitems;

        /** @memberOf GiftItem **/
        service.CurrentGiftItem = null;
        /** @memberOf GiftItem **/
        service.updateGiftItem = updateGiftItem;
        /** @memberOf GiftItem **/
        service.getGiftItem = getGiftItemDetail;
        /** @memberOf GiftItem **/
        service.deleteGiftItem = deleteGiftItem;
        /** @memberOf GiftItem **/
        service.searchGiftItems = searchGiftItems;
     

       

        function updateGiftItem(model) {
            var postedData = {};
            angular.forEach(model, function (item) {
                postedData[item.PropertyInternalName] = item.PropertyValue;
            });
            return giftItemApi.postToUri('/', postedData);
        }

        function getGiftItemDetail(itemID) {
            return giftItemApi.id(itemID).then(function(result) {
                return result.data;
            });
        }
       

        function deleteGiftItem() {
            delete service.CurrentGiftItem;
        }
        

        function searchGiftItems(active, query, findAll, permission) {
            var permissionId = permission ? permission : null;
            return giftItemApi.all({params: {
                Keyword: query,
                SearchMatchType: 'contains',
                FindAll: findAll === true
            }}).then(function (result) {
                return result.data.ResultSet;
            });
        }
      
    }
})(window.angular);

(function (angular) {
    'use strict';

    angular
        .module('CalcheeseAdmin')
        .service('PageViewTrackingService', PageViewTrackingService);

    PageViewTrackingService.$inject = ['apis'];

    function PageViewTrackingService(apis) {
        var service = this;
        var serviceApi = apis.page_view_tracking;

        /** @memberOf PageViewTrackingService **/
        service.getPageViewReportDetail = getPageViewReportDetail;

        /** @memberOf PageViewTrackingService **/
        service.getPageViewReportSummaryByPage = getPageViewReportSummaryByPage;

        /** @memberOf PageViewTrackingService **/
        service.getPageViewReportSummaryByUser = getPageViewReportSummaryByUser;

        function getPageViewReportDetail(pagetitle) {
            return serviceApi.postToUri('page-view-report-detail', { PageTitle: pagetitle });
        }

        function getPageViewReportSummaryByPage() {
            return serviceApi.postToUri('page-view-report-summary-by-page');
        }

        function getPageViewReportSummaryByUser() {
            return serviceApi.postToUri('page-view-report-summary-by-username');
        }
    }
})(window.angular);

(function (angular) {
    'use strict';

    angular
        .module('CalcheeseAdmin')
        .service("PokeCardService", PokeCardService);

    PokeCardService.$inject = ["apis"];

    function PokeCardService(apis, $rootScope, Memoization) {
        var service = this;
        var pokecardApi = apis.pokecards;

        
        service.updatePokeCard = updatePokeCard;
        service.getPokeCardDetail = getPokeCardDetail;
        service.deletePokeCard = deletePokeCard;
        service.searchPokeCards = searchPokeCards;
        service.allCardRelease = allCardRelease;


        function allCardRelease() {
            return pokecardApi.path("allCardRelease").then(function (result) {
                return result.data;
            });
        }


        function updatePokeCard(model) {
            var postedData = {};
            angular.forEach(model, function (item) {
                postedData[item.PropertyInternalName] = item.PropertyValue;
            });
            return pokecardApi.postToUri('/', postedData);
        }

        function getPokeCardDetail(itemID) {
            return pokecardApi.id(itemID).then(function (result) {
                return result.data;
            });
        }


        function deletePokeCard() {
            delete service.CurrentGiftItem;
        }


        function searchPokeCards(active, query, findAll, permission) {
            var permissionId = permission ? permission : null;
            return pokecardApi.all({
                params: {
                    Keyword: query,
                    SearchMatchType: 'contains',
                    FindAll: findAll === true
                }
            }).then(function (result) {
                return result.data.ResultSet;
            });
        }

    }
})(window.angular);

(function (angular) {
    'use strict';

    angular
        .module('CalcheeseAdmin')
        .service("PrizeLuckyDrawService", PrizeLuckyDrawService);

    PrizeLuckyDrawService.$inject = ["apis"];

    function PrizeLuckyDrawService(apis, $rootScope, Memoization) {
        var service = this;
        var prizeLuckyDrawdApi = apis.prizeluckydraw;
        service.initCurrentWeek = initCurrentWeek;
        service.getPrizeHistory = getPrizeHistory;
        service.getWinnerUserList = getWinnerUserList;
        service.processPrize = processPrize;
        service.removePrize = removePrize;
        service.setPrize = setPrize;

        function initCurrentWeek() {
            return prizeLuckyDrawdApi.path("GetCurrentWeekData").then(function (result) {
                return result.data;
            });
        }

        function getPrizeHistory(WeekNumber) {
            return prizeLuckyDrawdApi.path("GetPrizeHistory", {
                params: {
                    WeekNumber: WeekNumber
                }
            }).then(function (result) {
                return result.data;
            });
        }

        function getWinnerUserList(WeekNumber, Prize) {
            return prizeLuckyDrawdApi.path("GetWinnerUserList", {
                params: {
                    WeekNumber: WeekNumber,
                    Prize: Prize
                }
            }).then(function (result) {
                return result.data;
            });
        }

        function processPrize(WeekNumber) {
            return prizeLuckyDrawdApi.path("ProcessPrize", {
                params: {
                    WeekNumber: WeekNumber
                }
            }).then(function (result) {
                return result.data;
            });
        }

        function setPrize(weeknumber, prize) {
            return prizeLuckyDrawdApi.postToUri("SetPrize", {
                    Weeknumber:weeknumber,
                    Prize: prize
            }).then(function (result) {
                return result.data;
            });
        }

        function removePrize(id) {
          return  apis.prizeluckydraw.delete({
                params: {
                    Id: id
                }
            }).then(function (result) {
                return result.data;
            });
        }

    }
})(window.angular);

(function (angular) {
    'use strict';

    angular
        .module('CalcheeseAdmin')
        .service("ReportService", ReportService);

    ReportService.$inject = ["apis"];

    function ReportService(apis) {
        var service = this;
        var serviceApi = apis.report;
        var pageViewTrackingApi = apis.page_view_tracking;

        service.getAllImportJobStatus = function () {
            return serviceApi.path('coupon-import-job-status');
        }

        service.userCouponCodeStatistic = function () {
            return serviceApi.path('coupon-statistic');
        }

        service.getGiftSummaryByWeek = function (weekNumber) {
            return serviceApi.path('gift-summary-by-week', { queryString: 'WeekNumber=' + weekNumber })
                .then(function(res) {
                    return res.data;
                });
        }

        service.getGiftRedeemByWeek = function (weekNumber) {
            return serviceApi.path('gift-redeem-by-week', { queryString: 'WeekNumber=' + weekNumber })
                .then(function(res) {
                    return res.data;
                });
        }

        service.getPageViewReportDetail = function (pageTitle) {
            var query = { PageTitle: pageTitle };
            return pageViewTrackingApi.postToUri('/page-view-report-detail', query).then(function(res) {
                return res.data;
            });
        }

        service.getPageViewReportSummaryByPage = function () {
            return pageViewTrackingApi.postToUri('/page-view-report-summary-by-page').then(function (res) {
                return res.data;
            });
        }

        service.getPageViewReportSummaryByUser = function (weekNumber, pageTitle) {
            return pageViewTrackingApi.postToUri('/page-view-report-summary-by-username', { WeekNumber: weekNumber, PageTitle: pageTitle}).then(function (res) {
                return res.data;
            });
        }

        service.getAllGamePageViews = function (lastAccessType) {
            return pageViewTrackingApi.postToUri('/all-page-view', { LastAccessType: lastAccessType })
                .then(function(res) {
                    return res.data;
                });
        }
    }
})(window.angular);

(function (angular) {
    'use strict';

    angular
        .module('CalcheeseAdmin')
        .service("SettingService", SettingService);

    SettingService.$inject = ["apis", "UserService", "config", "storage", "R"];

    function SettingService(apis, UserService, config, storage, R) {
        var service = this;
        var settingsApi = apis.settings;

        var Settings = {
            asAttachmentMaxFileSize: 82
        };

        /** @memberOf SettingService **/
        service.dataImportSupportedFileTypes = ["application/vnd.ms-excel", "text/csv"];
        /** @memberOf SettingService **/
        service.getSettingValue = getSettingValue;
        /** @memberOf SettingService **/
        service.getServerSettingValue = getServerSettingValue;
        /** @memberOf SettingService **/
        service.getPreferredLocale = getPreferredLocale;
        /** @memberOf SettingService **/
        service.saveSetting = saveSetting;
        /** @memberOf SettingService **/
        service.SETTINGS = Settings;
        /** @memberOf SettingService **/
        service.maxUploadFileSize = maxUploadFileSize;
        /** @memberOf SettingService **/
        service.maxUploadFileSizeMB = maxUploadFileSizeMB;

        var _settings = {};

        function maxUploadFileSize() {
            return maxUploadFileSizeMB() * 1024 * 1024;
        }

        function maxUploadFileSizeMB() {
            var settingValue = getSettingValue(Settings.asAttachmentMaxFileSize, 0);
            return settingValue === 0 ? config.MAX_FILE_SIZE : settingValue;
        }

        function getSettingValue(settingId, defaultValue) {
            // user must be logged in to get setting value
            if (!UserService.CurrentUser) return defaultValue;
            return _settings[settingId] || defaultValue;
        }

        function isSettingAffirmative(settingId) {
            var settingValue = getSettingValue(settingId);
            settingValue = settingValue ? settingValue.toUpperCase() : "";
            return settingValue === "1"
                || settingValue === "-1"
                || settingValue === "YES"
                || settingValue === "ENABLED"
                || settingValue === "ACTIVATED"
                || settingValue === "ON";
        }

        function getServerSettingValue(settingId, defaultValue) {
            return settingsApi.getFromUri(settingId, "/value").then(function (res) {
                return res.data || defaultValue;
            });
        }

        var serverLocale = R.replace('-', '_', angular.element('html').data('locale'));

        function getPreferredLocale() {
            return storage.get('currentLocale', true) || serverLocale;
        }

        function saveSetting(settingId, value) {
            return settingsApi.post({SettingID: settingId, SettingValue: value}).then(function () {
                _settings[settingId] = value;
            });
        }
    }
})(window.angular);
(function (angular) {
    'use strict';

    angular
        .module('CalcheeseAdmin')
        .factory('storage',
        [
            '$rootScope', '$localStorage', 'UserService', function($rootScope, $localStorage, UserService) {

                // Days to expired
                var expiredDuration = 30;
                // Check different of 2 dates by days
                var daysBetween = function(first, second) {
                    // Copy date parts of the timestamps, discarding the time parts.
                    var one = new Date(first.getFullYear(), first.getMonth(), first.getDate());
                    var two = new Date(second.getFullYear(), second.getMonth(), second.getDate());
                    // Do the math.
                    var millisecondsPerDay = 1000 * 60 * 60 * 24;
                    var millisBetween = two.getTime() - one.getTime();
                    var days = millisBetween / millisecondsPerDay;
                    // Round down.
                    return Math.floor(days);
                };
                // Store key
                var store = function(key, value, notExpired) {
                    if (notExpired) {
                        $localStorage[key] = value;
                    } else {
                        $localStorage[key] = {};
                        $localStorage[key].date = new Date();
                        $localStorage[key].value = value;
                        $localStorage[key].user = UserService.CurrentUser
                            ? UserService.CurrentUser.NTUserNameFull
                            : null;
                    }
                };
                // Get key
                var get = function(key, notExpired) {
                    if ($localStorage[key]) {
                        // Store key with expired time?
                        return notExpired
                            ? $localStorage[key]
                            : $localStorage[key].date != null &&
                            $localStorage[key].date != undefined &&
                            $localStorage[key].user != null
                            ? (daysBetween(new Date($localStorage[key].date), new Date()) <= expiredDuration) &&
                            $localStorage[key].user == UserService.CurrentUser.NTUserNameFull
                            ? $localStorage[key].value
                            : null
                            : null;

                    } else {
                        return null;
                    }
                };

                return {
                    get: function(key, notExpired) {
                        return get(key, notExpired);
                    },
                    set: function(key, value, notExpired) {
                        store(key, value, notExpired);
                    },
                    add: function(key, field, value, notExpired) {
                        var objValue = get(key, notExpired);
                        if (objValue) {
                            objValue[field] = value;
                            store(key, objValue, notExpired);
                        }
                    },
                    remove: function(key) {
                        delete $localStorage[key];
                    },
                    reset: function() {
                        $localStorage.$reset();
                    }
                }

            }
        ]);

})(window.angular);
(function () {
    'use strict';

    angular
        .module("CalcheeseAdmin")
        .service("UploadService", UploadService);

    UploadService.$inject = ["$timeout", "webBaseUrl", "apiBaseUrl", "SettingService", "config", "toastr", "R"];

    function UploadService($timeout, webBaseUrl, apiBaseUrl, SettingService, config, toastr, R) {
        // Variables
        var service = this;

        // Exposed functions
        /** @memberOf UploadService **/
        service.options = {
            fileReaderSupported: false,
            uploadAction: "upload",
            supportFileTypes: []
        };

        /** @memberOf UploadService **/
        service.api = null;

        /** @memberOf UploadService **/
        service.uploadedCounter = 0;

        /** @memberOf UploadService **/
        service.onFileSelect = onFileSelect;

        /** @memberOf UploadService **/
        service.start = start;

        /** @memberOf UploadService **/
        service.uploadOptionData = uploadOptionData;

        /** @memberOf UploadService **/
        service.uploadCompleted = function () { };

        /** @memberOf UploadService **/
        service.imageUrl = imageUrl;

        /** @memberOf UploadService **/
        service.catalogueImageUrl = catalogueImageUrl;

        /** @memberOf UploadService **/
        service.download = download;

        /** @memberOf UploadService **/
        service.downloadCatalogueImage = downloadCatalogueImage;

        //--- Private        
        function download(typeFolder, typeId, fileName) {
            var api = apiBaseUrl + '/attachment/' + typeId + "/" + typeFolder;
            if (fileName) api = api + '?FileName=' + fileName;

            window.open(api);
        }

        function downloadCatalogueImage(attachmentId) {
            download(config.UPLOAD_FOLDER.Catalogue, attachmentId);
        }

        function imageUrl(typeFolder, typeId, fileName) {
            if ((typeFolder == null) || (typeId == null) || (fileName == null))
                return null;

            return webBaseUrl + "/" + config.UPLOAD_FOLDER.BaseFolder + "/" + typeFolder + "/" + typeId + "/" + fileName;
        }

        function catalogueImageUrl(imageFileName) {
            if (!imageFileName) return "";
            if (imageFileName.toLowerCase().startsWith("http"))
                return imageFileName;
            var date = new Date().getTime();
            return webBaseUrl + "/" + config.UPLOAD_FOLDER.BaseFolder + "/" + imageFileName + "?" + date;
        }

        function uploadOptionData() {
            return {}
        }

        function reset(model) {
            service.uploadedCounter = 0;
            model.selectedFiles = [];
            model.progress = []; // Import properties to support progress display
            model.progressData = [];
            if (model.upload && model.upload.length > 0) {
                for (var i = 0; i < model.upload.length; i++) {
                    if (model.upload[i] != null) {
                        model.upload[i].abort();
                    }
                }
            }
            model.upload = [];
            model.uploadResult = [];
            model.uploadInvalid = true;
            model.dataUrls = []; // Use to read image data
        }

        function onFileSelect(files, model) {
            reset(model);
            model.selectedFiles = files;
            if (files && files.length) {
                // Validate file size & mine type
                if (isValidFiles(files)) {
                    model.uploadInvalid = false;
                    for (var i = 0; i < files.length; i++) {
                        readData(i, files[i], model);
                        model.progress[i] = -1;
                    }
                } else {
                    toastr.error("File too large or invalid file types", 'Error', { closeButton: true, timeOut: 0 });
                }
            }
        }

        function isValidFiles(files) {
            return service.api
                && R.all(isValidFile, files);
        }

        function isValidFile(file) {
            return file.size <= SettingService.maxUploadFileSize()
                && R.contains(file.type, service.options.supportFileTypes);
        }

        function start(model) {
            for (var i = 0; i < model.selectedFiles.length; i++) {
                startUpload(i, model);
            }
        }

        function startUpload(index, model) {
            if (service.api == null) {
                toastr.error("System Error! Cannot upload file", 'Error', { closeButton: true, timeOut: 0 });
            } else {
                model.upload[index] = service.api().upload(SettingService.maxUploadFileSize(), model.selectedFiles[index], service.options.uploadAction, service.uploadOptionData());
                model.upload[index].then(function (response) {
                    // file is uploaded successfully
                    model.progressData[index] = response.data;
                    // check upload progress
                    service.uploadedCounter++;
                    if (service.uploadedCounter === model.selectedFiles.length)
                        service.uploadCompleted();
                }, function (response) {
                    // file failed to upload
                    console.log(response);
                }, function (evt) {
                    // Math.min is to fix IE which reports 200% sometimes
                    model.progress[index] = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
                });
            }
        }

        // Read local file data
        function readData(i, file, model) {
            service.options.fileReaderSupported = window.FileReader != null && (window.FileAPI == null || FileAPI.html5 != false);
            if (service.options.fileReaderSupported && file.type.indexOf('image') > -1) {
                var fileReader = new FileReader();
                fileReader.readAsDataURL(file);
                var loadFile = function (fileReader, index) {
                    fileReader.onload = function (e) {
                        $timeout(function () {
                            model.dataUrls[index] = e.target.result;
                        });
                    };
                };
                loadFile(fileReader, i);
            }
        }
    }
})();

(function (angular) {
    'use strict';

    angular
        .module('CalcheeseAdmin')
        .provider('siteBaseUrl',
            function siteBaseUrlProvider() {

                var baseUrl = angular.element('html').data('sitebaseurl');

                return {
                    setBaseUrl: function(url) {
                        baseUrl = url;
                    },
                    $get: function() {
                        return baseUrl;
                    }
                };
            });

    angular
        .module('CalcheeseAdmin')
        .provider('webBaseUrl',
            function webBaseUrlProvider() {

                var baseUrl = angular.element('html').data('webbaseurl');

                return {
                    setBaseUrl: function(url) {
                        baseUrl = url;
                    },
                    $get: function() {
                        return baseUrl;
                    }
                };
            });

    angular
        .module('CalcheeseAdmin')
        .provider('apiBaseUrl',
            function apiBaseUrlProvider() {

                var baseUrl = angular.element('html').data('apibaseurl');

                return {
                    setBaseUrl: function(url) {
                        baseUrl = url;
                    },
                    getBaseUrl: function() {
                        return baseUrl;
                    },
                    $get: function() {
                        return baseUrl;
                    }
                };
            });

    angular
        .module('CalcheeseAdmin').service('url',
        [
            '$location', 'webBaseUrl', function ($location, webBaseUrl) {
                var _this = this;

                //reload browser cache every new deploy
                var instanceID = angular.element('html').data('instanceid') || (new Date().getMilliseconds());
                var mode = angular.element('html').data('mode') || 'production';

                _this.partial = partial;
                _this.directives = directives;
                _this.components = components;
                _this.files = files;
                _this.tmpl = function(path) {
                    return /^partial:/.test(path)
                        ? partial(path.substr('partial:'.length))
                        : tmpl(path);
                };

                function getCacheId() {
                    return mode === 'dev'
                        ? new Date().getMilliseconds()
                        : instanceID;
                }

                function partial(path) {
                    return webBaseUrl + '/Scripts/app/partials' + path + "?" + getCacheId();
                }

                function directives(path) {
                    return webBaseUrl + '/Scripts/app/directives' + path + "?" + getCacheId();
                }

                function tmpl(path) {
                    return webBaseUrl + '/Scripts/app/views' + path + "?" + getCacheId();
                }

                function components(path) {
                    return webBaseUrl + '/Scripts/app/components' + path + "?" + getCacheId();
                }

                //template files
                function files(path) {
                    return webBaseUrl + '/Content/files/' + path;
                }
            }
        ]);

})(window.angular);
(function (angular) {
    'use strict';

    angular
        .module('CalcheeseAdmin')
        .service("UserService", UserService);

    UserService.$inject = ["apis", "$rootScope", "Memoization"];

    function UserService(apis, $rootScope, Memoization) {
        var service = this;
        var userApi = apis.users;

        /** @memberOf UserService **/
        service.CurrentUser = null;
        /** @memberOf UserService **/
        service.getCurrentUser = getCurrentUser;
        /** @memberOf UserService **/
        service.changePassword = changePassword;
        /** @memberOf UserService **/
        service.getUserDetail = getUserDetail;
        /** @memberOf UserService **/
        service.updateCurrentUser = updateCurrentUser;
        /** @memberOf UserService **/
        service.deleteCurrentUser = deleteCurrentUser;
        /** @memberOf UserService **/
        service.searchUsers = searchUsers;
        /** @memberOf UserService **/
        service.getUserGameHistories = getUserGameHistories;
        /** @memberOf UserService **/
        service.loadUserActivityHistories = loadUserActivityHistories;
        /** @memberOf UserService **/
        service.getUserUsedCouponCodes = getUserUsedCouponCodes;

        var CURRENT_USER_CACHE_DURATION = 60000; // 1 minute
        var currentUserMemoized = Memoization.memoize(getCurrentUserInternal, CURRENT_USER_CACHE_DURATION);
        function getCurrentUserInternal() {
            return userApi.id('current').then(function (result) {
                return updateCurrentUser(result.data);
            });
        }

        function getCurrentUser() {
            return currentUserMemoized()();
        }

        function changePassword(userDetailModel) {
            return userApi.postToUri('/password', userDetailModel);
        }

        function populateUserFullName(user) {
            if (user) {
                user.FullName = (user.FirstName || '') + ' ' + (user.LastName || '');
            }
        }

        function getUserDetail(userId) {
            return userApi.id(userId).then(function(result) {
                return result.data;
            });
        }

        function updateCurrentUser(user) {
            populateUserFullName(user);
            $rootScope.CurrentUser = user;
            service.CurrentUser = user;
            return user;
        }

        function deleteCurrentUser() {
            delete $rootScope.CurrentUser;
            delete service.CurrentUser;
        }
        

        function searchUsers(active, query, findAll, permission, requisitionApprover) {
            var permissionId = permission ? permission : null;
            return userApi.all({params: {
                Keyword: query,
                Permission: permissionId,
                IsApprover: requisitionApprover,
                SearchMatchType: 'contains',
                Active: active,
                FindAll: findAll === true
            }}).then(function (result) {
                return result.data.ResultSet;
            });
        }

        function getUserGameHistories(userId) {
            return userApi.path(userId + '/games').then(function(res) {
                return res.data;
            });
        }

        function loadUserActivityHistories(userName) {
            return userApi.postToUri("/activity-histories", { UserName: userName })
                .then(function(res) {
                    return res.data;
                });
        }

        function getUserUsedCouponCodes(userId) {
            return userApi.postToUri("/used-coupon-codes", { UserId: userId })
                .then(function (res) {
                    return res.data;
                });
        }
    }
})(window.angular);

(function (angular) {
    'use strict';

    angular
        .module('CalcheeseAdmin')
        .service('DateUtils', DateUtils);

    DateUtils.$inject = ['R', 'SettingService', '$moment'];

    function DateUtils(R, SettingService, $moment) {
        var service = this;

        var DateFormats = {
            'ar_SA' : 'dd/MM/yy',
            'bg_BG' : 'dd.M.yyyy',
            'ca_ES' : 'dd/MM/yyyy',
            'zh_TW' : 'yyyy/M/d',
            'cs_CZ' : 'd.M.yyyy',
            'da_DK' : 'dd-MM-yyyy',
            'de_DE' : 'dd.MM.yyyy',
            'el_GR' : 'd/M/yyyy',
            'en_US' : 'M/d/yyyy',
            'fi_FI' : 'd.M.yyyy',
            'fr_FR' : 'dd/MM/yyyy',
            'he_IL' : 'dd/MM/yyyy',
            'hu_HU' : 'yyyy. MM. dd.',
            'is_IS' : 'd.M.yyyy',
            'it_IT' : 'dd/MM/yyyy',
            'ja_JP' : 'yyyy/MM/dd',
            'ko_KR' : 'yyyy-MM-dd',
            'nl_NL' : 'd-M-yyyy',
            'nb_NO' : 'dd.MM.yyyy',
            'pl_PL' : 'yyyy-MM-dd',
            'pt_BR' : 'd/M/yyyy',
            'ro_RO' : 'dd.MM.yyyy',
            'ru_RU' : 'dd.MM.yyyy',
            'hr_HR' : 'd.M.yyyy',
            'sk_SK' : 'd. M. yyyy',
            'sq_AL' : 'yyyy-MM-dd',
            'sv_SE' : 'yyyy-MM-dd',
            'th_TH' : 'd/M/yyyy',
            'tr_TR' : 'dd.MM.yyyy',
            'ur_PK' : 'dd/MM/yyyy',
            'id_ID' : 'dd/MM/yyyy',
            'uk_UA' : 'dd.MM.yyyy',
            'be_BY' : 'dd.MM.yyyy',
            'sl_SI' : 'd.M.yyyy',
            'et_EE' : 'd.MM.yyyy',
            'lv_LV' : 'yyyy.MM.dd.',
            'lt_LT' : 'yyyy.MM.dd',
            'fa_IR' : 'MM/dd/yyyy',
            'vi_VN' : 'dd/MM/yyyy',
            'hy_AM' : 'dd.MM.yyyy',
            'az_Latn-AZ' : 'dd.MM.yyyy',
            'eu_ES' : 'yyyy/MM/dd',
            'mk_MK' : 'dd.MM.yyyy',
            'af_ZA' : 'yyyy/MM/dd',
            'ka_GE' : 'dd.MM.yyyy',
            'fo_FO' : 'dd-MM-yyyy',
            'hi_IN' : 'dd-MM-yyyy',
            'ms_MY' : 'dd/MM/yyyy',
            'kk_KZ' : 'dd.MM.yyyy',
            'ky_KG' : 'dd.MM.yy',
            'sw_KE' : 'M/d/yyyy',
            'uz_Latn-UZ' : 'dd/MM yyyy',
            'tt_RU' : 'dd.MM.yyyy',
            'pa_IN' : 'dd-MM-yy',
            'gu_IN' : 'dd-MM-yy',
            'ta_IN' : 'dd-MM-yyyy',
            'te_IN' : 'dd-MM-yy',
            'kn_IN' : 'dd-MM-yy',
            'mr_IN' : 'dd-MM-yyyy',
            'sa_IN' : 'dd-MM-yyyy',
            'mn_MN' : 'yy.MM.dd',
            'gl_ES' : 'dd/MM/yy',
            'kok_IN' : 'dd-MM-yyyy',
            'syr_SY' : 'dd/MM/yyyy',
            'dv_MV' : 'dd/MM/yy',
            'ar_IQ' : 'dd/MM/yyyy',
            'zh_CN' : 'yyyy/M/d',
            'de_CH' : 'dd.MM.yyyy',
            'en_GB' : 'dd/MM/yyyy',
            'es_MX' : 'dd/MM/yyyy',
            'fr_BE' : 'd/MM/yyyy',
            'it_CH' : 'dd.MM.yyyy',
            'nl_BE' : 'd/MM/yyyy',
            'nn_NO' : 'dd.MM.yyyy',
            'pt_PT' : 'dd-MM-yyyy',
            'sr_Latn-CS' : 'd.M.yyyy',
            'sv_FI' : 'd.M.yyyy',
            'az_Cyrl-AZ' : 'dd.MM.yyyy',
            'ms_BN' : 'dd/MM/yyyy',
            'uz_Cyrl-UZ' : 'dd.MM.yyyy',
            'ar_EG' : 'dd/MM/yyyy',
            'zh_HK' : 'd/M/yyyy',
            'de_AT' : 'dd.MM.yyyy',
            'en_AU' : 'd/MM/yyyy',
            'es_ES' : 'dd/MM/yyyy',
            'fr_CA' : 'yyyy-MM-dd',
            'sr_Cyrl-CS' : 'd.M.yyyy',
            'ar_LY' : 'dd/MM/yyyy',
            'zh_SG' : 'd/M/yyyy',
            'de_LU' : 'dd.MM.yyyy',
            'en_CA' : 'dd/MM/yyyy',
            'es_GT' : 'dd/MM/yyyy',
            'fr_CH' : 'dd.MM.yyyy',
            'ar_DZ' : 'dd-MM-yyyy',
            'zh_MO' : 'd/M/yyyy',
            'de_LI' : 'dd.MM.yyyy',
            'en_NZ' : 'd/MM/yyyy',
            'es_CR' : 'dd/MM/yyyy',
            'fr_LU' : 'dd/MM/yyyy',
            'ar_MA' : 'dd-MM-yyyy',
            'en_IE' : 'dd/MM/yyyy',
            'es_PA' : 'MM/dd/yyyy',
            'fr_MC' : 'dd/MM/yyyy',
            'ar_TN' : 'dd-MM-yyyy',
            'en_ZA' : 'yyyy/MM/dd',
            'es_DO' : 'dd/MM/yyyy',
            'ar_OM' : 'dd/MM/yyyy',
            'en_JM' : 'dd/MM/yyyy',
            'es_VE' : 'dd/MM/yyyy',
            'ar_YE' : 'dd/MM/yyyy',
            'en_029' : 'MM/dd/yyyy',
            'es_CO' : 'dd/MM/yyyy',
            'ar_SY' : 'dd/MM/yyyy',
            'en_BZ' : 'dd/MM/yyyy',
            'es_PE' : 'dd/MM/yyyy',
            'ar_JO' : 'dd/MM/yyyy',
            'en_TT' : 'dd/MM/yyyy',
            'es_AR' : 'dd/MM/yyyy',
            'ar_LB' : 'dd/MM/yyyy',
            'en_ZW' : 'M/d/yyyy',
            'es_EC' : 'dd/MM/yyyy',
            'ar_KW' : 'dd/MM/yyyy',
            'en_PH' : 'M/d/yyyy',
            'es_CL' : 'dd-MM-yyyy',
            'ar_AE' : 'dd/MM/yyyy',
            'es_UY' : 'dd/MM/yyyy',
            'ar_BH' : 'dd/MM/yyyy',
            'es_PY' : 'dd/MM/yyyy',
            'ar_QA' : 'dd/MM/yyyy',
            'es_BO' : 'dd/MM/yyyy',
            'es_SV' : 'dd/MM/yyyy',
            'es_HN' : 'dd/MM/yyyy',
            'es_NI' : 'dd/MM/yyyy',
            'es_PR' : 'dd/MM/yyyy',
            'am_ET' : 'd/M/yyyy',
            'tzm_Latn-DZ' : 'dd-MM-yyyy',
            'iu_Latn-CA' : 'd/MM/yyyy',
            'sma_NO' : 'dd.MM.yyyy',
            'mn_Mong-CN' : 'yyyy/M/d',
            'gd_GB' : 'dd/MM/yyyy',
            'en_MY' : 'd/M/yyyy',
            'prs_AF' : 'dd/MM/yy',
            'bn_BD' : 'dd-MM-yy',
            'wo_SN' : 'dd/MM/yyyy',
            'rw_RW' : 'M/d/yyyy',
            'qut_GT' : 'dd/MM/yyyy',
            'sah_RU' : 'MM.dd.yyyy',
            'gsw_FR' : 'dd/MM/yyyy',
            'co_FR' : 'dd/MM/yyyy',
            'oc_FR' : 'dd/MM/yyyy',
            'mi_NZ' : 'dd/MM/yyyy',
            'ga_IE' : 'dd/MM/yyyy',
            'se_SE' : 'yyyy-MM-dd',
            'br_FR' : 'dd/MM/yyyy',
            'smn_FI' : 'd.M.yyyy',
            'moh_CA' : 'M/d/yyyy',
            'arn_CL' : 'dd-MM-yyyy',
            'ii_CN' : 'yyyy/M/d',
            'dsb_DE' : 'd. M. yyyy',
            'ig_NG' : 'd/M/yyyy',
            'kl_GL' : 'dd-MM-yyyy',
            'lb_LU' : 'dd/MM/yyyy',
            'ba_RU' : 'dd.MM.yy',
            'nso_ZA' : 'yyyy/MM/dd',
            'quz_BO' : 'dd/MM/yyyy',
            'yo_NG' : 'd/M/yyyy',
            'ha_Latn-NG' : 'd/M/yyyy',
            'fil_PH' : 'M/d/yyyy',
            'ps_AF' : 'dd/MM/yy',
            'fy_NL' : 'd-M-yyyy',
            'ne_NP' : 'M/d/yyyy',
            'se_NO' : 'dd.MM.yyyy',
            'iu_Cans-CA' : 'd/M/yyyy',
            'sr_Latn-RS' : 'd.M.yyyy',
            'si_LK' : 'yyyy-MM-dd',
            'sr_Cyrl-RS' : 'd.M.yyyy',
            'lo_LA' : 'dd/MM/yyyy',
            'km_KH' : 'yyyy-MM-dd',
            'cy_GB' : 'dd/MM/yyyy',
            'bo_CN' : 'yyyy/M/d',
            'sms_FI' : 'd.M.yyyy',
            'as_IN' : 'dd-MM-yyyy',
            'ml_IN' : 'dd-MM-yy',
            'en_IN' : 'dd-MM-yyyy',
            'or_IN' : 'dd-MM-yy',
            'bn_IN' : 'dd-MM-yy',
            'tk_TM' : 'dd.MM.yy',
            'bs_Latn-BA' : 'd.M.yyyy',
            'mt_MT' : 'dd/MM/yyyy',
            'sr_Cyrl-ME' : 'd.M.yyyy',
            'se_FI' : 'd.M.yyyy',
            'zu_ZA' : 'yyyy/MM/dd',
            'xh_ZA' : 'yyyy/MM/dd',
            'tn_ZA' : 'yyyy/MM/dd',
            'hsb_DE' : 'd. M. yyyy',
            'bs_Cyrl-BA' : 'd.M.yyyy',
            'tg_Cyrl-TJ' : 'dd.MM.yy',
            'sr_Latn-BA' : 'd.M.yyyy',
            'smj_NO' : 'dd.MM.yyyy',
            'rm_CH' : 'dd/MM/yyyy',
            'smj_SE' : 'yyyy-MM-dd',
            'quz_EC' : 'dd/MM/yyyy',
            'quz_PE' : 'dd/MM/yyyy',
            'hr_BA' : 'd.M.yyyy.',
            'sr_Latn-ME' : 'd.M.yyyy',
            'sma_SE' : 'yyyy-MM-dd',
            'en_SG' : 'd/M/yyyy',
            'ug_CN' : 'yyyy-M-d',
            'sr_Cyrl-BA' : 'd.M.yyyy',
            'es_US' : 'M/d/yyyy'
        };

        /** @memberOf DateUtils **/
        service.getDateFormat = getDateFormat;

        /** @memberOf DateUtils **/
        service.getDateTimeFormat = getDateTimeFormat;

        /** @memberOf DateUtils **/
        service.fromString = fromString;

        /** @memberOf DateUtils **/
        service.correctValues = R.curry(correctValues);

        /** @memberOf DateUtils **/
        service.isSameOrAfter = isSameOrAfter;

        function getDateFormat() {
            return DateFormats[SettingService.getPreferredLocale()];
        }

        function getDateTimeFormat() {
            return getDateFormat() + ' H:mm';
        }

        function fromString(value) {
            return value ? new Date(value) : null;
        }

        function correctValues(props, obj) {
            return R.reduce(function (agg, prop) {
                return R.assoc(prop, fromString(R.prop(prop, agg)), agg);
            }, obj, props);
        }

        /* check whether dateTo is same or after dateFrom*/
        function isSameOrAfter(dateFrom, dateTo) {
            var fromDate = $moment(dateFrom);
            var toDate = $moment(dateTo);
            return fromDate.isValid() && toDate.isValid() && (toDate.isSameOrAfter(fromDate));
        }
    }
})(window.angular);
;(function (R) {
    'use strict';

    /**
     * @sig propEqIgnoreCase :: String -> String -> Object -> Boolean
     */
    R.propEqIgnoreCase = R.curry(function (prop, val, obj) {
        if (typeof val === 'undefined') return false;
        var objVal = R.prop(prop, obj);
        if (!objVal) return false;
        return R.toUpper(R.prop(prop, obj)) === val.toUpperCase();
    });

    /**
     * @sig propNotEq :: String -> a -> Object -> Boolean
     */
    R.propNotEq = R.complement(R.propEq);

    /**
     * @sig propNotEq :: String -> a -> Object -> Boolean
     */
    R.propEqEx = R.curry(function (field, val, obj) {
        return val == obj[field];
    });

    /**
     * @sig padRight :: Char -> int -> String -> String
     */
    R.padRight = R.curry(function (maskChar, length, input) {
        var padLength = length - input.length;
        if (padLength < 0) padLength = 0;
        var padContent = R.repeat(maskChar, padLength);
        return R.join('', R.concat(R.take(length, input.split('')), padContent));
    });

    /**
     * Creates a 'lens' that mutates the object directly when set is called instead of returning a copy of the object
     * @sig lensPropMutator :: String -> Lens s a
     */
    R.lensPropMutator = R.curry(function (focus) {
        return R.lens(
            function (target) {
                return target[focus];
            },
            function (val, target) {
                target[focus] = val;
                return target;
            }
        );
    });

    /**
     * @sig log :: a -> b -> b
     */
    R.log = R.curry(function (msg, x) {
        console.log(msg, x);
        return x;
    });

    R.debounce = debounce;

    R.toNumber = toNumber;


    /**
     * lodash (Custom Build) <https://lodash.com/>
     * Build: `lodash modularize exports="npm" -o ./`
     * Copyright jQuery Foundation and other contributors <https://jquery.org/>
     * Released under MIT license <https://lodash.com/license>
     * Based on Underscore.js 1.8.3 <http://underscorejs.org/LICENSE>
     * Copyright Jeremy Ashkenas, DocumentCloud and Investigative Reporters & Editors
     */

    /** Used as the `TypeError` message for "Functions" methods. */
    var FUNC_ERROR_TEXT = 'Expected a function';

    /** Used as references for various `Number` constants. */
    var NAN = 0 / 0;

    /** `Object#toString` result references. */
    var symbolTag = '[object Symbol]';

    /** Used to match leading and trailing whitespace. */
    var reTrim = /^\s+|\s+$/g;

    /** Used to detect bad signed hexadecimal string values. */
    var reIsBadHex = /^[-+]0x[0-9a-f]+$/i;

    /** Used to detect binary string values. */
    var reIsBinary = /^0b[01]+$/i;

    /** Used to detect octal string values. */
    var reIsOctal = /^0o[0-7]+$/i;

    /** Built-in method references without a dependency on `root`. */
    var freeParseInt = parseInt;

    /** Detect free variable `global` from Node.js. */
    var freeGlobal = typeof global == 'object' && global && global.Object === Object && global;

    /** Detect free variable `self`. */
    var freeSelf = typeof self == 'object' && self && self.Object === Object && self;

    /** Used as a reference to the global object. */
    var root = freeGlobal || freeSelf || Function('return this')();

    /** Used for built-in method references. */
    var objectProto = Object.prototype;

    /**
     * Used to resolve the
     * [`toStringTag`](http://ecma-international.org/ecma-262/7.0/#sec-object.prototype.tostring)
     * of values.
     */
    var objectToString = objectProto.toString;

    /* Built-in method references for those with the same name as other `lodash` methods. */
    var nativeMax = Math.max,
        nativeMin = Math.min;

    /**
     * Gets the timestamp of the number of milliseconds that have elapsed since
     * the Unix epoch (1 January 1970 00:00:00 UTC).
     *
     * @static
     * @memberOf _
     * @since 2.4.0
     * @category Date
     * @returns {number} Returns the timestamp.
     * @example
     *
     * _.defer(function(stamp) {
     *   console.log(_.now() - stamp);
     * }, _.now());
     * // => Logs the number of milliseconds it took for the deferred invocation.
     */
    var now = function() {
        return root.Date.now();
    };

    /**
     * Creates a debounced function that delays invoking `func` until after `wait`
     * milliseconds have elapsed since the last time the debounced function was
     * invoked. The debounced function comes with a `cancel` method to cancel
     * delayed `func` invocations and a `flush` method to immediately invoke them.
     * Provide `options` to indicate whether `func` should be invoked on the
     * leading and/or trailing edge of the `wait` timeout. The `func` is invoked
     * with the last arguments provided to the debounced function. Subsequent
     * calls to the debounced function return the result of the last `func`
     * invocation.
     *
     * **Note:** If `leading` and `trailing` options are `true`, `func` is
     * invoked on the trailing edge of the timeout only if the debounced function
     * is invoked more than once during the `wait` timeout.
     *
     * If `wait` is `0` and `leading` is `false`, `func` invocation is deferred
     * until to the next tick, similar to `setTimeout` with a timeout of `0`.
     *
     * See [David Corbacho's article](https://css-tricks.com/debouncing-throttling-explained-examples/)
     * for details over the differences between `_.debounce` and `_.throttle`.
     *
     * @static
     * @memberOf _
     * @since 0.1.0
     * @category Function
     * @param {Function} func The function to debounce.
     * @param {number} [wait=0] The number of milliseconds to delay.
     * @param {Object} [options={}] The options object.
     * @param {boolean} [options.leading=false]
     *  Specify invoking on the leading edge of the timeout.
     * @param {number} [options.maxWait]
     *  The maximum time `func` is allowed to be delayed before it's invoked.
     * @param {boolean} [options.trailing=true]
     *  Specify invoking on the trailing edge of the timeout.
     * @returns {Function} Returns the new debounced function.
     * @example
     *
     * // Avoid costly calculations while the window size is in flux.
     * jQuery(window).on('resize', _.debounce(calculateLayout, 150));
     *
     * // Invoke `sendMail` when clicked, debouncing subsequent calls.
     * jQuery(element).on('click', _.debounce(sendMail, 300, {
 *   'leading': true,
 *   'trailing': false
 * }));
     *
     * // Ensure `batchLog` is invoked once after 1 second of debounced calls.
     * var debounced = _.debounce(batchLog, 250, { 'maxWait': 1000 });
     * var source = new EventSource('/stream');
     * jQuery(source).on('message', debounced);
     *
     * // Cancel the trailing debounced invocation.
     * jQuery(window).on('popstate', debounced.cancel);
     */
    function debounce(func, wait, options) {
        var lastArgs,
            lastThis,
            maxWait,
            result,
            timerId,
            lastCallTime,
            lastInvokeTime = 0,
            leading = false,
            maxing = false,
            trailing = true;

        if (typeof func != 'function') {
            throw new TypeError(FUNC_ERROR_TEXT);
        }
        wait = toNumber(wait) || 0;
        if (isObject(options)) {
            leading = !!options.leading;
            maxing = 'maxWait' in options;
            maxWait = maxing ? nativeMax(toNumber(options.maxWait) || 0, wait) : maxWait;
            trailing = 'trailing' in options ? !!options.trailing : trailing;
        }

        function invokeFunc(time) {
            var args = lastArgs,
                thisArg = lastThis;

            lastArgs = lastThis = undefined;
            lastInvokeTime = time;
            result = func.apply(thisArg, args);
            return result;
        }

        function leadingEdge(time) {
            // Reset any `maxWait` timer.
            lastInvokeTime = time;
            // Start the timer for the trailing edge.
            timerId = setTimeout(timerExpired, wait);
            // Invoke the leading edge.
            return leading ? invokeFunc(time) : result;
        }

        function remainingWait(time) {
            var timeSinceLastCall = time - lastCallTime,
                timeSinceLastInvoke = time - lastInvokeTime,
                result = wait - timeSinceLastCall;

            return maxing ? nativeMin(result, maxWait - timeSinceLastInvoke) : result;
        }

        function shouldInvoke(time) {
            var timeSinceLastCall = time - lastCallTime,
                timeSinceLastInvoke = time - lastInvokeTime;

            // Either this is the first call, activity has stopped and we're at the
            // trailing edge, the system time has gone backwards and we're treating
            // it as the trailing edge, or we've hit the `maxWait` limit.
            return (lastCallTime === undefined || (timeSinceLastCall >= wait) ||
            (timeSinceLastCall < 0) || (maxing && timeSinceLastInvoke >= maxWait));
        }

        function timerExpired() {
            var time = now();
            if (shouldInvoke(time)) {
                return trailingEdge(time);
            }
            // Restart the timer.
            timerId = setTimeout(timerExpired, remainingWait(time));
        }

        function trailingEdge(time) {
            timerId = undefined;

            // Only invoke if we have `lastArgs` which means `func` has been
            // debounced at least once.
            if (trailing && lastArgs) {
                return invokeFunc(time);
            }
            lastArgs = lastThis = undefined;
            return result;
        }

        function cancel() {
            if (timerId !== undefined) {
                clearTimeout(timerId);
            }
            lastInvokeTime = 0;
            lastArgs = lastCallTime = lastThis = timerId = undefined;
        }

        function flush() {
            return timerId === undefined ? result : trailingEdge(now());
        }

        function debounced() {
            var time = now(),
                isInvoking = shouldInvoke(time);

            lastArgs = arguments;
            lastThis = this;
            lastCallTime = time;

            if (isInvoking) {
                if (timerId === undefined) {
                    return leadingEdge(lastCallTime);
                }
                if (maxing) {
                    // Handle invocations in a tight loop.
                    timerId = setTimeout(timerExpired, wait);
                    return invokeFunc(lastCallTime);
                }
            }
            if (timerId === undefined) {
                timerId = setTimeout(timerExpired, wait);
            }
            return result;
        }
        debounced.cancel = cancel;
        debounced.flush = flush;
        return debounced;
    }

    /**
     * Checks if `value` is the
     * [language type](http://www.ecma-international.org/ecma-262/7.0/#sec-ecmascript-language-types)
     * of `Object`. (e.g. arrays, functions, objects, regexes, `new Number(0)`, and `new String('')`)
     *
     * @static
     * @memberOf _
     * @since 0.1.0
     * @category Lang
     * @param {*} value The value to check.
     * @returns {boolean} Returns `true` if `value` is an object, else `false`.
     * @example
     *
     * _.isObject({});
     * // => true
     *
     * _.isObject([1, 2, 3]);
     * // => true
     *
     * _.isObject(_.noop);
     * // => true
     *
     * _.isObject(null);
     * // => false
     */
    function isObject(value) {
        var type = typeof value;
        return !!value && (type == 'object' || type == 'function');
    }

    /**
     * Checks if `value` is object-like. A value is object-like if it's not `null`
     * and has a `typeof` result of "object".
     *
     * @static
     * @memberOf _
     * @since 4.0.0
     * @category Lang
     * @param {*} value The value to check.
     * @returns {boolean} Returns `true` if `value` is object-like, else `false`.
     * @example
     *
     * _.isObjectLike({});
     * // => true
     *
     * _.isObjectLike([1, 2, 3]);
     * // => true
     *
     * _.isObjectLike(_.noop);
     * // => false
     *
     * _.isObjectLike(null);
     * // => false
     */
    function isObjectLike(value) {
        return !!value && typeof value == 'object';
    }

    /**
     * Checks if `value` is classified as a `Symbol` primitive or object.
     *
     * @static
     * @memberOf _
     * @since 4.0.0
     * @category Lang
     * @param {*} value The value to check.
     * @returns {boolean} Returns `true` if `value` is a symbol, else `false`.
     * @example
     *
     * _.isSymbol(Symbol.iterator);
     * // => true
     *
     * _.isSymbol('abc');
     * // => false
     */
    function isSymbol(value) {
        return typeof value == 'symbol' ||
            (isObjectLike(value) && objectToString.call(value) == symbolTag);
    }

    /**
     * Converts `value` to a number.
     *
     * @static
     * @memberOf _
     * @since 4.0.0
     * @category Lang
     * @param {*} value The value to process.
     * @returns {number} Returns the number.
     * @example
     *
     * _.toNumber(3.2);
     * // => 3.2
     *
     * _.toNumber(Number.MIN_VALUE);
     * // => 5e-324
     *
     * _.toNumber(Infinity);
     * // => Infinity
     *
     * _.toNumber('3.2');
     * // => 3.2
     */
    function toNumber(value) {
        if (typeof value == 'number') {
            return value;
        }
        if (isSymbol(value)) {
            return NAN;
        }
        if (isObject(value)) {
            var other = typeof value.valueOf == 'function' ? value.valueOf() : value;
            value = isObject(other) ? (other + '') : other;
        }
        if (typeof value != 'string') {
            return value === 0 ? value : +value;
        }
        value = value.replace(reTrim, '');
        var isBinary = reIsBinary.test(value);
        return (isBinary || reIsOctal.test(value))
            ? freeParseInt(value.slice(2), isBinary ? 2 : 8)
            : (reIsBadHex.test(value) ? NAN : +value);
    }
})(window.R);
(function (angular) {
    'use strict';

    angular
        .module('CalcheeseAdmin')
        .service('Memoization', Memoization);

    Memoization.$inject = ['R', '$timeout'];

    function Memoization(R, $timeout) {
        var _this = this;

        var DEFAULT_TIMEOUT = 10000; // 10 seconds

        /**
         * Produces a timed memoized function to prevent memory leak.
         * The memoiser will be de-referenced after the timeout and will get a fresh instance for subsequent calls
         * @memberOf Memoization
         * */
        _this.memoize = memoize;

        function memoize(fn, timeout) {
            var memoized = null;

            return function () {
                if (!memoized) {
                    var p = $timeout(
                        function () {
                            memoized = null;
                            $timeout.cancel(p);
                        },
                        timeout || DEFAULT_TIMEOUT,
                        false
                    );
                    memoized = R.memoize(fn);
                }
                return memoized;
            };
        }
    }
})(window.angular);

(function (angular) {
    'use strict';

    angular
        .module('CalcheeseAdmin')
        .service('StringUtils', StringUtils);

    StringUtils.$inject = ['R'];

    function StringUtils(R) {
        var _this = this;

        /** @memberOf StringUtils **/
        _this.contains = R.curry(contains);

        /** @memberOf StringUtils **/
        _this.containsR = R.curry(function (second, first) {
            return contains(first, second);
        });

        /** @memberOf StringUtils **/
        _this.match = R.curry(match);

        /** @memberOf StringUtils **/
        _this.matchR = R.curry(function (second, first) {
            return match(first, second);
        });

        /** @memberOf StringUtils **/
        _this.isNullOrEmpty = isNullOrEmptyString;

        /** @memberOf StringUtils **/
        _this.isNotNullOrEmpty = R.complement(isNullOrEmptyString);

        function contains(first, second) {
            if (!first) return false;
            if (!second) return true;
            return first.toLowerCase().indexOf(second.toLowerCase()) >= 0;
        }

        function match(first, second) {
            if (!first) return false;
            if (!second) return true;
            var expr = second.toLowerCase().replace(/%/g, '.*');
            return first.toLowerCase().match(new RegExp(expr));
        }

        function isNullOrEmptyString (s) {
            return R.isNil(s) || R.trim(s) === '';
        }
    }
})(window.angular);
(function(angular) {
    'use strict';

    var app = angular
        .module('CalcheeseAdmin');

    app.filter('localDate', ['$filter', 'DateUtils', localDate]);

    app.filter('localDateTime', ['$filter', 'DateUtils', localDateTime]);

    function localDate($filter, DateUtils) {
        return function(input) {
            return formatDateTime(input, DateUtils.getDateFormat(), $filter);
        };
    }

    function localDateTime($filter, DateUtils) {
        return function(input) {
            var result = formatDateTime(input, DateUtils.getDateTimeFormat(), $filter);
            return result ? result.replace(" 00:00:00", "") : undefined;
        };
    }

    function formatDateTime(input, format, filter) {
        if (input == null || input == undefined) {
            return undefined;
        }
        var value = filter('date')(new Date(input), format);
        return value.toUpperCase();
    }

    // Range filter to loop number
    app.filter('range',
        function() {
            return function(array, total) {
                total = parseInt(total);
                for (var i = 1; i <= total; i++)
                    array.push(i);
                return array;
            };
        });

    //page lower and upper

    app.filter('pageLower',
        function() {
            return function(result) {
                if (!result) return 0;
                return result.ResultCount == 0 ? 0 : Math.max((result.PageNumber - 1) * result.PageSize + 1, 1);
            };
        }).filter('pageUpper',
        function() {
            return function(result) {
                if (!result) return 0;
                return Math.min(result.PageNumber * result.PageSize, result.ResultCount);
            };
        });
    app.filter('call',
        function() {
            return function(f) {
                console.log(f);
                return f();
            };
        });
    app.filter('yesno',
        function() {
            return function(boolValue, defaultValue) {
                return boolValue === true ? 'Yes' : boolValue == false ? 'No' : defaultValue || '';
            };
        });
    app.filter('yesnoIcon',
        function() {
            return function(boolValue, defaultValue) {
                return boolValue === true ? 'fa-check' : boolValue == false ? 'fa-times' : defaultValue || '';
            };
        });

    app.filter('encode',
        function() {
            return function(input) {
                if (!input) return null;
                // Base64 encode / decode
                // http://www.webtoolkit.info/
                //
                var _keyStr = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
                var output = "";
                var chr1, chr2, chr3, enc1, enc2, enc3, enc4;
                var i = 0;

                //input = Base64._utf8_encode(input);

                while (i < input.length) {

                    chr1 = input.charCodeAt(i++);
                    chr2 = input.charCodeAt(i++);
                    chr3 = input.charCodeAt(i++);

                    enc1 = chr1 >> 2;
                    enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
                    enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
                    enc4 = chr3 & 63;

                    if (isNaN(chr2)) {
                        enc3 = enc4 = 64;
                    } else if (isNaN(chr3)) {
                        enc4 = 64;
                    }

                    output = output +
                        _keyStr.charAt(enc1) +
                        _keyStr.charAt(enc2) +
                        _keyStr.charAt(enc3) +
                        _keyStr.charAt(enc4);

                }

                return output;
            };
        });

    // turn an array of string into display comma separator string
    app.filter('arrayForDisplay',
        function() {
            return function(input) {
                if (input === undefined || input === null || input.length === 0) {
                    return '';
                }
                if (angular.isArray(input)) {
                    return input.join(', ');
                }
                return input;
            };
        });

    // format file size from bytes to KB, MB, GB
    app.filter('fileSize',
        function() {
            return function(input) {
                if (input === undefined || input === null || input.length === 0 || isNaN(input)) {
                    return '';
                }

                function getReadableFileSizeString(fileSizeInBytes) {
                    if (fileSizeInBytes === 0) return '0 bytes';
                    var i = -1;
                    var byteUnits = [' KB', ' MB', ' GB', ' TB', 'PB', 'EB', 'ZB', 'YB'];
                    do {
                        fileSizeInBytes = fileSizeInBytes / 1024;
                        i++;
                    } while (fileSizeInBytes > 1024);

                    return Math.max(fileSizeInBytes, 0.1).toFixed(1) + byteUnits[i];
                };

                return getReadableFileSizeString(parseInt(input, 10));
            };
        });

})(window.angular);
(function (angular) {
    'use strict';

    angular
        .module('CalcheeseAdmin')
        .controller('LoginCtrl', LoginCtrl);

    LoginCtrl.$inject = ['$scope', '$window', '$filter', 'config', 'webBaseUrl', 'apis', 'UserService'];

    function LoginCtrl($scope, $window, $filter, $constant, webBaseUrl, apis, UserService) {
        var ctrl = this;

        ctrl.focusOnUserName = true;
        ctrl.processing = true;
        //--- View actions
        ctrl.init = function () {
            // if current user is available, transit to dashboard
            // otherwise login is required
            UserService.getCurrentUser()
                .then(function () {
                    $scope.$state.go($constant.ROUTER.DASHBOARD, { reload: true });
                }).finally(function () {
                    ctrl.processing = false;
                });
        };

        // Validate input fields
        ctrl.canLogin = canLogin;
        ctrl.doLogin = doLogin;
        ctrl.windowsLogin = function () {
            $window.location.href = $scope.$stateParams.returnUrl || (webBaseUrl + '/dashboard');
        };

        function canLogin() {
            // Form model can not blank
            if (angular.isUndefined(ctrl.model)) return false;
            // Username can not be blank
            return ctrl.model.UserName && ctrl.model.Password;
        }

        // Form process
        function doLogin() {
            if (canLogin()) {
                ctrl.model.processing = true;
                apis.login.post(ctrl.model)
                    .then(function (result) {
                        var data = result.data;
                        if (data === 'ok') {
                            $scope.$emit('LoginSuccess');
                            $window.location.href = $scope.$stateParams.returnUrl || (webBaseUrl + '/dashboard');
                        } else {
                            ctrl.model.error = data;
                        }
                    })
                    .catch(function (error) {
                        ctrl.model.error = error;
                    })
                    .finally(function () {
                        ctrl.model.processing = false;
                    });
            } else {
                ctrl.model.error = 'Please Input Your Account And Password';
            }
        }
    }


    // Logout controller
    angular
        .module('CalcheeseAdmin')
        .controller('LogoutCtrl', LogoutCtrl);

    LogoutCtrl.$inject = ['$scope', '$timeout', '$state', 'config', 'apis', 'UserService'];

    function LogoutCtrl($scope, $timeout, $state, config, $apis, UserService) {
        // Call to dispose session

        $scope.init = function () {

            $apis.auth.postToUri('signout')
                .then(function () {
                    UserService.deleteCurrentUser();
                    $state.go(config.ROUTER.LOGIN, {}, { reload: true });
                });
        }
    }
})(window.angular);
(function (angular) {
    'use strict';

    angular
        .module('CalcheeseAdmin')
        .factory('BaseCtrlAs', ['$rootScope', '$filter', 'R', 'toastr', 'storage', 'config', 'url', 'apis', 'DateUtils', 'SettingService', 'UserService', BaseCtrlAs]);

    angular
        .module('CalcheeseAdmin')
        .factory('BaseCtrl', ['BaseCtrlAs', BaseCtrl]);

    function BaseCtrlAs($rootScope, $filter, R, toastr, storage, config, url, $apis, DateUtils, SettingService, UserService) {
        function getDefaultPageSize() {
            return config.PAGE_SIZE;
        }

        function resetLocalStorageIfVersionChange() {
            var appVersionStorageKey = "AppVersion";

            if (!storage.get(appVersionStorageKey)) {
                storage.reset();
                storage.set(appVersionStorageKey, config.VERSION);
                return;
            }

            var currentAppVersion = storage.get(appVersionStorageKey);
            if (config.VERSION !== currentAppVersion) {
                storage.reset();
                storage.set(appVersionStorageKey, config.VERSION);
            }
        }

        var Base = function ($scope, ctrl) {

            //Base properties and functions
            angular.extend(ctrl, {
                //do not override this, this is called on view ready <div class="page-list" ng-init="init()">
                init: function () {
                    ctrl.loading = true;
                    ctrl.resetLocalStorageIfVersionChange();

                    ctrl.request.params.SearchMatchType = config.DEFAULT_SEARCH_MATCH_TYPE;
                    ctrl.request.params.PageSize = getDefaultPageSize();

                    ctrl._overrideSearchParamsOnInit();
                    ctrl.momentDateFormat = DateUtils.getDateFormat().toUpperCase().replace(new RegExp('/','g'), '-');//momentjs only use format with dash (-)
                    ctrl.isAdvancedSearch = true;
                    if (ctrl._preGet()) {
                        ctrl.request.get().then(function () {
                            ctrl._init();
                            ctrl.loaded = true;
                        });
                    }
                },
                resetLocalStorageIfVersionChange: resetLocalStorageIfVersionChange,
                _overrideSearchParamsOnInit: angular.noop,
                _preGet: function () {
                    return true;
                },
                _extend: function (anotherScope) {
                    angular.extend(ctrl, anotherScope);
                },
                _go: function (state, params, options) {
                    return $rootScope.$state.go(state, params, options);
                },

                refresh: function() {
                    ctrl._go(ctrl._currentState, ctrl._params, {reload: true});
                },
                _currentState: $rootScope.$state.current.name,

                //$encode: $filter('encode'),
                //formatCurrency: $filter('currency'),
                 $apis: $apis,
                //used to inject templates
                url: url,
                //override for your logic
                _init: angular.noop,
                _useStorage: true, // To disabled local storage (for child listing)

                performAsync: function (action) {
                    ctrl.processing = true;
                    var doneProcessing = function (result) {
                        ctrl.processing = false;
                        return result;
                    };
                    var promise = action();
                    promise.then(doneProcessing, doneProcessing);
                    return promise;
                },

                rowNumOnPage: function (index, result) {
                    return result.PageSize
                        ? index + 1 + (result.PageNumber - 1)*result.PageSize
                        : index + 1;
                },

                //override if you use another lib
                _msg: function (message, type) {
                    if ('error' === type) {
                        console.error(message);
                        toastr.error(message, 'Error', {closeButton: true, timeOut: 0});
                    } else if (type === 'warning') {
                        toastr.warning(message, 'Warning');
                    } else {
                        toastr.success(message, 'Success');
                    }
                },
                _msgerr: function (message) {
                    ctrl._msg(message, 'error');
                },
                _alert: function (_title, _message, _type) {
                    swal({
                        title: _title,
                        type: _type,         // "warning", "error", "success" and "info"
                        html: _message
                    });
                },
                _alertWithAction: function (_title, _message, _type, action) {
                    swal({
                        title: _title,
                        type: _type,         // "warning", "error", "success" and "info"
                        html: _message
                    }).then(action);
                },
                _alertError: function (_message) {
                    ctrl._alert('Error', _message, "error");
                },
                _alertInfo: function (_title, _message) {
                    ctrl._alert(_title || "", _message, "info");
                },
                _confirm: function (message, action, html) {
                    swal({
                        title: "",
                        text: html ? null : message,
                        html: html ? message : '',
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Yes"
                    }).then(action);
                },
                _confirmSaveChanges: function (message, onConfirm, onCancel) {
                    swal({
                        title: "Are you sure?",
                        text: message ? message : 'You have un saved pending changes',
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        cancelButtonText: "Discard changes",
                        confirmButtonText: "Save changes"
                    }).then(onConfirm, onCancel);
                },
                _inputBox: function (title, message, allowsEmpty, onInputSuccessful) {
                    swal({
                        title: title,
                        text: message || 'Please type in the value',
                        input: 'text',
                        showCancelButton: true,
                        inputPlaceholder: 'Key in value here'
                    }).then(function (inputValue) {
                        if (inputValue === false) return false;
                        if (!allowsEmpty && (inputValue === "" || inputValue === undefined)) {
                            swal.showInputError('Input is required');
                            return false;
                        }
                        onInputSuccessful(inputValue);
                        return true;
                    });
                },

                //routes params
                _router: config.ROUTER,
                _params: $rootScope.$stateParams,
                //mode to capture after make request
                config: config,
                dateFormat: DateUtils.getDateFormat(),
                dateTimeFormat: DateUtils.getDateTimeFormat(),
                _mode: config.MODE,
                _currentMode: null,
                //current user
                CurrentUser: UserService.CurrentUser,
                //-- Common functions
                // Check changes of item
                isItemChanged: function (item, index) {
                    // Any special comparision
                    return !angular.equals(item, ctrl.OriginalResultSet[index]);
                },

                //override gor your function
                _api: function () {
                    throw "Please override _api(), and return API instance";
                },
                // Override search function for your controller,
                // user will input query string to serahc by configured fieldName
                // fieldName is fixed by html input(name=fieldName)
                // you can overide function of searching by field  _search_<fieldName>=function(query[,$scope]){}
                _search: function (query, fieldName) {
                    console.debug('Search for ', fieldName, ' with query', query);
                    var f = ctrl['_search_' + fieldName];

                    if (angular.isFunction(f)) {
                        return f.call(ctrl, query, ctrl);
                    }
                    //default use _api() to search
                    var params = {};
                    params[fieldName] = query;
                    return ctrl._api().get({params: params}).then(function (res) {
                        console.debug('Search', fieldName, 'with query', query, ' got result :', res);
                        return res.data.ResultSet;
                    });
                },

                /**
                 * Override to map result item to another structure.
                 * Default returns original item
                 */
                _resultMapper: function (item) {
                    return item;
                },
                _apiSuccessCallback: function (result) {
                    var data = result.data;
                    //result set response type
                    if (data.ResultSet) {
                        //remap to another structure if needing
                        angular.forEach(data.ResultSet, function (item, i) {
                            data.ResultSet[i] = ctrl._resultMapper(item);
                        });
                    }
                    ctrl.result = data;
                    ctrl.loading = false;
                    // clone result set to check changes
                    ctrl.OriginalResultSet = ctrl.result.ResultSet ? angular.copy(ctrl.result.ResultSet) : angular.copy(ctrl.result);
                    ctrl.isUnchanged = function (resultSet) {
                        return angular.equals(resultSet, ctrl.OriginalResultSet);
                    };

                    $scope.$emit('apiSuccess', data);
                    return data;
                },
                //override if you want another custom message
                onError: function (err) {
                    console.error('Error', err);
                    if (err.InnerExceptions !== undefined) {
                        var msg = "";
                        angular.forEach(err.InnerExceptions, function (e) {
                            msg += e.Message + " ([" + e.ExceptionType + ']:' + e.ExceptionMessage + ')' + "\n";
                            ctrl._msg(msg, 'error');
                        });
                    }
                    else {
                        ctrl._msgerr(err.Message);
                    }
                },
                _apiErrorCallback: function (err) {
                    if (!err) {
                        console.error('server no response', err);
                        return;
                    }
                    console.error('http request error', err);
                    //broadcast to do any stupid stuff you want :)
                    $scope.$emit('apiError', err);
                    ctrl.onError(err);
                    return err;
                },

                //sort dataset
                _sort: function (field, asc) {
                    var sort_asc = asc === undefined ? true : !asc;
                    ctrl.request.params.OrderBy = field;
                    ctrl.request.params.OrderAsc = sort_asc;
                    ctrl.request.get();
                },

                _handleRootScopeEvent: function (eventName, handler) {
                    $rootScope.$on(eventName, handler);
                },

                //do not override this
                request: {
                    //bind this with form model, see requisition/sidebar.html
                    params: {
                        PageSize: getDefaultPageSize(),
                        SearchMatchType: config.DEFAULT_SEARCH_MATCH_TYPE
                    },
                    // list of parameters that will be excluded when search filter is reset
                    paramsExcludedFromFilterReset: [],
                    get: function () {
                        ctrl.request.params.PageNumber = ctrl.request.params.PageNumber === 0 ? 1 : ctrl.request.params.PageNumber;
                        return ctrl._api()
                            .all({ params: ctrl.request.params })
                            .then(ctrl._apiSuccessCallback)
                            .catch(ctrl._apiErrorCallback);
                    },
                    id: function () {
                        if (!ctrl.request.params.id) {
                            throw "Invalid id in $scope.request.params.id";
                        }

                        ctrl._api()
                            .id(ctrl.request.params.id)
                            .then(ctrl._apiSuccessCallback)
                            .catch(ctrl._apiErrorCallback);
                    },
                    form: function () {
                        ctrl._api()
                            .form(ctrl.request.params)
                            .then(ctrl._apiSuccessCallback)
                            .catch(ctrl._apiErrorCallback);
                    }
                }
            });

            //use url request param as default if available
            angular.extend(ctrl.request.params, $rootScope.$stateParams);

            var resetDefaultParams = function () {
                //should reset by this because ng-model binding inner attributes, they are must reset too
                angular.forEach(ctrl.request.params, function (v, k) {
                    if (ctrl.request.paramsExcludedFromFilterReset.indexOf(k) > -1)
                        return;
                    ctrl.request.params[k] = (k.toLowerCase() === 'companyid')
                        ? ctrl.getDefaultCompany()
                        : null;
                });

                //default params
                angular.extend(ctrl.request.params, {
                    PageSize: getDefaultPageSize(),
                    PageNumber: 1,
                    SearchMatchType : config.DEFAULT_SEARCH_MATCH_TYPE
                }, $rootScope.$stateParams);

                //pagination bind to this attr
                ctrl.result = ctrl.result || {};
                ctrl.result.PageNumber = 1;
            };

            var doReloadData = function () {
                angular.extend(ctrl.request.params, {PageNumber: (ctrl.result || {PageNumber: 1}).PageNumber});
                ctrl._preGet();
                ctrl.request.get();
            };

            // Reset paging and reload data
            var doResetData = function () {
                resetDefaultParams();
                storage.remove(ctrl._currentState); //reset local storage
                ctrl._preGet();
                ctrl.request.get();
            };

            var defaultDataListener = {
                //this is fired on page change, see /partial/paging.html
                pageChanged: doReloadData,
                formSubmitted: doReloadData,
                formReset: doResetData,
                filterReset: doResetData
            };
            angular.forEach(defaultDataListener, function (v, k) {
                $scope.$on(k, v);
            });
        };

        return {
            extend: function (ctrl, $scope, $api) {
                Base($scope, ctrl);
                if (typeof $api == 'function') {
                    ctrl._api = $api;
                } else if (typeof $api == 'string') {
                    ctrl._api = function () {
                        return ctrl.$apis[$api];
                    };
                }
            }
        };
    }

    function BaseCtrl(BaseCtrlAs) {
        return {
            extend: function ($scope, $api) {
                BaseCtrlAs.extend($scope, $scope, $api);
            }
        };
    }
})(window.angular);

(function (angular) {
    'use strict';
    angular
        .module('CalcheeseAdmin')
        .factory('BaseDetailCtrlAs', ['R', 'BaseCtrlAs', BaseDetailCtrlAs]);

    angular
        .module('CalcheeseAdmin')
        .factory('BaseDetailCtrl', ['BaseDetailCtrlAs', BaseDetailCtrl]);

    function BaseDetailCtrlAs(R, BaseCtrlAs) {
        var Base = function (ctrl, $scope, $api) {
            BaseCtrlAs.extend(ctrl, $scope, $api);

            ctrl.model = {loading: true, processing: false, error: false};

            ctrl.init = function () {

                var ID_VALUE = ctrl._params[ctrl._api().idField] ||
                    ctrl._params.ID ||
                    ctrl._params.Id ||
                    ctrl._params.id ||
                        //Default fallback to first param
                    (function (obj) {
                        for (var id in obj) {
                            return id || '';
                        }
                        return '';
                    })(ctrl._params);
                ctrl._api()
                    .id(ID_VALUE, {queryString: ctrl.queryString || ''})
                    .then(function (result) {
                        var data = result.data;
                        // when cannot find data
                        if (data == 'null' || data == null) {
                            ctrl._alert('Item not found', 'Please check your request again');
                            ctrl._go(ctrl._router.DASHBOARD);
                            return;
                        }
                        ctrl.model = ctrl._resultMapper(data);
                        ctrl.model.loading = false;
                        ctrl.originalModel = angular.copy(ctrl.model);
                        ctrl.isUnchanged = function (model) {
                            return model._isNew !== true && angular.equals(model, ctrl.originalModel);
                        };
                        ctrl._init();
                    }, function (result) {
                        if (result.status < 500) ctrl._go(ctrl._router.DASHBOARD);
                    });
            };

            // Validation: use to disable save button
            ctrl.invalidForm = function () {
                return false;
            };

            // Validate required field to generate ErrorField and error message
            // @fieldName: field to check value
            // @errorMessage: default = "Required value"
            // @return: true/false
            ctrl.validateRequire = function (fieldName, errorMessage, model) {
                // Convention for Error field: FieldName + 'Error'
                var modelToValidate = model || ctrl.model;
                if (modelToValidate[fieldName] == null || modelToValidate[fieldName].toString().trim().length === 0) {
                    modelToValidate[fieldName + 'Error'] = errorMessage || 'Value is required';
                    return false;
                } else {
                    delete modelToValidate[fieldName + 'Error'];
                }
                return true;
            };

            ctrl.updateModel = function (newModel) {
                ctrl.model = ctrl._resultMapper(newModel);
                ctrl.originalModel = angular.copy(ctrl.model);
                ctrl.isUnchanged = function (model) {
                    return model._isNew !== true && angular.equals(model, ctrl.originalModel);
                };
            };

            ctrl.formatModelForSave = R.identity;

            // Save changes
            ctrl.save = function (displayToaster, refreshModel, modelFormatter) {
                displayToaster = typeof displayToaster !== 'undefined' ? displayToaster : true;
                refreshModel = typeof refreshModel !== 'undefined' ? refreshModel : true;
                modelFormatter = modelFormatter || ctrl.formatModelForSave;
                return ctrl.performAsync(function () {
                    return ctrl._api()
                        .post(modelFormatter(ctrl.model))
                        .then(function (result) {
                            // Update clone model
                            if (refreshModel) {
                                if (result) {
                                    ctrl.updateModel(result.data);
                                } else {
                                    ctrl.originalModel = angular.copy(ctrl.model);
                                }
                            }
                            if (displayToaster) {
                                ctrl._msg('Saved Successfully', 'success');
                            } else {
                                ctrl._message = 'Saved Successfully';
                            }
                            $scope.$emit('save', result.data);
                            return result.data;
                        });
                });
            };

            // override this in controllers
            ctrl.canDelete = function () {
                return false;
            };

            ctrl.canRefresh = function() {
                return true;
            };

            // Save and close
            ctrl.ok = function () {
                ctrl.backLink = ctrl.backLink || ctrl._router.DASHBOARD;
                if (ctrl.isUnchanged(ctrl.model) || ctrl.invalidForm()) {
                    ctrl._go(ctrl.backLink, ctrl.backParams);
                } else {
                    ctrl.save(true, false).then(function () {
                        ctrl._go(ctrl.backLink, ctrl.backParams);
                    });
                }
            };
            
            // refresh the form
            ctrl.refresh = function() {
                ctrl.init();
            };
        };
        return {
            extend: function (ctrl, $scope, $api) {
                Base(ctrl, $scope, $api);
            }
        };
    }

    function BaseDetailCtrl(BaseDetailCtrlAs) {
        return {
            extend: function ($scope, $api) {
                BaseDetailCtrlAs.extend($scope, $scope, $api);
            }
        };
    }
})(window.angular);

(function (angular) {
    'use strict';

    angular
        .module('CalcheeseAdmin')
        .factory('BaseAttachmentCtrlAs', ['BaseCtrlAs', 'apiBaseUrl', 'SettingService', '$timeout', 'R', BaseAttachmentCtrlAs]);

    angular
        .module('CalcheeseAdmin')
        .factory('BaseAttachmentCtrl', ['BaseAttachmentCtrlAs', BaseAttachmentCtrl]);
    
    angular
        .module('CalcheeseAdmin')
        .factory('BaseImportCtrlAs', ['BaseAttachmentCtrlAs', 'SettingService', BaseImportCtrlAs]);
    
    angular
        .module('CalcheeseAdmin')
        .factory('BaseImportCtrl', ['BaseImportCtrlAs', BaseImportCtrl]);

    function BaseAttachmentCtrlAs(BaseCtrlAs, apiBaseUrl, SettingService, $timeout, R) {
        var Base = function (ctrl, $scope, api) {
            BaseCtrlAs.extend(ctrl, $scope, api);

            ctrl.FileDescription = "";
            ctrl.onFileSelect = function (files) {
                if (files && files.length) {
                    if (files[0].size > SettingService.maxUploadFileSize()) {
                        ctrl._msgerr(ctrl.getLocalizedText("ClientMessage.FileIsTooLarge"));
                        files = null;
                    }
                    else {
                        ctrl.pendingFile = files[0];
                    }
                }
            };

            ctrl.uploadOptionData = function () {
                throw "Please override uploadOptionData";
            };

            ctrl.refreshRequested = false;
            ctrl.onUploaded = function () {
                ctrl.FileDescription = "";
                delete ctrl.pendingFile;

                // now signal angular to destroy the current file input element
                ctrl.refreshRequested = true;
                $scope.$applyAsync();

                $timeout(function () {
                    // once angular is done with its digest cycle, re-render the input element
                    ctrl.refreshRequested = false;
                }, 1, false);
            };

            ctrl.upload = function () {
                if (!ctrl.pendingFile) {
                    ctrl._msgerr('Select a file');
                    return;
                }
                ctrl._api()
                    .upload(SettingService.maxUploadFileSize(), ctrl.pendingFile, null, ctrl.uploadOptionData())
                    .then(function () {
                        // reload list
                        ctrl.init();
                        ctrl._msg('Uploaded');
                        ctrl.onUploaded();
                    });
            };

            ctrl.download = R.curry(function (attachmentType, attachmentId) {
                var api = apiBaseUrl + '/attachment/' + attachmentId + "/" + attachmentType;
                window.open(api);
            });

            ctrl.remove = function (attachmentId) {
                ctrl._confirm('You Are About To Remove Attachment', function () {
                    ctrl._api().del(attachmentId).then(function () {
                        ctrl.init();
                        ctrl._msg('Delete Successfully');
                    });
                });
            };
        };

        return {
            extend: function (ctrl, $scope, $api) {
                Base(ctrl, $scope, $api);
            }
        };
    }

    function BaseAttachmentCtrl(BaseAttachmentCtrlAs) {
        return {
            extend: function ($scope, $api) {
                BaseAttachmentCtrlAs.extend($scope, $scope, $api);
            }
        };
    }

    function BaseImportCtrlAs(BaseAttachmentCtrlAs, SettingService) {
        var Base = function(ctrl, $scope, api) {
            BaseAttachmentCtrlAs.extend(ctrl, $scope, api);

            // Abort all upload & reset variables
            ctrl.reset = function () {
                ctrl.selectedFiles = [];
                ctrl.progress = []; // Import properties to support progress display
                ctrl.progressData = [];
                if (ctrl.upload && ctrl.upload.length > 0) {
                    for (var i = 0; i < ctrl.upload.length; i++) {
                        if (ctrl.upload[i] != null) {
                            ctrl.upload[i].abort();
                        }
                    }
                }
                ctrl.upload = [];
                ctrl.uploadResult = [];
                ctrl.uploadInvalid = true;
                ctrl.dataUrls = []; // Use to read image data
            };

            ctrl.uploadAction = null; // import, upload, ...
            ctrl.supportFileTypes = function () {
                throw 'Please Override Support File Types'; // [ "", "", ... ]
            };

            // Override base attachment
            ctrl.onFileSelect = function (files) {
                ctrl.reset();
                ctrl.selectedFiles = files;

                if (files && files.length) {
                    // Validate file size & mine type
                    if (isValidFile(files)) {
                        ctrl.uploadInvalid = false;
                        for (var i = 0; i < files.length; i++) {
                            ctrl.progress[i] = -1;
                        }
                    } else {
                        ctrl._msgerr('File Too Large Or Invalid File Types');
                    }
                }
            };

            ctrl.import = function () {
                ctrl.processing = true;
                for (var i = 0; i < ctrl.selectedFiles.length; i++) {
                    startImport(i);
                }
            };

            ctrl.abortUpload = function (index) {
                ctrl.upload[index].abort();
            };

            ctrl.hasUploader = function (index) {
                return ctrl.upload[index] != null;
            };

            ctrl.onFileUploadSuccessfully = function(response) {
                ctrl.processing = false;
                //base class to override
            };
            
            ctrl.onFileUploadFailed = function (response) {
                //base class to override
            };

            // Private functions
            function isValidFile(files) {
                var isValid = true;
                angular.forEach(files, function (item) {
                    if ((item.size > SettingService.maxUploadFileSize()) || !isValidMineType(item.type))
                        isValid = false;
                });
                return isValid;
            }

            function isValidMineType(mineType) {
                if (mineType === "") return true;//client cannot detect the mime type so let server handle it
                var isValid = false;
                angular.forEach(ctrl.supportFileTypes(), function (item) {
                    if (mineType === item)
                        isValid = true;
                });

                return isValid;
            }

            function startImport(index) {
                ctrl.upload[index] = ctrl._api().upload(SettingService.maxUploadFileSize(), ctrl.selectedFiles[index], ctrl.uploadAction, ctrl.uploadOptionData());
                ctrl.upload[index].then(function (response) {
                    // file is uploaded successfully
                    ctrl.progressData[index] = response.data;
                    ctrl.onFileUploadSuccessfully(response);
                }, function(response) {
                    // file failed to upload
                    console.log(response);
                    ctrl.onFileUploadFailed(response);
                }, function(evt) {
                    // Math.min is to fix IE which reports 200% sometimes
                    ctrl.progress[index] = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
                });
            }

            // Read local file data
            function readData(file) {
                ctrl.fileReaderSupported = window.FileReader != null && (window.FileAPI == null || FileAPI.html5 != false);
                if (ctrl.fileReaderSupported && file.type.indexOf('image') > -1) {
                    var fileReader = new FileReader();
                    fileReader.readAsDataURL(file);
                    var loadFile = function(fileReader, index) {
                        fileReader.onload = function(e) {
                            $timeout(function() {
                                ctrl.dataUrls[index] = e.target.result;
                            });
                        };
                    }(fileReader, i);
                }
            }
        };

        return {
            extend: function (ctrl, $scope, $api) {
                Base(ctrl, $scope, $api);
            }
        };
    }
    
    function BaseImportCtrl(BaseImportCtrlAs) {
        
        return {
            extend: function ($scope, $api) {
                BaseImportCtrlAs.extend($scope, $scope, $api);
            }
        };
    }
})(window.angular);
(function (angular) {
    'use strict';
    angular
        .module('CalcheeseAdmin')
        .controller('CardReleaseCtrl', CardReleaseCtrl);

    CardReleaseCtrl.$inject = [ 'apis', 'PokeCardService'];

    function CardReleaseCtrl(apis, PokeCardService) {

        var ctrl = this;
        ctrl.init = function() {
            PokeCardService.allCardRelease().then(function(data) {
                ctrl.result = data;
            });
        }
    }

})(window.angular);
(function (angular) {
    'use strict';

    angular
        .module('CalcheeseAdmin')
        .controller('ChangePasswordModalCtrl', ChangePasswordModalCtrl);

    ChangePasswordModalCtrl.$inject = ['$scope', '$state', '$uibModal', '$uibModalInstance', 'BaseCtrlAs', 'config', 'UserService', 'UserCommand'];

    function ChangePasswordModalCtrl($scope, $state, $uibModal, $uibModalInstance, BaseCtrlAs, config, UserService, UserCommand) {

        var modalCtrl = this;

        modalCtrl.$onInit = function () {

            BaseCtrlAs.extend(modalCtrl, $scope, 'users');

            modalCtrl.processing = false;
            modalCtrl.model = {
                UserNameToChangePassword: UserCommand.UserName,
                ChangePasswordForUser: UserCommand.ChangePasswordForUser
            };
        };

        modalCtrl.doChangePassword = doChangePassword;

        function doChangePassword() {

            UserService.changePassword(modalCtrl.model).then(function (changePwdRes) {
                if (changePwdRes && changePwdRes.data && changePwdRes.data.success) {
                    var message = 'Mật khẩu của ' + modalCtrl.model.UserNameToChangePassword + ' đã được đổi, vui lòng đăng nhập lại.';
                    modalCtrl._alertWithAction('Success', message, 'success', logoutAfterChangePassword);

                } else {

                    modalCtrl._alert('Error', changePwdRes.data.error.message, 'error');
                }
            });
        }

        function logoutAfterChangePassword() {
            $state.go(config.ROUTER.DEFAULT, {}, { reload: true });
        }

        modalCtrl.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        }
    }

})(window.angular);

(function (angular) {
    'use strict';

    angular
        .module('CalcheeseAdmin')
        .controller('CouponCodeCtrl', CouponCodeCtrl);

    CouponCodeCtrl.$inject = ['$scope', 'BaseDetailCtrlAs', 'CouponCodeService', 'UploadService', 'apis', 'ReportService', 'R'];

    function CouponCodeCtrl($scope, BaseDetailCtrlAs, CouponCodeService, UploadService, apis, ReportService, R) {
        var ctrl = this;
        BaseDetailCtrlAs.extend(ctrl, $scope, 'couponcode');
        ctrl.backLink = ctrl._router.DASHBOARD;
        ctrl.isNewRecord = 'new';
        ctrl.supportFileTypes = ["application/vnd.ms-excel", "text/csv", "text/plain"];;
        ctrl.onFileSelect = onFileSelect;
        ctrl.uploadFile = uploadFile;
        ctrl.Pokecards = {};
        ctrl.currentCard = {};
        ctrl._init = function () {

            UploadService.api = function () {
                return apis.couponcode;
            }

            angular.extend(UploadService.options,
            {
                uploadAction: "upload",
                supportFileTypes: ctrl.supportFileTypes
                });

            UploadService.uploadCompleted = function () {
                fetchAllUploadInfo();
            };

            apis.pokecards.all({
                    params: {
                        SearchMatchType: 'contains',
                        FindAll: true
                    }
                })
                .then(function(result) {
                    ctrl.Pokecards = result.data.ResultSet;
                    ctrl.currentCard = ctrl.Pokecards[0];
                });

            fetchAllImportJobStatusInfo();

            fetchAllUploadInfo();
        };

        function fetchAllImportJobStatusInfo() {
            ReportService.getAllImportJobStatus().then(function(res) {
                ctrl.importJobStatus = res.data;
            });
        }

        function fetchAllUploadInfo() {
            CouponCodeService.getAllUploadInfo().then(function (res) {
                ctrl.allUploadInfo = res.data;
                var filePendingImport = function(fileInfo) {
                    return fileInfo.ImportStatus !== 'OK';
                }
                ctrl.totalPendingFileForProcessing = R.filter(filePendingImport, ctrl.allUploadInfo);
            });
        }

        function onFileSelect($files) {
            UploadService.onFileSelect($files, ctrl.model);
        }

        function uploadFile() {
            if (ctrl.currentCard.Id === "" || ctrl.currentCard.Id === undefined) return;
            // Options
            UploadService.uploadOptionData = function () {
                return {
                    PokeCardId: ctrl.currentCard.Id,
                    Description: ctrl.model.selectedFiles[0].name
                };
            }
            UploadService.start(ctrl.model);
        }

        ctrl.triggerImportAllData = function() {
            ctrl._confirm('Bạn có chắc muốn import tất cả dữ liệu bên dưới vào hệ thống?',
                function() {
                    CouponCodeService.triggerImportQueueMessage().then(function () {
                        ctrl._alert('Lệnh import đã ghi nhận, bạn có thể quay lại trang này để xem kết quả sau.');
                    });
                });
        }

        ctrl.canDeleteFile = function(fileInfo) {
            return fileInfo.ImportStatus !== 'OK' && (!ctrl.importJobStatus || ctrl.importJobStatus.length === 0);
        }

        ctrl.deleteFile = function(fileInfo) {
            CouponCodeService.deleteFile(fileInfo).then(function() {
                fetchAllUploadInfo();
            });
        }

        ctrl.close = function() {
            ctrl._go(ctrl._router.DASHBOARD);
        }
    }

})(window.angular);
(function (angular) {
    'use strict';

    angular
        .module('CalcheeseAdmin')
        .controller('WelcomeCtrl',
        [
            '$rootScope', '$scope', 'config', function ($rootScope, $scope, config) {
                $scope.goDashboard = function() {
                    $rootScope.$watch('_loggedIn',
                        function(newVal, oldVal) {
                            if (newVal === true) $rootScope.$state.go(config.ROUTER.DASHBOARD);
                        });
                };
            }
        ]);

    angular
        .module('CalcheeseAdmin')
        .controller('ErrorsCtrl',
        [
            '$rootScope', '$stateParams', 'config', function ($rootScope, $stateParams, config) {
                var ctrl = this;
                ctrl.hideErrorDetails = false;
                ctrl.init = function() {
                    // Redirect to dashboard if there is not errors
                    if ($stateParams.ErrorResponse) {
                        ctrl.response = $stateParams.ErrorResponse;
                        ctrl.router = config.ROUTER;
                    } else if ($stateParams.errorMsg) {
                        ctrl.response = {
                            data: {
                                ExceptionMessage: 'Generic error found',
                                Message: $stateParams.errorMsg
                            }
                        }
                    } else {
                        $rootScope.$state.go(config.ROUTER.DASHBOARD);
                    }
                };

                ctrl.toggleErrorDetails = function() {
                    ctrl.hideErrorDetails = !ctrl.hideErrorDetails;
                }
            }
        ]);

    angular
        .module('CalcheeseAdmin')
        .controller('DashboardCtrl', DashboardCtrl);

    DashboardCtrl.$inject = ['$scope', '$uibModal', 'url', 'BaseCtrlAs', 'apis'];

    function DashboardCtrl($scope, $uibModal, url, BaseCtrlAs, $apis) {
        var ctrl = this;

        BaseCtrlAs.extend(ctrl, $scope, 'users');

        if (ctrl.CurrentUser.UserPermission.IsSiteAdmin) {
            ctrl.meta = [
                [
                    { text: 'Danh sách người chơi', state: ctrl._router.USER_LIST },
                    //{ text: 'Danh sách người chơi đăng ký theo ngày', state: ctrl._router.REPORT_USER_REGISTRATION_BY_DATE },
                    //{ text: 'Tải lên mã code', state: ctrl._router.UPLOAD_COUPON },
                    { text: 'Lọc danh sách trúng thưởng', state: ctrl._router.PRIZE_LUCKY_DRAW },
                    { text: 'Thông số thống kê', state: ctrl._router.SITE_STATISTICS },
                    { text: 'Thống kê thẻ đã sử dụng', state: ctrl._router.CARD_RELEASE_DETAIL_LIST },
                    { text: 'Danh sách ứng viên tham gia lucky draw theo tuần', state: ctrl._router.LUCKY_DRAW_CANDIDATE_LIST_BY_WEEK },
                    { text: 'Thông tin chi tiết thẻ đã sử dụng', state: ctrl._router.USED_CODE_CARD_DETAIL_LIST },
                    { text: 'Người chơi có vào sân chơi Pokemon', state: ctrl._router.PAGE_VIEW_REPORT },
                    { text: 'Danh sách người đổi quà', state: ctrl._router.GIFT_REDEEM_REPORT }
                    //{ text: 'Danh sách người chơi Pokemon theo ngày', state: ctrl._router.REPORT_POKEMON_GAME_BY_DATE }
                ]
            ];
        }
        else if (ctrl.CurrentUser.UserPermission.CanUserAccessLuckyDraw) {
            ctrl.meta = [
                [
                    { text: 'Lọc danh sách trúng thưởng', state: ctrl._router.PRIZE_LUCKY_DRAW }
                ]
            ];
            //ctrl._go(ctrl._router.PRIZE_LUCKY_DRAW, {}, { reload: false });
        }

        ctrl.signOut = function() {
            $apis.auth.postToUri('signout')
                .then(function () {
                    window.location.href = '/';
                });
        }

        ctrl.changePassword = function() {
            $uibModal.open({
                templateUrl: url.tmpl('/users/change-password-modal.html'),
                controller: "ChangePasswordModalCtrl as modalCtrl",
                size: "md",
                resolve: {
                    UserCommand: {
                        UserName: ctrl.CurrentUser.UserName,
                        ChangePasswordForUser: false
                    }
                }
            });
        }
    }

})(window.angular);
(function (angular) {

    'use strict';

    angular
        .module('CalcheeseAdmin')
        .controller('GiftRedeemReportPageCtrl', GiftRedeemReportPageCtrl);

    GiftRedeemReportPageCtrl.$inject = ['$scope', 'BaseCtrlAs', 'apiBaseUrl'];

    function GiftRedeemReportPageCtrl($scope, BaseCtrlAs, apiBaseUrl) {

        var ctrl = this;
        BaseCtrlAs.extend(ctrl, $scope, 'report_gift_redeem_by_week');
        ctrl.request.params.WeekNumber = 1;

        ctrl.exportExcel = function() {
            var currentWeek = ctrl.request.params.WeekNumber;
            var exportUrlReq = apiBaseUrl + '/reports/export-gift-redeem-list-excel?WeekNumber=' + currentWeek;
            window.open(exportUrlReq);
        }
    }

})(window.angular);
(function (angular) {
    'use strict';

    angular
        .module('CalcheeseAdmin')
        .controller('GiftItemDetailCtrl', GiftItemDetailCtrl);

    GiftItemDetailCtrl.$inject = ['$scope', 'BaseDetailCtrlAs', 'GiftItemService', 'R', 'SettingService', 'UploadService'];

    function GiftItemDetailCtrl($scope, BaseDetailCtrlAs, GiftItemService, R) {
        var ctrl = this;
        BaseDetailCtrlAs.extend(ctrl, $scope, 'giftitems');
        ctrl.backLink = ctrl._router.GIFT_ITEM_LIST;
        ctrl.isNewRecord = ctrl._params.Id === 'new';


        //$scope.supportFileTypes = ["image/jpeg", "image/gif", "image/png"];
        //$scope.onFileSelect = onFileSelect;
        //$scope.uploadSignature = uploadSignature;
        //$scope.downloadSignature = downloadSignature;

        // Signature
        //function signatureUrl() {
        //    var encodedUserName = $scope._params.Id;
        //    var imagePath = UploadService.imageUrl($scope.config.UPLOAD_FOLDER.Signature, encodedUserName, encodedUserName + ".thumb");
        //    var date = new Date().getTime();
        //    return imagePath + "?" + date;
        //}

        //function onFileSelect($files) {
        //    UploadService.onFileSelect($files, $scope.model.upload);
        //}

        //function uploadSignature() {
        //    // Options
        //    UploadService.uploadOptionData = function () {
        //        return {
        //            EncodedNTUserNameFull: $scope._params.Id,
        //            Description: $scope.model.upload.selectedFiles[0].name
        //        }
        //    };
        //    UploadService.start($scope.model.upload);
        //}

        //function downloadSignature() {
        //    UploadService.download($scope.config.UPLOAD_FOLDER.Signature, $scope._params.Id, $scope.model.SignatureFileName);
        //}
    }

})(window.angular);
(function (angular) {
    'use strict';

    angular
        .module('CalcheeseAdmin')
        .controller('GiftItemSearchCtrl', GiftItemSearchCtrl);

    GiftItemSearchCtrl.$inject = ['$scope', 'apis', 'BaseCtrlAs', 'GiftItemService'];

    function GiftItemSearchCtrl($scope, apis, BaseCtrlAs, GiftItemService) {
        var ctrl = this;
        BaseCtrlAs.extend(ctrl, $scope, 'giftitems');
    }

})(window.angular);
(function (angular) {
    'use strict';

    angular
        .module('CalcheeseAdmin')
        .controller('LayoutCtrl', LayoutCtrl);

    LayoutCtrl.$inject = ['$rootScope', '$scope', '$state', 'config', 'UserService'];

    function LayoutCtrl($rootScope, $scope, $state, config, UserService) {
        $scope.dateTimeFormat = 'EEEE, d MMMM yyyy - h:mm:ss a';
        $scope._router = config.ROUTER;
        $scope.$state = $state;
        $scope.appVersion = config.VERSION;
        $scope.webUrl = $rootScope.webUrl;

        // Current user
        $scope.CurrentUser = UserService.CurrentUser;
        $scope.isLoginPage = function() {
            return $state.current.name === config.ROUTER.LOGIN;
        }
    }
})(window.angular);
(function (document, angular, $, Ps) {
    'use strict';
    angular
        .module('CalcheeseAdmin')
        .controller('LuckyDrawCtrl', LuckyDrawCtrl);
    LuckyDrawCtrl.$inject = ['$scope', '$timeout', '$uibModal', 'apis', 'BaseCtrlAs', 'PrizeLuckyDrawService', 'url', 'config', 'R'];

    function LuckyDrawCtrl($scope, $timeout, $uibModal, apis, BaseCtrlAs, PrizeLuckyDrawService, url, config, R) {
        var ctrl = this;
        BaseCtrlAs.extend(ctrl, $scope, 'prizeluckydraw');

        var baseInit = ctrl.init;

        ctrl.init = function () {
            PrizeLuckyDrawService.initCurrentWeek().then(function (data) {
                ctrl.PrizeData = data;
                if (ctrl.request.params.WeekNumber == undefined) ctrl.request.params.WeekNumber = data.CuurentWeekDefinition.WeekNumber;
                baseInit();
            });
        }
      
    }


})(window.document, window.angular, window.$, window.Ps);
(function (angular) {

    'use strict';

    //PageViewReportPageCtrl

    angular
        .module('CalcheeseAdmin')
        .controller('PageViewReportPageCtrl', PageViewReportPageCtrl);

    PageViewReportPageCtrl.$inject = ['$scope', 'ReportService', 'apiBaseUrl'];

    function PageViewReportPageCtrl($scope, ReportService, apiBaseUrl) {

        var ctrl = this;

        ctrl.allPageTitles = [
            '', 'san-choi', 'tham-gia-san-choi', 'choi-game'
        ];

        ctrl.init = function () {
            ctrl.pageTitle = '';
            ReportService.getPageViewReportSummaryByUser(ctrl.weekNumber, ctrl.pageTitle).then(function(data) {
                ctrl.result = data;
                ctrl.loaded = true;
            });
        }

        $scope.$on('formSubmitted',
            function() {
                ReportService.getPageViewReportSummaryByUser(ctrl.weekNumber, ctrl.pageTitle).then(function (data) {
                    ctrl.result = data;
                });
            });

        ctrl.exportExcel = function() {
            var exportUrlReq = apiBaseUrl + '/page-view-tracking/export-page-view-report-summary-by-username';
            window.open(exportUrlReq);
        }
    }

    //AllGamePageViewsPageCtrl

    angular
        .module('CalcheeseAdmin')
        .controller('AllGamePageViewsPageCtrl', AllGamePageViewsPageCtrl);

    AllGamePageViewsPageCtrl.$inject = ['$scope', 'ReportService'];

    function AllGamePageViewsPageCtrl($scope, ReportService) {

        var ctrl = this;
        ctrl.allAccessTypes = ['24-hours', '1-week', '1-month'];
        ctrl.init = function () {
            ctrl.lastAccessType = '1-week';
            ReportService.getAllGamePageViews(ctrl.lastAccessType).then(function (data) {
                ctrl.result = data;
                ctrl.loaded = true;
            });
        }

        $scope.$on('formSubmitted',
            function() {
                ReportService.getAllGamePageViews(ctrl.lastAccessType).then(function(data) {
                    ctrl.result = data;
                    ctrl.loaded = true;
                });
            });
    }

})(window.angular);
(function (angular) {
    'use strict';

    angular
        .module('CalcheeseAdmin')
        .controller('PokeCardDetailCtrl', PokeCardDetailCtrl);

    PokeCardDetailCtrl.$inject = ['$scope', 'BaseDetailCtrlAs', 'PokeCardService', 'R', 'SettingService'];

    function PokeCardDetailCtrl($scope, BaseDetailCtrlAs, PokeCardService, R) {
        var ctrl = this;
        BaseDetailCtrlAs.extend(ctrl, $scope, 'pokecards');
        ctrl.backLink = ctrl._router.POKE_CARD_LIST;
        ctrl.isNewRecord = ctrl._params.Id === 'new';
    }

})(window.angular);
(function (angular) {
    'use strict';

    angular
        .module('CalcheeseAdmin')
        .controller('PokeCardSearchCtrl', PokeCardSearchCtrl);

    PokeCardSearchCtrl.$inject = ['$scope', 'apis', 'BaseCtrlAs', 'PokeCardService'];

    function PokeCardSearchCtrl($scope, apis, BaseCtrlAs, PokeCardService) {
        var ctrl = this;
        BaseCtrlAs.extend(ctrl, $scope, 'pokecards');
    }

})(window.angular);
(function (document, angular, $, Ps) {
    'use strict';
    angular
        .module('CalcheeseAdmin')
        .controller('PrizeLuckyDrawCtrl', PrizeLuckyDrawCtrl);
    PrizeLuckyDrawCtrl.$inject = ['$scope', '$timeout', '$uibModal', 'apis', 'apiBaseUrl', 'BaseCtrlAs', 'PrizeLuckyDrawService', 'url', 'config', 'R'];

    function PrizeLuckyDrawCtrl($scope, $timeout, $uibModal, apis, apiBaseUrl, BaseCtrlAs, PrizeLuckyDrawService, url, config, R) {

        var ctrl = this;
        ctrl.initScrollUserTable = false;
        ctrl.initScrollWinnerTable = false;
        ctrl.drawCodeResult = {};
        ctrl.winnerResult = [];
        ctrl.request = {};
        ctrl.isInTestMode = config.SITE_MODE === 'test';
        ctrl.currentWeek = 1;

        BaseCtrlAs.extend(ctrl, $scope, 'prizeluckydraw');

        //ctrl.$onInit = function () {
        //    angular.element('#logo-admin').addClass('lucky-draw-logo-custom');
        //}

        ctrl.request.params = {
            PageNumber: 1,
            PageSize: 50,
            SearchMatchType: "",
            WeekNumber: undefined
        };

        ctrl.doLoadAllUserLuckyDraws = doLoadAllUserLuckyDraws;
        ctrl.getUserLuckyDraws = getUserLuckyDraws;

        ctrl._initData = function () {
            PrizeLuckyDrawService.initCurrentWeek().then(function (data) {
                ctrl.PrizeData = data;
                ctrl.request.params.WeekNumber = data.CuurentWeekDefinition.WeekNumber;
                ctrl.currentWeek = data.CuurentWeekDefinition.WeekNumber;
                getWinnerUserList();
            });
        }

        function getWinnerUserList() {
            PrizeLuckyDrawService.getWinnerUserList(ctrl.request.params.WeekNumber, 0).then(function(data) {
                ctrl.winnerResult = data;
                if (!ctrl.initScrollWinnerTable) {
                    $timeout(function () {
                        initScrollbarWinnerTable();
                    },
                        500);
                    ctrl.initScrollWinnerTable = true;
                }
            });
        }

        function initScrollbarUserTable() {
            var classScroll = document.getElementsByClassName('user-table-container');
            for (var i = 0; i < classScroll.length; i++) {
                Ps.initialize(classScroll[i],
                    {
                        wheelSpeed: 2,
                        wheelPropagation: true,
                        maxScrollbarLength: 110
                    });
            };
        }

        function initScrollbarWinnerTable() {
            var classScroll = document.getElementsByClassName('winner-table-container');
            for (var i = 0; i < classScroll.length; i++) {
                Ps.initialize(classScroll[i],
                    {
                        wheelSpeed: 2,
                        wheelPropagation: true,
                        maxScrollbarLength: 110
                    });
            };
        }

        function doLoadAllUserLuckyDraws() {
            ctrl.request.params.Keyword = '';
            ctrl.getUserLuckyDraws();
        }

        function getUserLuckyDraws() {
            apis.prizeluckydraw.all(ctrl.request).then(function (result) {
                ctrl.drawCodeResult = result.data;
                ctrl.winnerResult = [];
                if (!ctrl.initScrollUserTable) {
                    $timeout(function() {
                            initScrollbarUserTable();
                        },
                        500);
                    ctrl.initScrollUserTable = true;
                }
            });
        }

        ctrl.hasFirstPrize = function() {
            var prizeMatch = function(item) {
                return item.PriceType === 'TraiHe';
            }
            var matchItems = R.filter(prizeMatch, ctrl.winnerResult);
            return matchItems && matchItems.length > 0;
        }

        ctrl.hasSecondPrize=function() {
            var prizeMatch = function (item) {
                return item.PriceType === 'XeDien' || item.PriceType === 'XeDap' || item.PriceType === 'VinID';
            }
            var matchItems = R.filter(prizeMatch, ctrl.winnerResult);
            return matchItems && matchItems.length > 0;
        }

        ctrl.hasThirdPrize = function() {
            var prizeMatch = function (item) {
                return item.PriceType === 'DoChoi';
            }
            var matchItems = R.filter(prizeMatch, ctrl.winnerResult);
            return matchItems && matchItems.length >= 30;
        }

        ctrl.exportData = function () {
            var currentWeek = ctrl.request.params.WeekNumber;
            var exportType = (ctrl.winnerResult && ctrl.winnerResult.length > 0) ? 'winner' : 'candidate';
            var exportUrlReq = apiBaseUrl + '/PrizeLuckyDraw/export-result-excel?WeekNumber=' + currentWeek + '&Type=' + exportType;
            if (exportType === 'candidate') {
                ctrl.drawCodeResult = {};
                ctrl.initScrollUserTable = false;
            }
            window.open(exportUrlReq);
        }

        ctrl.openAddPrize = function(prize) {
            $uibModal.open({
                    templateUrl: url.tmpl('/modals/prize_modal-v2.html'),
                    controller: "AddPrizeCtrl as addPrizeCtrl",
                    size: "lg",
                    backdrop: true,
                    windowTopClass: "modal-quay-so-window",
                    resolve: {
                        WeekNumber: ctrl.request.params.WeekNumber,
                        Prize: prize
                    }
                })
                .result.then(function() {
                        ctrl.drawCodeResult = {};
                        ctrl.initScrollUserTable = false;
                        ctrl.initScrollWinnerTable = false;
                        getWinnerUserList();
                    },
                    function() {
                        ctrl.drawCodeResult = {};
                        ctrl.initScrollUserTable = false;
                        ctrl.initScrollWinnerTable = false;
                        getWinnerUserList();
                    });
        }

        ctrl.changedWeek = function () {
            ctrl.drawCodeResult = {};
            ctrl.initScrollUserTable = false;
            ctrl.initScrollWinnerTable = false;
            getWinnerUserList();
        }
        
        ctrl.pageChanged = function () {
            ctrl.request.params.PageNumber = ctrl.drawCodeResult.PageNumber;
            getUserLuckyDraws();
        }

        ctrl.signOut = function() {
            apis.auth.postToUri('signout')
                .then(function () {
                    window.location.href = '/';
                });
        }

        // update setting flag and confirm to show prize result to front site
        ctrl.showPrizeResultToFrontSite = function() {
            return apis.prizeluckydraw.postToUri('/set-show-prize-result',
                    { WeekNumber: ctrl.request.params.WeekNumber })
                .then(function() {
                    ctrl._msg('Kết quả quay số may mắn tuần ' + ctrl.request.params.WeekNumber + ' đã được cập nhật lên web site calcheese.vn');
                });
        }

        // confirm prize result
        ctrl.confirmPrize = function (prizeItem) {
            var confirmPrizeCommand = {
                WeekNumber: ctrl.request.params.WeekNumber,
                WinnerListUserId: prizeItem.Id,
                ConfirmPrize: true,
                UserId: prizeItem.UserId,
                PrizeType: prizeItem.PriceType,
                LuckyDrawCode: prizeItem.LuckyDrawCode
            };
            return apis.prizeluckydraw.postToUri('/confirm-prize', confirmPrizeCommand).then(function (res) {
                var name = prizeItem.FullName || prizeItem.UserName;
                ctrl._msg('Đã xác nhận ' + prizeItem.PriceDescription + ' của ' + name);
                getWinnerUserList();
            });
        }

        // reset prize result
        ctrl.resetPrize = function (prizeItem) {
            var resetPrizeCommand = {
                WeekNumber: ctrl.request.params.WeekNumber,
                WinnerListUserId: prizeItem.Id,
                ResetPrize: true,
                UserId: prizeItem.UserId,
                PrizeType: prizeItem.PriceType,
                LuckyDrawCode: prizeItem.LuckyDrawCode
            };
            return apis.prizeluckydraw.postToUri('/reset-prize', resetPrizeCommand).then(function (res) {
                var name = prizeItem.FullName || prizeItem.UserName;
                ctrl._msg('Đã hủy ' + prizeItem.PriceDescription + ' của ' + name);
                getWinnerUserList();
            });
        }

        ctrl.allPrizeResultConfirmed = function () {
            var prizeConfirmed = function (item) {
                return item.HasConfirmedPrize;
            }
            return ctrl.winnerResult && ctrl.winnerResult.length === 32 && R.all(prizeConfirmed)(ctrl.winnerResult);
        }

        ctrl.doSearch = function () {
            if (ctrl.filterText && (!ctrl.winnerResult || ctrl.winnerResult.length === 0)) {
                ctrl.initScrollUserTable = false;
                ctrl.request.params.Keyword = ctrl.filterText;
                ctrl.request.params.PageNumber = 1;
                ctrl.getUserLuckyDraws();
            }
        }

    }

    angular
        .module('CalcheeseAdmin')
        .controller('AddPrizeCtrl', AddPrizeCtrl);
    AddPrizeCtrl.$inject = ['$scope', '$uibModalInstance', 'apis', 'PrizeLuckyDrawService', 'WeekNumber', 'Prize', '$interval', '$timeout'];
    function AddPrizeCtrl($scope, $uibModalInstance, apis, PrizeLuckyDrawService, WeekNumber, Prize, $interval, $timeout) {

        var ctrl = this;
        ctrl.showDebug = false;
        ctrl.prize = Prize;
        ctrl.weekNumber = WeekNumber;
        ctrl.isDisableRandomButton = false;
        ctrl.Timer = null;
        ctrl.cancel = cancel;
        ctrl.Result = [];
        ctrl.startRandomNumber = startRandomNumber;
        ctrl.countSetPrize = 1;
        var totalThirdPrize = 30;
        ctrl.thirdPrizeCountLeft = totalThirdPrize;

        ctrl.$onInit = function() {
            if (ctrl.prize === 1) {
                ctrl.prizeDescrition = 'Giải Nhất';
                ctrl.prizeTitleText = 'Học Bổng Trại Hè Quốc Tế';
                ctrl.prizeImageUrl = 'Content/images/lucky-draw-tool/lucky-draw-gift-sample.png';
            } else if (ctrl.prize === 2) {
                ctrl.prizeDescrition = 'Giải Nhì';
                if (WeekNumber % 2 === 0) {
                    ctrl.prizeTitleText = 'Xe Điện Cân Bằng';
                    ctrl.prizeImageUrl = 'Content/images/lucky-draw-tool/giai-nhi-xe-dien.png';
                } else {
                    ctrl.prizeTitleText = 'Xe Đạp Thể Thao';
                    ctrl.prizeImageUrl = 'Content/images/lucky-draw-tool/giai-nhi-xe-dap.png';
                }
            } else if (ctrl.prize === 3) {
                ctrl.prizeDescrition = 'Giải Ba';
                ctrl.prizeTitleText = 'Bộ Đồ Chơi Pokemon';
                ctrl.prizeImageUrl = 'Content/images/lucky-draw-tool/giai-ba-bo-xep-hinh.png';
            }
        }

        ctrl.model = {};

        ctrl.init = function () {
            PrizeLuckyDrawService.getWinnerUserList(WeekNumber, Prize).then(function (data) {
                ctrl.Result = data;
                ctrl.thirdPrizeCountLeft = totalThirdPrize - ctrl.Result.length; //init onload and remain unchanged
                //console.log('total prize issued -> ', ctrl.Result.length);
                ctrl.isDisableRandomButton = getRandomButtonState(ctrl.prize, ctrl.Result);
            }).finally(function() {
                initScrollbarForResultList();
            });
        }

        function getRandomButtonState(prize, result) {
            if (prize === 3)
                return (result && result.length >= 30);
            return (result && result.length > 0);
        }

        function initScrollbarForResultList() {
            var classScroll = document.getElementsByClassName('lucky-draw-winner-user-list-container');
            if (ctrl.scrollbarInitialized) {
                for (var j = 0; j < classScroll.length; j++) {
                    var container = classScroll[j];
                    container.scrollTop = 0;
                    Ps.update(container);
                }
            }
            else {
                for (var i = 0; i < classScroll.length; i++) {
                    Ps.initialize(classScroll[i],
                    {
                        wheelSpeed: 2,
                        wheelPropagation: true,
                        maxScrollbarLength: 110
                    });
                }
                ctrl.scrollbarInitialized = true;
            }
        }

        function randomNumber() {
            return Math.round((Math.random() * 9));
        }

         function startRandomNumber() {
             ctrl.isDisableRandomButton = true;
             ctrl.Timer = $interval(function() {
                     ctrl.model.Number1 = randomNumber();
                     ctrl.model.Number2 = randomNumber();
                     ctrl.model.Number3 = randomNumber();
                     ctrl.model.Number4 = randomNumber();
                     ctrl.model.Number5 = randomNumber();
                     ctrl.model.Number6 = randomNumber();
                 },
                 100);
            $timeout(function () { setPrize(); }, 3000);
        }

        //Timer stop function.
        ctrl.StopTimer = function () {
            //Cancel the Timer.
            if (angular.isDefined(ctrl.Timer)) {
                $interval.cancel(ctrl.Timer);
            }
        }

        function setPrize() {
            PrizeLuckyDrawService.setPrize(WeekNumber, Prize).then(function(data) {
                    ctrl.StopTimer();
                    setLuckyDrawCode(data, data.LuckyDrawCode, Prize);
                    if (Prize === 3) {
                        ctrl.countSetPrize += 1;
                        if (ctrl.countSetPrize > ctrl.thirdPrizeCountLeft) return;

                        $timeout(function() { startRandomNumber() }, 2000);
                    }
                })
                .catch(function(fallback) {
                })
                .finally(function() {
                    initScrollbarForResultList();
                    if (Prize === 3) {
                        $timeout(function() { angular.element('#userRowId_0').addClass('flash animated'); }, 300);
                    } else {
                        $timeout(function () { angular.element('#userRowId_0').addClass('flash animated'); }, 4000);
                    }
                });
        }

        function setLuckyDrawCode(winData, luckyDrawCode, prize) {

            var str = String(luckyDrawCode);
            if (prize === 3) {
                ctrl.Result.unshift(winData);
                ctrl.model.Number1 = str[0];
                ctrl.model.Number2 = str[1];
                ctrl.model.Number3 = str[2];
                ctrl.model.Number4 = str[3];
                ctrl.model.Number5 = str[4];
                ctrl.model.Number6 = str[5];
            } else {

                ctrl.model.Number1 = str[0];

                var number2Timer = $interval(function() {
                        ctrl.model.Number2 = randomNumber();
                    },
                    100);

                var number3Timer = $interval(function() {
                        ctrl.model.Number3 = randomNumber();
                    },
                    100);

                var number4Timer = $interval(function() {
                        ctrl.model.Number4 = randomNumber();
                    },
                    100);

                var number5Timer = $interval(function() {
                        ctrl.model.Number5 = randomNumber();
                    },
                    100);

                var number6Timer = $interval(function() {
                        ctrl.model.Number6 = randomNumber();
                    },
                    100);

                $timeout(function() {
                        if (angular.isDefined(number2Timer))
                            $interval.cancel(number2Timer);
                        ctrl.model.Number2 = str[1];
                    },
                    500);

                $timeout(function() {
                        if (angular.isDefined(number3Timer))
                            $interval.cancel(number3Timer);
                        ctrl.model.Number3 = str[2];
                    },
                    1000);

                $timeout(function() {
                        if (angular.isDefined(number4Timer))
                            $interval.cancel(number4Timer);
                        ctrl.model.Number4 = str[3];
                    },
                    1500);

                $timeout(function() {
                        if (angular.isDefined(number5Timer))
                            $interval.cancel(number5Timer);
                        ctrl.model.Number5 = str[4];
                    },
                    2000);

                $timeout(function() {
                        if (angular.isDefined(number6Timer))
                            $interval.cancel(number6Timer);
                        ctrl.model.Number6 = str[5];
                    },
                    2500);

                $timeout(function() {
                     ctrl.Result.unshift(winData);
                }, 3000);
            }
        }

        function cancel() {
            $uibModalInstance.dismiss('cancel');
        }
    }

})(window.document, window.angular, window.$, window.Ps);

(function (angular) {
    'use strict';
    angular
        .module('CalcheeseAdmin')
        .controller('StatisticCtrl', StatisticCtrl);

    StatisticCtrl.$inject = ['apis', 'ReportService'];

    function StatisticCtrl(apis, ReportService) {

        var ctrl = this;
        ctrl.init = function () {
            ReportService.userCouponCodeStatistic().then(function (result) {
                ctrl.result = result.data;
            });
        }
    }

})(window.angular);
(function (angular) {
    'use strict';

    angular
        .module('CalcheeseAdmin')
        .controller('UserCodeReleaseCtrl', UserCodeReleaseCtrl);

    UserCodeReleaseCtrl.$inject = ['$scope', 'apis', 'BaseCtrlAs'];

    function UserCodeReleaseCtrl($scope, apis, BaseCtrlAs) {
        var ctrl = this;
        BaseCtrlAs.extend(ctrl, $scope, 'usedcouponcodereport');
    }

    angular
        .module('CalcheeseAdmin')
        .controller('UserCodeReleaseSummaryByDateCtrl', UserCodeReleaseSummaryByDateCtrl);

    UserCodeReleaseSummaryByDateCtrl.$inject = ['$scope', 'apiBaseUrl', 'BaseCtrlAs'];

    function UserCodeReleaseSummaryByDateCtrl($scope, apiBaseUrl, BaseCtrlAs) {
        var ctrl = this;
        BaseCtrlAs.extend(ctrl, $scope, 'usedcouponcodereport_summary_by_date');

        ctrl.exportExcel = function () {
            var exportUrlReq = apiBaseUrl + '/usedcouponcodereport/export-card-release-summary-by-date';
            window.open(exportUrlReq);
        }
    }

})(window.angular);
(function (angular) {
    'use strict';

    angular
        .module('CalcheeseAdmin')
        .controller('UserDetailCtrl', UserDetailCtrl);

    UserDetailCtrl.$inject = ['$scope', 'BaseDetailCtrlAs', 'UserService', 'R'];

    function UserDetailCtrl($scope, BaseDetailCtrlAs, UserService, R) {
        var ctrl = this;
        BaseDetailCtrlAs.extend(ctrl, $scope, 'users');

        ctrl.backLink = ctrl._router.USER_LIST;
        ctrl.isNewRecord = ctrl._params.Id === 'new';
        ctrl.binding = {
            focusUserName: ctrl.isNewRecord
        };

        ctrl.loadUserGameHistories = function () {
            UserService.getUserGameHistories(ctrl._params.Id).then(function (gameHistoryData) {
                ctrl.userGameHistories = gameHistoryData;
            });
        }

        ctrl.loadUserActivityHistories = function() {
            UserService.loadUserActivityHistories(ctrl.model.UserName).then(function (historyData) {
                ctrl.userActivityHistories = historyData;
            });
        }

        ctrl.getUserUsedCouponCodes = function() {
            UserService.getUserUsedCouponCodes(ctrl._params.Id).then(function (usedCodeData) {
                ctrl.userAllUsedCouponCodes = usedCodeData;
            });
        }

        ctrl.addRole = function () {
            if (ctrl.binding.currentRole && ctrl.binding.currentRole !== '' && ctrl.binding.currentRole.trim().length > 0) {
                if (R.findIndex(R.propEq('Name', ctrl.binding.currentRole))(ctrl.model.UserRoles) === -1) {
                    ctrl.model.UserRoles.push({ Name: ctrl.binding.currentRole });
                    ctrl.binding.currentRole = undefined;
                }
            }
        }

        ctrl.removeRole = function(item) {
            var removeIndex = R.findIndex(R.propEq('Name', item.Name))(ctrl.model.UserRoles);
            if (removeIndex === -1)
                return;
            ctrl.model.UserRoles = R.remove(removeIndex, 1, ctrl.model.UserRoles);
        }
    }

})(window.angular);
(function (angular) {
    'use strict';

    angular
        .module('CalcheeseAdmin')
        .controller('UserSearchCtrl', UserSearchCtrl);

    UserSearchCtrl.$inject = ['$scope', 'apis', 'config', '$uibModal', 'url', 'BaseCtrlAs'];

    function UserSearchCtrl($scope, apis, config, $uibModal, url, BaseCtrlAs) {
        var ctrl = this;
        BaseCtrlAs.extend(ctrl, $scope, 'users');

        ctrl.changePassword = function() {
            $uibModal.open({
                templateUrl: url.tmpl('/users/change-password-modal.html'),
                controller: "ChangePasswordModalCtrl as modalCtrl",
                size: "md",
                resolve: {
                    UserCommand: {
                        UserName: ctrl.CurrentUser.UserName,
                        ChangePasswordForUser: false
                    }
                }
            });
        }

        ctrl.userCanSetPassword = function() {
            return config.ALLOW_CHANGE_USER_PASSWORD && ctrl.CurrentUser.UserPermission.IsSiteAdmin;
        }

        ctrl.setUserPassword = function(userName) {
            $uibModal.open({
                templateUrl: url.tmpl('/users/change-password-modal.html'),
                controller: "ChangePasswordModalCtrl as modalCtrl",
                size: "md",
                resolve: {
                    UserCommand: {
                        UserName: userName,
                        ChangePasswordForUser: true
                    }
                }
            });
        }

        ctrl.exportLuckyDrawUser = function () {
            $uibModal.open({
                templateUrl: url.tmpl('/users/user-export-options.html'),
                controller: "ExportUserOptionModalCtrl as modalCtrl",
                size: "md"
            }).result.then(function(week) {
                // call for export service
            });
        }

    }

    // controller for export-user-options.html
    angular
        .module('CalcheeseAdmin')
        .controller('ExportUserOptionModalCtrl', ExportUserOptionModalCtrl);

    ExportUserOptionModalCtrl.$inject = ['$uibModalInstance'];

    function ExportUserOptionModalCtrl($uibModalInstance) {

        var modalCtrl = this;

        modalCtrl.doExport = function() {
            $uibModalInstance.close(modalCtrl.selectedWeekNumber);
        }

        modalCtrl.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        }
    }

})(window.angular);
(function(angular) {
    'use strict';

    var app = angular
        .module('CalcheeseAdmin');

    // Price format with color base on value
    app.directive('numPrice',
    [
        '$filter', function($filter) {
            return function(scope, element, attrs) {
                var data = attrs['numPrice'];
                var data_unit = attrs['numPriceUom'];
                var currSymbol = attrs["numPriceCurrency"];
                if (currSymbol) currSymbol += " ";
                // If number
                if (data && !isNaN(parseFloat(data)) && isFinite(data)) {
                    formatLabel(data, element);
                    var text = $filter('currency')(data, currSymbol, 2);
                    if (data_unit) text += ' ' + data_unit;
                    element.text(text); // fractionSize only available from 1.3.0
                } else {
                    element.hide();
                }
            };
        }
    ]);

    // Status format
    app.directive('labelColor',
        function() {
            return {
                restrict: 'A',
                scope: {
                    labelColor: '<'
                },
                link: function(scope, element, attrs) {
                    var val = scope.labelColor;
                    switch (val) {
                    case -3:
                    case "-3":
                    case "C":
                        break;
                    case 0:
                    case "0":
                    case "Saved":
                    case 'Not Yet Submitted':
                        element.addClass('label-default');
                        break;
                    case 1:
                    case 2:
                    case "1":
                    case "2":
                    case "Submitted":
                    case "Approval In Progress":
                        element.addClass('label-belizehole');
                        break;
                    case 3:
                    case "3":
                    case "Approved":
                        element.addClass('label-nephritis');
                        break;
                    case 4:
                    case 5:
                    case "4":
                    case "5":
                    case "H":
                    case "PO Created":
                    case "Purchase Order Created":
                    case "Ordered":
                    case "Received":
                    case 'Posted':
                        element.addClass('label-greensea');
                        break;
                    default:
                        break;
                    }
                }
            };
        });

    // Loading element in panel
    app.directive('comLoading',
    [
        '$timeout', function($timeout) {
            return {
                restrict: 'E',
                template:
                    '<div class="com-loading"><span class="com-loading-inner"><i class="fa fa-spinner fa-spin"></i> Loading</span></div>',
                link: function(scope, element, attrs) {
                    var timer = $timeout(function() {
                            element.children().css('display', 'block');
                        },
                        100); // delay for NOT display in reload

                    scope.$on('$destroy',
                        function() {
                            $timeout.cancel(timer);
                        });
                }
            }
        }
    ]);

    // iCheck directive
    // https://github.com/fronteed/iCheck/issues/62 by wajatimur
    app.directive('icheck',
    [
        '$timeout', function($timeout) {
            return {
                require: 'ngModel',
                link: function($scope, element, $attrs, ngModel) {
                    var elem;
                    var timer = $timeout(function() {
                        var value;
                        value = $attrs['value'];

                        $scope.$watch($attrs['ngModel'],
                            function(newValue) {
                                $(element).iCheck('update');
                            });

                        elem = $(element).iCheck({
                            checkboxClass: 'icheckbox_square-blue',
                            radioClass: 'icheckbox_square-blue'
                        });

                        elem.on('ifChanged',
                            function(event) {
                                if ($(element).attr('type') === 'checkbox' && $attrs['ngModel']) {
                                    $scope.$apply(function() {
                                        return ngModel.$setViewValue(event.target.checked);
                                    });
                                }
                                if ($(element).attr('type') === 'radio' && $attrs['ngModel']) {
                                    return $scope.$apply(function() {
                                        return ngModel.$setViewValue(value);
                                    });
                                }
                            });
                    });

                    $scope.$on('$destroy',
                        function() {
                            if (elem) elem.off();
                            $timeout.cancel(timer);
                        });
                }
            };
        }
    ]);

    // Focus field, used in modal dialog
    // http://stackoverflow.com/questions/14833326/how-to-set-focus-on-input-field by Mark Rajcok
    app.directive('focusMe',
    [
        '$timeout', '$parse', function($timeout, $parse) {
            return {
                //scope: true,   // optionally create a child scope
                link: function(scope, element, attrs) {
                    var model = $parse(attrs.focusMe);
                    scope.$watch(model,
                        function(value) {
                            if (value === true) {
                                $timeout(function() {
                                        element[0].focus();
                                    },
                                    100,
                                    false);
                            }
                        });
                    // to address @blesh's comment, set attribute value to 'false'
                    // on blur event:
                    element.bind('blur',
                        function() {
                            scope.$applyAsync(model.assign(scope, false));
                        });
                    scope.$on('$destroy',
                        function() {
                            element.off('blur');
                        });
                }
            };
        }
    ]);

    /**
     * Toggle class
     */
    app.directive('toggleClass', toggleClass);

    function toggleClass() {
        return {
            restrict: 'A',
            link: function(scope, element, attrs) {
                element.bind('click',
                    function() {
                        var targetObj = document.getElementById(attrs.toggleTarget);
                        var targetElement = angular.element(targetObj);
                        targetElement.toggleClass(attrs.toggleClass);
                    });
                scope.$on('$destroy',
                    function() {
                        element.off();
                    })
            }
        };
    }

    //limit input charaters of text field
    app.directive("limitTo",
    [
        function() {
            return {
                restrict: "A",
                require: "ngModel",
                link: function(scope, element, attrs, ngModelCtrl) {
                    var limit = parseInt(attrs.limitTo);
                    attrs.$set('maxlength', limit);
                    //paste value
                    var timer;
                    element.on("paste",
                        function() {
                            timer = setTimeout(function() {
                                    if (element.val().length > limit) {
                                        ngModelCtrl.$setViewValue(element.val().substring(0, limit));
                                        ngModelCtrl.$render();
                                    }
                                },
                                5);
                        });
                    scope.$on('$destroy',
                        function() {
                            clearTimeout(timer);
                            element.off();
                        })
                }
            }
        }
    ]);

    //truncate text and display tooltip 
    app.directive("truncateAt",
    [
        "$compile", function($compile) {
            return {
                restrict: "A",
                scope: {
                    text: "=truncateText"
                },
                link: function(scope, element, attrs) {
                    var limit = parseInt(attrs.truncateAt);
                    var limitText = scope.text;
                    if (scope.text.length > limit) {
                        limitText = limitText.substring(0, limit) + "...";
                        element.attr('uib-tooltip', scope.text);
                    }
                    element.removeAttr('truncate-at'); //remove the attribute to avoid indefinite loop
                    element.removeAttr('truncate-text');
                    element.removeAttr('ui-sref'); //for ui router link
                    element.text(limitText);
                    $compile(element)(scope);
                }
            }
        }
    ]);

    // resize image base on fixed width -> height and maintain w/h ratio
    app.directive("imageResize",
    [
        "$parse", function($parse) {
            return {
                link: function(scope, elm, attrs) {
                    return elm.one("load",
                        function() {
                            return resizeImage(scope, elm, attrs);
                        });
                }
            };

            function resizeImage(scope, elm, attrs) {
                var neededWidth = $parse(attrs.neededWidth)(scope);
                var neededHeight = neededWidth * elm[0].height / elm[0].width;
                var canvas = document.createElement("canvas");
                canvas.width = neededWidth;
                canvas.height = neededHeight;
                var ctx = canvas.getContext("2d");
                ctx.drawImage(elm[0], 0, 0, neededWidth, neededHeight);
                return elm.attr("src", canvas.toDataURL("image/jpeg"));
            }
        }
    ]);

})(window.angular);
(function(angular) {
    'use strict';

    var app = angular
        .module('CalcheeseAdmin');

    // Top section: quick actions list and right actions
    app.directive('pageTop',
    [
        'url', function(url) {
            return {
                restrict: 'E',
                controller: 'LayoutCtrl',
                templateUrl: url.partial('/page-top.tmpl.html')
            };
        }
    ]);

    // Left section: sidebar to display main navigation
    app.directive('pageSidebar',
    [
        'url', function(url) {
            return {
                restrict: 'E',
                controller: 'LayoutCtrl',
                templateUrl: url.partial('/page-sidebar.tmpl.html')
            };
        }
    ]);

    // Bottom section: current user, role or credit
    app.directive('pageBottom',
    [
        'url', function(url) {
            return {
                restrict: 'E',
                controller: 'LayoutCtrl',
                templateUrl: url.partial('/page-bottom.tmpl.html')
            };
        }
    ]);


    // Overlay to display processing message
    app.directive('comOverlay',
    [
        'url', function(url) {
            return {
                restrict: 'E',
                scope: {
                    show: '='
                },
                transclude: true, // Insert custom content inside the directive
                link: function(scope, element, attrs) {
                    scope.dialogStyle = {};
                    if (attrs.width) {
                        scope.dialogStyle.width = attrs.width;
                    }
                    if (attrs.height) {
                        scope.dialogStyle.height = attrs.height;
                    }
                    angular.element('body').addClass('overlay-open');

                    scope.hideModal = function() {
                        scope.show = false;
                        angular.element('body').removeClass('overlay-open');
                    };
                },
                templateUrl: url.partial('/com-overlay.tmpl.html')
            };
        }
    ]);

    app.directive("loadingIndicator",
        function() {
            return {
                restrict: "A",
                template: "<div>Loading...</div>",
                link: function(scope, element, attrs) {
                    scope.$on("loading-started",
                        function(e) {
                            element.css({ "display": "" });
                        });
                    scope.$on("loading-complete",
                        function(e) {
                            element.css({ "display": "none" });
                        });
                }
            };
        });

    // Component: breadcrumbs
    app.directive('comBreadcrumb',
    [
        'url', function(url) {
            return {
                restrict: 'E',
                controller: 'LayoutCtrl',
                templateUrl: url.partial('/com-breadcrumb.tmpl.html')
            };
        }
    ]);

    // AntiForgery token
    app.directive('tokenAf',
    [
        '$http', function($http) {
            return function(scope, element, attrs) {
                $http.defaults.headers
                    .common['RequestVerificationToken'] = attrs.tokenAf || "no request verification token";
            };
        }
    ]);

    // Affix addin: add/remove class 
    app.directive('comAffix',
    [
        '$window', '$uibPosition', function($window, $uibPosition) {
            return {
                restrict: 'A',
                scope: { 'compareWith': '@' },
                link: function(scope, element, attrs) {
                    var _initialOffset = getOffset();
                    var _compareElement = $window.document.querySelector('#' + scope.compareWith);

                    angular.element($window).bind('scroll',
                        function() {
                            var newOffset = getOffset();
                            scope.$apply(function() {
                                if (affixNeeded(newOffset)) {
                                    angular.element(element).addClass('affix');
                                    element.css({ top: '0', width: newOffset.width + 'px' });
                                } else {
                                    angular.element(element).removeClass('affix');
                                    element.css({ top: 'auto', width: 'auto' });
                                }
                            });
                        });

                    function getOffset() {
                        return $uibPosition.offset(element[0]);
                    }

                    function affixNeeded(newOffset) {
                        if (_compareElement) {
                            var offsetCompare = $uibPosition.offset(_compareElement);
                            if (offsetCompare.height < newOffset.height)
                                return false;
                        }
                        return $window.pageYOffset > _initialOffset.top;
                    }
                }
            };
        }
    ]);

})(window.angular);
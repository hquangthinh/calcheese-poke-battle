using System;

namespace CalcheesePokeBattle.CouponCode.Storage.Document
{
    public class UsedCouponCodeDocument
    {
        public int Id { get; set; }

        public string CouponCode { get; set; }

        public DateTime CreatedDate { get; set; }

        public string ClientIpAddress { get; set; }

        public string ClientInfo { get; set; }
    }
}
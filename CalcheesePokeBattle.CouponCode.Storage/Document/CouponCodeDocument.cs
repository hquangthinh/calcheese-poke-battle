﻿using System;

namespace CalcheesePokeBattle.CouponCode.Storage.Document
{
    public class CouponCodeDocument
    {
        public int Id { get; set; }

        public string Code { get; set; }

        public string Description { get; set; }

        public bool Available { get; set; }

        public int Point { get; set; }

        public int? CardId { get; set; }

        public string RedeemByUserName { get; set; }

        public DateTime? RedeemOn { get; set; }

        public string ClientIpAddress { get; set; }

        public string ClientInfo { get; set; }
    }
}

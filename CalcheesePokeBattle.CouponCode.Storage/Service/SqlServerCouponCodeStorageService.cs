﻿using System;
using CalcheesePokeBattle.CouponCode.Storage.Document;

namespace CalcheesePokeBattle.CouponCode.Storage.Service
{
    /// <summary>
    /// Use sql server as storage for coupon code
    /// </summary>
    public class SqlServerCouponCodeStorageService : ICouponCodeStorageService
    {
        public CouponCodeDocument FindCode(string rawCode)
        {
            throw new NotImplementedException();
        }

        public CouponCodeDocument MarkCodeHasBeenUsed(UpdateCouponCodeCommand command)
        {
            throw new NotImplementedException();
        }
    }
}
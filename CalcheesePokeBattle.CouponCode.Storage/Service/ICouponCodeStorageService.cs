﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CalcheesePokeBattle.CouponCode.Storage.Document;

namespace CalcheesePokeBattle.CouponCode.Storage.Service
{
    public class UpdateCouponCodeCommand
    {
        public string Code { get; set; }

        public bool Available { get; set; }

        public int Point { get; set; }

        public int CardId { get; set; }

        public string RedeemByUserName { get; set; }

        public DateTime RedeemOn { get; set; }

        public string ClientIpAddress { get; set; }

        public string ClientInfo { get; set; }
    }

    public interface ICouponCodeStorageService
    {
        CouponCodeDocument FindCode(string rawCode);

        CouponCodeDocument MarkCodeHasBeenUsed(UpdateCouponCodeCommand command);
    }
}

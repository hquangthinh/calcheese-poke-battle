﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using CalcheesePokeBattle.Common.DTO;
using Microsoft.Azure.WebJobs;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Security.Cryptography;
using Dapper;
using Newtonsoft.Json;

namespace CalcheesePokeBattle.ImportCouponCodeJob
{
    public class Functions
    {
        // This function will get triggered/executed when a new message is written 
        // on an Azure Queue called importcouponcoderequest
        public static void ProcessImportCouponCodeRequest(
            [QueueTrigger("importcouponcoderequest")] string secretMessageCommand,
            TextWriter log
        )
        {
            // validate message
            if (string.IsNullOrEmpty(secretMessageCommand))
                return;

            var message = JsonConvert.DeserializeObject<BatchImportQueueMessage>(secretMessageCommand);

            if (string.IsNullOrEmpty(message?.Message))
                return;

            var connString = ConfigurationManager.ConnectionStrings["CalcheeseAppDbContext"].ConnectionString;

            using (var db = new SqlConnection(connString))
            {
                db.Open();
                var userCheckRes =
                    db.Query<UserAccessInfo>(
                            $"SELECT * FROM UserAccessControl WHERE UserName = '{message.Message}'")
                        .ToList();
                if (userCheckRes.Count == 0)
                {
                    log.WriteLine($"Invalid request from user {message.Message} - ip address {message.IpAddress}");
                    return;
                }

                // check if there's ongoing import job
                var ongoingJobCount = db.QueryFirst<int>("SELECT COUNT(*) FROM ImportJobStatus");
                if (ongoingJobCount > 0)
                {
                    log.WriteLine("There's ongoing running job... stop this one");
                    return;
                }

                // register new job
                var jobName = $"Job by {message.Message} from {message.IpAddress}";
                db.Execute($"INSERT INTO ImportJobStatus(JobName, CreatedDate) VALUES('{jobName}', GETUTCDATE())");
            }

            log.WriteLine("Begin importing data...");

            // only perform batch import when secretMessageCommand meets condition
            var stopWatch = new Stopwatch();
            stopWatch.Start();

            var md5Hasher = new MD5CryptoServiceProvider();
            const int batchSize = 100;

            // read list of import request information from sql db table UploadFile 
            
            var listOfImportFileInfo = GetImportFileInfo(connString);
            if (listOfImportFileInfo.Count == 0)
            {
                DeleteJob(connString, log);

                stopWatch.Stop();
                log.WriteLine($"All done. Running time in minutes---- {stopWatch.Elapsed.TotalMinutes}");
                return;
            }

            // Retrieve storage account from connection string.
            var storageAccount = CloudStorageAccount.Parse(ConfigurationManager.ConnectionStrings["AzureWebJobsStorage"].ConnectionString);
            // Create the blob client.
            var blobClient = storageAccount.CreateCloudBlobClient();
            // Retrieve reference to a previously created container.
            var container = blobClient.GetContainerReference("dataforimport");

            foreach (var fileUploadInfo in listOfImportFileInfo)
            {
                var cardId = fileUploadInfo.PokeCardId;
                var batchCount = 1;

                log.WriteLine($"Upload data for card {cardId} from file {fileUploadInfo.FileName}");

                using (var fileReader = GetStreamFromFileUrl(fileUploadInfo.BlobName, container))
                {
                    using (var db = new SqlConnection(connString))
                    {
                        db.Open();
                        var counter = 0;
                        var sqlBuiler = new StringBuilder();
                        while (!fileReader.EndOfStream)
                        {
                            var rawCode = fileReader.ReadLine();
                            if (string.IsNullOrEmpty(rawCode))
                            {
                                continue;
                            }
                            var code = GetHashString(rawCode, md5Hasher);
                            sqlBuiler
                                .AppendFormat(
                                    $"INSERT INTO CouponCode(Code, Available, Point, CardId) VALUES(\'{code}\', 1, 0, {cardId})")
                                .AppendLine();
                            if (counter < batchSize)
                            {
                                counter++;
                            }
                            else
                            {
                                try
                                {
                                    var dbTrans = db.BeginTransaction();
                                    db.Execute(sqlBuiler.ToString(), null, dbTrans, 30, CommandType.Text);
                                    dbTrans.Commit();
                                    log.WriteLine($"batch commited...{counter * batchCount}");
                                }
                                catch (Exception ex)
                                {
                                    // log sql text error
                                    log.WriteLine(
                                        $"Error processing batch {batchCount} -> error details -> {ex} ---> sql text: {sqlBuiler}");
                                }
                                finally
                                {
                                    counter = 0;
                                    batchCount++;
                                    sqlBuiler = new StringBuilder();
                                }
                            }
                        }//end of reading file
                        var endBatchSql = sqlBuiler.ToString();
                        if (!string.IsNullOrEmpty(endBatchSql))
                        {
                            try
                            {
                                var dbTrans = db.BeginTransaction();
                                db.Execute(sqlBuiler.ToString(), null, dbTrans, 30, CommandType.Text);
                                dbTrans.Commit();
                                log.WriteLine("last batch commited...");
                            }
                            catch (Exception ex)
                            {
                                // log sql text error
                                log.WriteLine(
                                    $"Error processing batch {batchCount} -> error details -> {ex} ---> sql text: {sqlBuiler}");
                            }
                        }

                        // update file import status
                        db.Execute($"UPDATE UploadFile SET ImportStatus = 'OK' WHERE Id={fileUploadInfo.Id}");

                    }//dispose connection
                }// dispose file reader

                log.WriteLine($"Finish importing file {fileUploadInfo.FileName}. Go next file");
            }// end file info list

            // delete job
            DeleteJob(connString, log);

            stopWatch.Stop();
            log.WriteLine($"All done. Running time in minutes---- {stopWatch.Elapsed.TotalMinutes}");
        }

        private static void DeleteJob(string connString, TextWriter log)
        {
            using (var db = new SqlConnection(connString))
            {
                log.WriteLine("Delete job entry to mark complete");
                db.Open();
                db.Execute("DELETE ImportJobStatus");
            }
        }

        private static StreamReader GetStreamFromFileUrl(string blobName, CloudBlobContainer container)
        {
            // Retrieve reference to a blob named "blobName".
            var blockBlob = container.GetBlockBlobReference(blobName);
            return new StreamReader(blockBlob.OpenRead());
        }

        private static List<FileUploadInfo> GetImportFileInfo(string connString)
        {
            using (var db = new SqlConnection(connString))
            {
                db.Open();
                var result =
                    db.Query<FileUploadInfo>(
                            "SELECT Id, PokeCardID, CreatedDate, [FileName], ImportStatus FROM UploadFile WHERE [FileName] IS NOT NULL AND ImportStatus IS NULL")
                        .ToList();
                return result;
            }
        }

        private static void DeleteBlob(BlobInformation blobInfo)
        {
            if(blobInfo == null)
                return;

            // Retrieve storage account from connection string.
            var storageAccount = CloudStorageAccount.Parse(ConfigurationManager.ConnectionStrings["AzureWebJobsStorage"].ConnectionString);

            // Create the blob client.
            var blobClient = storageAccount.CreateCloudBlobClient();

            // Retrieve reference to a previously created container.
            var container = blobClient.GetContainerReference("dataforimport");

            // Retrieve reference to a blob named "myblob.txt".
            var blockBlob = container.GetBlockBlobReference(blobInfo.BlobName);

            // Delete the blob.
            blockBlob.Delete();
        }

        private static string GetHashString(string input, MD5CryptoServiceProvider md5Hasher)
        {
            if (string.IsNullOrEmpty(input))
                throw new ArgumentNullException(nameof(input));

            var hashData = md5Hasher.ComputeHash(Encoding.Default.GetBytes(input));
            var sBuilder = new StringBuilder();
            // Loop through each byte of the hashed data 
            // and format each one as a hexadecimal string.
            foreach (byte b in hashData)
            {
                sBuilder.Append(b.ToString("x2"));
            }
            return sBuilder.ToString();
        }
    }

    class FileUploadInfo
    {
        public int Id { get; set; }

        public DateTime CreatedDate { get; set; }

        public int PokeCardId { get; set; }

        public string FileName { get; set; }

        public Uri BlobUri => new Uri(FileName);

        public string BlobName => BlobUri.Segments[BlobUri.Segments.Length - 1];

    }

    class UserAccessInfo
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string IpAddress { get; set; }
    }

    class ImportJobStatus
    {
        public int Id { get; set; }
        public string JobName { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}

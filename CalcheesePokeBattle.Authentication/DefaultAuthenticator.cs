﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core;
using System.Threading.Tasks;
using CalcheesePokeBattle.Common.Utils;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security;

namespace CalcheesePokeBattle.Authentication
{
    public class DefaultAuthenticator : IAuthenticator
    {
        private ApplicationRoleManager RoleManager { get; }
        private ApplicationUserManager UserManager { get; }
        private ApplicationUserStore UserStore { get; }
        private IAuthenticationManager AuthenticationManager { get; }

        public DefaultAuthenticator(
            ApplicationRoleManager roleManager,
            ApplicationUserManager userManager, 
            ApplicationUserStore userStore, 
            IAuthenticationManager authenticationManager)
        {
            RoleManager = roleManager;
            UserManager = userManager;
            UserStore = userStore;
            AuthenticationManager = authenticationManager;
        }

        public IApplicationUser FindByName(string userName)
        {
            return AsyncUtils.RunSync(() => FindByNameAsync(userName));
        }

        public async Task<IApplicationUser> FindByNameAsync(string userName)
        {
            return await UserManager.FindByNameAsync(userName);
        }

        public void SetPassword(IApplicationUser user, string password)
        {
            AsyncUtils.RunSync(() => SetPasswordAsync(user, password));
        }

        public async Task SetPasswordAsync(IApplicationUser user, string password)
        {
            await UserStore.SetPasswordHashAsync((ApplicationUser)user, UserManager.PasswordHasher.HashPassword(password));
        }

        public IApplicationUser CreateAppUser(string userName, string email, string password, bool? isActive = null)
        {
            return AsyncUtils.RunSync(() => CreateAppUserAsync(userName, email, password, isActive));
        }

        public async Task<IApplicationUser> CreateAppUserAsync(string userName, string email, string password, bool? isActive = null)
        {
            var user = new ApplicationUser
            {
                UserName = userName,
                PasswordHash = UserManager.PasswordHasher.HashPassword(password),
                Email = email
            };

            if (await UserManager.CreateAsync(user) == IdentityResult.Success)
            {
                await UserManager.AddApplicationLoginAsync(user.Id);
                if (isActive.HasValue) await UserManager.SetLockoutEnabledAsync(user.Id, !isActive.Value);
            }
            return user;
        }

        public IApplicationUser CreateOrUpdateUser(string userName, string email, string password, bool isActive)
        {
            return AsyncUtils.RunSync(() => CreateOrUpdateUserAsync(userName, email, password, isActive));
        }

        public async Task<IApplicationUser> CreateOrUpdateUserAsync(string userName, string email, string password, bool isActive)
        {
            var user = await UserManager.FindByNameAsync(userName);
            if (user != null && password != null)
            {
                await SetPasswordAsync(user, password);
                var hasApplicationLogin = await UserManager.HasApplicationLoginAsync(user.Id);
                if (!hasApplicationLogin) await UserManager.AddApplicationLoginAsync(user.Id);
            }
            else if (user == null && password != null)
            {
                user = (ApplicationUser)await CreateAppUserAsync(userName, email, password);
                if (user != null) await UserManager.SetLockoutEnabledAsync(user.Id, !isActive);
            }

            return user;
        }

        public void Deactivate(string userName)
        {
            AsyncUtils.RunSync(() => DeactivateAsync(userName));
        }

        public async Task DeactivateAsync(string userName)
        {
            await SetUserActiveStatusAsync(userName, false);
        }

        public void Activate(string userName)
        {
            AsyncUtils.RunSync(() => ActivateAsync(userName));
        }

        public async Task ActivateAsync(string userName)
        {
            await SetUserActiveStatusAsync(userName, true);
        }

        public async Task SignInAsync(IApplicationUser user, bool isPersistent)
        {
            await UserManager.SignInAsync((ApplicationUser)user, isPersistent);
        }

        public void SignOut()
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
        }

        public void ChangePassword(string userName, string password)
        {
            AsyncUtils.RunSync(() => ChangePasswordAsync(userName, password));
        }

        public async Task ChangePasswordAsync(string userName, string password)
        {
            var appUser = await FindByNameAsync(userName);
            if (appUser == null)
            {
                throw new ArgumentException($"Cannot find user with name {userName}");
            }

            await SetPasswordAsync(appUser, password);
            await UserStore.UpdateAsync((ApplicationUser)appUser);
        }

        private async Task SetUserActiveStatusAsync(string userName, bool isActive)
        {
            var user = await FindByNameAsync(userName);
            if (user != null) await UserManager.SetLockoutEnabledAsync(user.Id, !isActive);
        }

        public void CreateRole(string roleName)
        {
            AsyncUtils.RunSync(() => CreateRoleAsync(roleName));
        }

        public async Task CreateRoleAsync(string roleName)
        {
            if (string.IsNullOrEmpty(roleName))
                throw new ArgumentNullException(nameof(roleName));

            await RoleManager.CreateAsync(new ApplicationRole
            {
                Id = Guid.NewGuid().ToString(),
                Name = roleName
            });
        }

        public IdentityRole FindRole(string roleName)
        {
            return AsyncUtils.RunSync(() => FindRoleAsync(roleName));
        }

        public async Task<IdentityRole> FindRoleAsync(string roleName)
        {
            if (string.IsNullOrEmpty(roleName))
                throw new ArgumentNullException(nameof(roleName));

            var appRole = await RoleManager.FindByNameAsync(roleName);
            if(appRole == null)
                throw new ObjectNotFoundException($"Cannot find role {roleName}");

            return appRole;
        }

        public IdentityResult AddUserToRole(string userId, string roleName)
        {
            return AsyncUtils.RunSync(() => AddUserToRoleAsync(userId, roleName));
        }

        public async Task<IdentityResult> AddUserToRoleAsync(string userId, string roleName)
        {
            if (string.IsNullOrEmpty(userId))
                throw new ArgumentNullException(nameof(userId));

            if (string.IsNullOrEmpty(roleName))
                throw new ArgumentNullException(nameof(roleName));
            return await UserManager.AddToRoleAsync(userId, roleName);
        }

        public IdentityResult AddUserToRoles(string userId, string[] roleNames)
        {
            return AsyncUtils.RunSync(() => AddToRolesAsync(userId, roleNames));
        }

        public async Task<IdentityResult> AddToRolesAsync(string userId, string[] roleNames)
        {
            if (string.IsNullOrEmpty(userId))
                throw new ArgumentNullException(nameof(userId));

            if (roleNames == null)
                throw new ArgumentNullException(nameof(roleNames));
            return await UserManager.AddToRolesAsync(userId, roleNames);
        }

        public IList<string> GetRoleForUser(string userId)
        {
            return AsyncUtils.RunSync(() => GetRoleForUserAsync(userId));
        }

        public async Task<IList<string>> GetRoleForUserAsync(string userId)
        {
            if (string.IsNullOrEmpty(userId))
                throw new ArgumentNullException(nameof(userId));

            return await UserManager.GetRolesAsync(userId);
        }
    }
}
﻿namespace CalcheesePokeBattle.Authentication.Models
{
    public class RoleDTO
    {
        public const string Administrator = "Administrator";

        public const string NormalUser = "NormalUser";

        public const string LuckyDrawManager = "LuckyDrawManager";

        public const string TestUser = "TestUser";

        public string Id { get; set; }

        public string Name { get; set; }
    }
}
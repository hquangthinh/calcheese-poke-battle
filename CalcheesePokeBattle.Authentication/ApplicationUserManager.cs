﻿using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security;

namespace CalcheesePokeBattle.Authentication
{
    public class ApplicationRoleManager : RoleManager<ApplicationRole>
    {
        private ApplicationRoleStore RoleStore { get; }

        public ApplicationRoleManager(ApplicationRoleStore store) : base(store)
        {
            RoleStore = store;
        }
    }

    public class ApplicationUserManager : UserManager<ApplicationUser>
    {
        private IAuthenticationManager AuthenticationManager { get; }
        private ApplicationUserStore UserStore { get; }

        public ApplicationUserManager(ApplicationUserStore store, IAuthenticationManager authenticationManager) : base(store)
        {
            AuthenticationManager = authenticationManager;
            UserStore = store;
        }

        public async Task SignInAsync(ApplicationUser user, bool isPersistent)
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ExternalCookie);
            var identity = await GenerateUserIdentityAsync(user);
            AuthenticationManager.SignIn(new AuthenticationProperties { IsPersistent = isPersistent }, identity);
        }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(ApplicationUser user)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await CreateIdentityAsync(user, DefaultAuthenticationTypes.ApplicationCookie);
            return userIdentity;
        }

        public async Task<IdentityResult> AddApplicationLoginAsync(string userId)
        {
            return await AddLoginAsync(userId, new UserLoginInfo(AppAuthenticationProvider, userId));
        }

        public async Task<bool> HasLoginAsync(string userId, string loginProvider)
        {
            var logins = await GetLoginsAsync(userId);
            return logins.Any(login => login.LoginProvider == loginProvider);
        }

        public async Task<bool> HasApplicationLoginAsync(string userId)
        {
            return await HasLoginAsync(userId, AppAuthenticationProvider);
        }

        const string AppAuthenticationProvider = "Application";
    }

    public class ApplicationRoleStore : RoleStore<ApplicationRole>
    {
        public ApplicationIdentityDbContext DbContext { get; }

        public ApplicationRoleStore(ApplicationIdentityDbContext dbContext) : base(dbContext)
        {
            DbContext = dbContext;
            DisposeContext = true;
        }
    }

    public class ApplicationUserStore : UserStore<ApplicationUser>
    {
        public ApplicationIdentityDbContext DbContext { get; }

        public ApplicationUserStore(ApplicationIdentityDbContext dbContext) : base(dbContext)
        {
            DbContext = dbContext;
            DisposeContext = true;
        }
    }
}

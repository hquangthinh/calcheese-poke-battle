﻿using Microsoft.AspNet.Identity.EntityFramework;

namespace CalcheesePokeBattle.Authentication
{
    public class ApplicationIdentityDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationIdentityDbContext() : base("CalcheeseIdentityDbContext", throwIfV1Schema: false)
        {
        }
    }
}

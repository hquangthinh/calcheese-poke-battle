﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CalcheesePokeBattle.Authentication.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace CalcheesePokeBattle.Authentication
{
    public interface IAuthenticator
    {
        IApplicationUser FindByName(string userName);
        Task<IApplicationUser> FindByNameAsync(string userName);

        void SetPassword(IApplicationUser user, string password);
        Task SetPasswordAsync(IApplicationUser user, string password);

        IApplicationUser CreateAppUser(string userName, string email, string password, bool? isActive = null);
        Task<IApplicationUser> CreateAppUserAsync(string userName, string email, string password, bool? isActive = null);

        IApplicationUser CreateOrUpdateUser(string userName, string email, string password, bool isActive);
        Task<IApplicationUser> CreateOrUpdateUserAsync(string userName, string email, string password, bool isActive);

        void Deactivate(string userName);
        Task DeactivateAsync(string userName);

        void Activate(string userName);
        Task ActivateAsync(string userName);

        Task SignInAsync(IApplicationUser user, bool isPersistent);
        void SignOut();

        void ChangePassword(string userName, string password);
        Task ChangePasswordAsync(string userName, string password);

        void CreateRole(string roleName);
        Task CreateRoleAsync(string roleName);

        IdentityRole FindRole(string roleName);
        Task<IdentityRole> FindRoleAsync(string roleName);

        IdentityResult AddUserToRole(string userId, string roleName);
        Task<IdentityResult> AddUserToRoleAsync(string userId, string roleName);

        IdentityResult AddUserToRoles(string userId, string[] roleNames);
        Task<IdentityResult> AddToRolesAsync(string userId, string[] roleNames);

        IList<string> GetRoleForUser(string userId);
        Task<IList<string>> GetRoleForUserAsync(string userId);
    }

    public interface IApplicationUser
    {
        string Id { get; set; }
        string UserName { get; set; }
        bool LockoutEnabled { get; set; }
    }

    public class ApplicationRole : IdentityRole, IRole
    {
    }

    public class ApplicationUser : IdentityUser, IApplicationUser
    {
    }
}
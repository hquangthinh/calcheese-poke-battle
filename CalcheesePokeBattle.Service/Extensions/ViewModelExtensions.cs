﻿using System.Text;
using CalcheesePokeBattle.DTO;

namespace CalcheesePokeBattle.Extensions
{
    public static class ViewModelExtensions
    {
        public static void SetProfileFullAddress(this UserProfileDTO profile)
        {
            var fullAddressBuilder = new StringBuilder();

            if (!string.IsNullOrEmpty(profile.AddressNumber))
                fullAddressBuilder.Append(profile.AddressNumber).Append(" ");

            if (!string.IsNullOrEmpty(profile.StreetName))
                fullAddressBuilder.Append(profile.StreetName).Append(" ");

            if (!string.IsNullOrEmpty(profile.WardName))
                fullAddressBuilder.Append(profile.WardName).Append(" ");

            if (!string.IsNullOrEmpty(profile.DistrictName))
                fullAddressBuilder.Append(profile.DistrictName).Append(" ");

            if (!string.IsNullOrEmpty(profile.CityName))
                fullAddressBuilder.Append(profile.CityName).Append(" ");

            var fullAddr = fullAddressBuilder.ToString().Trim();

            profile.FullAddress = string.IsNullOrEmpty(fullAddr) ? profile.Address : fullAddr;
        }
    }
}


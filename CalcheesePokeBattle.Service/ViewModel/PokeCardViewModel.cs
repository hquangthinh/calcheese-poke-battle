﻿using System;
using CalcheesePokeBattle.DTO;
using CalcheesePokeBattle.Helper;
using CalcheesePokeBattle.Model;

namespace CalcheesePokeBattle.ViewModel
{
    public class PokeCardViewModel : PokeCardDTO
    {
        public string ImageBaseUrl { get; set; }

        public int TotalCount { get; set; }

        public bool CardBelongToUser => TotalCount > 0;

        public string DisplayImageUrl => CardBelongToUser
            ? ImageUrl
            : $"{ImageBaseUrl}/default.png";

        public string CardElementDisplay => PokemonCardHelper.TranslateCardElementForDisplaying(CardElement);

        public string MainCardElementDisplay => PokemonCardHelper.TranslateMainCardElementForDisplaying(CardElement);

        public string CardElementImageUrl => $"{ImageBaseUrl}/{CardElement.Replace(",", "_")}.png".ToLower();

        public string CardTypeDisplay => IsSpecialCard ? "Đặc biệt" : "Thường";

        public static readonly Func<PokeCard, string, PokeCardViewModel> EntityToViewModelMapper =
            (entity, imageBaseUrl) => entity == null
                ? null
                : new PokeCardViewModel
                {
                    Id = entity.Id,
                    CardType = entity.CardType,
                    CardElement = entity.CardElement,
                    CardNumber = entity.CardNumber,
                    CardPoint = entity.CardPoint,
                    CardStar = entity.CardStar,
                    ImageBaseUrl = imageBaseUrl,
                    ImageUrl = $"{imageBaseUrl}/{entity.ImageUrl}",
                    BackSideImageUrl = $"{imageBaseUrl}/{entity.BackSideImageUrl}",
                    Category = entity.Category,
                    Height = entity.Height,
                    Weight = entity.Weight,
                    Skill1 = entity.Skill1,
                    Skill2 = entity.Skill2,
                    Skill3 = entity.Skill3,
                    UltimateSkill = entity.UltimateSkill,
                    EvolutionTo = entity.EvolutionTo,
                    CardName = entity.CardName,
                    CardGroup = entity.CardGroup
                };

        public static readonly Func<PokeCardDTO, string, PokeCardViewModel> DtoToViewModelMapper =
            (entity, imageBaseUrl) => entity == null
                ? null
                : new PokeCardViewModel
                {
                    Id = entity.Id,
                    CardType = entity.CardType,
                    CardElement = entity.CardElement,
                    CardNumber = entity.CardNumber,
                    CardPoint = entity.CardPoint,
                    CardStar = entity.CardStar,
                    ImageBaseUrl = imageBaseUrl,
                    ImageUrl = $"{imageBaseUrl}/{entity.ImageUrl}",
                    BackSideImageUrl = $"{imageBaseUrl}/{entity.BackSideImageUrl}",
                    Category = entity.Category,
                    Height = entity.Height,
                    Weight = entity.Weight,
                    Skill1 = entity.Skill1,
                    Skill2 = entity.Skill2,
                    Skill3 = entity.Skill3,
                    UltimateSkill = entity.UltimateSkill,
                    EvolutionTo = entity.EvolutionTo,
                    CardName = entity.CardName,
                    CardGroup = entity.CardGroup
                };
    }
}
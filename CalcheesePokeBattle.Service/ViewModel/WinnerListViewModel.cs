﻿using CalcheesePokeBattle.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalcheesePokeBattle.ViewModel
{
    public class WinnerListViewModel 
    {
        public List<WinnerListDTO> WinnerList { get; set; }

        public List<GiftItemDTO> ReedeemList { get; set; }

    }
}

﻿using System.Collections.Generic;
using CalcheesePokeBattle.DTO;

namespace CalcheesePokeBattle.ViewModel
{

    public class UserCouponCodeStatisticModel
    {
        public int TotalCode { get; set; }

        public int TotalCodeInUsed { get; set; }

        public int TotalUser { get; set; }

        public int TotalUserInPlay { get; set; }

        public List<UserRegistrationSummaryByDateDTO> TotalUserRegistrationSummaryByDate { get; set; }

        public List<UserRegistrationSummaryByDateDTO> TotalUserRegistrationWithInputCardSummaryByDate { get; set; }

        public List<SummaryByDateReportDTO> TotalUserPlayGame { get; set; }
    }
}
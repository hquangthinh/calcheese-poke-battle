﻿using System.Collections.Generic;
using CalcheesePokeBattle.DTO;

namespace CalcheesePokeBattle.ViewModel
{
    public class RedeemPageViewModel
    {
        public List<GiftItemDTO> LuckyDrawGifts { get; set; }

        public List<GiftItemDTO> RedeemGifts { get; set; }
    }
}
﻿using CalcheesePokeBattle.DTO;

namespace CalcheesePokeBattle.ViewModel
{
    public class AllGiftRedeemHistoryViewModel : GiftRedeemHistoryDTO
    {
        public string UserName { get; set; }

        public string FullName =>
            string.IsNullOrEmpty(FirstName) && string.IsNullOrEmpty(LastName) ? UserName : $"{FirstName} {LastName}";

        public string DeliveryAddress { get; set; }

        public string SocialIdNumber { get; set; }

        public string AddressNumber { get; set; }

        public string StreetName { get; set; }

        public string WardName { get; set; }

        public string DistrictName { get; set; }

        public string CityName { get; set; }

        public string GiftDescription { get; set; }
    }

    public class AllGiftRedeemHistoryExportModel
    {
        public int WeekNumber { get; set; }
        public string GiftDescription { get; set; }
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
        public string Address { get; set; }
        public string DeliveryAddress { get; set; }
        public string AddressNumber { get; set; }
        public string StreetName { get; set; }
        public string WardName { get; set; }
        public string DistrictName { get; set; }
        public string CityName { get; set; }
    }
}
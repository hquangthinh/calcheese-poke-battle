﻿using System;
using System.Collections.Generic;
using System.Linq;
using CalcheesePokeBattle.DTO;
using CalcheesePokeBattle.Model;

namespace CalcheesePokeBattle.ViewModel
{
    public class GiftItemViewModel : GiftItemDTO
    {
        public int TotalCount { get; set; }

        public bool BelongToUser => TotalCount > 0;

        public bool IsForRedeem =>
            GlobalConstants.SpecialPokemonNames.Contains(Name) || RedeemPoint != 9999999;

        public bool IsForLuckyDraw =>
            GlobalConstants.GiftNamesForLuckyDraw.Contains(Name) || RedeemPoint == 9999999;

        public static readonly Func<GiftItem, GiftItemViewModel> EntityToViewModelMapper =
           entity => entity == null
               ? null
               : new GiftItemViewModel
               {
                   Id = entity.Id,
                   Name = entity.Name,
                   ImageUrl = entity.ImageUrl,
                   Description = entity.Description,
                   RedeemPoint = entity.RedeemPoint,
                   IsRedeem = entity.IsRedeem
               };

        public static readonly Func<UserInventory, GiftItemViewModel> UserInventoryToMapper =
           entity => entity == null
               ? null
               : new GiftItemViewModel
               {
                   Id = entity.Id,
                   Name = entity.Name,
                   ImageUrl = entity.ImageUrl
               };
    }

    public class UserInventoryViewModel
    {
        public UserInventoryViewModel()
        {
            LuckyDrawCodes = new List<UserLuckyDrawCodeDTO>();
            GiftsWonFromLuckyDraw = new List<GiftItemViewModel>();
            GiftsWonFromLuckyDrawMap = new Dictionary<string, bool>();
            GiftWonFromRedeemPoints = new List<GiftItemViewModel>();
            GiftWonFromRedeemPointsMap = new Dictionary<string, int>();
        }

        public List<UserLuckyDrawCodeDTO> LuckyDrawCodes { get; set; }

        public List<GiftItemViewModel> GiftsWonFromLuckyDraw { get; set; }

        public Dictionary<string, bool> GiftsWonFromLuckyDrawMap { get; set; }

        public List<GiftItemViewModel> GiftWonFromRedeemPoints { get; set; }

        public Dictionary<string, int> GiftWonFromRedeemPointsMap { get; set; }

        public bool UserInventoryIsEmpty
            => LuckyDrawCodes?.Count == 0 && !HasGiftWonFromLuckyDraw && !HasGiftWonFromRedeemPoints;

        public bool HasGiftWonFromLuckyDraw => GiftsWonFromLuckyDraw?.Count(x => x.TotalCount > 0) > 0;

        public bool HasGiftWonFromRedeemPoints => GiftWonFromRedeemPoints?.Count(x => x.TotalCount > 0) > 0;
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalcheesePokeBattle.ViewModel
{
    public class PokeCardReleaseViewModel
    {
        public int Total { get; set; }
        public int TotalRelease { get; set; }
        public string ImageUrl { get; set; }
        public string CardName { get; set; }
    }
}

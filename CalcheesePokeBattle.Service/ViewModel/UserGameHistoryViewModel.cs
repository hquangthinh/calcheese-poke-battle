using CalcheesePokeBattle.DTO;

namespace CalcheesePokeBattle.ViewModel
{
    public class UserGameHistoryViewModel : GameHistoryDTO
    {
        public string OpponentUserName { get; set; }

        public string UserName1 { get; set; }

        public string UserName2 { get; set; }

        public int WinPoints { get; set; }

        public int LostPoints { get; set; }

        public int TotalPoints { get; set; }

        public string TradeCardImageUrl { get; set; }
    }
}
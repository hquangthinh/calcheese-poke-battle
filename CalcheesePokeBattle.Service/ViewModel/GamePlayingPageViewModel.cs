﻿namespace CalcheesePokeBattle.ViewModel
{
    public class GamePlayingPageViewModel
    {
        public int GameSessionId { get; set; }

        /// <summary>
        /// remaining time for user to submit card
        /// in case player is in game and reload page
        /// </summary>
        public int ThinkTimeRemain { get; set; }

        public UserProfileViewModel PlayerOnLeft { get; set; }

        public UserProfileViewModel PlayerOnRight { get; set; }
    }
}

﻿using CalcheesePokeBattle.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalcheesePokeBattle.ViewModel
{
    public class UserLuckyDrawCodeViewModel
    {
        public UserLuckyDrawCodeDTO UserLuckyDrawCodeDTO { get; set; }
        public UserProfileDTO UserProfileDTO { get; set; }
        //public PrizeLuckyDrawHistoryDTO PrizeLuckyDrawHistoryDTO { get; set; }
    }
}

﻿using CalcheesePokeBattle.DTO;
using System.Collections.Generic;

namespace CalcheesePokeBattle.ViewModel
{
    public class CurrentWeekViewModel
    {
        public List<WeekDefinitionViewModel> WeekDefinitionViewModels { get; set; }
        public WeekDefinitionDTO CuurentWeekDefinition { get; set; }
    }
}

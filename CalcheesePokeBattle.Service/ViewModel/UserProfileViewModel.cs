﻿using System;
using System.Collections.Generic;
using System.Linq;
using CalcheesePokeBattle.DTO;
using CalcheesePokeBattle.Model;

namespace CalcheesePokeBattle.ViewModel
{
    public class UserProfileViewModel : UserProfileDTO
    {
        public List<PokeCardViewModel> SpecialCardList { get; set; }

        /// <summary>
        /// Normal cards list in a group
        /// </summary>
        public List<List<PokeCardViewModel>> NormalCardList { get; set; }

        /// <summary>
        /// All normal card list in flat list
        /// </summary>
        public List<PokeCardViewModel> AllNormalCardList { get; set; }

        /// <summary>
        /// Combine all special & normal cards
        /// </summary>
        public List<PokeCardViewModel> AllUserCards { get; set; }

        public static readonly Func<UserProfile, UserProfileViewModel> MinimumEntityToViewModelMapper =
            entity => entity == null
                ? null
                : new UserProfileViewModel
                {
                    Id = entity.Id,
                    UserName = entity.UserName,
                    FirstName = entity.FirstName,
                    LastName = entity.LastName,
                    PhoneNumber = entity.PhoneNumber,
                    Email = entity.Email,
                    Address = entity.Address,
                    TotalPoint = entity.TotalPoint,
                    DeliveryAddress = entity.DeliveryAddress,
                    SocialIdNumber = entity.SocialIdNumber,
                    AvatarUrl = entity.AvatarUrl
                };

        public static readonly Func<UserProfile, UserProfileViewModel> EntityToViewModelMapper =
            entity => entity == null
                ? null
                : new UserProfileViewModel
                {
                    Id = entity.Id,
                    UserName = entity.UserName,
                    FirstName = entity.FirstName,
                    LastName = entity.LastName,
                    PhoneNumber = entity.PhoneNumber,
                    Email = entity.Email,
                    Address = entity.Address,
                    TotalPoint = entity.TotalPoint,
                    DeliveryAddress = entity.DeliveryAddress,
                    SocialIdNumber = entity.SocialIdNumber,
                    AvatarUrl = entity.AvatarUrl,
                    UserCardCollections = entity.UserCardCollections?.Select(UserCardCollectionDTO.EntityToDtoMapper).ToList(),
                    UserInventories = entity.UserInventories?.Select(UserInventoryDTO.EntityToDtoMapper).ToList(),
                    UserLuckyDrawCodes = entity.UserLuckyDrawCodes?.Select(UserLuckyDrawCodeDTO.EntityToDtoMapper).ToList()
                };

        public static readonly Func<UserProfileViewModel, UserProfileViewModel> SubSetDataMapper = fullVm =>
            fullVm == null
                ? null
                : new UserProfileViewModel
                {
                    Id = fullVm.Id,
                    UserName = fullVm.UserName,
                    FirstName = fullVm.FirstName,
                    LastName = fullVm.LastName,
                    PhoneNumber = fullVm.PhoneNumber,
                    Email = fullVm.Email,
                    Address = fullVm.Address,
                    TotalPoint = fullVm.TotalPoint,
                    HasNoCard = fullVm.TotalCards == 0,
                    DeliveryAddress = fullVm.DeliveryAddress,
                    SocialIdNumber = fullVm.SocialIdNumber,
                    AvatarUrl = fullVm.AvatarUrl
                };
    }
}

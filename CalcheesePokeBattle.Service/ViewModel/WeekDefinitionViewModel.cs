﻿using CalcheesePokeBattle.DTO;

namespace CalcheesePokeBattle.ViewModel
{
    public class WeekDefinitionViewModel : WeekDefinitionDTO
    {
        public string NameDisplay => $"{WeekName} ({WeekStart.Day}/{WeekStart.Month} - {WeekEnd.Day}/{WeekEnd.Month})".ToUpper();

    }
}

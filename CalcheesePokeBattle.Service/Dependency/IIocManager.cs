﻿using System;
using CalcheesePokeBattle.Service;

namespace CalcheesePokeBattle.Dependency
{
    public interface IIocManager
    {
        IServiceFactory ServiceFactory { get; set; }

        TService GetService<TService>();

        /// <summary>
        /// Checks whether given type is registered before.
        /// </summary>
        /// <param name="type">Type to check</param>
        bool IsRegistered(Type type);

        /// <summary>
        /// Checks whether given type is registered before.
        /// </summary>
        /// <typeparam name="T">Type to check</typeparam>
        bool IsRegistered<T>();
    }

    public class IocManager : IIocManager
    {
        public static IIocManager Instance { get; private set; }

        public IServiceFactory ServiceFactory { get; set; }

        static IocManager()
        {
            Instance = new IocManager();
        }

        public TService GetService<TService>()
        {
            return ServiceFactory.GetService<TService>();
        }

        public bool IsRegistered(Type type)
        {
            return ServiceFactory.IsRegistered(type);
        }

        public bool IsRegistered<T>()
        {
            return ServiceFactory.IsRegistered<T>();
        }
    }
}
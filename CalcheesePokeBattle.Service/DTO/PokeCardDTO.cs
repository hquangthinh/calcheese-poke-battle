﻿using System;
using System.Collections.Generic;
using CalcheesePokeBattle.Common.Utils;
using CalcheesePokeBattle.Helper;
using CalcheesePokeBattle.Model;

namespace CalcheesePokeBattle.DTO
{
    public enum PokemonCardType
    {
        Normal,
        Special
    }

    public enum PokemonCardElement
    {
        Electric,
        Water,
        Fire,
        Grass,
        Fairy,
        Normal,
        Dragon,
        Fly,
        Dark,
        Poison,
        Warrior, /* Giác đấu - Fighting */
        Hero /* Siêu năng Psychic */
    }

    public class CardInfo
    {
        public int CardId { get; set; }

        public PokemonCardType CardType { get; set; }

        public List<PokemonCardElement> CardElements { get; set; }

        public string CardMainElement
            => CardElements != null && CardElements.Count > 0 ? CardElements[0].ToString() : string.Empty;

        /// <summary>
        /// This point equals the stars on card use for determine win/loose
        /// if 2 cards are same element
        /// </summary>
        public int CardPoint { get; set; }

        /// <summary>
        /// This point comes from point exchange coupn code -> coupon points and card points
        /// the points used to add or subtract from player points when win or loose game
        /// </summary>
        public int CardValuePoint { get; set; }

        public string CardImageUrl { get; set; }

        public string CardElementImageUrl { get; set; }

        public string CardElementDisplay { get; set; }

        public bool IsSpecialCard { get; set; }

        public string CardTypeDisplay => IsSpecialCard ? "Đặc biệt" : "Thường";

        public int CardStar { get; set; }

        public static readonly Func<PokeCard, string, CardInfo> Mapper = (entity, imageBaseUrl) =>
            entity == null
                ? null
                : new CardInfo
                {
                    CardId = entity.Id,
                    CardType = EnumUtils.FromString(entity.CardType, PokemonCardType.Normal),
                    CardElements = ListHelper.FromValueList(entity.CardElement, PokemonCardElement.Normal),
                    CardPoint = entity.CardPoint,
                    CardStar = entity.CardStar.GetValueOrDefault(entity.CardPoint),
                    CardImageUrl = $"{imageBaseUrl}/{entity.ImageUrl}",
                    CardElementImageUrl = $"{imageBaseUrl}/{entity.CardElement.Replace(",", "_")}.png".ToLower(),
                    CardElementDisplay = PokemonCardHelper.TranslateCardElementForDisplaying(entity.CardElement),
                    IsSpecialCard = PokemonCardType.Special.ToString() == entity.CardType
                                    || GlobalConstants.SpecialPokemonNames.Contains(entity.CardName)
                };
    }

    public class PokeCardDTO
    {
        public int Id { get; set; }

        public string CardName { get; set; }

        public int? CardGroup { get; set; }

        public string CardType { get; set; }

        public string CardElement { get; set; }

        public string CardNumber { get; set; }

        public int CardPoint { get; set; }

        public int? CardStar { get; set; }

        public string ImageUrl { get; set; }

        public string BackSideImageUrl { get; set; }

        public string Category { get; set; }

        public string Height { get; set; }

        public string Weight { get; set; }

        public string Skill1 { get; set; }

        public string Skill2 { get; set; }

        public string Skill3 { get; set; }

        public string UltimateSkill { get; set; }

        public int? EvolutionTo { get; set; }

        public bool IsSpecialCard
            => PokemonCardType.Special.ToString() == CardType
               || GlobalConstants.SpecialPokemonNames.Contains(CardName);

        public virtual ICollection<UserCardCollectionDTO> UserCardCollections { get; set; }

        public static readonly Func<PokeCard, PokeCardDTO> EntityToDtoMapper =
            entity => entity == null
                ? null
                : new PokeCardDTO
                {
                    Id = entity.Id,
                    CardType = entity.CardType,
                    CardElement = entity.CardElement,
                    CardNumber = entity.CardNumber,
                    CardPoint = entity.CardPoint,
                    CardStar = entity.CardStar,
                    ImageUrl = entity.ImageUrl,
                    BackSideImageUrl = entity.BackSideImageUrl,
                    Category = entity.Category,
                    Height = entity.Height,
                    Weight = entity.Weight,
                    Skill1 = entity.Skill1,
                    Skill2 = entity.Skill2,
                    Skill3 = entity.Skill3,
                    UltimateSkill = entity.UltimateSkill,
                    EvolutionTo = entity.EvolutionTo,
                    CardName = entity.CardName,
                    CardGroup = entity.CardGroup
                };

        internal static PokeCard DtoToEntityMapper(PokeCardDTO dto)
        {
            throw new NotImplementedException();
        }
    }
}
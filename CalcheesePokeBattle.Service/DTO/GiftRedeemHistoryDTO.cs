﻿using System;
using System.Collections.Generic;

namespace CalcheesePokeBattle.DTO
{
    public class GiftRedeemHistoryDTO
    {
        public int Id { get; set; }

        public int WeekNumber { get; set; }

        public int GiftId { get; set; }

        public string UserId { get; set; }

        public DateTime RedeemOn { get; set; }

        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
        public string Address { get; set; }
        public string GiftName { get; set; }
        public string AvatarUrl { get; set; }
    }

    public class GiftRedeemHistoryDtoEqualComparer : IEqualityComparer<GiftRedeemHistoryDTO>
    {
        public bool Equals(GiftRedeemHistoryDTO x, GiftRedeemHistoryDTO y)
        {
            if (ReferenceEquals(x, y)) return true;
            if (ReferenceEquals(x, null)) return false;
            if (ReferenceEquals(y, null)) return false;
            if (x.GetType() != y.GetType()) return false;
            return string.Equals(x.UserId, y.UserId) && x.WeekNumber == y.WeekNumber && string.Equals(x.GiftName, y.GiftName);
        }

        public int GetHashCode(GiftRedeemHistoryDTO obj)
        {
            unchecked
            {
                var hashCode = obj.UserId?.GetHashCode() ?? 0;
                hashCode = (hashCode * 397) ^ obj.WeekNumber.GetHashCode();
                hashCode = (hashCode * 397) ^ (obj.GiftName?.GetHashCode() ?? 0);
                return hashCode;
            }
        }
    }
}

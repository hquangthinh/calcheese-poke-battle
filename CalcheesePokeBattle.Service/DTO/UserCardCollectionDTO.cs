﻿using System;
using CalcheesePokeBattle.Model;

namespace CalcheesePokeBattle.DTO
{
    public class UserCardCollectionDTO
    {
        public int Id { get; set; }

        public string UserId { get; set; }

        public int CardId { get; set; }

        public int? CardPoint { get; set; }

        public DateTime? CreatedDate { get; set; }

        public string CouponCode { get; set; }

        public string SourceOfCard { get; set; }

        public virtual PokeCardDTO PokeCard { get; set; }

        public static readonly Func<UserCardCollection, UserCardCollectionDTO> EntityToDtoMapper =
            entity => entity == null
                ? null
                : new UserCardCollectionDTO
                {
                    Id = entity.Id,
                    CardId = entity.CardId,
                    UserId = entity.UserId,
                    CardPoint = entity.CardPoint,
                    CreatedDate = entity.CreatedDate,
                    CouponCode = entity.CouponCode,
                    SourceOfCard = entity.SourceOfCard,
                    PokeCard = PokeCardDTO.EntityToDtoMapper(entity.PokeCard)
                };
    }
}
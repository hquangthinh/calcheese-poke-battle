﻿using System;

namespace CalcheesePokeBattle.DTO
{
    public class UserActivityHistoryDTO : ITrackableDTO
    {
        public int Id { get; set; }

        public string UserName { get; set; }

        public DateTime CreatedAt { get; set; }

        public string Action { get; set; }

        public string ActionDetail { get; set; }

        public string ClientIpAddress { get; set; }

        public string ClientInfo { get; set; }
    }
}
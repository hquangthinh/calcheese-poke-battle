﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CalcheesePokeBattle.Model;

namespace CalcheesePokeBattle.DTO
{
    public class PrizeLuckyDrawHistoryDTO
    {
        public int Id { get; set; }

        public int GiftItemID { get; set; }

        public string GiftDescription  { get; set; }

        public string LuckyDrawCode { get; set; }

        public DateTime CreatedDate { get; set; }

        public int WeekNumber { get; set; }

        public static readonly Func<PrizeLuckyDrawHistoryDTO, PrizeLuckyDrawHistory> DtoToEntityMapper =
           dto => dto == null
               ? null
               : new PrizeLuckyDrawHistory
               {
                   Id = dto.Id,
                   GiftItemID = dto.GiftItemID,
                   LuckyDrawCode = dto.LuckyDrawCode,
                   WeekNumber = dto.WeekNumber,
                   CreatedDate = dto.CreatedDate
               };

        public static readonly Func<PrizeLuckyDrawHistory, PrizeLuckyDrawHistoryDTO> EntityToDtoMapper =
            entity => entity == null
                ? null
                : new PrizeLuckyDrawHistoryDTO()
                {
                    Id = entity.Id,
                    GiftItemID = entity.GiftItemID,
                    GiftDescription = entity.GiftItem.Description,
                    LuckyDrawCode = entity.LuckyDrawCode,
                    WeekNumber = entity.WeekNumber,
                    CreatedDate = entity.CreatedDate
                };
    }
}

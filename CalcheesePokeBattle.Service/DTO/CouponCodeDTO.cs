﻿using System;
using CalcheesePokeBattle.Model;

namespace CalcheesePokeBattle.DTO
{
    public class CouponCodeResult
    {
        public int Point { get; set; }

        public string CardImageUrl { get; set; }

        public bool Success { get; set; }
    }

    public class CouponCodeDTO : ITrackableDTO
    {
        public int Id { get; set; }

        public string Code { get; set; }

        public string Description { get; set; }

        public bool Available { get; set; }

        public int Point { get; set; }

        public int? CardId { get; set; }

        public string CardImageUrl { get; set; }

        public string RedeemByUserName { get; set; }

        public DateTime? RedeemOn { get; set; }

        public string ClientIpAddress { get; set; }

        public string ClientInfo { get; set; }

        public static readonly Func<CouponCode, CouponCodeDTO> EntityToDtoMapper =
            entity => entity == null
                ? null
                : new CouponCodeDTO
                {
                    Id = entity.Id,
                    Code = entity.Code,
                    Description = entity.Description,
                    Available = entity.Available,
                    Point = entity.Point,
                    CardId = entity.CardId,
                    RedeemByUserName = entity.RedeemByUserName,
                    RedeemOn = entity.RedeemOn?.ToLocalTime(),
                    ClientInfo = entity.ClientInfo,
                    ClientIpAddress = entity.ClientIpAddress
                };
    }
}
﻿using System;
using CalcheesePokeBattle.Model;
using CalcheesePokeBattle.ViewModel;

namespace CalcheesePokeBattle.DTO
{
    public class WeekDefinitionDTO
    {
        public int Id { get; set; }

        public int WeekNumber { get; set; }

        public string WeekName { get; set; }

        public DateTime WeekStart { get; set; }

        public DateTime WeekEnd { get; set; }

        public static readonly Func<WeekDefinition, WeekDefinitionDTO> EntityToDtoMapper =
            entity => entity == null
                ? null
                : new WeekDefinitionDTO
                {
                    Id = entity.Id,
                    WeekNumber = entity.WeekNumber,
                    WeekName = entity.WeekName,
                    WeekStart = entity.WeekStart, // no need to convert to local time because configured data is in local time
                    WeekEnd = entity.WeekEnd
                };

        public static readonly Func<WeekDefinition, WeekDefinitionViewModel> EntityToViewModelMapper =
           entity => entity == null
               ? null
               : new WeekDefinitionViewModel
               {
                   Id = entity.Id,
                   WeekNumber = entity.WeekNumber,
                   WeekName = entity.WeekName,
                   WeekStart = entity.WeekStart, // no need to convert to local time because configured data is in local time
                   WeekEnd = entity.WeekEnd
               };
    }
}

﻿namespace CalcheesePokeBattle.DTO
{
    public interface ITrackableDTO
    {
        string ClientIpAddress { get; set; }

        string ClientInfo { get; set; }
    }

    public class BaseTrackableDTO : ITrackableDTO
    {
        public string ClientIpAddress { get; set; }

        public string ClientInfo { get; set; }
    }
}

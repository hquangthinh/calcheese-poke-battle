﻿using System;
using System.Collections.Generic;
using System.Linq;
using CalcheesePokeBattle.Authentication.Models;
using CalcheesePokeBattle.Model;
using Newtonsoft.Json;

namespace CalcheesePokeBattle.DTO
{
    public class UserCanRedeemGiftPermission
    {
        public UserCanRedeemGiftPermission(int userPoint)
        {
            Balo = userPoint >= GlobalConstants.MinimumPointsForPlayerToChangeBackPack;
            AoThun = userPoint >= GlobalConstants.MinimumPointsForPlayerToChangeTShirt;
            Mu = userPoint >= GlobalConstants.MinimumPointsForPlayerToChangeHat;
        }

        // Prop name is name of Gift item
        public bool Balo { get; set; }

        public bool AoThun { get; set; }

        public bool Mu { get; set; }
    }

    public class UserPermissionDTO
    {
        public bool IsSiteAdmin { get; set; }

        /// <summary>
        /// user can access test feature of the site, required role TestUser
        /// </summary>
        public bool CanUserAccessTestFeatures { get; set; }

        /// <summary>
        /// require role LuckyDrawManager
        /// </summary>
        public bool CanUserAccessLuckyDraw { get; set; }
    }

    public class UserProfileDTO
    {
        // property for client used only
        [JsonProperty("_isNew")]
        public bool IsNew => string.IsNullOrEmpty(Id);

        [JsonProperty("canEditUserName")]
        public bool CanEditUserName => IsNew;

        public string Id { get; set; }

        public string UserName { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string FullName
            => string.IsNullOrEmpty(FirstName) && string.IsNullOrEmpty(LastName) ? UserName : $"{FirstName} {LastName}";

        public string PhoneNumber { get; set; }

        public string Address { get; set; }

        public string DeliveryAddress { get; set; }

        public string SocialIdNumber { get; set; }

        public string Email { get; set; }

        public int? TotalPoint { get; set; }

        public int TotalCards { get; set; }

        public bool HasNoCard { get; set; }

        public string AvatarUrl { get; set; }

        /// <summary>
        /// FullAddress calculated from address components below
        /// </summary>
        public string FullAddress { get; set; }

        public string AddressNumber { get; set; }

        public string StreetName { get; set; }

        public string WardName { get; set; }

        public string DistrictName { get; set; }

        public string CityName { get; set; }

        public DateTime? CreatedDate { get; set; }

        // Calculated properties
        public bool UserCanRedeemLuckyCode => TotalPoint >= GlobalConstants.MinimumPointsForPlayerToChangeLuckyCode;

        public bool UserHaveEnoughPointToPlayGame => TotalPoint >= GlobalConstants.MinimumPointsForPlayerToPlayGame;

        // TODO another way to calculate this filter out card trade by playing games
        public int? TotalPointFromExchangeCode => UserCardCollections?.Sum(x => x.CardPoint);

        public int? TotalPointUsedForGiftRedeem
            =>
                UserInventories?.Sum(
                    x => "Mu".Equals(x.Name) ? 60 : "AoThun".Equals(x.Name) ? 80 : "Balo".Equals(x.Name) ? 100 : 0);

        public UserPermissionDTO UserPermission { get; set; }

        public UserCanRedeemGiftPermission UserCanRedeemGift => new UserCanRedeemGiftPermission(TotalPoint.GetValueOrDefault(0));

        public virtual ICollection<RoleDTO> UserRoles { get; set; }

        public virtual ICollection<UserCardCollectionDTO> UserCardCollections { get; set; }

        public virtual ICollection<UserInventoryDTO> UserInventories { get; set; }

        public virtual ICollection<UserLuckyDrawCodeDTO> UserLuckyDrawCodes { get; set; }

        public static readonly Func<UserProfileDTO, UserProfile> DtoToEntityMapper =
            dto => dto == null
                ? null
                : new UserProfile
                {
                    Id = dto.Id,
                    UserName = dto.UserName,
                    FirstName = dto.FirstName,
                    LastName = dto.LastName,
                    PhoneNumber = dto.PhoneNumber,
                    Email = dto.Email,
                    Address = dto.Address,
                    TotalPoint = dto.TotalPoint,
                    DeliveryAddress = dto.DeliveryAddress,
                    SocialIdNumber = dto.SocialIdNumber,
                    AvatarUrl = dto.AvatarUrl,
                    AddressNumber = dto.AddressNumber,
                    StreetName = dto.StreetName,
                    WardName = dto.WardName,
                    DistrictName = dto.DistrictName,
                    CityName = dto.CityName,
                    CreatedDate = dto.CreatedDate
                };

        public static readonly Func<UserProfile, UserProfileDTO> EntityToDtoMapper =
            entity => entity == null
                ? null
                : new UserProfileDTO
                {
                    Id = entity.Id,
                    UserName = entity.UserName,
                    FirstName = entity.FirstName,
                    LastName = entity.LastName,
                    PhoneNumber = entity.PhoneNumber,
                    Email = entity.Email,
                    Address = entity.Address,
                    TotalPoint = entity.TotalPoint,
                    DeliveryAddress = entity.DeliveryAddress,
                    SocialIdNumber = entity.SocialIdNumber,
                    AvatarUrl = entity.AvatarUrl,
                    AddressNumber = entity.AddressNumber,
                    StreetName = entity.StreetName,
                    WardName = entity.WardName,
                    DistrictName = entity.DistrictName,
                    CityName = entity.CityName,
                    CreatedDate = entity.CreatedDate,
                    UserCardCollections = entity.UserCardCollections?.Select(UserCardCollectionDTO.EntityToDtoMapper).ToList(),
                    UserInventories = entity.UserInventories?.Select(UserInventoryDTO.EntityToDtoMapper).ToList(),
                    UserLuckyDrawCodes = entity.UserLuckyDrawCodes?.Select(UserLuckyDrawCodeDTO.EntityToDtoMapper).ToList()
                };

        public static readonly Func<UserProfile, UserProfileDTO> EntityHeaderToDtoMapper =
            entity => entity == null
                ? null
                : new UserProfileDTO
                {
                    Id = entity.Id,
                    UserName = entity.UserName,
                    FirstName = entity.FirstName,
                    LastName = entity.LastName,
                    PhoneNumber = entity.PhoneNumber,
                    Email = entity.Email,
                    Address = entity.Address,
                    TotalPoint = entity.TotalPoint,
                    DeliveryAddress = entity.DeliveryAddress,
                    SocialIdNumber = entity.SocialIdNumber,
                    AvatarUrl = entity.AvatarUrl,
                    AddressNumber = entity.AddressNumber,
                    StreetName = entity.StreetName,
                    WardName = entity.WardName,
                    DistrictName = entity.DistrictName,
                    CityName = entity.CityName,
                    CreatedDate = entity.CreatedDate,
                };
    }
}

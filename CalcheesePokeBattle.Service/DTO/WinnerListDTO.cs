﻿using CalcheesePokeBattle.Model;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CalcheesePokeBattle.DTO
{
    public class WinnerListDTO
    {
        public int Id { get; set; }

        public int WeekNumber { get; set; }

        public DateTime CreatedDate { get; set; }

        public string PriceType { get; set; }

        public string PriceDescription { get; set; }

        public int GiftItemID { get; set; }

        public string Status { get; set; }

        public virtual ICollection<WinnerListUserDTO> WinnerListUsers { get; set; }

        public static readonly Func<WinnerList, WinnerListDTO> EntityToDtoMapper =
           entity => entity == null
               ? null
               : new WinnerListDTO
               {
                   Id = entity.Id,
                   WeekNumber = entity.WeekNumber,
                   PriceType = entity.PriceType,
                   PriceDescription = entity.PriceDescription,
                   GiftItemID = entity.GiftItemID,
                   Status = entity.Status,
                   WinnerListUsers = entity.WinnerListUsers?.Select(WinnerListUserDTO.EntityToDtoMapper).ToList()
               };
    }
}

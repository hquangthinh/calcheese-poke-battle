﻿using CalcheesePokeBattle.Common.Monads;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CalcheesePokeBattle.ViewModel;

namespace CalcheesePokeBattle.DTO
{
    public class CurrentWeekPrizeDataDTO
    {
        //public PagingResult<PrizeLuckyDrawHistoryDTO> Results { get; set; }
        public List<WeekDefinitionViewModel> WeekDefinitionDTOs { get; set; }
        public List<GiftItemDTO> GiftItemDTOs  {get; set; }
        public WeekDefinitionDTO CuurentWeekDefinition { get; set; }
    }
}

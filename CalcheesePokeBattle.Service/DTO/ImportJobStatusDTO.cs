﻿using System;
using CalcheesePokeBattle.Model;

namespace CalcheesePokeBattle.DTO
{
    public class ImportJobStatusDTO
    {
        public int Id { get; set; }

        public string JobName { get; set; }

        public DateTime CreatedDate { get; set; }

        public long TotalImportedCouponCode { get; set; }

        public static readonly Func<ImportJobStatus, ImportJobStatusDTO> EntityMapper = entity
            => entity == null
                ? null
                : new ImportJobStatusDTO
                {
                    Id = entity.Id,
                    CreatedDate = entity.CreatedDate,
                    JobName = entity.JobName,
                    TotalImportedCouponCode = 0
                };
    }
}
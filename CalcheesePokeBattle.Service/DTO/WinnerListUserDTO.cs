﻿using System;
using CalcheesePokeBattle.Model;

namespace CalcheesePokeBattle.DTO
{
    public class WinnerListUserDTO
    {
        public int Id { get; set; }

        public int? WinnerListId { get; set; }

        public string UserId { get; set; }

        public string UserName { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string FullName
            =>
                !string.IsNullOrEmpty(FirstName) && !string.IsNullOrEmpty(LastName)
                    ? $"{FirstName} {LastName}"
                    : UserName;

        public string Email { get; set; }

        public string Address { get; set; }

        public string PhoneNumber { get; set; }

        public string AvatarUrl { get; set; }
		
		public string PriceType { get; set; }

        public string PriceDescription { get; set; }

        public int PrizeSortOrder { get; set; }

        public string LuckyDrawCode { get; set; }

        public string Status { get; set; }

        public bool CanResetPrize => string.IsNullOrEmpty(Status) || Status != GlobalConstants.WinnerListStatusConfirmed;

        public bool CanConfirm => string.IsNullOrEmpty(Status);

        public bool HasConfirmedPrize => GlobalConstants.WinnerListStatusConfirmed.Equals(Status);

        public virtual WinnerListDTO WinnerList { get; set; }


        public static readonly Func<WinnerListUser, WinnerListUserDTO> EntityToDtoMapper =
            entity => entity == null
                ? null
                : new WinnerListUserDTO
                {
                    Id = entity.Id,
                    Email = entity.Email,
                    FirstName = entity.FirstName,
                    LastName = entity.LastName,
                    PhoneNumber = entity.PhoneNumber,
                    Address = entity.Address,
                    UserName = entity.UserName,
                    UserId = entity.UserId,
                    PriceType = entity.WinnerList?.PriceType,
                    PriceDescription = entity.WinnerList?.PriceDescription,
                    PrizeSortOrder = entity.WinnerList?.GiftItemID ?? 0,
                    LuckyDrawCode = entity.LuckyDrawCode,
                    Status = entity.Status
                };
    }
}

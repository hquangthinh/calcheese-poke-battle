﻿using System;
using System.Collections.Generic;
using CalcheesePokeBattle.Model;

namespace CalcheesePokeBattle.DTO
{
    public enum PlayerConnectionStatus
    {
        Online,
        Offline,
        Banned,
        InGame
    }

    public class PlayerConnectionInfoEqualityComparer : IEqualityComparer<PlayerConnectionInfo>
    {
        public bool Equals(PlayerConnectionInfo x, PlayerConnectionInfo y)
        {
            return x.UserName == y.UserName;
        }

        public int GetHashCode(PlayerConnectionInfo obj)
        {
            return obj.Id.GetHashCode();
        }
    }

    public class OnlinePlayerDtoEqualityComparer : IEqualityComparer<OnlinePlayerDTO>
    {
        public bool Equals(OnlinePlayerDTO x, OnlinePlayerDTO y)
        {
            return x.UserName == y.UserName;
        }

        public int GetHashCode(OnlinePlayerDTO obj)
        {
            return obj.Id.GetHashCode();
        }
    }

    public class OnlinePlayerDTO
    {
        public int Id { get; set; }

        public string ServerName { get; set; }

        public string ServerIpAddress { get; set; }

        public string UserId { get; set; }

        public string UserName { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string FullName
            => string.IsNullOrEmpty(FirstName) && string.IsNullOrEmpty(LastName) ? UserName : $"{FirstName} {LastName}";

        public string ConnectionId { get; set; }

        public int TotalPoints { get; set; }

        public DateTime ConnectedAt { get; set; }

        public string AvatarUrl { get; set; }

        public static readonly Func<PlayerConnectionInfo, OnlinePlayerDTO> EntityToDtoMapper =
            entity => entity == null
                ? null
                : new OnlinePlayerDTO
                {
                    Id = entity.Id,
                    ServerName = entity.ServerName,
                    ServerIpAddress = entity.ServerIpAddress,
                    UserId = entity.UserId,
                    UserName = entity.UserName,
                    ConnectionId = entity.ConnectionId,
                    TotalPoints = 0,
                    ConnectedAt = entity.ConnectedAt.ToLocalTime()
                };

        public static readonly Func<AllOnlinePlayerConnectionInfo, OnlinePlayerDTO> ViewEntityToDtoMapper =
            entity => entity == null
                ? null
                : new OnlinePlayerDTO
                {
                    Id = entity.Id,
                    ServerName = entity.ServerName,
                    ServerIpAddress = entity.ServerIpAddress,
                    UserId = entity.UserId,
                    UserName = entity.UserName,
                    FirstName = entity.FirstName,
                    LastName = entity.LastName,
                    ConnectionId = entity.ConnectionId,
                    TotalPoints = entity.TotalPoint,
                    ConnectedAt = entity.ConnectedAt.ToLocalTime(),
                    AvatarUrl = entity.AvatarUrl
                };
    }

    public class PlayerDisconnectInfo : PlayerConnectionInfoDTO
    {
        public List<string> AffectedUserIds { get; set; }
    }

    public class PlayerConnectionInfoDTO
    {
        public int Id { get; set; }

        public string ServerName { get; set; }

        public string ServerIpAddress { get; set; }

        public string UserId { get; set; }

        public string UserName { get; set; }

        public string ConnectionId { get; set; }

        public DateTime ConnectedAt { get; set; }

        public string DeviceInfo { get; set; }

        public string Status { get; set; }

        public string ClientIpAddress { get; set; }

        public static readonly Func<PlayerConnectionInfo, PlayerConnectionInfoDTO> EntityToDtoMapper =
            entity => entity == null
                ? null
                : new PlayerConnectionInfoDTO
                {
                    Id = entity.Id,
                    ServerName = entity.ServerName,
                    ServerIpAddress = entity.ServerIpAddress,
                    UserId = entity.UserId,
                    UserName = entity.UserName,
                    ConnectionId = entity.ConnectionId,
                    ConnectedAt = entity.ConnectedAt.ToLocalTime(),
                    DeviceInfo = entity.DeviceInfo,
                    Status = entity.Status,
                    ClientIpAddress = entity.ClientIpAddress
                };

        public static readonly Func<PlayerConnectionInfoDTO, PlayerConnectionInfo> DtoToEntityMapper =
            dto => dto == null
                ? new PlayerConnectionInfo()
                : new PlayerConnectionInfo
                {
                    Id = dto.Id,
                    ServerName = dto.ServerName,
                    ServerIpAddress = dto.ServerIpAddress,
                    UserId = dto.UserId,
                    UserName = dto.UserName,
                    ConnectionId = dto.ConnectionId,
                    ConnectedAt = dto.ConnectedAt,
                    DeviceInfo = dto.DeviceInfo,
                    Status = dto.Status,
                    ClientIpAddress = dto.ClientIpAddress
                };
    }
}
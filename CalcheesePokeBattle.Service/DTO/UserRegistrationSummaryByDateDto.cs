﻿using System;

namespace CalcheesePokeBattle.DTO
{
    public class UserRegistrationSummaryByDateDTO
    {
        public DateTime? UserCreatedDate { get; set; }
        public int TotalRegistration { get; set; }
    }

    public class SummaryByDateReportDTO
    {
        public DateTime ReportDate { get; set; }
        public int Total { get; set; }
    }
}

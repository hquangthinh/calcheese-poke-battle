﻿using System;
using CalcheesePokeBattle.Model;

namespace CalcheesePokeBattle.DTO
{
    public class UserLuckyDrawCodeDTO
    {
        public int Id { get; set; }

        public string UserId { get; set; }

        public string LuckyDrawCode { get; set; }

        public DateTime CreatedDate { get; set; }

        public int WeekNumber { get; set; }

        public bool IsActive { get; set; }

        public UserProfileDTO UserProfile { get; set; }

        public static readonly Func<UserLuckyDrawCode, UserLuckyDrawCodeDTO> EntityToDtoMapper =
            entity => entity == null
                ? null
                : new UserLuckyDrawCodeDTO
                {
                    Id = entity.Id,
                    UserId = entity.UserId,
                    LuckyDrawCode = entity.LuckyDrawCode,
                    CreatedDate = entity.CreatedDate,
                    WeekNumber = entity.WeekNumber,
                    IsActive = entity.IsActive,
                };
    }
}

﻿using System;

namespace CalcheesePokeBattle.DTO
{
    public class UploadFileDTO
    {
        public int Id { get; set; }
        public int PokeCardId { get; set; }
        public string FileName { get; set; }
        public string CardImageUrl { get; set; }
        public string FileDisplayName { get; set; }
        public string ImportStatus { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
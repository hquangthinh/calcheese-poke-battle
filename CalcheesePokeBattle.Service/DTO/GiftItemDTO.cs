﻿using CalcheesePokeBattle.Model;
using Newtonsoft.Json;
using System;

namespace CalcheesePokeBattle.DTO
{
    public class GiftItemDTO
    {

        // property for client used only
        [JsonProperty("_isNew")]
        public bool IsNew => Id==0;

        public int Id { get; set; }

        public string Name { get; set; }

        public string ImageUrl { get; set; }

        public int RedeemPoint { get; set; }

        public string Description { get; set; }

        public string DescriptionHtml { get; set; }

        public bool IsRedeem { get; set; }

        /// <summary>
        /// calculated fields to check if this gift is allowed to redeem for current week
        /// </summary>
        public bool GiftHasEnoughAvailableAmountForRedeem { get; set; }

        public static readonly Func<GiftItemDTO, GiftItem> DtoToEntityMapper =
          dto => dto == null
              ? null
              : new GiftItem
              {
                  Id = dto.Id,
                  Name = dto.Name,
                  ImageUrl = dto.ImageUrl,
                  Description = dto.Description,
                  RedeemPoint = dto.RedeemPoint,
                  IsRedeem = dto.IsRedeem,
              };

        public static readonly Func<GiftItem, GiftItemDTO> EntityToDtoMapper =
            entity => entity == null
                ? null
                : new GiftItemDTO()
                {
                    Id = entity.Id,
                    Name = entity.Name,
                    ImageUrl = entity.ImageUrl,
                    Description = entity.Description,
                    RedeemPoint = entity.RedeemPoint,
                    IsRedeem = entity.IsRedeem
                };
    }
}

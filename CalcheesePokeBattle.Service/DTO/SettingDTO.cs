﻿using System;
using CalcheesePokeBattle.Model;

namespace CalcheesePokeBattle.DTO
{
    public class SettingDTO
    {
        public string SettingKey { get; set; }

        public string Description { get; set; }

        public string SettingValueString { get; set; }

        public int? SettingValueInt { get; set; }

        public static readonly Func<Setting, SettingDTO> Mapper = entity => entity == null
            ? new SettingDTO()
            : new SettingDTO
            {
                SettingKey = entity.SettingKey,
                Description = entity.Description,
                SettingValueString = entity.SettingValueString,
                SettingValueInt = entity.SettingValueInt
            };
    }
}

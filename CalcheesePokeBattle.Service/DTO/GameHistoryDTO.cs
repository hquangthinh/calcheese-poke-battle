﻿using System;

namespace CalcheesePokeBattle.DTO
{
    public class GameHistoryDTO
    {
        public int Id { get; set; }

        public string UserId1 { get; set; }

        public string UserId2 { get; set; }

        public string WinnerUserId { get; set; }

        public DateTime CreatedDate { get; set; }

        public int? User1Score { get; set; }

        public int? User2Score { get; set; }

        public int? AdjustPoint { get; set; }

        public int? TradeCardId { get; set; }
    }
}

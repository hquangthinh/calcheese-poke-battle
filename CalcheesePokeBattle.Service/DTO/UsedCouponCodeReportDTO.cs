﻿using CalcheesePokeBattle.Model;
using System;

namespace CalcheesePokeBattle.DTO
{
    public class UsedCouponCodeReportDTO
    {
        public int Id { get; set; }

        public string CouponCode { get; set; }

        public DateTime CreatedDate { get; set; }

        public string UserId { get; set; }

        public string UserName { get; set; }

        public string UserFullName { get; set; }

        public string UserPhoneNumber { get; set; }

        public string UserAddress { get; set; }

        public string UserEmail { get; set; }

        public string ClientIpAddress { get; set; }

        public string ClientInfo { get; set; }

        public int? CodePoint { get; set; }

        public int? CardId { get; set; }

        public string CardType { get; set; }

        public string CardName { get; set; }

        public string CardImageUrl { get; set; }


        public static readonly Func<UsedCouponCodeReport, UsedCouponCodeReportDTO> EntityToDtoMapper =
            entity => entity == null
                ? null
                : new UsedCouponCodeReportDTO
                {
                    Id = entity.Id,
                    CouponCode = entity.CouponCode,
                    CreatedDate = entity.CreatedDate,
                    UserName = entity.UserName,
                    UserFullName = entity.UserFullName,
                    UserPhoneNumber = entity.UserPhoneNumber,
                    UserAddress = entity.UserAddress,
                    UserEmail = entity.UserEmail,
                    CodePoint = entity.CodePoint,
                    CardType = entity.CardType,
                    CardName = entity.CardName,
                    CardImageUrl = entity.CardImageUrl
                };
    }

    public class UsedCouponCodeSummaryByDateReportDTO
    {
        public DateTime CreatedDate { get; set; }
        public int TotalInputCard { get; set; }
    }
}

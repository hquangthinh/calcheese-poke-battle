﻿using Newtonsoft.Json;

namespace CalcheesePokeBattle.DTO
{
    public class ResponseDTO
    {
        [JsonProperty("success")]
        public bool Success { get; set; }

        [JsonProperty("error")]
        public ErrorDTO Error { get; set; }

        public static ResponseDTO SuccessResult()
        {
            return new ResponseDTO
            {
                Success = true
            };
        }

        public static ResponseDTO ErrorResult(string errorCode, string errorMessage)
        {
            return new ResponseDTO
            {
                Success = false,
                Error = new ErrorDTO
                {
                    Code = errorCode,
                    Message = errorMessage
                }
            };
        }
    }

    public class ResponseDTO<T>
    {
        [JsonProperty("success")]
        public bool Success { get; set; }

        [JsonProperty("error")]
        public ErrorDTO Error { get; set; }

        [JsonProperty("data")]
        public T Data { get; set; }

        public static ResponseDTO<T> NoDataSuccessResult()
        {
            return new ResponseDTO<T>
            {
                Success = true
            };
        }

        public static ResponseDTO<T> SuccessResult(T data)
        {
            return new ResponseDTO<T>
            {
                Success = true,
                Data = data
            };
        }

        public static ResponseDTO<T> ErrorResult(string errorCode, string errorMessage)
        {
            return new ResponseDTO<T>
            {
                Success = false,
                Error = new ErrorDTO
                {
                    Code = errorCode,
                    Message = errorMessage
                }
            };
        }
    }

    public class ErrorDTO
    {
        [JsonProperty("code")]
        public string Code { get; set; }

        [JsonProperty("message")]
        public string Message { get; set; }
    }

    public class ErrorCode
    {
        public class Redeem
        {
            public const string InvalidOperation = "0001";
            public const string InvalidGift = "0002";
            public const string NoGiftAvailable = "0003";
            public const string NotEnoughtPoints = "0004";
            public const string InvalidLuckyCodeGenerated = "0005";
        }
    }
}
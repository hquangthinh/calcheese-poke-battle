﻿namespace CalcheesePokeBattle.DTO
{
    public class GiftAmountPerWeekDTO
    {
        public int Id { get; set; }

        public int WeekNumber { get; set; }

        public string GiftName { get; set; }

        public int TotalPrize { get; set; }
    }
}

﻿using System;
using CalcheesePokeBattle.Model;

namespace CalcheesePokeBattle.DTO
{
    public class UserInventoryDTO
    {
        public int Id { get; set; }

        public string UserId { get; set; }

        public string Name { get; set; }

        public string ImageUrl { get; set; }

        public static readonly Func<UserInventory, UserInventoryDTO> EntityToDtoMapper =
            entity => entity == null
                ? null
                : new UserInventoryDTO
                {
                    Id = entity.Id,
                    UserId = entity.UserId,
                    Name = entity.Name,
                    ImageUrl = entity.ImageUrl
                };
    }
}

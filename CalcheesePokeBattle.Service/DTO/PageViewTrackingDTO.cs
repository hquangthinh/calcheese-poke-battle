﻿using System;
using System.Collections.Generic;

namespace CalcheesePokeBattle.DTO
{
    public class PageViewTrackingDTO : ITrackableDTO
    {
        public int Id { get; set; }

        public int? WeekNumber { get; set; }

        public string PageTitle { get; set; }

        public string PageUrl { get; set; }

        public DateTime AccessDateTime { get; set; }

        public string ClientIpAddress { get; set; }

        public string ClientInfo { get; set; }

        public string UserName { get; set; }

        public string UserId { get; set; }
    }

    public class PageViewTrackingSummaryByPageDTO
    {

        public string PageTitle { get; set; }

        public string PageUrl { get; set; }

        public int WeekNumber { get; set; }

        public int TotalVisit { get; set; }
    }

    public class PageViewTrackingSummaryByUserDTO : PageViewTrackingSummaryByPageDTO
    {
        public string UserName { get; set; }
    }

    public class PageViewTrackingViewModel
    {
        public PageViewTrackingSummaryByPageDTO PageViewSummary { get; set; }

        public List<PageViewTrackingDTO> PageViewDetail { get; set; }
    }
}

﻿using CalcheesePokeBattle.ViewModel;

namespace CalcheesePokeBattle.Helper
{
    public static class MockDataHelper
    {
        public static GamePlayingPageViewModel GamePlayingPageViewModelForTesting()
        {
            return new GamePlayingPageViewModel();
        }
    }
}
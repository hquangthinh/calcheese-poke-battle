﻿using System.Linq;
using CalcheesePokeBattle.Common.Monads;
using CalcheesePokeBattle.Service;

namespace CalcheesePokeBattle.Helper
{
    public static class UserProfileHelper
    {
        public static string Characters = "abcdefghijklmnopqrstuvxyz!@#$%^&*()_+=|{}[];:'\'\\<>,.?/";

        public static Result ValidateUserCreation(UpdateUserCommand command)
        {
            if (string.IsNullOrEmpty(command.UserName))
                return Result.Fail("Tên đăng nhập là bắt buộc");

            if (string.IsNullOrEmpty(command.Password))
                return Result.Fail("Mật khẩu là bắt buộc");

            if (!command.Password.Equals(command.PasswordConfirmation))
                return Result.Fail("Mật khẩu xác nhận không trùng khớp với mật khẩu ban đầu");

            // check phone number
            if (string.IsNullOrEmpty(command.PhoneNumber))
                return Result.Fail("Số điện thoại là bắt buộc");

            if (command.PhoneNumber.Length > 11)
                return Result.Fail("số điện thoại không hợp lệ");

            if(Characters.ToList().Any(command.PhoneNumber.ToLower().Contains))
                return Result.Fail("số điện thoại không hợp lệ");

            return Result.Success();
        }
    }
}

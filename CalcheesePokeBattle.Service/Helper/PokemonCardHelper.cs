﻿namespace CalcheesePokeBattle.Helper
{
    public static class PokemonCardHelper
    {
        public static string TranslateCardElementForDisplaying(string cardElement)
        {
            if (string.IsNullOrEmpty(cardElement))
                return string.Empty;

            return cardElement
                .Replace("Electric", "Điện")
                .Replace("Water", "Nước")
                .Replace("Fire", "Lửa")
                .Replace("Grass", "Cỏ")
                .Replace("Normal", "Thường")
                .Replace("Poison", "Độc")
                .Replace("Dragon", "Rồng")
                .Replace("Fly", "Bay")
                .Replace("Fairy", "Tiên")
                .Replace("Dark", "Bóng Tối")
                .Replace(",", "/");
        }

        public static string TranslateMainCardElementForDisplaying(string cardElement)
        {
            if (string.IsNullOrEmpty(cardElement))
                return string.Empty;

            return cardElement
                .Replace("Electric", "Điện")
                .Replace("Water", "Nước")
                .Replace("Fire", "Lửa")
                .Replace("Grass", "Cỏ")
                .Replace("Normal", "")
                .Replace("Poison", "")
                .Replace("Dragon", "")
                .Replace("Fly", "")
                .Replace("Fairy", "Tiên")
                .Replace("Dark", "Bóng Tối")
                .TrimEnd(',');
        }
    }
}

﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace CalcheesePokeBattle
{
    public static class GlobalConstants
    {
        public const int GameTimeOutPerRound = 10;

        public const int MinimumPointsForPlayerToPlayGame = 7;

        public const int MinimumPointsForPlayerToChangeLuckyCode = 10;

        public const int MinimumPointsForPlayerToChangeBackPack = 100;

        public const int MinimumPointsForPlayerToChangeHat = 60;

        public const int MinimumPointsForPlayerToChangeTShirt = 80;

        public const string WinnerListStatusConfirmed = "Confirmed";

        public const string PokeCardGamePageTitle = "san-choi";
        public const string JoinGamePageTitle = "tham-gia-san-choi";
        public const string PlayGamePageTitle = "choi-game";

        public static ReadOnlyCollection<string> SpecialPokemonNames = new ReadOnlyCollection<string>(
            new List<string>
            {
                "Mega Denryu",
                "Mega Fushigibana",
                "Mega Kamex",
                "Mega Rizadon X",
                "Mega Rizadon Y",
                "Zeruneasu",
                "Iberutaru"
            }
        );

        public static ReadOnlyCollection<string> GiftNamesForRedeem = new ReadOnlyCollection<string>(
            new List<string>
            {
                "Balo",
                "AoThun",
                "Mu"
            }
        );

        public static ReadOnlyCollection<string> GiftNamesForLuckyDraw = new ReadOnlyCollection<string>(
            new List<string>
            {
                "TraiHe",
                "XeDien",
                "DoChoi",
                "XeDap",
                "VinID"
            }
        );
    }

    public static class GlobalErrorMessage
    {
        public const string ExpectNonNullParameters = "Tham số không hợp lệ";
        public const string NeedAtleastSevenPointsToPlay = "Bạn cần ít nhất 7 điểm để giao đấu";
        public const string NotEnoughCardToPlay = "Bạn không có đủ thẻ để chơi";
        public const string InvalidUser = "Người chơi không hợp lệ";
        public const string InvalidInvitedUser = "Đối thủ không hợp lệ";
        public const string InvitedUserDoesNotHaveEnoughPoints = "Đối thủ không có đủ điểm để chơi";
        public const string InvitedUserDoesIsPlayingWithOther = "{0} đang bận thi đấu.";
        public const string InvitedUserDoesNotHaveEnoughCards = "Đối thủ không có đủ thẻ để chơi";
    }
}
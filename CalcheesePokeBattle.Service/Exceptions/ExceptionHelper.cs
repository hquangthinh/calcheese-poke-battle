﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.Linq;
using CalcheesePokeBattle.Common.Extensions;

namespace CalcheesePokeBattle.Exceptions
{
    public enum ErrorPresentationStyle
    {
        Suppress,
        Aggregated,
        Warning,
        Error
    }

    public class ExceptionInfo
    {
        public string ErrorMessage { get; private set; }
        public Exception[] Exceptions { get; private set; }
        public ErrorPresentationStyle PresentationStyle { get; private set; }

        public ExceptionInfo(string errorMessage, ErrorPresentationStyle presentationStyle, params Exception[] exceptions)
        {
            ErrorMessage = errorMessage;
            Exceptions = exceptions;
            PresentationStyle = presentationStyle;
        }
    }

    public class ExceptionHelper
    {
        public static Exception GetInnerMostException(Exception ex)
        {
            var rootEx = ex;
            while (rootEx.InnerException != null)
            {
                rootEx = rootEx.InnerException;
            }
            return rootEx;
        }

        public static ExceptionInfo GetExeptionDetails(Exception ex)
        {
            if (ex is ApplicationException
                || ex is BusinessException
                || ex is AppNotificationException
                || ex is InvalidOperationException
                || ex is ArgumentException)
            {
                return new ExceptionInfo(ex.Message, ErrorPresentationStyle.Warning, ex);
            }
            if (ex is AggregateException)
            {
                var e = (AggregateException)ex;
                return new ExceptionInfo(e.Message, ErrorPresentationStyle.Aggregated, ProcessAggregateException(e).ToArray());
            }
            if (ex is DbEntityValidationException)
            {
                var e = (DbEntityValidationException)ex;
                return new ExceptionInfo(GetErrorMessageFor(e), ErrorPresentationStyle.Warning, ex);
            }
            if (ex is DbUpdateException)
            {
                var e = (DbUpdateException)ex;
                return new ExceptionInfo(GetErrorMessageFor(e), ErrorPresentationStyle.Warning, GetInnerMostException(ex));
            }
            if (ex.InnerException is DbEntityValidationException)
            {
                var e = (DbEntityValidationException)ex.InnerException;
                return new ExceptionInfo(GetErrorMessageFor(e), ErrorPresentationStyle.Warning, e);
            }
            if (ex is EntityCommandExecutionException)
            {
                var e = (EntityCommandExecutionException)ex;
                return new ExceptionInfo(GetErrorMessageFor(e), ErrorPresentationStyle.Warning, GetInnerMostException(e));
            }

            return new ExceptionInfo(GetErrorMessageFor(ex), ErrorPresentationStyle.Error, GetInnerMostException(ex));
        }

        public static string GetErrorMessageFor(DbEntityValidationException ex)
        {
            return string.Join("\r\n",
                ex.EntityValidationErrors.SelectMany(
                    e => e.ValidationErrors.Select(ve => ve.PropertyName + ": " + ve.ErrorMessage))
            );
        }

        public static string GetErrorMessageFor(Exception ex)
        {
            var rootEx = GetInnerMostException(ex);
            var errorMsg = rootEx.Message;
            return errorMsg;
        }

        public static string GetErrorMessageFor(EntityCommandExecutionException ex)
        {
            var rootEx = GetInnerMostException(ex);
            string errorMsg;
            var exception = rootEx as SqlException;
            if (exception != null)
            {
                errorMsg = exception.Message + ". Procedure: " + exception.Procedure;
            }
            else
            {
                errorMsg = rootEx.Message;
            }
            return errorMsg;
        }

        private static List<Exception> ProcessAggregateException(AggregateException ex, List<Exception> agg = null)
        {
            if (agg == null) agg = new List<Exception>();
            var innerExceptions = ex.InnerExceptions;

            foreach (var e in innerExceptions)
            {
                var exception = e as AggregateException;
                if (exception == null)
                {
                    agg.Add(e.GetInnerMostException());
                }
                else
                {
                    ProcessAggregateException(exception, agg);
                }
            }
            return agg;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalcheesePokeBattle.Exceptions
{
    public class ResourceModifiedException : ApplicationException
    {
        public string ResourceName { get; }
        public ResourceModifiedException(string resourceName)
        {
            ResourceName = resourceName;
        }

        public override string Message => $"{ResourceName} has been modified since the last time it was loaded.";
    }
}

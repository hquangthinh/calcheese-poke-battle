﻿using System;
using System.Runtime.Serialization;

namespace CalcheesePokeBattle.Exceptions
{
    public class AppNotificationException : Exception
    {
        public AppNotificationException()
        {
        }

        public AppNotificationException(string message) : base(message)
        {
        }

        public AppNotificationException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected AppNotificationException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}

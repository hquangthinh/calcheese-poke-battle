﻿using System;
using System.Data.Entity;
using CalcheesePokeBattle.Common.DTO;
using CalcheesePokeBattle.Common.Monads;
using CalcheesePokeBattle.Common.Utils;
using System.Collections.Generic;
using System.Linq;
using CalcheesePokeBattle.Model;
using CalcheesePokeBattle.ViewModel;
using CalcheesePokeBattle.DTO;

namespace CalcheesePokeBattle.Service
{
    public class SearchGiftRedeemHistoryCommand : KeywordSearchCommand
    {
        public int WeekNumber { get; set; }
        public string GiftName { get; set; }
    }


    /// <summary>
    /// Service to manage winner list
    /// </summary>
    public interface IWinnerListService
    {
        CurrentWeekViewModel GetWeeekDefinitions();

        IEnumerable<WinnerListDTO> GetWinnerListForPublicView(int weekNumber);

        IEnumerable<WinnerListDTO> GetWinnerList(int weekNumber);

        IEnumerable<WinnerListUserDTO> GetWinnerUserList(SearchPrizeLuckyDrawHistoryCommand command);

        PagingResult<GiftRedeemHistoryDTO> GetRedeemList(SearchGiftRedeemHistoryCommand command);
    }

    public class DefaultWinnerListService : DefaultServiceBase, IWinnerListService
    {
        public DefaultWinnerListService(IDbFactory dbFactory) : base(dbFactory)
        {
        }

        public CurrentWeekViewModel GetWeeekDefinitions()
        {
            var models = DataContext.WeekDefinitions.OrderBy(x => x.WeekNumber).Select(WeekDefinitionDTO.EntityToViewModelMapper).ToList();
            WeekDefinitionDTO weekDto;
            try
            {
                weekDto = GetCurrentWeekForToday();
            }
            catch (Exception)
            {
                // ignored
                return new CurrentWeekViewModel
                {
                    CuurentWeekDefinition = new WeekDefinitionDTO {WeekNumber = 12},
                    WeekDefinitionViewModels = models
                };
            }
            return new CurrentWeekViewModel
            {
                CuurentWeekDefinition = weekDto,
                WeekDefinitionViewModels = models
            };
        }

        public IEnumerable<WinnerListDTO> GetWinnerListForPublicView(int weekNumber)
        {
            var shouldShowWinnerList = GetService<ISettingService>().ShouldShowWinnerListForPublicView(weekNumber);

            if (!shouldShowWinnerList)
                return new List<WinnerListDTO>();

            return GetWinnerList(weekNumber);
        }

        public IEnumerable<WinnerListDTO> GetWinnerList(int weekNumber)
        {
            var luckyDrawWinnerListOfTheWeek =
                DataContext.WinnerLists.Include(w => w.WinnerListUsers)
                    .Where(w => w.WeekNumber == weekNumber)
                    .Select(WinnerListDTO.EntityToDtoMapper)
                    .ToList();

            var allWinnerUsers = luckyDrawWinnerListOfTheWeek.SelectMany(x => x.WinnerListUsers).ToList();

            var winnerUserIds = allWinnerUsers.Select(x => x.UserId);

            var defaultAvatarUrl = "Content/images/avatars/no_avatar_120x120.png";
            var winnerAvatarMap = DataContext.UserProfiles.Where(p => winnerUserIds.Contains(p.Id))
                .ToDictionary(p => p.Id, p => p.AvatarUrl ?? defaultAvatarUrl);

            allWinnerUsers.ForEach(
                winUser =>
                    winUser.AvatarUrl =
                        winnerAvatarMap.ContainsKey(winUser.UserId) ? winnerAvatarMap[winUser.UserId] : defaultAvatarUrl);

            return luckyDrawWinnerListOfTheWeek;
        }

        public IEnumerable<WinnerListUserDTO> GetWinnerUserList(SearchPrizeLuckyDrawHistoryCommand command)
        {
            string giftItem = "";
            if (command.Prize > 0)
            {
                if (command.Prize == 1) giftItem = "TraiHe";
                else if (command.Prize == 3) giftItem = "DoChoi";
                else if (command.Prize == 2)
                {
                    //chan XeDien le XeDap(VinID)
                    giftItem = command.WeekNumber % 2 == 0 ? "XeDien" : "VinID";
                }
            }

            var predicate = Functions.Lambda<WinnerList, bool>(
                prize => (
                    (string.IsNullOrEmpty(giftItem) || giftItem == prize.PriceType) &&
                    (command.WeekNumber == 0 || prize.WeekNumber == command.WeekNumber)
                ));

            var winnerList = from w in DataContext.WinnerLists.Where(predicate)
                join u in DataContext.WinnerListUsers on w.Id equals u.WinnerListId
                orderby w.GiftItemID ascending 
                orderby w.Id descending 
                select u;

            return winnerList.ToList()
                .Select(w => WinnerListUserDTO.EntityToDtoMapper(w))
                .OrderBy(w => w.PrizeSortOrder);
        }


        /// <summary>
        /// Get Balo
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        public PagingResult<GiftRedeemHistoryDTO> GetRedeemList(SearchGiftRedeemHistoryCommand command)
        {
            var gift = DataContext.GiftItems.FirstOrDefault(g => g.Name == command.GiftName);
            if (command == null || gift == null)
                return PagingResult<GiftRedeemHistoryDTO>.Empty();

            var predicate =
                Functions.Lambda<GiftRedeemHistory, bool>(
                    g => (g.WeekNumber == command.WeekNumber &&
                                    g.GiftId == gift.Id
                                ));

            var queryable = from h in DataContext.GiftRedeemHistories.Where(predicate)
                join u in DataContext.UserProfiles on h.UserId equals u.Id
                select new GiftRedeemHistoryDTO
                {
                    //Id = h.Id,
                    Email = u.Email,
                    FirstName = u.FirstName,
                    LastName = u.LastName,
                    PhoneNumber = u.PhoneNumber,
                    Address = u.Address,
                    GiftName = gift.Name,
                    GiftId = gift.Id,
                    UserId = u.Id,
                    WeekNumber = h.WeekNumber
                };

            var totalResult = queryable.OrderBy(q => q.UserId).ToList()
                .Distinct(new GiftRedeemHistoryDtoEqualComparer()).ToList();

            var totalCount = totalResult.Count;

            var result = totalResult.Paginate(command).ToList();

            var userIds = result.Select(u => u.UserId).ToList();

            var defaultAvatarUrl = "Content/images/avatars/no_avatar_120x120.png";
            var winnerAvatarMap = DataContext.UserProfiles.Where(p => userIds.Contains(p.Id))
                .ToDictionary(p => p.Id, p => p.AvatarUrl ?? defaultAvatarUrl);

            result.ForEach(
                info =>
                    info.AvatarUrl =
                        winnerAvatarMap.ContainsKey(info.UserId) ? winnerAvatarMap[info.UserId] : defaultAvatarUrl);

            return new PagingResult<GiftRedeemHistoryDTO>(result)
            {
                PageSize = command.PageSize,
                PageNumber = command.PageNumber,
                PageTotal = PagingHelper.GetPageTotal(totalCount, command.PageSize),
                ResultCount = totalCount
            };
        }
    }
}
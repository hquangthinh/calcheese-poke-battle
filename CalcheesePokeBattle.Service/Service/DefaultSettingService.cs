﻿using System;
using System.Collections.Generic;
using System.Linq;
using CalcheesePokeBattle.Common.Monads;
using CalcheesePokeBattle.Common.Utils;
using CalcheesePokeBattle.DTO;
using CalcheesePokeBattle.Model;
using F = CalcheesePokeBattle.Common.Utils.Functions;

namespace CalcheesePokeBattle.Service
{
    public class DefaultSettingService: DefaultServiceBase, ISettingService
    {
        public DefaultSettingService(IDbFactory dbFactory) : base(dbFactory)
        {
        }

        public List<SettingDTO> GetAll()
        {
            return DataContext.Settings.Select(SettingDTO.Mapper).ToList();
        }

        public string GetStringValue(SettingKeys setting)
        {
            return GetStringValue(setting, string.Empty);
        }

        public string GetStringValue(SettingKeys setting, string defaultValue)
        {
            return GetStringValueCache(this.AsCacheContext())(setting, defaultValue);
        }

        private static readonly Func<CacheCommand<DefaultSettingService>, Func<SettingKeys, string, string>>
            GetStringValueCache
                = F.Lambda(
                        (DefaultSettingService ctx, SettingKeys setting, string defaultValue) =>
                            ctx.GetStringValueInternal(setting, defaultValue))
                    .MemoizeCtx(CacheConfig.Default);

        private static readonly Func<CacheCommand<DefaultSettingService>, Func<SettingKeys, int>>
            GetIntValueCache
                = F.Lambda(
                        (DefaultSettingService ctx, SettingKeys setting) =>
                            ctx.GetIntValueInternal(setting))
                    .MemoizeCtx(CacheConfig.Default);

        private string GetStringValueInternal(SettingKeys setting, string defaultValue)
        {
            var settingEntity = DataContext.Settings.FirstOrDefault(s => s.SettingKey == setting.ToString());
            return settingEntity?.SettingValueString ?? defaultValue;
        }

        private int GetIntValueInternal(SettingKeys setting)
        {
            var settingEntity = DataContext.Settings.FirstOrDefault(s => s.SettingKey == setting.ToString());
            return settingEntity?.SettingValueInt ?? 0;
        }

        public int GetIntValue(SettingKeys setting)
        {
            return GetIntValueCache(this.AsCacheContext())(setting);
        }

        public bool IsAffirmative(SettingKeys setting)
        {
            var settingValue = GetStringValue(setting, string.Empty).ToLower();

            return IsAffirmativeInternal(settingValue);
        }

        private bool IsAffirmativeInternal(string settingValue)
        {
            return "yes".Equals(settingValue) || "ok".Equals(settingValue) || "true".Equals(settingValue) ||
                   "1".Equals(settingValue);
        }

        public Result ResetSettingCache()
        {
            GetStringValueCache(this.AsCacheContext().Reset());
            GetIntValueCache(this.AsCacheContext().Reset());
            return Result.Success();
        }

        public Result<WeekDefinitionDTO> GetWeekFromDate(DateTime inputDateTime)
        {
            // base on week definition start and end -> week number, do not factor time
            var dtToCheck = new DateTime(inputDateTime.Year, inputDateTime.Month, inputDateTime.Day);
            var weekDef =
                DataContext.WeekDefinitions.FirstOrDefault(w => w.WeekStart <= dtToCheck && dtToCheck <= w.WeekEnd);
            return weekDef != null
                ? Result.Success(WeekDefinitionDTO.EntityToDtoMapper(weekDef))
                : Result.Fail<WeekDefinitionDTO>("Thời gian khuyến mãi đã hết.");
        }

        public Result<WeekDefinitionDTO> GetWeekFromCurrentDate()
        {
            var vnLocalNow = DateTime.Now.AddHours(7);
            return GetWeekFromDate(vnLocalNow);
        }

        /// <summary>
        /// Get total available amount gift for a week
        /// </summary>
        /// <param name="weekNumber"></param>
        /// <param name="giftKey"></param>
        /// <returns></returns>
        public Result<GiftAmountPerWeek> GetTotalGiftAvailableForWeek(int weekNumber, string giftKey)
            => GetTotalGiftAvailableForWeekCache(this.AsCacheContext())(weekNumber, giftKey);

        public static readonly Func<CacheCommand<DefaultSettingService>, Func<int, string, Result<GiftAmountPerWeek>>>
            GetTotalGiftAvailableForWeekCache =
                Functions.Lambda(
                    (DefaultSettingService ctx, int weekNumber, string giftKey) =>
                        ctx.GetTotalGiftAvailableForWeekInternal(weekNumber, giftKey)).MemoizeCtx();

        private Result<GiftAmountPerWeek> GetTotalGiftAvailableForWeekInternal(int weekNumber, string giftKey)
        {
            var giftConfig = DataContext.GiftAmountPerWeeks.FirstOrDefault(item => item.WeekNumber == weekNumber && item.GiftName == giftKey);
            return giftConfig != null
                ? Result.Success(giftConfig)
                : Result.Success(new GiftAmountPerWeek());
        }

        public Result UpdateSettingValue(SettingKeys settingKey, string value)
        {
            var setting = DataContext.Settings.FirstOrDefault(s => s.SettingKey == settingKey.ToString());

            if(setting == null)
                return Result.Fail("Invalid setting");

            setting.SettingValueString = value;
            SaveChanges("Setting");

            return Result.Success();
        }

        public Result UpdateShowLuckyDrawPrizeOnFrontSiteForWeek(int weekNumber)
        {
            var settingKey = $"ShowLuckyDrawPrizeOnFrontSiteWeek{weekNumber}";

            var keyEnum = EnumUtils.FromString<SettingKeys>(settingKey);

            if (!keyEnum.HasValue)
                return Result.Fail("Invalid setting");

            return UpdateSettingValue(keyEnum.Value, "yes");
        }

        public Result UpdateHideLuckyDrawPrizeOnFrontSiteForWeek(int weekNumber)
        {
            var settingKey = $"ShowLuckyDrawPrizeOnFrontSiteWeek{weekNumber}";

            var keyEnum = EnumUtils.FromString<SettingKeys>(settingKey);

            if (!keyEnum.HasValue)
                return Result.Fail("Invalid setting");

            return UpdateSettingValue(keyEnum.Value, "no");
        }

        public bool ShouldShowWinnerListForPublicView(int weekNumber)
        {
            var settingKey = $"ShowLuckyDrawPrizeOnFrontSiteWeek{weekNumber}";
            var setting = DataContext.Settings.FirstOrDefault(s => s.SettingKey == settingKey);
            return setting != null && IsAffirmativeInternal(setting.SettingValueString);
        }
    }
}

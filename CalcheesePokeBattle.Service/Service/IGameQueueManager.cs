﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CalcheesePokeBattle.DTO;

namespace CalcheesePokeBattle.Service
{
    /// <summary>
    /// Manage game queue used for matching game for players send find random opponents
    /// </summary>
    public interface IGameQueueManager
    {
        PlayerConnectionInfoDTO TryPeekPlayer();

        PlayerConnectionInfoDTO TryDequeuePlayer();

        void EnqueuePlayer(PlayerConnectionInfoDTO info);
    }

    public class DefaultGameQueueManager : IGameQueueManager
    {
        private static readonly ConcurrentQueue<PlayerConnectionInfoDTO> GameQueue = new ConcurrentQueue<PlayerConnectionInfoDTO>();

        public PlayerConnectionInfoDTO TryPeekPlayer()
        {
            PlayerConnectionInfoDTO info;
            if (GameQueue.TryPeek(out info))
                return info;
            return null;
        }

        public PlayerConnectionInfoDTO TryDequeuePlayer()
        {
            PlayerConnectionInfoDTO info;
            if (GameQueue.TryDequeue(out info))
                return info;
            return null;
        }

        public void EnqueuePlayer(PlayerConnectionInfoDTO info)
        {
            if (info == null)
                return; 
            GameQueue.Enqueue(info);
        }
    }
}

﻿using CalcheesePokeBattle.Common.DTO;
using CalcheesePokeBattle.Common.Monads;
using CalcheesePokeBattle.Common.Timing;
using CalcheesePokeBattle.Common.Utils;
using CalcheesePokeBattle.DTO;
using CalcheesePokeBattle.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalcheesePokeBattle.Service
{

    public class SearchPrizeLuckyDrawHistoryCommand : KeywordSearchCommand
    {
        public int WeekNumber { get; set; }
        public DateTime? Today { get; set; }
        public string LuckyDrawCode { get; set; }
        public int Prize { get; set; }
    }

    public class PrizeLuckyDrawHistoryCommandBase
    {
        public int Id { get; set; }
        public string WeekNumber { get; set; }
        public string LuckyDrawCode { get; set; }
    }

    public class UpdatePrizeLuckyDrawHistoryCommand : PrizeLuckyDrawHistoryDTO
    {
    }

    public class SetPrizeCommand : ITrackableDTO
    {
        public int WeekNumber;
        public int Prize;
        public string UserName { get; set; }
        public string ClientIpAddress { get; set; }
        public string ClientInfo { get; set; }
    }

    public class ConfirmPrizeCommand : SetPrizeCommand
    {
        /*
            4	TraiHe
            5	VinID
            6	XeDien
            7	DoChoi
         */
        public int WinnerListUserId { get; set; } // record id of WinnerListUser

        public string UserId { get; set; }

        public string PrizeType { get; set; } // name of the gift item: TraiHe, VinID, DoChoi, XeDien

        public string LuckyDrawCode { get; set; }

        public bool ConfirmPrize { get; set; }

        public bool ResetPrize { get; set; }
    }

    public class ExportResultCommand
    {
        public int WeekNumber { get; set; }
        public string Format { get; set; } //default to excel
        public string Type { get; set; } // candidate, winner,...
    }

    public interface IPrizeLuckyDrawHistoryService
    {
        Result<PrizeLuckyDrawHistoryDTO> CreatePrizeLuckyDrawHistory(PrizeLuckyDrawHistoryDTO dto);
        PagingResult<PrizeLuckyDrawHistoryDTO> SearchPrizeLuckyDrawHistory(SearchPrizeLuckyDrawHistoryCommand command);
        Result<PrizeLuckyDrawHistoryDTO> Update(UpdatePrizeLuckyDrawHistoryCommand command);
        Result<PrizeLuckyDrawHistoryDTO> Delete(PrizeLuckyDrawHistoryCommandBase command);
        void ProcessPrize(int weekNumber);
        Result<WinnerListUserDTO> SetPrize(SetPrizeCommand command);
        Result ConfirmPrize(ConfirmPrizeCommand command);
        Result ResetPrize(ConfirmPrizeCommand command);
    }
    public class PrizeLuckyDrawHistoryService : DefaultServiceBase, IPrizeLuckyDrawHistoryService
    {
        public PrizeLuckyDrawHistoryService(IDbFactory dbFactory) : base(dbFactory)
        {
        }

        public Result<PrizeLuckyDrawHistoryDTO> CreatePrizeLuckyDrawHistory(PrizeLuckyDrawHistoryDTO dto)
        {
            DataContext.PrizeLuckyDrawHistories.Add(PrizeLuckyDrawHistoryDTO.DtoToEntityMapper(dto));
            SaveChanges("PrizeLuckyDrawHistories", dto.Id.ToString());
            return Result.Success(dto);
        }

        public Result<PrizeLuckyDrawHistoryDTO> Delete(PrizeLuckyDrawHistoryCommandBase command)
        {
            var deleteEntity = DataContext.PrizeLuckyDrawHistories.FirstOrDefault(p => p.Id == command.Id);
            if (deleteEntity != null)
            {
                DataContext.PrizeLuckyDrawHistories.Remove(deleteEntity);
                DataContext.SaveChanges();
            }
            return null;
        }

        public PagingResult<PrizeLuckyDrawHistoryDTO> SearchPrizeLuckyDrawHistory(SearchPrizeLuckyDrawHistoryCommand command)
        {
            if (command == null)
                return PagingResult<PrizeLuckyDrawHistoryDTO>.Empty();
            return Timer.Time(() =>
            {
                var predicate =
                Functions.Lambda<PrizeLuckyDrawHistory, bool>(
                    prize => (
                                    (string.IsNullOrEmpty(command.LuckyDrawCode) || prize.LuckyDrawCode == command.LuckyDrawCode) &&
                                    (command.WeekNumber == 0 || prize.WeekNumber == command.WeekNumber)
                                ));

                var totalCount = DataContext.PrizeLuckyDrawHistories.Count(predicate);
                var result = DataContext.PrizeLuckyDrawHistories.Include(p => p.GiftItem).Where(predicate).Select(PrizeLuckyDrawHistoryDTO.EntityToDtoMapper).ToList();

                return new PagingResult<PrizeLuckyDrawHistoryDTO>(result)
                {
                    PageSize = int.MaxValue,
                    PageNumber = 1,
                    PageTotal = PagingHelper.GetPageTotal(totalCount, int.MaxValue),
                    ResultCount = totalCount
                };
            });
        }

        public Result<PrizeLuckyDrawHistoryDTO> Update(UpdatePrizeLuckyDrawHistoryCommand command)
        {
            if (command == null)
                return Result.Fail<PrizeLuckyDrawHistoryDTO>("Expect input data");
            var prizeLuckyDrawHistory = new PrizeLuckyDrawHistory();

            prizeLuckyDrawHistory.LuckyDrawCode = command.LuckyDrawCode;
            prizeLuckyDrawHistory.WeekNumber = command.WeekNumber;
            prizeLuckyDrawHistory.GiftItemID = command.GiftItemID;
            prizeLuckyDrawHistory.CreatedDate = DateTime.Now;
            DataContext.PrizeLuckyDrawHistories.Add(prizeLuckyDrawHistory);
            SaveChanges("PrizeLuckyDrawHistory", command.Id.ToString());
            var rs = DataContext.PrizeLuckyDrawHistories.Include(p => p.GiftItem).FirstOrDefault();
            return Result.Success(PrizeLuckyDrawHistoryDTO.EntityToDtoMapper(rs));
        }

        /// <summary>
        /// Process Prize current week obsolet .....
        /// </summary>
        /// <param name="weekNumber"></param>
        public void ProcessPrize(int weekNumber)
        {
            if (weekNumber == 0) return;
            var gifts = DataContext.GiftItems.Where(g => !g.IsRedeem).ToList();
            //Setup Winnerlist for luckydraw current week
            foreach (var gift in gifts)
            {
                if (!DataContext.WinnerLists.Any(w => w.WeekNumber == weekNumber && w.PriceType == gift.Name))
                {
                    var winnerList = new WinnerList();
                    winnerList.WeekNumber = weekNumber;
                    winnerList.PriceType = gift.Name;
                    winnerList.CreatedDate = DateTime.Now;
                    winnerList.GiftItemID = gift.Id;
                    DataContext.WinnerLists.Add(winnerList);
                }

            }

            DataContext.SaveChanges();

            //Get list prize history for current week
            //var listPrizes = DataContext.PrizeLuckyDrawHistories.Where(p => p.WeekNumber == weekNumber);

            var prizeUsers = from userLucky in DataContext.UserLuckyDrawCodes.Include(l=>l.UserProfile)
                             join prizeHistory in DataContext.PrizeLuckyDrawHistories
                             on new { userLucky.LuckyDrawCode, userLucky.WeekNumber } equals new { prizeHistory.LuckyDrawCode, prizeHistory.WeekNumber }
                             where userLucky.WeekNumber == weekNumber
                             select new { userLucky, prizeHistory };

            //Remove all giftwon
            var giftWonCurrentWeek = DataContext.GiftWonFromLuckyDraws.Where(w => w.WeekNumber == weekNumber).ToList();
            DataContext.GiftWonFromLuckyDraws.RemoveRange(giftWonCurrentWeek);

            //Remove all winner list uer
            var winnerLists = DataContext.WinnerLists.Include(ww => ww.WinnerListUsers).Where(w => w.WeekNumber == weekNumber).ToList();
            foreach (var w in winnerLists)
            {
                DataContext.Database.ExecuteSqlCommand("DELETE FROM dbo.WinnerListUser Where WinnerListId = {0}", w.Id);
            }
            
            //Add
            foreach (var prizeUser in prizeUsers)
            {
                //Insert GiftWonFromLuckyDraw
                var giftWon = new GiftWonFromLuckyDraw();
                giftWon.UserId = prizeUser.userLucky.UserId;
                giftWon.GiftId = prizeUser.prizeHistory.GiftItemID;
                giftWon.WeekNumber = prizeUser.prizeHistory.WeekNumber;
                giftWon.CreatedAt = DateTime.Now;
                DataContext.GiftWonFromLuckyDraws.Add(giftWon);
                var winnerList = winnerLists.FirstOrDefault(w => prizeUser.prizeHistory.GiftItemID == w.GiftItemID);


                //Insert WinnerListUser
                //if (!winnerList.WinnerListUsers.Any(u => u.UserId == prizeUser.userLucky.UserId && u.LuckyDrawCode == prizeUser.prizeHistory.LuckyDrawCode))
                //{
                winnerList.WinnerListUsers.Add(new WinnerListUser
                {
                    UserId = prizeUser.userLucky.UserId,
                    LuckyDrawCode = prizeUser.prizeHistory.LuckyDrawCode,
                    Email = prizeUser.userLucky.UserProfile.Email,
                    FirstName = prizeUser.userLucky.UserProfile.FirstName,
                    LastName = prizeUser.userLucky.UserProfile.LastName,
                    PhoneNumber = prizeUser.userLucky.UserProfile.PhoneNumber,
                    Address = prizeUser.userLucky.UserProfile.Address
                });
                //}
            }
            DataContext.SaveChanges();
        }


        /// <summary>
        /// User set prize for each week
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        public Result<WinnerListUserDTO> SetPrize(SetPrizeCommand command)
        {
            if (command == null || command.WeekNumber <=0 || command.Prize == 0)
                return Result.Fail<WinnerListUserDTO>("Expect input data");

            var listUserCode =
                DataContext.UserLuckyDrawCodes.Include(d => d.UserProfile)
                    .Where(l => l.WeekNumber == command.WeekNumber && l.IsActive);

            var drawtotals = listUserCode.Count();

            if(drawtotals==0) return Result.Fail<WinnerListUserDTO>("Không còn đủ mã may mắn để quay");

            string giftItem = "";
            if (command.Prize == 1)
            {
                giftItem = "TraiHe";
            }
            else if (command.Prize == 3)
            {
                giftItem = "DoChoi";
            }
            else if(command.Prize == 2)
            {
                giftItem = command.WeekNumber % 2 == 0 ? "XeDien" : "VinID";
            }

            var gift = DataContext.GiftItems.FirstOrDefault(g => g.Name == giftItem);

            if (gift == null) return Result.Fail<WinnerListUserDTO>("Không có quà tặng");

            var randomNumber = RandomNumbers(drawtotals);
            var user = listUserCode.OrderBy(l => l.LuckyDrawCode).Skip(randomNumber - 1).Take(1).FirstOrDefault();

            if (user == null)
                return Result.Fail<WinnerListUserDTO>("Không có thông tin user");

            // Insert GiftWonFromLuckyDraw
            var giftWon = new GiftWonFromLuckyDraw
            {
                UserId = user.UserId,
                GiftId = gift.Id,
                WeekNumber = user.WeekNumber,
                CreatedAt = DateTime.Now
            };
            DataContext.GiftWonFromLuckyDraws.Add(giftWon);

            //insert prize history
            var prizeLuckyDrawHistory = new PrizeLuckyDrawHistory
            {
                LuckyDrawCode = user.LuckyDrawCode,
                WeekNumber = user.WeekNumber,
                GiftItemID = gift.Id,
                CreatedDate = DateTime.Now
            };

            DataContext.PrizeLuckyDrawHistories.Add(prizeLuckyDrawHistory);

            var winnerList =
                DataContext.WinnerLists.FirstOrDefault(
                    w => w.WeekNumber == command.WeekNumber && w.GiftItemID == gift.Id);

            if(winnerList ==null)
            {
                winnerList = new WinnerList
                {
                    WeekNumber = user.WeekNumber,
                    GiftItemID = gift.Id,
                    CreatedDate = DateTime.Now,
                    PriceType = gift.Name,
                    PriceDescription = GetPrizeDescription(command.Prize)
                };
                //insert winnerlist
                DataContext.WinnerLists.Add(winnerList);
            }

            //insert winnerlistuser
            var winnerlistUser =  new WinnerListUser
            {
                UserId = user.UserId,
                LuckyDrawCode = user.LuckyDrawCode,
                Email = user.UserProfile.Email,
                FirstName = user.UserProfile.FirstName,
                LastName = user.UserProfile.LastName,
                PhoneNumber = user.UserProfile.PhoneNumber,
                Address = user.UserProfile.Address,
                UserName = user.UserProfile.UserName
            };

            winnerList.WinnerListUsers.Add(winnerlistUser);
           
            user.IsActive = false;
            DataContext.SaveChanges();
            return Result.Success(WinnerListUserDTO.EntityToDtoMapper(winnerlistUser));
        }

        private string GetPrizeDescription(int prizeId)
        {
            switch (prizeId)
            {
                case 1:
                    return "Giải Nhất";
                case 2:
                    return "Giải Nhì";
                case 3:
                    return "Giải Ba";
            }
            return string.Empty;
        }

        public int RandomNumbers(int max)
        {
            // Create generator
            return RandomNumber.Randomness.getNextInt(1, max);
        }

        public Result ConfirmPrize(ConfirmPrizeCommand command)
        {
            if(command == null)
                return Result.Fail("Tham số không hợp lệ.");

            var winnerList =
                DataContext.WinnerLists.FirstOrDefault(
                    item => item.WeekNumber == command.WeekNumber && item.PriceType == command.PrizeType);

            if(winnerList == null)
                return Result.Fail("Không có dữ liệu phù hợp.");

            var winnerListUser =
                DataContext.WinnerListUsers.FirstOrDefault(user => user.Id == command.WinnerListUserId);

            winnerList.Status = GlobalConstants.WinnerListStatusConfirmed;

            if (winnerListUser != null)
            {
                winnerListUser.Status = GlobalConstants.WinnerListStatusConfirmed;
            }

            SaveChanges("WinnerList_WinnerListUser");

            return Result.Success();
        }

        public Result ResetPrize(ConfirmPrizeCommand command)
        {
            if (command == null)
                return Result.Fail("Tham số không hợp lệ.");

            var winnerList =
                DataContext.WinnerLists.FirstOrDefault(
                    item => item.WeekNumber == command.WeekNumber && item.PriceType == command.PrizeType);

            if (winnerList == null)
                return Result.Fail("Không có dữ liệu phù hợp.");

            var winnerListUser =
                DataContext.WinnerListUsers.FirstOrDefault(user => user.Id == command.WinnerListUserId);

            if (winnerListUser == null)
                return Result.Fail("Không có dữ liệu phù hợp.");

            var totalWinnerUsersOfThisList = winnerList.WinnerListUsers.Count;

            var winnerUserCountLeft = totalWinnerUsersOfThisList - 1;

            var userLuckyDrawCode =
                DataContext.UserLuckyDrawCodes.FirstOrDefault(
                    code =>
                        code.UserId == command.UserId && code.WeekNumber == command.WeekNumber &&
                        code.LuckyDrawCode == command.LuckyDrawCode);

            // set active back the lucky draw code
            if (userLuckyDrawCode != null)
                userLuckyDrawCode.IsActive = true;

            // clear GiftWonFromLuckyDraw for user
            var giftId = GetGiftIdFromGiftName(command.PrizeType);
            var giftWonFromLuckyDraw =
                DataContext.GiftWonFromLuckyDraws.Where(
                    item =>
                        item.UserId == command.UserId && item.WeekNumber == command.WeekNumber &&
                        item.GiftId == giftId);

            DataContext.GiftWonFromLuckyDraws.RemoveRange(giftWonFromLuckyDraw);

            // clear PrizeLuckyDrawHistory
            var prizeLuckyDrawHistoryList =
                DataContext.PrizeLuckyDrawHistories.Where(item => item.WeekNumber == command.WeekNumber
                                                                  && item.LuckyDrawCode == command.LuckyDrawCode &&
                                                                  item.GiftItemID == giftId);
            DataContext.PrizeLuckyDrawHistories.RemoveRange(prizeLuckyDrawHistoryList);

            // record activity
            DataContext.UserActivityHistories.Add(new UserActivityHistory
            {
                UserName = command.UserName,
                CreatedAt = DateTime.UtcNow,
                Action = $"ResetPrize_Week_{command.WeekNumber}",
                ActionDetail = $"ResetPrize for week {command.WeekNumber} player {command.UserId} prize {command.PrizeType}",
                ClientIpAddress = command.ClientIpAddress,
                ClientInfo = command.ClientInfo
            });

            if (winnerUserCountLeft > 0)
            {
                DataContext.WinnerListUsers.Remove(winnerListUser);
                SaveChanges("WinnerList_WinnerListUser_UserLuckyDrawCode_PrizeLuckyDrawHistory");
                return Result.Success();
            }

            DataContext.WinnerListUsers.Remove(winnerListUser);
            DataContext.WinnerLists.Remove(winnerList);

            SaveChanges("WinnerList_WinnerListUser_UserLuckyDrawCode_PrizeLuckyDrawHistory");

            return Result.Success();
        }

        private int GetGiftIdFromGiftName(string commandPrizeType)
        {
            return
                "TraiHe".Equals(commandPrizeType)
                    ? 4
                    : "VinID".Equals(commandPrizeType)
                        ? 5
                        : "XeDien".Equals(commandPrizeType)
                            ? 6
                            : "DoChoi".Equals(commandPrizeType) ? 7 : -1;
        }
    }
}
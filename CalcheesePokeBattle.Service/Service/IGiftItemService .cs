﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CalcheesePokeBattle.Common.DTO;
using CalcheesePokeBattle.Common.Monads;
using CalcheesePokeBattle.DTO;
using CalcheesePokeBattle.Model;
using CalcheesePokeBattle.Common.Extensions;

namespace CalcheesePokeBattle.Service
{
    public class GiftItemCommandBase
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public class SearchGiftItemCommand : KeywordSearchCommand
    {
        public string Name { get; set; }
    }

    public class UpdateGiftItemCommand : GiftItemDTO
    {
    }

    public interface IGiftItemService
    {
        Result<GiftItemDTO> CreateGiftItem(GiftItemDTO dto);
        PagingResult<GiftItemDTO> SearchGiftItems(SearchGiftItemCommand command);
        Result<GiftItemDTO> GetGiftItemDetail(GiftItemCommandBase command);
        Result<GiftItemDTO> Update(UpdateGiftItemCommand command);
        Result<ResponseDTO> Delete(GiftItemCommandBase command);
    }

    public class GiftItemService : DefaultServiceBase, IGiftItemService
    {
        public GiftItemService(IDbFactory dbFactory) : base(dbFactory)
        {
        }

        public PagingResult<GiftItemDTO> SearchGiftItems(SearchGiftItemCommand command)
        {
            return PagingResult<GiftItemDTO>.AllResult
            (
                DataContext.GiftItems.Select(GiftItemDTO.EntityToDtoMapper).ToList()
            );
        }

        public Result<GiftItemDTO> CreateGiftItem(GiftItemDTO dto)
        {
            DataContext.GiftItems.Add(GiftItemDTO.DtoToEntityMapper(dto));
            SaveChanges("GiftItems", dto.Id.ToString());
            return Result.Success(dto);
        }

        public Result<GiftItemDTO> GetGiftItemDetail(GiftItemCommandBase command)
        {
            var giftItem = command.Id > 0
                ? DataContext.GiftItems.Find(command.Id)
                : DataContext.GiftItems.FirstOrDefault(
                    p => string.Compare(p.Name, command.Name, StringComparison.InvariantCultureIgnoreCase) == 0);

            return giftItem == null
                ? Result.Fail<GiftItemDTO>($"Cannot find user with Id -> {command.Name}")
                : Result.Success(GiftItemDTO.EntityToDtoMapper(giftItem));
        }

        public Result<GiftItemDTO> Update(UpdateGiftItemCommand command)
        {
            if (command == null)
                return Result.Fail<GiftItemDTO>("Expect input data");
            var giftItemEntity = new GiftItem();
            if (command.IsNew)
            {
                DataContext.GiftItems.Add(giftItemEntity);
            }
            else
            {
                giftItemEntity = FindGiftItemEntity(command.Id, command.Name);
            }


            if (giftItemEntity == null)
                return Result.Fail<GiftItemDTO>($"No Gift Item ${command.Id} to update");

            giftItemEntity.Name = command.Name;
            giftItemEntity.Description = command.Description;
            giftItemEntity.RedeemPoint = command.RedeemPoint;
            giftItemEntity.IsRedeem = command.IsRedeem;
            giftItemEntity.ImageUrl = command.ImageUrl ==""? string.Format("{0}.jpg", command.Name)  : command.ImageUrl;

            SaveChanges("GiftItem", command.Id.ToString());

            return Result.Success(GiftItemDTO.EntityToDtoMapper(giftItemEntity));
        }

        private GiftItem FindGiftItemEntity(int id, string name)
        {
            var giftItemEntity = id != 0
               ? DataContext.GiftItems.Find(id)
               : DataContext.GiftItems.FirstOrDefault(
                   p => string.Compare(p.Name, name, StringComparison.InvariantCultureIgnoreCase) == 0);
            return giftItemEntity;
        }


        public Result<ResponseDTO> Delete(GiftItemCommandBase command)
        {
            throw new NotImplementedException();
        }
    }
}

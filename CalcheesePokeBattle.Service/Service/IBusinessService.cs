﻿using System;
using System.Data.Entity;
using System.Data.Entity.Core;
using System.Data.Entity.Validation;
using System.Linq;
using CalcheesePokeBattle.Common.Monads;
using CalcheesePokeBattle.DTO;
using CalcheesePokeBattle.Model;
using Castle.Core.Logging;

namespace CalcheesePokeBattle.Service
{
    public interface IBusinessService
    {
        IServiceFactory ServiceFactory { get; set; }
        ILogger Logger { get; set; }
        IDbFactory DbFactory { get; }
        CalcheeseAppDbContext DataContext { get; }
    }

    public abstract class DefaultServiceBase : IBusinessService
    {
        public IServiceFactory ServiceFactory { get; set; }

        public ILogger Logger { get; set; }

        public IDbFactory DbFactory { get; }

        public CalcheeseAppDbContext DataContext => DbFactory.GetDbContext();

        protected const int DefaultCacheTimeOut = 900000;

        protected const int DefaultMaxItemsPerSearchResultPage = 100;

        protected DefaultServiceBase(IDbFactory dbFactory)
        {
            DbFactory = dbFactory;
        }

        protected T GetService<T>()
        {
            return ServiceFactory.GetService<T>();
        }

        protected T GetService<T>(string key)
        {
            return ServiceFactory.GetService<T>(key);
        }

        protected string GetSettingValue(SettingKeys setting)
        {
            return ServiceFactory.GetService<ISettingService>().GetStringValue(setting);
        }

        protected string GetSettingValue(SettingKeys setting, string defaultValue)
        {
            var settingValue = ServiceFactory.GetService<ISettingService>().GetStringValue(setting);
            return string.IsNullOrEmpty(settingValue) ? defaultValue : settingValue;
        }

        protected WeekDefinitionDTO GetCurrentWeekForToday()
        {
            return GetService<ISettingService>().GetWeekFromCurrentDate()
                .OnFailureRaiseError(msg => new ApplicationException(msg))
                .Value;
        }

        /// <summary>
        /// Should have cache for this method
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        protected UserProfile GetUserProfileEntity(string userId)
        {
            return DataContext.UserProfiles
                .Include(p => p.UserCardCollections)
                .FirstOrDefault(p => p.Id == userId);
        }

        #region Db Operations methods

        protected void SaveChanges(string objName = "Object", string objKey = "N/A")
        {
            var msg = $"Updated {objName} {objKey}";
            Logger.Info(msg);
            try
            {
                DataContext.SaveChanges();
            }
            catch (DbEntityValidationException dbe)
            {
                foreach (var eve in dbe.EntityValidationErrors)
                {
                    Logger.ErrorFormat(
                        "Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State
                    );
                    foreach (var ve in eve.ValidationErrors)
                        Logger.ErrorFormat("- Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage);
                }

                throw new ApplicationException(msg, dbe);
            }
            catch (EntityCommandExecutionException ecx)
            {
                var message = ecx.InnerException?.Message;
                Logger.Error($"{msg} failed.\n {message}", ecx);

                throw new ApplicationException(message, ecx);
            }
        }

        #endregion
    }
}

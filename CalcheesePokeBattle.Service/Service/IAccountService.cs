﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using CalcheesePokeBattle.Authentication;
using CalcheesePokeBattle.Authentication.Models;
using CalcheesePokeBattle.Common.Monads;
using CalcheesePokeBattle.DTO;
using CalcheesePokeBattle.Helper;
using CalcheesePokeBattle.Model;
using Newtonsoft.Json;

namespace CalcheesePokeBattle.Service
{
    public class InitAdminAccountCommand
    {
        public string Secret { get; set; }
    }

    public class ValidateCaptchaCommand
    {
        public string GRecaptchaResponse { get; set; }
        public string RemoteIp { get; set; }
    }

    public class RecaptchaVerifyCommand
    {
        [JsonProperty("secret")]
        public string secret;

        [JsonProperty("response")]
        public string response;

        [JsonProperty("remoteip")]
        public string remoteip;
    }

    public class RecaptchaVerifyResponse
    {
        [JsonProperty("success")]
        public bool Success { get; set; }

        [JsonProperty("challenge_ts")]
        public DateTime ChallengeTs { get; set; }

        [JsonProperty("hostname")]
        public string HostName { get; set; }

        [JsonProperty("error-codes")]
        public string[] ErrorCodes { get; set; }
    }

    public class ForgotPasswordRequestCommand : BaseTrackableDTO
    {
        public string Email { get; set; }
    }

    /// <summary>
    /// Service for user registration, reset password, forgot password
    /// </summary>
    public interface IAccountService
    {
        /// <summary>
        /// Back door to create admin user for initial deployment
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        Result<UserProfileDTO> InitAdminAccount(InitAdminAccountCommand command);

        ResponseDTO ValidatePlayerName(UpdateUserCommand command);

        ResponseDTO ValidatePhoneNumber(UpdateUserCommand command);

        Task<ResponseDTO> ValidateCaptcha(ValidateCaptchaCommand command);

        /// <summary>
        /// I.e use for user registration, by default add user to normal user role
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        Result<UserProfileDTO> RegisterPlayer(UpdateUserCommand command);

        Result ForgotPasswordRequest(ForgotPasswordRequestCommand command);
    }

    public class DefaultAccountService : DefaultServiceBase, IAccountService
    {
        private const string GoogleRecaptchaServiceUrl = "https://www.google.com/";
        private static List<string> UnsafeCharacterListUserName = new List<string>
        {
            ":", ";", "@", "&", "=", "<", ">", "#", "{", "}", "|", @"\", "^", "~", "[", "]", "`", " "
        };

        public DefaultAccountService(IDbFactory dbFactory) : base(dbFactory)
        {
        }

        public Result<UserProfileDTO> InitAdminAccount(InitAdminAccountCommand command)
        {
            if (!"274FB09A-86D0-40EC-8A20-748F88426882".Equals(command.Secret))
                return Result.Success(new UserProfileDTO());

            // Create Administrator role, by pass existing role check
            var authenticator = GetService<IAuthenticator>();
            authenticator.CreateRole(RoleDTO.Administrator);

            var cmd = new UpdateUserCommand
            {
                UserName = "admin",
                Email = "admin@dev.io",
                Password = "123qaz456"
            };

            var appUser = CreateAppUserInternal(authenticator, cmd);

            if (appUser == null)
                return Result.Success(new UserProfileDTO());

            var appRoleRes = authenticator.AddUserToRole(appUser.Id, RoleDTO.Administrator);

            if(!appRoleRes.Succeeded)
                return Result.Success(new UserProfileDTO());

            var profileRes = CreateUserProfileInternal(appUser, cmd);
            return profileRes;
        }

        private IApplicationUser CreateAppUserInternal(UpdateUserCommand command)
        {
            var authenticator = GetService<IAuthenticator>();
            return CreateAppUserInternal(authenticator, command);
        }

        private IApplicationUser CreateAppUserInternal(IAuthenticator authenticator, UpdateUserCommand command)
        {
            var appUser = authenticator.CreateAppUser(command.UserName, command.Email, command.Password, isActive: true);
            if (command.AddUserToRole && command.UserRoles != null && command.UserRoles.Count > 0)
            {
                authenticator.AddToRolesAsync(appUser.Id, command.UserRoles.Select(r => r.Name).ToArray());
            }
            return appUser;
        }

        private Result<UserProfileDTO> CreateUserProfileInternal(IApplicationUser appUser, UpdateUserCommand command)
        {
            var userProfileService = GetService<IUserProfileService>();
            var userProfileDto = new UserProfileDTO
            {
                Id = appUser.Id,
                UserName = appUser.UserName,
                PhoneNumber = command.PhoneNumber,
                Email = command.Email,
                TotalPoint = 0,
                Address = command.Address,
                CreatedDate = DateTime.UtcNow
            };
            return userProfileService.CreateUserProfile(userProfileDto);
        }

        public ResponseDTO ValidatePlayerName(UpdateUserCommand command)
        {
            if(string.IsNullOrEmpty(command?.UserName))
                return ResponseDTO.ErrorResult("001", "Tên đăng nhập không hợp lệ");

            if(UnsafeCharacterListUserName.Any(command.UserName.Contains))
                return ResponseDTO.ErrorResult("001", "Tên đăng nhập không hợp lệ");

            // Check existing
            var authenticator = GetService<IAuthenticator>();
            var user = authenticator.FindByName(command.UserName);
            if(user != null)
                return ResponseDTO.ErrorResult("002", "Tên đăng nhập đã được sử dụng");

            return ResponseDTO.SuccessResult();
        }

        public ResponseDTO ValidatePhoneNumber(UpdateUserCommand command)
        {
            if (string.IsNullOrEmpty(command?.PhoneNumber))
                return ResponseDTO.ErrorResult("001", "Số điện thoại không hợp lệ");

            if (UserProfileHelper.Characters.ToList().Any(command.PhoneNumber.Contains))
                return ResponseDTO.ErrorResult("001", "Số điện thoại không hợp lệ");

            if (command.PhoneNumber.Length > 11)
                return ResponseDTO.ErrorResult("001", "Số điện thoại không hợp lệ");

            // Check existing
            var existingPhoneNo = DataContext.UserProfiles.Any(p => command.PhoneNumber.Equals(p.PhoneNumber));
            if(existingPhoneNo)
                return ResponseDTO.ErrorResult("002", "Số điện thoại đã được sử dụng");

            return ResponseDTO.SuccessResult();
        }

        /// <summary>
        /// Validate recaptcha response
        /// more info: https://developers.google.com/recaptcha/docs/verify
        /// https://www.google.com/recaptcha/admin#site/337243572
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        public async Task<ResponseDTO> ValidateCaptcha(ValidateCaptchaCommand command)
        {
            if (string.IsNullOrEmpty(command?.GRecaptchaResponse))
                return ResponseDTO.ErrorResult("001", "Invalid captcha input");
            
            var client = new HttpClient {BaseAddress = new Uri(GoogleRecaptchaServiceUrl)};
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            var jsonInString = JsonConvert.SerializeObject(new RecaptchaVerifyCommand
            {
                response = command.GRecaptchaResponse,
                remoteip = command.RemoteIp,
                secret = "6Le07RkUAAAAAFPhpGbxYoWJBU2LJYpvmwI9PvWI"
            });
            var postMessage = new StringContent(jsonInString, Encoding.UTF8, "application/json");
            var uriPath = $"recaptcha/api/siteverify?secret=6Le07RkUAAAAAFPhpGbxYoWJBU2LJYpvmwI9PvWI&response={command.GRecaptchaResponse}";
            var response = await client.PostAsync(uriPath, postMessage);

            if (response.IsSuccessStatusCode)
            {
                //var res = await response.Content.ReadAsStringAsync();
                var verifyResponse = await response.Content.ReadAsAsync<RecaptchaVerifyResponse>();
                if(verifyResponse != null && verifyResponse.Success)
                    return ResponseDTO.SuccessResult();
            }

            return ResponseDTO.ErrorResult("002", "Unable to verify captcha");
        }

        public Result<UserProfileDTO> RegisterPlayer(UpdateUserCommand command)
        {
            var createUserCheckResult = UserProfileHelper.ValidateUserCreation(command);
            if (createUserCheckResult.IsFailure)
                return Result.Fail<UserProfileDTO>(createUserCheckResult.Error);

            var checkUserExist = ValidatePlayerName(command);
            if (!checkUserExist.Success)
                return Result.Fail<UserProfileDTO>(checkUserExist.Error.Message);

            var appUser = CreateAppUserInternal(command);

            if (appUser == null)
                return Result.Fail<UserProfileDTO>($"Cannot create user {command.UserName}");

            return CreateUserProfileInternal(appUser, command);
        }

        /// <summary>
        /// In whatever condition always return success
        /// do not let client know real status of system
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        public Result ForgotPasswordRequest(ForgotPasswordRequestCommand command)
        {
            if(command == null)
                return Result.Success();

            // if user has email send reset password to email that's all

            return Result.Success();
        }
    }
}

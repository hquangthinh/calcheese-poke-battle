﻿using System;
using CalcheesePokeBattle.Common.Monads;
using CalcheesePokeBattle.DTO;
using CalcheesePokeBattle.Model;

namespace CalcheesePokeBattle.Service
{
    public class DefaultLuckyDrawCodeService : DefaultServiceBase, ILuckyDrawCodeService
    {
        public DefaultLuckyDrawCodeService(IDbFactory dbFactory) : base(dbFactory)
        {
        }

        public Result<string> GenerateLuckyDrawCodeForWeek(GenerateLuckyDrawCodeForWeekCommand command, UserProfileDTO currentProfileDto)
        {
            var generator = GetService<ILuckyDrawCodeServiceFactory>().GetGeneratorServiceInstance(command.WeekNumber);
            return generator.GenerateLuckyDrawCodeForWeek(command, currentProfileDto)
                .OnFailureRaiseError(msg => new ApplicationException(msg))
                .OnSuccess(codePool => codePool.Id.ToString().PadLeft(6, '0'));
        }

        public Result<string> GenerateLuckyDrawCodeForCurrentWeek(ITrackableDTO commanDto, UserProfileDTO currentProfileDto)
        {
            var currentWeek = GetCurrentWeekForToday();
            return GenerateLuckyDrawCodeForWeek(new GenerateLuckyDrawCodeForWeekCommand
            {
                WeekNumber = currentWeek.WeekNumber,
                ClientInfo = commanDto.ClientInfo,
                ClientIpAddress = commanDto.ClientIpAddress
            }, currentProfileDto);
        }
    }
}
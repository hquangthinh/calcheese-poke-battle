﻿using System;
using System.Linq;
using CalcheesePokeBattle.Common.Extensions;
using CalcheesePokeBattle.Common.Monads;
using CalcheesePokeBattle.Common.Utils;
using CalcheesePokeBattle.DTO;
using CalcheesePokeBattle.Model;
using Castle.Core.Internal;

namespace CalcheesePokeBattle.Service
{
    public class GameBaseCommand
    {
        /// <summary>
        /// UserId of player who send game request
        /// </summary>
        public string PlayerUserId { get; set; }

        public string PlayerUserName { get; set; }

        /// <summary>
        /// Full name of player who send game request
        /// </summary>
        public string PlayerName { get; set; }

        public string PlayerFullName => string.IsNullOrWhiteSpace(PlayerName) ? PlayerUserName : PlayerName;
    }

    public class NewGameRequestCommand : GameBaseCommand
    {
        /// <summary>
        /// Total point of player who send game request
        /// </summary>
        public int PlayerTotalPoint { get; set; }

        /// <summary>
        /// UserId of player who is invited for a new game
        /// </summary>
        public string InvitedPlayerUserId { get; set; }

        public string InvitedPlayerUserName { get; set; }

        public string InvitedPlayerName { get; set; }

        public string InvitedPlayerFullName => string.IsNullOrWhiteSpace(InvitedPlayerName) ? InvitedPlayerUserName : InvitedPlayerName;
    }

    public class ConfirmGameInvitationCommand : GameBaseCommand
    {
        /// <summary>
        /// UserId of player who initial new game request, the invited player confirms back to this player
        /// </summary>
        public string ToPlayerUserId { get; set; }

        public string ToPlayerUserName { get; set; }

        public string ToPlayerName { get; set; }

        public string ToPlayerFullName => string.IsNullOrWhiteSpace(ToPlayerName) ? ToPlayerUserName : ToPlayerName;

        public bool Ok { get; set; }
    }

    public class ConfirmGameInvitationResult
    {
        public int GameSessionId { get; set; }

        public bool ConfirmStart { get; set; }

        public bool DeclineInvitation => !ConfirmStart;
    }

    public class SubmitPokemonCardCommand : GameBaseCommand
    {
        public int GameSessionId { get; set; }

        public int PlayerCardId { get; set; }

        /// <summary>
        /// Time in ms of player thinking before submit card
        /// </summary>
        public int PlayerTimeSpentForCardSelection { get; set; }

        public int PlayerThinkTimeRemain => 10 - PlayerTimeSpentForCardSelection;
    }

    public enum GameStatusEnum
    {
        ResultAvailable,
        GameInProgress,
        Timeout
    }

    public class GameResult : GameSessionInfo
    {
        public string WonUserId { get; set; }
        public string WonUserName { get; set; }
        public int WonCardId { get; set; }
        public string WonCardImageUrl { get; set; }
        public string WonCardElementImageUrl { get; set; }
        public string WonCardElementDisplay { get; set; }
        public bool WonIsSpecialCard { get; set; }
        public string WonCardTypeDisplay { get; set; }

        /// <summary>
        /// Number of points the winner take
        /// </summary>
        public int WonPoints { get; set; }
        public string LostUserId { get; set; }
        public string LostUserName { get; set; }
        public int LostCardId { get; set; }
        public string LostCardImageUrl { get; set; }
        public string LostCardElementImageUrl { get; set; }
        public string LostCardElementDisplay { get; set; }
        public bool LostIsSpecialCard { get; set; }
        public string LostCardTypeDisplay { get; set; }

        /// <summary>
        /// Number of points the looser lost
        /// </summary>
        public int LostPoints { get; set; }

        // properties used for game tie
        public int LeftCardId { get; set; }
        public string LeftCardImageUrl { get; set; }
        public string LeftCardElementImageUrl { get; set; }
        public string LeftCardElementDisplay { get; set; }
        public bool LeftIsSpecialCard { get; set; }
        public string LeftCardTypeDisplay { get; set; }

        public int RightCardId { get; set; }
        public string RightCardImageUrl { get; set; }
        public string RightCardElementImageUrl { get; set; }
        public string RightCardElementDisplay { get; set; }
        public bool RightIsSpecialCard { get; set; }
        public string RightCardTypeDisplay { get; set; }

        public bool CanLooserContinueToPlayNextRound { get; set; }
        public string GameStopReason { get; set; }
        public bool HasWinner { get; set; }
        public bool GameTie { get; set; }
        public GameStatusEnum GameStatus { get; set; }
    }

    /// <summary>
    /// Define contracts to handle game logic
    /// </summary>
    public interface IPokeCardGameLogicService
    {
        Result<ResponseDTO<string>> CheckIfUserCanSendNewGameInvitation(string userId);

        Result<OnlinePlayerDTO> FindRandomOpponentPlayer(ServerInfoCommand command, PlayerConnectionInfoDTO playerConnectionInfo);

        Result<ResponseDTO<string>> ValidateNewGameRequest(PlayerConnectionInfoDTO playerConnectionInfo, NewGameRequestCommand command);

        Result<ConfirmGameInvitationResult> ConfirmGameInvitation(ConfirmGameInvitationCommand command);

        Result<GameResult> SubmitPokemonCard(SubmitPokemonCardCommand command);

        Result<GameResult> SubmitPokemonCardForTest(SubmitPokemonCardCommand command);
    }

    public class DefaultPokeCardGameLogicService : DefaultServiceBase, IPokeCardGameLogicService
    {
        public DefaultPokeCardGameLogicService(IDbFactory dbFactory) : base(dbFactory)
        {
        }

        /// <summary>
        /// Perform server side check if a player is eligable to send a new game invitation to another user
        /// I.e player is valid loggedin, have at least 7 points, have enough cards to play
        /// To be called before player can send game request to opponents
        /// Call again when new game request is sent
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public Result<ResponseDTO<string>> CheckIfUserCanSendNewGameInvitation(string userId)
        {
            if (string.IsNullOrEmpty(userId))
                return Result.Fail(ResponseDTO<string>.ErrorResult("001", GlobalErrorMessage.InvalidUser), GlobalErrorMessage.InvalidUser);

            var curreProfile = GetUserProfileEntity(userId);

            if (curreProfile.TotalPoint < GlobalConstants.MinimumPointsForPlayerToPlayGame)
                return Result.Fail(ResponseDTO<string>.ErrorResult("002", GlobalErrorMessage.NeedAtleastSevenPointsToPlay),
                    GlobalErrorMessage.NeedAtleastSevenPointsToPlay);

            if (curreProfile.UserCardCollections.Count == 0)
                return Result.Fail(ResponseDTO<string>.ErrorResult("002", GlobalErrorMessage.NotEnoughCardToPlay),
                    GlobalErrorMessage.NotEnoughCardToPlay);

            return Result.Success(ResponseDTO<string>.NoDataSuccessResult());
        }

        /// <summary>
        /// Validate a new game request to check if this request comes from a valid user
        /// User has enough points to play
        /// </summary>
        /// <param name="playerConnectionInfo"></param>
        /// <param name="command"></param>
        /// <returns></returns>
        public Result<ResponseDTO<string>> ValidateNewGameRequest(PlayerConnectionInfoDTO playerConnectionInfo, NewGameRequestCommand command)
        {
            if (playerConnectionInfo == null || command == null)
                return Result.Fail(ResponseDTO<string>.ErrorResult("000", GlobalErrorMessage.ExpectNonNullParameters), GlobalErrorMessage.ExpectNonNullParameters);

            // validate the new game request
            // check if user who send request has enough points and card
            var canUserSendRequest = CheckIfUserCanSendNewGameInvitation(playerConnectionInfo.UserId);

            if (canUserSendRequest.IsFailure)
                return canUserSendRequest;

            var invitedPlayer = GetUserProfileEntity(command.InvitedPlayerUserId);

            if(invitedPlayer == null)
                return Result.Fail(ResponseDTO<string>.ErrorResult("003", GlobalErrorMessage.InvalidInvitedUser), GlobalErrorMessage.InvalidInvitedUser);

            // check if invited user is not in game
            var gameSessionManager = GetService<IGameSessionService>();
            var invitedPlayerIsInGame = gameSessionManager.IsUserInGame(invitedPlayer.Id);
            if (invitedPlayerIsInGame)
            {
                var msg = string.Format(GlobalErrorMessage.InvitedUserDoesIsPlayingWithOther, command.InvitedPlayerFullName);
                return Result.Fail(ResponseDTO<string>.ErrorResult("006", msg), msg);
            }

            if (invitedPlayer.TotalPoint < GlobalConstants.MinimumPointsForPlayerToPlayGame)
                return
                    Result.Fail(
                        ResponseDTO<string>.ErrorResult("004", GlobalErrorMessage.InvitedUserDoesNotHaveEnoughPoints),
                        GlobalErrorMessage.InvitedUserDoesNotHaveEnoughPoints);

            if (invitedPlayer.UserCardCollections.Count == 0)
                return
                    Result.Fail(
                        ResponseDTO<string>.ErrorResult("005", GlobalErrorMessage.InvitedUserDoesNotHaveEnoughCards),
                        GlobalErrorMessage.InvitedUserDoesNotHaveEnoughCards);

            return Result.Success(ResponseDTO<string>.NoDataSuccessResult());
        }

        private static readonly Func<CacheCommand<DefaultPokeCardGameLogicService>, Func<Random>> GetRandomInstanceCache
            = Functions.Lambda((DefaultPokeCardGameLogicService ctx) => ctx.GetRandomInstanceInternal())
                .MemoizeCtx(CacheConfig.LifeTime(TimeSpan.FromMinutes(30)));

        private Random GetRandomInstanceInternal()
            => new Random();

        private Random GetRandomManager() => GetRandomInstanceCache(this.AsCacheContext())();

        /// <summary>
        /// Find a random player to become an opponent for player who initial random game request
        /// return the connection information of mattching opponent
        /// </summary>
        /// <param name="command"></param>
        /// <param name="playerConnectionInfo"></param>
        /// <returns></returns>
        public Result<OnlinePlayerDTO> FindRandomOpponentPlayer(ServerInfoCommand command, PlayerConnectionInfoDTO playerConnectionInfo)
        {
            // validate request
            if (playerConnectionInfo == null || command == null)
                return Result.Fail<OnlinePlayerDTO>("Expect non null parameters");

            var currentProfileDto = UserProfileDTO.EntityToDtoMapper(GetUserProfileEntity(playerConnectionInfo.UserId));

            if(currentProfileDto == null)
                return Result.Fail<OnlinePlayerDTO>("Người chơi không hợp lệ.");

            var gameServer = GetService<IPokeCardGameServerService>();
            var allOnlinePlayers = gameServer.GetAllOnlinePlayers(command, currentProfileDto).Value.ResultSet.ToList();

            if(allOnlinePlayers.Count == 0)
                return Result.Fail<OnlinePlayerDTO>("Rất tiếc chưa chọn được đối thủ.");

            var random = GetRandomManager();
            var randomIdx = random.Next(0, allOnlinePlayers.Count - 1);

            var matchingOpponentInfo = allOnlinePlayers[randomIdx];
            return Result.Success(matchingOpponentInfo);
        }

        public Result<ConfirmGameInvitationResult> ConfirmGameInvitation(ConfirmGameInvitationCommand command)
        {
            // validate the new game confirmation
            if (command == null)
                return Result.Fail<ConfirmGameInvitationResult>("Expect non null parameters");

            if (command.Ok)
            {
                var newCreatedGameSession = StartGameSessionInternal(command);

                return
                    Result.Success(new ConfirmGameInvitationResult
                    {
                        ConfirmStart = true,
                        GameSessionId = newCreatedGameSession.Id
                    });
            }

            return Result.Success(new ConfirmGameInvitationResult {ConfirmStart = false});
        }

        private GameSessionInfo StartGameSessionInternal(ConfirmGameInvitationCommand command)
        {
            var gameSessionInfo = new GameSessionInfo();

            DataContext.PerformInTransaction("StartGameSessionInternal : Mark player connection status InGame", ctx =>
            {
                var invitedPlayerConnections =
                    ctx.PlayerConnectionInfos.Where(item => item.UserId == command.PlayerUserId);

                foreach (var connection in invitedPlayerConnections)
                {
                    connection.Status = PlayerConnectionStatus.InGame.ToString();
                }

                var requesPlayerConnections =
                    ctx.PlayerConnectionInfos.Where(item => item.UserId == command.ToPlayerUserId);

                foreach (var connection in requesPlayerConnections)
                {
                    connection.Status = PlayerConnectionStatus.InGame.ToString();
                }
                
                var gameSession = new GameSession
                {
                    PlayerUserId1 = command.ToPlayerUserId,
                    PlayerUserName1 = command.ToPlayerUserName,

                    PlayerUserId2 = command.PlayerUserId,
                    PlayerUserName2 = command.PlayerUserName,

                    CreatedDate = DateTime.UtcNow
                };

                ctx.GameSessions.Add(gameSession);

                ctx.SaveChanges();

                // store the game session info in memory for fast processing
                gameSessionInfo = GameSessionInfo.ForNewMapper(gameSession);
                GetService<IGameSessionService>()
                    .StartNewGameSession(gameSessionInfo);
            });
            return gameSessionInfo;
        }

        public Result<GameResult> SubmitPokemonCardForTest(SubmitPokemonCardCommand command)
        {
            // validate the new game action
            if (command == null)
                return Result.Fail<GameResult>("Expect a non null parameter");

            var gameSessionService = GetService<IGameSessionService>();

            var gameResult = gameSessionService
                .GameSessionForUser(command.PlayerUserId)
                .OnSuccess(sessionInfo =>
                {
                    return gameSessionService.UpdateGameState(new UpdateGameSessionStateCommand
                    {
                        GameSessionId = sessionInfo.Id,
                        PlayerUserId = command.PlayerUserId,
                        PlayerCardId = command.PlayerCardId
                    })
                        .OnSuccess(updatedSessionInfo =>
                        {
                            if (updatedSessionInfo.ValidToCalculateWinResult)
                            {
                                return CalculateGameResult(updatedSessionInfo, command.PlayerUserId).Value;
                            }
                            return new GameResult { GameStatus = GameStatusEnum.GameInProgress };
                        })
                        .OnFailureRaiseError(msg => new ApplicationException(msg))
                        .Value;
                })
                .OnFailureRaiseError(msg => new ApplicationException(msg))
                .Value;

            return Result.Success(gameResult);
        }

        /// <summary>
        /// A player submit a card to game session
        /// get game session from player submited info
        /// calculate game result if 2 players have submited cards
        /// return win or tie result with 2 players information
        /// check if 2 players have enough cards & point to play next round
        /// reset game state to be prepare for next round
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        public Result<GameResult> SubmitPokemonCard(SubmitPokemonCardCommand command)
        {
            // validate the new game action
            if(command == null)
                return Result.Fail<GameResult>("Expect a non null parameter");

            var gameSessionService = GetService<IGameSessionService>();

            var gameResult = gameSessionService
                .GameSessionForUserBySessionId(command.GameSessionId)
                .OnSuccess(sessionInfo =>
                {
                    return gameSessionService.UpdateGameState(new UpdateGameSessionStateCommand
                        {
                            GameSessionId = sessionInfo.Id,
                            PlayerUserId = command.PlayerUserId,
                            PlayerCardId = command.PlayerCardId,
                            PlayerTimeSpentForCardSelection = command.PlayerTimeSpentForCardSelection
                        })
                        .OnSuccess(updatedSessionInfo =>
                        {
                            if (updatedSessionInfo.ValidToCalculateWinResult)
                            {
                                // calculate game win-loose result
                                return CalculateGameResult(updatedSessionInfo, command.PlayerUserId)
                                    .OnSuccess(currentRoundGameRes =>
                                    {
                                        if (currentRoundGameRes.GameTie && !currentRoundGameRes.HasWinner)
                                        {
                                            Logger.Debug(
                                                $"Game tie for {currentRoundGameRes.PlayerUserName1} vs {currentRoundGameRes.PlayerUserName2}");
                                            gameSessionService.ResetGameRound(sessionInfo.Id);
                                            currentRoundGameRes.CanLooserContinueToPlayNextRound = true;
                                        }
                                        else if (currentRoundGameRes.HasWinner)
                                        {
                                            Logger.Debug(
                                                $"Game has winner for {currentRoundGameRes.WonUserName} vs {currentRoundGameRes.LostUserName}");

                                            var finalGameRes = UpdatePlayersPointsCardsUsingStoreProcedure(currentRoundGameRes);
                                            // Reset game state to next round
                                            gameSessionService.ResetGameRound(sessionInfo.Id);
                                            return finalGameRes;
                                        }

                                        currentRoundGameRes.CanLooserContinueToPlayNextRound = true;
                                        gameSessionService.ResetGameRound(sessionInfo.Id);
                                        return currentRoundGameRes;
                                    })
                                    .OnFailureRaiseError(msg => new ApplicationException(msg))
                                    .Value;
                            }
                            return new GameResult {GameStatus = GameStatusEnum.GameInProgress};
                        })
                        .OnFailureRaiseError(msg => new ApplicationException(msg))
                        .Value;
                })
                .OnFailureRaiseError(msg => new ApplicationException(msg))
                .Value;

            return Result.Success(gameResult);
        }

        private GameResult UpdatePlayersPointsCardsUsingStoreProcedure(GameResult gameResult)
        {
            Logger.Debug(
                $"Update game final result: {gameResult.WonUserName} won over {gameResult.LostUserName} points {gameResult.LostPoints} take card {gameResult.LostCardId}");

            DataContext.ExecSpStoreGameResult(gameResult.WonUserId, gameResult.LostUserId, gameResult.LostPoints,
                gameResult.LostCardId);

            var lostProfileEntity = GetUserProfileEntity(gameResult.LostUserId);

            var totalRemainCardsForLooser = lostProfileEntity.UserCardCollections.Count;

            gameResult.CanLooserContinueToPlayNextRound =
                        lostProfileEntity.TotalPoint >= GlobalConstants.MinimumPointsForPlayerToPlayGame && totalRemainCardsForLooser > 0;

            if (!gameResult.CanLooserContinueToPlayNextRound)
                gameResult.GameStopReason = "Bạn không đủ điểm hoặc thẻ để chơi tiếp. Hãy nạp thêm điểm để tiếp tục.";

            return gameResult;
        }

        private Result<GameResult> CalculateGameResult(GameSessionInfo updatedSessionInfo, string currentUserId)
        {
            return GetService<IGameRuleService>().CalculateGameResult(updatedSessionInfo, currentUserId);
        }
    }
}
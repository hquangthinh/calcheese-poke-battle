﻿using System;
using System.Linq;
using CalcheesePokeBattle.Common.DTO;
using CalcheesePokeBattle.Common.Monads;
using CalcheesePokeBattle.Common.Timing;
using CalcheesePokeBattle.Common.Utils;
using CalcheesePokeBattle.DTO;
using CalcheesePokeBattle.Model;
using CalcheesePokeBattle.ViewModel;

namespace CalcheesePokeBattle.Service
{
    public class SearchUserLuckyDrawCodeCommand : KeywordSearchCommand
    {
        public int WeekNumber { get; set; }
    }

    public interface IUserLuckyDrawCodeService
    {
        PagingResult<UserLuckyDrawCodeViewModel> SearchUserLuckyDrawCode(SearchUserLuckyDrawCodeCommand command);
        //PagingResult<UserLuckyDrawCodeViewModel> SearchUserLuckyDrawCodeWithPrize(SearchUserLuckyDrawCodeCommand command);
        CurrentWeekPrizeDataDTO GetCurrentWeekData();
    }

    public class UserLuckyDrawCodeService : DefaultServiceBase, IUserLuckyDrawCodeService
    {
        public UserLuckyDrawCodeService(IDbFactory dbFactory) : base(dbFactory)
        {
        }

        public PagingResult<UserLuckyDrawCodeViewModel> SearchUserLuckyDrawCode(SearchUserLuckyDrawCodeCommand command)
        {
           
            if (command == null)
                return PagingResult<UserLuckyDrawCodeViewModel>.Empty();
            var searchKeyword = string.IsNullOrEmpty(command.Keyword) ? string.Empty : command.Keyword.ToLower();
            var rawSearchKeyword = string.IsNullOrEmpty(command.Keyword) ? string.Empty : command.Keyword.Trim();

            return Timer.Time(() =>
            {
                var predicate =
                    Functions.Lambda<UserLuckyDrawCode, bool>(
                        drawCode =>
                            command.WeekNumber == 0 || drawCode.WeekNumber == command.WeekNumber
                            &&
                            (string.IsNullOrEmpty(searchKeyword) ||
                             drawCode.UserProfile.UserName.ToLower().Contains(searchKeyword) ||

                             (drawCode.UserProfile.FirstName !=null && drawCode.UserProfile.FirstName.ToLower().Contains(searchKeyword)) ||

                             (drawCode.UserProfile.LastName != null && drawCode.UserProfile.LastName.ToLower().Contains(searchKeyword)) ||

                             (drawCode.UserProfile.FirstName != null && drawCode.UserProfile.LastName != null && 
                                (drawCode.UserProfile.FirstName.Trim() + " " + drawCode.UserProfile.LastName.Trim() == rawSearchKeyword)) ||

                            (drawCode.UserProfile.FirstName != null && drawCode.UserProfile.LastName != null &&
                                (drawCode.UserProfile.LastName.Trim() + " " + drawCode.UserProfile.FirstName.Trim() == rawSearchKeyword)) ||

                             (drawCode.UserProfile.PhoneNumber != null && drawCode.UserProfile.PhoneNumber.Contains(searchKeyword)) ||

                             (drawCode.UserProfile.Address != null && drawCode.UserProfile.Address.ToLower().Contains(searchKeyword))
                            )
                    );

                var totalCountQuery = from h in DataContext.UserLuckyDrawCodes.Where(predicate)
                    join u in DataContext.UserProfiles on h.UserId equals u.Id
                    select u;

                var totalCount = totalCountQuery.Count();

                var drawCodes = from h in DataContext.UserLuckyDrawCodes.Where(predicate)
                                join u in DataContext.UserProfiles on h.UserId equals u.Id
                                select new UserLuckyDrawCodeViewModel
                                {
                                    UserLuckyDrawCodeDTO = UserLuckyDrawCodeDTO.EntityToDtoMapper(h),
                                    UserProfileDTO = UserProfileDTO.EntityHeaderToDtoMapper(u)
                                };

                var result = drawCodes.Paginate(command);

                return new PagingResult<UserLuckyDrawCodeViewModel>(result)
                {
                    PageSize = command.PageSize,
                    PageNumber = command.PageNumber,
                    PageTotal = PagingHelper.GetPageTotal(totalCount, command.PageSize),
                    ResultCount = totalCount
                };
            });

        }

        public CurrentWeekPrizeDataDTO GetCurrentWeekData()
        {
            var current = new CurrentWeekPrizeDataDTO();
            WeekDefinitionDTO currentWeekDto;
            try
            {
                currentWeekDto = GetCurrentWeekForToday();
            }
            catch (Exception)
            {
                // ignored
                // get the end week of campaign
                current.CuurentWeekDefinition =
                    GetService<ISettingService>().GetWeekFromDate(new DateTime(2017, 7, 8)).Value;
                current.WeekDefinitionDTOs = DataContext.WeekDefinitions.Select(WeekDefinitionDTO.EntityToViewModelMapper).ToList();
                current.GiftItemDTOs = DataContext.GiftItems.Where(g => !g.IsRedeem).Select(GiftItemDTO.EntityToDtoMapper).ToList();
                return current;
            }

            //Get Current Week definition
            current.CuurentWeekDefinition = currentWeekDto;
            //Get List Week definition
            current.WeekDefinitionDTOs = DataContext.WeekDefinitions.Select(WeekDefinitionDTO.EntityToViewModelMapper).ToList();

            //Get GiftItem for Lucky Draw
            current.GiftItemDTOs = DataContext.GiftItems.Where(g => !g.IsRedeem).Select(GiftItemDTO.EntityToDtoMapper).ToList();

            return current;
        }

    }
}

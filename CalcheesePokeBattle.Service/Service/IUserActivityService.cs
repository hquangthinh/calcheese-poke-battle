﻿using System;
using CalcheesePokeBattle.Common.Monads;
using CalcheesePokeBattle.DTO;
using CalcheesePokeBattle.Model;

namespace CalcheesePokeBattle.Service
{
    public interface IUserActivityService
    {
        Result RecordUserActivity(UserActivityHistoryDTO activityDto);
    }

    public class DefaultUserActivityService : DefaultServiceBase, IUserActivityService
    {
        public DefaultUserActivityService(IDbFactory dbFactory) : base(dbFactory)
        {
        }

        public Result RecordUserActivity(UserActivityHistoryDTO activityDto)
        {
            if (activityDto == null)
                return Result.Fail("Expect non null parameter");

            DataContext.UserActivityHistories.Add(new UserActivityHistory
            {
                Action = activityDto.Action,
                ActionDetail = activityDto.ActionDetail,
                UserName = activityDto.UserName,
                ClientInfo = activityDto.ClientInfo,
                ClientIpAddress = activityDto.ClientIpAddress,
                CreatedAt = DateTime.UtcNow
            });
            SaveChanges("UserActivityHistories");

            return Result.Success();
        }
    }
}

﻿using CalcheesePokeBattle.Common.Monads;
using CalcheesePokeBattle.DTO;
using CalcheesePokeBattle.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace CalcheesePokeBattle.Service
{
    public interface IUploadFileService
    {
        Result<UploadFileDTO> Update(UploadFileDTO command);

        List<UploadFileDTO> GetAllUploadInfo();

        ResponseDTO DeleteFile(UploadFileDTO fileInfoDto);
    }
    public class UploadFileService : DefaultServiceBase, IUploadFileService
    {
        public UploadFileService(IDbFactory dbFactory) : base(dbFactory)
        {

        }

        public Result<UploadFileDTO> Update(UploadFileDTO command)
        {
            if (command.PokeCardId == 0 || command.FileName == "")
                return Result.Fail<UploadFileDTO>("Expect input data");
            var uploadFileEntity = new UploadFile
            {
                CreatedDate = DateTime.Now,
                PokeCardId = command.PokeCardId,
                FileName = command.FileName
            };

            DataContext.UploadFiles.Add(uploadFileEntity);
            SaveChanges("UploadFile");
            return Result.Success(command);
        }

        public List<UploadFileDTO> GetAllUploadInfo()
        {
            var query = from f in DataContext.UploadFiles
                join card in DataContext.PokeCards on f.PokeCardId equals card.Id
                select new UploadFileDTO
                {
                    Id = f.Id,
                    FileName = f.FileName,
                    CreatedDate = f.CreatedDate,
                    PokeCardId = card.Id,
                    CardImageUrl = card.ImageUrl,
                    ImportStatus = f.ImportStatus
                };
            var baseImageUrl = GetSettingValue(SettingKeys.WebAppImagesBaseUrl);

            return query.ToList().Select(x => new UploadFileDTO
                {
                    Id = x.Id,
                    FileName = x.FileName,
                    PokeCardId = x.PokeCardId,
                    CreatedDate = x.CreatedDate,
                    CardImageUrl = $"{baseImageUrl}/{x.CardImageUrl}",
                    FileDisplayName = Path.GetFileName(x.FileName),
                    ImportStatus = string.IsNullOrEmpty(x.ImportStatus) ? "Pending Import" : x.ImportStatus
                })
                .ToList();
        }

        public ResponseDTO DeleteFile(UploadFileDTO fileInfoDto)
        {
            if (fileInfoDto == null)
                return ResponseDTO.ErrorResult("000", "No file");

            var entity = DataContext.UploadFiles.Find(fileInfoDto.Id);
            if (entity != null)
            {
                DataContext.UploadFiles.Remove(entity);
                SaveChanges("UploadFile");
            }
            return ResponseDTO.SuccessResult();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CalcheesePokeBattle.Common.Monads;
using CalcheesePokeBattle.DTO;

namespace CalcheesePokeBattle.Service
{
    public class PokemonChartElement
    {
        public PokemonChartElement(string elementName, int weight)
        {
            ElementName = elementName;
            Weight = weight;
        }

        public string ElementName { get; set; }
        public int Weight { get; set; }
    }

    public interface IGameRuleService
    {
        Result<GameResult> CalculateGameResult(GameSessionInfo gameSessionInfo, string currentUserId);
    }

    /// <summary>
    /// Default implementation of game rules
    /// use fixed  predefined logic
    /// </summary>
    public class DefaultGameRuleService : IGameRuleService
    {
        private readonly IServiceFactory _serviceFactory;

        public DefaultGameRuleService(IServiceFactory serviceFactory)
        {
            _serviceFactory = serviceFactory;
        }

        #region PokemonElementCharts definition

        private static readonly ReadOnlyDictionary<string, ReadOnlyCollection<PokemonChartElement>> PokemonElementCharts
            =
            new ReadOnlyDictionary<string, ReadOnlyCollection<PokemonChartElement>>(new Dictionary
                <string, ReadOnlyCollection<PokemonChartElement>>
                {
                    {
                        "Electric", new ReadOnlyCollection<PokemonChartElement>(new List<PokemonChartElement>()
                        {
                            new PokemonChartElement("Electric", 0),new PokemonChartElement("Water", 1),new PokemonChartElement("Fire", 0),
                            new PokemonChartElement("Grass", -1),new PokemonChartElement("Fairy", -1),new PokemonChartElement("Normal", 0),
                            new PokemonChartElement("Dragon", 0),new PokemonChartElement("Fly", 0),new PokemonChartElement("Dark", -1),
                            new PokemonChartElement("Poison", 0),new PokemonChartElement("Warrior", 0),new PokemonChartElement("Hero", 0)
                        })
                    },
                    {
                        "Water", new ReadOnlyCollection<PokemonChartElement>(new List<PokemonChartElement>()
                        {
                            new PokemonChartElement("Electric", -1),new PokemonChartElement("Water", 0),new PokemonChartElement("Fire", 1),
                            new PokemonChartElement("Grass", -1),new PokemonChartElement("Fairy", -1),new PokemonChartElement("Normal", 0),
                            new PokemonChartElement("Dragon", 0),new PokemonChartElement("Fly", 0),new PokemonChartElement("Dark", -1),
                            new PokemonChartElement("Poison", 0),new PokemonChartElement("Warrior", 0),new PokemonChartElement("Hero", 0)
                        })
                    },
                    {
                        "Fire", new ReadOnlyCollection<PokemonChartElement>(new List<PokemonChartElement>()
                        {
                            new PokemonChartElement("Electric", 0),new PokemonChartElement("Water", -1),new PokemonChartElement("Fire", 0),
                            new PokemonChartElement("Grass", 1),new PokemonChartElement("Fairy", -1),new PokemonChartElement("Normal", 0),
                            new PokemonChartElement("Dragon", 0),new PokemonChartElement("Fly", 0),new PokemonChartElement("Dark", -1),
                            new PokemonChartElement("Poison", 0),new PokemonChartElement("Warrior", 0),new PokemonChartElement("Hero", 0)
                        })
                    },
                    {
                        "Grass", new ReadOnlyCollection<PokemonChartElement>(new List<PokemonChartElement>()
                        {
                            new PokemonChartElement("Electric", 1),new PokemonChartElement("Water", 1),new PokemonChartElement("Fire", -1),
                            new PokemonChartElement("Grass", 0),new PokemonChartElement("Fairy", -1),new PokemonChartElement("Normal", 0),
                            new PokemonChartElement("Dragon", 0),new PokemonChartElement("Fly", 0),new PokemonChartElement("Dark", -1),
                            new PokemonChartElement("Poison", 0),new PokemonChartElement("Warrior", 0),new PokemonChartElement("Hero", 0)
                        })
                    },
                    {
                        // Fairy wins all cards except for Dark 
                        "Fairy", new ReadOnlyCollection<PokemonChartElement>(new List<PokemonChartElement>()
                        {
                            new PokemonChartElement("Electric", 1),new PokemonChartElement("Water", 1),new PokemonChartElement("Fire", 1),
                            new PokemonChartElement("Grass", 1),new PokemonChartElement("Fairy", 0),new PokemonChartElement("Normal", 1),
                            new PokemonChartElement("Dragon", 1),new PokemonChartElement("Fly", 1),new PokemonChartElement("Dark", -1),
                            new PokemonChartElement("Poison", 1),new PokemonChartElement("Warrior", 1),new PokemonChartElement("Hero", 1)
                        })
                    },
                    {
                        "Normal", new ReadOnlyCollection<PokemonChartElement>(new List<PokemonChartElement>()
                        {
                            new PokemonChartElement("Electric", 0),new PokemonChartElement("Water", 0),new PokemonChartElement("Fire", 0),
                            new PokemonChartElement("Grass", 0),new PokemonChartElement("Fairy", -1),new PokemonChartElement("Normal", 0),
                            new PokemonChartElement("Dragon", 0),new PokemonChartElement("Fly", 0),new PokemonChartElement("Dark", -1),
                            new PokemonChartElement("Poison", 0),new PokemonChartElement("Warrior", 0),new PokemonChartElement("Hero", 0)
                        })
                    },
                    {
                        "Dragon", new ReadOnlyCollection<PokemonChartElement>(new List<PokemonChartElement>()
                        {
                            new PokemonChartElement("Electric", 0),new PokemonChartElement("Water", 0),new PokemonChartElement("Fire", 0),
                            new PokemonChartElement("Grass", 0),new PokemonChartElement("Fairy", -1),new PokemonChartElement("Normal", 0),
                            new PokemonChartElement("Dragon", 0),new PokemonChartElement("Fly", 0),new PokemonChartElement("Dark", -1),
                            new PokemonChartElement("Poison", 0),new PokemonChartElement("Warrior", 0),new PokemonChartElement("Hero", 0)
                        })
                    },
                    {
                        "Fly", new ReadOnlyCollection<PokemonChartElement>(new List<PokemonChartElement>()
                        {
                            new PokemonChartElement("Electric", 0),new PokemonChartElement("Water", 0),new PokemonChartElement("Fire", 0),
                            new PokemonChartElement("Grass", 0),new PokemonChartElement("Fairy", -1),new PokemonChartElement("Normal", 0),
                            new PokemonChartElement("Dragon", 0),new PokemonChartElement("Fly", 0),new PokemonChartElement("Dark", -1),
                            new PokemonChartElement("Poison", 0),new PokemonChartElement("Warrior", 0),new PokemonChartElement("Hero", 0)
                        })
                    },
                    {
                        "Dark", new ReadOnlyCollection<PokemonChartElement>(new List<PokemonChartElement>()
                        {
                            // Dark wins all cards
                            new PokemonChartElement("Electric", 1),new PokemonChartElement("Water", 1),new PokemonChartElement("Fire", 1),
                            new PokemonChartElement("Grass", 1),new PokemonChartElement("Fairy", 1),new PokemonChartElement("Normal", 1),
                            new PokemonChartElement("Dragon", 1),new PokemonChartElement("Fly", 1),new PokemonChartElement("Dark", 0),
                            new PokemonChartElement("Poison", 1),new PokemonChartElement("Warrior", 1),new PokemonChartElement("Hero", 1)
                        })
                    },
                    {
                        "Poison", new ReadOnlyCollection<PokemonChartElement>(new List<PokemonChartElement>()
                        {
                            new PokemonChartElement("Electric", 0),new PokemonChartElement("Water", 0),new PokemonChartElement("Fire", 0),
                            new PokemonChartElement("Grass", 0),new PokemonChartElement("Fairy", -1),new PokemonChartElement("Normal", 0),
                            new PokemonChartElement("Dragon", 0),new PokemonChartElement("Fly", 0),new PokemonChartElement("Dark", -1),
                            new PokemonChartElement("Poison", 0),new PokemonChartElement("Warrior", 0),new PokemonChartElement("Hero", 0)
                        })
                    },
                    {
                        "Warrior", new ReadOnlyCollection<PokemonChartElement>(new List<PokemonChartElement>()
                        {
                            new PokemonChartElement("Electric", 0),new PokemonChartElement("Water", 0),new PokemonChartElement("Fire", 0),
                            new PokemonChartElement("Grass", 0),new PokemonChartElement("Fairy", -1),new PokemonChartElement("Normal", 0),
                            new PokemonChartElement("Dragon", 0),new PokemonChartElement("Fly", 0),new PokemonChartElement("Dark", -1),
                            new PokemonChartElement("Poison", 0),new PokemonChartElement("Warrior", 0),new PokemonChartElement("Hero", 0)
                        })
                    },
                    {
                        "Hero", new ReadOnlyCollection<PokemonChartElement>(new List<PokemonChartElement>()
                        {
                            new PokemonChartElement("Electric", 0),new PokemonChartElement("Water", 0),new PokemonChartElement("Fire", 0),
                            new PokemonChartElement("Grass", 0),new PokemonChartElement("Fairy", -1),new PokemonChartElement("Normal", 0),
                            new PokemonChartElement("Dragon", 0),new PokemonChartElement("Fly", 0),new PokemonChartElement("Dark", -1),
                            new PokemonChartElement("Poison", 0),new PokemonChartElement("Warrior", 0),new PokemonChartElement("Hero", 0)
                        })
                    }
                });

        #endregion

        /// <summary>
        /// Decide which card win or tie base on a fix game rule table
        /// </summary>
        /// <param name="gameSessionInfo"></param>
        /// <returns></returns>
        public Result<GameResult> CalculateGameResult(GameSessionInfo gameSessionInfo, string currentUserId)
        {
            if (gameSessionInfo == null)
                return Result.Fail<GameResult>("Không có dữ liệu trận đấu");

            var cardService = _serviceFactory.GetService<IPokeCardService>();

            var playerCardInfo1 = cardService.GetCardInfoForUser(gameSessionInfo.PlayerUserId1, gameSessionInfo.PlayerCard1).Unwrap(null);
            var playerCardInfo2 = cardService.GetCardInfoForUser(gameSessionInfo.PlayerUserId2, gameSessionInfo.PlayerCard2).Unwrap(null);

            if (playerCardInfo1 == null || playerCardInfo2 == null)
                return Result.Fail<GameResult>("Thẻ không hợp lệ được sử dụng");

            // Compare element point of card 1 against card 2
            var gameResult = new GameResult();

            if (playerCardInfo1.IsSpecialCard && !playerCardInfo2.IsSpecialCard)
            {
                // card 1 wins over card 2 does not need to check element table
                gameResult = SetGameResultCard1WinOverCard2(gameSessionInfo, wonCardInfo1: playerCardInfo1, lostCardInfo2: playerCardInfo2);
                return Result.Success(gameResult);
            }
            else if (!playerCardInfo1.IsSpecialCard && playerCardInfo2.IsSpecialCard)
            {
                // card 2 wins over card 1 does not need to check element table
                gameResult = SetGameResultCard2WinOverCard1(gameSessionInfo, wonCardInfo1: playerCardInfo2, lostCardInfo2: playerCardInfo1);
                return Result.Success(gameResult);
            }
            else if (PokemonElementCharts.TryGetValue(playerCardInfo1.CardMainElement,
                    out ReadOnlyCollection<PokemonChartElement> card1VsCard2ChartElements))
            {
                var card1VsCard2Info =
                    card1VsCard2ChartElements.FirstOrDefault(x => x.ElementName == playerCardInfo2.CardMainElement);

                if(card1VsCard2Info == null)
                    return Result.Fail<GameResult>("Invalid game rules");

                if (card1VsCard2Info.Weight > 0 
                        || (card1VsCard2Info.Weight == 0 && playerCardInfo1.CardPoint > playerCardInfo2.CardPoint))
                {
                    // card 1 win over card 2
                    gameResult = SetGameResultCard1WinOverCard2(gameSessionInfo, wonCardInfo1: playerCardInfo1, lostCardInfo2: playerCardInfo2);
                    return Result.Success(gameResult);
                }
                else if (card1VsCard2Info.Weight < 0 
                        || (card1VsCard2Info.Weight == 0 && playerCardInfo1.CardPoint < playerCardInfo2.CardPoint))
                {
                    // card 2 win over card 1
                    gameResult = SetGameResultCard2WinOverCard1(gameSessionInfo, wonCardInfo1: playerCardInfo2, lostCardInfo2: playerCardInfo1);
                    return Result.Success(gameResult);
                }
                else if (card1VsCard2Info.Weight == 0 && playerCardInfo1.CardPoint == playerCardInfo2.CardPoint)
                {
                    // game tie
                    gameResult = SetGameResultCard1DrawCard2(gameSessionInfo, currentUserId, playerCardInfo1, playerCardInfo2);
                    return Result.Success(gameResult);
                }

                return Result.Success(gameResult);
            }

            return Result.Fail<GameResult>($"Unable to get game result for session {gameSessionInfo.Id}");
        }

        private GameResult SetGameResultCard1WinOverCard2(GameSessionInfo gameSessionInfo, CardInfo wonCardInfo1, CardInfo lostCardInfo2)
        {
            var gameResult = new GameResult
            {
                HasWinner = true,

                WonCardId = wonCardInfo1.CardId,
                WonPoints = wonCardInfo1.CardValuePoint,
                WonCardImageUrl = wonCardInfo1.CardImageUrl,
                WonCardElementImageUrl = wonCardInfo1.CardElementImageUrl,
                WonCardElementDisplay = wonCardInfo1.CardElementDisplay,
                WonIsSpecialCard = wonCardInfo1.IsSpecialCard,
                WonCardTypeDisplay = wonCardInfo1.CardTypeDisplay,

                WonUserId = gameSessionInfo.PlayerUserId1,
                WonUserName = gameSessionInfo.PlayerUserName1,

                LostCardId = lostCardInfo2.CardId,
                LostPoints = lostCardInfo2.CardValuePoint,
                LostCardImageUrl = lostCardInfo2.CardImageUrl,
                LostCardElementImageUrl = lostCardInfo2.CardElementImageUrl,
                LostCardElementDisplay = lostCardInfo2.CardElementDisplay,
                LostIsSpecialCard = lostCardInfo2.IsSpecialCard,
                LostCardTypeDisplay = lostCardInfo2.CardTypeDisplay,

                LostUserId = gameSessionInfo.PlayerUserId2,
                LostUserName = gameSessionInfo.PlayerUserName2
            };

            return gameResult;
        }

        private GameResult SetGameResultCard2WinOverCard1(GameSessionInfo gameSessionInfo, CardInfo wonCardInfo1, CardInfo lostCardInfo2)
        {
            var gameResult = new GameResult
            {
                HasWinner = true,

                WonCardId = wonCardInfo1.CardId,
                WonPoints = wonCardInfo1.CardValuePoint,
                WonCardImageUrl = wonCardInfo1.CardImageUrl,
                WonCardElementImageUrl = wonCardInfo1.CardElementImageUrl,
                WonCardElementDisplay = wonCardInfo1.CardElementDisplay,
                WonIsSpecialCard = wonCardInfo1.IsSpecialCard,
                WonCardTypeDisplay = wonCardInfo1.CardTypeDisplay,

                WonUserId = gameSessionInfo.PlayerUserId2,
                WonUserName = gameSessionInfo.PlayerUserName2,

                LostCardId = lostCardInfo2.CardId,
                LostPoints = lostCardInfo2.CardValuePoint,
                LostCardImageUrl = lostCardInfo2.CardImageUrl,
                LostCardElementImageUrl = lostCardInfo2.CardElementImageUrl,
                LostCardElementDisplay = lostCardInfo2.CardElementDisplay,
                LostIsSpecialCard = lostCardInfo2.IsSpecialCard,
                LostCardTypeDisplay = lostCardInfo2.CardTypeDisplay,

                LostUserId = gameSessionInfo.PlayerUserId1,
                LostUserName = gameSessionInfo.PlayerUserName1
            };

            return gameResult;
        }

        private GameResult SetGameResultCard1DrawCard2(GameSessionInfo gameSessionInfo, string currentUserId, CardInfo playerCardInfo1, CardInfo playerCardInfo2)
        {
            var gameResult = new GameResult();

            gameResult.HasWinner = false;
            gameResult.GameTie = true;

            // set user id and user name just for information
            gameResult.WonCardId = playerCardInfo1.CardId;
            gameResult.WonPoints = playerCardInfo1.CardValuePoint;
            gameResult.WonCardImageUrl = playerCardInfo1.CardImageUrl;
            gameResult.WonCardElementImageUrl = playerCardInfo1.CardElementImageUrl;
            gameResult.WonCardElementDisplay = playerCardInfo1.CardElementDisplay;
            gameResult.WonIsSpecialCard = playerCardInfo1.IsSpecialCard;
            gameResult.WonCardTypeDisplay = playerCardInfo1.CardTypeDisplay;

            gameResult.WonUserId = gameSessionInfo.PlayerUserId1;
            gameResult.WonUserName = gameSessionInfo.PlayerUserName1;

            gameResult.LostCardId = playerCardInfo2.CardId;
            gameResult.LostPoints = playerCardInfo2.CardValuePoint;
            gameResult.LostCardImageUrl = playerCardInfo2.CardImageUrl;
            gameResult.LostCardElementImageUrl = playerCardInfo2.CardElementImageUrl;
            gameResult.LostCardElementDisplay = playerCardInfo2.CardElementDisplay;
            gameResult.LostIsSpecialCard = playerCardInfo2.IsSpecialCard;
            gameResult.LostCardTypeDisplay = playerCardInfo2.CardTypeDisplay;

            gameResult.LostUserId = gameSessionInfo.PlayerUserId2;
            gameResult.LostUserName = gameSessionInfo.PlayerUserName2;

            // set card information depend on player context left vs right
            if (currentUserId.Equals(gameSessionInfo.PlayerUserId1))
            {
                gameResult.LeftCardId = playerCardInfo1.CardId;
                gameResult.LeftCardImageUrl = playerCardInfo1.CardImageUrl;
                gameResult.LeftCardElementImageUrl = playerCardInfo1.CardElementImageUrl;
                gameResult.LeftCardElementDisplay = playerCardInfo1.CardElementDisplay;
                gameResult.LeftIsSpecialCard = playerCardInfo1.IsSpecialCard;
                gameResult.LeftCardTypeDisplay = playerCardInfo1.CardTypeDisplay;

                gameResult.RightCardId = playerCardInfo2.CardId;
                gameResult.RightCardImageUrl = playerCardInfo2.CardImageUrl;
                gameResult.RightCardElementImageUrl = playerCardInfo2.CardElementImageUrl;
                gameResult.RightCardElementDisplay = playerCardInfo2.CardElementDisplay;
                gameResult.RightIsSpecialCard = playerCardInfo2.IsSpecialCard;
                gameResult.RightCardTypeDisplay = playerCardInfo2.CardTypeDisplay;
            }
            else if (currentUserId.Equals(gameSessionInfo.PlayerUserId2))
            {
                gameResult.LeftCardId = playerCardInfo2.CardId;
                gameResult.LeftCardImageUrl = playerCardInfo2.CardImageUrl;
                gameResult.LeftCardElementImageUrl = playerCardInfo2.CardElementImageUrl;
                gameResult.LeftCardElementDisplay = playerCardInfo2.CardElementDisplay;
                gameResult.LeftIsSpecialCard = playerCardInfo2.IsSpecialCard;
                gameResult.LeftCardTypeDisplay = playerCardInfo2.CardTypeDisplay;

                gameResult.RightCardId = playerCardInfo1.CardId;
                gameResult.RightCardImageUrl = playerCardInfo1.CardImageUrl;
                gameResult.RightCardElementImageUrl = playerCardInfo1.CardElementImageUrl;
                gameResult.RightCardElementDisplay = playerCardInfo1.CardElementDisplay;
                gameResult.RightIsSpecialCard = playerCardInfo1.IsSpecialCard;
                gameResult.RightCardTypeDisplay = playerCardInfo1.CardTypeDisplay;
            }

            return gameResult;
        }
    }
}

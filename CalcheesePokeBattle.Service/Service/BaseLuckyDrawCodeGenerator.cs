﻿using System;
using CalcheesePokeBattle.Common.Monads;
using CalcheesePokeBattle.DTO;
using CalcheesePokeBattle.Model;

namespace CalcheesePokeBattle.Service
{
    /// <summary>
    /// Quick implementation of code generator base on the Seed Identity field of Sql
    /// Leverage generated id to have unique increament code
    /// </summary>
    public abstract class BaseLuckyDrawCodeGenerator<TEntity> : DefaultServiceBase, ILuckyDrawCodeGenerator
        where TEntity : class, ILuckyDrawPoolForWeek, new()
    {
        protected BaseLuckyDrawCodeGenerator(IDbFactory dbFactory) : base(dbFactory)
        {
        }

        public Result<ILuckyDrawPoolForWeek> GenerateLuckyDrawCodeForWeek(GenerateLuckyDrawCodeForWeekCommand command, UserProfileDTO currentProfileDto)
        {
            // Validate input
            if (command == null || currentProfileDto == null)
                return Result.Fail<ILuckyDrawPoolForWeek>("Invalid operation");

            if(command.WeekNumber <= 0 || command.WeekNumber > 12)
                return Result.Fail<ILuckyDrawPoolForWeek>("Invalid week");

            var codePoolEntity = new TEntity
            {
                CreatedAt = DateTime.UtcNow,
                CreatedByUserId = currentProfileDto.Id,
                CreatedByUserName = currentProfileDto.UserName,
                WeekNumber = command.WeekNumber
            };

            DataContext.Set<TEntity>().Add(codePoolEntity);

            SaveChanges(typeof(TEntity).FullName);

            return Result.Success((ILuckyDrawPoolForWeek) codePoolEntity);
        }

        public Result<ILuckyDrawPoolForWeek> GenerateLuckyDrawCodeForCurrentWeek(ITrackableDTO command, UserProfileDTO currentProfileDto)
        {
            var currentWeek = GetCurrentWeekForToday();
            return GenerateLuckyDrawCodeForWeek(new GenerateLuckyDrawCodeForWeekCommand
            {
                WeekNumber = currentWeek.WeekNumber,
                ClientInfo = command.ClientInfo,
                ClientIpAddress = command.ClientIpAddress
            }, currentProfileDto);
        }
    }

    public class LuckyDrawCodeGeneratorForWeek1 : BaseLuckyDrawCodeGenerator<LuckyDrawPoolWeek1>, ILuckyDrawCodeGeneratorForWeek1
    {
        public LuckyDrawCodeGeneratorForWeek1(IDbFactory dbFactory) : base(dbFactory)
        {
        }
    }

    public class LuckyDrawCodeGeneratorForWeek2 : BaseLuckyDrawCodeGenerator<LuckyDrawPoolWeek2>, ILuckyDrawCodeGeneratorForWeek2
    {
        public LuckyDrawCodeGeneratorForWeek2(IDbFactory dbFactory) : base(dbFactory)
        {
        }
    }

    public class LuckyDrawCodeGeneratorForWeek3 : BaseLuckyDrawCodeGenerator<LuckyDrawPoolWeek3>, ILuckyDrawCodeGeneratorForWeek3
    {
        public LuckyDrawCodeGeneratorForWeek3(IDbFactory dbFactory) : base(dbFactory)
        {
        }
    }

    public class LuckyDrawCodeGeneratorForWeek4 : BaseLuckyDrawCodeGenerator<LuckyDrawPoolWeek4>, ILuckyDrawCodeGeneratorForWeek4
    {
        public LuckyDrawCodeGeneratorForWeek4(IDbFactory dbFactory) : base(dbFactory)
        {
        }
    }

    public class LuckyDrawCodeGeneratorForWeek5 : BaseLuckyDrawCodeGenerator<LuckyDrawPoolWeek5>, ILuckyDrawCodeGeneratorForWeek5
    {
        public LuckyDrawCodeGeneratorForWeek5(IDbFactory dbFactory) : base(dbFactory)
        {
        }
    }

    public class LuckyDrawCodeGeneratorForWeek6 : BaseLuckyDrawCodeGenerator<LuckyDrawPoolWeek6>, ILuckyDrawCodeGeneratorForWeek6
    {
        public LuckyDrawCodeGeneratorForWeek6(IDbFactory dbFactory) : base(dbFactory)
        {
        }
    }

    public class LuckyDrawCodeGeneratorForWeek7 : BaseLuckyDrawCodeGenerator<LuckyDrawPoolWeek7>, ILuckyDrawCodeGeneratorForWeek7
    {
        public LuckyDrawCodeGeneratorForWeek7(IDbFactory dbFactory) : base(dbFactory)
        {
        }
    }

    public class LuckyDrawCodeGeneratorForWeek8 : BaseLuckyDrawCodeGenerator<LuckyDrawPoolWeek8>, ILuckyDrawCodeGeneratorForWeek8
    {
        public LuckyDrawCodeGeneratorForWeek8(IDbFactory dbFactory) : base(dbFactory)
        {
        }
    }

    public class LuckyDrawCodeGeneratorForWeek9 : BaseLuckyDrawCodeGenerator<LuckyDrawPoolWeek9>, ILuckyDrawCodeGeneratorForWeek9
    {
        public LuckyDrawCodeGeneratorForWeek9(IDbFactory dbFactory) : base(dbFactory)
        {
        }
    }

    public class LuckyDrawCodeGeneratorForWeek10 : BaseLuckyDrawCodeGenerator<LuckyDrawPoolWeek10>, ILuckyDrawCodeGeneratorForWeek10
    {
        public LuckyDrawCodeGeneratorForWeek10(IDbFactory dbFactory) : base(dbFactory)
        {
        }
    }

    public class LuckyDrawCodeGeneratorForWeek11 : BaseLuckyDrawCodeGenerator<LuckyDrawPoolWeek11>, ILuckyDrawCodeGeneratorForWeek11
    {
        public LuckyDrawCodeGeneratorForWeek11(IDbFactory dbFactory) : base(dbFactory)
        {
        }
    }

    public class LuckyDrawCodeGeneratorForWeek12 : BaseLuckyDrawCodeGenerator<LuckyDrawPoolWeek12>, ILuckyDrawCodeGeneratorForWeek12
    {
        public LuckyDrawCodeGeneratorForWeek12(IDbFactory dbFactory) : base(dbFactory)
        {
        }
    }

    public class LuckyDrawCodeGeneratorForWeek13 : BaseLuckyDrawCodeGenerator<LuckyDrawPoolWeek13>, ILuckyDrawCodeGeneratorForWeek13
    {
        public LuckyDrawCodeGeneratorForWeek13(IDbFactory dbFactory) : base(dbFactory)
        {
        }
    }
}
﻿namespace CalcheesePokeBattle.Service
{
    public enum GiftKeyEnum
    {
        Balo,
        AoThun,
        Mu,
        TraiHe,
        VinID,
        XeDien,
        DoChoi
    }

    /// <summary>
    /// Enum defines setting keys
    /// </summary>
    public enum SettingKeys
    {
        WebAppImagesBaseUrl,
        ShowLuckyDrawPrizeOnFrontSiteWeek1,
        ShowLuckyDrawPrizeOnFrontSiteWeek2,
        ShowLuckyDrawPrizeOnFrontSiteWeek3,
        ShowLuckyDrawPrizeOnFrontSiteWeek4,
        ShowLuckyDrawPrizeOnFrontSiteWeek5,
        ShowLuckyDrawPrizeOnFrontSiteWeek6,
        ShowLuckyDrawPrizeOnFrontSiteWeek7,
        ShowLuckyDrawPrizeOnFrontSiteWeek8,
        ShowLuckyDrawPrizeOnFrontSiteWeek9,
        ShowLuckyDrawPrizeOnFrontSiteWeek10,
        ShowLuckyDrawPrizeOnFrontSiteWeek11,
        ShowLuckyDrawPrizeOnFrontSiteWeek12,
        ShowLuckyDrawPrizeOnFrontSiteWeek13
    }
}
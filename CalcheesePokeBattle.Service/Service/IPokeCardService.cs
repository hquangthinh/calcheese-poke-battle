﻿using System;
using System.Collections.Generic;
using System.Linq;
using CalcheesePokeBattle.Common.Monads;
using CalcheesePokeBattle.Common.Utils;
using CalcheesePokeBattle.DTO;
using CalcheesePokeBattle.Model;
using CalcheesePokeBattle.ViewModel;
using F = CalcheesePokeBattle.Common.Utils.Functions;
using CalcheesePokeBattle.Common.DTO;

namespace CalcheesePokeBattle.Service
{

    public class PokeCardCommandBase
    {
        public int Id { get; set; }
        public string CardName { get; set; }
    }

    public class SearchPokeCardCommand : KeywordSearchCommand
    {
        public string CardName { get; set; }
    }

    public class UpdatePokeCardCommand : PokeCardDTO
    {
    }

    /// <summary>
    /// Service to handle CRUD PokeCard, retrieve data for front site to use
    /// </summary>
    public interface IPokeCardService
    {
        /// <summary>
        /// Get all pokemon cards with detail information, total 42 cards atm
        /// </summary>
        /// <returns></returns>
        Result<List<PokeCardDTO>> GetAllPokemonCards();

        List<PokeCardViewModel> GetAllPokemonCardViewModels();

        IEnumerable<PokeCardReleaseViewModel> AllCardRelease();

        PokeCardDTO GetPokeCardDetailCache(int cardId);

        Result<CardInfo> GetCardInfo(int cardId);
        Result<CardInfo> GetCardInfoForUser(string userId, int cardId);
        PagingResult<PokeCardViewModel> SearchPokeCards(SearchPokeCardCommand command);
        Result<PokeCardDTO> GetPokeCardDetail(PokeCardCommandBase command);
        Result<PokeCardDTO> Update(UpdatePokeCardCommand command);
        Result<ResponseDTO> Delete(PokeCardCommandBase command);
    }

    public class DefaultPokeCardService : DefaultServiceBase, IPokeCardService
    {
        public DefaultPokeCardService(IDbFactory dbFactory) : base(dbFactory)
        {
        }

        private static readonly Func<CacheCommand<DefaultPokeCardService>, Func<Result<List<PokeCardDTO>>>>
            GetAllPokeCardDTOCache
                = Functions.Lambda((DefaultPokeCardService ctx) => ctx.GetAllPokemonCardsInternal()).MemoizeCtx();

        private Result<List<PokeCardDTO>> GetAllPokemonCardsInternal()
            => Result.Success(
                DataContext.PokeCards
                    .OrderBy(c => c.CardGroup).ThenBy(c => c.Id)
                    .Select(PokeCardDTO.EntityToDtoMapper).ToList()
            );

        public Result<List<PokeCardDTO>> GetAllPokemonCards()
            => GetAllPokeCardDTOCache(this.AsCacheContext())();


        private static readonly Func<CacheCommand<DefaultPokeCardService>, Func<List<PokeCardViewModel>>>
            GetAllPokeCardViewModelCache
                = Functions.Lambda((DefaultPokeCardService ctx) => ctx.GetAllPokemonCardViewModelsInternal()).MemoizeCtx();

        private List<PokeCardViewModel> GetAllPokemonCardViewModelsInternal()
        {
            var imageBaseUrl = GetSettingValue(SettingKeys.WebAppImagesBaseUrl);
            return GetAllPokemonCards()
                .Value.Select(cardDto => PokeCardViewModel.DtoToViewModelMapper(cardDto, imageBaseUrl))
                .ToList();
        }

        public List<PokeCardViewModel> GetAllPokemonCardViewModels()
            => GetAllPokeCardViewModelCache(this.AsCacheContext())();

        public PokeCardDTO GetPokeCardDetailCache(int cardId)
            => GetCardDetailDtoCache(this.AsCacheContext())(cardId);

        private static readonly Func<CacheCommand<DefaultPokeCardService>, Func<int, PokeCardDTO>> GetCardDetailDtoCache = F
            .Lambda((DefaultPokeCardService ctx, int cardId) => ctx.GetPokeCardDetailCacheInternal(cardId))
            .MemoizeCtx();

        private PokeCardDTO GetPokeCardDetailCacheInternal(int cardId)
        {
            var cardEntity = DataContext.PokeCards.Find(cardId);
            return cardEntity != null ? PokeCardDTO.EntityToDtoMapper(cardEntity) : new PokeCardDTO();
        }

        private static readonly Func<CacheCommand<DefaultPokeCardService>, Func<int, Result<CardInfo>>> GetCardInfoCache = F
            .Lambda((DefaultPokeCardService ctx, int cardId) => ctx.GetCardInternal(cardId))
            .MemoizeCtx();

        /// <summary>
        /// Get card by cardId
        /// should have cache
        /// </summary>
        /// <param name="cardId"></param>
        /// <returns></returns>
        public Result<CardInfo> GetCardInfo(int cardId)
            => GetCardInfoCache(this.AsCacheContext())(cardId);

        private Result<CardInfo> GetCardInternal(int cardId)
        {
            var cardEntity = DataContext.PokeCards.FirstOrDefault(c => c.Id == cardId);
            var imgBaseUrl = "https://storagecalcheeseprod.blob.core.windows.net/images";
            return cardEntity != null
                ? Result.Success(CardInfo.Mapper(cardEntity, imgBaseUrl))
                : Result.Fail<CardInfo>("Invalid card");
        }

        public Result<CardInfo> GetCardInfoForUser(string userId, int cardId)
        {
            return GetCardInfo(cardId)
                .OnSuccess(cardInfo =>
                {
                    cardInfo.CardValuePoint = GetCardValuePointForUserInternal(userId, cardId, cardInfo.CardPoint);
                    return cardInfo;
                });
        }

        // cache does not work for this case

        //private static readonly Func<CacheCommand<DefaultPokeCardService>, Func<string, int, int, int>>
        //    GetCardValuePointForUserCache =
        //        F.Lambda((DefaultPokeCardService ctx, string userId, int cardId, int defaultCardPoint)
        //            => ctx.GetCardValuePointForUserInternal(userId, cardId, defaultCardPoint)).MemoizeCtx();

        //public int GetCardValuePointForUser(string userId, int cardId, int defaultCardPoint)
        //    => GetCardValuePointForUserCache(this.AsCacheContext())(userId, cardId, defaultCardPoint);

        /// <summary>
        /// Get the min point of user card matches user id & card id
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="cardId"></param>
        /// <param name="defaultCardPoint"></param>
        /// <returns></returns>
        private int GetCardValuePointForUserInternal(string userId, int cardId, int defaultCardPoint)
        {
            var userCardInfo =
                DataContext.UserCardCollections
                    .Where(uc => uc.UserId == userId && uc.CardId == cardId)
                    .OrderBy(uc => uc.CardPoint)
                    .FirstOrDefault();
            return userCardInfo != null ? userCardInfo.CardPoint.GetValueOrDefault(0) : defaultCardPoint;
        }

        public Result<PokeCardDTO> CreatePokeCard(PokeCardDTO dto)
        {
            DataContext.PokeCards.Add(PokeCardDTO.DtoToEntityMapper(dto));
            SaveChanges("PokeCards", dto.Id.ToString());
            return Result.Success(dto);
        }

        public Result<PokeCardDTO> GetPokeCardDetail(PokeCardCommandBase command)
        {
            var giftItem = command.Id > 0
                ? DataContext.PokeCards.Find(command.Id)
                : DataContext.PokeCards.FirstOrDefault(
                    p => string.Compare(p.CardName, command.CardName, StringComparison.InvariantCultureIgnoreCase) == 0);

            return giftItem == null
                ? Result.Fail<PokeCardDTO>($"Cannot find user with Id -> {command.CardName}")
                : Result.Success(PokeCardDTO.EntityToDtoMapper(giftItem));
        }

        public Result<PokeCardDTO> Update(UpdatePokeCardCommand command)
        {
            throw new NotImplementedException();
        }

        public PagingResult<PokeCardViewModel> SearchPokeCards(SearchPokeCardCommand command)
        {
            if (command == null)
                return PagingResult<PokeCardViewModel>.Empty();
            var predicate =
                Functions.Lambda<PokeCard, bool>(
                    pokecard => (
                        (string.IsNullOrEmpty(command.CardName) || pokecard.CardName == command.CardName)
                    ));

            if (command.PageNumber == 1 && command.PageSize == 7)
            {
                var allCards = GetAllPokemonCardViewModels();
                var totalCount = allCards.Count;
                var rs = allCards.Where(c => c.IsSpecialCard);
                return new PagingResult<PokeCardViewModel>(rs)
                {
                    PageSize = command.PageSize,
                    PageNumber = 1,
                    PageTotal = 5,
                    ResultCount = totalCount
                };
            }
            else if(command.PageNumber  > 1 && command.PageSize == 10)
            {
                command.PageNumber = command.PageNumber - 1;
                var allCards = GetAllPokemonCardViewModels();
                var totalCount = allCards.Count;
                var rs = allCards.Where(c => !c.IsSpecialCard).Paginate(command);
                return new PagingResult<PokeCardViewModel>(rs)
                {
                    PageSize = command.PageSize,
                    PageNumber = ++command.PageNumber,
                    PageTotal = 5,
                    ResultCount = totalCount
                };
            }
            else
            {
                var rs = GetAllPokemonCardViewModels();
                return new PagingResult<PokeCardViewModel>(rs)
                {
                    PageSize = int.MaxValue,
                    PageNumber = command.PageNumber,
                    PageTotal = 1,
                    ResultCount = rs.Count
                };
            }
        }

        public Result<ResponseDTO> Delete(PokeCardCommandBase command)
        {
            throw new Exception();
        }

        

        public IEnumerable<PokeCardReleaseViewModel> AllCardRelease()
        {
            var allCards = GetAllPokemonCardViewModels();
            var listCardRelease = new List<PokeCardReleaseViewModel>();
            foreach(var card in allCards)
            {
                listCardRelease.Add(new PokeCardReleaseViewModel
                {
                    CardName = card.CardName,
                    ImageUrl = card.ImageUrl,
                    TotalRelease = DataContext.UserCardCollections.Where(c=>c.CardId == card.Id).Count(),
                    Total = 338095
                });
            }
            return listCardRelease;
        }
    }
}
﻿using System;
using Castle.Windsor;

namespace CalcheesePokeBattle.Service
{
    public interface IServiceFactory
    {
        TService GetService<TService>();

        TService GetService<TService>(string key);

        /// <summary>
        /// Checks whether given type is registered before.
        /// </summary>
        /// <param name="type">Type to check</param>
        bool IsRegistered(Type type);

        /// <summary>
        /// Checks whether given type is registered before.
        /// </summary>
        /// <typeparam name="T">Type to check</typeparam>
        bool IsRegistered<T>();
    }

    public class DefaultWindsorServiceFactory : IServiceFactory
    {
        protected readonly IWindsorContainer Container;

        public DefaultWindsorServiceFactory(IWindsorContainer container)
        {
            Container = container;
        }

        public TService GetService<TService>()
        {
            return Container.Resolve<TService>();
        }

        public TService GetService<TService>(string key)
        {
            return Container.Resolve<TService>(key);
        }

        public bool IsRegistered(Type type)
        {
            return Container.Kernel.HasComponent(type);
        }

        public bool IsRegistered<T>()
        {
            return Container.Kernel.HasComponent(typeof(T));
        }
    }
}

﻿using System;
using System.Collections.Generic;
using CalcheesePokeBattle.Common.Monads;
using CalcheesePokeBattle.DTO;
using CalcheesePokeBattle.Model;

namespace CalcheesePokeBattle.Service
{
    /// <summary>
    /// Service to get application settings, define configuration key-value as needed
    /// Also dealing with other settings
    /// </summary>
    public interface ISettingService
    {
        List<SettingDTO> GetAll();

        string GetStringValue(SettingKeys setting);

        string GetStringValue(SettingKeys setting, string defaultValue);

        int GetIntValue(SettingKeys setting);

        bool IsAffirmative(SettingKeys setting);

        Result ResetSettingCache();

        Result<WeekDefinitionDTO> GetWeekFromDate(DateTime inputDateTime);

        Result<WeekDefinitionDTO> GetWeekFromCurrentDate();

        Result<GiftAmountPerWeek> GetTotalGiftAvailableForWeek(int weekNumber, string giftKey);

        Result UpdateSettingValue(SettingKeys settingKey, string value);

        Result UpdateShowLuckyDrawPrizeOnFrontSiteForWeek(int weekNumber);

        Result UpdateHideLuckyDrawPrizeOnFrontSiteForWeek(int weekNumber);

        bool ShouldShowWinnerListForPublicView(int weekNumber);

    }
}

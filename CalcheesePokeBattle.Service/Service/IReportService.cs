﻿using System;
using System.Collections.Generic;
using System.Linq;
using CalcheesePokeBattle.Common.Monads;
using CalcheesePokeBattle.DTO;
using CalcheesePokeBattle.Model;
using CalcheesePokeBattle.ViewModel;

namespace CalcheesePokeBattle.Service
{
    public class ReportQuery
    {
        public int WeekNumber { get; set; }
    }

    public class GiftSummaryByWeekViewModel
    {
        public int WeekNumber { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }

        public int TotalAllowBackpack { get; set; }
        public int TotalBackpackToDate { get; set; }

        public int TotalAllowHat { get; set; }
        public int TotalHatToDate { get; set; }

        public int TotalAllowTShirt { get; set; }
        public int TotalTShirtToDate { get; set; }
    }
    /// <summary>
    /// Query data for some reports gift by week, lucky draw code, winners,...
    /// </summary>
    public interface IReportService
    {
        List<ImportJobStatusDTO> GetAllImportJobStatus();

        GiftSummaryByWeekViewModel GetGiftSummaryByWeek(int weekNumber);

        PagingResult<AllGiftRedeemHistoryViewModel> GetAllGiftRedeemByWeek(ReportQuery query);

        List<AllGiftRedeemHistoryExportModel> GetAllGiftRedeemForExportByWeek(ReportQuery query);

        UserCouponCodeStatisticModel UserCouponCodeStatistic();
    }

    public class DefaultReportService : DefaultServiceBase, IReportService
    {
        public DefaultReportService(IDbFactory dbFactory) : base(dbFactory)
        {
        }

        public List<ImportJobStatusDTO> GetAllImportJobStatus()
        {
            var allImportJobs = DataContext.ImportJobStatuses.ToList().OrderByDescending(job => job.Id)
                .Select(ImportJobStatusDTO.EntityMapper)
                .ToList();

            var queryTotalImportCode = @"SELECT 
                                            [RowCount] = MAX(si.rows)
                                        FROM 
                                            sysobjects so, 
                                            sysindexes si 
                                        WHERE 
                                            so.xtype = 'U' 
                                            AND 
                                            si.id = OBJECT_ID(so.name) 
	                                        AND so.name = 'CouponCode'
                                        GROUP BY so.name";

            var totalImportedCouponCode = DataContext.Database.SqlQuery<int>(queryTotalImportCode).FirstOrDefault();

            allImportJobs.ForEach(job => job.TotalImportedCouponCode = totalImportedCouponCode);

            return allImportJobs;
        }

        public GiftSummaryByWeekViewModel GetGiftSummaryByWeek(int weekNumber)
        {
            var allGifts = DataContext.GiftItems.ToDictionary(x => x.Name, x => x.Id);
            var giftAmountPerWeek = DataContext.GiftAmountPerWeeks.Where(x => x.WeekNumber == weekNumber)
                .ToDictionary(x => x.GiftName, x => x.TotalPrize);
            
            var backPackGiftId = allGifts["Balo"];
            var totalClaimBackPack =
                DataContext.GiftRedeemHistories.Count(x => x.WeekNumber == weekNumber && x.GiftId == backPackGiftId);

            var hatGiftId = allGifts["Mu"];
            var totalClaimHat =
                DataContext.GiftRedeemHistories.Count(x => x.WeekNumber == weekNumber && x.GiftId == hatGiftId);

            var tshirtGiftId = allGifts["AoThun"];
            var totalClaimTshirt =
                DataContext.GiftRedeemHistories.Count(x => x.WeekNumber == weekNumber && x.GiftId == tshirtGiftId);

            var weekInfo = DataContext.WeekDefinitions.FirstOrDefault(x => x.WeekNumber == weekNumber);

            if (weekInfo == null)
                return new GiftSummaryByWeekViewModel();

            return new GiftSummaryByWeekViewModel
            {
                WeekNumber = weekNumber,
                FromDate = weekInfo.WeekStart,
                ToDate = weekInfo.WeekEnd,

                TotalAllowBackpack = giftAmountPerWeek["Balo"],
                TotalBackpackToDate = totalClaimBackPack,

                TotalAllowHat = giftAmountPerWeek["Mu"],
                TotalHatToDate = totalClaimHat,

                TotalAllowTShirt = giftAmountPerWeek["AoThun"],
                TotalTShirtToDate = totalClaimTshirt
            };
        }

        public PagingResult<AllGiftRedeemHistoryViewModel> GetAllGiftRedeemByWeek(ReportQuery query)
        {
            if(query == null)
                return PagingResult<AllGiftRedeemHistoryViewModel>.Empty();

            var getGiftRedeemQuery = $"SELECT * FROM vwAllGiftRedeemHistory WHERE WeekNumber={query.WeekNumber} ORDER BY GiftId";

            var giftList = DataContext.Database.SqlQuery<AllGiftRedeemHistoryViewModel>(getGiftRedeemQuery).ToList();

            return PagingResult<AllGiftRedeemHistoryViewModel>.AllResult(giftList);
        }

        public List<AllGiftRedeemHistoryExportModel> GetAllGiftRedeemForExportByWeek(ReportQuery query)
        {
            if(query ==null)
                return new List<AllGiftRedeemHistoryExportModel>();

            var getGiftRedeemQuery = $@"SELECT WeekNumber, GiftDescription, UserName, 
                                                FirstName, LastName, PhoneNumber, 
                                                Address, DeliveryAddress, AddressNumber, 
                                                StreetName, WardName, DistrictName, CityName 
                                        FROM vwAllGiftRedeemHistory WHERE WeekNumber={query.WeekNumber} ORDER BY GiftId";

            return DataContext.Database.SqlQuery<AllGiftRedeemHistoryExportModel>(getGiftRedeemQuery).ToList();
        }

        public UserCouponCodeStatisticModel UserCouponCodeStatistic()
        {
            var userRegistrationByDateQuery = @"select DATEADD(dd, DATEDIFF(dd, 0, CreatedDate), 0) as UserCreatedDate , count(*) as TotalRegistration from UserProfile
                                                group by DATEADD(dd, DATEDIFF(dd, 0, CreatedDate), 0)
                                                order by UserCreatedDate asc";

            var userRegistrationByDateWithInputCardQuery = @"select DATEADD(dd, DATEDIFF(dd, 0, p.CreatedDate), 0) as UserCreatedDate , count(*) as TotalRegistration from UserProfile p
                                                            where p.Id in (select UserId from UsedCouponCode)
                                                            group by DATEADD(dd, DATEDIFF(dd, 0, p.CreatedDate), 0)
                                                            order by UserCreatedDate asc";

            var userPlayGameSummaryByDateQuery = @"select DATEADD(dd, DATEDIFF(dd, 0, g.CreatedDate), 0) as ReportDate, count(*) as Total
                                                    from GameHistory g
                                                    group by DATEADD(dd, DATEDIFF(dd, 0, g.CreatedDate), 0)
                                                    order by ReportDate asc";

            return new UserCouponCodeStatisticModel
            {
                TotalCode = 14209283,
                TotalCodeInUsed = DataContext.UsedCouponCodes.Count(),
                TotalUser = DataContext.UserProfiles.Count(),
                TotalUserInPlay = DataContext.UsedCouponCodes.GroupBy(u=>u.UserId).Count(),
                TotalUserRegistrationSummaryByDate = DataContext.Database.SqlQuery<UserRegistrationSummaryByDateDTO>(userRegistrationByDateQuery).ToList(),
                TotalUserRegistrationWithInputCardSummaryByDate = DataContext.Database.SqlQuery<UserRegistrationSummaryByDateDTO>(userRegistrationByDateWithInputCardQuery).ToList(),
                TotalUserPlayGame = DataContext.Database.SqlQuery<SummaryByDateReportDTO>(userPlayGameSummaryByDateQuery).ToList()
            };
        }
    }
}
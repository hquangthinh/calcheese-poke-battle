﻿using System;
using System.Collections.Generic;
using System.Linq;
using CalcheesePokeBattle.Common.Monads;
using CalcheesePokeBattle.DTO;
using CalcheesePokeBattle.Model;
using CalcheesePokeBattle.ViewModel;

namespace CalcheesePokeBattle.Service
{
    public class ServerInfoCommand : Paging
    {
        public string RequestUserId { get; set; }

        public string ServerName { get; set; }

        public string ServerIpAddress { get; set; }

        public static ServerInfoCommand Default() => new ServerInfoCommand();
    }

    public class GameSessionQuery
    {
        public int GameSessionId { get; set; }

        public string RequestUserName { get; set; }

        public string RequestUserId { get; set; }
    }

    public class ResumeGameSessionQuery : GameSessionQuery
    {
        public string PlayerUserId1 { get; set; }

        public string PlayerUserName1 { get; set; }

        public string PlayerUserId2 { get; set; }

        public string PlayerUserName2 { get; set; }

        public string XKey { get; set; }
    }

    /// <summary>
    /// Define contracts to handle logic for game server such as client connection, player online
    /// </summary>
    public interface IPokeCardGameServerService
    {
        Result<PagingResult<OnlinePlayerDTO>> GetAllOnlinePlayers(ServerInfoCommand command, UserProfileDTO currentUser);

        /// <summary>
        /// Add new player connection when a new player goes online
        /// </summary>
        /// <param name="playerConnectionInfo"></param>
        /// <param name="currentUser"></param>
        /// <returns></returns>
        Result<PlayerConnectionInfoDTO> NewPlayerOnline(PlayerConnectionInfoDTO playerConnectionInfo, UserProfileDTO currentUser);

        Result<PlayerConnectionInfoDTO> PlayerReconnect(PlayerConnectionInfoDTO playerConnectionInfo, UserProfileDTO currentUser);

        Result<PlayerDisconnectInfo> PlayerDisconnect(PlayerConnectionInfoDTO playerConnectionInfo, UserProfileDTO currentUser);

        Result<List<string>> DisconnectGame(GameSessionQuery query);

        /// <summary>
        /// Call when server is restarted or stopped
        /// </summary>
        /// <returns></returns>
        Result DisconnectAllPlayers();

        Result<GamePlayingPageViewModel> GetGamePlayInfoFromGameSession(GameSessionQuery query, UserProfileDTO currentUser);

        Result<GameSessionInfo> StopGameRequest(GameSessionQuery query);
    }

    public class DefaultPokeCardGameServerService : DefaultServiceBase, IPokeCardGameServerService
    {
        public DefaultPokeCardGameServerService(IDbFactory dbFactory) : base(dbFactory)
        {
        }

        public Result<PagingResult<OnlinePlayerDTO>> GetAllOnlinePlayers(ServerInfoCommand command, UserProfileDTO currentUser)
        {
            if(command == null || currentUser == null)
                return Result.Success(PagingResult<OnlinePlayerDTO>.Empty());

            var allOnlinePlayersOnServer = DataContext.AllOnlinePlayerConnectionInfos.Where(
                    info =>
                        (string.IsNullOrEmpty(command.ServerName) || string.IsNullOrEmpty(command.ServerIpAddress) 
                            || info.ServerName == command.ServerName || info.ServerIpAddress == command.ServerIpAddress)
                                && info.UserId != currentUser.Id)
                .OrderByDescending(x => x.Id)
                .Select(OnlinePlayerDTO.ViewEntityToDtoMapper)
                .ToList();

            var totalCount = allOnlinePlayersOnServer.Count;
            var pagedList = allOnlinePlayersOnServer
                .GroupBy(x => x.UserName).Select(x => x.First())
                .OrderByDescending(x => x.TotalPoints)
                .ThenByDescending(x => x.ConnectedAt)
                .Paginate(command);

            return
                Result.Success(PagingResult<OnlinePlayerDTO>.FromResult(pagedList, 0, totalCount, command.PageSize,
                    command.PageNumber));
        }

        public Result<PlayerConnectionInfoDTO> NewPlayerOnline(PlayerConnectionInfoDTO playerConnectionInfo, UserProfileDTO currentUser)
        {
            var userProfileCheckResult = GetService<IUserProfileService>().CheckUserExisting(currentUser.Id);
            if (userProfileCheckResult.IsFailure)
            {
                return
                    Result.Fail<PlayerConnectionInfoDTO>(
                        $"User {playerConnectionInfo.UserName} does not exist in system");
            }

            var playerInfoEntity =
                    DataContext.PlayerConnectionInfos
                                    .FirstOrDefault(p => p.UserId == playerConnectionInfo.UserId
                                                            && p.ConnectionId == playerConnectionInfo.ConnectionId);
            if (playerInfoEntity == null)
            {
                playerInfoEntity = PlayerConnectionInfoDTO.DtoToEntityMapper(playerConnectionInfo);
                DataContext.PlayerConnectionInfos.Add(playerInfoEntity);
            }
            else
            {
                playerInfoEntity.Status = playerConnectionInfo.Status;
                playerInfoEntity.ConnectedAt = DateTime.UtcNow;
            }

            SaveChanges("PlayerConnectionInfo");
            playerConnectionInfo.Id = playerInfoEntity.Id;

            return Result.Success(playerConnectionInfo);
        }

        public Result<PlayerConnectionInfoDTO> PlayerReconnect(PlayerConnectionInfoDTO playerConnectionInfo, UserProfileDTO currentUser)
        {
            return NewPlayerOnline(playerConnectionInfo, currentUser);
        }

        public Result<PlayerDisconnectInfo> PlayerDisconnect(PlayerConnectionInfoDTO playerConnectionInfo, UserProfileDTO currentUser)
        {
            //var userProfileCheckResult = GetService<IUserProfileService>().CheckUserExisting(currentUser.Id);
            //if (userProfileCheckResult.IsFailure)
            //{
            //    return
            //        Result.Fail<PlayerConnectionInfoDTO>(
            //            $"User {playerConnectionInfo.UserName} does not exist in system");
            //}

            //var playerInfoEntity =
            //        DataContext.PlayerConnectionInfos
            //                        .FirstOrDefault(p => p.UserId == playerConnectionInfo.UserId
            //                                                && p.ConnectionId == playerConnectionInfo.ConnectionId);
            //if (playerInfoEntity != null)
            //{
            //    DataContext.PlayerConnectionInfos.Remove(playerInfoEntity);
            //    SaveChanges("PlayerConnectionInfo");
            //}

            var affectedUserIds = DisconnectGame(new GameSessionQuery {RequestUserId = currentUser.Id}).Value;

            return Result.Success(new PlayerDisconnectInfo {AffectedUserIds = affectedUserIds});
        }

        public Result DisconnectAllPlayers()
        {
            try
            {
                Logger.Warn("Disconnect all players, this is maybe from server restart");
                DataContext.ExecspDisconnectAllPlayers();
            }
            catch (Exception ex)
            {
                Logger.Error($"Error while trying to disconnect all players -> ", ex);
            }
            return Result.Success();
        }

        public Result<GamePlayingPageViewModel> GetGamePlayInfoFromGameSession(GameSessionQuery query, UserProfileDTO currentUser)
        {
            if (query == null || currentUser == null)
                return Result.Fail<GamePlayingPageViewModel>("Game không hợp lệ");

            // security check for game session

            var gameState = GetService<IGameSessionService>().GetGameRound(query.GameSessionId);

            if (gameState == null)
                return Result.Fail<GamePlayingPageViewModel>("Game không hợp lệ");

            if (string.Compare(gameState.PlayerUserId1, currentUser.Id, StringComparison.OrdinalIgnoreCase) != 0
                && string.Compare(gameState.PlayerUserId2, currentUser.Id, StringComparison.OrdinalIgnoreCase) != 0)
            {
                // some one hijack the game url of 2 players
                return Result.Fail<GamePlayingPageViewModel>("Game không hợp lệ");
            }

            // validate game sessionId with db storage

            var gameSession = DataContext.GameSessions.Find(query.GameSessionId);

            if(gameSession == null)
                return Result.Fail<GamePlayingPageViewModel>("Game không hợp lệ");

            var userProfileService = GetService<IUserProfileService>();

            var fetchPlayer1Query = new UserCommandBase
            {
                UserId = gameSession.PlayerUserId1,
                PopulateAllUserCardsInfo = true
            };

            var fetchPlayer2Query = new UserCommandBase
            {
                UserId = gameSession.PlayerUserId2,
                PopulateAllUserCardsInfo = true
            };

            var gamePlayVm = new GamePlayingPageViewModel
            {
                GameSessionId = gameSession.Id
            };
            

            if (currentUser.Id.Equals(gameSession.PlayerUserId1))
            {
                gamePlayVm.PlayerOnLeft = userProfileService.GetUserProfileViewModel(fetchPlayer1Query).Value;
                gamePlayVm.ThinkTimeRemain = gameState.ThinkTimeRemainForPlayer1;

                // only return subset of user profile vm
                var userOnRightVm = userProfileService.GetUserProfileViewModelMainProperties(fetchPlayer2Query).Value;
                gamePlayVm.PlayerOnRight = UserProfileViewModel.SubSetDataMapper(userOnRightVm);
            }
            else if(currentUser.Id.Equals(gameSession.PlayerUserId2))
            {
                gamePlayVm.PlayerOnLeft = userProfileService.GetUserProfileViewModel(fetchPlayer2Query).Value;
                gamePlayVm.ThinkTimeRemain = gameState.ThinkTimeRemainForPlayer2;

                // only return subset of user profile vm
                var userOnRightVm = userProfileService.GetUserProfileViewModelMainProperties(fetchPlayer1Query).Value;
                gamePlayVm.PlayerOnRight = UserProfileViewModel.SubSetDataMapper(userOnRightVm);
            }

            return Result.Success(gamePlayVm);
        }

        public Result<GameSessionInfo> StopGameRequest(GameSessionQuery query)
        {
            if (query == null)
                return Result.Success(new GameSessionInfo());

            // clear memory game session
            var gameSessionInfoTokill = GetService<IGameSessionService>().EndGameSession(query.GameSessionId).Value;

            // clear game session storage

            var gameSession = DataContext.GameSessions.Find(query.GameSessionId);
            if (gameSession == null)
                return Result.Success(new GameSessionInfo());

            // clear player connection information
            var playerConnectionInfos =
                DataContext.PlayerConnectionInfos.Where(
                    conn => conn.UserId == gameSession.PlayerUserId1 || conn.UserId == gameSession.PlayerUserId2);

            DataContext.GameSessions.Remove(gameSession);
            DataContext.PlayerConnectionInfos.RemoveRange(playerConnectionInfos);

            SaveChanges("GameSession_PlayerConnectionInfo");

            return Result.Success(gameSessionInfoTokill);
        }

        public Result<List<string>> DisconnectGame(GameSessionQuery query)
        {
            if (query == null)
            {
                Logger.Debug("Disconnect game request from unknown source");
                return Result.Success(new List<string>());
            }

            Logger.Debug($"Disconnect game request from {query.RequestUserId}");

            // clear memory game session
            var gameSessionManager = GetService<IGameSessionService>();
            var gameSessionsInfoTokill = gameSessionManager.GameAllSessionForUser(query.RequestUserId);
            if (gameSessionsInfoTokill == null || gameSessionsInfoTokill.Count == 0)
            {
                var connectionInfos =
                    DataContext.PlayerConnectionInfos.Where(conn => conn.UserId == query.RequestUserId);

                DataContext.PlayerConnectionInfos.RemoveRange(connectionInfos);

                SaveChanges("PlayerConnectionInfo");

                return Result.Success(new List<string>());
            }

            // clear session information first
            var sessionIds = gameSessionsInfoTokill.Select(s => s.Id).ToList();
            gameSessionManager.EndGameSessions(sessionIds);

            // clear game session storage
            var gameSessions =
                DataContext.GameSessions.Where(
                    s => s.PlayerUserId1 == query.RequestUserId || s.PlayerUserId2 == query.RequestUserId);

            // clear player connection information
            var playerConnectionInfos =
                DataContext.PlayerConnectionInfos.Where(conn => conn.UserId == query.RequestUserId).ToList();

            var affectedUserIds = playerConnectionInfos.Select(c => c.UserId).ToList();

            DataContext.GameSessions.RemoveRange(gameSessions);
            DataContext.PlayerConnectionInfos.RemoveRange(playerConnectionInfos);
            SaveChanges("GameSession_PlayerConnectionInfo");

            return Result.Success(affectedUserIds);
        }
    }
}
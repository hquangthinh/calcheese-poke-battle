﻿using CalcheesePokeBattle.Common.DTO;
using CalcheesePokeBattle.Common.Monads;
using CalcheesePokeBattle.DTO;
using System;
using System.Linq;
using CalcheesePokeBattle.Model;
using CalcheesePokeBattle.Common.Utils;

namespace CalcheesePokeBattle.Service
{
    public class UsedCouponCodeReportCommand : KeywordSearchCommand
    {
        public DateTime? DateFrom { get; set; }

        public DateTime? DateTo { get; set; }

        public string CouponCode { get; set; }
    }

    public interface IUsedCouponCodeReportService
    {
        PagingResult<UsedCouponCodeReportDTO> SearchUsedCouponCodeReport(UsedCouponCodeReportCommand command);

        PagingResult<UsedCouponCodeSummaryByDateReportDTO> GetDateForUsedCouponCodeSummaryByDateReport(KeywordSearchCommand command);
    }

    public class DefaultUsedCouponCodeReportService : DefaultServiceBase, IUsedCouponCodeReportService
    {
        public DefaultUsedCouponCodeReportService(IDbFactory dbFactory) : base(dbFactory)
        {
        }

        public PagingResult<UsedCouponCodeReportDTO> SearchUsedCouponCodeReport(UsedCouponCodeReportCommand command)
        {
            if (command == null)
                return PagingResult<UsedCouponCodeReportDTO>.Empty();

            var predicate =
                Functions.Lambda<UsedCouponCodeReport, bool>(
                    c => (!command.DateFrom.HasValue || command.DateFrom <= c.CreatedDate)
                        && (!command.DateTo.HasValue || command.DateTo >= c.CreatedDate) 
                        && (string.IsNullOrEmpty(command.CouponCode) || c.CouponCode == command.CouponCode)
                    );

            var totalCount = DataContext.UsedCouponCodeReports.Count(predicate);
            var query =
                DataContext.UsedCouponCodeReports.Where(predicate)
                    .OrderByDescending(x => x.CreatedDate)
                    .Select(UsedCouponCodeReportDTO.EntityToDtoMapper)
                    .Paginate(command)
                    .ToList();

            return new PagingResult<UsedCouponCodeReportDTO>(query)
            {
                PageSize = command.PageSize,
                PageNumber = command.PageNumber,
                PageTotal = PagingHelper.GetPageTotal(totalCount, command.PageSize),
                ResultCount = totalCount
            };
        }

        public PagingResult<UsedCouponCodeSummaryByDateReportDTO> GetDateForUsedCouponCodeSummaryByDateReport(KeywordSearchCommand command)
        {
            var orderDirection = "desc";

            if (command != null && command.OrderAsc)
            {
                orderDirection = "asc";
            }

            var reportQuery = $@"select DATEADD(dd, DATEDIFF(dd, 0, r.CreatedDate), 0) as CreatedDate , count(*) as TotalInputCard
                                from UsedCouponCodeReport r
                                group by DATEADD(dd, DATEDIFF(dd, 0, r.CreatedDate), 0)
                                order by CreatedDate {orderDirection}";

            var result = DataContext.Database.SqlQuery<UsedCouponCodeSummaryByDateReportDTO>(reportQuery).ToList();

            return PagingResult<UsedCouponCodeSummaryByDateReportDTO>.AllResult(result);
        }
    }
}

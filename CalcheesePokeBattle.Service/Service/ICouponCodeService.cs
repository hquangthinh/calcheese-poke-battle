﻿using System;
using System.Linq;
using CalcheesePokeBattle.Common.Cryptography;
using CalcheesePokeBattle.Common.Monads;
using CalcheesePokeBattle.Common.Utils;
using CalcheesePokeBattle.DTO;
using CalcheesePokeBattle.Model;

namespace CalcheesePokeBattle.Service
{
    public class RedeemCodeForPointCommand
    {
        
    }

    public interface ICouponCodeService
    {
        /// <summary>
        /// Exchange coupon code for points and poke card
        /// perform lookup on CouponCode table to find point and poke card
        /// </summary>
        /// <param name="couponCodeDto"></param>
        /// <param name="currentUser"></param>
        /// <returns></returns>
        Result<CouponCodeDTO> RedeemCodeForPoint(CouponCodeDTO couponCodeDto, UserProfileDTO currentUser);

        int GetRandomPoint();
    }

    public class DefaultCouponCodeService : DefaultServiceBase, ICouponCodeService
    {
        public DefaultCouponCodeService(IDbFactory dbFactory) : base(dbFactory)
        {
        }

        public Result<CouponCodeDTO> RedeemCodeForPoint(CouponCodeDTO couponCodeDto, UserProfileDTO currentUser)
        {
            var validateResult = ValidateInput(couponCodeDto, currentUser);

            if (validateResult.Item1.IsFailure || validateResult.Item2.IsFailure)
                return validateResult.Item1;

            var dataEncryptionService = GetService<IOneWayEncryptionService>();
            var encryptedCode = dataEncryptionService.GetHashString(couponCodeDto.Code).ToLower();
            var matchCode = DataContext.CouponCodes.FirstOrDefault(c => c.Code == encryptedCode);

            if (matchCode?.CardId == null)
                return Result.Fail<CouponCodeDTO>("Mã không hợp lệ");

            if (!matchCode.Available)
                return Result.Fail<CouponCodeDTO>("Mã không hợp lệ");

            // Get the card information
            var cardInfo = GetService<IPokeCardService>().GetPokeCardDetailCache(matchCode.CardId.Value);

            if (cardInfo == null)
                return Result.Fail<CouponCodeDTO>("Mã không hợp lệ");

            var pointsForCode = GetPointsForCodeFromCardInfo(cardInfo);

            DataContext.PerformInTransaction("Redeem code for point", ctx =>
            {
                // increase point for user
                var userProfileEntity = validateResult.Item2.Value;
                userProfileEntity.TotalPoint += pointsForCode;

                // add user card
                userProfileEntity.UserCardCollections.Add(new UserCardCollection
                {
                    UserId = userProfileEntity.Id,
                    CardId = matchCode.CardId.Value,
                    CardPoint = pointsForCode,
                    CreatedDate = DateTime.UtcNow,
                    CouponCode = couponCodeDto.Code,
                    SourceOfCard = $"Đổi mã {couponCodeDto.Code} được {pointsForCode} điểm"
                });

                // Update properties for coupon code
                matchCode.Available = false;
                matchCode.RedeemByUserName = userProfileEntity.UserName;
                matchCode.RedeemOn = DateTime.UtcNow;
                matchCode.ClientIpAddress = couponCodeDto.ClientIpAddress;
                matchCode.ClientInfo = couponCodeDto.ClientInfo;

                // Record user activity history - code user has used
                ctx.UsedCouponCodes.Add(new UsedCouponCode
                {
                    CouponCode = couponCodeDto.Code,
                    UserId = userProfileEntity.Id,
                    ClientInfo = couponCodeDto.ClientInfo,
                    ClientIpAddress = couponCodeDto.ClientIpAddress,
                    CreatedDate = DateTime.UtcNow,
                    CodePoint = pointsForCode
                });

                ctx.SaveChanges();
            });

            var codeResult = CouponCodeDTO.EntityToDtoMapper(matchCode);
            codeResult.Point = pointsForCode;
            var imgBaseUrl = GetSettingValue(SettingKeys.WebAppImagesBaseUrl);
            codeResult.CardImageUrl = $"{imgBaseUrl}/{cardInfo.ImageUrl}";

            return Result.Success(codeResult);
        }

        private static readonly Func<CacheCommand<DefaultCouponCodeService>, Func<Random>> GetRandomInstanceCache
            = Functions.Lambda((DefaultCouponCodeService ctx) => ctx.GetRandomInstanceInternal())
                .MemoizeCtx(CacheConfig.LifeTime(TimeSpan.FromMinutes(30)));

        private Random GetRandomInstanceInternal()
            => new Random();

        public int GetRandomPoint()
            => GetRandomInstanceCache(this.AsCacheContext())().Next(1, 6);

        private int GetPointsForCodeFromCardInfo(PokeCardDTO cardInfo)
        {
            return cardInfo.IsSpecialCard
                ? 7
                : GetRandomInstanceCache(this.AsCacheContext())().Next(1, 6);
        }

        private Tuple<Result<CouponCodeDTO>, Result<UserProfile>> ValidateInput(CouponCodeDTO couponCodeDto, UserProfileDTO currentUser)
        {
            if (couponCodeDto == null || currentUser == null)
                return Tuple.Create(Result.Fail<CouponCodeDTO>("Expect a non null parameters"), Result.Fail<UserProfile>("Expect a non null parameters"));

            if(string.IsNullOrEmpty(couponCodeDto.Code))
                return Tuple.Create(Result.Fail<CouponCodeDTO>("Invalid coupon code"), Result.Fail<UserProfile>("Invalid coupon code"));

            var userProfileService = GetService<IUserProfileService>();
            var userCheckResult = userProfileService.CheckUserExisting(currentUser.Id);

            if (userCheckResult.IsFailure)
                return Tuple.Create(Result.Fail<CouponCodeDTO>("Cannot redeem. Invalid user"), Result.Fail<UserProfile>("Cannot redeem. Invalid user"));

            return Tuple.Create(Result.Success(couponCodeDto), userCheckResult);
        }
    }
}

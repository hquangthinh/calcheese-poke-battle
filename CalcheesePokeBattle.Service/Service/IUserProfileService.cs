﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Data.Entity.Core;
using CalcheesePokeBattle.Authentication;
using CalcheesePokeBattle.Authentication.Models;
using CalcheesePokeBattle.Common.DTO;
using CalcheesePokeBattle.Common.Extensions;
using CalcheesePokeBattle.Common.Monads;
using CalcheesePokeBattle.Common.Timing;
using CalcheesePokeBattle.Common.Utils;
using CalcheesePokeBattle.DTO;
using CalcheesePokeBattle.Extensions;
using CalcheesePokeBattle.Helper;
using CalcheesePokeBattle.Model;
using CalcheesePokeBattle.ViewModel;

namespace CalcheesePokeBattle.Service
{
    public class UserCommandBase
    {
        public string UserId { get; set; }

        public string UserName { get; set; }

        public static readonly Func<string, UserCommandBase> FromUserId = s => new UserCommandBase { UserId = s };

        public static readonly Func<string, UserCommandBase> FromUserName = s => new UserCommandBase { UserName = s };

        public bool PopulateAllUserCardsInfo { get; set; }

        public bool PopulateRole { get; set; }
    }

    public class SearchUserCommand : KeywordSearchCommand
    {
        public string UserName { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string FullName { get; set; }

        public string PhoneNumber { get; set; }
    }

    public class UpdateUserCommand : UserProfileDTO
    {
        public string Password { get; set; }

        public string PasswordConfirmation { get; set; }

        public bool AddUserToRole { get; set; }
    }

    public class ChangePasswordCommand
    {
        public bool ChangePasswordForUser { get; set; }

        public string UserNameToChangePassword { get; set; }

        public string CurrentPassword { get; set; }

        public string Password { get; set; }

        public string PasswordConfirmation { get; set; }
    }

    /// <summary>
    /// Service for CRUD of user profiles, dealing with user profile stuffs i.e card collections, player inventory
    /// </summary>
    public interface IUserProfileService
    {
        Result<UserProfileDTO> CreateUserProfile(UserProfileDTO dto);

        PagingResult<UserProfileDTO> SearchUserProfiles(SearchUserCommand command);

        Result<UserProfile> CheckUserExisting(string userId);

        /// <summary>
        /// Get basic detail information for user profile and profile role
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        Result<UserProfileDTO> GetUserProfileDetail(UserCommandBase command);

        Result<UserProfileViewModel> GetUserProfileDetailIncludeChildren(UserCommandBase command);

        /// <summary>
        /// Get view model for user profile detail page
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        Result<UserProfileViewModel> GetUserProfileViewModel(UserCommandBase command);

        /// <summary>
        /// Only get profile main properties
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        Result<UserProfileViewModel> GetUserProfileViewModelMainProperties(UserCommandBase command);

        Result<UserInventoryViewModel> GetPlayerInventoryViewModel(UserCommandBase command);

        /// <summary>
        /// Query user game history for admin view
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        Result<List<UserGameHistoryViewModel>> GetUserGameHistoryViewModel(UserCommandBase command);

        Result<List<UserActivityHistoryDTO>> GetUserActivityHistories(UserCommandBase command);

        Result<List<UsedCouponCodeReportDTO>> GetUserUsedCouponCodes(UserCommandBase command);

        /// <summary>
        /// Update user profile properties except for TotalPoint property
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        Result<UserProfileDTO> Update(UpdateUserCommand command);

        Result<UserProfileDTO> UpdateProfileMainDetails(UpdateUserCommand command);

        /// <summary>
        /// Update user details and roles, used for admin to update user detail
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        Result<UserProfileDTO> UpdateUserDetailsWithRoles(UpdateUserCommand command);

        /// <summary>
        /// Update user profile point
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        Result<UserProfileDTO> UpdateUserPoint(UpdateUserCommand command);

        Result<ResponseDTO> Delete(UserCommandBase command);

        Result UpdateProfileAvatarUrl(BlobInformation blobInfo, UserProfileDTO currentProfileDto);
    }

    public class DefaultUserProfileService : DefaultServiceBase, IUserProfileService
    {
        public DefaultUserProfileService(IDbFactory dbFactory) : base(dbFactory)
        {
        }

        public PagingResult<UserProfileDTO> SearchUserProfiles(SearchUserCommand command)
        {
            if (command == null)
                return PagingResult<UserProfileDTO>.Empty();

            if (!string.IsNullOrEmpty(command.UserName))
                command.UserName = command.UserName.ToLower();

            if (!string.IsNullOrEmpty(command.FullName))
                command.FullName = command.FullName.ToLower();

            if (string.IsNullOrEmpty(command.OrderBy))
            {
                command.OrderBy = "UserName";
                command.OrderAsc = true;
            }

            return Timer.Time(() =>
            {
                var predicate =
                    Functions.Lambda<UserProfile, bool>(
                        profile =>
                            (string.IsNullOrEmpty(command.UserName) ||
                             (!string.IsNullOrEmpty(profile.UserName) &&
                              profile.UserName.ToLower().Contains(command.UserName)))

                            && !profile.UserName.Equals("admin")

                            &&
                            ((string.IsNullOrEmpty(command.FullName) ||
                              (!string.IsNullOrEmpty(profile.FirstName) &&
                               profile.FirstName.ToLower().Contains(command.FullName)))

                             ||
                             (string.IsNullOrEmpty(command.FullName) ||
                              (!string.IsNullOrEmpty(profile.LastName) &&
                               profile.LastName.ToLower().Contains(command.FullName))))
                            
                            && (string.IsNullOrEmpty(command.PhoneNumber) ||
                                (!string.IsNullOrEmpty(profile.PhoneNumber)
                                 && profile.PhoneNumber.Contains(command.PhoneNumber)))
                    );

                var totalCount = DataContext.UserProfiles.Count(predicate);
                var query = DataContext.UserProfiles
                    .Include(x => x.UserCardCollections.Select(c => c.PokeCard))
                    .Include(x => x.UserInventories)
                    .Include(x => x.UserLuckyDrawCodes)
                    .Where(predicate);

                if ("TotalPoint".Equals(command.OrderBy))
                {
                    var orderByTotalPointFn = Functions.Lambda<UserProfileDTO, int?>(profile => profile.TotalPoint);
                    if (command.OrderAsc)
                    {
                        var resultOrderByPoint = query.Select(UserProfileDTO.EntityToDtoMapper)
                            .OrderBy(orderByTotalPointFn)
                            .ToList()
                            .Paginate(command)
                            .ToList();

                        return PagingResult<UserProfileDTO>.FromResult(resultOrderByPoint, 0, totalCount, command.PageSize,
                            command.PageNumber);
                    }

                    var resultOrderByPointDesc = query.Select(UserProfileDTO.EntityToDtoMapper)
                            .OrderByDescending(orderByTotalPointFn)
                            .ToList()
                            .Paginate(command)
                            .ToList();

                    return PagingResult<UserProfileDTO>.FromResult(resultOrderByPointDesc, 0, totalCount, command.PageSize,
                        command.PageNumber);
                }

                var orderByFn = Functions.Lambda<UserProfile, string>(profile => profile.UserName);

                if ("FullName".Equals(command.OrderBy))
                {
                    orderByFn = Functions.Lambda<UserProfile, string>(profile => profile.FirstName + " " + profile.LastName);
                }

                if (command.OrderAsc)
                {
                    var result = query.OrderBy(orderByFn).Select(UserProfileDTO.EntityToDtoMapper)
                        .ToList()
                        .Paginate(command)
                        .ToList();

                    return PagingResult<UserProfileDTO>.FromResult(result, 0, totalCount, command.PageSize,
                        command.PageNumber);
                }

                var resultDesc = query.OrderByDescending(orderByFn).Select(UserProfileDTO.EntityToDtoMapper)
                    .ToList()
                    .Paginate(command)
                    .ToList();

                return PagingResult<UserProfileDTO>.FromResult(resultDesc, 0, totalCount, command.PageSize,
                    command.PageNumber);
            });
        }

        public Result<UserProfileDTO> CreateUserProfile(UserProfileDTO dto)
        {
            DataContext.UserProfiles.Add(UserProfileDTO.DtoToEntityMapper(dto));
            SaveChanges("UserProfiles", dto.Id);
            return Result.Success(dto);
        }

        public Result<UserProfile> CheckUserExisting(string userId)
        {
            var profileEntity = FindProfileEntity(userId, string.Empty);
            return profileEntity == null ? Result.Fail<UserProfile>($"Cannot find user {userId}") : Result.Success(profileEntity);
        }

        public Result<UserProfileDTO> GetUserProfileDetail(UserCommandBase command)
        {
            var profileEntity = FindProfileEntity(command.UserId, command.UserName);

            if (profileEntity == null)
                Result.Fail<UserProfileDTO>($"Cannot find user with Id -> {command.UserId}");

            var profileDto = UserProfileDTO.EntityToDtoMapper(profileEntity);
            profileDto.UserRoles =
                GetService<IAuthenticator>()
                    .GetRoleForUser(profileDto.Id)
                    .Select(r => new RoleDTO {Name = r})
                    .ToList();

            profileDto.UserPermission = new UserPermissionDTO
            {
                IsSiteAdmin = profileDto.UserRoles.Any(r => r.Name.Equals(RoleDTO.Administrator)),
                CanUserAccessLuckyDraw = profileDto.UserRoles.Any(r => r.Name.Equals(RoleDTO.Administrator) || r.Name.Equals(RoleDTO.LuckyDrawManager)),
                CanUserAccessTestFeatures = profileDto.UserRoles.Any(r => r.Name.Equals(RoleDTO.Administrator) || r.Name.Equals(RoleDTO.TestUser))
            };

            profileDto.TotalCards = profileDto.UserCardCollections.Count;

            profileDto.SetProfileFullAddress();

            return Result.Success(profileDto);
        }

        public Result<UserProfileViewModel> GetUserProfileDetailIncludeChildren(UserCommandBase command)
        {
            var profileEntity = DataContext.UserProfiles
                .Include(p => p.UserCardCollections.Select(c => c.PokeCard))
                .Include(p => p.UserInventories)
                .Include(p => p.UserLuckyDrawCodes)
                .FirstOrDefault(
                    p =>
                        p.Id == command.UserId ||
                        string.Compare(p.UserName, command.UserName, StringComparison.InvariantCultureIgnoreCase) == 0);

            var profileViewModel = UserProfileViewModel.EntityToViewModelMapper(profileEntity);

            if (command.PopulateRole)
            {
                profileViewModel.UserRoles =
                    GetService<IAuthenticator>()
                        .GetRoleForUser(profileViewModel.Id)
                        .Select(r => new RoleDTO {Name = r})
                        .ToList();
            }

            profileViewModel.SetProfileFullAddress();

            var allPokemonCards = GetService<IPokeCardService>().GetAllPokemonCardViewModels();

            var userCardList =
                profileViewModel.UserCardCollections.OrderByDescending(c => c.PokeCard.CardPoint)
                    .ThenByDescending(c => c.Id)
                    .ToList();

            // Populate this property for used in game playing page
            profileViewModel.TotalCards = userCardList.Count;
            if (command.PopulateAllUserCardsInfo)
            {
                // get all user's cards info, sort decesing by card power
                var imageBaseUrl = GetSettingValue(SettingKeys.WebAppImagesBaseUrl);
                var allUserCardViewModels =
                    userCardList.Select(uc => PokeCardViewModel.DtoToViewModelMapper(uc.PokeCard, imageBaseUrl))
                        .OrderByDescending(c => c.CardPoint)
                        .ToList();

                profileViewModel.AllUserCards = new List<PokeCardViewModel>();

                // group the card by id
                var userCardGroups = allUserCardViewModels.GroupBy(card => card.Id, card => card)
                    .ToDictionary(g => g.Key, g => g.ToList());

                foreach (var cardGroup in userCardGroups)
                {
                    var cardCountPergroup = cardGroup.Value.Count;
                    cardGroup.Value.ForEach(c => c.TotalCount = cardCountPergroup);
                    profileViewModel.AllUserCards.Add(cardGroup.Value.FirstOrDefault());
                }
            }
            else
            {
                // special card list
                profileViewModel.SpecialCardList = allPokemonCards.Where(c => c.IsSpecialCard)
                    .OrderBy(c => c.CardGroup).ThenBy(c => c.Id).ToList();

                profileViewModel.SpecialCardList.ForEach(specialCard =>
                {
                    specialCard.TotalCount = userCardList.Count(c => c.CardId == specialCard.Id);
                });

                // normal card list
                var allNormalCardList = allPokemonCards.Where(c => !c.IsSpecialCard)
                    .OrderBy(c => c.CardGroup).ThenBy(c => c.Id).ToList();

                allNormalCardList.ForEach(normalCard =>
                {
                    normalCard.TotalCount = userCardList.Count(c => c.CardId == normalCard.Id);
                });

                profileViewModel.AllNormalCardList = allNormalCardList;
                profileViewModel.NormalCardList = new List<List<PokeCardViewModel>>();
                const int normalCardDisplayPerLine = 7;
                var normalCardLines = allNormalCardList.Count / normalCardDisplayPerLine;
                for (var i = 0; i < normalCardLines; i++)
                {
                    profileViewModel.NormalCardList.Add(allNormalCardList.Skip(i * normalCardDisplayPerLine)
                        .Take(normalCardDisplayPerLine).ToList());
                }
            }

            return Result.Success(profileViewModel);
        }

        private UserProfile FindProfileEntity(string userId, string userName)
        {
            var profileEntity = !string.IsNullOrEmpty(userId)
                ? DataContext.UserProfiles.Find(userId)
                : DataContext.UserProfiles.FirstOrDefault(
                    p => string.Compare(p.UserName, userName, StringComparison.InvariantCultureIgnoreCase) == 0);
            return profileEntity;
        }

        /// <summary>
        /// Update user profile general properties, not the user points
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        public Result<UserProfileDTO> Update(UpdateUserCommand command)
        {
            if(command == null)
                return Result.Fail<UserProfileDTO>("Expect input data");

            if (command.IsNew)
                return CreateNewUserWithProfile(command);

            var profileEntity = FindProfileEntity(command.Id, command.UserName);

            if (profileEntity == null)
                return Result.Fail<UserProfileDTO>($"No profile ${command.Id} to update");

            if (!command.FirstName.IsNullOrEmpty())
                profileEntity.FirstName = command.FirstName;

            if (!command.LastName.IsNullOrEmpty())
                profileEntity.LastName = command.LastName;

            if (!command.Email.IsNullOrEmpty())
                profileEntity.Email = command.Email;

            if (!command.PhoneNumber.IsNullOrEmpty())
                profileEntity.PhoneNumber = command.PhoneNumber;

            if (!command.Address.IsNullOrEmpty())
                profileEntity.Address = command.Address;

            if (!command.DeliveryAddress.IsNullOrEmpty())
                profileEntity.DeliveryAddress = command.DeliveryAddress;

            if (!command.SocialIdNumber.IsNullOrEmpty())
                profileEntity.SocialIdNumber = command.SocialIdNumber;

            SaveChanges("UserProfile", command.Id);

            return Result.Success(UserProfileDTO.EntityToDtoMapper(profileEntity));
        }

        /// <summary>
        /// Only perform update main details for profile do not create new profile
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        public Result<UserProfileDTO> UpdateProfileMainDetails(UpdateUserCommand command)
        {
            if (command == null)
                return Result.Fail<UserProfileDTO>("Expect input data");

            var profileEntity = FindProfileEntity(command.Id, command.UserName);

            if (profileEntity == null)
                return Result.Fail<UserProfileDTO>($"No profile ${command.Id} to update");

            if (!command.FirstName.IsNullOrEmpty())
                profileEntity.FirstName = command.FirstName;

            if (!command.LastName.IsNullOrEmpty())
                profileEntity.LastName = command.LastName;

            if (!command.Email.IsNullOrEmpty())
                profileEntity.Email = command.Email;

            if (!command.PhoneNumber.IsNullOrEmpty())
                profileEntity.PhoneNumber = command.PhoneNumber;

            if (!command.Address.IsNullOrEmpty())
                profileEntity.Address = command.Address;

            if (!command.DeliveryAddress.IsNullOrEmpty())
                profileEntity.DeliveryAddress = command.DeliveryAddress;

            if (!command.SocialIdNumber.IsNullOrEmpty())
                profileEntity.SocialIdNumber = command.SocialIdNumber;

            SaveChanges("UserProfile", command.Id);

            return Result.Success(UserProfileDTO.EntityToDtoMapper(profileEntity));
        }

        public Result<UserProfileDTO> UpdateUserDetailsWithRoles(UpdateUserCommand command)
        {
            if (command == null)
                return Result.Fail<UserProfileDTO>("Expect input data");

            if (command.IsNew)
                return CreateNewUserWithProfile(command);

            var profileEntity = FindProfileEntity(command.Id, command.UserName);

            if (profileEntity == null)
                return Result.Fail<UserProfileDTO>($"No profile ${command.Id} to update");

            if (!command.FirstName.IsNullOrEmpty())
                profileEntity.FirstName = command.FirstName;

            if (!command.LastName.IsNullOrEmpty())
                profileEntity.LastName = command.LastName;

            if (!command.Email.IsNullOrEmpty())
                profileEntity.Email = command.Email;

            if (!command.PhoneNumber.IsNullOrEmpty())
                profileEntity.PhoneNumber = command.PhoneNumber;

            if (!command.Address.IsNullOrEmpty())
                profileEntity.Address = command.Address;

            if (!command.DeliveryAddress.IsNullOrEmpty())
                profileEntity.DeliveryAddress = command.DeliveryAddress;

            if (!command.SocialIdNumber.IsNullOrEmpty())
                profileEntity.SocialIdNumber = command.SocialIdNumber;

            SaveChanges("UserProfile", command.Id);

            GetService<IAuthenticator>()
                .AddUserToRoles(profileEntity.Id, command.UserRoles.Select(r => r.Name).ToArray());

            return Result.Success(UserProfileDTO.EntityToDtoMapper(profileEntity));
        }

        private Result<UserProfileDTO> CreateNewUserWithProfile(UpdateUserCommand command)
        {
            var createUserCheckResult = UserProfileHelper.ValidateUserCreation(command);
            if (createUserCheckResult.IsFailure)
                return Result.Fail<UserProfileDTO>(createUserCheckResult.Error);

            var authenticator = GetService<IAuthenticator>();
            var appUser = authenticator.CreateAppUser(command.UserName, command.Email, command.Password, isActive: true);

            // add user to role
            command.UserRoles?.ToList().ForEach(r => authenticator.AddUserToRole(appUser.Id, r.Name));

            var userProfileDto = new UserProfileDTO
            {
                Id = appUser.Id,
                UserName = appUser.UserName,
                PhoneNumber = command.PhoneNumber,
                Email = command.Email,
                TotalPoint = 0
            };

            return CreateUserProfile(userProfileDto);
        }

        /// <summary>
        /// Update user point only
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        public Result<UserProfileDTO> UpdateUserPoint(UpdateUserCommand command)
        {
            var profileEntity = FindProfileEntity(command.Id, command.UserName);
            if (profileEntity == null)
                return Result.Fail<UserProfileDTO>($"No profile ${command.Id} to update");
            if (command.TotalPoint > 0 && command.TotalPoint < Int32.MaxValue)
                profileEntity.TotalPoint = command.TotalPoint;

            SaveChanges("UserProfile", command.Id);

            return Result.Success(UserProfileDTO.EntityToDtoMapper(profileEntity));
        }


        public Result<UserProfileViewModel> GetUserProfileViewModel(UserCommandBase command)
            => GetUserProfileDetailIncludeChildren(command);

        public Result<UserProfileViewModel> GetUserProfileViewModelMainProperties(UserCommandBase command)
            => GetUserProfileViewModelMainPropertiesCache(this.AsCacheContext())(command);

        private static readonly
            Func<CacheCommand<DefaultUserProfileService>, Func<UserCommandBase, Result<UserProfileViewModel>>>
            GetUserProfileViewModelMainPropertiesCache
                =
                Functions.Lambda(
                        (DefaultUserProfileService ctx, UserCommandBase cmd) =>
                            ctx.GetUserProfileViewModelMainPropertiesInternal(cmd))
                    .MemoizeCtx(CacheConfig.LifeTime(TimeSpan.FromMinutes(30)));

        private Result<UserProfileViewModel> GetUserProfileViewModelMainPropertiesInternal(UserCommandBase command)
        {
            var profileEntity = FindProfileEntity(command.UserId, command.UserName);
            return Result.Success(UserProfileViewModel.MinimumEntityToViewModelMapper(profileEntity));
        }

        public Result<UserInventoryViewModel> GetPlayerInventoryViewModel(UserCommandBase command)
        {
            if (command == null)
                return Result.Success(new UserInventoryViewModel());

            var currentWeek = GetCurrentWeekForToday();

            var allGifts = DataContext.GiftItems.Select(GiftItemViewModel.EntityToViewModelMapper).ToList();

            // list of lucky draw codes for user of current week
            var resultModel = new UserInventoryViewModel
            {
                LuckyDrawCodes = DataContext.UserLuckyDrawCodes
                    .Where(item => item.WeekNumber == currentWeek.WeekNumber && item.IsActive && item.UserId == command.UserId)
                    .Select(UserLuckyDrawCodeDTO.EntityToDtoMapper)
                    .ToList(),
                GiftWonFromRedeemPoints = allGifts.Where(item => item.IsRedeem).ToList(),
                GiftsWonFromLuckyDraw = allGifts.Where(item => !item.IsRedeem).ToList()
            };

            var userGiftsFromRedeem = DataContext.UserInventories
                .Where(item => item.UserId == command.UserId)
                .Select(GiftItemViewModel.UserInventoryToMapper)
                .ToList();

            resultModel.GiftWonFromRedeemPoints.ForEach(g =>
            {
                g.TotalCount = userGiftsFromRedeem.Count(item => item.Name == g.Name);
            });

            resultModel.GiftWonFromRedeemPointsMap = resultModel.GiftWonFromRedeemPoints.ToDictionary(
                item => item.Name, item => item.TotalCount);

            var userGiftsFromWonLuckyDraw = DataContext.GiftWonFromLuckyDraws
                .Where(item => item.UserId == command.UserId)
                .OrderBy(item => item.WeekNumber).ToList();

            resultModel.GiftsWonFromLuckyDraw.ForEach(g =>
            {
                g.TotalCount = userGiftsFromWonLuckyDraw.Count(item => item.GiftId == g.Id);
            });

            resultModel.GiftsWonFromLuckyDrawMap = resultModel.GiftsWonFromLuckyDraw.ToDictionary(item => item.Name,
                item => item.BelongToUser);

            return Result.Success(resultModel);
        }

        public Result<List<UserGameHistoryViewModel>> GetUserGameHistoryViewModel(UserCommandBase command)
        {
            var sql = $"select * from [vwAllGameHistory] where UserId1='{command.UserId}' or UserId2='{command.UserId}' order by Id desc";

            var gameHistoryList = DataContext.Database.SqlQuery<UserGameHistoryViewModel>(sql).ToList();

            gameHistoryList.Where(item => item.WinnerUserId.Equals(command.UserId)).ToList().ForEach(game =>
            {
                game.OpponentUserName = game.UserName2;
                game.WinPoints = game.AdjustPoint.GetValueOrDefault(0);
                game.LostPoints = 0;
                game.TotalPoints = game.User1Score.GetValueOrDefault(0);
            });

            gameHistoryList.Where(item => !item.WinnerUserId.Equals(command.UserId)).ToList().ForEach(game =>
            {
                game.OpponentUserName = game.UserName1;
                game.WinPoints = 0;
                game.LostPoints = game.AdjustPoint.GetValueOrDefault(0);
                game.TotalPoints = game.User2Score.GetValueOrDefault(0);
            });

            return Result.Success(gameHistoryList);
        }

        public Result<List<UserActivityHistoryDTO>> GetUserActivityHistories(UserCommandBase command)
        {
            var sql = $"select * from UserActivityHistory where UserName = '{command.UserName}' order by id desc";

            var result = DataContext.Database.SqlQuery<UserActivityHistoryDTO>(sql).ToList();

            return Result.Success(result);
        }

        public Result<List<UsedCouponCodeReportDTO>> GetUserUsedCouponCodes(UserCommandBase command)
        {
            var sql = $@"select * from vwAllUsedCouponCode where UserId = '{command.UserId}' order by id desc";

            var result = DataContext.Database.SqlQuery<UsedCouponCodeReportDTO>(sql).ToList();

            return Result.Success(result);
        }

        public Result<ResponseDTO> Delete(UserCommandBase command)
        {
            throw new NotImplementedException();
        }

        public Result UpdateProfileAvatarUrl(BlobInformation blobInfo, UserProfileDTO currentProfileDto)
        {
            if (blobInfo == null || currentProfileDto == null)
                return Result.Fail("Invalid params");

            var profileEntity = FindProfileEntity(currentProfileDto.Id, currentProfileDto.UserName);
            profileEntity.AvatarUrl = blobInfo.BlobUri.ToString();
            SaveChanges("UserProfile");
            return Result.Success();
        }
    }
}
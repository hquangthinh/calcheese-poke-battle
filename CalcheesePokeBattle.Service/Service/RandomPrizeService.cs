﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CalcheesePokeBattle.Common.Monads;
using CalcheesePokeBattle.DTO;

namespace CalcheesePokeBattle.Service
{
    public class RandomPrizeService : IPrizeLuckyDrawHistoryService
    {
        public Result<PrizeLuckyDrawHistoryDTO> CreatePrizeLuckyDrawHistory(PrizeLuckyDrawHistoryDTO dto)
        {
            throw new NotImplementedException();
        }

        public Result<PrizeLuckyDrawHistoryDTO> Delete(PrizeLuckyDrawHistoryCommandBase command)
        {
            throw new NotImplementedException();
        }

        public void ProcessPrize(int weekNumber)
        {
            throw new NotImplementedException();
        }

        public PagingResult<PrizeLuckyDrawHistoryDTO> SearchPrizeLuckyDrawHistory(SearchPrizeLuckyDrawHistoryCommand command)
        {
            throw new NotImplementedException();
        }

        public Result<WinnerListUserDTO> SetPrize(SetPrizeCommand command)
        {
            throw new NotImplementedException();
        }

        public Result<PrizeLuckyDrawHistoryDTO> Update(UpdatePrizeLuckyDrawHistoryCommand command)
        {
            throw new NotImplementedException();
        }

        public Result ConfirmPrize(ConfirmPrizeCommand command)
        {
            throw new NotImplementedException();
        }

        public Result ResetPrize(ConfirmPrizeCommand command)
        {
            throw new NotImplementedException();
        }
    }
}

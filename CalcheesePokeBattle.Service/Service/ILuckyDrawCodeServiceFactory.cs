﻿using System;

namespace CalcheesePokeBattle.Service
{
    /// <summary>
    /// Service for create correct instance of lucky draw service for the respective week
    /// </summary>
    public interface ILuckyDrawCodeServiceFactory
    {
        ILuckyDrawCodeGenerator GetGeneratorServiceInstance(int weekNumber);
    }

    public class DefaultLuckyDrawCodeServiceFactory : ILuckyDrawCodeServiceFactory
    {
        private readonly IServiceFactory _serviceFactory;

        public DefaultLuckyDrawCodeServiceFactory(IServiceFactory serviceFactory)
        {
            _serviceFactory = serviceFactory;
        }

        public ILuckyDrawCodeGenerator GetGeneratorServiceInstance(int weekNumber)
        {
            if(weekNumber <=0 || weekNumber > 12)
                throw new NotSupportedException($"Do not support code generator for week {weekNumber}");

            var instanceName = $"ILuckyDrawCodeGeneratorForWeek{weekNumber}";

            return _serviceFactory.GetService<ILuckyDrawCodeGenerator>(instanceName);
        }
    }
}
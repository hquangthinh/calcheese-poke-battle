﻿using System;
using System.Collections.Generic;
using System.Linq;
using CalcheesePokeBattle.Common.Extensions;
using CalcheesePokeBattle.Common.Monads;
using CalcheesePokeBattle.DTO;
using CalcheesePokeBattle.Model;
using CalcheesePokeBattle.ViewModel;

namespace CalcheesePokeBattle.Service
{
    public class RedeemPointToGiftCommand : BaseTrackableDTO
    {
        /// <summary>
        /// Id of the gift item for redeem point
        /// </summary>
        public int GiftId { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string FullName { get; set; }

        public string PhoneNumber { get; set; }

        public string PhoneNumberConfirmed { get; set; }

        public string Address { get; set; }

        public string AddressNumber { get; set; }

        public string StreetName { get; set; }

        public string WardName { get; set; }

        public string DistrictName { get; set; }

        public string CityName { get; set; }

        public string DeliveryAddress { get; set; }

        public string SocialIdNumber { get; set; }
    }

    /// <summary>
    /// Define contract for dealing with redeem points to gifts or lucky draw code
    /// </summary>
    public interface IRedeemService
    {
        Result<RedeemPageViewModel> GetViewModelForRedeemPage();

        /// <summary>
        /// Perform redeem points of current logged in user to a gift item
        /// </summary>
        /// <param name="command"></param>
        /// <param name="currentProfileDto"></param>
        /// <returns></returns>
        Result<ResponseDTO<string>> RedeemPointsForGift(RedeemPointToGiftCommand command, UserProfileDTO currentProfileDto);

        /// <summary>
        /// Perform redeem points of current logged in user to a lucky draw code
        /// </summary>
        /// <param name="command"></param>
        /// <param name="currentProfileDto"></param>
        /// <returns></returns>
        Result<ResponseDTO<string>> RedeemPointsForLuckyDrawCode(RedeemPointToGiftCommand command, UserProfileDTO currentProfileDto);
    }

    public class DefaultRedeemService : DefaultServiceBase, IRedeemService
    {
        public DefaultRedeemService(IDbFactory dbFactory) : base(dbFactory)
        {
        }

        public Result<RedeemPageViewModel> GetViewModelForRedeemPage()
        {
            // check and set gift amount threshold for redeem
            WeekDefinitionDTO currentWeek;
            try
            {
                currentWeek = GetCurrentWeekForToday();
            }
            catch (Exception)
            {
                return ReadOnlyViewModelForRedeemPage();
            }

            var totalBackPackHasBeenRedeemed =
                DataContext.GiftRedeemHistories.Count(x => x.WeekNumber == currentWeek.WeekNumber && x.GiftId == 1);

            var totalTShirtHasBeenRedeemed =
                DataContext.GiftRedeemHistories.Count(x => x.WeekNumber == currentWeek.WeekNumber && x.GiftId == 2);

            var totalHatHasBeenRedeemed =
                DataContext.GiftRedeemHistories.Count(x => x.WeekNumber == currentWeek.WeekNumber && x.GiftId == 3);

            var resultVm = new RedeemPageViewModel
            {
                LuckyDrawGifts = new List<GiftItemDTO>
                {
                    new GiftItemDTO
                    {
                        Description = "Học bổng trại hè quốc tế",
                        ImageUrl = "Content/images/lucky-draw-prize-1.png"
                    },
                    new GiftItemDTO
                    {
                        Description = "Xe điện cân bằng",
                        ImageUrl = "Content/images/lucky-draw-prize-2.png"
                    },
                    new GiftItemDTO
                    {
                        Description = "Xe đạp thể thao",
                        ImageUrl = "Content/images/lucky-draw-prize-3.png"
                    },
                    new GiftItemDTO
                    {
                        Description = "Bộ đồ chơi Pokemon",
                        ImageUrl = "Content/images/lucky-draw-prize-4-140x186.png"
                    }
                },
                RedeemGifts = new List<GiftItemDTO>
                {
                    new GiftItemDTO
                    {
                        Id = 1,
                        Name = "Balo",
                        Description = "Ba lô Pokemon",
                        DescriptionHtml = "BALÔ <br> POKÉMON",
                        ImageUrl = "Content/images/lucky-draw-gift-backpack.png",
                        RedeemPoint = 100,
                        GiftHasEnoughAvailableAmountForRedeem = totalBackPackHasBeenRedeemed < 25
                    },
                    new GiftItemDTO
                    {
                        Id = 3,
                        Name = "Mu",
                        Description = "Nón huấn luyện viên",
                        DescriptionHtml = "NÓN <br> HUẤN LUYỆN VIÊN",
                        ImageUrl = "Content/images/lucky-draw-gift-hat.png",
                        RedeemPoint = 60,
                        GiftHasEnoughAvailableAmountForRedeem = totalHatHasBeenRedeemed < 50
                    },
                    new GiftItemDTO
                    {
                        Id = 2,
                        Name = "AoThun",
                        Description = "Áo thun Pokemon",
                        DescriptionHtml = "ÁO THUN <br> POKÉMON",
                        ImageUrl = "Content/images/lucky-draw-gift-shirt.png",
                        RedeemPoint = 80,
                        GiftHasEnoughAvailableAmountForRedeem = totalTShirtHasBeenRedeemed < 50
                    },
                }
            };

            return Result.Success(resultVm);
        }

        private Result<RedeemPageViewModel> ReadOnlyViewModelForRedeemPage()
        {
            var resultVm = new RedeemPageViewModel
            {
                LuckyDrawGifts = new List<GiftItemDTO>
                {
                    new GiftItemDTO
                    {
                        Description = "Học bổng trại hè quốc tế",
                        ImageUrl = "Content/images/lucky-draw-prize-1.png"
                    },
                    new GiftItemDTO
                    {
                        Description = "Xe điện cân bằng",
                        ImageUrl = "Content/images/lucky-draw-prize-2.png"
                    },
                    new GiftItemDTO
                    {
                        Description = "Xe đạp thể thao",
                        ImageUrl = "Content/images/lucky-draw-prize-3.png"
                    },
                    new GiftItemDTO
                    {
                        Description = "Bộ đồ chơi Pokemon",
                        ImageUrl = "Content/images/lucky-draw-prize-4-140x186.png"
                    }
                },
                RedeemGifts = new List<GiftItemDTO>
                {
                    new GiftItemDTO
                    {
                        Id = 1,
                        Name = "Balo",
                        Description = "Ba lô Pokemon",
                        DescriptionHtml = "BALÔ <br> POKÉMON",
                        ImageUrl = "Content/images/lucky-draw-gift-backpack.png",
                        RedeemPoint = 100,
                        GiftHasEnoughAvailableAmountForRedeem = false
                    },
                    new GiftItemDTO
                    {
                        Id = 3,
                        Name = "Mu",
                        Description = "Nón huấn luyện viên",
                        DescriptionHtml = "NÓN <br> HUẤN LUYỆN VIÊN",
                        ImageUrl = "Content/images/lucky-draw-gift-hat.png",
                        RedeemPoint = 60,
                        GiftHasEnoughAvailableAmountForRedeem = false
                    },
                    new GiftItemDTO
                    {
                        Id = 2,
                        Name = "AoThun",
                        Description = "Áo thun Pokemon",
                        DescriptionHtml = "ÁO THUN <br> POKÉMON",
                        ImageUrl = "Content/images/lucky-draw-gift-shirt.png",
                        RedeemPoint = 80,
                        GiftHasEnoughAvailableAmountForRedeem = false
                    },
                }
            };

            return Result.Success(resultVm);
        }

        public Result<ResponseDTO<string>> RedeemPointsForGift(RedeemPointToGiftCommand command, UserProfileDTO currentProfileDto)
        {
            if (command == null || currentProfileDto == null)
                return Result.Fail(ResponseDTO<string>.ErrorResult(ErrorCode.Redeem.InvalidOperation, "Invalid operation"),
                    "Invalid operation");

            var currentWeek = GetCurrentWeekForToday();

            var actionDetails = $"Perform redeem operation for {currentProfileDto.UserName} with gift {command.GiftId}";

            var opResult = Result.Success(ResponseDTO<string>.NoDataSuccessResult());

            DataContext.PerformInTransaction(
                actionDetails,
                ctx =>
                {
                    // check if user is valid
                    var userCheckResult = GetService<IUserProfileService>().CheckUserExisting(currentProfileDto.Id);
                    if (userCheckResult.IsFailure)
                        throw new ApplicationException("Người chơi không hợp lệ");

                    // check if gift item is valid & has enough amount for redeem
                    var giftItemForRedeem = DataContext.GiftItems.FirstOrDefault(item => item.Id == command.GiftId);
                    if (giftItemForRedeem == null)
                        throw new ApplicationException("Quà không hợp lệ");

                    var totalGiftHasRedeemForWeek =
                        DataContext.GiftRedeemHistories
                            .Count(item => item.GiftId == command.GiftId && item.WeekNumber == currentWeek.WeekNumber);

                    var totalGiftAvailableForTheWeek =
                        GetService<ISettingService>()
                            .GetTotalGiftAvailableForWeek(currentWeek.WeekNumber, giftItemForRedeem.Name)
                            .Value;

                    if (totalGiftHasRedeemForWeek >= totalGiftAvailableForTheWeek.TotalPrize)
                        throw new ApplicationException("Số lượng quà của tuần này đã hết, bạn vui lòng đổi vào tuần sau nhé!");

                    // check if user has enough points to redeem for this gift
                    var userProfile = userCheckResult.Value;
                    var userAvailablePoints = userProfile.TotalPoint;
                    if (userAvailablePoints < giftItemForRedeem.RedeemPoint)
                        throw new ApplicationException("Tiếc quá... số điểm của bạn chưa đủ để đổi quà này. Bạn hãy tiếp tục sưu tập thêm thẻ Pokémon nhé!");

                    // Perform redeem points -> gift
                    var userPointToDecrease = giftItemForRedeem.RedeemPoint;
                    userProfile.TotalPoint -= userPointToDecrease;

                    // update user contact information if necessary
                    UpdateUserContactInfoInternal(command, userProfile);

                    userProfile.UserInventories.Add(new UserInventory
                    {
                        Name = giftItemForRedeem.Name,
                        ImageUrl = giftItemForRedeem.ImageUrl,
                        UserId = userProfile.Id
                    });

                    // Increase gift has redeem to 1
                    ctx.GiftRedeemHistories.Add(new GiftRedeemHistory
                    {
                        WeekNumber = currentWeek.WeekNumber,
                        GiftId = giftItemForRedeem.Id,
                        UserId = userProfile.Id,
                        RedeemOn = DateTime.UtcNow
                    });

                    // Record activity history
                    ctx.UserActivityHistories.Add(new UserActivityHistory
                    {
                        Action = "RedeemPointsForGift",
                        ActionDetail = actionDetails,
                        ClientInfo = command.ClientInfo,
                        ClientIpAddress = command.ClientIpAddress,
                        CreatedAt = DateTime.UtcNow,
                        UserName = userProfile.UserName
                    });

                    ctx.SaveChanges();

                    opResult = Result.Success(ResponseDTO<string>.SuccessResult(giftItemForRedeem.ImageUrl));

                });

            return opResult;
        }

        private const int PointsToRedeemLuckyDrawCode = 10;

        public Result<ResponseDTO<string>> RedeemPointsForLuckyDrawCode(RedeemPointToGiftCommand command, UserProfileDTO currentProfileDto)
        {
            // Validate input
            if (command == null || currentProfileDto == null)
                return
                    Result.Fail(
                        ResponseDTO<string>.ErrorResult(ErrorCode.Redeem.InvalidOperation, "Invalid operation"),
                        "Invalid operation");

            var currentWeek = GetCurrentWeekForToday();

            var opResult = Result.Success(ResponseDTO<string>.NoDataSuccessResult());

            var actionDetails = $"Perform redeem operation for {currentProfileDto.UserName} to lucky draw code";
            var newLuckyCode = GetService<ILuckyDrawCodeService>().GenerateLuckyDrawCodeForWeek(
                new GenerateLuckyDrawCodeForWeekCommand
                {
                    WeekNumber = currentWeek.WeekNumber,
                    ClientInfo = command.ClientInfo,
                    ClientIpAddress = command.ClientIpAddress
                }, currentProfileDto).Value;

            if (string.IsNullOrEmpty(newLuckyCode))
                return
                    Result.Fail(
                        ResponseDTO<string>.ErrorResult(ErrorCode.Redeem.InvalidLuckyCodeGenerated,
                            "Invalid lucky code generated"), "Invalid lucky code generated");

            DataContext.PerformInTransaction(actionDetails, ctx =>
            {
                // check if user is valid
                var userCheckResult = GetService<IUserProfileService>().CheckUserExisting(currentProfileDto.Id);
                if (userCheckResult.IsFailure)
                    throw new ApplicationException("Người chơi không hợp lệ");

                // check if user has enough points to redeem for this gift
                var userProfile = userCheckResult.Value;
                var userAvailablePoints = userProfile.TotalPoint;
                if (userAvailablePoints < PointsToRedeemLuckyDrawCode)
                    throw new ApplicationException("Tiếc quá... số điểm của bạn chưa đủ. Bạn hãy tiếp tục sưu tập thêm thẻ Pokémon nhé!");

                // Perform redeem points -> lucky draw code
                userProfile.TotalPoint -= PointsToRedeemLuckyDrawCode;

                // update user contact information if necessary
                UpdateUserContactInfoInternal(command, userProfile);

                userProfile.UserLuckyDrawCodes.Add(new UserLuckyDrawCode
                {
                    UserId = userProfile.Id,
                    CreatedDate = DateTime.UtcNow,
                    IsActive = true,
                    LuckyDrawCode = newLuckyCode,
                    WeekNumber = currentWeek.WeekNumber
                });

                // Record activity history
                ctx.UserActivityHistories.Add(new UserActivityHistory
                {
                    Action = "RedeemPointsForLuckyDrawCode",
                    ActionDetail = actionDetails + $" {newLuckyCode}",
                    ClientInfo = command.ClientInfo,
                    ClientIpAddress = command.ClientIpAddress,
                    CreatedAt = DateTime.UtcNow,
                    UserName = userProfile.UserName
                });

                ctx.SaveChanges();

                opResult = Result.Success(ResponseDTO<string>.SuccessResult(newLuckyCode));
            });

            return opResult;
        }

        private static void UpdateUserContactInfoInternal(RedeemPointToGiftCommand command, UserProfile userProfile)
        {
            if (!string.IsNullOrEmpty(command.FirstName))
            {
                userProfile.FirstName = command.FirstName;
            }

            if (!string.IsNullOrEmpty(command.LastName))
            {
                userProfile.LastName = command.LastName;
            }

            if (!string.IsNullOrEmpty(command.Address))
            {
                userProfile.Address = command.Address;
            }

            if (!string.IsNullOrEmpty(command.AddressNumber))
            {
                userProfile.AddressNumber = command.AddressNumber;
            }

            if (!string.IsNullOrEmpty(command.StreetName))
            {
                userProfile.StreetName = command.StreetName;
            }

            if (!string.IsNullOrEmpty(command.WardName))
            {
                userProfile.WardName = command.WardName;
            }

            if (!string.IsNullOrEmpty(command.DistrictName))
            {
                userProfile.DistrictName = command.DistrictName;
            }

            if (!string.IsNullOrEmpty(command.CityName))
            {
                userProfile.CityName = command.CityName;
            }

            if (!string.IsNullOrEmpty(command.DeliveryAddress))
            {
                userProfile.DeliveryAddress = command.DeliveryAddress;
            }

            if (!string.IsNullOrEmpty(command.PhoneNumber))
            {
                userProfile.PhoneNumber = command.PhoneNumber;
            }

            if (!string.IsNullOrEmpty(command.SocialIdNumber))
            {
                userProfile.SocialIdNumber = command.SocialIdNumber;
            }
        }
    }
}

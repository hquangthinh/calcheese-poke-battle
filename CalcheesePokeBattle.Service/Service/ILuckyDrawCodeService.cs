﻿using CalcheesePokeBattle.Common.Monads;
using CalcheesePokeBattle.DTO;
using CalcheesePokeBattle.Model;

namespace CalcheesePokeBattle.Service
{
    public class GenerateLuckyDrawCodeForWeekCommand : BaseTrackableDTO
    {
        public int WeekNumber { get; set; }
    }

    /// <summary>
    /// Service for generating lucky draw code
    /// </summary>
    public interface ILuckyDrawCodeService
    {
        Result<string> GenerateLuckyDrawCodeForWeek(GenerateLuckyDrawCodeForWeekCommand command, UserProfileDTO currentProfileDto);

        Result<string> GenerateLuckyDrawCodeForCurrentWeek(ITrackableDTO commanDto, UserProfileDTO currentProfileDto);
    }

    public interface ILuckyDrawCodeGenerator
    {
        Result<ILuckyDrawPoolForWeek> GenerateLuckyDrawCodeForWeek(GenerateLuckyDrawCodeForWeekCommand command, UserProfileDTO currentProfileDto);

        Result<ILuckyDrawPoolForWeek> GenerateLuckyDrawCodeForCurrentWeek(ITrackableDTO commanDto, UserProfileDTO currentProfileDto);
    }

    public interface ILuckyDrawCodeGeneratorForWeek1 : ILuckyDrawCodeGenerator
    {
    }

    public interface ILuckyDrawCodeGeneratorForWeek2 : ILuckyDrawCodeGenerator
    {
    }

    public interface ILuckyDrawCodeGeneratorForWeek3 : ILuckyDrawCodeGenerator
    {
    }

    public interface ILuckyDrawCodeGeneratorForWeek4 : ILuckyDrawCodeGenerator
    {
    }

    public interface ILuckyDrawCodeGeneratorForWeek5 : ILuckyDrawCodeGenerator
    {
    }

    public interface ILuckyDrawCodeGeneratorForWeek6 : ILuckyDrawCodeGenerator
    {
    }

    public interface ILuckyDrawCodeGeneratorForWeek7 : ILuckyDrawCodeGenerator
    {
    }

    public interface ILuckyDrawCodeGeneratorForWeek8 : ILuckyDrawCodeGenerator
    {
    }

    public interface ILuckyDrawCodeGeneratorForWeek9 : ILuckyDrawCodeGenerator
    {
    }

    public interface ILuckyDrawCodeGeneratorForWeek10 : ILuckyDrawCodeGenerator
    {
    }

    public interface ILuckyDrawCodeGeneratorForWeek11 : ILuckyDrawCodeGenerator
    {
    }

    public interface ILuckyDrawCodeGeneratorForWeek12 : ILuckyDrawCodeGenerator
    {
    }

    public interface ILuckyDrawCodeGeneratorForWeek13 : ILuckyDrawCodeGenerator
    {
    }
}
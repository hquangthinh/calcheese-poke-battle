﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using CalcheesePokeBattle.Common.Monads;
using CalcheesePokeBattle.Model;

namespace CalcheesePokeBattle.Service
{
    public class GameSessionInfo
    {
        public int Id { get; set; }

        public string PlayerUserId1 { get; set; }

        public string PlayerUserName1 { get; set; }

        public string PlayerUserId2 { get; set; }

        public string PlayerUserName2 { get; set; }

        public int PlayerCard1 { get; set; }

        public int ThinkTimeRemainForPlayer1 { get; set; }

        public int PlayerCard2 { get; set; }

        public int ThinkTimeRemainForPlayer2 { get; set; }

        public bool ValidToCalculateWinResult => PlayerCard1 > 0 && PlayerCard2 > 0;

        public static readonly Func<GameSession, GameSessionInfo> ForNewMapper = entity =>
            entity == null
                ? null
                : new GameSessionInfo
                {
                    Id = entity.Id,
                    PlayerUserId1 = entity.PlayerUserId1,
                    PlayerUserName1 = entity.PlayerUserName1,
                    PlayerUserId2 = entity.PlayerUserId2,
                    PlayerUserName2 = entity.PlayerUserName2
                };
    }

    public class UpdateGameSessionStateCommand
    {
        public int GameSessionId { get; set; }

        public string PlayerUserId { get; set; }

        public int PlayerCardId { get; set; }

        public int PlayerTimeSpentForCardSelection { get; set; }
    }

    public interface IGameSessionService
    {
        Result StartNewGameSession(GameSessionInfo sessionInfo);

        List<GameSessionInfo> GetAllGameSessionInfos();

        Result<GameSessionInfo> EndGameSession(int sessionId);

        Result EndGameSessions(IEnumerable<int> sessionIds);

        bool IsUserInGame(string playerUserId);

        Result<GameSessionInfo> GameSessionForUser(string playerUserId);

        List<GameSessionInfo> GameAllSessionForUser(string playerUserId);

        Result<GameSessionInfo> GameSessionForUserBySessionId(int sessionId);

        Result<GameSessionInfo> UpdateGameState(UpdateGameSessionStateCommand command);

        GameSessionInfo GetGameRound(int sessionId);

        Result ResetGameRound(int sessionId);
    }

    /// <summary>
    /// Use memory for game session management
    /// </summary>
    public class InMemoryGameSessionService : IGameSessionService
    {
        private static readonly ConcurrentDictionary<int, GameSessionInfo> GameSessions =
            new ConcurrentDictionary<int, GameSessionInfo>();

        public List<GameSessionInfo> GetAllGameSessionInfos()
        {
            return GameSessions.Values.ToList();
        }

        /// <summary>
        /// Call when a game event start for 2 players
        /// </summary>
        /// <param name="sessionInfo"></param>
        /// <returns></returns>
        public Result StartNewGameSession(GameSessionInfo sessionInfo)
        {
            GameSessions.AddOrUpdate(sessionInfo.Id, sessionInfo, (k, gameSessionInfo) => sessionInfo);
            return Result.Success();
        }

        /// <summary>
        /// Call when a game event end for 2 players
        /// </summary>
        /// <param name="sessionId"></param>
        /// <returns></returns>
        public Result<GameSessionInfo> EndGameSession(int sessionId)
        {
            GameSessionInfo info;
            GameSessions.TryRemove(sessionId, out info);
            return Result.Success(info);
        }

        public Result EndGameSessions(IEnumerable<int> sessionIds)
        {
            foreach (var id in sessionIds)
            {
                GameSessionInfo info;
                GameSessions.TryRemove(id, out info);
            }
            return Result.Success();
        }

        public bool IsUserInGame(string playerUserId)
        {
            var sessionInfo =
                GameSessions.Values.FirstOrDefault(
                    s => s.PlayerUserId1 == playerUserId || s.PlayerUserId2 == playerUserId);
            return sessionInfo != null;
        }

        /// <summary>
        /// Find a current game session belong to player with user id
        /// </summary>
        /// <param name="playerUserId"></param>
        /// <returns></returns>
        public Result<GameSessionInfo> GameSessionForUser(string playerUserId)
        {
            var sessionInfo =
                GameSessions.Values.FirstOrDefault(
                    s => s.PlayerUserId1 == playerUserId || s.PlayerUserId2 == playerUserId);
            return sessionInfo != null
                ? Result.Success(sessionInfo)
                : Result.Fail<GameSessionInfo>($"No game session for user {playerUserId}");
        }

        public List<GameSessionInfo> GameAllSessionForUser(string playerUserId)
        {
            return
                GameSessions.Values.Where(s => s.PlayerUserId1 == playerUserId || s.PlayerUserId2 == playerUserId)
                    .ToList();
        }

        public Result<GameSessionInfo> GameSessionForUserBySessionId(int sessionId)
        {
            var sessionInfo =
                GameSessions.Values.FirstOrDefault(
                    s => s.Id == sessionId);
            return sessionInfo != null
                ? Result.Success(sessionInfo)
                : Result.Fail<GameSessionInfo>("No game session found");
        }

        /// <summary>
        /// Call when player submits card to current game session
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        public Result<GameSessionInfo> UpdateGameState(UpdateGameSessionStateCommand command)
        {
            GameSessionInfo info;
            if (GameSessions.TryGetValue(command.GameSessionId, out info))
            {
                if (info.PlayerUserId1 == command.PlayerUserId)
                {
                    info.PlayerCard1 = command.PlayerCardId;
                    info.ThinkTimeRemainForPlayer1 = GlobalConstants.GameTimeOutPerRound - command.PlayerTimeSpentForCardSelection;
                }
                else if (info.PlayerUserId2 == command.PlayerUserId)
                {
                    info.PlayerCard2 = command.PlayerCardId;
                    info.ThinkTimeRemainForPlayer2 = GlobalConstants.GameTimeOutPerRound - command.PlayerTimeSpentForCardSelection;
                }

                return Result.Success(info);
            }
            return
                Result.Fail<GameSessionInfo>(
                    $"Unable to update game state with session {command.GameSessionId} - {command.PlayerCardId} -> card {command.PlayerCardId}");
        }

        public GameSessionInfo GetGameRound(int sessionId)
        {
            GameSessionInfo info;
            if (GameSessions.TryGetValue(sessionId, out info))
            {
                return info;
            }
            return null;
        }

        /// <summary>
        /// 2 players still in game session, game is reset for new round
        /// </summary>
        /// <param name="sessionId"></param>
        /// <returns></returns>
        public Result ResetGameRound(int sessionId)
        {
            GameSessionInfo info;
            if (GameSessions.TryGetValue(sessionId, out info))
            {
                info.PlayerCard1 = 0;
                info.ThinkTimeRemainForPlayer1 = 0;
                info.PlayerCard2 = 0;
                info.ThinkTimeRemainForPlayer2 = 0;
                return Result.Success(info);
            }
            return Result.Fail($"Unable to reset game state with session {sessionId}");
        }
    }
}

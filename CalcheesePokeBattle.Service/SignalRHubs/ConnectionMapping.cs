﻿using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;

namespace CalcheesePokeBattle.SignalRHubs
{
    public class ConnectionMapping<T>
    {
        private readonly ConcurrentDictionary<T, ConcurrentBag<string>> _connections =
            new ConcurrentDictionary<T, ConcurrentBag<string>>();

        public int Count => _connections.Count;

        public ICollection<T> AllConnectionKeys => _connections.Keys;

        public void Add(T key, string connectionId)
        {
            ConcurrentBag<string> connections;
            if (!_connections.TryGetValue(key, out connections))
            {
                connections = new ConcurrentBag<string>();
                _connections.TryAdd(key, connections);
            }
            connections.Add(connectionId);
        }

        public IEnumerable<string> GetConnections(T key)
        {
            ConcurrentBag<string> connections;
            if (_connections.TryGetValue(key, out connections))
            {
                return connections;
            }

            return Enumerable.Empty<string>();
        }

        public void Remove(T key, string connectionId)
        {
            lock (_connections)
            {
                ConcurrentBag<string> connections;
                if (!_connections.TryGetValue(key, out connections))
                {
                    return;
                }

                connections.TryTake(out connectionId);

                if (connections.Count == 0)
                {
                    _connections.TryRemove(key, out connections);
                }
            }
        }
    }
}

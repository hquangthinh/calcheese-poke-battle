﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalcheesePokeBattle.Common.Monads
{
    public struct Maybe<T> : IEquatable<Maybe<T>>
    {
        private readonly T _value;

        public static readonly Maybe<T> Nothing = new Maybe<T>();

        public T Value
        {
            get
            {
                if (!HasValue) throw new InvalidOperationException("Cannot access Value of Nothing");
                return _value;
            }
        }

        public bool HasValue { get; }

        private Maybe(T value)
        {
            _value = value;
            HasValue = value != null;
        }

        public static Maybe<T> Of(T value)
        {
            return new Maybe<T>(value);
        }

        public Maybe<U> Map<U>(Func<T, U> map)
        {
            return HasValue ? map(Value) : Maybe<U>.Nothing;
        }

        public Maybe<U> Bind<U>(Func<T, Maybe<U>> bind)
        {
            return HasValue ? bind(Value) : Maybe<U>.Nothing;
        }

        public async Task<Maybe<U>> MapAsync<U>(Func<T, Task<U>> map)
        {
            return HasValue ? await map(Value) : Maybe<U>.Nothing;
        }

        public async Task<Maybe<U>> BindAsync<U>(Func<T, Task<Maybe<U>>> bind)
        {
            return HasValue ? await bind(Value) : Maybe<U>.Nothing;
        }

        public Maybe<T> If(Func<T, bool> predicate)
        {
            if (!HasValue) return Nothing;
            return predicate(Value) ? this : Nothing;
        }

        public Maybe<T> Unless(Func<T, bool> predicate)
        {
            if (!HasValue) return Nothing;
            return predicate(Value) ? Nothing : this;
        }

        public Maybe<T> Do(Action<T> action)
        {
            if (HasValue) action(Value);
            return this;
        }

        public static implicit operator Maybe<T>(T value)
        {
            return new Maybe<T>(value);
        }

        public static bool operator ==(T value, Maybe<T> maybe)
        {
            return maybe.HasValue && maybe.Value.Equals(value);
        }

        public static bool operator !=(T value, Maybe<T> maybe)
        {
            return !(value == maybe);
        }

        public static bool operator ==(Maybe<T> maybe, T value)
        {
            return maybe.HasValue && maybe.Value.Equals(value);
        }

        public static bool operator !=(Maybe<T> maybe, T value)
        {
            return !(maybe == value);
        }

        public static bool operator ==(Maybe<T> first, Maybe<T> second)
        {
            return first.Equals(second);
        }

        public static bool operator !=(Maybe<T> first, Maybe<T> second)
        {
            return !(first == second);
        }

        public override bool Equals(object obj)
        {
            if (!(obj is Maybe<T>)) return false;

            var other = (Maybe<T>)obj;
            return Equals(other);
        }

        public bool Equals(Maybe<T> other)
        {
            if (!HasValue && !other.HasValue) return true;
            if (!HasValue || !other.HasValue) return false;

            return _value.Equals(other._value);
        }

        public override int GetHashCode()
        {
            // ReSharper disable once NonReadonlyMemberInGetHashCode
            return HasValue ? _value.GetHashCode() : 0;
        }

        public override string ToString()
        {
            return HasValue ? Value.ToString() : "Nothing";
        }
    }
}

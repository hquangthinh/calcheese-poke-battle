﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalcheesePokeBattle.Common.Monads
{
    public static class MaybeExtensions
    {
        public static TResult With<T, TResult>(this T input, Func<T, TResult> evaluator)
            where T : class
            where TResult : class
        {
            return input == null ? null : evaluator(input);
        }

        public static TResult? Return<T, TResult>(this T input, Func<T, TResult> evaluator)
            where T : class
            where TResult : struct
        {
            return input == null ? (TResult?)null : evaluator(input);
        }

        public static TResult Return<T, TResult>(this T input, Func<T, TResult> evaluator, TResult defaultValue)
            where T : class
        {
            return input == null ? defaultValue : evaluator(input);
        }

        public static T If<T>(this T input, Func<T, bool> predicate) where T : class
        {
            if (input == null) return null;
            return predicate(input) ? input : null;
        }

        public static T Unless<T>(this T input, Func<T, bool> predicate) where T : class
        {
            if (input == null) return null;
            return predicate(input) ? null : input;
        }

        public static T Do<T>(this T input, Action<T> action) where T : class
        {
            if (input == null) return null;
            action(input);
            return input;
        }

        public static Maybe<T> AsMaybe<T>(this T value)
        {
            return value;
        }

        public static Maybe<T> AsMaybe<T>(this T? value) where T : struct
        {
            return value ?? Maybe<T>.Nothing;
        }

        public static Maybe<U> Select<T, U>(this Maybe<T> self, Func<T, U> map)
        {
            return self.Map(map);
        }

        public static Maybe<U> SelectMany<T, U>(this Maybe<T> self, Func<T, Maybe<U>> bind)
        {
            if (bind == null) throw new ArgumentNullException(nameof(bind));
            return self.HasValue ? bind(self.Value) : Maybe<U>.Nothing;
        }

        public static Maybe<V> SelectMany<T, U, V>(this Maybe<T> self, Func<T, Maybe<U>> bind, Func<T, U, V> project)
        {
            if (bind == null) throw new ArgumentNullException(nameof(bind));
            if (project == null) throw new ArgumentNullException(nameof(project));

            if (!self.HasValue) return Maybe<V>.Nothing;

            var maybeU = bind(self.Value);
            return maybeU.HasValue ? project(self.Value, maybeU.Value) : Maybe<V>.Nothing;
        }

        public static Maybe<T> Where<T>(this Maybe<T> self, Func<T, bool> predicate)
        {
            if (!self.HasValue) return Maybe<T>.Nothing;
            return predicate(self.Value) ? self : Maybe<T>.Nothing;
        }

        public static Maybe<T> OnNoValueRaiseError<T>(this Maybe<T> mb, string message)
        {
            if (!mb.HasValue) throw new ApplicationException(message);
            return mb;
        }

        public static Maybe<T> OnNoValueRaiseError<T, TException>(this Maybe<T> mb, Func<TException> exceptionFactory) where TException : Exception
        {
            if (!mb.HasValue) throw exceptionFactory();
            return mb;
        }

        public static T Unwrap<T>(this Maybe<T> mb, T defaultValue = default(T))
        {
            return mb.HasValue ? mb.Value : defaultValue;
        }

        public static T Unwrap<T>(this Maybe<T> mb, Func<T> defaultValueFactory)
        {
            return mb.HasValue ? mb.Value : defaultValueFactory();
        }

        public static U? MapAsNullable<T, U>(this Maybe<T> mb, Func<T, U> map) where U : struct
        {
            return mb.HasValue ? map(mb.Value) : (U?)null;
        }

        public static Maybe<bool> AsJustTrue(this bool target)
        {
            return target ? true : Maybe<bool>.Nothing;
        }

        public static Maybe<bool> AsJustFalse(this bool target)
        {
            return target ? Maybe<bool>.Nothing : false;
        }

        public static async Task<Maybe<U>> BindAsync<T, U>(this Task<Maybe<T>> target, Func<T, Maybe<U>> bind)
        {
            var ma = await target;
            return ma.HasValue ? bind(ma.Value) : Maybe<U>.Nothing;
        }

        public static async Task<Maybe<U>> MapAsync<T, U>(this Task<Maybe<T>> target, Func<T, U> map)
        {
            var ma = await target;
            return ma.HasValue ? map(ma.Value) : Maybe<U>.Nothing;
        }

        public static async Task<T> UnwrapAsync<T>(this Task<Maybe<T>> target, T defaultValue = default(T))
        {
            var ma = await target;
            return ma.HasValue ? ma.Value : defaultValue;
        }
    }
}

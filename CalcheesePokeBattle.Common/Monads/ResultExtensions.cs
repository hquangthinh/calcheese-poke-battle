﻿using System;
using System.Threading.Tasks;
using CalcheesePokeBattle.Common.Extensions;

namespace CalcheesePokeBattle.Common.Monads
{
    public static class ResultExtensions
    {
        public static Result<T> AsResult<T>(this T value, string errorMessage) where T : class
        {
            return value == null
                ? Result.Fail<T>(errorMessage)
                : Result.Success(value);
        }

        public static Result<T> AsResult<T>(this T? nullable, string errorMessage) where T : struct
        {
            return nullable.HasValue
                ? Result.Success(nullable.Value)
                : Result.Fail<T>(errorMessage);
        }

        public static Result<T> ToResult<T>(this Maybe<T> maybe, string errorMessage)
        {
            return maybe.HasValue
                ? Result.Success(maybe.Value)
                : Result.Fail<T>(errorMessage);
        }

        /// <summary>
        /// Alias of Map
        /// </summary>
        public static Result<K> OnSuccess<T, K>(this Result<T> result, Func<T, K> func)
        {
            return Map(result, func);
        }

        /// <summary>
        /// Alias of Bind
        /// </summary>
        public static Result<K> OnSuccess<T, K>(this Result<T> result, Func<T, Result<K>> func)
        {
            return Bind(result, func);
        }

        public static TResult OnFailure<TResult>(this TResult result, Action<string> action) where TResult : Result
        {
            if (result.IsFailure) action(result.Error);
            return result;
        }

        public static TResult OnFailureRaiseError<TResult, TException>(this TResult input, Func<string, TException> exceptionFactory)
            where TResult : Result
            where TException : Exception
        {
            if (input.IsFailure) throw exceptionFactory(input.Error);
            return input;
        }

        public static Result<T> Ensure<T>(this Result<T> result, Func<T, bool> predicate, string errorMessage)
        {
            if (result.IsFailure) return result;

            return predicate(result.Value) ? result : Result.Fail<T>(errorMessage);
        }

        public static Result<T> Ensure<T>(this Result<T> result, Func<T, bool> predicate, Func<T, string> errorMessage)
        {
            if (result.IsFailure) return result;

            return predicate(result.Value) ? result : Result.Fail<T>(errorMessage(result.Value));
        }

        public static Result<K> Map<T, K>(this Result<T> result, Func<T, K> func)
        {
            return result.IsFailure
                ? Result.Fail<K>(result.Error)
                : Result.Success(func(result.Value));
        }

        public static Result<K> Map<K>(this Result result, Func<K> func)
        {
            return result.IsFailure
                ? Result.Fail<K>(result.Error)
                : Result.Success(func());
        }

        public static Result<K> Bind<K>(this Result result, Func<Result<K>> func)
        {
            return result.IsFailure
                ? Result.Fail<K>(result.Error)
                : func();
        }

        public static Result<K> Bind<T, K>(this Result<T> result, Func<T, Result<K>> func)
        {
            return result.IsFailure
                ? Result.Fail<K>(result.Error)
                : func(result.Value);
        }

        public static Result<T> OnSuccess<T>(this Result<T> result, Action<T> action)
        {
            if (result.IsSuccess) action(result.Value);
            return result;
        }

        public static T OnBoth<R, T>(this R result, Func<R, T> func) where R : Result
        {
            return func(result);
        }

        public static Result OnSuccess(this Result result, Action action)
        {
            if (result.IsSuccess) action();
            return result;
        }

        public static Result Bind(this Result result, Func<Result> action)
        {
            return result.IsSuccess ? action() : result;
        }

        public static Result<U> Select<T, U>(this Result<T> self, Func<T, U> map)
        {
            return self.Map(map);
        }

        public static Result<U> SelectMany<T, U>(this Result<T> self, Func<T, Result<U>> bind)
        {
            if (bind == null) throw new ArgumentNullException(nameof(bind));

            return Bind(self, bind);
        }

        public static Result<V> SelectMany<T, U, V>(this Result<T> self, Func<T, Result<U>> bind, Func<T, U, V> project)
        {
            if (bind == null) throw new ArgumentNullException(nameof(bind));
            if (project == null) throw new ArgumentNullException(nameof(project));

            if (self.IsFailure) return Result.Fail<V>(self.Error);

            var resultU = bind(self.Value);
            return resultU.IsSuccess
                ? Result.Success(project(self.Value, resultU.Value))
                : Result.Fail<V>(resultU.Error);
        }
    }

    public static class AsyncResultExtensions
    {
        public static async Task<Result<T>> AsAsyncResult<T>(this Task<T> task, string errorMessage)
        {
            var value = await task;
            return value != null
                ? Result.Success(value)
                : Result.Fail<T>(errorMessage);
        }

        public static async Task<Result<T>> OnSuccessAsync<T>(this Result<T> result, Func<T, Task> action)
        {
            if (result.IsSuccess) await action(result.Value);
            return result;
        }

        /// <summary>
        /// Alias of Map
        /// </summary>
        public static async Task<Result<K>> OnSuccessAsync<T, K>(this Result<T> result, Func<T, Task<K>> map)
        {
            if (result.IsFailure) return Result.Fail<K>(result.Error);
            var kResult = await map(result.Value);
            return Result.Success(kResult);
        }

        /// <summary>
        /// Alias of Bind
        /// </summary>
        public static async Task<Result<K>> OnSuccessAsync<T, K>(this Result<T> result, Func<T, Task<Result<K>>> bind)
        {
            if (result.IsFailure) return Result.Fail<K>(result.Error);
            var kResult = await bind(result.Value);
            return kResult;
        }

        public static async Task<Result<T>> OnSuccessAsync<T>(this Task<Result<T>> taskResult, Action<T> action)
        {
            var result = await taskResult;
            if (result.IsSuccess) action(result.Value);
            return result;
        }

        public static async Task<Result<T>> OnSuccessAsync<T>(this Task<Result<T>> taskResult, Func<T, Task> action)
        {
            var result = await taskResult;
            if (result.IsSuccess) await action(result.Value);
            return result;
        }

        /// <summary>
        /// Alias of Map
        /// </summary>
        public static async Task<Result<K>> OnSuccessAsync<T, K>(this Task<Result<T>> taskResult, Func<T, K> map)
        {
            var result = await taskResult;
            if (result.IsFailure) return Result.Fail<K>(result.Error);
            var kResult = map(result.Value);
            return Result.Success(kResult);
        }

        /// <summary>
        /// Alias of Map
        /// </summary>
        public static async Task<Result<K>> OnSuccessAsync<T, K>(this Task<Result<T>> taskResult, Func<T, Task<K>> map)
        {
            var result = await taskResult;
            if (result.IsFailure) return Result.Fail<K>(result.Error);
            var kResult = await map(result.Value);
            return Result.Success(kResult);
        }

        public static async Task<Result<K>> OnSuccessAsync<T, K>(this Task<Result<T>> task, Func<T, Result<K>> bind)
        {
            var result = await task;
            return result.IsFailure
                ? Result.Fail<K>(result.Error)
                : bind(result.Value);
        }

        public static async Task<Result<K>> OnSuccessAsync<T, K>(this Task<Result<T>> task, Func<T, Task<Result<K>>> bind)
        {
            var result = await task;
            if (result.IsFailure) return Result.Fail<K>(result.Error);
            return await bind(result.Value);
        }

        public static async Task<T> UnwrapAsync<T>(this Task<Result<T>> task)
        {
            try
            {
                var result = await task;
                return result.Value;
            }
            catch (Exception e)
            {
                e.ReThrow();
            }
            return default(T);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CalcheesePokeBattle.Common.Monads
{
    public class Result
    {
        public bool IsSuccess { get; }
        public string Error { get; }
        public bool IsFailure => !IsSuccess;

        protected Result(bool isSuccess, string error)
        {
            if (isSuccess && error != string.Empty) throw new InvalidOperationException();
            if (!isSuccess && error == string.Empty) throw new InvalidOperationException();

            IsSuccess = isSuccess;
            Error = error;
        }

        public static Result Fail(string message)
        {
            return new Result(false, message);
        }

        public static Result Fail(string messagePrefix, string message)
        {
            return new Result(false, $"{messagePrefix}: {message}");
        }

        public static Result<T> Fail<T>(string message)
        {
            return new Result<T>(default(T), false, message);
        }

        public static Result<T> Fail<T>(string messagePrefix, string message)
        {
            return new Result<T>(default(T), false, $"{messagePrefix}: {message}");
        }

        public static Result<T> Fail<T>(T value, string errorMessage)
        {
            return new Result<T>(value, false, errorMessage);
        }

        public static Result Success()
        {
            return new Result(true, string.Empty);
        }

        public static Result<T> Success<T>(T value)
        {
            return new Result<T>(value, true, string.Empty);
        }

        public static Result Combine(params Result[] results)
        {
            return results.Any(r => r.IsFailure)
                ? Fail(string.Join("\n", results.Where(r => r.IsFailure).Select(r => r.Error).Distinct()))
                : Success();
        }

        public static Result<K> Combine<T, K>(IEnumerable<Result<T>> results, Func<IEnumerable<T>, K> combineReducer)
        {
            var enumerable = results as Result<T>[] ?? results.ToArray();
            return enumerable.Any(r => r.IsFailure)
                ? Fail<K>(string.Join("\n", enumerable.Where(r => r.IsFailure).Select(r => r.Error).Distinct()))
                : Success(combineReducer(enumerable.Select(r => r.Value)));
        }

        public static Result<K> Reduce<T, K>(K seed, Func<K, T, K> reducer, IEnumerable<Result<T>> results)
        {
            var enumerable = results as Result<T>[] ?? results.ToArray();
            return enumerable.Any(r => r.IsFailure)
                ? Fail<K>(string.Join("\n", enumerable.Where(r => r.IsFailure).Select(r => r.Error).Distinct()))
                : Success(enumerable.Select(r => r.Value).Aggregate(seed, reducer));
        }

        public static Result<T> Reduce<T>(Func<T, T, T> reducer, IEnumerable<Result<T>> results)
        {
            var enumerable = results as Result<T>[] ?? results.ToArray();
            return enumerable.Any(r => r.IsFailure)
                ? Fail<T>(string.Join("\n", enumerable.Where(r => r.IsFailure).Select(r => r.Error).Distinct()))
                : Success(enumerable.Select(r => r.Value).Aggregate(reducer));
        }

        public static Func<T1, Result<TResult>> ComposeK<T1, T2, TResult>
        (Func<T2, Result<TResult>> f
            , Func<T1, Result<T2>> g
        )
        {
            return a => g(a).Bind(f);
        }

        public static Func<T1, Result<TResult>> ComposeK<T1, T2, T3, TResult>
        (Func<T3, Result<TResult>> f
            , Func<T2, Result<T3>> g
            , Func<T1, Result<T2>> h
        )
        {
            return a => h(a).Bind(g).Bind(f);
        }

        public static Func<T1, Result<TResult>> ComposeK<T1, T2, T3, T4, TResult>
        (Func<T4, Result<TResult>> f
            , Func<T3, Result<T4>> g
            , Func<T2, Result<T3>> h
            , Func<T1, Result<T2>> i
        )
        {
            return a => i(a).Bind(h).Bind(g).Bind(f);
        }

        public static Func<T1, Result<TResult>> ComposeK<T1, T2, T3, T4, T5, TResult>
        (Func<T5, Result<TResult>> f
            , Func<T4, Result<T5>> g
            , Func<T3, Result<T4>> h
            , Func<T2, Result<T3>> i
            , Func<T1, Result<T2>> j
        )
        {
            return a => j(a).Bind(i).Bind(h).Bind(g).Bind(f);
        }

        public static Func<T1, Result<TResult>> ComposeK<T1, T2, T3, T4, T5, T6, TResult>
        (Func<T6, Result<TResult>> f
            , Func<T5, Result<T6>> g
            , Func<T4, Result<T5>> h
            , Func<T3, Result<T4>> i
            , Func<T2, Result<T3>> j
            , Func<T1, Result<T2>> k
        )
        {
            return a => k(a).Bind(j).Bind(i).Bind(h).Bind(g).Bind(f);
        }
    }

    public class Result<T> : Result
    {
        private readonly T _value;

        public T Value
        {
            get
            {
                if (!IsSuccess) throw new InvalidOperationException(Error);
                return _value;
            }
        }

        public T Unwrap(T defaultValue)
        {
            return IsSuccess ? _value : defaultValue;
        }

        protected internal Result(T value, bool isSuccess, string error)
            : base(isSuccess, error)
        {
            _value = value;
        }

        protected bool Equals(Result<T> other)
        {
            return EqualityComparer<T>.Default.Equals(_value, other._value);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((Result<T>)obj);
        }

        public override int GetHashCode()
        {
            return EqualityComparer<T>.Default.GetHashCode(_value);
        }
    }
}

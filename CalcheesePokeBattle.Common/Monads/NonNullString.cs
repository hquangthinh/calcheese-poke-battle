﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalcheesePokeBattle.Common.Monads
{
    public class NonNullString
    {
        public string Value { get; }

        protected NonNullString(string value)
        {
            Value = value;
        }

        public static NonNullString Of(string value)
        {
            return new NonNullString(value ?? "");
        }

        public static implicit operator NonNullString(string value)
        {
            return new NonNullString(value ?? "");
        }

        public static explicit operator string(NonNullString str)
        {
            return str.Value;
        }

        public bool IsEmpty => string.IsNullOrEmpty(Value);

        private bool Equals(NonNullString other)
        {
            return string.Equals(Value, other.Value);
        }

        public override bool Equals(object obj)
        {
            return obj is NonNullString && Equals((NonNullString)obj);
        }

        public override int GetHashCode()
        {
            return Value?.GetHashCode() ?? 0;
        }

        public override string ToString()
        {
            return Value;
        }
    }
}

﻿using System;
using System.Collections.Concurrent;

namespace CalcheesePokeBattle.Common.Concurrency.Synchronization
{
    public interface IKeyBaseSync<TKey>
    {
        TResult PerformAction<TResult>(TKey key, Func<TKey, TResult> action);
        void PerformAction(TKey key, Action<TKey> action);
    }

    public class KeyBasedSynchronization
    {
        public static IKeyBaseSync<TKey> Get<TKey>()
        {
            return new LocalMachineKeyBaseSync<TKey>();
        }
    }

    public class LocalMachineKeyBaseSync<TKey> : IKeyBaseSync<TKey>
    {
        private readonly ConcurrentDictionary<TKey, object> _keyLocks = new ConcurrentDictionary<TKey, object>();

        public TResult PerformAction<TResult>(TKey key, Func<TKey, TResult> action)
        {
            TResult result;
            var keyLock = _keyLocks.GetOrAdd(key, new object());
            lock (keyLock) result = action(key);
            /*
             * Let the GC collect the key locks when the Sync object is destroyed.
             * Use a bit more memory (an Object instance for each key) in exchange
             * for 25 to 40 per cent better latency
             */
            // _keyLocks.TryRemove(key, out keyLock);
            return result;
        }

        public void PerformAction(TKey key, Action<TKey> action)
        {
            PerformAction(key, k =>
            {
                action(k);
                return true;
            });
        }
    }
}

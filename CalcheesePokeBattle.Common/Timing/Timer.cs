﻿using System;
using System.Diagnostics;
using CalcheesePokeBattle.Common.Monads;

namespace CalcheesePokeBattle.Common.Timing
{
    public class Timer
    {
        public static T Time<T>(Func<T> action, Action<T, TimeSpan> onComplete)
        {
            var stopped = false;
            var sw = Stopwatch.StartNew();
            try
            {
                var result = action();
                sw.Stop();
                stopped = true;
                onComplete(result, sw.Elapsed);
                return result;
            }
            finally
            {
                if (!stopped) sw.Stop();
            }
        }

        public static PagingResult<T> Time<T>(Func<PagingResult<T>> action)
        {
            return Time(action, (result, elapsed) => result.ExecutionDuration = (long)elapsed.TotalMilliseconds);
        }
    }
}

﻿using System;
using System.IO;
using System.Security.Cryptography;

namespace CalcheesePokeBattle.Common.Cryptography
{
    public interface IDataEncryptionService
    {
        string EncryptString(string original);

        string DecryptString(string base64EncryptedString);
    }

    public class DefaultAesDataEncryptionService : IDataEncryptionService
    {
        // The key and iv is generated randomly by Aes.Create() and convert to Base64 string
        private readonly string _defaultKeyInBase64String = "sEx1Nja6Sy/Jmx7CAiXXaRlT86rbCM5jbnj9O5myNKA=";
        private readonly string _defaultIvInBase64String = "GUsL5JDQokSPYlYP9pyJRw==";

        public DefaultAesDataEncryptionService()
        {
        }

        public DefaultAesDataEncryptionService(string defaultKeyInBase64String, string defaultIvInBase64String)
        {
            _defaultKeyInBase64String = defaultKeyInBase64String;
            _defaultIvInBase64String = defaultIvInBase64String;
        }

        public string EncryptString(string original)
        {
            if (string.IsNullOrEmpty(original)) return string.Empty;

            var key = Convert.FromBase64String(_defaultKeyInBase64String);
            var iv = Convert.FromBase64String(_defaultIvInBase64String);

            // Encrypt the string to an array of bytes.
            var encrypted = EncryptStringToBytesAes(original, key, iv);

            return Convert.ToBase64String(encrypted);
        }

        public string DecryptString(string base64EncryptedString)
        {
            if (string.IsNullOrEmpty(base64EncryptedString)) return string.Empty;

            var encryptedBytes = Convert.FromBase64String(base64EncryptedString);
            var key = Convert.FromBase64String(_defaultKeyInBase64String);
            var iv = Convert.FromBase64String(_defaultIvInBase64String);

            // Encrypt the string to an array of bytes.
            return DecryptStringFromBytesAes(encryptedBytes, key, iv);
        }

        private byte[] EncryptStringToBytesAes(string plainText, byte[] key, byte[] iv)
        {
            // Check arguments.
            if (plainText == null || plainText.Length <= 0)
                throw new ArgumentNullException(nameof(plainText));

            if (key == null || key.Length <= 0)
                throw new ArgumentNullException(nameof(key));

            if (iv == null || iv.Length <= 0)
                throw new ArgumentNullException(nameof(iv));

            byte[] encrypted;
            // Create an Aes object
            // with the specified key and iv.
            using (var aesAlg = Aes.Create())
            {
                aesAlg.Key = key;
                aesAlg.IV = iv;

                // Create a decrytor to perform the stream transform.
                var encryptor = aesAlg.CreateEncryptor(aesAlg.Key, aesAlg.IV);

                // Create the streams used for encryption.
                using (var msEncrypt = new MemoryStream())
                {
                    using (var csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                    {
                        using (var swEncrypt = new StreamWriter(csEncrypt))
                        {
                            //Write all data to the stream.
                            swEncrypt.Write(plainText);
                        }
                        encrypted = msEncrypt.ToArray();
                    }
                }
            }

            // Return the encrypted bytes from the memory stream.
            return encrypted;
        }

        private string DecryptStringFromBytesAes(byte[] cipherText, byte[] key, byte[] iv)
        {
            // Check arguments.
            if (cipherText == null || cipherText.Length <= 0)
                throw new ArgumentNullException(nameof(cipherText));
            if (key == null || key.Length <= 0)
                throw new ArgumentNullException(nameof(key));
            if (iv == null || iv.Length <= 0)
                throw new ArgumentNullException(nameof(iv));

            // Declare the string used to hold
            // the decrypted text.
            var plaintext = string.Empty;

            // Create an Aes object
            // with the specified key and iv.
            using (var aesAlg = Aes.Create())
            {
                aesAlg.Key = key;
                aesAlg.IV = iv;

                // Create a decrytor to perform the stream transform.
                var decryptor = aesAlg.CreateDecryptor(aesAlg.Key, aesAlg.IV);

                // Create the streams used for decryption.
                using (var msDecrypt = new MemoryStream(cipherText))
                {
                    using (var csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                    {
                        using (var srDecrypt = new StreamReader(csDecrypt))
                        {
                            // Read the decrypted bytes from the decrypting stream
                            // and place them in a string.
                            plaintext = srDecrypt.ReadToEnd();
                        }
                    }
                }
            }
            return plaintext;
        }
    }
}

using System;
using System.Security.Cryptography;
using System.Text;

namespace CalcheesePokeBattle.Common.Cryptography
{
    public interface IOneWayEncryptionService
    {
        string GetHashString(string input);

        bool VerifyHash(string input, string hash);
    }

    /// <summary>
    /// Encrypt the data to MD5 hash
    /// </summary>
    public class MD5DataEncryptionService : IOneWayEncryptionService
    {
        public string GetHashString(string input)
        {
            if (string.IsNullOrEmpty(input))
                throw new ArgumentNullException(nameof(input));

            var md5Hasher = new MD5CryptoServiceProvider();
            var hashData = md5Hasher.ComputeHash(Encoding.Default.GetBytes(input));
            var sBuilder = new StringBuilder();
            // Loop through each byte of the hashed data 
            // and format each one as a hexadecimal string.
            foreach (byte b in hashData)
            {
                sBuilder.Append(b.ToString("x2"));
            }
            return sBuilder.ToString();
        }

        public bool VerifyHash(string input, string hash)
        {
            // Hash the input.
            var hashOfInput = GetHashString(input);

            // Create a StringComparer an compare the hashes.
            var comparer = StringComparer.OrdinalIgnoreCase;

            return 0 == comparer.Compare(hashOfInput, hash);
        }
    }
}
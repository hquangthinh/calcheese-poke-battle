﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalcheesePokeBattle.Common.DTO
{
    public interface IWildcardSearch
    {
        /// <summary>
        /// The string search match type. I.e  Starts With(startsWith) || Contains(contains) || Match(match) || Ends With(endsWith)
        /// NOTE: Currently ASP.NET performs string->enum auto-conversion
        /// </summary>
        WildcardSearchType SearchMatchType { get; set; }

        /// <summary>
        /// Build wildcard search params based on search match type
        /// </summary>
        void BuildParamsForWildcardSearch();
    }

    public enum WildcardSearchType
    {
        Match,
        StartsWith,
        Contains,
        EndsWith
    }
}

﻿using System;
using System.IO;

namespace CalcheesePokeBattle.Common.DTO
{
    public class BlobInformation
    {
        public Uri BlobUri { get; set; }

        public string BlobName => BlobUri.Segments[BlobUri.Segments.Length - 1];

        public string BlobNameWithoutExtension => Path.GetFileNameWithoutExtension(BlobName);
    }

    public class BatchImportQueueMessage
    {
        public string Message { get; set; }

        public string IpAddress { get; set; }
    }
}
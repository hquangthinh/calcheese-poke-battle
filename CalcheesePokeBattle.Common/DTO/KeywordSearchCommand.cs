﻿using System;
using System.Collections.Generic;
using System.Reflection;
using CalcheesePokeBattle.Common.Monads;

namespace CalcheesePokeBattle.Common.DTO
{
    public class KeywordSearchCommand : Paging, IWildcardSearch
    {
        private static readonly HashSet<string> DefaultExactMatchProperties = new HashSet<string>
        {
            "PageSize", "PageNumber", "SearchMatchType", "OrderBy", "OrderAsc", "ID", "id", "Id"
        };

        public string Keyword { get; set; }

        public WildcardSearchType SearchMatchType { get; set; }

        public void BuildParamsForWildcardSearch()
        {
            QuoteSearchParamValueWithWildcard(SearchMatchType);
        }

        private void QuoteSearchParamValueWithWildcard(WildcardSearchType wildcardSearchType)
        {
            var wildcardSearchPattern = "{0}{1}";//default for matching search
            var wildcardString = "%"; //default for other search type except for matching
            switch (wildcardSearchType)
            {
                case WildcardSearchType.StartsWith:
                    wildcardSearchPattern = "{1}{0}";
                    break;
                case WildcardSearchType.Contains:
                    wildcardSearchPattern = "{0}{1}{0}";
                    break;
                case WildcardSearchType.EndsWith:
                    wildcardSearchPattern = "{0}{1}";
                    break;
                case WildcardSearchType.Match:
                    wildcardString = string.Empty;
                    break;
                default:
                    wildcardString = string.Empty;
                    break;
            }

            var properties = GetType().GetProperties();
            foreach (var property in properties)
            {
                if (!DataTypeSupportWildcardSearch(property.PropertyType) || !PropertySupportWildcardSearch(property))
                {
                    continue;
                }

                var propOriginalValue = property.GetValue(this);

                if (propOriginalValue == null) continue;

                propOriginalValue = EscapeSpecialCharacters(propOriginalValue.ToString());

                property.SetValue(this,
                    PropertyAlwaysUsesStartWithMatching(property)
                        ? $"{propOriginalValue}%"
                        : string.Format(wildcardSearchPattern, wildcardString, propOriginalValue));
            }
        }

        private bool DataTypeSupportWildcardSearch(Type type)
        {
            return type == typeof(string);
        }

        private bool PropertyAlwaysUsesStartWithMatching(PropertyInfo property)
        {
            return property.GetCustomAttribute(typeof(SearchStartWithAttribute)) != null;
        }

        private bool PropertySupportWildcardSearch(PropertyInfo property)
        {
            return !DefaultExactMatchProperties.Contains(property.Name)
                && property.GetCustomAttribute(typeof(SearchExactMatchAttribute)) == null;
        }

        private string EscapeSpecialCharacters(string searchInput)
        {
            return searchInput.Replace("'", "_");
        }
    }

    public class SearchExactMatchAttribute : Attribute
    {
    }

    public class SearchStartWithAttribute : Attribute
    {
    }
}
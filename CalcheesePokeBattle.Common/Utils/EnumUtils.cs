﻿using System;
using System.Linq;
using System.Reflection;

namespace CalcheesePokeBattle.Common.Utils
{
    public class EnumUtils
    {
        public static T FromString<T>(string valueString, T defaultValue) where T : struct
        {
            return TryParse(valueString, FromString<T>, defaultValue);
        }

        public static T? FromString<T>(string valueString) where T : struct
        {
            if (!typeof(T).IsEnum) throw new ArgumentException("Generic Type 'T' must be an Enum");

            if (!string.IsNullOrEmpty(valueString))
            {
                if (Enum.GetNames(typeof(T)).Any(e => string.Equals(e.Trim(), valueString.Trim(), StringComparison.InvariantCultureIgnoreCase)))
                {
                    return (T)Enum.Parse(typeof(T), valueString, true);
                }
            }
            return null;
        }

        public static T FromInteger<T>(int value, T defaultValue) where T : struct
        {
            return TryParse(value, FromInteger<T>, defaultValue);
        }

        public static T? FromInteger<T>(int value) where T : struct
        {
            Type t = typeof(T);
            if (!t.IsEnum) throw new ArgumentException("Generic Type 'T' must be an Enum");
            int[] validValues = Enum.GetValues(t).OfType<T>().Cast<int>().ToArray();
            if (validValues.Any(x => x == value))
            {
                return (T)Enum.ToObject(t, value);
            }
            return null;
        }

        public static T FromInteger<T>(string value, T defaultValue) where T : struct
        {
            return TryParse(value, FromInteger<T>, defaultValue);
        }

        public static T? FromInteger<T>(string value) where T : struct
        {
            int intValue;
            return int.TryParse(value, out intValue) ? FromInteger<T>(intValue) : null;
        }

        private static TResult TryParse<T, TResult>(T value, Func<T, TResult?> parseFn, TResult defaultValue)
            where TResult : struct
        {
            return parseFn(value) ?? defaultValue;
        }
    }

    public static class EnumExtensions
    {
        public static TExpected GetAttributeValue<T, TExpected>(this Enum enumeration, Func<T, TExpected> expression)
        where T : Attribute
        {
            var memberInfo = enumeration
                .GetType()
                .GetMember(enumeration.ToString())
                .FirstOrDefault(member => member.MemberType == MemberTypes.Field);
            if (memberInfo != null)
            {
                var attribute =
                    memberInfo
                        .GetCustomAttributes(typeof(T), false)
                        .Cast<T>()
                        .FirstOrDefault();

                if (attribute == null)
                    return default(TExpected);

                return expression(attribute);
            }

            return default(TExpected);
        }
    }
}

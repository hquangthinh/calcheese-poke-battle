﻿using System;
using CalcheesePokeBattle.Common.FunctionExtensions;

namespace CalcheesePokeBattle.Common.Utils
{
    public enum CacheAction
    {
        /// <summary>
        /// No invalidation should be performed, returns the cached value
        /// </summary>
        Get,

        /// <summary>
        /// Invalidate the specified key, recalculates its value
        /// </summary>
        RefreshKey,

        /// <summary>
        /// Removes the specified key from the cache
        /// </summary>
        DeleteKey,

        /// <summary>
        /// Clears all the cached keys
        /// </summary>
        Reset
    }

    public class CacheCommand : CacheCommand<bool>
    {
        public static readonly CacheCommand Default = new CacheCommand { Action = CacheAction.Get };
    }

    public class CacheCommand<TContext>
    {
        public CacheAction Action { get; set; }
        public TContext Context { get; set; }

        public CacheCommand<TContext> Get()
        {
            Action = CacheAction.Get;
            return this;
        }

        public CacheCommand<TContext> DeleteKey()
        {
            Action = CacheAction.DeleteKey;
            return this;
        }

        public CacheCommand<TContext> RefreshKey()
        {
            Action = CacheAction.RefreshKey;
            return this;
        }

        public CacheCommand<TContext> Reset()
        {
            Action = CacheAction.Reset;
            return this;
        }
    }

    public static class CacheCommandBuilder
    {
        public static CacheCommand<T> AsCacheContext<T>(this T target)
        {
            return new CacheCommand<T>
            {
                Context = target,
                Action = CacheAction.Get
            };
        }
    }

    public class CacheConfig
    {
        public double? Lifespan { get; private set; }
        public int MaxItems { get; private set; }

        public static CacheConfig LifeTime(TimeSpan lifeTime)
        {
            return new CacheConfig { Lifespan = lifeTime.TotalMilliseconds, MaxItems = DefaultCacheMaxItems };
        }

        public CacheConfig TotalItems(int items)
        {
            MaxItems = items;
            return this;
        }

        private const int DefaultCacheMaxItems = 1000;

        public static readonly CacheConfig Default = new CacheConfig { Lifespan = null, MaxItems = DefaultCacheMaxItems };
        public static readonly CacheConfig PerRequest = LifeTime(TimeSpan.FromSeconds(2));
    }

    public static class Memoization
    {
        #region Parameterless Memoization

        public static Func<CacheCommand<TContext>, Func<TResult>> MemoizeCtx<TContext, TResult>(this Func<TContext, TResult> f, CacheConfig cacheConfig = null)
        {
            var value = default(TResult);
            var config = cacheConfig ?? CacheConfig.Default;
            var lastUpdateTime = DateTime.MinValue;
            var sync = new object();
            return opts => () =>
            {
                lock (sync)
                {
                    if (opts.Action == CacheAction.DeleteKey || opts.Action == CacheAction.Reset)
                    {
                        value = default(TResult);
                        lastUpdateTime = DateTime.MinValue;
                    }
                    else if (
                        opts.Action == CacheAction.RefreshKey
                        || lastUpdateTime == DateTime.MinValue
                        || (
                            config.Lifespan > 0
                            && (DateTime.UtcNow - lastUpdateTime).TotalMilliseconds > config.Lifespan
                        )
                    )
                    {
                        value = f(opts.Context);
                        lastUpdateTime = DateTime.UtcNow;
                    }
                    return value;
                }
            };
        }

        public static Func<CacheCommand, Func<TResult>> Memoize<TResult>(this Func<TResult> f, CacheConfig cacheConfig)
        {
            return MemoizeCtx<bool, TResult>(_ => f(), cacheConfig);
        }

        public static Func<TResult> Memoize<TResult>(this Func<TResult> f)
        {
            return Memoize(f, CacheConfig.Default)(CacheCommand.Default);
        }

        #endregion


        #region One-param Memoization

        public static Func<CacheCommand<TContext>, Func<T, TResult>> MemoizeCtx<TContext, T, TResult>(this Func<TContext, T, TResult> f, CacheConfig cacheConfig = null)
        {
            var config = cacheConfig ?? CacheConfig.Default;

            // cacheExpires :: Tuple<TResult, DateTime> -> bool
            var cacheExpires = Functions.Lambda((Tuple<TResult, DateTime> tuple) =>
                (DateTime.UtcNow - tuple.Item2).TotalMilliseconds > config.Lifespan);

            // valueFactory :: TContext -> T -> Tuple<TResult, DateTime>
            var valueFactory = Functions
                .Lambda((TContext ctx, T a) => Tuple.Create(f(ctx, a), DateTime.UtcNow))
                .Curry();

            var cache = new BlockingLRUCache<T, Tuple<TResult, DateTime>>(config.MaxItems);

            return opts =>
            {
                if (opts.Action == CacheAction.Reset)
                {
                    cache.Clear();
                    return _ => default(TResult);
                }
                return a =>
                {
                    var factory = valueFactory(opts.Context);
                    if (opts.Action == CacheAction.DeleteKey)
                    {
                        cache.Remove(a);
                        return default(TResult);
                    }
                    if (opts.Action == CacheAction.RefreshKey)
                    {
                        return cache.Update(a, factory).Item1;
                    }
                    if (config.Lifespan == null)
                    {
                        return cache.GetOrAdd(a, factory).Item1;
                    }
                    return cache.GetOrUpdate(a, factory, cacheExpires).Item1;
                };
            };
        }

        public static Func<CacheCommand, Func<T, TResult>> Memoize<T, TResult>(this Func<T, TResult> f, CacheConfig cacheConfig)
        {
            return MemoizeCtx<bool, T, TResult>((_, a) => f(a), cacheConfig);
        }

        public static Func<T, TResult> Memoize<T, TResult>(this Func<T, TResult> f)
        {
            return Memoize(f, CacheConfig.Default)(CacheCommand.Default);
        }

        #endregion


        #region Two-param Memoization

        private static Func<TContext, Tuple<T1, T2>, TResult> TuplifyCtx<TContext, T1, T2, TResult>(this Func<TContext, T1, T2, TResult> f)
        {
            return (ctx, t) => f(ctx, t.Item1, t.Item2);
        }

        private static Func<CacheCommand<TContext>, Func<T1, T2, TResult>> DetuplifyCtx<TContext, T1, T2, TResult>(this Func<CacheCommand<TContext>, Func<Tuple<T1, T2>, TResult>> f)
        {
            return opts => (a, b) => f(opts)(Tuple.Create(a, b));
        }

        private static Func<Tuple<T1, T2>, TResult> Tuplify<T1, T2, TResult>(this Func<T1, T2, TResult> f)
        {
            return t => f(t.Item1, t.Item2);
        }

        private static Func<CacheCommand, Func<T1, T2, TResult>> Detuplify<T1, T2, TResult>(this Func<CacheCommand, Func<Tuple<T1, T2>, TResult>> f)
        {
            return opts => (a, b) => f(opts)(Tuple.Create(a, b));
        }

        public static Func<CacheCommand<TContext>, Func<T1, T2, TResult>> MemoizeCtx<TContext, T1, T2, TResult>(this Func<TContext, T1, T2, TResult> f, CacheConfig cacheConfig = null)
        {
            return f.TuplifyCtx().MemoizeCtx(cacheConfig).DetuplifyCtx();
        }

        public static Func<CacheCommand, Func<T1, T2, TResult>> Memoize<T1, T2, TResult>(this Func<T1, T2, TResult> f, CacheConfig cacheConfig)
        {
            return f.Tuplify().Memoize(cacheConfig).Detuplify();
        }

        public static Func<T1, T2, TResult> Memoize<T1, T2, TResult>(this Func<T1, T2, TResult> f)
        {
            return Memoize(f, CacheConfig.Default)(CacheCommand.Default);
        }

        #endregion

        #region Three-param Memoization

        private static Func<TContext, Tuple<T1, T2, T3>, TResult> TuplifyCtx<TContext, T1, T2, T3, TResult>(this Func<TContext, T1, T2, T3, TResult> f)
        {
            return (ctx, t) => f(ctx, t.Item1, t.Item2, t.Item3);
        }

        private static Func<CacheCommand<TContext>, Func<T1, T2, T3, TResult>> DetuplifyCtx<TContext, T1, T2, T3, TResult>(this Func<CacheCommand<TContext>, Func<Tuple<T1, T2, T3>, TResult>> f)
        {
            return opts => (a, b, c) => f(opts)(Tuple.Create(a, b, c));
        }

        private static Func<Tuple<T1, T2, T3>, TResult> Tuplify<T1, T2, T3, TResult>(this Func<T1, T2, T3, TResult> f)
        {
            return t => f(t.Item1, t.Item2, t.Item3);
        }

        private static Func<CacheCommand, Func<T1, T2, T3, TResult>> Detuplify<T1, T2, T3, TResult>(this Func<CacheCommand, Func<Tuple<T1, T2, T3>, TResult>> f)
        {
            return opts => (a, b, c) => f(opts)(Tuple.Create(a, b, c));
        }

        public static Func<CacheCommand<TContext>, Func<T1, T2, T3, TResult>> MemoizeCtx<TContext, T1, T2, T3, TResult>(this Func<TContext, T1, T2, T3, TResult> f, CacheConfig cacheConfig = null)
        {
            return f.TuplifyCtx().MemoizeCtx(cacheConfig).DetuplifyCtx();
        }


        public static Func<CacheCommand, Func<T1, T2, T3, TResult>> Memoize<T1, T2, T3, TResult>(this Func<T1, T2, T3, TResult> f, CacheConfig cacheConfig)
        {
            return f.Tuplify().Memoize(cacheConfig).Detuplify();
        }

        public static Func<T1, T2, T3, TResult> Memoize<T1, T2, T3, TResult>(this Func<T1, T2, T3, TResult> f)
        {
            return Memoize(f, CacheConfig.Default)(CacheCommand.Default);
        }

        # endregion


        #region Four-param Memoization

        private static Func<TContext, Tuple<T1, T2, T3, T4>, TResult> TuplifyCtx<TContext, T1, T2, T3, T4, TResult>(this Func<TContext, T1, T2, T3, T4, TResult> f)
        {
            return (ctx, t) => f(ctx, t.Item1, t.Item2, t.Item3, t.Item4);
        }

        private static Func<CacheCommand<TContext>, Func<T1, T2, T3, T4, TResult>> DetuplifyCtx<TContext, T1, T2, T3, T4, TResult>(this Func<CacheCommand<TContext>, Func<Tuple<T1, T2, T3, T4>, TResult>> f)
        {
            return opts => (a, b, c, d) => f(opts)(Tuple.Create(a, b, c, d));
        }

        private static Func<Tuple<T1, T2, T3, T4>, TResult> Tuplify<T1, T2, T3, T4, TResult>(this Func<T1, T2, T3, T4, TResult> f)
        {
            return t => f(t.Item1, t.Item2, t.Item3, t.Item4);
        }

        private static Func<CacheCommand, Func<T1, T2, T3, T4, TResult>> Detuplify<T1, T2, T3, T4, TResult>(this Func<CacheCommand, Func<Tuple<T1, T2, T3, T4>, TResult>> f)
        {
            return opts => (a, b, c, d) => f(opts)(Tuple.Create(a, b, c, d));
        }

        public static Func<CacheCommand<TContext>, Func<T1, T2, T3, T4, TResult>> MemoizeCtx<TContext, T1, T2, T3, T4, TResult>(this Func<TContext, T1, T2, T3, T4, TResult> f, CacheConfig cacheConfig = null)
        {
            return f.TuplifyCtx().MemoizeCtx(cacheConfig).DetuplifyCtx();
        }

        public static Func<CacheCommand, Func<T1, T2, T3, T4, TResult>> Memoize<T1, T2, T3, T4, TResult>(this Func<T1, T2, T3, T4, TResult> f, CacheConfig cacheConfig)
        {
            return f.Tuplify().Memoize(cacheConfig).Detuplify();
        }

        public static Func<T1, T2, T3, T4, TResult> Memoize<T1, T2, T3, T4, TResult>(this Func<T1, T2, T3, T4, TResult> f)
        {
            return Memoize(f, CacheConfig.Default)(CacheCommand.Default);
        }

        #endregion


        #region Five-param Memoization

        private static Func<TContext, Tuple<T1, T2, T3, T4, T5>, TResult> TuplifyCtx<TContext, T1, T2, T3, T4, T5, TResult>(this Func<TContext, T1, T2, T3, T4, T5, TResult> f)
        {
            return (ctx, t) => f(ctx, t.Item1, t.Item2, t.Item3, t.Item4, t.Item5);
        }

        private static Func<CacheCommand<TContext>, Func<T1, T2, T3, T4, T5, TResult>> DetuplifyCtx<TContext, T1, T2, T3, T4, T5, TResult>(this Func<CacheCommand<TContext>, Func<Tuple<T1, T2, T3, T4, T5>, TResult>> f)
        {
            return opts => (a, b, c, d, e) => f(opts)(Tuple.Create(a, b, c, d, e));
        }

        private static Func<Tuple<T1, T2, T3, T4, T5>, TResult> Tuplify<T1, T2, T3, T4, T5, TResult>(this Func<T1, T2, T3, T4, T5, TResult> f)
        {
            return t => f(t.Item1, t.Item2, t.Item3, t.Item4, t.Item5);
        }

        private static Func<CacheCommand, Func<T1, T2, T3, T4, T5, TResult>> Detuplify<T1, T2, T3, T4, T5, TResult>(this Func<CacheCommand, Func<Tuple<T1, T2, T3, T4, T5>, TResult>> f)
        {
            return opts => (a, b, c, d, e) => f(opts)(Tuple.Create(a, b, c, d, e));
        }

        public static Func<CacheCommand<TContext>, Func<T1, T2, T3, T4, T5, TResult>> MemoizeCtx<TContext, T1, T2, T3, T4, T5, TResult>(this Func<TContext, T1, T2, T3, T4, T5, TResult> f, CacheConfig cacheConfig = null)
        {
            return f.TuplifyCtx().MemoizeCtx(cacheConfig).DetuplifyCtx();
        }

        public static Func<CacheCommand, Func<T1, T2, T3, T4, T5, TResult>> Memoize<T1, T2, T3, T4, T5, TResult>(this Func<T1, T2, T3, T4, T5, TResult> f, CacheConfig cacheConfig)
        {
            return f.Tuplify().Memoize(cacheConfig).Detuplify();
        }

        public static Func<T1, T2, T3, T4, T5, TResult> Memoize<T1, T2, T3, T4, T5, TResult>(this Func<T1, T2, T3, T4, T5, TResult> f)
        {
            return Memoize(f, CacheConfig.Default)(CacheCommand.Default);
        }

        #endregion
    }
}

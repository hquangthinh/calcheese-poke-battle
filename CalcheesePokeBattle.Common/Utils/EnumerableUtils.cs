﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CalcheesePokeBattle.Common.Utils
{
    public static class EnumerableUtils
    {
        public delegate bool MatchingFunc<T>(IEnumerable<T> xs, Func<T, bool> predicate);

        public static IEnumerable<int> Times(this int value)
        {
            for (var i = 1; i <= value; i++)
            {
                yield return i;
            }
        }

        public static object[] ToObjectArray<T>(this IEnumerable<T> sequence)
        {
            return sequence.Cast<object>().ToArray();
        }

        public static void Each<T>(this IEnumerable<T> sequence, Action<T> action)
        {
            foreach (var item in sequence)
            {
                action(item);
            }
        }

        public static void Each<T>(this IEnumerable<T> sequence, Action<T, int> action)
        {
            int i = 0;
            foreach (var item in sequence)
            {
                action(item, i++);
            }
        }

        public static void EachForModification<T>(this ICollection<T> collection, Action<T> action)
        {
            for (int i = collection.Count - 1; i >= 0; i--)
            {
                var item = collection.ElementAt(i);
                action(item);
            }
        }

        public static void EachForModification<T>(this ICollection<T> collection, Action<T, int> action)
        {
            for (int i = collection.Count - 1; i >= 0; i--)
            {
                var item = collection.ElementAt(i);
                action(item, i);
            }
        }

        public static IEnumerable<T> Rest<T>(this IEnumerable<T> sequence)
        {
            return sequence.Skip(1);
        }

        public static bool All<T>(this IEnumerable<T> sequence, Func<T, int, bool> predicate)
        {
            var index = 0;
            foreach (var a in sequence)
            {
                if (!predicate(a, index++)) return false;
            }
            return true;
        }

        public static bool Any<T>(this IEnumerable<T> sequence, Func<T, int, bool> predicate)
        {
            var index = 0;
            foreach (var a in sequence)
            {
                if (predicate(a, index++)) return true;
            }
            return false;
        }
    }
}

﻿using System;
using CalcheesePokeBattle.Common.Concurrency.Synchronization;

namespace CalcheesePokeBattle.Common.Utils
{
    public class BlockingLRUCache<TKey, TValue>
    {
        private readonly LRUCache<TKey, TValue> _cacheStorage;

        public BlockingLRUCache(int maxItems)
        {
            _cacheStorage = new LRUCache<TKey, TValue>(maxItems);
        }

        public TValue GetOrAdd(TKey key, Func<TKey, TValue> factory)
        {
            return _keySync.PerformAction(key, k =>
            {
                lock (_cacheStorage)
                {
                    TValue itemValue;
                    if (_cacheStorage.TryGetValue(k, out itemValue)) return itemValue;
                }

                return StoreValue(k, factory);
            });
        }

        public TValue GetOrUpdate(TKey key, Func<TKey, TValue> factory, Func<TValue, bool> shouldUpdate)
        {
            return _keySync.PerformAction(key, k =>
            {
                lock (_cacheStorage)
                {
                    TValue itemValue;
                    if (_cacheStorage.TryGetValue(k, out itemValue) && !shouldUpdate(itemValue)) return itemValue;
                }

                return StoreValue(k, factory);
            });
        }

        public void Clear()
        {
            lock (_cacheStorage) _cacheStorage.Clear();
        }

        public void Remove(TKey key)
        {
            lock (_cacheStorage) _cacheStorage.Remove(key);
        }

        public void Update(TKey key, TValue value)
        {
            lock (_cacheStorage) _cacheStorage[key] = value;
        }

        public TValue Update(TKey key, Func<TKey, TValue> valueFactory)
        {
            return _keySync.PerformAction(key, k => StoreValue(k, valueFactory));
        }

        private TValue StoreValue(TKey k, Func<TKey, TValue> valueFactory)
        {
            // calculating cache's value remains outside of the lock
            // to ensure cache's accessibility while the heavy work is happening
            var value = valueFactory(k);
            lock (_cacheStorage) _cacheStorage[k] = value;
            return value;
        }

        private readonly LocalMachineKeyBaseSync<TKey> _keySync = new LocalMachineKeyBaseSync<TKey>();
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CalcheesePokeBattle.Common.Utils
{
    public static class ListHelper
    {
        public static List<string> FromValueList(string commaSeparatedValueList)
        {
            if(string.IsNullOrEmpty(commaSeparatedValueList))
                return new List<string>();

            var values = commaSeparatedValueList.Split(new[] {','}, StringSplitOptions.RemoveEmptyEntries);

            return values.ToList();
        }

        public static List<T> FromValueList<T>(string commaSeparatedValueList, T defaultValue) where T : struct 
        {
            if (string.IsNullOrEmpty(commaSeparatedValueList))
                return new List<T>();

            var values = FromValueList(commaSeparatedValueList);

            return values.Select(s => EnumUtils.FromString(s, defaultValue)).ToList();
        }
    }
}

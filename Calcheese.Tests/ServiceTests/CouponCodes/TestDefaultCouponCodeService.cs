﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Calcheese.Tests.WindsorInstaller;
using CalcheesePokeBattle.DTO;
using CalcheesePokeBattle.Service;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Calcheese.Tests.ServiceTests.CouponCodes
{
    [TestClass]
    public class TestDefaultCouponCodeService : IocTestFixture
    {
        private UserProfileDTO GetCurrentProfileForTest()
            => new UserProfileDTO
            {
                Id = "29e2ecdf-ac39-4d29-bdc4-e6c834e90497"
            };

        [TestMethod]
        public void Test_Redeem_Code()
        {
            var service = ServiceFactory.GetService<ICouponCodeService>();
            var testCode = new CouponCodeDTO
            {
                Code = "00000003",
                ClientIpAddress = "test_agent",
                ClientInfo = "test_agent"
            };

            var result = service.RedeemCodeForPoint(testCode, GetCurrentProfileForTest());
            Assert.IsNotNull(result);
            Assert.IsTrue(result.IsSuccess);
        }

        [TestMethod]
        public void Test_Get_Random_Points()
        {
            var service = ServiceFactory.GetService<ICouponCodeService>();
            var p1 = service.GetRandomPoint();
            var p2 = service.GetRandomPoint();
            var p3 = service.GetRandomPoint();
            var p4 = service.GetRandomPoint();
            var p5 = service.GetRandomPoint();
            var p6 = service.GetRandomPoint();

            Console.WriteLine("{0} - {1} - {2} - {3} - {4} - {5}", p1, p2, p3, p4, p5, p6);
            Assert.IsTrue(p1 != p2);
        }
    }
}

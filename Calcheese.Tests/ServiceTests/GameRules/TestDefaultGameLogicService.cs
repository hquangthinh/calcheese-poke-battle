﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Calcheese.Tests.WindsorInstaller;
using CalcheesePokeBattle.Service;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Calcheese.Tests.ServiceTests.GameRules
{
    [TestClass]
    public class TestDefaultGameLogicService : IocTestFixture
    {
        //f22bafc5-4374-4341-8ef3-f43cc1d38ff tom 1602
        private const string PlayerUserId1 = "f22bafc5-4374-4341-8ef3-f43cc1d38ffc";
        private const string PlayerUserName1 = "tom";

        //b7b06a10-f145-4c78-8c11-be4fb7eef244 player3 139
        private const string PlayerUserId2 = "b7b06a10-f145-4c78-8c11-be4fb7eef244";
        private const string PlayerUserName2 = "player3";

        [TestMethod]
        public void Test_Submit_Pokemom_Card_Get_Result_Player1_Water_Player2_Grass_P2_Win_P1_On_Left_Side()
        {
            var playerCardId1 = 12; // water - 1p
            var playerCardId2 = 33; // grass - 2p

            // session with player 1 left (send req) player 2 right (confirm req)
            var gameSessionInfo = new GameSessionInfo
            {
                Id = 1,

                PlayerUserId1 = PlayerUserId1,
                PlayerUserName1 = PlayerUserName1,

                PlayerUserId2 = PlayerUserId2,
                PlayerUserName2 = PlayerUserName2
            };

            var gameSessionManager = ServiceFactory.GetService<IGameSessionService>();
            gameSessionManager.StartNewGameSession(gameSessionInfo);

            var gameLogicService = ServiceFactory.GetService<IPokeCardGameLogicService>();

            // left user submits card
            var leftCardResult = gameLogicService.SubmitPokemonCardForTest(new SubmitPokemonCardCommand
            {
                GameSessionId = gameSessionInfo.Id,
                PlayerUserId = PlayerUserId1,
                PlayerUserName = PlayerUserName1,
                PlayerCardId = playerCardId1
            }).Value;

            var player1GameSession = gameSessionManager.GameSessionForUser(PlayerUserId1).Value;
            var player2GameSession = gameSessionManager.GameSessionForUser(PlayerUserId2).Value;
            Assert.AreEqual(player1GameSession.Id, player2GameSession.Id);

            // right user submits card
            var finalRes = gameLogicService.SubmitPokemonCardForTest(new SubmitPokemonCardCommand
            {
                GameSessionId = gameSessionInfo.Id,
                PlayerUserId = PlayerUserId2,
                PlayerUserName = PlayerUserName2,
                PlayerCardId = playerCardId2
            }).Value;

            Assert.IsNotNull(finalRes);
            Assert.IsTrue(finalRes.HasWinner);

            Assert.AreEqual(PlayerUserId2, finalRes.WonUserId);
            Assert.AreEqual(PlayerUserName2, finalRes.WonUserName);

            Assert.AreEqual(PlayerUserId1, finalRes.LostUserId);
            Assert.AreEqual(PlayerUserName1, finalRes.LostUserName);

            Assert.AreEqual(1, finalRes.LostPoints);
            Assert.AreEqual(playerCardId1, finalRes.LostCardId);
        }

        [TestMethod]
        public void Test_Submit_Pokemom_Card_Get_Result_Player1_Water_Player2_Grass_P2_Win_P1_On_Left_Side_P2_Submit_First()
        {
            var playerCardId1 = 12; // water - 1p
            var playerCardId2 = 33; // grass - 2p

            // session with player 1 left (send req) player 2 right (confirm req)
            var gameSessionInfo = new GameSessionInfo
            {
                Id = 1,

                PlayerUserId1 = PlayerUserId1,
                PlayerUserName1 = PlayerUserName1,

                PlayerUserId2 = PlayerUserId2,
                PlayerUserName2 = PlayerUserName2
            };

            var gameSessionManager = ServiceFactory.GetService<IGameSessionService>();
            gameSessionManager.StartNewGameSession(gameSessionInfo);

            var gameLogicService = ServiceFactory.GetService<IPokeCardGameLogicService>();

            // left user submits card
            var leftCardResult = gameLogicService.SubmitPokemonCardForTest(new SubmitPokemonCardCommand
            {
                GameSessionId = gameSessionInfo.Id,
                PlayerUserId = PlayerUserId2,
                PlayerUserName = PlayerUserName2,
                PlayerCardId = playerCardId2
            }).Value;

            var player1GameSession = gameSessionManager.GameSessionForUser(PlayerUserId1).Value;
            var player2GameSession = gameSessionManager.GameSessionForUser(PlayerUserId2).Value;
            Assert.AreEqual(player1GameSession.Id, player2GameSession.Id);

            // right user submits card
            var finalRes = gameLogicService.SubmitPokemonCardForTest(new SubmitPokemonCardCommand
            {
                GameSessionId = gameSessionInfo.Id,
                PlayerUserId = PlayerUserId1,
                PlayerUserName = PlayerUserName1,
                PlayerCardId = playerCardId1
            }).Value;

            Assert.IsNotNull(finalRes);
            Assert.IsTrue(finalRes.HasWinner);

            Assert.AreEqual(PlayerUserId2, finalRes.WonUserId);
            Assert.AreEqual(PlayerUserName2, finalRes.WonUserName);

            Assert.AreEqual(PlayerUserId1, finalRes.LostUserId);
            Assert.AreEqual(PlayerUserName1, finalRes.LostUserName);

            Assert.AreEqual(1, finalRes.LostPoints);
            Assert.AreEqual(playerCardId1, finalRes.LostCardId);
        }

        [TestMethod]
        public void Test_Submit_Pokemom_Card_Get_Result_Player1_Water_Player2_Grass_P2_Win_Player1_On_Right_Side()
        {
            var playerCardId1 = 12; // water - 1p
            var playerCardId2 = 33; // grass - 2p

            // session with player 1 left (send req) player 2 right (confirm req)
            var gameSessionInfo = new GameSessionInfo
            {
                Id = 1,

                PlayerUserId1 = PlayerUserId2, // this order is important
                PlayerUserName1 = PlayerUserName2,

                PlayerUserId2 = PlayerUserId1,
                PlayerUserName2 = PlayerUserName1
            };

            var gameSessionManager = ServiceFactory.GetService<IGameSessionService>();
            gameSessionManager.StartNewGameSession(gameSessionInfo);

            var gameLogicService = ServiceFactory.GetService<IPokeCardGameLogicService>();

            // left user submits card
            var leftCardResult = gameLogicService.SubmitPokemonCardForTest(new SubmitPokemonCardCommand
            {
                GameSessionId = gameSessionInfo.Id,
                PlayerUserId = PlayerUserId1,
                PlayerUserName = PlayerUserName1,
                PlayerCardId = playerCardId1
            }).Value;

            var player1GameSession = gameSessionManager.GameSessionForUser(PlayerUserId1).Value;
            var player2GameSession = gameSessionManager.GameSessionForUser(PlayerUserId2).Value;
            Assert.AreEqual(player1GameSession.Id, player2GameSession.Id);

            // right user submits card
            var finalRes = gameLogicService.SubmitPokemonCardForTest(new SubmitPokemonCardCommand
            {
                GameSessionId = gameSessionInfo.Id,
                PlayerUserId = PlayerUserId2,
                PlayerUserName = PlayerUserName2,
                PlayerCardId = playerCardId2
            }).Value;

            Assert.IsNotNull(finalRes);
            Assert.IsTrue(finalRes.HasWinner);

            Assert.AreEqual(PlayerUserId2, finalRes.WonUserId);
            Assert.AreEqual(PlayerUserName2, finalRes.WonUserName);

            Assert.AreEqual(PlayerUserId1, finalRes.LostUserId);
            Assert.AreEqual(PlayerUserName1, finalRes.LostUserName);

            Assert.AreEqual(1, finalRes.LostPoints);
            Assert.AreEqual(playerCardId1, finalRes.LostCardId);
        }

        [TestMethod]
        public void Test_Submit_Pokemom_Card_Get_Result_Player1_Water_Player2_Grass_P2_Win_Player1_On_Right_Side_P2_Submit_First()
        {
            var playerCardId1 = 12; // water - 1p
            var playerCardId2 = 33; // grass - 2p

            // session with player 1 left (send req) player 2 right (confirm req)
            var gameSessionInfo = new GameSessionInfo
            {
                Id = 1,

                PlayerUserId1 = PlayerUserId2, // this order is important
                PlayerUserName1 = PlayerUserName2,

                PlayerUserId2 = PlayerUserId1,
                PlayerUserName2 = PlayerUserName1
            };

            var gameSessionManager = ServiceFactory.GetService<IGameSessionService>();
            gameSessionManager.StartNewGameSession(gameSessionInfo);

            var gameLogicService = ServiceFactory.GetService<IPokeCardGameLogicService>();

            // left user submits card
            var leftCardResult = gameLogicService.SubmitPokemonCardForTest(new SubmitPokemonCardCommand
            {
                GameSessionId = gameSessionInfo.Id,
                PlayerUserId = PlayerUserId2,
                PlayerUserName = PlayerUserName2,
                PlayerCardId = playerCardId2
            }).Value;

            var player1GameSession = gameSessionManager.GameSessionForUser(PlayerUserId1).Value;
            var player2GameSession = gameSessionManager.GameSessionForUser(PlayerUserId2).Value;
            Assert.AreEqual(player1GameSession.Id, player2GameSession.Id);

            // right user submits card
            var finalRes = gameLogicService.SubmitPokemonCardForTest(new SubmitPokemonCardCommand
            {
                GameSessionId = gameSessionInfo.Id,
                PlayerUserId = PlayerUserId1,
                PlayerUserName = PlayerUserName1,
                PlayerCardId = playerCardId1
            }).Value;

            Assert.IsNotNull(finalRes);
            Assert.IsTrue(finalRes.HasWinner);

            Assert.AreEqual(PlayerUserId2, finalRes.WonUserId);
            Assert.AreEqual(PlayerUserName2, finalRes.WonUserName);

            Assert.AreEqual(PlayerUserId1, finalRes.LostUserId);
            Assert.AreEqual(PlayerUserName1, finalRes.LostUserName);

            Assert.AreEqual(1, finalRes.LostPoints);
            Assert.AreEqual(playerCardId1, finalRes.LostCardId);
        }
    }
}

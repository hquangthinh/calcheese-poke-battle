﻿using Calcheese.Tests.WindsorInstaller;
using CalcheesePokeBattle.Service;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Calcheese.Tests.ServiceTests.GameRules
{
    [TestClass]
    public class TestDefaultGameRuleService : IocTestFixture
    {
        private const string CurrentUserId = "b7b06a10-f145-4c78-8c11-be4fb7eef244";

        private const string PlayerUserId1 = "xxx";
        private const string PlayerUserName1 = "player 1";

        private const string PlayerUserId2 = "yyy";
        private const string PlayerUserName2 = "player 2";

        [TestMethod]
        public void Test_CalculateGameResult_Player1_Water_Player2_Grass()
        {
            var gameSessionInfo = new GameSessionInfo
            {
                Id = 1,
                PlayerUserId1 = PlayerUserId1,
                PlayerUserName1 = PlayerUserName1,
                PlayerCard1 = 12, // Water
                PlayerUserId2 = PlayerUserId2,
                PlayerUserName2 = PlayerUserName2,
                PlayerCard2 = 32 // Grass
            };
            var gameResult =
                ServiceFactory.GetService<IGameRuleService>().CalculateGameResult(gameSessionInfo, CurrentUserId).Value;
            Assert.IsNotNull(gameResult);
            Assert.IsTrue(gameResult.HasWinner);
            Assert.AreEqual(PlayerUserId2, gameResult.WonUserId);
            Assert.AreEqual(PlayerUserName2, gameResult.WonUserName);
        }

        [TestMethod]
        public void Test_CalculateGameResult_Player1_Water_Player2_Fire()
        {
            var gameSessionInfo = new GameSessionInfo
            {
                Id = 1,
                PlayerUserId1 = PlayerUserId1,
                PlayerUserName1 = PlayerUserName1,
                PlayerCard1 = 12, // Water
                PlayerUserId2 = PlayerUserId2,
                PlayerUserName2 = PlayerUserName2,
                PlayerCard2 = 21 // Fire
            };
            var gameResult =
                ServiceFactory.GetService<IGameRuleService>().CalculateGameResult(gameSessionInfo, CurrentUserId).Value;
            Assert.IsNotNull(gameResult);
            Assert.IsTrue(gameResult.HasWinner);

            Assert.AreEqual(PlayerUserId1, gameResult.WonUserId);
            Assert.AreEqual(PlayerUserName1, gameResult.WonUserName);
            //Assert.AreEqual(PlayerUserName1, gameResult.WonPoints);

            Assert.AreEqual(PlayerUserId2, gameResult.LostUserId);
            Assert.AreEqual(PlayerUserName2, gameResult.LostUserName);
        }

        [TestMethod]
        public void Test_CalculateGameResult_Player1_Water_Player2_Dark()
        {
            var gameSessionInfo = new GameSessionInfo
            {
                Id = 1,
                PlayerUserId1 = PlayerUserId1,
                PlayerUserName1 = PlayerUserName1,
                PlayerCard1 = 12, // Water
                PlayerUserId2 = PlayerUserId2,
                PlayerUserName2 = PlayerUserName2,
                PlayerCard2 = 42 // Dark
            };
            var gameResult =
                ServiceFactory.GetService<IGameRuleService>().CalculateGameResult(gameSessionInfo, CurrentUserId).Value;
            Assert.IsNotNull(gameResult);
            Assert.IsTrue(gameResult.HasWinner);

            Assert.AreEqual(PlayerUserId2, gameResult.WonUserId);
            Assert.AreEqual(PlayerUserName2, gameResult.WonUserName);

            Assert.AreEqual(PlayerUserId1, gameResult.LostUserId);
            Assert.AreEqual(PlayerUserName1, gameResult.LostUserName);
        }

        [TestMethod]
        public void Test_CalculateGameResult_Player1_Fairy_Player2_Fire()
        {
            var gameSessionInfo = new GameSessionInfo
            {
                Id = 1,
                PlayerUserId1 = PlayerUserId1,
                PlayerUserName1 = PlayerUserName1,
                PlayerCard1 = 41, // Fairy
                PlayerUserId2 = PlayerUserId2,
                PlayerUserName2 = PlayerUserName2,
                PlayerCard2 = 23 // Fire,Fly
            };
            var gameResult =
                ServiceFactory.GetService<IGameRuleService>().CalculateGameResult(gameSessionInfo, CurrentUserId).Value;
            Assert.IsNotNull(gameResult);
            Assert.IsTrue(gameResult.HasWinner);

            Assert.AreEqual(PlayerUserId1, gameResult.WonUserId);
            Assert.AreEqual(PlayerUserName1, gameResult.WonUserName);

            Assert.AreEqual(PlayerUserId2, gameResult.LostUserId);
            Assert.AreEqual(PlayerUserName2, gameResult.LostUserName);
        }

        [TestMethod]
        public void Test_CalculateGameResult_Player1_Dark_Player2_Fairy()
        {
            var gameSessionInfo = new GameSessionInfo
            {
                Id = 1,
                PlayerUserId1 = PlayerUserId1,
                PlayerUserName1 = PlayerUserName1,
                PlayerCard1 = 42, // Dark
                PlayerUserId2 = PlayerUserId2,
                PlayerUserName2 = PlayerUserName2,
                PlayerCard2 = 41 // Fairy
            };
            var gameResult =
                ServiceFactory.GetService<IGameRuleService>().CalculateGameResult(gameSessionInfo, CurrentUserId).Value;
            Assert.IsNotNull(gameResult);
            Assert.IsTrue(gameResult.HasWinner);

            Assert.AreEqual(PlayerUserId1, gameResult.WonUserId);
            Assert.AreEqual(PlayerUserName1, gameResult.WonUserName);

            Assert.AreEqual(PlayerUserId2, gameResult.LostUserId);
            Assert.AreEqual(PlayerUserName2, gameResult.LostUserName);
        }

        [TestMethod]
        public void Test_CalculateGameResult_Player1_Fairy_Player2_Dark()
        {
            var gameSessionInfo = new GameSessionInfo
            {
                Id = 1,
                PlayerUserId1 = PlayerUserId1,
                PlayerUserName1 = PlayerUserName1,
                PlayerCard1 = 41, // Fairy
                PlayerUserId2 = PlayerUserId2,
                PlayerUserName2 = PlayerUserName2,
                PlayerCard2 = 42 // Dark
            };
            var gameResult =
                ServiceFactory.GetService<IGameRuleService>().CalculateGameResult(gameSessionInfo, CurrentUserId).Value;
            Assert.IsNotNull(gameResult);
            Assert.IsTrue(gameResult.HasWinner);

            Assert.AreEqual(PlayerUserId2, gameResult.WonUserId);
            Assert.AreEqual(PlayerUserName2, gameResult.WonUserName);

            Assert.AreEqual(PlayerUserId1, gameResult.LostUserId);
            Assert.AreEqual(PlayerUserName1, gameResult.LostUserName);
        }

        [TestMethod]
        public void Test_CalculateGameResult_Player1_Grass_Player2_Fire()
        {

        }

        [TestMethod]
        public void Test_CalculateGameResult_Player1_All_Element_vs_Player2_Dark()
        {
            // 1 vs 42
            var gameSessionInfo = new GameSessionInfo
            {
                Id = 1,
                PlayerUserId1 = PlayerUserId1,
                PlayerUserName1 = PlayerUserName1,
                PlayerCard1 = 1, // Electric
                PlayerUserId2 = PlayerUserId2,
                PlayerUserName2 = PlayerUserName2,
                PlayerCard2 = 42 // Dark
            };

            var gameResult =
                ServiceFactory.GetService<IGameRuleService>().CalculateGameResult(gameSessionInfo, CurrentUserId).Value;
            Assert.IsNotNull(gameResult);
            Assert.IsTrue(gameResult.HasWinner);

            Assert.AreEqual(PlayerUserId2, gameResult.WonUserId);
            Assert.AreEqual(PlayerUserName2, gameResult.WonUserName);

            Assert.AreEqual(PlayerUserId1, gameResult.LostUserId);
            Assert.AreEqual(PlayerUserName1, gameResult.LostUserName);

            // 8 vs 42
            gameSessionInfo = new GameSessionInfo
            {
                Id = 1,
                PlayerUserId1 = PlayerUserId1,
                PlayerUserName1 = PlayerUserName1,
                PlayerCard1 = 8, // Electric,Dragon
                PlayerUserId2 = PlayerUserId2,
                PlayerUserName2 = PlayerUserName2,
                PlayerCard2 = 42 // Dark
            };

            gameResult =
                ServiceFactory.GetService<IGameRuleService>().CalculateGameResult(gameSessionInfo, CurrentUserId).Value;
            Assert.IsNotNull(gameResult);
            Assert.IsTrue(gameResult.HasWinner);

            Assert.AreEqual(PlayerUserId2, gameResult.WonUserId);
            Assert.AreEqual(PlayerUserName2, gameResult.WonUserName);

            Assert.AreEqual(PlayerUserId1, gameResult.LostUserId);
            Assert.AreEqual(PlayerUserName1, gameResult.LostUserName);

            // 8 vs 41
            gameSessionInfo = new GameSessionInfo
            {
                Id = 1,
                PlayerUserId1 = PlayerUserId1,
                PlayerUserName1 = PlayerUserName1,
                PlayerCard1 = 8, // Electric,Dragon
                PlayerUserId2 = PlayerUserId2,
                PlayerUserName2 = PlayerUserName2,
                PlayerCard2 = 41 // Fairy
            };

            gameResult =
                ServiceFactory.GetService<IGameRuleService>().CalculateGameResult(gameSessionInfo, CurrentUserId).Value;
            Assert.IsNotNull(gameResult);
            Assert.IsTrue(gameResult.HasWinner);

            Assert.AreEqual(PlayerUserId2, gameResult.WonUserId);
            Assert.AreEqual(PlayerUserName2, gameResult.WonUserName);

            Assert.AreEqual(PlayerUserId1, gameResult.LostUserId);
            Assert.AreEqual(PlayerUserName1, gameResult.LostUserName);

            // 41 vs 42
            gameSessionInfo = new GameSessionInfo
            {
                Id = 1,
                PlayerUserId1 = PlayerUserId1,
                PlayerUserName1 = PlayerUserName1,
                PlayerCard1 = 41, // Fairy
                PlayerUserId2 = PlayerUserId2,
                PlayerUserName2 = PlayerUserName2,
                PlayerCard2 = 42 // Dark
            };

            gameResult =
                ServiceFactory.GetService<IGameRuleService>().CalculateGameResult(gameSessionInfo, CurrentUserId).Value;
            Assert.IsNotNull(gameResult);
            Assert.IsTrue(gameResult.HasWinner);

            Assert.AreEqual(PlayerUserId2, gameResult.WonUserId);
            Assert.AreEqual(PlayerUserName2, gameResult.WonUserName);

            Assert.AreEqual(PlayerUserId1, gameResult.LostUserId);
            Assert.AreEqual(PlayerUserName1, gameResult.LostUserName);
        }

        [TestMethod]
        public void Test_CalculateGameResult_Player1_Electric_Player2_Grass()
        {
            var gameSessionInfo = new GameSessionInfo
            {
                Id = 1,
                PlayerUserId1 = PlayerUserId1,
                PlayerUserName1 = PlayerUserName1,
                PlayerCard1 = 1, // Electric
                PlayerUserId2 = PlayerUserId2,
                PlayerUserName2 = PlayerUserName2,
                PlayerCard2 = 32 // Grass
            };
            var gameResult =
                ServiceFactory.GetService<IGameRuleService>().CalculateGameResult(gameSessionInfo, CurrentUserId).Value;
            Assert.IsNotNull(gameResult);
            Assert.IsTrue(gameResult.HasWinner);

            Assert.AreEqual(PlayerUserId2, gameResult.WonUserId);
            Assert.AreEqual(PlayerUserName2, gameResult.WonUserName);

            Assert.AreEqual(PlayerUserId1, gameResult.LostUserId);
            Assert.AreEqual(PlayerUserName1, gameResult.LostUserName);
        }

        [TestMethod]
        public void Test_CalculateGameResult_Player1_Electric_Dragon_Player2_Grass()
        {
            var gameSessionInfo = new GameSessionInfo
            {
                Id = 1,
                PlayerUserId1 = PlayerUserId1,
                PlayerUserName1 = PlayerUserName1,
                PlayerCard1 = 8, // Electric,Dragon
                PlayerUserId2 = PlayerUserId2,
                PlayerUserName2 = PlayerUserName2,
                PlayerCard2 = 32 // Grass
            };
            var gameResult =
                ServiceFactory.GetService<IGameRuleService>().CalculateGameResult(gameSessionInfo, CurrentUserId).Value;
            Assert.IsNotNull(gameResult);
            Assert.IsTrue(gameResult.HasWinner);

            Assert.AreEqual(PlayerUserId1, gameResult.WonUserId);
            Assert.AreEqual(PlayerUserName1, gameResult.WonUserName);

            Assert.AreEqual(PlayerUserId2, gameResult.LostUserId);
            Assert.AreEqual(PlayerUserName2, gameResult.LostUserName);
        }

        [TestMethod]
        public void Test_CalculateGameResult_Player1_Special_Player2_Normal()
        {
            // car 17 vs 32
            var gameSessionInfo = new GameSessionInfo
            {
                Id = 1,
                PlayerUserId1 = PlayerUserId1,
                PlayerUserName1 = PlayerUserName1,
                PlayerCard1 = 17, // Water
                PlayerUserId2 = PlayerUserId2,
                PlayerUserName2 = PlayerUserName2,
                PlayerCard2 = 32 // Grass
            };

            var gameResult =
                ServiceFactory.GetService<IGameRuleService>().CalculateGameResult(gameSessionInfo, CurrentUserId).Value;
            Assert.IsNotNull(gameResult);
            Assert.IsTrue(gameResult.HasWinner);

            Assert.AreEqual(PlayerUserId1, gameResult.WonUserId);
            Assert.AreEqual(PlayerUserName1, gameResult.WonUserName);

            Assert.AreEqual(PlayerUserId2, gameResult.LostUserId);
            Assert.AreEqual(PlayerUserName2, gameResult.LostUserName);

            // car 27 vs 32
            gameSessionInfo = new GameSessionInfo
            {
                Id = 1,
                PlayerUserId1 = PlayerUserId1,
                PlayerUserName1 = PlayerUserName1,
                PlayerCard1 = 27, // Water
                PlayerUserId2 = PlayerUserId2,
                PlayerUserName2 = PlayerUserName2,
                PlayerCard2 = 32 // Grass
            };

            gameResult =
                ServiceFactory.GetService<IGameRuleService>().CalculateGameResult(gameSessionInfo, CurrentUserId).Value;
            Assert.IsNotNull(gameResult);
            Assert.IsTrue(gameResult.HasWinner);

            Assert.AreEqual(PlayerUserId1, gameResult.WonUserId);
            Assert.AreEqual(PlayerUserName1, gameResult.WonUserName);

            Assert.AreEqual(PlayerUserId2, gameResult.LostUserId);
            Assert.AreEqual(PlayerUserName2, gameResult.LostUserName);

            // car 28 vs 32
            gameSessionInfo = new GameSessionInfo
            {
                Id = 1,
                PlayerUserId1 = PlayerUserId1,
                PlayerUserName1 = PlayerUserName1,
                PlayerCard1 = 28, // Water
                PlayerUserId2 = PlayerUserId2,
                PlayerUserName2 = PlayerUserName2,
                PlayerCard2 = 32 // Grass
            };

            gameResult =
                ServiceFactory.GetService<IGameRuleService>().CalculateGameResult(gameSessionInfo, CurrentUserId).Value;
            Assert.IsNotNull(gameResult);
            Assert.IsTrue(gameResult.HasWinner);

            Assert.AreEqual(PlayerUserId1, gameResult.WonUserId);
            Assert.AreEqual(PlayerUserName1, gameResult.WonUserName);

            Assert.AreEqual(PlayerUserId2, gameResult.LostUserId);
            Assert.AreEqual(PlayerUserName2, gameResult.LostUserName);

            // car 40 vs 32
            gameSessionInfo = new GameSessionInfo
            {
                Id = 1,
                PlayerUserId1 = PlayerUserId1,
                PlayerUserName1 = PlayerUserName1,
                PlayerCard1 = 40, // Water
                PlayerUserId2 = PlayerUserId2,
                PlayerUserName2 = PlayerUserName2,
                PlayerCard2 = 32 // Grass
            };

            gameResult =
                ServiceFactory.GetService<IGameRuleService>().CalculateGameResult(gameSessionInfo, CurrentUserId).Value;
            Assert.IsNotNull(gameResult);
            Assert.IsTrue(gameResult.HasWinner);

            Assert.AreEqual(PlayerUserId1, gameResult.WonUserId);
            Assert.AreEqual(PlayerUserName1, gameResult.WonUserName);

            Assert.AreEqual(PlayerUserId2, gameResult.LostUserId);
            Assert.AreEqual(PlayerUserName2, gameResult.LostUserName);

            // car 32 vs 40
            gameSessionInfo = new GameSessionInfo
            {
                Id = 1,
                PlayerUserId1 = PlayerUserId1,
                PlayerUserName1 = PlayerUserName1,
                PlayerCard1 = 32, // Water
                PlayerUserId2 = PlayerUserId2,
                PlayerUserName2 = PlayerUserName2,
                PlayerCard2 = 40 // Grass,Poison-Mega Fushigibana
            };
            gameResult =
                ServiceFactory.GetService<IGameRuleService>().CalculateGameResult(gameSessionInfo, CurrentUserId).Value;

            Assert.IsNotNull(gameResult);
            Assert.IsTrue(gameResult.HasWinner);

            Assert.AreEqual(PlayerUserId2, gameResult.WonUserId);
            Assert.AreEqual(PlayerUserName2, gameResult.WonUserName);

            Assert.AreEqual(PlayerUserId1, gameResult.LostUserId);
            Assert.AreEqual(PlayerUserName1, gameResult.LostUserName);
        }
    }
}

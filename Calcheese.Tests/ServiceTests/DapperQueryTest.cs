﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Calcheese.Tests.ServiceTests
{
    [TestClass]
    public class DapperQueryTest
    {
        [TestMethod]
        public void Test_Query_Count()
        {
            var connString = ConfigurationManager.ConnectionStrings["CalcheeseAppDbContext"].ConnectionString;
            using (var db = new SqlConnection(connString))
            {
                db.Open();
                // check if there's ongoing import job
                var count = db.QueryFirst<int>("SELECT COUNT(*) FROM PokeCard");
                Assert.AreEqual(42, count);
            }
        }

        [TestMethod]
        public void Test_Job_Operation()
        {
            var connString = ConfigurationManager.ConnectionStrings["CalcheeseAppDbContext"].ConnectionString;
            using (var db = new SqlConnection(connString))
            {
                db.Open();
                // delete job
                db.Execute("DELETE ImportJobStatus");

                // check if there's ongoing import job
                var count = db.QueryFirst<int>("SELECT COUNT(*) FROM ImportJobStatus");
                Assert.AreEqual(0, count);

                // add job
                var jobName = Guid.NewGuid();
                db.Execute($"INSERT INTO ImportJobStatus(JobName, CreatedDate) VALUES('{jobName}', GETUTCDATE())");

                var countAgain = db.QueryFirst<int>("SELECT COUNT(*) FROM ImportJobStatus");
                Assert.AreEqual(1, countAgain);
            }
        }
    }
}

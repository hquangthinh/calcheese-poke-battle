﻿using Calcheese.Tests.WindsorInstaller;
using CalcheesePokeBattle.DTO;
using CalcheesePokeBattle.Service;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Calcheese.Tests.ServiceTests
{
    [TestClass]
    public class TestDefaultPokeCardService : IocTestFixture
    {
        private CardInfo GetCardInfo(int cardId)
        {
            var cardService = ServiceFactory.GetService<IPokeCardService>();
            return cardService.GetCardInfo(cardId).Value;
        }

        [TestMethod]
        public void TestGetCardInfo_Card_1()
        {
            var cardInfo = GetCardInfo(1);
            Assert.IsNotNull(cardInfo);
            Assert.AreEqual(1, cardInfo.CardElements.Count);
            Assert.AreEqual("Electric", cardInfo.CardMainElement);
            Assert.AreEqual(PokemonCardType.Normal, cardInfo.CardType);
        }

        [TestMethod]
        public void TestGetCardInfo_Card_2()
        {
            var cardInfo = GetCardInfo(2);
            Assert.IsNotNull(cardInfo);
            Assert.AreEqual(2, cardInfo.CardElements.Count);
            Assert.AreEqual("Electric", cardInfo.CardMainElement);
            Assert.AreEqual(PokemonCardElement.Electric, cardInfo.CardElements[0]);
            Assert.AreEqual(PokemonCardElement.Fairy, cardInfo.CardElements[1]);
            Assert.AreEqual(PokemonCardType.Normal, cardInfo.CardType);
        }
    }
}

﻿using System;
using CalcheesePokeBattle.Service;
using Castle.MicroKernel.Resolvers.SpecializedResolvers;
using Castle.Windsor;

namespace Calcheese.Tests.WindsorInstaller
{
    public class IocTestFixture : IDisposable
    {
        public IWindsorContainer Container { get; }
        public IServiceFactory ServiceFactory { get; }

        public IocTestFixture()
        {
            Container = new WindsorContainer();
            Container.Kernel.Resolver.AddSubResolver(new CollectionResolver(Container.Kernel));
            Container.Install(new UnitTestServiceInstaller(),
                new UnitTestLoggerInstaller());
            ServiceFactory = Container.Resolve<IServiceFactory>();
        }

        public void Dispose()
        {
            Container.Dispose();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CalcheesePokeBattle.Service;
using Castle.Windsor;

namespace Calcheese.Tests.WindsorInstaller
{
    public class UnitTestWindsorServiceFactory : DefaultWindsorServiceFactory
    {
        public UnitTestWindsorServiceFactory(IWindsorContainer container) : base(container)
        {
        }
    }
}

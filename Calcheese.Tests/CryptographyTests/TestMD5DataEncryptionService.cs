﻿using System;
using System.Collections.Generic;
using System.Text;
using Calcheese.Tests.WindsorInstaller;
using CalcheesePokeBattle.Common.Cryptography;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Calcheese.Tests.CryptographyTests
{
    [TestClass]
    public class TestMD5DataEncryptionService : IocTestFixture
    {
        [TestMethod]
        public void TestGetHashString()
        {
            var md5Hasher = ServiceFactory.GetService<IOneWayEncryptionService>();
            var output = md5Hasher.GetHashString("XREQGQCJ9");
            Assert.IsNotNull(output);
            Console.WriteLine(output);
        }

        [TestMethod]
        public void TestVerifyHash()
        {
            var hashString = "9d6bdf1f587c03f3280d62789df83a52";
            var md5Hasher = ServiceFactory.GetService<IOneWayEncryptionService>();
            var output = md5Hasher.VerifyHash("A0000001", hashString);
            Assert.IsTrue(output);

            var outputNotMatch = md5Hasher.VerifyHash("A0000002", hashString);
            Assert.IsFalse(outputNotMatch);
        }

        [TestMethod]
        public void GenerateHashString()
        {
            var codeList = new List<string>
            {
                "	00008084	",
                "	00008083	",
                "	00006783	",
                "	00008289	",
                "	00008189	",
                "	00008205	",
                "	00008065	",
                "	00002565	",
                "	00003565	",
                "	00003564	",
                "	00008279	",
                "	00007679	",
                "	00002678	",
                "	00007678	",
                "	00005674	",
                "	00008110	",
                "	00001109	",
                "	00002109	",
                "	00005109	",
                "	00006109	",
                "	00008109	",
                "	00008209	",
                "	00008206	",
                "	00008245	",
                "	00006185	",
                "	00006785	",
                "	00006784	"
            };
            var md5Hasher = ServiceFactory.GetService<IOneWayEncryptionService>();
            var result = new StringBuilder();
            foreach (var rawCode in codeList)
            {
                var code = md5Hasher.GetHashString(rawCode.Trim());
                result.Append($"UPDATE CouponCode Set Available=1 WHERE Code = '{code}'").AppendLine();
            }
            Console.WriteLine(result.ToString());
        }
    }
}

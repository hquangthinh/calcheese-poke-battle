﻿using System;
using CalcheesePokeBattle.Common.Cryptography;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Calcheese.Tests.CryptographyTests
{
    [TestClass]
    public class DefaultAesDataEncryptionServiceTest
    {
        [TestMethod]
        public void Test_EncryptString_String_Length_100()
        {
            var input = "ABCDEFGHIJKLMNOPQRSTUVXYZabcdefghijklmnopqrstuvxyz12345678901234567890123456789012345678901234567890";
            var encryptedText = new DefaultAesDataEncryptionService().EncryptString(input);
            Assert.IsTrue(!string.IsNullOrEmpty(encryptedText));
            Assert.IsTrue(encryptedText.Length < 450);
            Console.WriteLine(encryptedText);
        }

        [TestMethod]
        public void TestDecrypt()
        {
            var input = "ABCDEFGHIJKLMNOPQRSTUVXYZabcdefghijklmnopqrstuvxyz12345678901234567890123456789012345678901234567890";
            var encryptionService = new DefaultAesDataEncryptionService();
            var encryptedText = encryptionService.EncryptString(input);
            Assert.IsTrue(!string.IsNullOrEmpty(encryptedText));
            var decryptedText = encryptionService.DecryptString(encryptedText);
            Assert.AreEqual(input, decryptedText);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using Dapper;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CalcheesePokeBattle.Model;

namespace Calcheese.Tests.TestDataGen
{
    [TestClass]
    public class GenerateTestData
    {
        [TestMethod]
        public void Generate_Coupon_Code_Files_For_Import_Testing()
        {
            var folderPath = @"F:\proj\calcheese\test-data";

            var connString = ConfigurationManager.ConnectionStrings["CalcheeseAppDbContext"].ConnectionString;
            var cardNameList = new List<string>();
            using (var db = new SqlConnection(connString))
            {
                db.Open();
                cardNameList = db.Query<string>("SELECT CardName FROM PokeCard").ToList();
            }

            for (var i = 0; i < 42; i++)
            {
                var codeBuilder = new StringBuilder();

                for (var cj = 1; cj <= 200; cj++)
                {
                    var prefix = cardNameList[i];
                    var rawCode = $"{prefix}/{cj.ToString().PadLeft(4, '0')}";
                    codeBuilder.Append(rawCode).AppendLine();
                }
                var filePath = Path.Combine(folderPath, $"code_{i + 1}_{cardNameList[i]}.txt");
                File.WriteAllText(filePath, codeBuilder.ToString());
            }
        }

        [TestMethod]
        public void Generate_User_Lucky_Draw_Code()
        {
            var weekNumber = 5;
            using (var dbContext = new CalcheeseAppDbContext())
            {
                var lucky = 0;
                for (var i = 3100; i < 3600; i++)
                {
                    var userprofile = new UserProfile
                    {
                        Id = Guid.NewGuid().ToString(),
                        UserName = $"UserName_{i}",
                        FirstName = Faker.Name.First(),
                        LastName = Faker.Name.Last(),
                        Email = $"fakemail@ex.com",
                        PhoneNumber = Faker.Phone.Number().Substring(0, 11),
                        Address = Faker.Address.StreetAddress(true),
                        TotalPoint = 0
                    };

                    for (var j = 0; j < 5; j++)
                    {
                        lucky++;
                        userprofile.UserLuckyDrawCodes.Add(new UserLuckyDrawCode
                        {
                            UserId = userprofile.Id,
                            LuckyDrawCode = lucky.ToString().PadLeft(6, '0'),
                            CreatedDate = DateTime.Now,
                            IsActive = true,
                            WeekNumber = weekNumber
                        });
                    }

                    dbContext.UserProfiles.Add(userprofile);
                    dbContext.SaveChanges();
                }
            }
            
        }
    }
}

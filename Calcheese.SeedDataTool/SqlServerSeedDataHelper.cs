﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using CalcheesePokeBattle.Common.Cryptography;
using CalcheesePokeBattle.Common.Monads;
using CalcheesePokeBattle.Model;
using Dapper;

namespace Calcheese.SeedDataTool
{
    public static class SqlServerSeedDataHelper
    {
        /// <summary>
        /// Seed 8400 codes for 42 cards. Each card 200 codes
        /// </summary>
        public static void RunSeedDataForSqlServer8400Code()
        {
            const int codeNoPerCard = 200;
            var stopWatch = new Stopwatch();
            Console.WriteLine("Seeding data...");

            var dataEncryptionService = new MD5DataEncryptionService();

            using (var db = new CalcheeseAppDbContext())
            {
                var cardPointList = db.PokeCards.Select(c => new {Id = c.Id, Point = c.CardPoint}).ToList();

                for (var i = 0; i < cardPointList.Count; i++)
                {
                    var cardInfo = cardPointList[i];
                    var startCodeNo = codeNoPerCard * i + 1;
                    var endCodeNo = startCodeNo + codeNoPerCard;
                    Console.WriteLine($"Seeding data from {startCodeNo} to {endCodeNo}...");
                    for (var codeNo = startCodeNo; codeNo < endCodeNo; codeNo++)
                    {
                        var rawCode = codeNo.ToString().PadLeft(8, '0');
                        var codeEntity = new CouponCode
                        {
                            Code = dataEncryptionService.GetHashString(rawCode),
                            Available = true,
                            Point = cardInfo.Point,
                            CardId = cardInfo.Id
                        };
                        db.CouponCodes.Add(codeEntity);
                    }
                    db.SaveChanges();
                }
            }

            stopWatch.Stop();
            Console.WriteLine($"All done. Running time in minutes---- {stopWatch.Elapsed.TotalMinutes}");
            Console.ReadLine();
        }

        public static void RunSeedDataForSqlServer(string[] args)
        {
            var totalCode = 100;
            if (args.Length > 0)
            {
                Int32.TryParse(args[0], out totalCode);
            }

            var stopWatch = new Stopwatch();
            stopWatch.Start();

            Console.WriteLine("Seeding data...");

            using (var db = new CalcheeseAppDbContext())
            {
                SeedCodeData(db, totalCode)
                    .OnSuccess(() =>
                    {
                        stopWatch.Stop();
                        Console.WriteLine($"All done. Running time in minutes---- {stopWatch.Elapsed.TotalMinutes}");
                        Console.ReadLine();
                    });
            }
        }

        private static Result SeedCodeData(CalcheeseAppDbContext dbContext, int totalCode)
        {
            var dataEncryptionService = new MD5DataEncryptionService();
            const int batchSize = 100;
            var startIndex = 0;
            var endIndex = Math.Min(batchSize, totalCode);
            while (true)
            {
                Console.WriteLine($"Seeding data from {startIndex} to {endIndex}...");

                for (var i = startIndex; i < endIndex; i++)
                {
                    var codeEntity = CreateRandomCouponCode(i, dataEncryptionService);
                    dbContext.CouponCodes.Add(codeEntity);
                }

                dbContext.SaveChanges();

                if (endIndex >= totalCode)
                    break;

                startIndex = endIndex;
                endIndex = Math.Min(endIndex + batchSize, totalCode);
            }
            return Result.Success();
        }

        private static CouponCode CreateRandomCouponCode(int codeNo, IOneWayEncryptionService dataEncryptionService)
        {
            var rawCode = codeNo.ToString().PadLeft(8, '0');
            return new CouponCode
            {
                Code = dataEncryptionService.GetHashString(rawCode),
                Available = true,
                Point = new Random().Next(1, 7),
                CardId = new Random().Next(1, 42)
            };
        }

        public static void RunSeedDataForSqlServerWithDapper(string[] args)
        {
            var filePath = args[0];
            var stopWatch = new Stopwatch();
            stopWatch.Start();

            Console.WriteLine("Seeding data...");
            File.WriteAllText("log.txt", $"Start seeding data at {DateTime.Now}");

            var md5Hasher = new MD5CryptoServiceProvider();
            var connString = ConfigurationManager.ConnectionStrings["PerformanceTestDb"].ConnectionString;
            const int BATCH_SIZE = 100;
            var batchCount = 1;
            var cardId = 1;
            using (var fileReader = File.OpenText(filePath))
            {
                using (IDbConnection db = new SqlConnection(connString))
                {
                    db.Open();
                    var counter = 0;
                    var sqlBuiler = new StringBuilder();
                    while (!fileReader.EndOfStream)
                    {
                        var rawCode = fileReader.ReadLine();
                        var code = GetHashString(rawCode, md5Hasher);
                        sqlBuiler
                            .AppendFormat(
                                $"INSERT INTO CouponCode(Code, Available, Point, CardId) VALUES(\'{code}\', 1, 0, {cardId})")
                            .AppendLine();
                        if (counter < BATCH_SIZE)
                        {
                            counter++;
                        }
                        else
                        {
                            try
                            {
                                var dbTrans = db.BeginTransaction();
                                db.Execute(sqlBuiler.ToString(), null, dbTrans, 30, CommandType.Text);
                                dbTrans.Commit();
                                Console.WriteLine($"batch commited...{counter * batchCount}");
                            }
                            catch (Exception)
                            {
                                // log sql text error
                            }
                            finally
                            {
                                counter = 0;
                                batchCount++;
                                cardId++;
                                if (cardId > 42)
                                    cardId = 1;
                                sqlBuiler = new StringBuilder();
                            }
                        }
                    }//end of reading file
                    var endBatchSql = sqlBuiler.ToString();
                    if (!string.IsNullOrEmpty(endBatchSql))
                    {
                        try
                        {
                            var dbTrans = db.BeginTransaction();
                            db.Execute(sqlBuiler.ToString(), null, dbTrans, 30, CommandType.Text);
                            dbTrans.Commit();
                            Console.WriteLine("last batch commited...");
                        }
                        catch (Exception)
                        {
                            // log sql text error
                        }
                    }
                }//dispose connection
            }// dispose file reader
            stopWatch.Stop();
            Console.WriteLine($"All done. Running time in minutes---- {stopWatch.Elapsed.TotalMinutes}");
            Console.ReadLine();
        }

        private static string GetHashString(string input, MD5CryptoServiceProvider md5Hasher)
        {
            if (string.IsNullOrEmpty(input))
                throw new ArgumentNullException(nameof(input));

            var hashData = md5Hasher.ComputeHash(Encoding.Default.GetBytes(input));
            var sBuilder = new StringBuilder();
            // Loop through each byte of the hashed data 
            // and format each one as a hexadecimal string.
            foreach (byte b in hashData)
            {
                sBuilder.Append(b.ToString("x2"));
            }
            return sBuilder.ToString();
        }
    }
}

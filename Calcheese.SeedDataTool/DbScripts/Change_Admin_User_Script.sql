﻿-- When change admin user in db CalcheeseDevIdentity the UserId change
-- Update children table in CalcheeseDev db to refer to new UserId

-- First create a copy of admin in user profile table with new UserId then run below script

DECLARE @oldUserId nvarchar(128)
DECLARE @newUserId nvarchar(128)

SET @oldUserId = '29e2ecdf-ac39-4d29-bdc4-e6c834e90497'
SET @newUserId = '68b1ec50-3ca7-4528-a229-2f3642ba5641'


Update [GiftRedeemHistory] Set UserId = @newUserId Where UserId = @oldUserId
 
Update [GiftWonFromLuckyDraw] Set UserId = @newUserId Where UserId = @oldUserId

Update [UserCardCollection] Set UserId = @newUserId Where UserId = @oldUserId

Update [UserInventory] Set UserId = @newUserId Where UserId = @oldUserId

Update [UserLuckyDrawCode] Set UserId = @newUserId Where UserId = @oldUserId

Update [WinnerListUser] Set UserId = @newUserId Where UserId = @oldUserId

Update [UserProfile] Set UserName = 'admin' Where Id = @newUserId

Update [UserProfile] Set UserName = 'admin-old' Where Id = @oldUserId


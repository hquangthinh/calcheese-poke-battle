﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CalcheesePokeBattle.Common.DTO;
using Microsoft.Azure;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.WindowsAzure.Storage.Queue;
using Newtonsoft.Json;

namespace Calcheese.SeedDataTool
{
    public class CouponCodeSeedDataHelper
    {
        private const string dataImportBlobContainerName = "dataforimport";
        private const string importCouponCodeQueueName = "importcouponcoderequest";

        private static CloudQueueClient queueClient;
        private static CloudQueue dataImportQueue;

        private static CloudBlobClient blobClient;
        private static CloudBlobContainer blobContainer;

        public void InitStorage()
        {
            var storageAccount = CloudStorageAccount.Parse(ConfigurationManager.ConnectionStrings["AzureWebJobsStorage"].ConnectionString);
            // Create a blob client for interacting with the blob service.
            blobClient = storageAccount.CreateCloudBlobClient();
            blobContainer = blobClient.GetContainerReference(dataImportBlobContainerName);
            blobContainer.CreateIfNotExists();

            // Create a queue client
            queueClient = storageAccount.CreateCloudQueueClient();
            dataImportQueue = queueClient.GetQueueReference(importCouponCodeQueueName);
            dataImportQueue.CreateIfNotExists();
        }

        /// <summary>
        /// Upload file to the import blob
        /// </summary>
        /// <param name="filePaths"></param>
        /// <returns></returns>
        public void UploadDataForImport(List<string> filePaths)
        {
            foreach (var filePath in filePaths)
            {
                var fi = new FileInfo(filePath);
                var blob = blobContainer.GetBlockBlobReference(fi.Name);
                blob.UploadFromFile(fi.FullName);

                // send message to import queue
                var blobInfo = new BlobInformation
                {
                    BlobUri = new Uri(blob.Uri.ToString())
                };
                var queueMessage = new CloudQueueMessage(JsonConvert.SerializeObject(blobInfo));
                dataImportQueue.AddMessage(queueMessage);
            }
        }

        public void UploadDataForImport(string folderPath)
        {
            var files = Directory.GetFiles(folderPath).ToList();
            UploadDataForImport(files);
        }

        public void DeleteBlob(BlobInformation blobInfo)
        {
            if (blobInfo == null)
                return;

            // Retrieve storage account from connection string.
            var storageAccount = CloudStorageAccount.Parse(ConfigurationManager.ConnectionStrings["AzureWebJobsStorage"].ConnectionString);

            // Create the blob client.
            blobClient = storageAccount.CreateCloudBlobClient();

            // Retrieve reference to a previously created container.
            var container = blobClient.GetContainerReference("dataforimport");

            // Retrieve reference to a blob named "myblob.txt".
            var blockBlob = container.GetBlockBlobReference(blobInfo.BlobName);

            // Delete the blob.
            blockBlob.Delete();
        }
    }
}

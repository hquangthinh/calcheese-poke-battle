﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calcheese.SeedDataTool
{
    public class CodeFileGenerator
    {
        /// <summary>
        /// Generate incremental code write result to file
        /// return running time in minutes
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <returns></returns>
        public double GenerateCodeToFile(string filePath, int from, int to)
        {
            var timer = new Stopwatch();
            timer.Start();
            using (var sw = File.CreateText(filePath))
            {
                for (var i = from; i <= to; i++)
                {
                    var code = i.ToString().PadLeft(8, '0');
                    sw.Write(code);
                    sw.WriteLine();
                }
                sw.Flush();
                sw.Close();
            }
            timer.Stop();
            return timer.Elapsed.TotalMinutes;
        }
    }
}

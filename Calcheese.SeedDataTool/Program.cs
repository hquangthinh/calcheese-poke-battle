﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using CalcheesePokeBattle.Common.Cryptography;
using CalcheesePokeBattle.Common.DTO;
using CalcheesePokeBattle.Common.Monads;
using CalcheesePokeBattle.Model;

namespace Calcheese.SeedDataTool
{
    class Program
    {
        static void Main(string[] args)
        {
            // Uncomment below line to seed data to sql server table
            //SqlServerSeedDataHelper.RunSeedDataForSqlServer8400Code();

            // Seed data to Azure blob
            //var seedHelper = new CouponCodeSeedDataHelper();
            //seedHelper.InitStorage();
            //var folderPath = @"D:\calcheese\CalcheesePokeBattle\Calcheese.SeedDataTool\SeedDataFiles\";
            //seedHelper.UploadDataForImport(folderPath);

            // Delete blob information
            //var seedHelper = new CouponCodeSeedDataHelper();
            //seedHelper.DeleteBlob(new BlobInformation
            //{
            //    BlobUri = new Uri("https://storagecalcheeseprod.blob.core.windows.net/dataforimport/BodyPart_476d30ce-ba2d-44a7-80cf-d55d0ab506ba")
            //});

            // Upload images files to blob storage
            //var imagesPathToUpload = @"D:\calcheese\CalcheesePokeBattle\CalcheesePokeBattle.WebApp\Content\images\pokemon-cards";
            //var result = UploadStaticFilesForWeb.UploadImagesToStorageForWeb(imagesPathToUpload);
            //Console.WriteLine($"All files uploaded. Check log result at {result}");

            // test code generator write to file
            //var codeGen = new CodeFileGenerator();
            //var from = Convert.ToInt32(args[0]);
            //var to = Convert.ToInt32(args[1]);
            //var file = $"code_{from}_{to}.txt";
            //var totalMins = codeGen.GenerateCodeToFile(file, from, to);

            SqlServerSeedDataHelper.RunSeedDataForSqlServerWithDapper(args);

            //Console.WriteLine($"Running time {totalMins} minutues");

            //Console.WriteLine("Operation done...");
            //Console.ReadLine();
        }


    }
}

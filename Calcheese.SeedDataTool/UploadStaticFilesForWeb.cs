﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;

namespace Calcheese.SeedDataTool
{
    public static class UploadStaticFilesForWeb
    {
        private static CloudBlobClient _blobClient;
        private static CloudBlobContainer _blobContainer;

        private static void InitStorage(string containerName)
        {
            var storageAccount = CloudStorageAccount.Parse(ConfigurationManager.ConnectionStrings["AzureWebJobsStorage"].ConnectionString);

            // Create a blob client for interacting with the blob service.
            _blobClient = storageAccount.CreateCloudBlobClient();
            _blobContainer = _blobClient.GetContainerReference(containerName);
            _blobContainer.CreateIfNotExists();
        }

        /// <summary>
        /// Upload image files in a folder path to blob storage
        /// in container images
        /// </summary>
        /// <param name="folderPath"></param>
        /// <returns></returns>
        public static string UploadImagesToStorageForWeb(string folderPath)
        {
            const string containerName = "images";

            InitStorage(containerName);

            var files = Directory.GetFiles(folderPath).ToList();
            var sbUrls = new StringBuilder();

            foreach (var filePath in files)
            {
                var fi = new FileInfo(filePath);
                var blobBlock = _blobContainer.GetBlockBlobReference(fi.Name);
                blobBlock.UploadFromFile(fi.FullName);
                sbUrls.Append(blobBlock.Uri).AppendLine();
            }
            // dump uri list to a result file
            var resultFile = $"upload_result_{DateTime.Now:yyyy-MM-dd}_{DateTime.Now.Ticks}.txt";
            File.WriteAllText(resultFile, sbUrls.ToString());
            return resultFile;
        }
    }
}
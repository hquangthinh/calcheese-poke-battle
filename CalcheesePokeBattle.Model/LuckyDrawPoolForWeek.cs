﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CalcheesePokeBattle.Model
{
    public interface ILuckyDrawPoolForWeek
    {
        int Id { get; set; }

        int WeekNumber { get; set; }

        DateTime CreatedAt { get; set; }

        string CreatedByUserId { get; set; }

        string CreatedByUserName { get; set; }
    }

    public class LuckyDrawPoolForWeek : ILuckyDrawPoolForWeek
    {
        public int Id { get; set; }

        public int WeekNumber { get; set; }

        public DateTime CreatedAt { get; set; }

        [StringLength(128)]
        public string CreatedByUserId { get; set; }

        [StringLength(256)]
        public string CreatedByUserName { get; set; }
    }

    [Table("LuckyDrawPoolWeek1")]
    public class LuckyDrawPoolWeek1 : LuckyDrawPoolForWeek
    {
        public LuckyDrawPoolWeek1()
        {
            WeekNumber = 1;
        }
    }

    [Table("LuckyDrawPoolWeek2")]
    public class LuckyDrawPoolWeek2 : LuckyDrawPoolForWeek
    {
        public LuckyDrawPoolWeek2()
        {
            WeekNumber = 2;
        }
    }

    [Table("LuckyDrawPoolWeek3")]
    public class LuckyDrawPoolWeek3 : LuckyDrawPoolForWeek
    {
        public LuckyDrawPoolWeek3()
        {
            WeekNumber = 3;
        }
    }

    [Table("LuckyDrawPoolWeek4")]
    public class LuckyDrawPoolWeek4 : LuckyDrawPoolForWeek
    {
        public LuckyDrawPoolWeek4()
        {
            WeekNumber = 4;
        }
    }

    [Table("LuckyDrawPoolWeek5")]
    public class LuckyDrawPoolWeek5 : LuckyDrawPoolForWeek
    {
        public LuckyDrawPoolWeek5()
        {
            WeekNumber = 5;
        }
    }

    [Table("LuckyDrawPoolWeek6")]
    public class LuckyDrawPoolWeek6 : LuckyDrawPoolForWeek
    {
        public LuckyDrawPoolWeek6()
        {
            WeekNumber = 6;
        }
    }

    [Table("LuckyDrawPoolWeek7")]
    public class LuckyDrawPoolWeek7 : LuckyDrawPoolForWeek
    {
        public LuckyDrawPoolWeek7()
        {
            WeekNumber = 7;
        }
    }

    [Table("LuckyDrawPoolWeek8")]
    public class LuckyDrawPoolWeek8 : LuckyDrawPoolForWeek
    {
        public LuckyDrawPoolWeek8()
        {
            WeekNumber = 8;
        }
    }

    [Table("LuckyDrawPoolWeek9")]
    public class LuckyDrawPoolWeek9 : LuckyDrawPoolForWeek
    {
        public LuckyDrawPoolWeek9()
        {
            WeekNumber = 9;
        }
    }

    [Table("LuckyDrawPoolWeek10")]
    public class LuckyDrawPoolWeek10 : LuckyDrawPoolForWeek
    {
        public LuckyDrawPoolWeek10()
        {
            WeekNumber = 10;
        }
    }

    [Table("LuckyDrawPoolWeek11")]
    public class LuckyDrawPoolWeek11 : LuckyDrawPoolForWeek
    {
        public LuckyDrawPoolWeek11()
        {
            WeekNumber = 11;
        }
    }

    [Table("LuckyDrawPoolWeek12")]
    public class LuckyDrawPoolWeek12 : LuckyDrawPoolForWeek
    {
        public LuckyDrawPoolWeek12()
        {
            WeekNumber = 12;
        }
    }

    [Table("LuckyDrawPoolWeek12")]
    public class LuckyDrawPoolWeek13 : LuckyDrawPoolForWeek
    {
        public LuckyDrawPoolWeek13()
        {
            WeekNumber = 13;
        }
    }
}

﻿namespace CalcheesePokeBattle.Model
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("WeekDefinition")]
    public class WeekDefinition
    {
        public int Id { get; set; }

        public int WeekNumber { get; set; }

        [StringLength(128)]
        public string WeekName { get; set; }

        public DateTime WeekStart { get; set; }

        public DateTime WeekEnd { get; set; }
    }
}
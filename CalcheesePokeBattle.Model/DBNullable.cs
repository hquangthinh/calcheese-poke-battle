﻿using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;

namespace CalcheesePokeBattle.Model
{
    public struct DBNullable<T>
    {
        private readonly T _value;

        public object Value => (object)_value ?? DBNull.Value;

        public DbParameter ToDbParameter(string parameterName, SqlDbType dbType = SqlDbType.VarChar, ParameterDirection direction = ParameterDirection.Input)
        {
            return new SqlParameter { ParameterName = parameterName, SqlDbType = dbType, Value = Value, Direction = direction };
        }

        public DBNullable(T value)
        {
            _value = value;
        }

        public static implicit operator DBNullable<T>(T value)
        {
            return new DBNullable<T>(value);
        }
    }
}

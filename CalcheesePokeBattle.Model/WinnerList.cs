﻿namespace CalcheesePokeBattle.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("WinnerList")]
    public partial class WinnerList
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public WinnerList()
        {
            WinnerListUsers = new HashSet<WinnerListUser>();
        }

        public int Id { get; set; }

        public int WeekNumber { get; set; }

        public DateTime CreatedDate { get; set; }

        [Required]
        [StringLength(50)]
        public string PriceType { get; set; }

        [Required]
        [StringLength(256)]
        public string PriceDescription { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<WinnerListUser> WinnerListUsers { get; set; }

        public int GiftItemID { get; set; }

        [StringLength(50)]
        public string Status { get; set; }
    }
}

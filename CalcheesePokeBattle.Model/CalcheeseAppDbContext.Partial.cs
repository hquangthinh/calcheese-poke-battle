﻿using System.Data;
using System.Data.SqlClient;

namespace CalcheesePokeBattle.Model
{
    public partial class CalcheeseAppDbContext : BaseDbContext, ICalcheeseAppDbContext
    {
        public void ExecSpStoreGameResult(string wonUserId, string lostUserId, int lostPoint, int lostCardId)
        {
            ExecuteNonQueryStoredProcedure(
                "spStoreGameResult",
                new SqlParameter { ParameterName = "wonUserId", Value = wonUserId, SqlDbType = SqlDbType.NVarChar },
                new SqlParameter { ParameterName = "lostUserId", Value = lostUserId, SqlDbType = SqlDbType.NVarChar },
                new SqlParameter { ParameterName = "lostPoint", Value = lostPoint, SqlDbType = SqlDbType.Int },
                new SqlParameter { ParameterName = "lostCardId", Value = lostCardId, SqlDbType = SqlDbType.Int }
            );
        }

        public void ExecspDisconnectAllPlayers()
        {
            ExecuteNonQueryStoredProcedure("spDisconnectAllPlayers");
        }
    }
}

﻿using System.Collections.Generic;

namespace CalcheesePokeBattle.Model
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("PrizeLuckyDrawHistory")]
    public partial class PrizeLuckyDrawHistory
    {

        public int Id { get; set; }
        [Required]
        public int GiftItemID { get; set; }
        [Required]
        [StringLength(20)]
        public string LuckyDrawCode { get; set; }
        public DateTime CreatedDate { get; set; }
        [Required]
        public int WeekNumber { get; set; }

        public virtual GiftItem GiftItem { get; set; }
    }
}
﻿namespace CalcheesePokeBattle.Model
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("PokeCard")]
    public partial class PokeCard
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public PokeCard()
        {
            UserCardCollections = new HashSet<UserCardCollection>();
        }

        public int Id { get; set; }

        [StringLength(255)]
        public string CardName { get; set; }

        public int? CardGroup { get; set; }

        [Required]
        [StringLength(20)]
        public string CardType { get; set; }

        [Required]
        [StringLength(50)]
        public string CardElement { get; set; }

        [StringLength(20)]
        public string CardNumber { get; set; }

        public int CardPoint { get; set; }

        public int? CardStar { get; set; }

        [StringLength(512)]
        public string ImageUrl { get; set; }

        [StringLength(512)]
        public string BackSideImageUrl { get; set; }

        [StringLength(255)]
        public string Category { get; set; }

        [StringLength(50)]
        public string Height { get; set; }

        [StringLength(50)]
        public string Weight { get; set; }

        [StringLength(255)]
        public string Skill1 { get; set; }

        [StringLength(255)]
        public string Skill2 { get; set; }

        [StringLength(255)]
        public string Skill3 { get; set; }

        [StringLength(255)]
        public string UltimateSkill { get; set; }

        public int? EvolutionTo { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<UserCardCollection> UserCardCollections { get; set; }
    }
}
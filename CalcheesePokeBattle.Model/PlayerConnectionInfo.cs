﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CalcheesePokeBattle.Model
{
    [Table("PlayerConnectionInfo")]
    public class PlayerConnectionInfo
    {
        public int Id { get; set; }

        [StringLength(255)]
        public string ServerName { get; set; }

        [StringLength(50)]
        public string ServerIpAddress { get; set; }

        [StringLength(128)]
        public string UserId { get; set; }

        [StringLength(255)]
        public string UserName { get; set; }

        [StringLength(255)]
        public string ConnectionId { get; set; }

        public DateTime ConnectedAt { get; set; }

        [StringLength(255)]
        public string DeviceInfo { get; set; }

        [StringLength(50)]
        public string Status { get; set; }

        [StringLength(50)]
        public string ClientIpAddress { get; set; }
    }
}

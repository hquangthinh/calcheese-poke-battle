﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.Entity;
using System.Data.Entity.Core;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using CalcheesePokeBattle.Common.Extensions;
using CalcheesePokeBattle.Common.Utils;
using Castle.Core.Logging;

namespace CalcheesePokeBattle.Model
{
    public interface IContext : IDisposable
    {
        ILogger Logger { get; set; }
        Database Database { get; }
        int SaveChanges();
        DbSet<TEntity> Set<TEntity>() where TEntity : class;
        DbEntityEntry<TEntity> Entry<TEntity>(TEntity entity) where TEntity : class;
        DbEntityEntry Entry(object entity);
    }

    public interface ICalcheeseAppDbContext : IContext
    {
        DbSet<CouponCode> CouponCodes { get; set; }
        DbSet<UsedCouponCode> UsedCouponCodes { get; set; }
        DbSet<UsedCouponCodeReport> UsedCouponCodeReports { get; set; }
        DbSet<GameHistory> GameHistories { get; set; }
        DbSet<GiftItem> GiftItems { get; set; }
        DbSet<PokeCard> PokeCards { get; set; }
        DbSet<UserCardCollection> UserCardCollections { get; set; }
        DbSet<UserInventory> UserInventories { get; set; }
        DbSet<UserLuckyDrawCode> UserLuckyDrawCodes { get; set; }
        DbSet<UserProfile> UserProfiles { get; set; }
        DbSet<WinnerList> WinnerLists { get; set; }
        DbSet<WinnerListUser> WinnerListUsers { get; set; }
        DbSet<Setting> Settings { get; set; }
        DbSet<UserActivityHistory> UserActivityHistories { get; set; }
        DbSet<PlayerConnectionInfo> PlayerConnectionInfos { get; set; }
        DbSet<AllOnlinePlayerConnectionInfo> AllOnlinePlayerConnectionInfos { get; set; }
        DbSet<WeekDefinition> WeekDefinitions { get; set; }
        DbSet<GiftRedeemHistory> GiftRedeemHistories { get; set; }
        DbSet<GiftWonFromLuckyDraw> GiftWonFromLuckyDraws { get; set; }
        DbSet<GiftAmountPerWeek> GiftAmountPerWeeks { get; set; }
        DbSet<GameSession> GameSessions { get; set; }
        DbSet<ImportJobStatus> ImportJobStatuses { get; set; }

        DbSet<LuckyDrawPoolWeek1> LuckyDrawPoolsWeek1 { get; set; }
        DbSet<LuckyDrawPoolWeek2> LuckyDrawPoolsWeek2 { get; set; }
        DbSet<LuckyDrawPoolWeek3> LuckyDrawPoolsWeek3 { get; set; }
        DbSet<LuckyDrawPoolWeek4> LuckyDrawPoolsWeek4 { get; set; }
        DbSet<LuckyDrawPoolWeek5> LuckyDrawPoolsWeek5 { get; set; }
        DbSet<LuckyDrawPoolWeek6> LuckyDrawPoolsWeek6 { get; set; }
        DbSet<LuckyDrawPoolWeek7> LuckyDrawPoolsWeek7 { get; set; }
        DbSet<LuckyDrawPoolWeek8> LuckyDrawPoolsWeek8 { get; set; }
        DbSet<LuckyDrawPoolWeek9> LuckyDrawPoolsWeek9 { get; set; }
        DbSet<LuckyDrawPoolWeek10> LuckyDrawPoolsWeek10 { get; set; }
        DbSet<LuckyDrawPoolWeek11> LuckyDrawPoolsWeek11 { get; set; }
        DbSet<LuckyDrawPoolWeek12> LuckyDrawPoolsWeek12 { get; set; }
    }

    public class BaseDbContext : DbContext, IContext
    {
        public ILogger Logger { get; set; }

        private readonly TransactionContext _transactionContext;

        protected BaseDbContext() : base("name=CalcheeseAppDbContext")
        {
            _transactionContext = new TransactionContext(this as ICalcheeseAppDbContext);
        }

        protected BaseDbContext(string nameOrConnectionString) : base(nameOrConnectionString)
        {
            _transactionContext = new TransactionContext(this as ICalcheeseAppDbContext);
        }

        public void PerformInTransaction(string transactionMessage, Action<ICalcheeseAppDbContext> action)
        {
            if (Logger.IsInfoEnabled) Logger.Info("Transaction started for: " + transactionMessage);
            var sqlConn = Database.Connection as SqlConnection;
            if (sqlConn != null)
            {
                sqlConn.InfoMessage += (s, a) =>
                {
                    if (Logger.IsDebugEnabled) Logger.Debug($"SQL Message : {a.Message}");
                };
            }
            var msg = $"Error! Transaction rolled back for: {transactionMessage}";
            try
            {
                _transactionContext.Enlist(() => action(this as ICalcheeseAppDbContext));
            }
            catch (DbEntityValidationException dbe)
            {
                foreach (var eve in dbe.EntityValidationErrors)
                {
                    Logger.ErrorFormat(
                        "Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State
                    );
                    foreach (var ve in eve.ValidationErrors)
                        Logger.ErrorFormat("- Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage);
                }

                throw new ApplicationException(msg, dbe);
            }
            catch (EntityCommandExecutionException ecx)
            {
                var message = ecx.InnerException?.Message;
                Logger.Error($"{transactionMessage} failed.\n {message}", ecx);

                throw new ApplicationException(message, ecx);
            }
        }

        protected int ExecuteNonQueryStoredProcedure(string procedureCall, params DbParameter[] parameters)
        {
            var parameterList = GenerateParameterList(parameters);
            return Database.ExecuteSqlCommand($"EXEC {procedureCall} {parameterList}", parameters.ToObjectArray());
        }

        private static string GenerateParameterList(IEnumerable<DbParameter> parameters)
        {
            return string.Join(
                ", ",
                parameters.Select(x =>
                    x.Direction == ParameterDirection.Output
                        ? $"@{x.ParameterName} OUTPUT"
                        : $"@{x.ParameterName}"
                )
            );
        }

        /// <remarks>
        /// This is NOT thread-safe by design due to the nature of DbContext where
        /// one instance of DbContext should belong to one and only one thread -
        /// which means each thread should have its own Transaction context
        /// </remarks>
        private class TransactionContext
        {
            private uint _latch;
            private readonly ICalcheeseAppDbContext _dbContext;

            public TransactionContext(ICalcheeseAppDbContext dbContext)
            {
                _latch = 0;
                _dbContext = dbContext;
            }

            public void Enlist(Action action)
            {
                var txn = _latch++ == 0
                    ? _dbContext.Database.BeginTransaction()
                    : _dbContext.Database.CurrentTransaction;

                try
                {
                    action();
                    if (--_latch == 0) txn.Commit();
                }
                catch (Exception ex)
                {
                    try
                    {
                        if (--_latch == 0) txn.Rollback();
                    }
                    catch
                    {
                        // ignored
                    }
                    ex.ReThrow();
                }
                finally
                {
                    if (_latch == 0) txn.Dispose();
                }
            }

            public async Task<TResult> EnlistAsync<TResult>(Func<Task<TResult>> action)
            {
                var txn = _latch++ == 0
                    ? _dbContext.Database.BeginTransaction()
                    : _dbContext.Database.CurrentTransaction;

                try
                {
                    var result = await action();
                    if (--_latch == 0) txn.Commit();
                    return result;
                }
                catch (Exception ex)
                {
                    try
                    {
                        if (--_latch == 0) txn.Rollback();
                    }
                    catch
                    {
                        // ignored
                    }
                    ex.ReThrow();
                    // never reach this - just to make code compile
                    return default(TResult);
                }
                finally
                {
                    if (_latch == 0) txn.Dispose();
                }
            }
        }
    }
}

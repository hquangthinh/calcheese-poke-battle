﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CalcheesePokeBattle.Model
{
    [Table("GiftWonFromLuckyDraw")]
    public class GiftWonFromLuckyDraw
    {
        public int Id { get; set; }

        public int WeekNumber { get; set; }

        public int GiftId { get; set; }

        [StringLength(128)]
        public string UserId { get; set; }

        public DateTime CreatedAt { get; set; }
    }
}

﻿using Castle.Core.Logging;

namespace CalcheesePokeBattle.Model
{
    public interface IDbFactory
    {
        CalcheeseAppDbContext GetDbContext();
        CalcheeseAppDbContext CreateNewContext();
    }

    public class DefaultDbFactory : IDbFactory
    {
        private readonly CalcheeseAppDbContext _dbContext;

        public ILogger Logger { get; set; }

        public DefaultDbFactory(CalcheeseAppDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public CalcheeseAppDbContext GetDbContext()
        {
            return _dbContext;
        }

        public CalcheeseAppDbContext CreateNewContext()
        {
            return new CalcheeseAppDbContext {Logger = Logger};
        }
    }
}

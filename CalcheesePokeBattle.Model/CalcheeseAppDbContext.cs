﻿using System.Data.Entity;

namespace CalcheesePokeBattle.Model
{
    public partial class CalcheeseAppDbContext
    {
        public CalcheeseAppDbContext()
            : base("name=CalcheeseAppDbContext")
        {
            Database.SetInitializer(new NullDatabaseInitializer<CalcheeseAppDbContext>());
        }

        public virtual DbSet<UploadFile> UploadFiles { get; set; }
        public virtual DbSet<CouponCode> CouponCodes { get; set; }
        public virtual DbSet<UsedCouponCode> UsedCouponCodes { get; set; }
        public virtual DbSet<UsedCouponCodeReport> UsedCouponCodeReports { get; set; }
        public virtual DbSet<GameHistory> GameHistories { get; set; }
        public virtual DbSet<GiftItem> GiftItems { get; set; }
        public virtual DbSet<PokeCard> PokeCards { get; set; }
        public virtual DbSet<UserCardCollection> UserCardCollections { get; set; }
        public virtual DbSet<UserInventory> UserInventories { get; set; }
        public virtual DbSet<UserLuckyDrawCode> UserLuckyDrawCodes { get; set; }
        public virtual DbSet<UserProfile> UserProfiles { get; set; }
        public virtual DbSet<WinnerList> WinnerLists { get; set; }
        public virtual DbSet<WinnerListUser> WinnerListUsers { get; set; }
        public virtual DbSet<Setting> Settings { get; set; }
        public virtual DbSet<UserActivityHistory> UserActivityHistories { get; set; }
        public virtual DbSet<PlayerConnectionInfo> PlayerConnectionInfos { get; set; }
        public virtual DbSet<AllOnlinePlayerConnectionInfo> AllOnlinePlayerConnectionInfos { get; set; }
        public virtual DbSet<WeekDefinition> WeekDefinitions { get; set; }
        public virtual DbSet<GiftRedeemHistory> GiftRedeemHistories { get; set; }
        public virtual DbSet<GiftWonFromLuckyDraw> GiftWonFromLuckyDraws { get; set; }
        public virtual DbSet<GiftAmountPerWeek> GiftAmountPerWeeks { get; set; }
        public virtual DbSet<GameSession> GameSessions { get; set; }
        public virtual DbSet<ImportJobStatus> ImportJobStatuses { get; set; }

        public virtual DbSet<PrizeLuckyDrawHistory> PrizeLuckyDrawHistories { get; set; }
        public virtual DbSet<LuckyDrawPoolWeek1> LuckyDrawPoolsWeek1 { get; set; }
        public virtual DbSet<LuckyDrawPoolWeek2> LuckyDrawPoolsWeek2 { get; set; }
        public virtual DbSet<LuckyDrawPoolWeek3> LuckyDrawPoolsWeek3 { get; set; }
        public virtual DbSet<LuckyDrawPoolWeek4> LuckyDrawPoolsWeek4 { get; set; }
        public virtual DbSet<LuckyDrawPoolWeek5> LuckyDrawPoolsWeek5 { get; set; }
        public virtual DbSet<LuckyDrawPoolWeek6> LuckyDrawPoolsWeek6 { get; set; }
        public virtual DbSet<LuckyDrawPoolWeek7> LuckyDrawPoolsWeek7 { get; set; }
        public virtual DbSet<LuckyDrawPoolWeek8> LuckyDrawPoolsWeek8 { get; set; }
        public virtual DbSet<LuckyDrawPoolWeek9> LuckyDrawPoolsWeek9 { get; set; }
        public virtual DbSet<LuckyDrawPoolWeek10> LuckyDrawPoolsWeek10 { get; set; }
        public virtual DbSet<LuckyDrawPoolWeek11> LuckyDrawPoolsWeek11 { get; set; }
        public virtual DbSet<LuckyDrawPoolWeek12> LuckyDrawPoolsWeek12 { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<PokeCard>()
                .HasMany(e => e.UserCardCollections)
                .WithRequired(e => e.PokeCard)
                .HasForeignKey(e => e.CardId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<UserProfile>()
                .HasMany(e => e.UserCardCollections)
                .WithRequired(e => e.UserProfile)
                .HasForeignKey(e => e.UserId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<UserProfile>()
                .HasMany(e => e.UserInventories)
                .WithRequired(e => e.UserProfile)
                .HasForeignKey(e => e.UserId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<UserProfile>()
                .HasMany(e => e.UserLuckyDrawCodes)
                .WithRequired(e => e.UserProfile)
                .HasForeignKey(e => e.UserId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<GiftItem>()
               .HasMany(e => e.PrizeLuckyDrawHistories)
               .WithRequired(e => e.GiftItem)
               .HasForeignKey(e => e.GiftItemID)
               .WillCascadeOnDelete(false);
        }
    }
}

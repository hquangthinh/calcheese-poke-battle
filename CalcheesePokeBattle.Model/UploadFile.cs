﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace CalcheesePokeBattle.Model
{
    [Table("UploadFile")]
    public class UploadFile
    {
        public int Id { get; set; }

        public DateTime CreatedDate { get; set; }

        public int PokeCardId { get; set; }

        public string FileName { get; set; }

        public string ImportStatus { get; set; }
    }
}

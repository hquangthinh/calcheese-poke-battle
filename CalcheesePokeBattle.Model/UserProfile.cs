﻿using System;

namespace CalcheesePokeBattle.Model
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("UserProfile")]
    public partial class UserProfile
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public UserProfile()
        {
            UserCardCollections = new HashSet<UserCardCollection>();
            UserInventories = new HashSet<UserInventory>();
            UserLuckyDrawCodes = new HashSet<UserLuckyDrawCode>();
        }

        public string Id { get; set; }

        [Required]
        [StringLength(256)]
        public string UserName { get; set; }

        [StringLength(128)]
        public string FirstName { get; set; }

        [StringLength(256)]
        public string LastName { get; set; }

        [StringLength(20)]
        public string PhoneNumber { get; set; }

        [StringLength(512)]
        public string Address { get; set; }

        [StringLength(512)]
        public string DeliveryAddress { get; set; }

        [StringLength(20)]
        public string SocialIdNumber { get; set; }

        [StringLength(256)]
        public string Email { get; set; }

        [StringLength(255)]
        public string AvatarUrl { get; set; }

        [StringLength(20)]
        public string AddressNumber { get; set; }

        [StringLength(128)]
        public string StreetName { get; set; }

        [StringLength(128)]
        public string WardName { get; set; }

        [StringLength(128)]
        public string DistrictName { get; set; }

        [StringLength(128)]
        public string CityName { get; set; }

        public DateTime? CreatedDate { get; set; }

        public int? TotalPoint { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<UserCardCollection> UserCardCollections { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<UserInventory> UserInventories { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<UserLuckyDrawCode> UserLuckyDrawCodes { get; set; }
    }
}

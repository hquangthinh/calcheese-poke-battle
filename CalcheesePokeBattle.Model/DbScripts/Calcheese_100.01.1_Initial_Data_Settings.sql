﻿
-- settings for confirm lucky draw prize on front site
-- setting value is string: yes | no
IF NOT EXISTS(SELECT 1 FROM Setting WHERE SettingKey='ShowLuckyDrawPrizeOnFrontSiteWeek1')
	INSERT INTO Setting(SettingKey, SettingValueString)
		VALUES('ShowLuckyDrawPrizeOnFrontSiteWeek1', 'no')
GO

IF NOT EXISTS(SELECT 1 FROM Setting WHERE SettingKey='ShowLuckyDrawPrizeOnFrontSiteWeek2')
	INSERT INTO Setting(SettingKey, SettingValueString)
		VALUES('ShowLuckyDrawPrizeOnFrontSiteWeek2', 'no')
GO

IF NOT EXISTS(SELECT 1 FROM Setting WHERE SettingKey='ShowLuckyDrawPrizeOnFrontSiteWeek3')
	INSERT INTO Setting(SettingKey, SettingValueString)
		VALUES('ShowLuckyDrawPrizeOnFrontSiteWeek3', 'no')
GO

IF NOT EXISTS(SELECT 1 FROM Setting WHERE SettingKey='ShowLuckyDrawPrizeOnFrontSiteWeek4')
	INSERT INTO Setting(SettingKey, SettingValueString)
		VALUES('ShowLuckyDrawPrizeOnFrontSiteWeek4', 'no')
GO

IF NOT EXISTS(SELECT 1 FROM Setting WHERE SettingKey='ShowLuckyDrawPrizeOnFrontSiteWeek5')
	INSERT INTO Setting(SettingKey, SettingValueString)
		VALUES('ShowLuckyDrawPrizeOnFrontSiteWeek5', 'no')
GO

IF NOT EXISTS(SELECT 1 FROM Setting WHERE SettingKey='ShowLuckyDrawPrizeOnFrontSiteWeek6')
	INSERT INTO Setting(SettingKey, SettingValueString)
		VALUES('ShowLuckyDrawPrizeOnFrontSiteWeek6', 'no')
GO

IF NOT EXISTS(SELECT 1 FROM Setting WHERE SettingKey='ShowLuckyDrawPrizeOnFrontSiteWeek7')
	INSERT INTO Setting(SettingKey, SettingValueString)
		VALUES('ShowLuckyDrawPrizeOnFrontSiteWeek7', 'no')
GO

IF NOT EXISTS(SELECT 1 FROM Setting WHERE SettingKey='ShowLuckyDrawPrizeOnFrontSiteWeek8')
	INSERT INTO Setting(SettingKey, SettingValueString)
		VALUES('ShowLuckyDrawPrizeOnFrontSiteWeek8', 'no')
GO

IF NOT EXISTS(SELECT 1 FROM Setting WHERE SettingKey='ShowLuckyDrawPrizeOnFrontSiteWeek9')
	INSERT INTO Setting(SettingKey, SettingValueString)
		VALUES('ShowLuckyDrawPrizeOnFrontSiteWeek9', 'no')
GO

IF NOT EXISTS(SELECT 1 FROM Setting WHERE SettingKey='ShowLuckyDrawPrizeOnFrontSiteWeek10')
	INSERT INTO Setting(SettingKey, SettingValueString)
		VALUES('ShowLuckyDrawPrizeOnFrontSiteWeek10', 'no')
GO

IF NOT EXISTS(SELECT 1 FROM Setting WHERE SettingKey='ShowLuckyDrawPrizeOnFrontSiteWeek11')
	INSERT INTO Setting(SettingKey, SettingValueString)
		VALUES('ShowLuckyDrawPrizeOnFrontSiteWeek11', 'no')
GO

IF NOT EXISTS(SELECT 1 FROM Setting WHERE SettingKey='ShowLuckyDrawPrizeOnFrontSiteWeek12')
	INSERT INTO Setting(SettingKey, SettingValueString)
		VALUES('ShowLuckyDrawPrizeOnFrontSiteWeek12', 'no')
GO

IF NOT EXISTS(SELECT 1 FROM Setting WHERE SettingKey='ShowLuckyDrawPrizeOnFrontSiteWeek13')
	INSERT INTO Setting(SettingKey, SettingValueString)
		VALUES('ShowLuckyDrawPrizeOnFrontSiteWeek13', 'no')
GO

IF NOT EXISTS(SELECT 1 FROM Setting WHERE SettingKey='UsedCouponCodeReportGeneratorLastRunSettingKey')
	INSERT INTO Setting(SettingKey, SettingValueString)
		VALUES('UsedCouponCodeReportGeneratorLastRunSettingKey', '2017-04-15')
GO
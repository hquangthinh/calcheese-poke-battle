﻿/* Data for table GiftAmountPerWeek*/
USE [CalcheeseDev]
GO
SET IDENTITY_INSERT [dbo].[GiftAmountPerWeek] ON 

GO
INSERT [dbo].[GiftAmountPerWeek] ([Id], [WeekNumber], [GiftName], [TotalPrize]) VALUES (1, 1, N'Balo', 25)
GO
INSERT [dbo].[GiftAmountPerWeek] ([Id], [WeekNumber], [GiftName], [TotalPrize]) VALUES (2, 1, N'AoThun', 50)
GO
INSERT [dbo].[GiftAmountPerWeek] ([Id], [WeekNumber], [GiftName], [TotalPrize]) VALUES (3, 1, N'Mu', 50)
GO
INSERT [dbo].[GiftAmountPerWeek] ([Id], [WeekNumber], [GiftName], [TotalPrize]) VALUES (4, 1, N'TraiHe', 1)
GO
INSERT [dbo].[GiftAmountPerWeek] ([Id], [WeekNumber], [GiftName], [TotalPrize]) VALUES (5, 1, N'VinID', 1)
GO
INSERT [dbo].[GiftAmountPerWeek] ([Id], [WeekNumber], [GiftName], [TotalPrize]) VALUES (6, 1, N'XeDien', 0)
GO
INSERT [dbo].[GiftAmountPerWeek] ([Id], [WeekNumber], [GiftName], [TotalPrize]) VALUES (7, 1, N'DoChoi', 30)
GO
INSERT [dbo].[GiftAmountPerWeek] ([Id], [WeekNumber], [GiftName], [TotalPrize]) VALUES (8, 2, N'Balo', 25)
GO
INSERT [dbo].[GiftAmountPerWeek] ([Id], [WeekNumber], [GiftName], [TotalPrize]) VALUES (9, 2, N'AoThun', 50)
GO
INSERT [dbo].[GiftAmountPerWeek] ([Id], [WeekNumber], [GiftName], [TotalPrize]) VALUES (10, 2, N'Mu', 50)
GO
INSERT [dbo].[GiftAmountPerWeek] ([Id], [WeekNumber], [GiftName], [TotalPrize]) VALUES (11, 2, N'TraiHe', 1)
GO
INSERT [dbo].[GiftAmountPerWeek] ([Id], [WeekNumber], [GiftName], [TotalPrize]) VALUES (12, 2, N'VinID', 0)
GO
INSERT [dbo].[GiftAmountPerWeek] ([Id], [WeekNumber], [GiftName], [TotalPrize]) VALUES (13, 2, N'XeDien', 1)
GO
INSERT [dbo].[GiftAmountPerWeek] ([Id], [WeekNumber], [GiftName], [TotalPrize]) VALUES (14, 2, N'DoChoi', 30)
GO
INSERT [dbo].[GiftAmountPerWeek] ([Id], [WeekNumber], [GiftName], [TotalPrize]) VALUES (15, 3, N'Balo', 25)
GO
INSERT [dbo].[GiftAmountPerWeek] ([Id], [WeekNumber], [GiftName], [TotalPrize]) VALUES (16, 3, N'AoThun', 50)
GO
INSERT [dbo].[GiftAmountPerWeek] ([Id], [WeekNumber], [GiftName], [TotalPrize]) VALUES (17, 3, N'Mu', 50)
GO
INSERT [dbo].[GiftAmountPerWeek] ([Id], [WeekNumber], [GiftName], [TotalPrize]) VALUES (18, 3, N'TraiHe', 1)
GO
INSERT [dbo].[GiftAmountPerWeek] ([Id], [WeekNumber], [GiftName], [TotalPrize]) VALUES (19, 3, N'VinID', 1)
GO
INSERT [dbo].[GiftAmountPerWeek] ([Id], [WeekNumber], [GiftName], [TotalPrize]) VALUES (20, 3, N'XeDien', 0)
GO
INSERT [dbo].[GiftAmountPerWeek] ([Id], [WeekNumber], [GiftName], [TotalPrize]) VALUES (21, 3, N'DoChoi', 30)
GO
INSERT [dbo].[GiftAmountPerWeek] ([Id], [WeekNumber], [GiftName], [TotalPrize]) VALUES (22, 4, N'Balo', 25)
GO
INSERT [dbo].[GiftAmountPerWeek] ([Id], [WeekNumber], [GiftName], [TotalPrize]) VALUES (23, 4, N'AoThun', 50)
GO
INSERT [dbo].[GiftAmountPerWeek] ([Id], [WeekNumber], [GiftName], [TotalPrize]) VALUES (24, 4, N'Mu', 50)
GO
INSERT [dbo].[GiftAmountPerWeek] ([Id], [WeekNumber], [GiftName], [TotalPrize]) VALUES (25, 4, N'TraiHe', 1)
GO
INSERT [dbo].[GiftAmountPerWeek] ([Id], [WeekNumber], [GiftName], [TotalPrize]) VALUES (26, 4, N'VinID', 0)
GO
INSERT [dbo].[GiftAmountPerWeek] ([Id], [WeekNumber], [GiftName], [TotalPrize]) VALUES (27, 4, N'XeDien', 1)
GO
INSERT [dbo].[GiftAmountPerWeek] ([Id], [WeekNumber], [GiftName], [TotalPrize]) VALUES (28, 4, N'DoChoi', 30)
GO
INSERT [dbo].[GiftAmountPerWeek] ([Id], [WeekNumber], [GiftName], [TotalPrize]) VALUES (29, 5, N'Balo', 25)
GO
INSERT [dbo].[GiftAmountPerWeek] ([Id], [WeekNumber], [GiftName], [TotalPrize]) VALUES (30, 5, N'AoThun', 50)
GO
INSERT [dbo].[GiftAmountPerWeek] ([Id], [WeekNumber], [GiftName], [TotalPrize]) VALUES (31, 5, N'Mu', 50)
GO
INSERT [dbo].[GiftAmountPerWeek] ([Id], [WeekNumber], [GiftName], [TotalPrize]) VALUES (32, 5, N'TraiHe', 1)
GO
INSERT [dbo].[GiftAmountPerWeek] ([Id], [WeekNumber], [GiftName], [TotalPrize]) VALUES (33, 5, N'VinID', 1)
GO
INSERT [dbo].[GiftAmountPerWeek] ([Id], [WeekNumber], [GiftName], [TotalPrize]) VALUES (34, 5, N'XeDien', 0)
GO
INSERT [dbo].[GiftAmountPerWeek] ([Id], [WeekNumber], [GiftName], [TotalPrize]) VALUES (35, 5, N'DoChoi', 30)
GO
INSERT [dbo].[GiftAmountPerWeek] ([Id], [WeekNumber], [GiftName], [TotalPrize]) VALUES (36, 6, N'Balo', 25)
GO
INSERT [dbo].[GiftAmountPerWeek] ([Id], [WeekNumber], [GiftName], [TotalPrize]) VALUES (37, 6, N'AoThun', 50)
GO
INSERT [dbo].[GiftAmountPerWeek] ([Id], [WeekNumber], [GiftName], [TotalPrize]) VALUES (38, 6, N'Mu', 50)
GO
INSERT [dbo].[GiftAmountPerWeek] ([Id], [WeekNumber], [GiftName], [TotalPrize]) VALUES (39, 6, N'TraiHe', 1)
GO
INSERT [dbo].[GiftAmountPerWeek] ([Id], [WeekNumber], [GiftName], [TotalPrize]) VALUES (40, 6, N'VinID', 0)
GO
INSERT [dbo].[GiftAmountPerWeek] ([Id], [WeekNumber], [GiftName], [TotalPrize]) VALUES (41, 6, N'XeDien', 1)
GO
INSERT [dbo].[GiftAmountPerWeek] ([Id], [WeekNumber], [GiftName], [TotalPrize]) VALUES (42, 6, N'DoChoi', 30)
GO
INSERT [dbo].[GiftAmountPerWeek] ([Id], [WeekNumber], [GiftName], [TotalPrize]) VALUES (43, 7, N'Balo', 25)
GO
INSERT [dbo].[GiftAmountPerWeek] ([Id], [WeekNumber], [GiftName], [TotalPrize]) VALUES (44, 7, N'AoThun', 50)
GO
INSERT [dbo].[GiftAmountPerWeek] ([Id], [WeekNumber], [GiftName], [TotalPrize]) VALUES (45, 7, N'Mu', 50)
GO
INSERT [dbo].[GiftAmountPerWeek] ([Id], [WeekNumber], [GiftName], [TotalPrize]) VALUES (46, 7, N'TraiHe', 1)
GO
INSERT [dbo].[GiftAmountPerWeek] ([Id], [WeekNumber], [GiftName], [TotalPrize]) VALUES (47, 7, N'VinID', 1)
GO
INSERT [dbo].[GiftAmountPerWeek] ([Id], [WeekNumber], [GiftName], [TotalPrize]) VALUES (48, 7, N'XeDien', 0)
GO
INSERT [dbo].[GiftAmountPerWeek] ([Id], [WeekNumber], [GiftName], [TotalPrize]) VALUES (49, 7, N'DoChoi', 30)
GO
INSERT [dbo].[GiftAmountPerWeek] ([Id], [WeekNumber], [GiftName], [TotalPrize]) VALUES (50, 8, N'Balo', 25)
GO
INSERT [dbo].[GiftAmountPerWeek] ([Id], [WeekNumber], [GiftName], [TotalPrize]) VALUES (51, 8, N'AoThun', 50)
GO
INSERT [dbo].[GiftAmountPerWeek] ([Id], [WeekNumber], [GiftName], [TotalPrize]) VALUES (52, 8, N'Mu', 50)
GO
INSERT [dbo].[GiftAmountPerWeek] ([Id], [WeekNumber], [GiftName], [TotalPrize]) VALUES (53, 8, N'TraiHe', 1)
GO
INSERT [dbo].[GiftAmountPerWeek] ([Id], [WeekNumber], [GiftName], [TotalPrize]) VALUES (54, 8, N'VinID', 0)
GO
INSERT [dbo].[GiftAmountPerWeek] ([Id], [WeekNumber], [GiftName], [TotalPrize]) VALUES (55, 8, N'XeDien', 1)
GO
INSERT [dbo].[GiftAmountPerWeek] ([Id], [WeekNumber], [GiftName], [TotalPrize]) VALUES (56, 8, N'DoChoi', 30)
GO
INSERT [dbo].[GiftAmountPerWeek] ([Id], [WeekNumber], [GiftName], [TotalPrize]) VALUES (57, 9, N'Balo', 25)
GO
INSERT [dbo].[GiftAmountPerWeek] ([Id], [WeekNumber], [GiftName], [TotalPrize]) VALUES (58, 9, N'AoThun', 50)
GO
INSERT [dbo].[GiftAmountPerWeek] ([Id], [WeekNumber], [GiftName], [TotalPrize]) VALUES (59, 9, N'Mu', 50)
GO
INSERT [dbo].[GiftAmountPerWeek] ([Id], [WeekNumber], [GiftName], [TotalPrize]) VALUES (60, 9, N'TraiHe', 1)
GO
INSERT [dbo].[GiftAmountPerWeek] ([Id], [WeekNumber], [GiftName], [TotalPrize]) VALUES (61, 9, N'VinID', 1)
GO
INSERT [dbo].[GiftAmountPerWeek] ([Id], [WeekNumber], [GiftName], [TotalPrize]) VALUES (62, 9, N'XeDien', 0)
GO
INSERT [dbo].[GiftAmountPerWeek] ([Id], [WeekNumber], [GiftName], [TotalPrize]) VALUES (63, 9, N'DoChoi', 30)
GO
INSERT [dbo].[GiftAmountPerWeek] ([Id], [WeekNumber], [GiftName], [TotalPrize]) VALUES (64, 10, N'Balo', 25)
GO
INSERT [dbo].[GiftAmountPerWeek] ([Id], [WeekNumber], [GiftName], [TotalPrize]) VALUES (65, 10, N'AoThun', 50)
GO
INSERT [dbo].[GiftAmountPerWeek] ([Id], [WeekNumber], [GiftName], [TotalPrize]) VALUES (66, 10, N'Mu', 50)
GO
INSERT [dbo].[GiftAmountPerWeek] ([Id], [WeekNumber], [GiftName], [TotalPrize]) VALUES (67, 10, N'TraiHe', 1)
GO
INSERT [dbo].[GiftAmountPerWeek] ([Id], [WeekNumber], [GiftName], [TotalPrize]) VALUES (68, 10, N'VinID', 0)
GO
INSERT [dbo].[GiftAmountPerWeek] ([Id], [WeekNumber], [GiftName], [TotalPrize]) VALUES (69, 10, N'XeDien', 1)
GO
INSERT [dbo].[GiftAmountPerWeek] ([Id], [WeekNumber], [GiftName], [TotalPrize]) VALUES (70, 10, N'DoChoi', 30)
GO
INSERT [dbo].[GiftAmountPerWeek] ([Id], [WeekNumber], [GiftName], [TotalPrize]) VALUES (71, 11, N'Balo', 25)
GO
INSERT [dbo].[GiftAmountPerWeek] ([Id], [WeekNumber], [GiftName], [TotalPrize]) VALUES (72, 11, N'AoThun', 50)
GO
INSERT [dbo].[GiftAmountPerWeek] ([Id], [WeekNumber], [GiftName], [TotalPrize]) VALUES (73, 11, N'Mu', 50)
GO
INSERT [dbo].[GiftAmountPerWeek] ([Id], [WeekNumber], [GiftName], [TotalPrize]) VALUES (74, 11, N'TraiHe', 1)
GO
INSERT [dbo].[GiftAmountPerWeek] ([Id], [WeekNumber], [GiftName], [TotalPrize]) VALUES (75, 11, N'VinID', 1)
GO
INSERT [dbo].[GiftAmountPerWeek] ([Id], [WeekNumber], [GiftName], [TotalPrize]) VALUES (76, 11, N'XeDien', 0)
GO
INSERT [dbo].[GiftAmountPerWeek] ([Id], [WeekNumber], [GiftName], [TotalPrize]) VALUES (77, 11, N'DoChoi', 30)
GO
INSERT [dbo].[GiftAmountPerWeek] ([Id], [WeekNumber], [GiftName], [TotalPrize]) VALUES (78, 12, N'Balo', 25)
GO
INSERT [dbo].[GiftAmountPerWeek] ([Id], [WeekNumber], [GiftName], [TotalPrize]) VALUES (79, 12, N'AoThun', 50)
GO
INSERT [dbo].[GiftAmountPerWeek] ([Id], [WeekNumber], [GiftName], [TotalPrize]) VALUES (80, 12, N'Mu', 50)
GO
INSERT [dbo].[GiftAmountPerWeek] ([Id], [WeekNumber], [GiftName], [TotalPrize]) VALUES (81, 12, N'TraiHe', 1)
GO
INSERT [dbo].[GiftAmountPerWeek] ([Id], [WeekNumber], [GiftName], [TotalPrize]) VALUES (82, 12, N'VinID', 0)
GO
INSERT [dbo].[GiftAmountPerWeek] ([Id], [WeekNumber], [GiftName], [TotalPrize]) VALUES (83, 12, N'XeDien', 1)
GO
INSERT [dbo].[GiftAmountPerWeek] ([Id], [WeekNumber], [GiftName], [TotalPrize]) VALUES (84, 12, N'DoChoi', 30)
GO
INSERT [dbo].[GiftAmountPerWeek] ([Id], [WeekNumber], [GiftName], [TotalPrize]) VALUES (85, 13, N'Balo', 25)
GO
INSERT [dbo].[GiftAmountPerWeek] ([Id], [WeekNumber], [GiftName], [TotalPrize]) VALUES (86, 13, N'AoThun', 50)
GO
INSERT [dbo].[GiftAmountPerWeek] ([Id], [WeekNumber], [GiftName], [TotalPrize]) VALUES (87, 13, N'Mu', 50)
GO
INSERT [dbo].[GiftAmountPerWeek] ([Id], [WeekNumber], [GiftName], [TotalPrize]) VALUES (88, 13, N'TraiHe', 1)
GO
INSERT [dbo].[GiftAmountPerWeek] ([Id], [WeekNumber], [GiftName], [TotalPrize]) VALUES (89, 13, N'VinID', 1)
GO
INSERT [dbo].[GiftAmountPerWeek] ([Id], [WeekNumber], [GiftName], [TotalPrize]) VALUES (90, 13, N'XeDien', 0)
GO
INSERT [dbo].[GiftAmountPerWeek] ([Id], [WeekNumber], [GiftName], [TotalPrize]) VALUES (91, 13, N'DoChoi', 30)
GO
SET IDENTITY_INSERT [dbo].[GiftAmountPerWeek] OFF
GO

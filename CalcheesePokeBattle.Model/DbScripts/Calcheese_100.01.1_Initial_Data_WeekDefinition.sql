﻿/* Data for table WeekDefinition*/

SET IDENTITY_INSERT [dbo].[WeekDefinition] ON 

GO
INSERT [dbo].[WeekDefinition] ([Id], [WeekNumber], [WeekName], [WeekStart], [WeekEnd]) VALUES (1, 1, N'Tuần 1', CAST(N'2017-03-26 00:00:00.000' AS DateTime), CAST(N'2017-04-01 00:00:00.000' AS DateTime))
GO
INSERT [dbo].[WeekDefinition] ([Id], [WeekNumber], [WeekName], [WeekStart], [WeekEnd]) VALUES (2, 2, N'Tuần 2', CAST(N'2017-04-02 00:00:00.000' AS DateTime), CAST(N'2017-04-08 00:00:00.000' AS DateTime))
GO
INSERT [dbo].[WeekDefinition] ([Id], [WeekNumber], [WeekName], [WeekStart], [WeekEnd]) VALUES (3, 3, N'Tuần 3', CAST(N'2017-04-09 00:00:00.000' AS DateTime), CAST(N'2017-04-15 00:00:00.000' AS DateTime))
GO
INSERT [dbo].[WeekDefinition] ([Id], [WeekNumber], [WeekName], [WeekStart], [WeekEnd]) VALUES (4, 4, N'Tuần 4', CAST(N'2017-04-16 00:00:00.000' AS DateTime), CAST(N'2017-04-22 00:00:00.000' AS DateTime))
GO
INSERT [dbo].[WeekDefinition] ([Id], [WeekNumber], [WeekName], [WeekStart], [WeekEnd]) VALUES (5, 5, N'Tuần 5', CAST(N'2017-04-23 00:00:00.000' AS DateTime), CAST(N'2017-04-29 00:00:00.000' AS DateTime))
GO
INSERT [dbo].[WeekDefinition] ([Id], [WeekNumber], [WeekName], [WeekStart], [WeekEnd]) VALUES (6, 6, N'Tuần 6', CAST(N'2017-04-30 00:00:00.000' AS DateTime), CAST(N'2017-05-06 00:00:00.000' AS DateTime))
GO
INSERT [dbo].[WeekDefinition] ([Id], [WeekNumber], [WeekName], [WeekStart], [WeekEnd]) VALUES (7, 7, N'Tuần 7', CAST(N'2017-05-07 00:00:00.000' AS DateTime), CAST(N'2017-05-13 00:00:00.000' AS DateTime))
GO
INSERT [dbo].[WeekDefinition] ([Id], [WeekNumber], [WeekName], [WeekStart], [WeekEnd]) VALUES (8, 8, N'Tuần 8', CAST(N'2017-05-14 00:00:00.000' AS DateTime), CAST(N'2017-05-20 00:00:00.000' AS DateTime))
GO
INSERT [dbo].[WeekDefinition] ([Id], [WeekNumber], [WeekName], [WeekStart], [WeekEnd]) VALUES (9, 9, N'Tuần 9', CAST(N'2017-05-21 00:00:00.000' AS DateTime), CAST(N'2017-05-27 00:00:00.000' AS DateTime))
GO
INSERT [dbo].[WeekDefinition] ([Id], [WeekNumber], [WeekName], [WeekStart], [WeekEnd]) VALUES (10, 10, N'Tuần 10', CAST(N'2017-05-28 00:00:00.000' AS DateTime), CAST(N'2017-06-03 00:00:00.000' AS DateTime))
GO
INSERT [dbo].[WeekDefinition] ([Id], [WeekNumber], [WeekName], [WeekStart], [WeekEnd]) VALUES (11, 11, N'Tuần 11', CAST(N'2017-06-04 00:00:00.000' AS DateTime), CAST(N'2017-06-10 00:00:00.000' AS DateTime))
GO
INSERT [dbo].[WeekDefinition] ([Id], [WeekNumber], [WeekName], [WeekStart], [WeekEnd]) VALUES (12, 12, N'Tuần 12', CAST(N'2017-06-11 00:00:00.000' AS DateTime), CAST(N'2017-06-17 00:00:00.000' AS DateTime))
GO
INSERT [dbo].[WeekDefinition] ([Id], [WeekNumber], [WeekName], [WeekStart], [WeekEnd]) VALUES (13, 13, N'Tuần 13', CAST(N'2017-06-18 00:00:00.000' AS DateTime), CAST(N'2017-06-24 00:00:00.000' AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[WeekDefinition] OFF
GO

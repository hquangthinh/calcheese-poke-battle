﻿/****** Object:  Table [dbo].[GameHistory]    Script Date: 3/20/2017 7:56:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GameHistory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[GameHistory](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId1] [nvarchar](128) NOT NULL,
	[UserId2] [nvarchar](128) NOT NULL,
	[WinnerUserId] [nvarchar](128) NULL,
	[CreatedDate] [datetime] NOT NULL,
	[User1Score] [int] NULL,
	[User2Score] [int] NULL,
 CONSTRAINT [PK_GameHistory] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[GiftItem]    Script Date: 3/20/2017 7:56:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GiftItem]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[GiftItem](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](512) NOT NULL,
	[ImageUrl] [nvarchar](512) NOT NULL,
	[RedeemPoint] [int] NOT NULL,
 CONSTRAINT [PK_GiftItem] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[PokeCard]    Script Date: 3/20/2017 7:56:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PokeCard]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[PokeCard](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CardType] [nvarchar](20) NOT NULL,
	[CardElement] [nvarchar](50) NOT NULL,
	[CardNumber] [nvarchar](20) NULL,
	[CardPoint] [int] NOT NULL,
	[CardStar] [int] NULL,
	[ImageUrl] [nvarchar](512) NULL,
 CONSTRAINT [PK_PokeCard] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[Setting]    Script Date: 3/20/2017 7:56:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Setting]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Setting](
	[SettingKey] [nvarchar](128) NOT NULL,
	[Description] [nvarchar](512) NULL,
	[SettingValueString] [nvarchar](2048) NULL,
	[SettingValueInt] [int] NULL,
 CONSTRAINT [PK_Setting] PRIMARY KEY CLUSTERED 
(
	[SettingKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[UserCardCollection]    Script Date: 3/20/2017 7:56:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UserCardCollection]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[UserCardCollection](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [nvarchar](128) NOT NULL,
	[CardId] [int] NOT NULL,
 CONSTRAINT [PK_UserCardCollection] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[UserInventory]    Script Date: 3/20/2017 7:56:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UserInventory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[UserInventory](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [nvarchar](128) NOT NULL,
	[Name] [nvarchar](512) NOT NULL,
	[ImageUrl] [nvarchar](512) NULL,
 CONSTRAINT [PK_UserInventory] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[UserLuckyDrawCode]    Script Date: 3/20/2017 7:56:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UserLuckyDrawCode]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[UserLuckyDrawCode](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [nvarchar](128) NOT NULL,
	[LuckyDrawCode] [nvarchar](20) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[WeekNumber] [int] NOT NULL,
	[IsActive] [bit] NOT NULL,
 CONSTRAINT [PK_UserLuckyDrawCode] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[UserProfile]    Script Date: 3/20/2017 7:56:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UserProfile]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[UserProfile](
	[Id] [nvarchar](128) NOT NULL,
	[UserName] [nvarchar](256) NOT NULL,
	[FirstName] [nvarchar](128) NULL,
	[LastName] [nvarchar](256) NULL,
	[PhoneNumber] [nvarchar](20) NULL,
	[Address] [nvarchar](512) NULL,
	[Email] [nvarchar](256) NULL,
	[TotalPoint] [int] NULL,
 CONSTRAINT [PK_UserProfile] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[WinnerList]    Script Date: 3/20/2017 7:56:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[WinnerList]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[WinnerList](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[WeekNumber] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[PriceType] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_WinnerList] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[WinnerListUser]    Script Date: 3/20/2017 7:56:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[WinnerListUser]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[WinnerListUser](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[WinnerListId] [int] NULL,
	[UserId] [nvarchar](128) NULL,
	[FirstName] [nvarchar](128) NULL,
	[LastName] [nvarchar](256) NULL,
	[Email] [nvarchar](256) NULL,
	[Address] [nvarchar](512) NULL,
	[PhoneNumber] [nvarchar](20) NULL,
 CONSTRAINT [PK_WinnerListUser] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO

/****** Object:  Table [dbo].[CouponCode]    Script Date: 3/20/2017 7:56:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CouponCode]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[CouponCode](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Code] [nvarchar](450) NOT NULL,
	[Description] [nvarchar](512) NULL,
	[Available] [bit] NOT NULL,
	[Point] [int] NOT NULL,
	[CardId] [int] NULL,
	[RedeemByUserName] [nvarchar](256) NULL,
	[RedeemOn] [datetime] NULL,
	[ClientIpAddress] [nvarchar](128) NULL,
	[ClientInfo] [nvarchar](1024) NULL,
 CONSTRAINT [PK_CouponCode_1] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO

/****** Object:  Table [dbo].[UserActivityHistory]    Script Date: 3/20/2017 7:56:58 PM ******/
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UserActivityHistory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[UserActivityHistory](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserName] [nvarchar](256) NOT NULL,
	[CreatedAt] [datetime] NOT NULL,
	[Action] [nvarchar](256) NULL,
	[ActionDetail] [nvarchar](max) NULL,
	[ClientIpAddress] [nvarchar](128) NULL,
	[ClientInfo] [nvarchar](1024) NULL,
 CONSTRAINT [PK_UserActivityHistory] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO


IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_UserCardCollection_PokeCard]') AND parent_object_id = OBJECT_ID(N'[dbo].[UserCardCollection]'))
ALTER TABLE [dbo].[UserCardCollection]  WITH CHECK ADD  CONSTRAINT [FK_UserCardCollection_PokeCard] FOREIGN KEY([CardId])
REFERENCES [dbo].[PokeCard] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_UserCardCollection_PokeCard]') AND parent_object_id = OBJECT_ID(N'[dbo].[UserCardCollection]'))
ALTER TABLE [dbo].[UserCardCollection] CHECK CONSTRAINT [FK_UserCardCollection_PokeCard]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_UserCardCollection_UserProfile]') AND parent_object_id = OBJECT_ID(N'[dbo].[UserCardCollection]'))
ALTER TABLE [dbo].[UserCardCollection]  WITH CHECK ADD  CONSTRAINT [FK_UserCardCollection_UserProfile] FOREIGN KEY([UserId])
REFERENCES [dbo].[UserProfile] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_UserCardCollection_UserProfile]') AND parent_object_id = OBJECT_ID(N'[dbo].[UserCardCollection]'))
ALTER TABLE [dbo].[UserCardCollection] CHECK CONSTRAINT [FK_UserCardCollection_UserProfile]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_UserInventory_UserProfile]') AND parent_object_id = OBJECT_ID(N'[dbo].[UserInventory]'))
ALTER TABLE [dbo].[UserInventory]  WITH CHECK ADD  CONSTRAINT [FK_UserInventory_UserProfile] FOREIGN KEY([UserId])
REFERENCES [dbo].[UserProfile] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_UserInventory_UserProfile]') AND parent_object_id = OBJECT_ID(N'[dbo].[UserInventory]'))
ALTER TABLE [dbo].[UserInventory] CHECK CONSTRAINT [FK_UserInventory_UserProfile]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_UserLuckyDrawCode_UserProfile]') AND parent_object_id = OBJECT_ID(N'[dbo].[UserLuckyDrawCode]'))
ALTER TABLE [dbo].[UserLuckyDrawCode]  WITH CHECK ADD  CONSTRAINT [FK_UserLuckyDrawCode_UserProfile] FOREIGN KEY([UserId])
REFERENCES [dbo].[UserProfile] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_UserLuckyDrawCode_UserProfile]') AND parent_object_id = OBJECT_ID(N'[dbo].[UserLuckyDrawCode]'))
ALTER TABLE [dbo].[UserLuckyDrawCode] CHECK CONSTRAINT [FK_UserLuckyDrawCode_UserProfile]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_WinnerListUser_WinnerList]') AND parent_object_id = OBJECT_ID(N'[dbo].[WinnerListUser]'))
ALTER TABLE [dbo].[WinnerListUser]  WITH CHECK ADD  CONSTRAINT [FK_WinnerListUser_WinnerList] FOREIGN KEY([WinnerListId])
REFERENCES [dbo].[WinnerList] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_WinnerListUser_WinnerList]') AND parent_object_id = OBJECT_ID(N'[dbo].[WinnerListUser]'))
ALTER TABLE [dbo].[WinnerListUser] CHECK CONSTRAINT [FK_WinnerListUser_WinnerList]
GO

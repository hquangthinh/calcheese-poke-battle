﻿/* stored procedure for fast processing*/

IF EXISTS (SELECT * FROM  dbo.sysobjects WHERE id = object_id(N'[dbo].[spStoreGameResult]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [dbo].[spStoreGameResult]
GO
CREATE PROCEDURE spStoreGameResult
	(
		@wonUserId nvarchar(128),
		@lostUserId nvarchar(128),
		@lostPoint int,
		@lostCardId int
	)
AS
	--Declare variables
	DECLARE @ErrorCode INT
		, @TransactionCountOnEntry INT
		, @user1Point INT
		, @user2Point INT
		, @lostUserName nvarchar(255)
		, @lostCardName nvarchar(255)
		, @tradeCardMessage nvarchar(255)

	PRINT 'Start spStoreGameResult ...'

	--Initialise error handling
	SELECT @ErrorCode = @@ERROR

	--Begin Transaction Handling
	IF @ErrorCode = 0 BEGIN
		--Remember how many transaction were open at the start of the procedure
		SELECT @TransactionCountOnEntry = @@TRANCOUNT
		IF @TransactionCountOnEntry = 0 BEGIN
			--Begin a transaction
			PRINT ' spStoreGameResult Beginning Transaction'
			BEGIN TRANSACTION
		END
	END

	-- Process business logic
	PRINT 'spStoreGameResult: update points for winner and looser'

	SELECT @user1Point = TotalPoint FROM UserProfile WHERE Id = @wonUserId

	SELECT @user2Point = TotalPoint FROM UserProfile WHERE Id = @lostUserId

	UPDATE UserProfile SET TotalPoint = TotalPoint + @lostPoint WHERE Id = @wonUserId

	UPDATE UserProfile SET TotalPoint = TotalPoint - @lostPoint WHERE Id = @lostUserId

	PRINT 'spStoreGameResult: Point to adjust ' + CAST(@lostPoint as nvarchar)

	PRINT 'spStoreGameResult: Transfer card from lost player to win player'

	SELECT @lostCardName = CardName FROM PokeCard WHERE Id = @lostCardId
	
	SELECT @lostUserName = UserName FROM UserProfile WHERE Id = @lostUserId

	SET @tradeCardMessage = N'Thắng thẻ ' + @lostCardName + N' từ user ' + @lostUserName + N' được ' + cast(@lostPoint as nvarchar) + N' điểm'

	INSERT INTO UserCardCollection(UserId, CardId, CardPoint, SourceOfCard) VALUES(@wonUserId, @lostCardId, @lostPoint, @tradeCardMessage)

	DELETE TOP (1) UserCardCollection WHERE UserId = @lostUserId AND CardId = @lostCardId AND CardPoint = @lostPoint

	PRINT 'spStoreGameResult: record game history'
	
	INSERT INTO GameHistory(UserId1, UserId2, WinnerUserId, CreatedDate, User1Score, User2Score, AdjustPoint, TradeCardId)
	VALUES(@wonUserId, @lostUserId, @wonUserId, getutcdate(), @user1Point + @lostPoint , @user2Point - @lostPoint, @lostPoint, @lostCardId)

	--Complete Transaction Handling
	IF @@TRANCOUNT > @TransactionCountOnEntry BEGIN
		IF @ErrorCode = 0 BEGIN
			PRINT ' spStoreGameResult Commit Transaction'
			COMMIT TRANSACTION
		END ELSE BEGIN
			PRINT ' spStoreGameResult Rollback Transaction'
			ROLLBACK TRANSACTION
		END
	END
	
	--Return error code
	PRINT 'Complete spStoreGameResult. Return: ' + STR(@ErrorCode)
	RETURN @ErrorCode

GO


/* Store proc to disconnect all players and clear game session */
IF EXISTS (SELECT * FROM  dbo.sysobjects WHERE id = object_id(N'[dbo].[spDisconnectAllPlayers]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [dbo].[spDisconnectAllPlayers]
GO
CREATE PROCEDURE spDisconnectAllPlayers
AS
	DELETE PlayerConnectionInfo
	DELETE GameSession
GO
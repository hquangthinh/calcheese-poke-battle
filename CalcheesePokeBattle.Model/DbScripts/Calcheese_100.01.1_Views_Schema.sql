﻿/* Schema for all views*/

-- View to query online players who eligible for playing game
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vwAllOnlinePlayerConnectionInfo]'))
    DROP VIEW [dbo].[vwAllOnlinePlayerConnectionInfo]
GO
CREATE VIEW [dbo].[vwAllOnlinePlayerConnectionInfo]
AS
    SELECT conn.*, u.TotalPoint, u.FirstName, u.LastName, u.AvatarUrl
		FROM PlayerConnectionInfo conn
			INNER JOIN UserProfile u ON u.Id = conn.UserId
	WHERE u.TotalPoint >= 7 AND conn.Status = 'Online' AND DATEDIFF(HOUR, conn.ConnectedAt, GETUTCDATE()) = 0

GO

-- view query game history
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vwAllGameHistory]'))
    DROP VIEW [dbo].[vwAllGameHistory]
GO
CREATE VIEW [dbo].[vwAllGameHistory]
AS
	SELECT h.*, p1.UserName as UserName1, p2.UserName as UserName2, 'https://storagecalcheeseprod.blob.core.windows.net/images/' + c.ImageUrl as TradeCardImageUrl
		FROM GameHistory h INNER JOIN UserProfile p1  ON h.UserId1 = p1.Id 
			INNER JOIN UserProfile p2 ON h.UserId2 = p2.Id
				LEFT JOIN PokeCard c ON h.TradeCardId = c.Id
GO

-- view query user lucky draw code
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vwAllUserLuckyDrawCode]'))
    DROP VIEW [dbo].[vwAllUserLuckyDrawCode]
GO
CREATE VIEW [dbo].[vwAllUserLuckyDrawCode]
AS
	SELECT uc.*, p.UserName, p.FirstName, p.LastName, p.TotalPoint, p.PhoneNumber, p.Address 
		FROM UserLuckyDrawCode uc INNER JOIN UserProfile p on uc.UserId = p.Id
GO

-- view query used coupon code
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vwAllUsedCouponCode]'))
    DROP VIEW [dbo].[vwAllUsedCouponCode]
GO
CREATE VIEW [dbo].[vwAllUsedCouponCode]
AS
	SELECT uc.*, p.UserName, p.FirstName, p.LastName, p.TotalPoint 
		FROM UsedCouponCode uc INNER JOIN UserProfile p on uc.UserId = p.Id
GO

-- view query gift redeem history
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vwAllGiftRedeemHistory]'))
    DROP VIEW [dbo].[vwAllGiftRedeemHistory]
GO
CREATE VIEW [dbo].[vwAllGiftRedeemHistory]
AS
	SELECT uc.*, 
		p.UserName, p.FirstName, p.LastName, p.Address, p.PhoneNumber, p.Email, p.DeliveryAddress, p.SocialIdNumber, p.AvatarUrl, 
		p.AddressNumber, p.StreetName, p.WardName, p.DistrictName, p.CityName, 
		g.Name as GiftName, g.Description as GiftDescription
			FROM GiftRedeemHistory uc INNER JOIN UserProfile p on uc.UserId = p.Id
				INNER JOIN GiftItem g ON uc.GiftId = g.Id
GO

-- view AllPageViewTracking for grouping by week
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vwAllPageViewTracking]'))
    DROP VIEW [dbo].[vwAllPageViewTracking]
GO
CREATE VIEW [dbo].[vwAllPageViewTracking]
AS
	SELECT [Id]
      ,[PageTitle]
      ,[PageUrl]
	  ,[AccessDateTime]
      ,[ClientIpAddress]
      ,[ClientInfo]
      ,[UserName]
      ,[UserId]
	  , DATEADD(dd, DATEDIFF(dd, 0, AccessDateTime), 0) as NormalizedAccessDateTime
	  ,CASE 
			WHEN AccessDateTime >= '2017-04-15 00:00:00.000' AND AccessDateTime < '2017-04-24 00:00:00.000' THEN 1
			WHEN AccessDateTime >= '2017-04-24 00:00:00.000' AND AccessDateTime < '2017-05-01 00:00:00.000' THEN 2
			WHEN AccessDateTime >= '2017-05-01 00:00:00.000' AND AccessDateTime < '2017-05-08 00:00:00.000' THEN 3
			WHEN AccessDateTime >= '2017-05-08 00:00:00.000' AND AccessDateTime < '2017-05-15 00:00:00.000' THEN 4
			WHEN AccessDateTime >= '2017-05-15 00:00:00.000' AND AccessDateTime < '2017-05-22 00:00:00.000' THEN 5
			WHEN AccessDateTime >= '2017-05-22 00:00:00.000' AND AccessDateTime < '2017-05-29 00:00:00.000' THEN 6
			WHEN AccessDateTime >= '2017-05-29 00:00:00.000' AND AccessDateTime < '2017-06-05 00:00:00.000' THEN 7
			WHEN AccessDateTime >= '2017-06-05 00:00:00.000' AND AccessDateTime < '2017-06-12 00:00:00.000' THEN 8
			WHEN AccessDateTime >= '2017-06-12 00:00:00.000' AND AccessDateTime < '2017-06-19 00:00:00.000' THEN 9
			WHEN AccessDateTime >= '2017-06-19 00:00:00.000' AND AccessDateTime < '2017-06-26 00:00:00.000' THEN 10
			WHEN AccessDateTime >= '2017-06-26 00:00:00.000' AND AccessDateTime < '2017-07-03 00:00:00.000' THEN 11
			WHEN AccessDateTime >= '2017-07-03 00:00:00.000' AND AccessDateTime < '2017-07-10 00:00:00.000' THEN 12
			WHEN AccessDateTime >= '2017-07-10 00:00:00.000' AND AccessDateTime < '2017-07-14 00:00:00.000' THEN 13
			ELSE 0
	   END AS WeekNumber
	FROM [PageViewTracking]	
GO

IF COL_LENGTH('dbo.GiftItem','Description') IS  NULL
BEGIN
ALTER TABLE dbo.GiftItem ADD [Description] VARCHAR(100) NULL
END


IF COL_LENGTH('dbo.GiftItem','IsRedeem') IS  NULL
BEGIN
ALTER TABLE dbo.GiftItem ADD IsRedeem bit NOT NULL DEFAULT(1) 
END


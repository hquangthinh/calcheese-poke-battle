﻿/*Script to insert seed data*/
USE [CalcheeseDevIdentity]
GO

--Roles

INSERT [dbo].[AspNetRoles] ([Id], [Name], [Discriminator]) VALUES (N'00efbab4-437b-4d74-af53-bfd1f567ba76', N'Administrator', N'ApplicationRole')
GO
INSERT [dbo].[AspNetRoles] ([Id], [Name], [Discriminator]) VALUES (N'eda5fa6f-903d-43a6-91e1-8c5639d1f455', N'NormalUser', N'ApplicationRole')
GO
INSERT [dbo].[AspNetRoles] ([Id], [Name], [Discriminator]) VALUES (N'9fedf78d-f4bc-43bb-96fe-9d3581caeae6', N'LuckyDrawManager', N'ApplicationRole')
GO
INSERT [dbo].[AspNetRoles] ([Id], [Name], [Discriminator]) VALUES (N'aea56957-0406-4fef-84c9-b908ad01892c', N'TestUser', N'ApplicationRole')
GO
--Users

INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'29e2ecdf-ac39-4d29-bdc4-e6c834e90497', N'admin@dev.io', 0, N'AIzwb2eeOREbvNM8KdJgJ0SLw8Yv0KkE1fAsWKKdixQ8qfcUq8nh4+UqVTl/x9BP1w==', N'3d34abe3-d8d7-40db-905d-a44a67c2cb56', NULL, 0, 0, NULL, 0, 0, N'admin')
GO
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'ae0ab5cd-2e2e-4459-99a1-c223d4d4834b', N'admin1@ex.com', 0, N'AE9caQEqPL2LyfjblHMWR4XRWLu+tOGhVBDId3OAf5AcsiXV1p0xiJp63lj80atFNw==', N'7dd0a146-8f11-42fb-b702-e3c4fc0c7728', NULL, 0, 0, NULL, 0, 0, N'admin1')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'29e2ecdf-ac39-4d29-bdc4-e6c834e90497', N'00efbab4-437b-4d74-af53-bfd1f567ba76')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'ae0ab5cd-2e2e-4459-99a1-c223d4d4834b', N'eda5fa6f-903d-43a6-91e1-8c5639d1f455')
GO
INSERT [dbo].[AspNetUserLogins] ([LoginProvider], [ProviderKey], [UserId]) VALUES (N'Application', N'29e2ecdf-ac39-4d29-bdc4-e6c834e90497', N'29e2ecdf-ac39-4d29-bdc4-e6c834e90497')
GO
INSERT [dbo].[AspNetUserLogins] ([LoginProvider], [ProviderKey], [UserId]) VALUES (N'Application', N'ae0ab5cd-2e2e-4459-99a1-c223d4d4834b', N'ae0ab5cd-2e2e-4459-99a1-c223d4d4834b')
GO


IF NOT EXISTS(SELECT 1 FROM Setting WHERE SettingKey='WebAppImagesBaseUrl')
	INSERT INTO Setting(SettingKey, SettingValueString)
		VALUES('WebAppImagesBaseUrl', 'https://storagecalcheeseprod.blob.core.windows.net/images')
GO
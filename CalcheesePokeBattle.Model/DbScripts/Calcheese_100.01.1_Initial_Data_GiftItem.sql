﻿-- Begin insert data for Gift Item

USE [CalcheeseProdDb]
GO
SET IDENTITY_INSERT [dbo].[GiftItem] ON 

GO
INSERT [dbo].[GiftItem] ([Id], [Name], [ImageUrl], [RedeemPoint], [Description], [IsRedeem]) VALUES (1, N'Balo', N'ba-lo.png', 100, N'Ba lo Pokemon', 1)
GO
INSERT [dbo].[GiftItem] ([Id], [Name], [ImageUrl], [RedeemPoint], [Description], [IsRedeem]) VALUES (2, N'AoThun', N'ao.png', 80, N'Áo Thun Pokemon', 1)
GO
INSERT [dbo].[GiftItem] ([Id], [Name], [ImageUrl], [RedeemPoint], [Description], [IsRedeem]) VALUES (3, N'Mu', N'non.png', 60, N'Mũ Pokemon', 1)
GO
INSERT [dbo].[GiftItem] ([Id], [Name], [ImageUrl], [RedeemPoint], [Description], [IsRedeem]) VALUES (4, N'TraiHe', N'hoc-bong.png', 99999999, N'Trại hè', 0)
GO
INSERT [dbo].[GiftItem] ([Id], [Name], [ImageUrl], [RedeemPoint], [Description], [IsRedeem]) VALUES (5, N'VinID', N'xe-dap.png', 99999999, N'Thẻ quà tặng VinID để mua xe đạp', 0)
GO
INSERT [dbo].[GiftItem] ([Id], [Name], [ImageUrl], [RedeemPoint], [Description], [IsRedeem]) VALUES (6, N'XeDien', N'xe-dien.png', 99999999, N'Xe điện cân bằng', 0)
GO
INSERT [dbo].[GiftItem] ([Id], [Name], [ImageUrl], [RedeemPoint], [Description], [IsRedeem]) VALUES (7, N'DoChoi', N'bo-do-choi.png', 99999999, N'Bộ đồ chơi Pokemon', 0)
GO
SET IDENTITY_INSERT [dbo].[GiftItem] OFF
GO


-- End insert data for Gift Item

﻿/* Schema for all reports view*/


-- All table records count
SELECT 
    [TableName] = so.name, 
    [RowCount] = MAX(si.rows)
FROM 
    sysobjects so, 
    sysindexes si 
WHERE 
    so.xtype = 'U' 
    AND 
    si.id = OBJECT_ID(so.name) 
GROUP BY 
    so.name 
ORDER BY 
    2 DESC

-----------------
select uc.*,p.UserName 
	from UsedCouponCode uc Inner Join UserProfile p on uc.UserId = p.Id
	order by uc.Id desc

-----------------
select uc.*,p.UserName 
	from UserLuckyDrawCode uc Inner Join UserProfile p on uc.UserId = p.Id
	order by uc.Id desc


-- User registration by date
select DATEADD(dd, DATEDIFF(dd, 0, CreatedDate), 0) as UserCreatedDate , count(*) as TotalRegistration from UserProfile
group by DATEADD(dd, DATEDIFF(dd, 0, CreatedDate), 0)
order by UserCreatedDate asc

--User registration by date with filter user has claimed coupon code
select DATEADD(dd, DATEDIFF(dd, 0, p.CreatedDate), 0) as UserCreatedDate , count(*) as TotalRegistration from UserProfile p
	--inner join UsedCouponCode uc on p.Id = uc.UserId
where p.Id in (select UserId from UsedCouponCode)
group by DATEADD(dd, DATEDIFF(dd, 0, p.CreatedDate), 0)
order by UserCreatedDate asc

-- access page view
select PageTitle, DATEADD(dd, DATEDIFF(dd, 0, AccessDateTime), 0) as AccessDateTime, UserName
from PageViewTracking


select PageTitle, DATEADD(dd, DATEDIFF(dd, 0, AccessDateTime), 0) as AccessDateTime, UserName
from PageViewTracking
where PageTitle = 'tham-gia-san-choi'
order by PageTitle, AccessDateTime

-- user card release by date
select DATEADD(dd, DATEDIFF(dd, 0, r.CreatedDate), 0) as CreatedDate , count(*) as TotalInputCard
from UsedCouponCodeReport r
group by DATEADD(dd, DATEDIFF(dd, 0, r.CreatedDate), 0)
order by CreatedDate desc
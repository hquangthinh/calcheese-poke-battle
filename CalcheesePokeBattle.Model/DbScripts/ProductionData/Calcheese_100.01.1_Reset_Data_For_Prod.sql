﻿/* Reset all test data prepare for production live */

Update UserProfile Set TotalPoint = 0
GO

-- Begin Lucky draw code
Truncate Table LuckyDrawPoolWeek1
GO

Truncate Table LuckyDrawPoolWeek2
GO

Truncate Table LuckyDrawPoolWeek3
GO

Truncate Table LuckyDrawPoolWeek4
GO

Truncate Table LuckyDrawPoolWeek5
GO

Truncate Table LuckyDrawPoolWeek6
GO

Truncate Table LuckyDrawPoolWeek7
GO

Truncate Table LuckyDrawPoolWeek8
GO

Truncate Table LuckyDrawPoolWeek9
GO

Truncate Table LuckyDrawPoolWeek10
GO

Truncate Table LuckyDrawPoolWeek11
GO

Truncate Table LuckyDrawPoolWeek12
GO

Truncate Table LuckyDrawPoolWeek13
GO

Truncate Table UserLuckyDrawCode
GO

-- End Lucky draw code
Truncate Table [PrizeLuckyDrawHistory]
GO

Truncate Table [GiftRedeemHistory]
GO

Truncate Table [GiftWonFromLuckyDraw]
GO

Truncate Table [GameHistory]
GO

Truncate Table [GameSession]
GO

Truncate Table [PlayerConnectionInfo]
GO

Truncate Table [UsedCouponCode]
GO

Delete [UserActivityHistory]
GO

Delete [UserCardCollection]
GO

Delete [UserInventory]
GO

Delete [UserLuckyDrawCode]
GO

Delete [WinnerListUser]
GO

Delete [WinnerList]
GO


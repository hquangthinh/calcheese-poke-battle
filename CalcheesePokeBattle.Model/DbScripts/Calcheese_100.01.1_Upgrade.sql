﻿/* Insert database upgrade script here */

--Schema for table UsedCouponCode
IF NOT EXISTS (SELECT * FROM sysobjects WHERE ID = OBJECT_ID('[UsedCouponCode]') AND OBJECTPROPERTY(ID, 'IsTable') = 1)
	CREATE TABLE [dbo].[UsedCouponCode](
		[Id] [int] IDENTITY(1,1) NOT NULL,
		[CouponCode] [nvarchar](50) NOT NULL,
		[CreatedDate] [datetime] NOT NULL,
		[UserId] [nvarchar](128) NOT NULL,
		[ClientIpAddress] [nvarchar](128) NULL,
		[ClientInfo] [nvarchar](1024) NULL,
	 CONSTRAINT [PK_UsedCouponCode] PRIMARY KEY CLUSTERED 
	(
		[Id] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]
GO

-- Index for CouponCode of [UsedCouponCode], max length of indexed key is 900 bytes
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[UsedCouponCode]') AND name = N'UsedCouponCodeIndex')
	CREATE UNIQUE NONCLUSTERED INDEX [UsedCouponCodeIndex] ON [dbo].[UsedCouponCode]
	(
		[CouponCode] ASC
	)WITH 
		(	PAD_INDEX = OFF, 
			STATISTICS_NORECOMPUTE = OFF, 
			SORT_IN_TEMPDB = OFF, 
			IGNORE_DUP_KEY = OFF, 
			DROP_EXISTING = OFF, 
			ONLINE = OFF, 
			ALLOW_ROW_LOCKS = ON, 
			ALLOW_PAGE_LOCKS = ON
		)	
GO

--Schema for table GameSession
IF NOT EXISTS (SELECT * FROM sysobjects WHERE ID = OBJECT_ID('[GameSession]') AND OBJECTPROPERTY(ID, 'IsTable') = 1)
	CREATE TABLE [dbo].[GameSession](
		[Id] [int] IDENTITY(1,1) NOT NULL,
		[PlayerUserId1] [nvarchar](128) NOT NULL,
		[PlayerUserName1] [nvarchar](256) NOT NULL,
		[PlayerUserId2] [nvarchar](128) NOT NULL,
		[PlayerUserName2] [nvarchar](256) NOT NULL,
		[CreatedDate] [datetime] NOT NULL,
	 CONSTRAINT [PK_GameSession] PRIMARY KEY CLUSTERED 
	(
		[Id] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]

GO


--Schema for table WeekDefinition
IF NOT EXISTS (SELECT * FROM sysobjects WHERE ID = OBJECT_ID('[WeekDefinition]') AND OBJECTPROPERTY(ID, 'IsTable') = 1)
	CREATE TABLE [dbo].[WeekDefinition](
		[Id] [int] IDENTITY(1,1) NOT NULL,
		[WeekNumber] [int] NOT NULL,
		[WeekName] [nvarchar](128) NOT NULL,
		[WeekStart] [datetime] NOT NULL,
		[WeekEnd] [datetime] NOT NULL,
		 CONSTRAINT [PK_WeekDefinition] PRIMARY KEY CLUSTERED 
		(
			[Id] ASC
		)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
		) ON [PRIMARY]

GO

--Schema for table GiftAmountPerWeek
IF NOT EXISTS (SELECT * FROM sysobjects WHERE ID = OBJECT_ID('[GiftAmountPerWeek]') AND OBJECTPROPERTY(ID, 'IsTable') = 1)
	CREATE TABLE [dbo].[GiftAmountPerWeek](
		[Id] [int] IDENTITY(1,1) NOT NULL,
		[WeekNumber] [int] NOT NULL,
		[GiftName] [nvarchar](255) NOT NULL,
		[TotalPrize] [int] NOT NULL,
		 CONSTRAINT [PK_GiftAmountPerWeek] PRIMARY KEY CLUSTERED 
		(
			[Id] ASC
		)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
		) ON [PRIMARY]

GO

--Schema for table GiftRedeemHistory
IF NOT EXISTS (SELECT * FROM sysobjects WHERE ID = OBJECT_ID('[GiftRedeemHistory]') AND OBJECTPROPERTY(ID, 'IsTable') = 1)
	CREATE TABLE [dbo].[GiftRedeemHistory](
		[Id] [int] IDENTITY(1,1) NOT NULL,
		[WeekNumber] [int] NOT NULL,
		[GiftId] [int] NOT NULL,
		[UserId] [nvarchar](128) NOT NULL,
		[RedeemOn] [datetime] NOT NULL,
	 CONSTRAINT [PK_GiftRedeemHistory] PRIMARY KEY CLUSTERED 
	(
		[Id] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]

GO

--Schema for table GiftWonFromLuckyDraw
IF NOT EXISTS (SELECT * FROM sysobjects WHERE ID = OBJECT_ID('[GiftWonFromLuckyDraw]') AND OBJECTPROPERTY(ID, 'IsTable') = 1)
	CREATE TABLE [dbo].[GiftWonFromLuckyDraw](
		[Id] [int] IDENTITY(1,1) NOT NULL,
		[WeekNumber] [int] NOT NULL,
		[GiftId] [int] NOT NULL,
		[UserId] [nvarchar](128) NOT NULL,
		[CreatedAt] [datetime] NOT NULL,
	 CONSTRAINT [PK_GiftWonFromLuckyDraw] PRIMARY KEY CLUSTERED 
	(
		[Id] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]	

GO

--Schema for table LuckyDrawPoolWeek1
IF NOT EXISTS (SELECT * FROM sysobjects WHERE ID = OBJECT_ID('[LuckyDrawPoolWeek1]') AND OBJECTPROPERTY(ID, 'IsTable') = 1)
	CREATE TABLE [dbo].[LuckyDrawPoolWeek1](
		[Id] [int] IDENTITY(1,1) NOT NULL,
		[WeekNumber] [int] NOT NULL,
		[CreatedAt] [datetime] NOT NULL,
		[CreatedByUserId] [nvarchar](128) NOT NULL,
		[CreatedByUserName] [nvarchar](256) NOT NULL,
	 CONSTRAINT [PK_LuckyDrawPoolWeek1] PRIMARY KEY CLUSTERED 
	(
		[Id] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]	

GO

--Schema for table LuckyDrawPoolWeek2
IF NOT EXISTS (SELECT * FROM sysobjects WHERE ID = OBJECT_ID('[LuckyDrawPoolWeek2]') AND OBJECTPROPERTY(ID, 'IsTable') = 1)
	CREATE TABLE [dbo].[LuckyDrawPoolWeek2](
		[Id] [int] IDENTITY(1,1) NOT NULL,
		[WeekNumber] [int] NOT NULL,
		[CreatedAt] [datetime] NOT NULL,
		[CreatedByUserId] [nvarchar](128) NOT NULL,
		[CreatedByUserName] [nvarchar](256) NOT NULL,
	 CONSTRAINT [PK_LuckyDrawPoolWeek2] PRIMARY KEY CLUSTERED 
	(
		[Id] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]	

GO

--Schema for table LuckyDrawPoolWeek3
IF NOT EXISTS (SELECT * FROM sysobjects WHERE ID = OBJECT_ID('[LuckyDrawPoolWeek3]') AND OBJECTPROPERTY(ID, 'IsTable') = 1)
	CREATE TABLE [dbo].[LuckyDrawPoolWeek3](
		[Id] [int] IDENTITY(1,1) NOT NULL,
		[WeekNumber] [int] NOT NULL,
		[CreatedAt] [datetime] NOT NULL,
		[CreatedByUserId] [nvarchar](128) NOT NULL,
		[CreatedByUserName] [nvarchar](256) NOT NULL,
	 CONSTRAINT [PK_LuckyDrawPoolWeek3] PRIMARY KEY CLUSTERED 
	(
		[Id] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]	

GO

--Schema for table LuckyDrawPoolWeek4
IF NOT EXISTS (SELECT * FROM sysobjects WHERE ID = OBJECT_ID('[LuckyDrawPoolWeek4]') AND OBJECTPROPERTY(ID, 'IsTable') = 1)
	CREATE TABLE [dbo].[LuckyDrawPoolWeek4](
		[Id] [int] IDENTITY(1,1) NOT NULL,
		[WeekNumber] [int] NOT NULL,
		[CreatedAt] [datetime] NOT NULL,
		[CreatedByUserId] [nvarchar](128) NOT NULL,
		[CreatedByUserName] [nvarchar](256) NOT NULL,
	 CONSTRAINT [PK_LuckyDrawPoolWeek4] PRIMARY KEY CLUSTERED 
	(
		[Id] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]	

GO

--Schema for table LuckyDrawPoolWeek5
IF NOT EXISTS (SELECT * FROM sysobjects WHERE ID = OBJECT_ID('[LuckyDrawPoolWeek5]') AND OBJECTPROPERTY(ID, 'IsTable') = 1)
	CREATE TABLE [dbo].[LuckyDrawPoolWeek5](
		[Id] [int] IDENTITY(1,1) NOT NULL,
		[WeekNumber] [int] NOT NULL,
		[CreatedAt] [datetime] NOT NULL,
		[CreatedByUserId] [nvarchar](128) NOT NULL,
		[CreatedByUserName] [nvarchar](256) NOT NULL,
	 CONSTRAINT [PK_LuckyDrawPoolWeek5] PRIMARY KEY CLUSTERED 
	(
		[Id] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]	

GO

--Schema for table LuckyDrawPoolWeek6
IF NOT EXISTS (SELECT * FROM sysobjects WHERE ID = OBJECT_ID('[LuckyDrawPoolWeek6]') AND OBJECTPROPERTY(ID, 'IsTable') = 1)
	CREATE TABLE [dbo].[LuckyDrawPoolWeek6](
		[Id] [int] IDENTITY(1,1) NOT NULL,
		[WeekNumber] [int] NOT NULL,
		[CreatedAt] [datetime] NOT NULL,
		[CreatedByUserId] [nvarchar](128) NOT NULL,
		[CreatedByUserName] [nvarchar](256) NOT NULL,
	 CONSTRAINT [PK_LuckyDrawPoolWeek6] PRIMARY KEY CLUSTERED 
	(
		[Id] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]	

GO

--Schema for table LuckyDrawPoolWeek7
IF NOT EXISTS (SELECT * FROM sysobjects WHERE ID = OBJECT_ID('[LuckyDrawPoolWeek7]') AND OBJECTPROPERTY(ID, 'IsTable') = 1)
	CREATE TABLE [dbo].[LuckyDrawPoolWeek7](
		[Id] [int] IDENTITY(1,1) NOT NULL,
		[WeekNumber] [int] NOT NULL,
		[CreatedAt] [datetime] NOT NULL,
		[CreatedByUserId] [nvarchar](128) NOT NULL,
		[CreatedByUserName] [nvarchar](256) NOT NULL,
	 CONSTRAINT [PK_LuckyDrawPoolWeek7] PRIMARY KEY CLUSTERED 
	(
		[Id] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]	

GO

--Schema for table LuckyDrawPoolWeek8
IF NOT EXISTS (SELECT * FROM sysobjects WHERE ID = OBJECT_ID('[LuckyDrawPoolWeek8]') AND OBJECTPROPERTY(ID, 'IsTable') = 1)
	CREATE TABLE [dbo].[LuckyDrawPoolWeek8](
		[Id] [int] IDENTITY(1,1) NOT NULL,
		[WeekNumber] [int] NOT NULL,
		[CreatedAt] [datetime] NOT NULL,
		[CreatedByUserId] [nvarchar](128) NOT NULL,
		[CreatedByUserName] [nvarchar](256) NOT NULL,
	 CONSTRAINT [PK_LuckyDrawPoolWeek8] PRIMARY KEY CLUSTERED 
	(
		[Id] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]	

GO

--Schema for table LuckyDrawPoolWeek9
IF NOT EXISTS (SELECT * FROM sysobjects WHERE ID = OBJECT_ID('[LuckyDrawPoolWeek9]') AND OBJECTPROPERTY(ID, 'IsTable') = 1)
	CREATE TABLE [dbo].[LuckyDrawPoolWeek9](
		[Id] [int] IDENTITY(1,1) NOT NULL,
		[WeekNumber] [int] NOT NULL,
		[CreatedAt] [datetime] NOT NULL,
		[CreatedByUserId] [nvarchar](128) NOT NULL,
		[CreatedByUserName] [nvarchar](256) NOT NULL,
	 CONSTRAINT [PK_LuckyDrawPoolWeek9] PRIMARY KEY CLUSTERED 
	(
		[Id] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]	

GO

--Schema for table LuckyDrawPoolWeek10
IF NOT EXISTS (SELECT * FROM sysobjects WHERE ID = OBJECT_ID('[LuckyDrawPoolWeek10]') AND OBJECTPROPERTY(ID, 'IsTable') = 1)
	CREATE TABLE [dbo].[LuckyDrawPoolWeek10](
		[Id] [int] IDENTITY(1,1) NOT NULL,
		[WeekNumber] [int] NOT NULL,
		[CreatedAt] [datetime] NOT NULL,
		[CreatedByUserId] [nvarchar](128) NOT NULL,
		[CreatedByUserName] [nvarchar](256) NOT NULL,
	 CONSTRAINT [PK_LuckyDrawPoolWeek10] PRIMARY KEY CLUSTERED 
	(
		[Id] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]	

GO

--Schema for table LuckyDrawPoolWeek11
IF NOT EXISTS (SELECT * FROM sysobjects WHERE ID = OBJECT_ID('[LuckyDrawPoolWeek11]') AND OBJECTPROPERTY(ID, 'IsTable') = 1)
	CREATE TABLE [dbo].[LuckyDrawPoolWeek11](
		[Id] [int] IDENTITY(1,1) NOT NULL,
		[WeekNumber] [int] NOT NULL,
		[CreatedAt] [datetime] NOT NULL,
		[CreatedByUserId] [nvarchar](128) NOT NULL,
		[CreatedByUserName] [nvarchar](256) NOT NULL,
	 CONSTRAINT [PK_LuckyDrawPoolWeek11] PRIMARY KEY CLUSTERED 
	(
		[Id] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]	

GO

--Schema for table LuckyDrawPoolWeek12
IF NOT EXISTS (SELECT * FROM sysobjects WHERE ID = OBJECT_ID('[LuckyDrawPoolWeek12]') AND OBJECTPROPERTY(ID, 'IsTable') = 1)
	CREATE TABLE [dbo].[LuckyDrawPoolWeek12](
		[Id] [int] IDENTITY(1,1) NOT NULL,
		[WeekNumber] [int] NOT NULL,
		[CreatedAt] [datetime] NOT NULL,
		[CreatedByUserId] [nvarchar](128) NOT NULL,
		[CreatedByUserName] [nvarchar](256) NOT NULL,
	 CONSTRAINT [PK_LuckyDrawPoolWeek12] PRIMARY KEY CLUSTERED 
	(
		[Id] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]	

GO

--Schema for table LuckyDrawPoolWeek13
IF NOT EXISTS (SELECT * FROM sysobjects WHERE ID = OBJECT_ID('[LuckyDrawPoolWeek13]') AND OBJECTPROPERTY(ID, 'IsTable') = 1)
	CREATE TABLE [dbo].[LuckyDrawPoolWeek13](
		[Id] [int] IDENTITY(1,1) NOT NULL,
		[WeekNumber] [int] NOT NULL,
		[CreatedAt] [datetime] NOT NULL,
		[CreatedByUserId] [nvarchar](128) NOT NULL,
		[CreatedByUserName] [nvarchar](256) NOT NULL,
	 CONSTRAINT [PK_LuckyDrawPoolWeek13] PRIMARY KEY CLUSTERED 
	(
		[Id] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]	

GO

--Schema for table PlayerConnectionInfo
IF NOT EXISTS (SELECT * FROM sysobjects WHERE ID = OBJECT_ID('[PlayerConnectionInfo]') AND OBJECTPROPERTY(ID, 'IsTable') = 1)
	CREATE TABLE [dbo].[PlayerConnectionInfo](
		[Id] [int] IDENTITY(1,1) NOT NULL,
		[ServerName] [nvarchar](255) NOT NULL,
		[ServerIpAddress] [nvarchar](50) NOT NULL,
		[UserId] [nvarchar](128) NOT NULL,
		[UserName] [nvarchar](255) NOT NULL,
		[ConnectionId] [nvarchar](255) NOT NULL,
		[ConnectedAt] [datetime] NOT NULL,
		[DeviceInfo] [nvarchar](255) NULL,
		[Status] [nvarchar](50) NOT NULL,
		[ClientIpAddress] [nvarchar](50) NOT NULL,
	 CONSTRAINT [PK_PlayerConnectionInfo] PRIMARY KEY CLUSTERED 
	(
		[Id] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]

GO

-- Index for Code of [CouponCode], max length of indexed key is 900 bytes
IF  NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[CouponCode]') AND name = N'CouponCodeIndex')
	CREATE UNIQUE NONCLUSTERED INDEX [CouponCodeIndex] ON [dbo].[CouponCode]
	(
		[Code] ASC
	)WITH 
		(	PAD_INDEX = OFF, 
			STATISTICS_NORECOMPUTE = OFF, 
			SORT_IN_TEMPDB = OFF, 
			IGNORE_DUP_KEY = OFF, 
			DROP_EXISTING = OFF, 
			ONLINE = OFF, 
			ALLOW_ROW_LOCKS = ON, 
			ALLOW_PAGE_LOCKS = ON
		)	
GO


-- Add columns for table PokeCard
-- Category: Pokémon Chuột, Pokémon Ăngten, ...
IF NOT EXISTS (SELECT * FROM syscolumns WHERE id = object_id('dbo.PokeCard') and name = 'Category')
  ALTER TABLE [PokeCard]
      ADD Category NVARCHAR(255) NULL
GO

-- Height: 0.4m
IF NOT EXISTS (SELECT * FROM syscolumns WHERE id = object_id('dbo.PokeCard') and name = 'Height')
  ALTER TABLE [PokeCard]
      ADD Height NVARCHAR(50) NULL
GO

-- Weight: 6.0kg
IF NOT EXISTS (SELECT * FROM syscolumns WHERE id = object_id('dbo.PokeCard') and name = 'Weight')
  ALTER TABLE [PokeCard]
      ADD [Weight] NVARCHAR(50) NULL
GO

-- Reserve column for now
IF NOT EXISTS (SELECT * FROM syscolumns WHERE id = object_id('dbo.PokeCard') and name = 'Skill1')
  ALTER TABLE [PokeCard]
      ADD Skill1 NVARCHAR(255) NULL
GO

-- Reserve column for now
IF NOT EXISTS (SELECT * FROM syscolumns WHERE id = object_id('dbo.PokeCard') and name = 'Skill2')
  ALTER TABLE [PokeCard]
      ADD Skill2 NVARCHAR(255) NULL
GO

-- Reserve column for now
IF NOT EXISTS (SELECT * FROM syscolumns WHERE id = object_id('dbo.PokeCard') and name = 'Skill3')
  ALTER TABLE [PokeCard]
      ADD Skill3 NVARCHAR(255) NULL
GO

-- Tuyệt chiêu: Điện 100 ngàn vôn,...
IF NOT EXISTS (SELECT * FROM syscolumns WHERE id = object_id('dbo.PokeCard') and name = 'UltimateSkill')
  ALTER TABLE [PokeCard]
      ADD UltimateSkill NVARCHAR(255) NULL
GO

-- Evolution : refer to Id of poke card which this card evolve to
IF NOT EXISTS (SELECT * FROM syscolumns WHERE id = object_id('dbo.PokeCard') and name = 'EvolutionTo')
  ALTER TABLE [PokeCard]
      ADD EvolutionTo INT NULL
GO

-- URL to card image back side
IF NOT EXISTS (SELECT * FROM syscolumns WHERE id = object_id('dbo.PokeCard') and name = 'BackSideImageUrl')
  ALTER TABLE [PokeCard]
      ADD BackSideImageUrl NVARCHAR(512) NULL
GO

-- CardName: Pikachu,...
IF NOT EXISTS (SELECT * FROM syscolumns WHERE id = object_id('dbo.PokeCard') and name = 'CardName')
  ALTER TABLE [PokeCard]
      ADD CardName NVARCHAR(255) NULL
GO

-- CardGroup: 1,2,3...
IF NOT EXISTS (SELECT * FROM syscolumns WHERE id = object_id('dbo.PokeCard') and name = 'CardGroup')
  ALTER TABLE [PokeCard]
      ADD CardGroup INT NULL
GO

-- Add columns to UserProfile table
IF NOT EXISTS (SELECT * FROM syscolumns WHERE id = object_id('dbo.UserProfile') and name = 'DeliveryAddress')
  ALTER TABLE [UserProfile]
      ADD DeliveryAddress NVARCHAR(512) NULL
GO

IF NOT EXISTS (SELECT * FROM syscolumns WHERE id = object_id('dbo.UserProfile') and name = 'SocialIdNumber')
  ALTER TABLE [UserProfile]
      ADD SocialIdNumber NVARCHAR(20) NULL
GO

IF NOT EXISTS (SELECT * FROM syscolumns WHERE id = object_id('dbo.UserProfile') and name = 'AvatarUrl')
  ALTER TABLE [UserProfile]
      ADD AvatarUrl NVARCHAR(255) NULL
GO

--PrizeLuckyDrawHistory schema
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PrizeLuckyDrawHistory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[PrizeLuckyDrawHistory](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[WeekNumber] [int] NOT NULL,
	[LuckyDrawCode] [nvarchar](20) NULL,
	[GiftItemID] [int] NOT NULL,
	[CreatedDate] datetime NOT Null
		
 CONSTRAINT [PK_PrizeLuckyDrawHistory] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO

IF NOT EXISTS (SELECT * FROM syscolumns WHERE id = object_id('dbo.WinnerListUser') and name = 'LuckyDrawCode')
  ALTER TABLE [WinnerListUser]
      ADD LuckyDrawCode [nvarchar](20) NULL
GO

IF NOT EXISTS (SELECT * FROM syscolumns WHERE id = object_id('dbo.WinnerList') and name = 'GiftItemID')
  ALTER TABLE WinnerList ADD GiftItemID int
GO

-- Add columns for GiftItem table
IF NOT EXISTS (SELECT * FROM syscolumns WHERE id = object_id('dbo.GiftItem') and name = 'Description')
  ALTER TABLE GiftItem ADD [Description] NVARCHAR(512) NULL
GO

IF NOT EXISTS (SELECT * FROM syscolumns WHERE id = object_id('dbo.GiftItem') and name = 'IsRedeem')
  ALTER TABLE GiftItem ADD IsRedeem bit NOT NULL default(1)
GO

-- FK for table PrizeLuckyDrawHistory to GiftItem
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_PrizeLuckyDrawHistory_GiftItem]') AND parent_object_id = OBJECT_ID(N'[dbo].[PrizeLuckyDrawHistory]'))
	ALTER TABLE [dbo].[PrizeLuckyDrawHistory]  WITH CHECK ADD  CONSTRAINT [FK_PrizeLuckyDrawHistory_GiftItem] FOREIGN KEY([GiftItemID])
		REFERENCES [dbo].[GiftItem] ([Id])
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_PrizeLuckyDrawHistory_GiftItem]') AND parent_object_id = OBJECT_ID(N'[dbo].[PrizeLuckyDrawHistory]'))
	ALTER TABLE [dbo].[PrizeLuckyDrawHistory] CHECK CONSTRAINT [FK_PrizeLuckyDrawHistory_GiftItem]
GO

IF NOT EXISTS (SELECT * FROM syscolumns WHERE id = object_id('dbo.UsedCouponCode') and name = 'CodePoint')
  ALTER TABLE UsedCouponCode ADD [CodePoint] INT NULL
GO

UPDATE [GiftItem] SET [ImageUrl] = 'non.png', IsRedeem = 1, RedeemPoint = 60, [Description] = N'Nón huấn luyện viên' WHERE [Name] = 'Mu' 
UPDATE [GiftItem] SET [ImageUrl] = 'ao.png', IsRedeem = 1, RedeemPoint = 80, [Description] = N'Áo thun Pokemon' WHERE [Name] = 'AoThun' 
UPDATE [GiftItem] SET [ImageUrl] = 'ba-lo.png', IsRedeem = 1, RedeemPoint = 100, [Description] = N'Balo Pokemon' WHERE [Name] = 'Balo' 
UPDATE [GiftItem] SET [ImageUrl] = 'hoc-bong.png', IsRedeem = 0, [Description] = N'Học bổng trại hè quốc tế' WHERE [Name] = 'TraiHe' 
UPDATE [GiftItem] SET [ImageUrl] = 'xe-dap.png', IsRedeem = 0, [Description] = N'Xe đạp thể thao' WHERE [Name] = 'VinID'  OR [Name] = 'XeDap' 
UPDATE [GiftItem] SET [ImageUrl] = 'xe-dien.png', IsRedeem = 0, [Description] = N'Xe điện cân bằng' WHERE [Name] = 'XeDien' 
UPDATE [GiftItem] SET [ImageUrl] = 'bo-do-choi.png', IsRedeem = 0, [Description] = N'Bộ đồ chơi Pokemon' WHERE [Name] = 'DoChoi' 
GO


--Schema for table UploadFile 
IF NOT EXISTS (SELECT * FROM sysobjects WHERE ID = OBJECT_ID('[UploadFile]') AND OBJECTPROPERTY(ID, 'IsTable') = 1)
	CREATE TABLE [dbo].[UploadFile](
		[Id] [int] IDENTITY(1,1) NOT NULL,
		[PokeCardID] integer NOT NULL,
		[CreatedDate] [datetime] NOT NULL,
		[FileName] [nvarchar](200) NOT NULL,
	 CONSTRAINT [PK_UploadFile] PRIMARY KEY CLUSTERED 
	(
		[Id] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]
GO

IF NOT EXISTS (SELECT * FROM syscolumns WHERE id = object_id('dbo.WinnerListUser') and name = 'UserName')
  ALTER TABLE WinnerListUser ADD [UserName] nvarchar(256) NULL
GO

IF NOT EXISTS (SELECT * FROM syscolumns WHERE id = object_id('dbo.UploadFile') and name = 'ImportStatus')
  ALTER TABLE UploadFile ADD ImportStatus nvarchar(50) NULL
GO

--Schema for control user access and job status
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ImportJobStatus]') AND type in (N'U'))
BEGIN
	CREATE TABLE [dbo].[ImportJobStatus](
		[Id] [int] IDENTITY(1,1) NOT NULL,
		[JobName] [nvarchar](50) NOT NULL,
		[CreatedDate] [datetime] NOT NULL,
	 CONSTRAINT [PK_ImportJobStatus] PRIMARY KEY CLUSTERED 
	(
		[Id] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]

END
GO


IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UserAccessControl]') AND type in (N'U'))
BEGIN
	CREATE TABLE [dbo].[UserAccessControl](
		[Id] [int] IDENTITY(1,1) NOT NULL,
		[UserName] [nvarchar](128) NOT NULL,
		[IpAddress] [nvarchar](128) NOT NULL,
	 CONSTRAINT [PK_UserAccessControl] PRIMARY KEY CLUSTERED 
	(
		[Id] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]

END
GO

IF NOT EXISTS (SELECT * FROM syscolumns WHERE id = object_id('dbo.WinnerList') and name = 'PriceDescription')
  ALTER TABLE WinnerList ADD [PriceDescription] nvarchar(256) NULL
go

IF NOT EXISTS (SELECT *  FROM sys.indexes  WHERE name='idx_WeekNumber' 
    AND object_id = OBJECT_ID('UserLuckyDrawCode'))
  begin
   CREATE INDEX idx_WeekNumber
	ON UserLuckyDrawCode (WeekNumber);
  end
go

IF NOT EXISTS (SELECT * FROM syscolumns WHERE id = object_id('dbo.UserCardCollection') and name = 'CardPoint')
  ALTER TABLE UserCardCollection ADD CardPoint int NULL
GO

UPDATE UserCardCollection 
	SET CardPoint = (SELECT TOP 1 CardPoint FROM PokeCard WHERE Id = CardId)
	WHERE CardPoint IS NULL

GO

-- Index for UserName of [UserProfile]
IF  NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[UserProfile]') AND name = N'UserNameIndex')
	CREATE UNIQUE NONCLUSTERED INDEX [UserNameIndex] ON [dbo].[UserProfile]
	(
		[UserName] ASC
	)WITH 
		(	PAD_INDEX = OFF, 
			STATISTICS_NORECOMPUTE = OFF, 
			SORT_IN_TEMPDB = OFF, 
			IGNORE_DUP_KEY = OFF, 
			DROP_EXISTING = OFF, 
			ONLINE = OFF, 
			ALLOW_ROW_LOCKS = ON, 
			ALLOW_PAGE_LOCKS = ON
		)	
GO

-- Add column to GameHistory
IF NOT EXISTS (SELECT * FROM syscolumns WHERE id = object_id('dbo.GameHistory') and name = 'AdjustPoint')
  ALTER TABLE GameHistory ADD AdjustPoint int NULL
GO

IF NOT EXISTS (SELECT * FROM syscolumns WHERE id = object_id('dbo.GameHistory') and name = 'TradeCardId')
  ALTER TABLE GameHistory ADD TradeCardId int NULL
GO

--update data
Update PokeCard Set CardElement = 'Water' where Id=15
Update PokeCard Set CardElement = 'Water' where Id=18
GO

--Add columns for table WinnerList & WinnerListUser
IF NOT EXISTS (SELECT * FROM syscolumns WHERE id = object_id('dbo.WinnerList') and name = 'Status')
  ALTER TABLE WinnerList ADD Status nvarchar(50) NULL
GO

IF NOT EXISTS (SELECT * FROM syscolumns WHERE id = object_id('dbo.WinnerListUser') and name = 'Status')
  ALTER TABLE WinnerListUser ADD Status nvarchar(50) NULL
GO


--Schema for table UsedCouponCodeReport
IF NOT EXISTS (SELECT * FROM sysobjects WHERE ID = OBJECT_ID('[UsedCouponCodeReport]') AND OBJECTPROPERTY(ID, 'IsTable') = 1)
	CREATE TABLE [dbo].UsedCouponCodeReport(
		[Id] [int] IDENTITY(1,1) NOT NULL,
		[CouponCode] [nvarchar](50) NOT NULL,
		[CreatedDate] [datetime] NOT NULL,
		[UserId] [nvarchar](128) NOT NULL,
		[UserName] [nvarchar](256) NOT NULL,
		[UserFullName] [nvarchar](256) NOT NULL,
		[UserPhoneNumber] [nvarchar](20) NOT NULL,
		[UserAddress] [nvarchar](512) NOT NULL,
		[UserEmail] [nvarchar](256) NOT NULL,
		[ClientIpAddress] [nvarchar](128) NULL,
		[ClientInfo] [nvarchar](1024) NULL,
		[CardId] [int] NULL,
		[CardType] [nvarchar](50) NULL,
		[CardName] [nvarchar](255) NULL,
		[CardImageUrl] [nvarchar](255) NULL,
		[CodePoint] INT NULL
	 CONSTRAINT [PK_UsedCouponCodeReport] PRIMARY KEY CLUSTERED 
	(
		[Id] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]
GO

-- Add field to UserCardCollection to support card tracking win, lose, exchange code
IF NOT EXISTS (SELECT * FROM syscolumns WHERE id = object_id('dbo.UserCardCollection') and name = 'CreatedDate')
  ALTER TABLE UserCardCollection ADD CreatedDate DateTime NULL
GO

IF NOT EXISTS (SELECT * FROM syscolumns WHERE id = object_id('dbo.UserCardCollection') and name = 'CouponCode')
  ALTER TABLE UserCardCollection ADD CouponCode nvarchar(50) NULL
GO

-- SourceOfCard : WinGame/ExchangeCode
IF NOT EXISTS (SELECT * FROM syscolumns WHERE id = object_id('dbo.UserCardCollection') and name = 'SourceOfCard')
  ALTER TABLE UserCardCollection ADD SourceOfCard nvarchar(255) NULL
GO

--Schema for table page view tracking
IF NOT EXISTS (SELECT * FROM sysobjects WHERE ID = OBJECT_ID('[PageViewTracking]') AND OBJECTPROPERTY(ID, 'IsTable') = 1)

	CREATE TABLE [dbo].[PageViewTracking](
		[Id] [int] IDENTITY(1,1) NOT NULL,
		[PageTitle] [nvarchar](255) NOT NULL,
		[PageUrl] [nvarchar](255) NOT NULL,
		[AccessDateTime] [datetime] NOT NULL,
		[ClientIpAddress] [nvarchar](50) NOT NULL,
		[ClientInfo] [nvarchar](255) NOT NULL,
		[UserName] [nvarchar](255) NOT NULL,
		[UserId] [nvarchar](128) NOT NULL,
	 CONSTRAINT [PK_PageViewTracking] PRIMARY KEY CLUSTERED 
	(
		[Id] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]

GO

-- add fields to UserProfile to capture all details of address
IF NOT EXISTS (SELECT * FROM syscolumns WHERE id = object_id('dbo.UserProfile') and name = 'AddressNumber')
  ALTER TABLE UserProfile ADD AddressNumber nvarchar(20) NULL
GO

IF NOT EXISTS (SELECT * FROM syscolumns WHERE id = object_id('dbo.UserProfile') and name = 'StreetName')
  ALTER TABLE UserProfile ADD StreetName nvarchar(128) NULL
GO

IF NOT EXISTS (SELECT * FROM syscolumns WHERE id = object_id('dbo.UserProfile') and name = 'WardName')
  ALTER TABLE UserProfile ADD WardName nvarchar(128) NULL
GO

IF NOT EXISTS (SELECT * FROM syscolumns WHERE id = object_id('dbo.UserProfile') and name = 'DistrictName')
  ALTER TABLE UserProfile ADD DistrictName nvarchar(128) NULL
GO

IF NOT EXISTS (SELECT * FROM syscolumns WHERE id = object_id('dbo.UserProfile') and name = 'CityName')
  ALTER TABLE UserProfile ADD CityName nvarchar(128) NULL
GO

IF NOT EXISTS (SELECT * FROM syscolumns WHERE id = object_id('dbo.UserProfile') and name = 'CreatedDate')
  ALTER TABLE UserProfile ADD CreatedDate datetime NULL
GO
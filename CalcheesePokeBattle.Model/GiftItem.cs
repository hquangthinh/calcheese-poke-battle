﻿using System.Collections.Generic;

namespace CalcheesePokeBattle.Model
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("GiftItem")]
    public partial class GiftItem
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage",
            "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public GiftItem()
        {
            PrizeLuckyDrawHistories = new HashSet<PrizeLuckyDrawHistory>();
        }


        public int Id { get; set; }

        [Required]
        [StringLength(512)]
        public string Name { get; set; }

        public string ImageUrl { get; set; }

        public int RedeemPoint { get; set; }

        [StringLength(512)]
        public string Description { get; set; }

        public bool IsRedeem { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PrizeLuckyDrawHistory> PrizeLuckyDrawHistories { get; set; }
    }
}
﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace CalcheesePokeBattle.Model
{
    [Table("ImportJobStatus")]
    public class ImportJobStatus
    {
        public int Id { get; set; }

        public string JobName { get; set; }

        public DateTime CreatedDate { get; set; }
    }
}

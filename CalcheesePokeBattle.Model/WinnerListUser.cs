﻿namespace CalcheesePokeBattle.Model
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("WinnerListUser")]
    public partial class WinnerListUser
    {
        public int Id { get; set; }

        public int? WinnerListId { get; set; }

        [StringLength(128)]
        public string UserId { get; set; }

        [StringLength(128)]
        public string FirstName { get; set; }

        [StringLength(256)]
        public string LastName { get; set; }

        [StringLength(256)]
        public string Email { get; set; }

        [StringLength(512)]
        public string Address { get; set; }

        [StringLength(20)]
        public string PhoneNumber { get; set; }

        [StringLength(20)]
        public string LuckyDrawCode { get; set; }

        [StringLength(256)]
        public string UserName { get; set; }

        [StringLength(50)]
        public string Status { get; set; }

        public virtual WinnerList WinnerList { get; set; }
    }
}
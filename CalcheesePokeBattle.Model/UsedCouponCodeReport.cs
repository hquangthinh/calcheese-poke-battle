﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CalcheesePokeBattle.Model
{
    [Table("UsedCouponCodeReport")]
    public class UsedCouponCodeReport
    {
        public int Id { get; set; }

        [StringLength(50)]
        public string CouponCode { get; set; }

        public DateTime CreatedDate { get; set; }

        [StringLength(128)]
        public string UserId { get; set; }

        [StringLength(256)]
        public string UserName { get; set; }

        [StringLength(256)]
        public string UserFullName { get; set; }

        [StringLength(20)]
        public string UserPhoneNumber { get; set; }

        [StringLength(512)]
        public string UserAddress { get; set; }

        [StringLength(256)]
        public string UserEmail { get; set; }

        [StringLength(128)]
        public string ClientIpAddress { get; set; }

        [StringLength(1024)]
        public string ClientInfo { get; set; }

        public int? CodePoint { get; set; }

        public int? CardId { get; set; }

        [StringLength(50)]
        public string CardType { get; set; }

        [StringLength(255)]
        public string CardName { get; set; }

        [StringLength(255)]
        public string CardImageUrl { get; set; }
    }
}

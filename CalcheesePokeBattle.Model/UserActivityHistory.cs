﻿namespace CalcheesePokeBattle.Model
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("UserActivityHistory")]
    public partial class UserActivityHistory
    {
        public int Id { get; set; }

        [StringLength(128)]
        public string UserName { get; set; }

        public DateTime CreatedAt { get; set; }

        [StringLength(256)]
        public string Action { get; set; }

        public string ActionDetail { get; set; }

        [StringLength(128)]
        public string ClientIpAddress { get; set; }

        [StringLength(1024)]
        public string ClientInfo { get; set; }
    }
}

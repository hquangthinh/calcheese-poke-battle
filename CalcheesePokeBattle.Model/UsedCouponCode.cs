﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CalcheesePokeBattle.Model
{
    [Table("UsedCouponCode")]
    public class UsedCouponCode
    {
        public int Id { get; set; }

        [StringLength(50)]
        public string CouponCode { get; set; }

        public DateTime CreatedDate { get; set; }

        [StringLength(128)]
        public string UserId { get; set; }

        [StringLength(128)]
        public string ClientIpAddress { get; set; }

        [StringLength(1024)]
        public string ClientInfo { get; set; }

        public int? CodePoint { get; set; }
    }
}
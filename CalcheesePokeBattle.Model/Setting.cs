﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CalcheesePokeBattle.Model
{
    [Table("Setting")]
    public partial class Setting
    {
        [Key]
        [StringLength(128)]
        public string SettingKey { get; set; }

        [StringLength(512)]
        public string Description { get; set; }

        [StringLength(2048)]
        public string SettingValueString { get; set; }

        public int? SettingValueInt { get; set; }
    }
}
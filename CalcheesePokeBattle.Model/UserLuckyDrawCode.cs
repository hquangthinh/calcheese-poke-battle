﻿namespace CalcheesePokeBattle.Model
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("UserLuckyDrawCode")]
    public partial class UserLuckyDrawCode
    {
        public int Id { get; set; }

        [Required]
        [StringLength(128)]
        public string UserId { get; set; }

        [Required]
        [StringLength(20)]
        public string LuckyDrawCode { get; set; }

        public DateTime CreatedDate { get; set; }

        public int WeekNumber { get; set; }

        public bool IsActive { get; set; }

        public virtual UserProfile UserProfile { get; set; }
    }
}
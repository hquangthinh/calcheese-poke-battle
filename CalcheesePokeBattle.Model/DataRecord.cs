﻿using System;
using System.Collections.Generic;

namespace CalcheesePokeBattle.Model
{
    public class DataRecord
    {
        private readonly List<string> _fields = new List<string>();
        private readonly Dictionary<string, object> _container = new Dictionary<string, object>();

        public object this[string fieldName]
        {
            get
            {
                object output;
                if (_container.TryGetValue(fieldName, out output))
                {
                    return output;
                }
                throw new ArgumentException("Invalid field name " + fieldName);
            }
            set
            {
                if (HasField(fieldName))
                {
                    _container[fieldName] = value;
                }
                else
                {
                    AddField(fieldName, value);
                }
            }
        }

        public object this[int ordinal]
        {
            get
            {
                if (ordinal < FieldCount) return _container[_fields[ordinal]];
                throw new IndexOutOfRangeException();
            }
        }

        public string Add(string fieldName, object value)
        {
            var fieldNameFinal = HasField(fieldName) ? $"{fieldName}{FieldCount + 1}" : fieldName;
            AddField(fieldNameFinal, value);
            return fieldNameFinal;
        }

        public int FieldCount => _fields.Count;

        public string[] Fields => _fields.ToArray();

        public T ReadValue<T>(string fieldName, T defaultValue)
        {
            var output = this[fieldName];
            return output == DBNull.Value ? defaultValue : (T)output;
        }

        public T ReadValue<T>(string fieldName)
        {
            return ReadValue(fieldName, default(T));
        }

        public T ReadValue<T>(int ordinal, T defaultValue)
        {
            var output = this[ordinal];
            return output == DBNull.Value ? defaultValue : (T)output;
        }

        public T ReadValue<T>(int ordinal)
        {
            return ReadValue(ordinal, default(T));
        }

        public bool HasField(string fieldName)
        {
            return _container.ContainsKey(fieldName);
        }

        private void AddField(string fieldName, object fieldValue)
        {
            _fields.Add(fieldName);
            _container.Add(fieldName, fieldValue);
        }
    }
}

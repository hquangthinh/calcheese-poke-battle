﻿using System;

namespace CalcheesePokeBattle.Model
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("UserCardCollection")]
    public partial class UserCardCollection
    {
        public int Id { get; set; }

        [Required]
        [StringLength(128)]
        public string UserId { get; set; }

        public int CardId { get; set; }

        public int? CardPoint { get; set; }

        public DateTime? CreatedDate { get; set; }

        public  string CouponCode { get; set; }

        public string SourceOfCard { get; set; }

        public virtual PokeCard PokeCard { get; set; }

        public virtual UserProfile UserProfile { get; set; }
    }
}
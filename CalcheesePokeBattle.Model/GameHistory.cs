﻿namespace CalcheesePokeBattle.Model
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("GameHistory")]
    public partial class GameHistory
    {
        public int Id { get; set; }

        [Required]
        [StringLength(128)]
        public string UserId1 { get; set; }

        [Required]
        [StringLength(128)]
        public string UserId2 { get; set; }

        [StringLength(128)]
        public string WinnerUserId { get; set; }

        public DateTime CreatedDate { get; set; }

        public int? User1Score { get; set; }

        public int? User2Score { get; set; }

        public int? AdjustPoint { get; set; }

        public int? TradeCardId { get; set; }
    }
}
﻿namespace CalcheesePokeBattle.Model
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("GiftRedeemHistory")]
    public class GiftRedeemHistory
    {
        public int Id { get; set; }

        public int WeekNumber { get; set; }

        public int GiftId { get; set; }

        [StringLength(128)]
        public string UserId { get; set; }

        public DateTime RedeemOn { get; set; }


    }
}
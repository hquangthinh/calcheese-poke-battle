﻿using System;

namespace CalcheesePokeBattle.Model
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("CouponCode")]
    public partial class CouponCode
    {
        public int Id { get; set; }

        [StringLength(450)]
        public string Code { get; set; }

        [StringLength(512)]
        public string Description { get; set; }

        public bool Available { get; set; }

        public int Point { get; set; }

        public int? CardId { get; set; }

        [StringLength(256)]
        public string RedeemByUserName { get; set; }

        public DateTime? RedeemOn { get; set; }

        [StringLength(128)]
        public string ClientIpAddress { get; set; }

        [StringLength(1024)]
        public string ClientInfo { get; set; }
    }
}

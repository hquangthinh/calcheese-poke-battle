﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CalcheesePokeBattle.Model
{
    [Table("vwAllOnlinePlayerConnectionInfo")]
    public class AllOnlinePlayerConnectionInfo
    {
        [Key]
        [Column(Order = 0)]
        public int Id { get; set; }

        [StringLength(255)]
        [Column(Order = 1)]
        public string ServerName { get; set; }

        [StringLength(50)]
        [Column(Order = 2)]
        public string ServerIpAddress { get; set; }

        [StringLength(128)]
        [Column(Order = 3)]
        public string UserId { get; set; }

        [StringLength(255)]
        [Column(Order = 4)]
        public string UserName { get; set; }

        [StringLength(255)]
        [Column(Order = 5)]
        public string ConnectionId { get; set; }

        [Column(Order = 6)]
        public DateTime ConnectedAt { get; set; }

        [StringLength(255)]
        [Column(Order = 7)]
        public string DeviceInfo { get; set; }

        [StringLength(50)]
        [Column(Order = 8)]
        public string Status { get; set; }

        [StringLength(50)]
        [Column(Order = 9)]
        public string ClientIpAddress { get; set; }

        [Column(Order = 10)]
        public int TotalPoint { get; set; }

        [Column(Order = 11)]
        public string FirstName { get; set; }

        [Column(Order = 12)]
        public string LastName { get; set; }

        [Column(Order = 13)]
        public string AvatarUrl { get; set; }
    }
}
﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CalcheesePokeBattle.Model
{
    [Table("GameSession")]
    public class GameSession
    {
        public int Id { get; set; }

        [StringLength(128)]
        public string PlayerUserId1 { get; set; }

        [StringLength(128)]
        public string PlayerUserName1 { get; set; }

        [StringLength(128)]
        public string PlayerUserId2 { get; set; }

        [StringLength(128)]
        public string PlayerUserName2 { get; set; }

        public DateTime CreatedDate { get; set; }
    }
}
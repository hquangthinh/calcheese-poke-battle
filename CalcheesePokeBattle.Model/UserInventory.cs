﻿namespace CalcheesePokeBattle.Model
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("UserInventory")]
    public partial class UserInventory
    {
        public int Id { get; set; }

        [Required]
        [StringLength(128)]
        public string UserId { get; set; }

        [Required]
        [StringLength(512)]
        public string Name { get; set; }

        [StringLength(512)]
        public string ImageUrl { get; set; }

        public virtual UserProfile UserProfile { get; set; }
    }
}
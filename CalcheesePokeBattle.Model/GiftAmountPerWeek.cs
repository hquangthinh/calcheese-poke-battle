﻿namespace CalcheesePokeBattle.Model
{
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("GiftAmountPerWeek")]
    public class GiftAmountPerWeek
    {
        public int Id { get; set; }

        public int WeekNumber { get; set; }

        public string GiftName { get; set; }

        public int TotalPrize { get; set; }
    }
}

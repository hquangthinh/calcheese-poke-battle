﻿// Required modules
var gulp = require('gulp');
var sass = require('gulp-sass');
var jade = require('gulp-jade');
var concat = require('gulp-concat');
var cssmin = require('gulp-cssmin');
var uglify = require('gulp-uglify');
var ngAnnotate = require('gulp-ng-annotate');
var sourcemaps = require('gulp-sourcemaps');
var rename = require('gulp-rename');

// Global variables
var appTheme = 'default';
var bowerPath = 'Content/assets';
var customVendorPath = 'Content/vendor';
// Path to compile/concat/minify source files
var paths = {
    stylesDefault: {
        src: ['Content/scss/theme/default/**/*.sass', 'Content/scss/theme/default/**/*.scss'],
        dest: 'Content/styles/theme/default'
    },
    jadeViews: {
        src: 'Scripts/app/views/**/*.jade',
        dest: 'Scripts/app/views/'
    },
    jadePartials: {
        src: 'Scripts/app/partials/*.jade',
        dest: 'Scripts/app/partials/'
    },
    jadeComponentViews: {
        src: 'Scripts/app/components/**/*.jade',
        dest: 'Scripts/app/components/'
    },
    stylesFinal: {
        vendor: [
            customVendorPath + '/bootstrap.css',
            bowerPath + '/angular-toastr/dist/angular-toastr.min.css',
            bowerPath + '/angular-loading-bar/build/loading-bar.min.css',
            bowerPath + '/sweetalert2/dist/sweetalert2.min.css',
            bowerPath + '/fontawesome/css/font-awesome.min.css',
            bowerPath + '/perfect-scrollbar/css/perfect-scrollbar.min.css',
            bowerPath + '/ng-img-crop/compile/minified/ng-img-crop.css',
            //bowerPath + '/slick-carousel/slick/slick.css',
            //bowerPath + '/slick-carousel/slick/slick-theme.css',
            bowerPath + '/angular-spinkit/build/angular-spinkit.min.css',
            'Content/styles/theme/' + appTheme + '/vendor.css',
            customVendorPath + '/animate.min.css',
            customVendorPath + '/vendor-override.css',
            customVendorPath + '/awesome-bootstrap-checkbox.css'
        ],
        app: [
            'Content/styles/theme/' + appTheme + '/ie.css',
            'Content/styles/theme/' + appTheme + '/print.css',
            'Content/styles/theme/' + appTheme + '/screen.css',
            'Content/styles/theme/' + appTheme + '/main.css'
        ],
        dest: 'Content/styles/theme/' + appTheme + '/'
    },
    scriptsFinal: {
        vendor: [
            bowerPath + '/es6-promise/es6-promise.auto.min.js',
            bowerPath + '/jquery/dist/jquery.min.js',
            bowerPath + '/ramda/dist/ramda.min.js',
            bowerPath + '/bootstrap-sass-official/assets/javascripts/bootstrap.js',
            bowerPath + '/iCheck/icheck.js',
            bowerPath + '/sweetalert2/dist/sweetalert2.min.js',
            bowerPath + '/perfect-scrollbar/js/perfect-scrollbar.min.js',
            bowerPath + '/animejs/anime.min.js',
            //bowerPath + '/slick-carousel/slick/slick.min.js',
            bowerPath + '/signalr/jquery.signalR.min.js' /* signalR does not end with ; put it at the end*/
        ],
        angular: [
            bowerPath + '/angular/angular.min.js',
            bowerPath + '/ng-file-upload/angular-file-upload-shim.min.js',
            bowerPath + '/ng-file-upload/angular-file-upload.min.js',
            bowerPath + '/angular-resource/angular-resource.js',
            bowerPath + '/angular-animate/angular-animate.js',
            bowerPath + '/angular-sanitize/angular-sanitize.js',
            bowerPath + '/angular-bootstrap/ui-bootstrap-tpls.min.js',
            bowerPath + '/angular-ui-router/release/angular-ui-router.min.js',
            bowerPath + '/angular-loading-bar/build/loading-bar.js',
            bowerPath + '/angular-breadcrumb/dist/angular-breadcrumb.js',
            bowerPath + '/angular-input-masks/angular-input-masks-standalone.min.js',
            bowerPath + '/angular-toastr/dist/angular-toastr.tpls.min.js',
            bowerPath + '/ngstorage/ngStorage.js',
            bowerPath + '/moment/min/moment.min.js',
            bowerPath + '/angular-momentjs/angular-momentjs.min.js',
            bowerPath + '/angular-cookies/angular-cookies.min.js',
            bowerPath + '/ng-img-crop/compile/minified/ng-img-crop.js',
            bowerPath + '/angular-recaptcha/release/angular-recaptcha.min.js',
            bowerPath + '/angular-socialshare/dist/angular-socialshare.min.js',
            bowerPath + '/angular-spinkit/build/angular-spinkit.min.js',
            bowerPath + '/angular-signalr-hub/signalr-hub.min.js'
        ],
        app: [
            'Scripts/app/calcheese-front-site.module.js',
            'Scripts/app/calcheese-front-site.routes.js',
            'Scripts/app/config/*.js',
            'Scripts/app/services/*.js',
            'Scripts/app/helpers/*.js',
            'Scripts/app/filters/*.js',
            'Scripts/app/controllers/*.js',
            'Scripts/app/directives/*.js',
            'Scripts/app/components/**/*.js'
        ],
        dest: 'Scripts/'
    }
};

function parseSASS(src, dest) {
    return gulp.src(src)
        .pipe(sass({ errLogToConsole: true }))
        .pipe(gulp.dest(dest));
}

// [TASK] ----------------------------------- 
// Compile SASS to CSS
gulp.task('styles-blue', function () {
    return parseSASS(paths.stylesDefault.src, paths.stylesDefault.dest);
});
gulp.task('compile-styles-vendor', ['styles-blue'], function () {
    return gulp.src(paths.stylesFinal.vendor)
        .pipe(concat('calcheese-front-site-lib.css'))
        .pipe(gulp.dest(paths.stylesFinal.dest))
        .pipe(cssmin())
        .pipe(rename('calcheese-front-site-lib.min.css'))
        .pipe(gulp.dest(paths.stylesFinal.dest));
});
gulp.task('compile-styles-app', ['styles-blue'], function () {
    return gulp.src(paths.stylesFinal.app)
        .pipe(concat('calcheese-front-site-app.css'))
        .pipe(gulp.dest(paths.stylesFinal.dest))
        .pipe(cssmin())
        .pipe(rename('calcheese-front-site-app.min.css'))
        .pipe(gulp.dest(paths.stylesFinal.dest));
});
gulp.task('styles', ['styles-blue', 'compile-styles-vendor', 'compile-styles-app']);

// [TASK] ----------------------------------- 
// Compile javascripts
gulp.task('compile-scripts-vendor', function () {
    return gulp.src(paths.scriptsFinal.vendor)
        .pipe(concat('calcheese-front-site-lib.js'))
        .pipe(gulp.dest(paths.scriptsFinal.dest))
        .pipe(uglify())
        .pipe(rename('calcheese-front-site-lib.min.js'))
        .pipe(gulp.dest(paths.scriptsFinal.dest));
});
gulp.task('compile-scripts-angular', function () {
    return gulp.src(paths.scriptsFinal.angular)
        .pipe(concat('angular-calcheese-front-site-modules.js'))
        .pipe(gulp.dest(paths.scriptsFinal.dest))
        .pipe(uglify())
        .pipe(rename('angular-calcheese-front-site-modules.min.js'))
        .pipe(gulp.dest(paths.scriptsFinal.dest));
});
gulp.task('compile-scripts-app', function () {
    return gulp.src(paths.scriptsFinal.app)
        .pipe(concat('calcheese-front-site-app.js'))
        .pipe(gulp.dest(paths.scriptsFinal.dest))
        .pipe(ngAnnotate())
        .pipe(uglify())
        .pipe(rename('calcheese-front-site-app.min.js'))
        .pipe(gulp.dest(paths.scriptsFinal.dest));
});

gulp.task('scripts', ['compile-scripts-vendor', 'compile-scripts-angular', 'compile-scripts-app']);

// [TASK] ----------------------------------- 
// Parse jade template to HTML
gulp.task('jade-views', function () {
    return gulp.src(paths.jadeViews.src)
        .pipe(jade({
            locals: {},
            debug: false,
            pretty: true
        }))
        .pipe(gulp.dest(paths.jadeViews.dest));
});
gulp.task('jade-partials', function () {
    return gulp.src(paths.jadePartials.src)
        .pipe(jade({
            locals: {},
            debug: false,
            pretty: true
        }))
        .pipe(gulp.dest(paths.jadePartials.dest));
});
gulp.task('jade-components-views', function () {
    return gulp.src(paths.jadeComponentViews.src)
        .pipe(jade({
            locals: {},
            debug: false,
            pretty: true
        }))
        .pipe(gulp.dest(paths.jadeComponentViews.dest));
});

// [TASK] ----------------------------------- 
// Watch file changes
gulp.task('watch', function () {
    gulp.watch([paths.stylesDefault.src], ['styles']);
    gulp.watch([paths.jadeViews.src], ['jade-views']);
    gulp.watch([paths.jadePartials.src], ['jade-partials']);
    gulp.watch([paths.jadeComponentViews.src], ['jade-components-views']);
    gulp.watch([paths.scriptsFinal.app], ['compile-scripts-app']);
});

// Default tasks
gulp.task('build', ['styles', 'scripts', 'jade-views', 'jade-partials', 'jade-components-views']);
gulp.task('build-app', ['compile-styles-app', 'compile-scripts-app', 'jade-views', 'jade-partials', 'jade-components-views']);
gulp.task('watch-app', ['build-app', 'watch']);
gulp.task('default', ['build', 'watch']);
﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="CalcheesePokeBattle.WebApp.Default" %>
<%@ Import Namespace="System.Globalization" %>
<%@ Import Namespace="CalcheesePokeBattle.WebApp" %>

<!DOCTYPE html>

<html lang="en" ng-app="CalcheeseFrontSite"
    data-sitebaseurl="<%=ViewState["siteBaseUrl"]%>"
    data-webbaseurl="<%=ViewState["webBaseUrl"]%>"
    data-apibaseurl="<%=ViewState["apiBaseUrl"]%>"
    data-instanceid="<%=Application["__INSTANCE_ID__"]%>"
    data-mode="<%=Application["__MODE__"]%>"
    data-locale="<%=CultureInfo.CurrentCulture.ToString()%>">
<head>
    <title>Calcheese</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="title" content="Calcheese" />
    <meta name="description" content="Sưu tập Pokemon, giao đấu giành wà khủng." />
    <meta name="keywords" content="Calcheese, Pokemon" />
    
    <meta property="og:title" content="Calcheese" />
    <meta property="og:description" content="Sưu tập Pokemon, giao đấu giành wà khủng." />
    <meta property="og:image" content="https://storagecalcheeseprod.blob.core.windows.net/images/calcheese-fb-share-1200x630.png" />
    
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="apple-touch-fullscreen" content="yes" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">

    <link rel="icon" type="image/png" href="Content/favicons/logo.png">

    <base href="<%=ResolveUrl("~")%>"/>

    <link rel="stylesheet" href="<%=ResolveUrl("~/Content/styles/theme/default/calcheese-front-site-lib.min.css") + "?" + Application.Get(Global.INSTANCE_ID)%>">
    <link rel="stylesheet" href="<%=ResolveUrl("~/Content/styles/theme/default/calcheese-front-site-app.min.css") + "?" + Application.Get(Global.INSTANCE_ID)%>">
</head>
<body ng-class="{'thamgia': isInJoinGamePage(), 'profile': isProfilePage(), 'sanchoi': isInGamePlayGroundPage(), 'game': isInPlayingGame(), 'danh-sach-trung-giai': isInGiftPage(), 'list-card': isInPokeListPage() }">
    <!--[if lt IE 10]>
      <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
    <page-top></page-top>
    <main ui-view></main>

    <span id="token-af" value="<%=ViewState["tokenAntiForgery"]%>"></span>

    <div class="modal fade" id="Global-Modal-Error" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <img src="Content/images/icons/icon-close.png" alt="">
                    </button>
                    <div id="Global-Modal-Error-Message" class="info text-center">
                        Không thể kết nối đến máy chủ, vui lòng thử lại!
                    </div>
                    <img class="modal-img-bottom mb-none" src="Content/images/img-bottom-popup.png" alt="">
                </div>
            </div>
        </div>
    </div>
    
    <script>
        (function(i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] ||
                function() {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-86129213-1', 'auto');
    ga('send', 'pageview');
  </script>

    <%= AssetScripts(
         "~/Scripts/calcheese-front-site-lib.min.js"
        ,"~/Scripts/angular-calcheese-front-site-modules.min.js"
        ,"~/Scripts/calcheese-front-site-app.min.js"
    )%>
    <script src="<%=ViewState["siteBaseUrl"]%>/signalr/hubs" async></script>
</body>
</html>
﻿(function (angular) {
    'use strict';

    angular
        .module('CalcheeseFrontSite')
        .controller('GameConfirmationModalCtrl', GameConfirmationModalCtrl);

    GameConfirmationModalCtrl.$inject = ['$uibModalInstance', 'MessageModel'];

    function GameConfirmationModalCtrl($uibModalInstance, MessageModel) {

        var ctrl = this;

        ctrl.$onInit = function () {
            ctrl.model = MessageModel;
        }

        ctrl.confirmGame = function() {
            $uibModalInstance.close('ok');
        }

        ctrl.declineGame = function () {
            $uibModalInstance.dismiss('cancel');
        }

        ctrl.close = function () {
            $uibModalInstance.dismiss('cancel');
        }
    }

    angular
        .module('CalcheeseFrontSite')
        .controller('GameInfoWithRetryModalCtrl', GameInfoWithRetryModalCtrl);

    GameInfoWithRetryModalCtrl.$inject = ['$uibModalInstance', 'MessageModel'];

    function GameInfoWithRetryModalCtrl($uibModalInstance, MessageModel) {
        var ctrl = this;

        ctrl.$onInit = function () {
            ctrl.model = MessageModel;
        }

        ctrl.retry = function () {
            $uibModalInstance.close('retry');
        }

        ctrl.close = function () {
            $uibModalInstance.dismiss('cancel');
        }
    }

    // modal controller for poke-card-game/modal-leave-game.html
    angular
        .module('CalcheeseFrontSite')
        .controller('PlayerLeaveGameModalCtrl', PlayerLeaveGameModalCtrl);

    PlayerLeaveGameModalCtrl.$inject = ['$uibModalInstance', 'MessageModel'];

    function PlayerLeaveGameModalCtrl($uibModalInstance, MessageModel) {
        var ctrl = this;

        ctrl.$onInit = function () {
            ctrl.model = MessageModel;
        }

        ctrl.close = function () {
            $uibModalInstance.dismiss('cancel');
        }
    }

    // modal controller for poke-card-game/game-not-enough-point-modal.html
    angular
        .module('CalcheeseFrontSite')
        .controller('GameNotEnoughPointModalCtrl', GameNotEnoughPointModalCtrl);

    GameNotEnoughPointModalCtrl.$inject = ['$uibModalInstance', 'MessageModel'];

    function GameNotEnoughPointModalCtrl($uibModalInstance, MessageModel) {
        var ctrl = this;

        ctrl.$onInit = function () {
            ctrl.model = MessageModel;
        }

        ctrl.close = function () {
            $uibModalInstance.dismiss('cancel');
        }
    }

})(window.angular);
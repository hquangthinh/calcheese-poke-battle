﻿(function (angular) {
    'use strict';

    angular
        .module('CalcheeseFrontSite')
        .controller('PokeCardGamePageCtrl', PokeCardGamePageCtrl);

    PokeCardGamePageCtrl.$inject = ['$rootScope', '$scope', '$uibModal', '$timeout', 'url',
        'BaseCtrlAs', 'GameHubService', 'MessageAlertService', 'config', 'webBaseUrl', 'PageViewTrackingService'];

    function PokeCardGamePageCtrl($rootScope, $scope, $uibModal, $timeout, url,
        BaseCtrlAs, GameHubService, MessageAlertService, config, webBaseUrl, PageViewTrackingService) {

        var ctrl = this;
        BaseCtrlAs.extend(ctrl, $scope, 'game_hub_online_players');

        ctrl.$onInit = function() {
            PageViewTrackingService.trackPageView({
                PageTitle: 'san-choi',
                PageUrl: webBaseUrl + '/san-choi',
                UserId: ctrl.CurrentUser.Id,
                UserName: ctrl.CurrentUser.UserName
            });
        }

        ctrl.openGameTutorialModal = openGameTutorialModal;
        ctrl.goToJoinGamePage = goToJoinGamePage;

        function openGameTutorialModal() {
            $uibModal.open({
                templateUrl: url.tmpl('/poke-card-game/game-tutorial-modal-v2.html'),
                controller: "GameTutorialModalCtrl as modalCtrl",
                size: "lg",
                windowTopClass: "modal-huong-dan-window"
            }).result.then(function(modalRes) {
                if ('join_game' === modalRes) {
                    ctrl.goToJoinGamePage();
                }
            });
        }

        function goToJoinGamePage() {
            if (ctrl.CurrentUser.TotalPoint < config.MIN_POINTS_TO_PLAY_GAME) {
                MessageAlertService.showGameNotEnoughPointModal();
                return;
            }
            if (ctrl.CurrentUser.TotalCards === 0) {
                MessageAlertService.showGameNotEnoughPointModal('Bạn không có đủ thẻ để chơi');
                return;
            }
            GameHubService.checkIfUserCanJoinGame().then(function (joinGameCheckRes) {
                if (joinGameCheckRes && joinGameCheckRes.data && joinGameCheckRes.data.success) {
                    // Go to join game page
                    ctrl._go(ctrl._router.POKE_CARD_GAME_JOIN_GAME);
                } else {
                    MessageAlertService.showGameNotEnoughPointModal();
                }
            });
        }
    }

})(window.angular);
﻿(function (angular) {
    'use strict';

    angular
        .module('CalcheeseFrontSite')
        .controller('ConfirmRedeemLuckyCodeModalCtrl', ConfirmRedeemLuckyCodeModalCtrl);

    ConfirmRedeemLuckyCodeModalCtrl.$inject = ['$scope', '$uibModalInstance'];

    function ConfirmRedeemLuckyCodeModalCtrl($scope, $uibModalInstance) {
        var ctrl = this;

        ctrl.confirmRedeemLuckyDrawCode = confirmRedeemLuckyDrawCode;
        ctrl.close = close;

        function confirmRedeemLuckyDrawCode() {
            $uibModalInstance.close({ result: 'ok' });
        }

        function close() {
            $uibModalInstance.dismiss('cancel');
        }
    }

})(window.angular);
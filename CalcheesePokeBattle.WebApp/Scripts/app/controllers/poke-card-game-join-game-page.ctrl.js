﻿(function (document, angular, $) {
    'use strict';

    angular
        .module('CalcheeseFrontSite')
        .controller('PokeCardGameJoinGamePageCtrl', PokeCardGameJoinGamePageCtrl);

    PokeCardGameJoinGamePageCtrl.$inject = ['$rootScope', '$scope', '$timeout', '$interval', '$log', 'url', 'BaseCtrlAs',
        'UserService', 'MessageAlertService', 'GameClientService', 'GameHubService', 'config', 'webBaseUrl', 'PageViewTrackingService'];

    function PokeCardGameJoinGamePageCtrl($rootScope, $scope, $timeout, $interval, $log, url, BaseCtrlAs,
        UserService, MessageAlertService, GameClientService, GameHubService, config, webBaseUrl, PageViewTrackingService) {

        var ctrl = this;

        ctrl.$onInit = function () {

            $(document).ready(function() {
                $timeout(function() {
                        // init scroll bar
                        var classScroll = document.getElementsByClassName('scroll-pf');
                        for (var i = 0; i < classScroll.length; i++) {
                            Ps.initialize(classScroll[i],
                            {
                                wheelSpeed: 2,
                                wheelPropagation: true,
                                maxScrollbarLength: 60
                            });
                        };
                    },
                    1000);
            });

            BaseCtrlAs.extend(ctrl, $scope, 'game_hub_online_players');

            PageViewTrackingService.trackPageView({
                PageTitle: 'tham-gia-san-choi',
                PageUrl: webBaseUrl + '/tham-gia-san-choi',
                UserId: ctrl.CurrentUser.Id,
                UserName: ctrl.CurrentUser.UserName
            });

            ctrl.request.params = {
                PageNumber: 1,
                PageSize: config.MAX_PLAYER_ONLINE_SUPPORT,
                FindAll: false
            };

            GameHubService.getAllOnlinePlayers({ params: ctrl.request.params })
                .then(function (playersOnlineRes) {
                    //console.log('playersOnlineRes -> ', playersOnlineRes);
                    ctrl.result = playersOnlineRes.data;
                });

            // setup polling to refresh game lobby every 5 seconds
            // execute 200 times only
            ctrl.timerRefreshGameLobby = $interval(function() {
                    ctrl.refreshGameLobby();
                },
                5000,
                200);
        }

        ctrl.inviteToJoinGame = inviteToJoinGame;
        ctrl.selectRandomOpponent = selectRandomOpponent;
        ctrl.refreshGameLobby = refreshGameLobby;

        // send game invitation to other player
        function inviteToJoinGame(playerInfo) {
            //console.log('playerInfo -> ', playerInfo);
            if (ctrl.CurrentUser.TotalPoint < config.MIN_POINTS_TO_PLAY_GAME) {
                MessageAlertService.showGameNotEnoughPointModal();
                return;
            }
            if (ctrl.waitingForInvitationResponse) {
                return;
            }
            GameClientService.sendNewGameRequest({
                    PlayerUserId: ctrl.CurrentUser.Id,
                    PlayerUserName: ctrl.CurrentUser.UserName,
                    PlayerName: ctrl.CurrentUser.FullName || ctrl.CurrentUser.UserName,
                    PlayerTotalPoint: playerInfo.TotalPoints,
                    InvitedPlayerUserId: playerInfo.UserId,
                    InvitedPlayerUserName: playerInfo.UserName,
                    InvitedPlayerName: playerInfo.FullName || playerInfo.UserName
                })
                .done(function () {
                    ctrl.waitingForInvitationResponse = true;
                    MessageAlertService.showToastrMessage('Thách đấu đã gửi đến ' +
                        (playerInfo.FullName || playerInfo.UserName) +
                        '. Đang chờ chấp nhận.');
                });
        }

        function selectRandomOpponent() {

            if (ctrl.CurrentUser.TotalPoint < config.MIN_POINTS_TO_PLAY_GAME) {
                MessageAlertService.showGameNotEnoughPointModal();
                return;
            }
            if (ctrl.waitingForInvitationResponse) {
                return;
            }

            GameClientService.findRandomOpponentPlayer({
                    PlayerUserId: ctrl.CurrentUser.Id,
                    PlayerUserName: ctrl.CurrentUser.UserName,
                    PlayerName: ctrl.CurrentUser.FullName || ctrl.CurrentUser.UserName
                })
                .done(function() {
                    ctrl.waitingForInvitationResponse = true;
                    MessageAlertService.showToastrMessage('Hãy chờ hệ thống lựa chọn đối thủ ngẫu nhiên cho bạn');
                });
        }

        function refreshGameLobby() {
            return GameHubService.getAllOnlinePlayers({ params: ctrl.request.params })
                .then(function(playersOnlineRes) {
                    ctrl.result = playersOnlineRes.data;
                });
        }

        // event listiners
        $rootScope.$on('game-hub:game-start',
            function(evt, evtData) {
                //console.log('game-hub:game-start', evtData);
                ctrl._go(ctrl._router.POKE_CARD_GAME_PLAYING, { gameSessionId: evtData.GameSessionId });
            });

        $rootScope.$on('game-hub:game-decline',
            function (evt, evtData) {
                //console.log('game-hub:game-decline', evtData);
                ctrl.waitingForInvitationResponse = false;
                var message = evtData.PlayerFullName + ' đã từ chối lời mời giao đấu của bạn';
                MessageAlertService.showModalInfo(message);
            });

        $rootScope.$on('game-hub:new-game-request-failed',
            function (evt, errorMessage) {
                //console.log('game-hub:new-game-request-failed', errorMessage);
                ctrl.waitingForInvitationResponse = false;
                MessageAlertService.showModalInfo(errorMessage);
            });

        $rootScope.$on('game-hub:no-random-opponent',
            function (evt, message) {
                //console.log('game-hub:no-random-opponent', message);
                ctrl.waitingForInvitationResponse = false;
                MessageAlertService.showGameModalInfoWithRetry(message).result.then(function() {
                    // retry find random players
                    ctrl.waitingForInvitationResponse = false;
                    ctrl._go(ctrl._router.POKE_CARD_GAME_JOIN_GAME, {}, { reload: true });
                });
            });

        $rootScope.$on('game-hub:new-player-online',
            function (evt, evtData) {
                // reload user online list on new player goes online
                // TODO: this is not stable
                //if (evtData && evtData.UserId !== ctrl.CurrentUser.Id) {
                //    GameHubService.getAllOnlinePlayers({ params: ctrl.request.params })
                //        .then(function (playersOnlineRes) {
                //            ctrl.result = playersOnlineRes.data;
                //        });
                //}
            });

        $rootScope.$on('game-hub:player-disconnect',
            function (evt, evtData) {
                //if (evtData && evtData.UserId !== ctrl.CurrentUser.Id) {
                //    GameHubService.getAllOnlinePlayers({ params: ctrl.request.params })
                //        .then(function (playersOnlineRes) {
                //            ctrl.result = playersOnlineRes.data;
                //        });
                //}
            });

        //function disconnectGameLobbyOnNavigateAwayPlayingPage() {
        //    return GameHubService.disconnectGameForUser(ctrl.CurrentUser.Id);
        //}

        $scope.$on("$destroy",
            function () {
                $log.debug('scope destroy');
                if (ctrl.timerRefreshGameLobby) {
                    $interval.cancel(ctrl.timerRefreshGameLobby);
                }
                ////try stop game session
                //if (ctrl.CurrentUser) {
                //    disconnectGameLobbyOnNavigateAwayPlayingPage();
                //}
            });
    }

})(window.document, window.angular, window.$);

﻿(function (angular) {
    'use strict';

    angular
        .module('CalcheeseFrontSite')
        .controller('UpdateProfileModalCtrl', UpdateProfileModalCtrl);

    UpdateProfileModalCtrl.$inject = ['$scope', '$uibModalInstance', 'url', 'BaseDetailCtrlAs', 'UserService'];

    function UpdateProfileModalCtrl($scope, $uibModalInstance, url, BaseDetailCtrlAs, UserService) {
        var ctrl = this;
        ctrl.$onInit = function() {
            BaseDetailCtrlAs.extend(ctrl, $scope, 'users');
            ctrl._params = { Id: 'current' };
        }

        ctrl.invalidForm = invalidForm;
        ctrl.saveAndClose = saveAndClose;
        ctrl.cancel = cancel;

        function invalidForm() {
            return false;
        }

        function saveAndClose() {
            var isFormInvalid = invalidForm();
            if (isFormInvalid)
                return;
            UserService.updateProfileMainDetails(ctrl.model)
                .then(function () {
                    $uibModalInstance.close({
                        success: true
                    });
                })
                .catch(function(err) {
                    $uibModalInstance.close({
                        success: true,
                        message: err
                    });
                });
        }

        function cancel() {
            $uibModalInstance.dismiss('cancel');
        }

    }

})(window.angular);
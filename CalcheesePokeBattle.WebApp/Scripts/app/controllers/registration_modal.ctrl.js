﻿(function (angular) {
    'use strict';

    angular
        .module('CalcheeseFrontSite')
        .controller('RegistrationModalCtrl', RegistrationModalCtrl);

    RegistrationModalCtrl.$inject = ['$scope', '$window', '$timeout', '$uibModalInstance', 'vcRecaptchaService', 'UserService', 'FormValidationUtils'];

    function RegistrationModalCtrl($scope, $window, $timeout, $uibModalInstance, vcRecaptchaService, UserService, FormValidationUtils) {

        var modalCtrl = this;

        modalCtrl.$onInit = function () {
            modalCtrl.processing = false;
            modalCtrl.model = {};
            modalCtrl.recaptcha = {
                key: '6Le07RkUAAAAADk946WUmIaS9bjgglTUgFgURsy-',
                response: null,
                widgetId: null
            };

            $timeout(function() {
                    //console.log('$window.innerWidth :: ', $window.innerWidth);
                    if ($window.innerWidth < 767) {
                        angular.element('.modal-backdrop').css('z-index', '1049');
                    }
                },
                1000);
        };

        modalCtrl.setResponse = function (response) {
            //console.info('Response available');
            if (modalCtrl.captchaError)
                delete modalCtrl.captchaError;
            modalCtrl.recaptcha.response = response;
        };

        modalCtrl.setWidgetId = function (widgetId) {
            //console.info('Created widget ID: %s', widgetId);
            modalCtrl.recaptcha.widgetId = widgetId;
        };

        modalCtrl.cbExpiration = function () {
            //console.info('Captcha expired. Resetting response object');
            vcRecaptchaService.reload(modalCtrl.recaptcha.widgetId);
            modalCtrl.recaptcha.response = null;
        };

        modalCtrl.validateInput = validateInput;
        modalCtrl.checkPlayerName = checkPlayerName;
        modalCtrl.checkPhoneNumber = checkPhoneNumber;
        modalCtrl.register = register;
        modalCtrl.registerWithCaptchaValidation = registerWithCaptchaValidation;

        function checkPlayerName() {

            if (!FormValidationUtils.validateRequire('UserName', 'Tên đăng nhập là bắt buộc', modalCtrl.model))
                return;

            UserService.checkUserName(modalCtrl.model.UserName).then(function (result) {
                if (result && result.data && !result.data.success) {
                    modalCtrl.model.UserNameInvalidNameError = result.data.error.message;
                } else {
                    delete modalCtrl.model.UserNameInvalidNameError;
                }
            });
        }

        function checkPhoneNumber() {

            if (!FormValidationUtils.validateRequire('PhoneNumber', 'Số điện thoại là bắt buộc', modalCtrl.model))
                return;

            UserService.checkPhoneNumber(modalCtrl.model.PhoneNumber).then(function (result) {
                if (result && result.data && !result.data.success) {
                    modalCtrl.model.UserNameInvalidPhoneNumberError = result.data.error.message;
                } else {
                    delete modalCtrl.model.UserNameInvalidPhoneNumberError;
                }
            });
        }

        function registerWithCaptchaValidation() {
            var inputIsInvalid = validateInput(modalCtrl.model);
            if (inputIsInvalid)
                return false;

            //console.log('sending the captcha response to the server', modalCtrl.recaptcha.response);
            UserService.validateCaptcha({ GRecaptchaResponse: modalCtrl.recaptcha.response }).then(function (result) {
                if (result && result.data && result.data.success) {
                    //console.log('Success');
                    modalCtrl.processing = true;
                    UserService.registerUser(modalCtrl.model)
                        .then(function (registrationResult) {

                            $uibModalInstance.close({
                                success: true,
                                data: registrationResult
                            });

                            return registrationResult;
                        })
                        .finally(function () {
                            modalCtrl.processing = false;
                        });
                } else {
                    //console.log('Failed validation');
                    // In case of a failed validation you need to reload the captcha
                    // because each response can be checked just once
                    modalCtrl.captchaError = "Lỗi xác nhận captcha. Vui lòng thử lại.";
                    vcRecaptchaService.reload(modalCtrl.widgetId);
                }
            });
        }

        function register() {
            var inputIsInvalid = validateInput(modalCtrl.model);
            if (inputIsInvalid)
                return false;

            modalCtrl.processing = true;
            UserService.registerUser(modalCtrl.model)
                .then(function(registrationResult) {

                    $uibModalInstance.close({
                        success: true,
                        data: registrationResult
                    });

                    return registrationResult;
                })
                .finally(function() {
                    modalCtrl.processing = false;
                });
        }

        modalCtrl.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        }

        // -- private function

        function validateInput(modelToValidate) {
            // return true if input is invalid

            if (!FormValidationUtils.validateRequire('UserName', 'Tên đăng nhập là bắt buộc', modelToValidate)) return true;

            if (!FormValidationUtils.validateRequire('Password', 'Mật khẩu là bắt buộc', modelToValidate)) return true;

            if (!FormValidationUtils.validateLength('Password', 8, 32, 'Mật khẩu ít nhất 8 ký tự', modelToValidate)) return true;

            if (!FormValidationUtils.validateRequire('PasswordConfirmation', 'Mật khẩu xác nhận là bắt buộc', modelToValidate)) return true;

            if (!FormValidationUtils.validateRequire('PhoneNumber', 'Số điện thoại là bắt buộc', modelToValidate)) return true;

            if (!FormValidationUtils.validateMatch('Password', 'PasswordConfirmation', 'Mật khẩu và mật khẩu xác nhận không giống nhau', modelToValidate)) return true;

            if (!FormValidationUtils.validateRequire('confirmedRegistration', 'Bạn chưa xác nhận thông tin', modelToValidate)) return true;

            if (!FormValidationUtils.validateTrue('confirmedRegistration', 'Bạn chưa xác nhận thông tin', modelToValidate)) return true;

            if (modelToValidate.UserNameInvalidNameError) return true;

            if (modelToValidate.UserNameInvalidPhoneNumberError) return true;

            return false;
        }
    }

})(window.angular);
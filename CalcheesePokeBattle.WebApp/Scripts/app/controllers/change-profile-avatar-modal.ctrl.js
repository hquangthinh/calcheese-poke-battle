﻿(function (angular) {
    'use strict';

    angular
        .module('CalcheeseFrontSite')
        .controller('ChangeProfileAvatarModalCtrl', ChangeProfileAvatarModalCtrl);

    ChangeProfileAvatarModalCtrl.$inject = ['$scope', '$state', '$uibModalInstance', 'UserService'];

    function ChangeProfileAvatarModalCtrl($scope, $state, $uibModalInstance, UserService) {

        var modalCtrl = this;
        $scope.selectedImage = '';
        $scope.resultImage = '';

        modalCtrl.saveChangeAvatar = function () {

            UserService.uploadProfileAvatar($scope.resultImage).then(function(uploadRes) {
                $uibModalInstance.close(uploadRes);
            });
        }

        modalCtrl.onFileSelect = function($files) {
            var file = $files[0];
            var reader = new FileReader();
            reader.onload = function (evt) {
                $scope.$apply(function ($scope) {
                    $scope.selectedImage = evt.target.result;
                });
            };
            reader.readAsDataURL(file);
        }

        modalCtrl.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        }
    }

})(window.angular);
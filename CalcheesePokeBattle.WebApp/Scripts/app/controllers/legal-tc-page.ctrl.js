﻿(function (document, angular, $, Ps) {
    'use strict';

    // Controller for modal legal-tc-page.html
    angular
        .module('CalcheeseFrontSite')
        .controller('LeagalTcPageCtrl', LeagalTcPageCtrl);

    LeagalTcPageCtrl.$inject = ['$scope', '$timeout'];

    function LeagalTcPageCtrl($scope, $timeout) {

        $scope.giftRedeemTc = false;
        $scope.luckyDrawTc = true;

        $scope.btnLuckyDrawImageUrl = function() {
            return $scope.luckyDrawTc ? 'Content/images/btn_luckydraw_active.png' : 'Content/images/btn_luckydraw.png';
        };

        $scope.btnGiftImageUrl = function () {
            return $scope.giftRedeemTc ? 'Content/images/btn_gift_active.png' : 'Content/images/btn_gift.png';
        };

        $timeout(function() {
                $(document).ready(function() {
                    var windowWidth = $(window).width();
                    var windowHeight = $(window).height();
                    if (windowWidth > 767) {
                        var classScroll = document.getElementsByClassName('modal-body-scroll');
                        for (var i = 0; i < classScroll.length; i++) {
                            Ps.initialize(classScroll[i],
                            {
                                wheelSpeed: 2,
                                wheelPropagation: true,
                                maxScrollbarLength: 25
                            });
                        };

                        if (windowHeight < 740) {
                            angular.element('#legal-tc-modal-content').addClass('content-custom-position');
                        } else {
                            angular.element('#legal-tc-modal-content').removeClass('content-custom-position');
                        }
                    }
                });
            },
            1000);

        $scope.viewGiftRedeemTc = function () {
            $scope.giftRedeemTc = true;
            $scope.luckyDrawTc = false;
            var container = document.getElementById('tc-scroll-container');
            container.scrollTop = 0;
            Ps.update(container);
        }

        $scope.viewLuckyDrawTc = function () {
            $scope.giftRedeemTc = false;
            $scope.luckyDrawTc = true;
            var container = document.getElementById('tc-scroll-container');
            container.scrollTop = 0;
            Ps.update(container);
        }
    }

    // Controller for modal uib-legal-tc-modal.html
    angular
        .module('CalcheeseFrontSite')
        .controller('LegalTcModalCtrl', LegalTcModalCtrl);

    LegalTcModalCtrl.$inject = ['$scope', '$uibModalInstance', '$timeout'];

    function LegalTcModalCtrl($scope, $uibModalInstance, $timeout) {

        var ctrl = this;

        ctrl.$onInit = function() {
            ctrl.giftRedeemTc = false;
            ctrl.luckyDrawTc = true;

            $timeout(function() {
                    $(document).ready(function() {
                        var classScroll = document.getElementsByClassName('modal-body-scroll');
                        for (var i = 0; i < classScroll.length; i++) {
                            Ps.initialize(classScroll[i],
                            {
                                wheelSpeed: 2,
                                wheelPropagation: true,
                                maxScrollbarLength: 25
                            });
                        };
                    });
                },
                1000);
        }

        ctrl.viewGiftRedeemTc = function() {
            ctrl.giftRedeemTc = true;
            ctrl.luckyDrawTc = false;
        }

        ctrl.viewLuckyDrawTc = function () {
            ctrl.giftRedeemTc = false;
            ctrl.luckyDrawTc = true;
        }

        ctrl.close = close;

        function close() {
            $uibModalInstance.dismiss('cancel');
        }

    }

})(window.document, window.angular, window.$, window.Ps);
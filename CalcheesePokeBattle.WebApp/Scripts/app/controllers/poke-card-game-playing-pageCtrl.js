﻿(function (document, angular, $) {
    'use strict';

    angular
        .module('CalcheeseFrontSite')
        .controller('PokeCardGamePlayingPageCtrl', PokeCardGamePlayingPageCtrl);

    PokeCardGamePlayingPageCtrl.$inject = ['$rootScope', '$scope', '$log', '$window', '$interval', '$timeout', '$uibModal', 'url', 'config',
        'BaseCtrlAs', 'UserService', 'GameClientService', 'GameHubService', 'MessageAlertService', 'webBaseUrl', 'PageViewTrackingService'];

    function PokeCardGamePlayingPageCtrl($rootScope, $scope, $log, $window, $interval, $timeout, $uibModal, url, config,
        BaseCtrlAs, UserService, GameClientService, GameHubService, MessageAlertService, webBaseUrl, PageViewTrackingService) {

        var ctrl = this;
        BaseCtrlAs.extend(ctrl, $scope, 'game_hub_online_players');

        function fetchGameSessionDataOnStartGameRound() {
            var gameSessionId = ctrl._params.gameSessionId;
            ctrl.model = {};
            return GameHubService.getGamePlayInfoFromGameSession(gameSessionId).then(function (gamePlayRes) {
                if (!gamePlayRes || !gamePlayRes.data) {
                    MessageAlertService.showModalError('Lỗi kết nối game');
                    return { success: false };
                }
                ctrl.model.GameSessionId = gamePlayRes.data.GameSessionId;
                ctrl.PlayerOnLeft = gamePlayRes.data.PlayerOnLeft;
                // show first card only for mobile view
                ctrl.PlayerOnLeft.CurrentMobileCardOnDeckIndex = 0;
                ctrl.PlayerOnLeft.CurrentCardOnDeckForMobileView = ctrl.PlayerOnLeft.AllUserCards[ctrl.PlayerOnLeft.CurrentMobileCardOnDeckIndex];

                // store the card infor after submit card action
                // ctrl.model.TempSelectedCard is card select before submit card action
                ctrl.PlayerOnLeft.SelectedCard = {};
                ctrl.PlayerOnLeft.HasSubmitCardManually = false;

                ctrl.PlayerOnRight = gamePlayRes.data.PlayerOnRight;
                ctrl.PlayerOnRight.SelectedCard = {};

                // check again if player has enough points/cards to play
                if (ctrl.PlayerOnLeft.TotalPoint < config.MIN_POINTS_TO_PLAY_GAME) {
                    // kick this player when total point is lower than threshold
                    ctrl.stopGameRequest();
                    return gamePlayRes;
                }
                if (!ctrl.PlayerOnLeft.AllUserCards || ctrl.PlayerOnLeft.AllUserCards.length === 0) {
                    // kick this player when has no card
                    ctrl.stopGameRequest();
                    return gamePlayRes;
                }

                ctrl.TimerCountDown = config.GameDurationTimeOut;
                ctrl.stopTimer = $interval(ctrl.updateTimerCountDown, 1000);

                return gamePlayRes;
            }).catch(function (err) {
                ctrl.goHome();
            });
        }

        ctrl.$onInit = function () {

            fetchGameSessionDataOnStartGameRound();

            $timeout(function() {
                    $(document).ready(function() {
                        var classScroll = document.getElementsByClassName('game-list-cards');
                        for (var i = 0; i < classScroll.length; i++) {
                            Ps.initialize(classScroll[i],
                            {
                                wheelSpeed: 2,
                                wheelPropagation: true,
                                maxScrollbarLength: 15
                            });
                        };
                    });
                },
                1000);

            PageViewTrackingService.trackPageView({
                PageTitle: 'choi-game',
                PageUrl: webBaseUrl + '/choi-game',
                UserId: ctrl.CurrentUser.Id,
                UserName: ctrl.CurrentUser.UserName
            });
        }

        ctrl.selectCard = selectCard;
        ctrl.noCardSelectedOrUserHasSubmitted = noCardSelectedOrUserHasSubmitted;
        ctrl.submitCard = submitCard;
        ctrl.submitCardForMobile = submitCardForMobile;
        ctrl.selectRandomCardAndSubmit = selectRandomCardAndSubmit;
        ctrl.stopGameRequest = stopGameRequest;
        ctrl.updateTimerCountDown = updateTimerCountDown;
        ctrl.updateWinnerGameScene = updateWinnerGameScene;
        ctrl.updateLooserGameScene = updateLooserGameScene;
        ctrl.updateGameTieGameScene = updateGameTieGameScene;
        ctrl.turnOnPlayerCardsForWinner = turnOnPlayerCardsForWinner;
        ctrl.turnOnPlayerCardsForLooser = turnOnPlayerCardsForLooser;
        ctrl.turnOnPlayerCardsForGameTie = turnOnPlayerCardsForGameTie;
        ctrl.goHome = goHome;
        ctrl.nextCardOnDeck = nextCardOnDeck;
        ctrl.toggleCardDeckForMobile = toggleCardDeckForMobile;

        function noCardSelectedOrUserHasSubmitted() {
            return !ctrl.model.TempSelectedCard || !ctrl.model.TempSelectedCard.Id || ctrl.model.TempSelectedCard.Id === 0 ||
                (ctrl.PlayerOnLeft.SelectedCard && ctrl.PlayerOnLeft.SelectedCard.Id && ctrl.PlayerOnLeft.SelectedCard.Id > 0);
        }

        function selectCard(card) {
            ctrl.model.TempSelectedCard = card;
            ctrl.PlayerOnLeft.CurrentCardOnDeckForMobileView = card;
        }

        function submitCardForMobile() {
            angular.element('#game-left-container').removeClass('show-card');
            submitCard();
        }

        function submitCard() {

            if (ctrl.TimerCountDown <= 0)
                return;

            if (!ctrl.model.TempSelectedCard || ctrl.model.TempSelectedCard.Id === 0) {
                MessageAlertService.showToastrMessage('Bạn bạn chưa chọn thẻ để xuất trận');
                return;
            }

            ctrl.PlayerOnLeft.SelectedCard = ctrl.model.TempSelectedCard;
            ctrl.PlayerOnLeft.HasSubmitCardManually = true;
            performSubmitCardFlyAnimation(ctrl.PlayerOnLeft.SelectedCard);

            // lock user from submit card again while waiting for game result
            // stop timer
            stopCoundownTimer();

            // send card information to server
            var submitCardCommand = {
                GameSessionId: ctrl.model.GameSessionId,
                PlayerUserId: ctrl.PlayerOnLeft.Id,
                PlayerUserName: ctrl.PlayerOnLeft.UserName,
                PlayerCardId: ctrl.PlayerOnLeft.SelectedCard.Id,
                PlayerTimeSpentForCardSelection: ctrl.TimerCountDown
            };

            $log.debug('call for submitPokemonCard -> ', submitCardCommand);

            GameClientService.submitPokemonCard(submitCardCommand)
                .done(function() {
                    $log.debug('submitPokemonCard done');
                    MessageAlertService.showToastrMessage('Bạn đã xuất thẻ ' + ctrl.PlayerOnLeft.SelectedCard.CardName + ' hãy chờ kết quả');
                    ctrl.model.TempSelectedCard = null;
                })
                .fail(function (error) {
                    $log.debug('submitPokemonCard -> ', error);
                    ctrl.model.TempSelectedCard = null;
                    MessageAlertService.showModalError('Lỗi kết nối game. Đối thủ của bạn đã dừng trận đấu.').result.then(function() {
                        ctrl._go(ctrl._router.POKE_CARD_GAME_JOIN_GAME, {}, { reload: true });
                    },function() {
                        ctrl._go(ctrl._router.POKE_CARD_GAME_JOIN_GAME, {}, { reload: true });
                    });
                });
        }

        function selectRandomCardAndSubmit() {
            if (ctrl.PlayerOnLeft.HasSubmitCardManually)
                return;
            // get the card on top to submit cause cards already sorted by power descending
            var topMostPowerfullCard = ctrl.PlayerOnLeft.AllUserCards[0];
            ctrl.selectCard(topMostPowerfullCard);
            submitCardRandomCard();
        }

        function submitCardRandomCard() {

            ctrl.PlayerOnLeft.SelectedCard = ctrl.model.TempSelectedCard;
            ctrl.PlayerOnLeft.HasSubmitCardManually = false;
            performSubmitCardFlyAnimation(ctrl.PlayerOnLeft.SelectedCard);

            // send card information to server
            var submitCardCommand = {
                GameSessionId: ctrl.model.GameSessionId,
                PlayerUserId: ctrl.PlayerOnLeft.Id,
                PlayerUserName: ctrl.PlayerOnLeft.UserName,
                PlayerCardId: ctrl.PlayerOnLeft.SelectedCard.Id,
                PlayerTimeSpentForCardSelection: ctrl.TimerCountDown
            };

            $log.debug('call for submitCardRandomCard -> ', submitCardCommand);

            GameClientService.submitPokemonCard(submitCardCommand)
                .done(function () {
                    $log.debug('submitPokemonCard done');
                    MessageAlertService.showToastrMessage('Bạn đã xuất thẻ ' + ctrl.PlayerOnLeft.SelectedCard.CardName + ' hãy chờ kết quả');
                    ctrl.model.TempSelectedCard = null;
                })
                .fail(function (error) {
                    $log.debug('submitPokemonCard -> ', error);
                    ctrl.model.TempSelectedCard = null;
                    MessageAlertService.showModalError('Lỗi kết nối game. Đối thủ của bạn đã dừng trận đấu.').result.then(function () {
                        ctrl._go(ctrl._router.POKE_CARD_GAME_JOIN_GAME, {}, { reload: true });
                    }, function () {
                        ctrl._go(ctrl._router.POKE_CARD_GAME_JOIN_GAME, {}, { reload: true });
                    });
                });
        }

        function toggleCardDeckForMobile() {
            // show card deck on mobile view
            angular.element('#game-left-container').toggleClass('show-card');
        }

        function nextCardOnDeck() {
            var currentMobileCardIndex = ctrl.PlayerOnLeft.CurrentMobileCardOnDeckIndex;
            var totalCardUserHas = ctrl.PlayerOnLeft.AllUserCards.length;
            if (currentMobileCardIndex < totalCardUserHas - 1) {
                ctrl.PlayerOnLeft.CurrentMobileCardOnDeckIndex = ctrl.PlayerOnLeft.CurrentMobileCardOnDeckIndex + 1;
            } else {
                ctrl.PlayerOnLeft.CurrentMobileCardOnDeckIndex = 0;
            }
            ctrl.PlayerOnLeft.CurrentCardOnDeckForMobileView = ctrl.PlayerOnLeft.AllUserCards[ctrl.PlayerOnLeft.CurrentMobileCardOnDeckIndex];
        }

        function goHome() {
            ctrl._go(ctrl._router.POKE_CARD_GAME_JOIN_GAME, {}, { reload: true });
        }

        function stopGameRequest() {
            var stopGameReqCommand = {
                GameSessionId: ctrl.model.GameSessionId,
                RequestUserName: ctrl.PlayerOnLeft.UserName,
                RequestUserId: ctrl.PlayerOnLeft.Id
            };
            GameClientService.stopGameRequest(stopGameReqCommand)
                .done(function() {
                    ctrl.goHome();
                })
                .fail(function() {
                    ctrl.goHome();
                });
        }

        function stopGameOnNavigateAwayPlayingPage() {
            var stopGameReqCommand = {
                GameSessionId: ctrl.model.GameSessionId,
                RequestUserName: ctrl.PlayerOnLeft.UserName,
                RequestUserId: ctrl.PlayerOnLeft.Id
            };
            return GameClientService.stopGameRequest(stopGameReqCommand);
        }

        function stopCoundownTimer() {
            $interval.cancel(ctrl.stopTimer);
        }

        function updateTimerCountDown() {
            ctrl.TimerCountDown -= 1;
            if (ctrl.TimerCountDown === 0) {
                $log.debug('// auto submit game & stop timer');
                stopCoundownTimer();
                // disable manual card select

                ctrl.selectRandomCardAndSubmit();
            } else if (ctrl.TimerCountDown < 0) {
                // exit game
                stopGameRequest();
            }
        }

        // turn on front site of 2 player cards on table
        function getFirstString(fullString) {
            return fullString.split('/')[0];
        }
        // perform animation
        function turnOnPlayerCardsForWinner(gameResult) {
            // left card already turn on, now reveal right card
            ctrl.PlayerOnRight.SelectedCard = {
                ImageUrl: gameResult.LostCardImageUrl,
                CardElementImageUrl: gameResult.LostCardElementImageUrl,
                CardElementDisplay: gameResult.LostCardElementDisplay,
                IsSpecialCard: gameResult.LostIsSpecialCard,
                CardTypeDisplay: gameResult.LostCardTypeDisplay
            };
            ctrl.PlayerOnRight.SelectedCard.MainCardElementDisplay = getFirstString(ctrl.PlayerOnRight.SelectedCard.CardElementDisplay);
        }

        function turnOnPlayerCardsForLooser(gameResult) {
            // left card already turn on, now reveal right card
            ctrl.PlayerOnRight.SelectedCard = {
                ImageUrl: gameResult.WonCardImageUrl,
                CardElementImageUrl: gameResult.WonCardElementImageUrl,
                CardElementDisplay: gameResult.WonCardElementDisplay,
                IsSpecialCard: gameResult.WonIsSpecialCard,
                CardTypeDisplay: gameResult.WonCardTypeDisplay
            };
            ctrl.PlayerOnRight.SelectedCard.MainCardElementDisplay = getFirstString(ctrl.PlayerOnRight.SelectedCard.CardElementDisplay);
        }

        function turnOnPlayerCardsForGameTie(gameResult) {
            // left card already turn on, now reveal right card
            if (ctrl.PlayerOnLeft.SelectedCard.Id === gameResult.LeftCardId) {
                ctrl.PlayerOnRight.SelectedCard = {
                    ImageUrl: gameResult.RightCardImageUrl,
                    CardElementImageUrl: gameResult.RightCardElementImageUrl,
                    CardElementDisplay: gameResult.RightCardElementDisplay,
                    IsSpecialCard: gameResult.RightIsSpecialCard,
                    CardTypeDisplay: gameResult.RightCardTypeDisplay
                };
                ctrl.PlayerOnRight.SelectedCard.MainCardElementDisplay = getFirstString(ctrl.PlayerOnRight.SelectedCard.CardElementDisplay);
            }
            else if (ctrl.PlayerOnLeft.SelectedCard.Id === gameResult.RightCardId) {
                ctrl.PlayerOnRight.SelectedCard = {
                    ImageUrl: gameResult.LeftCardImageUrl,
                    CardElementImageUrl: gameResult.LeftCardElementImageUrl,
                    CardElementDisplay: gameResult.LeftCardElementDisplay,
                    IsSpecialCard: gameResult.LeftIsSpecialCard,
                    CardTypeDisplay: gameResult.LeftCardTypeDisplay
                };
                ctrl.PlayerOnRight.SelectedCard.MainCardElementDisplay = getFirstString(ctrl.PlayerOnRight.SelectedCard.CardElementDisplay);
            }
        }

        // update card deck and point info for winner
        function updateWinnerGameScene(gameResult) {

            $log.debug('updateWinnerGameScene -> ', gameResult);

            if (!gameResult.CanLooserContinueToPlayNextRound) {
                MessageAlertService.showModalError('Đối thủ của bạn không còn đủ điểm để tiếp tục chơi.')
                    .result.then(function() {
                            ctrl._go(ctrl._router.POKE_CARD_GAME_JOIN_GAME, {}, { reload: true });
                        },
                        function() {
                            ctrl._go(ctrl._router.POKE_CARD_GAME_JOIN_GAME, {}, { reload: true });
                        });
            } else {
                // add card to winner card deck
                fetchGameSessionDataOnStartGameRound();
                resetScreenElementsForNewGameRound();
            }
        }

        // update card deck and point info for looser
        function updateLooserGameScene(gameResult) {

            $log.debug('updateLooserGameScene -> ', gameResult);

            // check if looser can continue to play next round
            if (!gameResult.CanLooserContinueToPlayNextRound) {
                MessageAlertService.showModalError(gameResult.GameStopReason).result.then(function() {
                        ctrl._go(ctrl._router.HOME, {}, { reload: true });
                    },
                    function() {
                        ctrl._go(ctrl._router.HOME, {}, { reload: true });
                    });
            } else {
                // remove card from looser's card deck'
                fetchGameSessionDataOnStartGameRound();
                resetScreenElementsForNewGameRound();
            }
        }

        function updateGameTieGameScene(gameResult) {
            $log.debug('updateGameTieGameScene -> ', gameResult);
            fetchGameSessionDataOnStartGameRound();
            resetScreenElementsForNewGameRound();
        }

        function resetScreenElementsForNewGameRound() {
            $('#left-player-selected-card-img')
                .css({
                    "display": "none"
                });
        }


        // event listeners
        $rootScope.$on('game-hub:game-result-success',
            function (evt, evtData) {
                // stop coundown timer
                $interval.cancel(ctrl.stopTimer);
                // show result modal and perform scene update according to game result
                // winner sees win modal
                // looser sees loose modal
                // game tie both players see draw modal
                // modal should auto close after several seconds
                // game count down only restarted when 2 players ready again
                if (evtData.HasWinner && evtData.WonUserId === ctrl.CurrentUser.Id) {

                    ctrl.turnOnPlayerCardsForWinner(evtData);
                     //show result after 1s
                    $timeout(function() {
                            var winInfo = {
                                imageUrl: evtData.LostCardImageUrl,
                                point: evtData.LostPoints
                            };

                            MessageAlertService.showWinGameModal(winInfo)
                                .result.then(function() {
                                        ctrl.updateWinnerGameScene(evtData);
                                    },
                                    function() {
                                        ctrl.updateWinnerGameScene(evtData);
                                    });
                        },
                        config.DelayBeforeRevealGameResult);
                }
                else if (evtData.HasWinner && evtData.LostUserId === ctrl.CurrentUser.Id) {

                    ctrl.turnOnPlayerCardsForLooser(evtData);
                    //show result after 1s
                    $timeout(function() {
                            var looseInfo = {
                                imageUrl: evtData.LostCardImageUrl,
                                point: evtData.LostPoints
                            };
                            MessageAlertService.showLostGameModal(looseInfo)
                                .result.then(function() {
                                        ctrl.updateLooserGameScene(evtData);
                                    },
                                    function() {
                                        ctrl.updateLooserGameScene(evtData);
                                    });

                        },
                        config.DelayBeforeRevealGameResult);
                }
                else if (!evtData.HasWinner) {

                    // game tie 2 cards are same
                    ctrl.turnOnPlayerCardsForGameTie(evtData);
                    //show result after 1s
                    $timeout(function() {
                            var tieInfo = {
                                imageUrl: ctrl.PlayerOnRight.SelectedCard.ImageUrl
                            };
                            MessageAlertService.showGameTieModal(tieInfo)
                                .result.then(function() {
                                        ctrl.updateGameTieGameScene(evtData);
                                    },
                                    function() {
                                        ctrl.updateGameTieGameScene(evtData);
                                    });
                        },
                        config.DelayBeforeRevealGameResult);
                }
            });

        $rootScope.$on('game-hub:game-result-error',
            function (evt, evtData) {
                $interval.cancel(ctrl.stopTimer);
                MessageAlertService.showModalError('Có lỗi xảy ra, vui lòng bắt đầu lại trận đấu')
                    .result.then(fetchGameSessionDataOnStartGameRound, fetchGameSessionDataOnStartGameRound);
            });

        $rootScope.$on('game-hub:game-end',
            function (evt, evtData) {
                $interval.cancel(ctrl.stopTimer);
                var opponentName = ctrl.PlayerOnRight.FullName || ctrl.PlayerOnRight.UserName;
                MessageAlertService.showPlayerLeaveGameModal(opponentName)
                    .result.then(ctrl.goHome, ctrl.goHome);
            });

        $rootScope.$on('game-hub:player-disconnect',
            function (evt, evtData) {
                if (evtData && evtData.AffectedUserIds.indexOf(ctrl.CurrentUser.Id) > -1) {
                    $interval.cancel(ctrl.stopTimer);
                    var opponentName = ctrl.PlayerOnRight.FullName || ctrl.PlayerOnRight.UserName;
                    MessageAlertService.showPlayerLeaveGameModal(opponentName)
                        .result.then(ctrl.goHome, ctrl.goHome);
                }
            });

        $scope.$on("$destroy",
            function () {
                $log.debug('scope destroy');
                if (ctrl.stopTimer) {
                    $interval.cancel(ctrl.stopTimer);
                }
                //try stop game session
                if (ctrl.model && ctrl.PlayerOnLeft) {
                    stopGameOnNavigateAwayPlayingPage();
                }
            });

        // private functions

        function performSubmitCardFlyAnimation(cardToAnimate) {
            var cardElId = '#user-card-' + cardToAnimate.Id;

            var cardJqEl = angular.element(cardElId);
            var selectedCardImgJqEl = angular.element('#left-player-selected-card');

            set_position_img(cardJqEl, selectedCardImgJqEl);
        }

        function set_position_img(card, l_img) {
            
            var t_l = $(l_img).offset().top;
            var l_l = $(l_img).offset().left;
            var w_l = $(l_img).width();
            var h_l = $(l_img).height();

            var t = $(card).offset().top;
            var l = $(card).offset().left;
            var w = $(card).width();
            var h = $(card).height();

            $('#left-player-selected-card-img')
                .css({
                    "position": "fixed",
                    "top": t,
                    "left": l,
                    "width": w,
                    "height": h,
                    "display": "block"
                })
                .animate({
                        "top": t_l,
                        "left": l_l,
                        "width": w_l,
                        "height": h_l
                    },
                    function() {
                        $(this).css({
                            "position": "relative",
                            "top": 0,
                            "left": 0
                        });
                    });
        }
    }

})(window.document, window.angular, window.$);
﻿(function (angular) {
    'use strict';

    angular
        .module('CalcheeseFrontSite')
        .controller('ChangePasswordModalCtrl', ChangePasswordModalCtrl);

    ChangePasswordModalCtrl.$inject = ['$state', '$uibModal', '$uibModalInstance', 'config', 'UserService', 'MessageAlertService', 'FormValidationUtils'];

    function ChangePasswordModalCtrl($state, $uibModal, $uibModalInstance, config, UserService, MessageAlertService, FormValidationUtils) {

        var modalCtrl = this;

        modalCtrl.$onInit = function () {
            modalCtrl.processing = false;
            modalCtrl.model = {};
        };

        modalCtrl.validateInput = validateInput;
        modalCtrl.doChangePassword = doChangePassword;

        function doChangePassword() {

            var inputIsInvalid = validateInput(modalCtrl.model);
            if (inputIsInvalid)
                return false;

            UserService.changePassword(modalCtrl.model).then(function (changePwdRes) {
                if (changePwdRes && changePwdRes.data && changePwdRes.data.success) {
                    MessageAlertService.showModalInfo('Mật khẩu của bạn đã được đổi, vui lòng đăng nhập lại.')
                        .result.then(logoutAfterChangePassword, logoutAfterChangePassword);
                } else {
                    MessageAlertService.showModalInfo(changePwdRes.data.error.message);
                }
            });
        }

        function logoutAfterChangePassword() {
            $state.go(config.ROUTER.DEFAULT, {}, { reload: true });
        }

        modalCtrl.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        }

        function validateInput(modelToValidate) {
            // return true if input is invalid

            if (!FormValidationUtils.validateRequire('Password', 'Mật khẩu là bắt buộc', modelToValidate)) return true;

            if (!FormValidationUtils.validateLength('Password', 8, 32, 'Mật khẩu ít nhất 8 ký tự', modelToValidate)) return true;

            if (!FormValidationUtils.validateRequire('PasswordConfirmation', 'Mật khẩu xác nhận là bắt buộc', modelToValidate)) return true;

            if (!FormValidationUtils.validateMatch('Password', 'PasswordConfirmation', 'Mật khẩu và mật khẩu xác nhận không giống nhau', modelToValidate)) return true;

            return false;
        }
    }

})(window.angular);
﻿(function (angular) {
    'use strict';

    angular
        .module('CalcheeseFrontSite')
        .controller('JoinGameModalCtrl', JoinGameModalCtrl);

    JoinGameModalCtrl.$inject = ['$scope', '$uibModalInstance', 'url', 'BaseCtrlAs', 'UserService', 'MessageAlertService', 'GameClientService', 'GameHubService', 'config'];

    function JoinGameModalCtrl($scope, $uibModalInstance, url, BaseCtrlAs, UserService, MessageAlertService, GameClientService, GameHubService, config) {

        var ctrl = this;

        ctrl.$onInit = function () {
            //console.log('JoinGameModalCtrl');
            BaseCtrlAs.extend(ctrl, $scope, 'game_hub_online_players');
            ctrl.request.params = {
                PageNumber: 1,
                PageSize: config.PAGE_SIZE
            };
            GameHubService.getAllOnlinePlayers({ params: ctrl.request.params })
                .then(function (playersOnlineRes) {
                    //console.log('playersOnlineRes -> ', playersOnlineRes);
                    ctrl.result = playersOnlineRes.data;
                });
        }

        ctrl.cancel = cancel;
        ctrl.inviteToJoinGame = inviteToJoinGame;

        function inviteToJoinGame(playerInfo) {
            //console.log('playerInfo -> ', playerInfo);
            //console.log('ctrl.CurrentUser -> ', ctrl.CurrentUser);
            if (ctrl.CurrentUser.TotalPoint < config.MIN_POINTS_TO_PLAY_GAME) {
                MessageAlertService.showErrorMessage('Bạn cần ít nhất 7 điểm để giao đấu');
                return;
            }
            GameClientService.sendNewGameRequest({
                    PlayerUserId: ctrl.CurrentUser.Id,
                    PlayerUserName: ctrl.CurrentUser.UserName,
                    PlayerName: ctrl.CurrentUser.FullName || ctrl.CurrentUser.UserName,
                    PlayerTotalPoint: playerInfo.TotalPoints,
                    InvitedPlayerUserId: playerInfo.UserId,
                    InvitedPlayerUserName: playerInfo.UserName,
                    InvitedPlayerName: playerInfo.FullName || playerInfo.UserName
                })
                .done(function() {
                    MessageAlertService.showToastrMessage('Thách đấu đã gửi đến ' +
                        (playerInfo.FullName || playerInfo.UserName) +
                        '. Đang chờ chấp nhận.');
                    $uibModalInstance.close({
                        success: true,
                        data: playerInfo
                    });
                });
        }

        function cancel() {
            $uibModalInstance.dismiss('cancel');
        }

    }

})(window.angular);
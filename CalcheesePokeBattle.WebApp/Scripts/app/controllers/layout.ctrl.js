﻿(function (angular) {
    'use strict';

    angular
        .module('CalcheeseFrontSite')
        .controller('LayoutCtrl', LayoutCtrl);

    LayoutCtrl.$inject = ['$rootScope', '$scope', '$state', '$uibModal', '$log', 'url', 'config', 'UserService', 'Socialshare'];

    function LayoutCtrl($rootScope, $scope, $state, $uibModal, $log, url, config, UserService, Socialshare) {
        $scope.dateTimeFormat = 'EEEE, d MMMM yyyy - h:mm:ss a';
        $scope._router = config.ROUTER;
        $scope.$state = $state;
        $scope.appVersion = config.VERSION;
        $scope.webUrl = $rootScope.webUrl;
        $scope.CurrentUser = UserService.CurrentUser;
        $scope.logoutLabel = 'Thoát ';

        $scope.openMobileMenu = openMobileMenu;
        $scope.closeMobileMenu = closeMobileMenu;

        $scope.signInRequest = function() {
            $uibModal.open({
                templateUrl: url.tmpl('/v2-modals/login_modal_mobile.html'),
                controller: "LoginModalCtrl as modalCtrl",
                size: "lg",
                windowTopClass: "modal-sign-in-window"
            });
        }

        $scope.goToPage = function(pageState) {
            if (!$scope.CurrentUser) {
                $scope.signInRequest();
                return;
            } else {
                $rootScope.$state.go(pageState);
            }
        }

        $scope.goToPageWithCheck = function (pageState) {
            if (!$scope.CurrentUser)
                return;
            if ($scope.CurrentUser.UserPermission.CanUserAccessTestFeatures) {
                $rootScope.$state.go(pageState);
            }
        }

        $scope.goToPageMobile = function (pageState, allowPublicAccess) {
            closeMobileMenu();
            // change state
            if (!$scope.CurrentUser && !allowPublicAccess) {
                $scope.signInRequest();
                return;
            } else {
                $rootScope.$state.go(pageState);
            }
        }

        $scope.goToPageMobileWithCheck = function (pageState, allowPublicAccess) {
            closeMobileMenu();
            $scope.goToPageWithCheck(pageState);
        }

        function openMobileMenu() {
            angular.element('#mobile-nav-menu').slideDown(300);
            angular.element('html').css({ 'position': 'fixed' });
        }

        function closeMobileMenu() {
            // close mobile menu
            angular.element('#mobile-nav-menu').slideUp(300);
            angular.element('html').css({ 'position': 'relative' });
        }

        $scope.shareFacebook = function() {
            Socialshare.share({
                'provider': 'facebook',
                'attrs': {
                    'socialshareUrl': 'http://calcheese.vn'
                }
            });
        }
    }

})(window.angular);
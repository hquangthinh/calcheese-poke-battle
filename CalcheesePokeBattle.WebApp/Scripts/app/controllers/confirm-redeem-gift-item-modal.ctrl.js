﻿(function (document, angular, $, Ps) {
    'use strict';

    angular
        .module('CalcheeseFrontSite')
        .controller('ConfirmRedeemGiftItemModalCtrl', ConfirmRedeemGiftItemModalCtrl);

    ConfirmRedeemGiftItemModalCtrl.$inject = ['$scope', '$timeout', '$uibModalInstance', 'config', 'UserService', 'FormValidationUtils'];

    function ConfirmRedeemGiftItemModalCtrl($scope, $timeout, $uibModalInstance, config, UserService, FormValidationUtils) {
        var ctrl = this;

        ctrl.$onInit = function () {
            var currentUser = UserService.CurrentUser;
            ctrl.userRedeemGiftModel = {
                FirstName: currentUser.FirstName,
                LastName: currentUser.LastName,
                FullName: currentUser.FullName,
                Address: currentUser.Address,
                AddressNumber: currentUser.AddressNumber,
                StreetName: currentUser.StreetName,
                WardName: currentUser.WardName,
                DistrictName: currentUser.DistrictName,
                CityName: currentUser.CityName,
                DeliveryAddress: currentUser.DeliveryAddress,
                PhoneNumber: currentUser.PhoneNumber,
                PhoneNumberConfirmed: currentUser.PhoneNumber,
                SocialIdNumber: currentUser.SocialIdNumber
            };
            ctrl.allCities = config.AllCities;

            $timeout(function() {
                    $(document).ready(function() {
                        var windowWidth = $(window).width();
                        if (windowWidth > 767) {
                            var classScroll = document.getElementsByClassName('form-body-scroll-container');
                            for (var i = 0; i < classScroll.length; i++) {
                                Ps.initialize(classScroll[i],
                                {
                                    wheelSpeed: 2,
                                    wheelPropagation: true,
                                    maxScrollbarLength: 110
                                });
                            };
                        }
                        if (windowWidth <= 767) {
                            angular.element('#btnCloseDialog').css('transform', 'rotate(360deg)');
                        }
                    });
                },
                1000);
        }

        ctrl.confirmRedeemGiftItem = confirmUserInfoAndCloseModal;
        ctrl.confirmRedeemLuckyDrawCode = confirmUserInfoAndCloseModal;
        ctrl.close = close;

        function confirmUserInfoAndCloseModal() {
            // validate input -> return true if input is invalid
            var inputIsInvalid = validateInput(ctrl.userRedeemGiftModel);
            if (inputIsInvalid)
                return false;

            $uibModalInstance.close(ctrl.userRedeemGiftModel);
        }

        function close() {
            $uibModalInstance.dismiss('cancel');
        }

        function validateInput(modelToValidate) {

            if (!FormValidationUtils.validateRequire('FirstName', 'Họ và Tên là bắt buộc', modelToValidate)) return true;

            if (!FormValidationUtils.validateRequire('LastName', 'Họ và Tên là bắt buộc', modelToValidate)) return true;

            if (!FormValidationUtils.validateRequire('AddressNumber', 'Số nhà là bắt buộc', modelToValidate)) return true;

            if (!FormValidationUtils.validateRequire('StreetName', 'Tên đường là bắt buộc', modelToValidate)) return true;

            if (!FormValidationUtils.validateRequire('WardName', 'Phường/Xã là bắt buộc', modelToValidate)) return true;

            if (!FormValidationUtils.validateRequire('DistrictName', 'Quận/Huyện là bắt buộc', modelToValidate)) return true;

            if (!FormValidationUtils.validateRequire('CityName', 'Tỉnh/Thành Phố là bắt buộc', modelToValidate)) return true;

            if (!FormValidationUtils.validateRequire('PhoneNumber', 'Số điện thoại là bắt buộc', modelToValidate)) return true;

            if (!FormValidationUtils.validateRequire('PhoneNumberConfirmed', 'Số điện thoại xác nhận là bắt buộc', modelToValidate)) return true;

            if (!FormValidationUtils.validateMatch('PhoneNumber', 'PhoneNumberConfirmed', 'Số điện thoại và Số điện thoại xác nhận không giống nhau', modelToValidate)) return true;

            return false;
        }
    }

})(window.document, window.angular, window.$, window.Ps);
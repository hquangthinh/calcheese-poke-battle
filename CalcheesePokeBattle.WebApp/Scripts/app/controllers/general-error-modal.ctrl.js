﻿(function (angular) {
    'use strict';

    angular
        .module('CalcheeseFrontSite')
        .controller('GeneralErrorModalCtrl', GeneralErrorModalCtrl);

    GeneralErrorModalCtrl.$inject = ['$uibModalInstance', 'MessageParams'];

    function GeneralErrorModalCtrl($uibModalInstance, MessageParams) {
        var ctrl = this;

        ctrl.$onInit = function() {
            ctrl.errorMessage = (MessageParams && MessageParams.errorMessage) || 'Có lỗi xảy ra bạn vui lòng thử lại';
        }

        ctrl.close = function() {
            $uibModalInstance.dismiss('cancel');
        }
    }

})(window.angular);

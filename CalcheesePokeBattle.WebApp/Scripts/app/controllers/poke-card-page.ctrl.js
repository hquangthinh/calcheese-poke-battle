﻿(function (angular) {
    'use strict';

    angular
        .module('CalcheeseFrontSite')
        .controller('PokeCardPageCtrl', PokeCardPageCtrl);

    PokeCardPageCtrl.$inject = ['$scope', '$uibModal', 'url', 'BaseCtrlAs', '$location', '$anchorScroll'];

    function PokeCardPageCtrl($scope, $uibModal, url, BaseCtrlAs, $location, $anchorScroll) {
        var ctrl = this;
        BaseCtrlAs.extend(ctrl, $scope, 'pokecards');
        //init value
        ctrl.currentCard = {};
        ctrl.indexCurrentCard = -1;
        ctrl.request.params.PageNumber = 1;

        ctrl.indexRecord = function () {
            if (ctrl.request.params.PageNumber === 1)
                return ctrl.indexCurrentCard + 1;
            else
                return 8 + (ctrl.request.params.PageSize * (ctrl.request.params.PageNumber - 2)) + ctrl.indexCurrentCard;
        }
       
        ctrl._preGet = function () {
            if (ctrl.request.params.PageNumber === 1) ctrl.request.params.PageSize = 7;
            else ctrl.request.params.PageSize = 10;
            return true;
        }

        ctrl._init = function () {
            ctrl.indexCurrentCard = -1;
            ctrl.nextItem();
        }

        ctrl.nextItem = function () {
            if ((ctrl.indexCurrentCard + 1) < ctrl.result.ResultSet.length) {
                ctrl.indexCurrentCard = ctrl.indexCurrentCard + 1;
            }else{
                ctrl.indexCurrentCard = 0;
            }
            ctrl.currentCard = ctrl.result.ResultSet[ctrl.indexCurrentCard];
        }
        ctrl.backItem = function () {
            if ((ctrl.indexCurrentCard ) > 0 ) {
                ctrl.indexCurrentCard = ctrl.indexCurrentCard -1;
            } else {
                ctrl.indexCurrentCard = ctrl.result.ResultSet.length -1;
            }
            ctrl.currentCard = ctrl.result.ResultSet[ctrl.indexCurrentCard];
        }

        ctrl.backPage = function () {
            if (ctrl.request.params.PageNumber === 1) return;
            ctrl.request.params.PageNumber = ctrl.request.params.PageNumber - 1;
            ctrl.searchPage();
        }

        ctrl.nextPage = function () {
            if (ctrl.request.params.PageNumber === ctrl.result.PageTotal) return;
            ctrl.request.params.PageNumber = ctrl.request.params.PageNumber + 1;
            ctrl.searchPage();
        }

        ctrl.searchPage = function () {
            if (ctrl._preGet()) {
                ctrl.request.get().then(function () {
                    ctrl._init();
                });
            }
        }

        ctrl.detailItem = function (card) {

            for (var i = 0; i < ctrl.result.ResultSet.length; i++) {
                if (card === ctrl.result.ResultSet[i]) {
                    ctrl.indexCurrentCard = i;
                    ctrl.currentCard = card;
                    break;
                }
            }
            $location.hash('carDetailTop');
            $anchorScroll();
        }

    }

})(window.angular);
﻿(function(angular) {
    'use strict';

    // Controller for modal about.html
    angular
        .module('CalcheeseFrontSite')
        .controller('AboutPageCtrl', AboutPageCtrl);

    AboutPageCtrl.$inject = ['$scope', 'config'];

    function AboutPageCtrl($scope, config) {

        $scope.appVersion = config.VERSION;
        $scope.buildTime = config.BUILD_TIME;
    }

})(window.angular);
﻿(function (angular) {
    'use strict';

    angular
        .module('CalcheeseFrontSite')
        .controller('AnnounceWinGameModalCtrl', AnnounceWinGameModalCtrl);

    AnnounceWinGameModalCtrl.$inject = ['$uibModalInstance', '$timeout', 'MessageModel'];

    function AnnounceWinGameModalCtrl($uibModalInstance, $timeout, MessageModel) {
        var ctrl = this;

        ctrl.$onInit = function () {
            ctrl.model = MessageModel;
            if (!ctrl.model.imageUrl)
                ctrl.model.imageUrl = 'Content/images/pokemon-cards/34-fushigidane-front_339x514.png';

            $timeout(function() {
                    $uibModalInstance.dismiss('cancel');
                },
                3000);
        }

        ctrl.close = function () {
            $uibModalInstance.dismiss('cancel');
        }
    }

    // controller for game draw modal -> announce-draw-game-modal.html
    angular
        .module('CalcheeseFrontSite')
        .controller('AnnounceDrawGameModalCtrl', AnnounceDrawGameModalCtrl);

    AnnounceDrawGameModalCtrl.$inject = ['$uibModalInstance', '$timeout', 'MessageModel'];

    function AnnounceDrawGameModalCtrl($uibModalInstance, $timeout, MessageModel) {
        var ctrl = this;

        ctrl.$onInit = function () {
            ctrl.model = MessageModel;
            $timeout(function() {
                    $uibModalInstance.dismiss('cancel');
                },
                3000);
        }

        ctrl.close = function () {
            $uibModalInstance.dismiss('cancel');
        }
    }

})(window.angular);
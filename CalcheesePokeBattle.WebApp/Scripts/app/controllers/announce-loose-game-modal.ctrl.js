﻿(function (angular) {
    'use strict';

    angular
        .module('CalcheeseFrontSite')
        .controller('AnnounceLooseGameModalCtrl', AnnounceLooseGameModalCtrl);

    AnnounceLooseGameModalCtrl.$inject = ['$uibModalInstance', '$timeout', 'MessageModel'];

    function AnnounceLooseGameModalCtrl($uibModalInstance, $timeout, MessageModel) {
        var ctrl = this;

        ctrl.$onInit = function () {
            ctrl.model = MessageModel;
            if (!ctrl.model.imageUrl)
                ctrl.model.imageUrl = 'Content/images/pokemon-cards/34-fushigidane-front_339x514.png';
            $timeout(function() {
                    $uibModalInstance.dismiss('cancel');
                },
                3000);
        }

        ctrl.close = function () {
            $uibModalInstance.dismiss('cancel');
        }
    }

})(window.angular);
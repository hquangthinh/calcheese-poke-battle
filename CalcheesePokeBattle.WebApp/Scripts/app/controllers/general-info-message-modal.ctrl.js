﻿(function (angular) {
    'use strict';

    angular
        .module('CalcheeseFrontSite')
        .controller('GeneralInfoMessageModalCtrl', GeneralInfoMessageModalCtrl);

    GeneralInfoMessageModalCtrl.$inject = ['$uibModalInstance', 'MessageModel'];

    function GeneralInfoMessageModalCtrl($uibModalInstance, MessageModel) {
        var ctrl = this;

        ctrl.$onInit = function () {
            ctrl.model = MessageModel;
            if (!ctrl.model.imageUrl)
                ctrl.model.imageUrl = 'Content/images/card-ma-khong-hop-le.png';
        }

        ctrl.close = function () {
            $uibModalInstance.dismiss('cancel');
        }
    }

    angular
        .module('CalcheeseFrontSite')
        .controller('UibStandardModalCtrl', UibStandardModalCtrl);

    UibStandardModalCtrl.$inject = ['$uibModalInstance'];

    function UibStandardModalCtrl($uibModalInstance) {

        var ctrl = this;

        ctrl.ok = function () {
            $uibModalInstance.close('ok');
        }

        ctrl.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        }
    }

})(window.angular);
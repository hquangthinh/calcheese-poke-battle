(function (angular) {
    'use strict';
    angular
        .module('CalcheeseFrontSite')
        .factory('BaseDetailCtrlAs', ['R', 'BaseCtrlAs', BaseDetailCtrlAs]);

    angular
        .module('CalcheeseFrontSite')
        .factory('BaseDetailCtrl', ['BaseDetailCtrlAs', BaseDetailCtrl]);

    function BaseDetailCtrlAs(R, BaseCtrlAs) {
        var Base = function (ctrl, $scope, $api) {
            BaseCtrlAs.extend(ctrl, $scope, $api);

            ctrl.model = {loading: true, processing: false, error: false};

            ctrl.init = function () {

                var ID_VALUE = ctrl._params[ctrl._api().idField] ||
                    ctrl._params.ID ||
                    ctrl._params.Id ||
                    ctrl._params.id ||
                        //Default fallback to first param
                    (function (obj) {
                        for (var id in obj) {
                            return id || '';
                        }
                        return '';
                    })(ctrl._params);
                ctrl._api()
                    .id(ID_VALUE, {queryString: ctrl.queryString || ''})
                    .then(function (result) {
                        var data = result.data;
                        // when cannot find data
                        if (data === 'null' || data === null || data === undefined) {
                            ctrl._alert('Item not found', 'Please check your request again');
                            ctrl._go(ctrl._router.HOME);
                            return;
                        }
                        ctrl.model = ctrl._resultMapper(data);
                        ctrl.model.loading = false;
                        ctrl.originalModel = angular.copy(ctrl.model);
                        ctrl.isUnchanged = function (model) {
                            return model._isNew !== true && angular.equals(model, ctrl.originalModel);
                        };
                        ctrl._init();
                    }, function (result) {
                        if (result.status < 500) ctrl._go(ctrl._router.HOME);
                    });
            };

            // Validation: use to disable save button
            ctrl.invalidForm = function () {
                return false;
            };

            // Validate required field to generate ErrorField and error message
            // @fieldName: field to check value
            // @errorMessage: default = "Required value"
            // @return: true/false
            ctrl.validateRequire = function (fieldName, errorMessage, model) {
                // Convention for Error field: FieldName + 'Error'
                var modelToValidate = model || ctrl.model;
                if (modelToValidate[fieldName] == null || modelToValidate[fieldName].toString().trim().length === 0) {
                    modelToValidate[fieldName + 'Error'] = errorMessage || 'Value is required';
                    return false;
                } else {
                    delete modelToValidate[fieldName + 'Error'];
                }
                return true;
            };

            ctrl.updateModel = function (newModel) {
                ctrl.model = ctrl._resultMapper(newModel);
                ctrl.originalModel = angular.copy(ctrl.model);
                ctrl.isUnchanged = function (model) {
                    return model._isNew !== true && angular.equals(model, ctrl.originalModel);
                };
            };

            ctrl.formatModelForSave = R.identity;

            // Save changes
            ctrl.save = function (displayToaster, refreshModel, modelFormatter) {
                displayToaster = typeof displayToaster !== 'undefined' ? displayToaster : true;
                refreshModel = typeof refreshModel !== 'undefined' ? refreshModel : true;
                modelFormatter = modelFormatter || ctrl.formatModelForSave;
                return ctrl.performAsync(function () {
                    return ctrl._api()
                        .post(modelFormatter(ctrl.model))
                        .then(function (result) {
                            // Update clone model
                            if (refreshModel) {
                                if (result) {
                                    ctrl.updateModel(result.data);
                                } else {
                                    ctrl.originalModel = angular.copy(ctrl.model);
                                }
                            }
                            if (displayToaster) {
                                ctrl._msg('Saved Successfully', 'success');
                            } else {
                                ctrl._message = 'Saved Successfully';
                            }
                            $scope.$emit('save', result.data);
                            return result.data;
                        });
                });
            };

            // override this in controllers
            ctrl.canDelete = function () {
                return false;
            };

            ctrl.canRefresh = function() {
                return true;
            };

            // Save and close
            ctrl.ok = function () {
                ctrl.backLink = ctrl.backLink || ctrl._router.DASHBOARD;
                if (ctrl.isUnchanged(ctrl.model) || ctrl.invalidForm()) {
                    ctrl._go(ctrl.backLink, ctrl.backParams);
                } else {
                    ctrl.save(true, false).then(function () {
                        ctrl._go(ctrl.backLink, ctrl.backParams);
                    });
                }
            };
            
            // refresh the form
            ctrl.refresh = function() {
                ctrl.init();
            };
        };
        return {
            extend: function (ctrl, $scope, $api) {
                Base(ctrl, $scope, $api);
            }
        };
    }

    function BaseDetailCtrl(BaseDetailCtrlAs) {
        return {
            extend: function ($scope, $api) {
                BaseDetailCtrlAs.extend($scope, $scope, $api);
            }
        };
    }
})(window.angular);

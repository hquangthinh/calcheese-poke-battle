﻿(function (angular) {
    'use strict';

    angular
        .module('CalcheeseFrontSite')
        .controller('LoginModalCtrl', LoginModalCtrl);

    LoginModalCtrl.$inject = ['$scope', '$uibModal', '$uibModalInstance', 'url', 'BaseCtrlAs', 'apis', '$window', 'UserService', 'FormValidationUtils', 'MessageAlertService'];

    function LoginModalCtrl($scope, $uibModal, $uibModalInstance, url, BaseCtrlAs, apis, $window, UserService, FormValidationUtils, MessageAlertService) {

        var modalCtrl = this;

        BaseCtrlAs.extend(modalCtrl, $scope, 'users');

        modalCtrl.$onInit = function () {
            modalCtrl.processing = false;
            modalCtrl.model = {};
        };

        modalCtrl.doLogin = doLogin;
        modalCtrl.openRequestForgotPassword = openRequestForgotPassword;

        modalCtrl.register = function () {
            var registerUserModalInstance = $uibModal.open({
                templateUrl: url.tmpl('/v2-modals/registration_modal.html'),
                controller: "RegistrationModalCtrl as modalCtrl",
                backdrop: false,
                size: "lg",
                windowTopClass: "modal-sign-up-window"
            });
            registerUserModalInstance.result.then(function(registrationResult) {
                if (registrationResult && registrationResult.success) {

                    //modalCtrl._msg('Đăng ký thành công. Hãy đăng nhập.');
                    MessageAlertService.showModalInfo('Đăng ký thành công. Hãy đăng nhập.');

                } else if (registrationResult && !registrationResult.success) {
                    //modalCtrl._msgerr(registrationResult.message);
                    MessageAlertService.showModalInfo(registrationResult.message);
                }
            });
        }

        function validateInput(modelToValidate) {
            // return true if input is invalid
            if (!FormValidationUtils.validateRequire('UserName', 'Tên đăng nhập là bắt buộc', modelToValidate))
                return true;

            if (!FormValidationUtils.validateRequire('Password', 'Mật khẩu là bắt buộc', modelToValidate))
                return true;

            return false;
        }

        function doLogin() {

            var inputIsInvalid = validateInput(modalCtrl.model);
            if (inputIsInvalid)
                return false;

            loginAndReload(modalCtrl.model);
        }

        function loginAndReload(loginModel) {
            modalCtrl.processing = true;
            apis.login.post(loginModel)
                .then(function (result) {
                    //console.log('login result -> ', result);
                    var data = result.data;
                    if (data === 'ok') {
                        UserService.refreshCurrentUser().then(function () {
                            $scope.$state.go($scope._router.HOME, {}, { reload: true });
                        });
                    } else {
                        //console.log('cannot login -> ', data);
                        modalCtrl.errors = 'Tên đăng nhập hoặc mật khẩu của bạn chưa chính xác, vui lòng kiểm tra lại nhé!';
                    }
                })
                .finally(function () {
                    modalCtrl.processing = false;
                });
        }

        function openRequestForgotPassword() {
            $uibModal.open({
                templateUrl: url.tmpl('/v2-modals/forgot_password_modal.html'),
                controller: "ForgotPasswordModalCtrl as modalCtrl",
                size: "md"
            });
        }

        modalCtrl.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        }
    }

})(window.angular);
﻿(function (document, angular, $, Ps) {
    'use strict';

    angular
        .module('CalcheeseFrontSite')
        .controller('GiftItemPageCtrl', GiftItemPageCtrl);

    GiftItemPageCtrl.$inject = ['$scope', '$log', '$uibModal', 'url', 'BaseCtrlAs', 'WinningListService'];

    function GiftItemPageCtrl($scope, $log, $uibModal, url, BaseCtrlAs, WinningListService) {
        var ctrl = this;
        BaseCtrlAs.extend(ctrl, $scope, 'winninlist');
        ctrl.Weeks = {};
        ctrl.winnerList = {};
        ctrl.luckyPrize = {};
        ctrl.luckyPrizeTraiHe = {};
        ctrl.luckyPrizeVinID = {};
        ctrl.luckyPrizeXeDien = {};
        ctrl.luckyPrizeDoChoi = {};

        ctrl.requestBalo = {};
        ctrl.requestAoThun = {};
        ctrl.requestMu = {};

        ctrl.resultBalo = {};
        ctrl.resultAoThun = {};
        ctrl.resultMu = {};


        ctrl.initData = function () {

            return WinningListService.initCurrentWeek().then(function (data) {

                ctrl.request.params.WeekNumber = data.CuurentWeekDefinition.WeekNumber;
                ctrl.Weeks = data.WeekDefinitionViewModels;

                ctrl.requestBalo.params = {
                    PageNumber: 1,
                    PageSize: 8,
                    SearchMatchType: "",
                    WeekNumber: ctrl.request.params.WeekNumber,
                    GiftName: 'Balo'
                };

                ctrl.requestAoThun.params = {
                    PageNumber: 1,
                    PageSize: 8,
                    SearchMatchType: "",
                    WeekNumber: ctrl.request.params.WeekNumber,
                    GiftName: 'AoThun'
                };
                ctrl.requestMu.params = {
                    PageNumber: 1,
                    PageSize: 8,
                    SearchMatchType: "",
                    WeekNumber: ctrl.request.params.WeekNumber,
                    GiftName: 'Mu'
                };
                ctrl.reloadData();
                return data;
            });
        }

        ctrl.getWinnerList = function (weekNumber) {
            WinningListService.getWinnerList(weekNumber).then(function (data) {
                ctrl.luckyPrize = data;
                if (!data || data.length === 0) {
                    ctrl.luckyPrize = {};
                    ctrl.luckyPrizeTraiHe = {};
                    ctrl.luckyPrizeVinID = {};
                    ctrl.luckyPrizeXeDien = {};
                    ctrl.luckyPrizeDoChoi = {};
                    return;
                }
                angular.forEach(data, function (item) {
                    if (item.PriceType === "TraiHe")
                        ctrl.luckyPrizeTraiHe = item;
                    else if (item.PriceType === "VinID")
                        ctrl.luckyPrizeVinID = item;
                    else if (item.PriceType === "XeDien")
                        ctrl.luckyPrizeXeDien = item;
                    else if (item.PriceType === "DoChoi")
                        ctrl.luckyPrizeDoChoi = item;
                });
            });
        };

        ctrl.getRedeemList = function (request) {
            request.params.WeekNumber = ctrl.request.params.WeekNumber;
            return WinningListService.getRedeemList(request);
        }

        ctrl.reloadData = function () {
            ctrl.getWinnerList(ctrl.request.params.WeekNumber);
            requestRedeem('Balo');
            requestRedeem('AoThun');
            requestRedeem('Mu');
            //ctrl.getRedeemList(ctrl.requestBalo);
            //ctrl.getRedeemList(ctrl.requestAoThun);
            //ctrl.getRedeemList(ctrl.requestMu);
        }

        function requestRedeem(giftName) {

            if (giftName === 'Balo') {
                ctrl.requestBalo.params.PageNumber = ctrl.resultBalo.PageNumber;
                ctrl.getRedeemList(ctrl.requestBalo).then(function (data) {
                    ctrl.resultBalo = data;
                });
            }
            if (giftName === 'AoThun') {
                ctrl.requestAoThun.params.PageNumber = ctrl.resultAoThun.PageNumber;
                ctrl.getRedeemList(ctrl.requestAoThun).then(function (data) {
                    ctrl.resultAoThun = data;
                });
            }

            if (giftName === 'Mu') {
                ctrl.requestMu.params.PageNumber = ctrl.resultMu.PageNumber;
                ctrl.getRedeemList(ctrl.requestMu).then(function (data) {
                    ctrl.resultMu = data;
                });
            }
        }

        ctrl.pageChanged = function (giftName) {
            requestRedeem(giftName);

        }

        ctrl.openShowPrize = function () {

            if (!ctrl.luckyPrizeDoChoi || !ctrl.luckyPrizeDoChoi.WinnerListUsers || ctrl.luckyPrizeDoChoi.WinnerListUsers.length === 0)
                return;

            $uibModal.open({
                templateUrl: url.tmpl('/gift-item/prize_modal.html'),
                controller: "ShowPrizeCtrl as showCtrl",
                size: "lg",
                windowTopClass: "modal-winner-list-user-third-prize-window",
                resolve: {
                    items: ctrl.luckyPrizeDoChoi
                }
            }).closed.then(function () {

            });
        }

    }

    angular
        .module('CalcheeseFrontSite')
        .controller('ShowPrizeCtrl', ShowPrizeCtrl);

    ShowPrizeCtrl.$inject = ['$scope', '$timeout', '$uibModalInstance', 'items'];

    function ShowPrizeCtrl($scope, $timeout, $uibModalInstance, items) {

        var ctrl = this;

        ctrl.$onInit = function() {
            ctrl.model = items;

            $timeout(function() {
                    $(document).ready(function() {
                        var windowWidth = $(window).width();
                        if (windowWidth > 767) {
                            var classScroll = document.getElementsByClassName('modal-body-winner-list-user-third-prize');
                            for (var i = 0; i < classScroll.length; i++) {
                                Ps.initialize(classScroll[i],
                                {
                                    wheelSpeed: 2,
                                    wheelPropagation: true,
                                    maxScrollbarLength: 110
                                });
                            };
                        }
                    });
                },
                1000);
        }

        ctrl.cancel = function() {
            $uibModalInstance.dismiss('cancel');
        };
    }

})(window.document, window.angular, window.$, window.Ps);
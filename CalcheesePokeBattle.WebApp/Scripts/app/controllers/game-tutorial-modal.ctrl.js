﻿(function (document, angular, $) {
    'use strict';

    angular
        .module('CalcheeseFrontSite')
        .controller('GameTutorialModalCtrl', GameTutorialModalCtrl);

    GameTutorialModalCtrl.$inject = ['$scope', '$uibModalInstance', '$timeout'];

    function GameTutorialModalCtrl($scope, $uibModalInstance, $timeout) {
        var ctrl = this;

        ctrl.$onInit = function () {
            $timeout(function() {
                    $(document).ready(function() {
                        var windowWidth = $(window).width();
                        if (windowWidth > 767) {
                            var classScroll = document.getElementsByClassName('modal-body-scroll');
                            for (var i = 0; i < classScroll.length; i++) {
                                Ps.initialize(classScroll[i],
                                {
                                    wheelSpeed: 2,
                                    wheelPropagation: true,
                                    maxScrollbarLength: 60
                                });
                            };
                        }
                        if (windowWidth <= 767) {
                            angular.element('#btnCloseDialog').css('transform', 'rotate(360deg)');
                        }
                    });
                },
                1000);
        }

        ctrl.joinGame = function() {
            $uibModalInstance.close('join_game');
        }

        ctrl.close = function () {
            $uibModalInstance.dismiss('cancel');
        }
    }

})(window.document, window.angular, window.$);
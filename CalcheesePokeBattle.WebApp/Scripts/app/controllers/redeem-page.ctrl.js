﻿(function (document, angular, $, Ps) {
    'use strict';

    angular
        .module('CalcheeseFrontSite')
        .controller('RedeemPageCtrl', RedeemPageCtrl);

    RedeemPageCtrl.$inject = ['$scope', '$uibModal', 'url', 'BaseCtrlAs',
        'UserService', 'SettingService', 'RedeemService', 'MessageAlertService'];

    function RedeemPageCtrl($scope, $uibModal, url, BaseCtrlAs,
        UserService, SettingService, RedeemService, MessageAlertService) {

        var ctrl = this;

        BaseCtrlAs.extend(ctrl, $scope, 'redeem');

        ctrl.initData = function () {
            MessageAlertService.showModalInfo('Chào bạn hiện tại chương trình khuyến mã đã hết nên bạn không thể đổi quà hay mã số may mắn được nữa. Bạn có thể tiếp tục chơi game trên website.');
            RedeemService.getViewModelForRedeemPage().then(function (redeemVmRes) {
                var redeemVmData = redeemVmRes.data;
                ctrl.luckyDrawGifts = redeemVmData.LuckyDrawGifts;
                ctrl.redeemGifts = redeemVmData.RedeemGifts;
                ctrl.BackpackItem = ctrl.redeemGifts[0];
                ctrl.HatItem = ctrl.redeemGifts[1];
                ctrl.TshirtItem = ctrl.redeemGifts[2];
            });
        }

        ctrl.openConfirmRedeemLuckyDrawCode = openConfirmRedeemLuckyDrawCode;
        ctrl.openLuckyDrawCandidateList = openLuckyDrawCandidateList;
        ctrl.openConfirmRedeemGiftItem = openConfirmRedeemGiftItem;

        function openConfirmRedeemLuckyDrawCode() {

            var pointNeedForRedeemLuckyCode = SettingService.getPointsNeedForRedeemLuckyDrawCode();

            if (ctrl.CurrentUser.TotalPoint < pointNeedForRedeemLuckyCode) {

                MessageAlertService.showModalError('Tiếc quá... số điểm của bạn chưa đủ. Bạn hãy tiếp tục sưu tập thêm thẻ Pokémon nhé!');

            } else {

                $uibModal.open({
                        templateUrl: url.tmpl('/redeem/confirm-redeem-lucky-code-modal-v2.html'),
                        controller: "ConfirmRedeemGiftItemModalCtrl as modalCtrl",
                        size: "md",
                        windowTopClass: "modal-confirm-personal-info-for-redeem-window"
                    })
                    .result.then(function(modalReturnData) {
                        var redeemLuckyCodeModal = {
                            FirstName: modalReturnData.FirstName,
                            LastName: modalReturnData.LastName,
                            AddressNumber: modalReturnData.AddressNumber,
                            StreetName: modalReturnData.StreetName,
                            WardName: modalReturnData.WardName,
                            DistrictName: modalReturnData.DistrictName,
                            CityName: modalReturnData.CityName,
                            DeliveryAddress: modalReturnData.DeliveryAddress,
                            PhoneNumber: modalReturnData.PhoneNumber,
                            SocialIdNumber: modalReturnData.SocialIdNumber
                        };
                        RedeemService.redeemLuckyDrawCode(redeemLuckyCodeModal)
                            .then(function(res) {
                                if (res.data.success) {

                                    $uibModal.open({
                                            templateUrl: url.tmpl('/redeem/redeem-lucky-code-ok-result-modal.html'),
                                            controller: "RedeemLuckyCodeOkResultModalCtrl as modalCtrl",
                                            size: "md",
                                            windowTopClass: "modal-confirm-info-doi-ma-window",
                                            resolve: {
                                                MessageParams: { luckyCode: res.data.data }
                                            }
                                        })
                                        .result.then(function() {
                                                UserService.refreshCurrentUser().then(function() {
                                                    ctrl._go(ctrl._router.REDEEM, ctrl._params, { reload: true });
                                                });
                                            },
                                            function() {
                                                UserService.refreshCurrentUser().then(function() {
                                                    ctrl._go(ctrl._router.REDEEM, ctrl._params, { reload: true });
                                                });
                                            });
                                } else {
                                    MessageAlertService.showModalError(res.data.error);
                                }
                            });
                    });
            }
        }

        function openLuckyDrawCandidateList() {
            $uibModal.open({
                templateUrl: url.tmpl('/redeem/lucky-draw-candidate-list-modal.html'),
                controller: "LuckyDrawCandidateListModalCtrl as modalCtrl",
                size: "md",
                windowTopClass: "modal-lucky-draw-candidate-list-window"
            });
        }

        function openConfirmRedeemGiftItem(giftItem) {
            if (!giftItem || ctrl.CurrentUser.TotalPoint < giftItem.RedeemPoint) {

                MessageAlertService.showModalError('Tiếc quá... số điểm của bạn chưa đủ để đổi quà này. Bạn hãy tiếp tục sưu tập thêm thẻ Pokémon nhé!');

            } else {

                $uibModal.open({
                        templateUrl: url.tmpl('/redeem/confirm-redeem-gift-item-modal-v2.html'),
                        controller: "ConfirmRedeemGiftItemModalCtrl as modalCtrl",
                        size: "md",
                        windowTopClass: "modal-confirm-personal-info-for-redeem-window"
                    })
                    .result.then(function(modalReturnData) {

                        var redeemGiftItemModal = {
                            GiftId: giftItem.Id,
                            FirstName: modalReturnData.FirstName,
                            LastName: modalReturnData.LastName,
                            AddressNumber: modalReturnData.AddressNumber,
                            StreetName: modalReturnData.StreetName,
                            WardName: modalReturnData.WardName,
                            DistrictName: modalReturnData.DistrictName,
                            CityName: modalReturnData.CityName,
                            DeliveryAddress: modalReturnData.DeliveryAddress,
                            PhoneNumber: modalReturnData.PhoneNumber,
                            SocialIdNumber: modalReturnData.SocialIdNumber,
                        };

                        RedeemService.redeemGiftItem(redeemGiftItemModal)
                            .then(function(res) {
                                if (res.data.success) {
                                    $uibModal.open({
                                            templateUrl: url.tmpl('/redeem/redeem-gift-ok-result-modal.html'),
                                            controller: "RedeemGiftOkResultModalCtrl as modalCtrl",
                                            size: "md",
                                            windowTopClass: "modal-confirm-info-doi-qua-window",
                                            resolve: {
                                                ModalParams: {
                                                    giftImageName: res.data.data
                                                }
                                            }
                                        })
                                        .result.then(function() {
                                                UserService.refreshCurrentUser().then(function() {
                                                    ctrl._go(ctrl._router.REDEEM_GIFT, ctrl._params, { reload: true });
                                                });
                                            },
                                            function() {
                                                UserService.refreshCurrentUser().then(function() {
                                                    ctrl._go(ctrl._router.REDEEM_GIFT, ctrl._params, { reload: true });
                                                });
                                            });
                                } else {
                                    MessageAlertService.showModalError(res.data.error);
                                }
                            });
                    });
            }
        }
    }

    // controller for redeem-lucky-code-ok-result-modal.html
    angular
        .module('CalcheeseFrontSite')
        .controller('RedeemLuckyCodeOkResultModalCtrl', RedeemLuckyCodeOkResultModalCtrl);

    RedeemLuckyCodeOkResultModalCtrl.$inject = ['$uibModalInstance', 'MessageParams'];

    function RedeemLuckyCodeOkResultModalCtrl($uibModalInstance, MessageParams) {
        var ctrl = this;

        ctrl.$onInit = function () {
            ctrl.luckyCode = (MessageParams && MessageParams.luckyCode) || 'Không có vui lòng thử lại';
        }

        ctrl.close = function () {
            $uibModalInstance.dismiss('cancel');
        }
    }

    // controller for redeem-gift-ok-result-modal.html
    angular
        .module('CalcheeseFrontSite')
        .controller('RedeemGiftOkResultModalCtrl', RedeemGiftOkResultModalCtrl);

    RedeemGiftOkResultModalCtrl.$inject = ['$uibModalInstance', 'ModalParams'];

    function RedeemGiftOkResultModalCtrl($uibModalInstance, ModalParams) {
        var ctrl = this;

        ctrl.$onInit = function () {
            ctrl.giftImageName = (ModalParams && ModalParams.giftImageName) || 'non_93x78.png';
        }

        ctrl.close = function () {
            $uibModalInstance.dismiss('cancel');
        }
    }

    // controller for lucky-draw-candidate-list-modal.html
    angular
        .module('CalcheeseFrontSite')
        .controller('LuckyDrawCandidateListModalCtrl', LuckyDrawCandidateListModalCtrl);

    LuckyDrawCandidateListModalCtrl.$inject = ['$uibModalInstance', '$timeout', 'RedeemService', 'WinningListService', 'R'];

    function LuckyDrawCandidateListModalCtrl($uibModalInstance, $timeout, RedeemService, WinningListService, R) {

        var ctrl = this;

        ctrl.$onInit = function () {

            WinningListService.initCurrentWeek().then(function (weekData) {

                ctrl.Weeks = weekData.WeekDefinitionViewModels;
                ctrl.currentWeek = weekData.CuurentWeekDefinition.WeekNumber;

                RedeemService.getLuckyDrawCandidateListForCurrentWeek(ctrl.currentWeek, 1).then(function(luckyDrawData) {
                    ctrl.luckyDrawCandidateListForCurrentWeek = luckyDrawData;
                    ctrl.currentPage = 1;
                    ctrl.allPages = R.range(1, ctrl.luckyDrawCandidateListForCurrentWeek.PageTotal + 1);
                    ctrl.loading = false;

                    $timeout(function() {
                            $(document).ready(function() {
                                var windowWidth = $(window).width();
                                if (windowWidth > 767) {
                                    var classScroll = document
                                        .getElementsByClassName('lucky-draw-candidate-list-container');
                                    for (var i = 0; i < classScroll.length; i++) {
                                        Ps.initialize(classScroll[i],
                                        {
                                            wheelSpeed: 2,
                                            wheelPropagation: false,
                                            minScrollbarLength: 20,
                                            maxScrollbarLength: 50
                                        });
                                    };
                                }
                                if (windowWidth <= 767) {
                                    angular.element('#btnCloseDialog').css('transform', 'rotate(360deg)');
                                }
                            });
                        },
                        1000);

                });

            });
        }

        ctrl.close = function () {
            $uibModalInstance.dismiss('cancel');
        }

        ctrl.changePage = function (pageNumber) {
            ctrl.loading = true;
            RedeemService.getLuckyDrawCandidateListForCurrentWeek(ctrl.currentWeek, pageNumber).then(function (luckyDrawData) {
                ctrl.luckyDrawCandidateListForCurrentWeek = luckyDrawData;
                ctrl.allPages = R.range(1, ctrl.luckyDrawCandidateListForCurrentWeek.PageTotal + 1);
                ctrl.loading = false;
            });
        }

        ctrl.reloadData = function() {
            ctrl.loading = true;
            RedeemService.getLuckyDrawCandidateListForCurrentWeek(ctrl.currentWeek, 1).then(function (luckyDrawData) {
                ctrl.luckyDrawCandidateListForCurrentWeek = luckyDrawData;
                ctrl.allPages = R.range(1, ctrl.luckyDrawCandidateListForCurrentWeek.PageTotal + 1);
                ctrl.loading = false;
            });
        }
    }

})(window.document, window.angular, window.$, window.Ps);
﻿(function (angular) {
    'use strict';

    angular
        .module('CalcheeseFrontSite')
        .controller('PlayerProfileDetailCtrl', PlayerProfileDetailCtrl);

    PlayerProfileDetailCtrl.$inject = ['$scope', '$uibModal', 'url', 'BaseCtrlAs', 'UserService'];

    function PlayerProfileDetailCtrl($scope, $uibModal, url, BaseCtrlAs, UserService) {
        var ctrl = this;
        BaseCtrlAs.extend(ctrl, $scope, 'users');

        ctrl.openProfileModal = openProfileModal;
        ctrl.openInventoryModal = openInventoryModal;
        ctrl.openChangePasswordModal = openChangePasswordModal;
        ctrl.logout = logout;
        ctrl.changeAvatar = changeAvatar;

        ctrl.$onInit = function() {
            getUserProfileViewModelOnInit();
        }

        function getUserProfileViewModelOnInit() {
            UserService.getUserProfileViewModel(ctrl.CurrentUser.Id).then(function (fullProfileData) {
                ctrl.userProfile = fullProfileData;
            });
        }

        function openProfileModal() {
            $uibModal.open({
                templateUrl: url.tmpl('/player-profile/update-profile-modal.html'),
                controller: "UpdateProfileModalCtrl as modalCtrl",
                size: "md",
                windowTopClass: "modal-confirm-window"
            }).result.then(getUserProfileViewModelOnInit, getUserProfileViewModelOnInit);
        }

        function openInventoryModal() {
            $uibModal.open({
                templateUrl: url.tmpl('/player-profile/player-inventory-modal.html'),
                controller: "PlayerInventoryModalCtrl as modalCtrl",
                size: "md",
                windowTopClass: "modal-kho-chien-cong-window"
            });
        }

        function openChangePasswordModal() {
            $uibModal.open({
                templateUrl: url.tmpl('/player-profile/change-password-modal.html'),
                controller: "ChangePasswordModalCtrl as modalCtrl",
                size: "md"
            });
        }

        function logout() {
            ctrl._go(ctrl._router.LOGOUT);
        }

        function changeAvatar() {
            $uibModal.open({
                    templateUrl: url.tmpl('/player-profile/change-avatar-modal.html'),
                    controller: "ChangeProfileAvatarModalCtrl as modalCtrl",
                    size: "md"
                })
                .result.then(reloadPage, reloadPage);
        }

        function reloadPage() {
            ctrl._go(ctrl._router.PLAYER_PROFILE, {}, { reload: true });
        }

    }

})(window.angular);
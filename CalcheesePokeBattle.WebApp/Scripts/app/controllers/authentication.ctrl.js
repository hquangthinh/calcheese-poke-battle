(function (angular) {
    'use strict';

    angular
        .module('CalcheeseFrontSite')
        .controller('LoginCtrl', LoginCtrl);

    LoginCtrl.$inject = ['$scope', '$window', '$filter', 'config', 'webBaseUrl', 'apis', 'UserService'];

    function LoginCtrl($scope, $window, $filter, $constant, webBaseUrl, apis, UserService) {
        var ctrl = this;

        ctrl.focusOnUserName = true;
        ctrl.processing = true;
        //--- View actions
        ctrl.init = function () {
            // if current user is available, transit to dashboard
            // otherwise login is required
            UserService.getCurrentUser()
                .then(function () {
                    $scope.$state.go($constant.ROUTER.HOME, { reload: true });
                }).finally(function () {
                    ctrl.processing = false;
                });
        };

        // Validate input fields
        ctrl.canLogin = canLogin;
        ctrl.doLogin = doLogin;

        function canLogin() {
            // Form model can not blank
            if (angular.isUndefined(ctrl.model)) return false;
            // Username can not be blank
            return ctrl.model.UserName && ctrl.model.Password;
        }

        // Form process
        function doLogin() {
            if (canLogin()) {
                ctrl.model.processing = true;
                apis.login.post(ctrl.model)
                    .then(function (result) {
                        var data = result.data;
                        console.log(data);
                        if (data == null || data.length === 0) {
                            $scope.$emit('LoginSuccess');
                            $window.location.href = $scope.$stateParams.returnUrl || webBaseUrl;
                        } else {
                            ctrl.model.error = data;
                        }
                    })
                    .catch(function (error) {
                        ctrl.model.error = error;
                    })
                    .finally(function () {
                        ctrl.model.processing = false;
                    });
            } else {
                ctrl.model.error = 'Please Input Your Account And Password';
            }
        }
    }


    // Logout controller
    angular
        .module('CalcheeseFrontSite')
        .controller('LogoutCtrl', LogoutCtrl);

    LogoutCtrl.$inject = ['$rootScope', '$filter', 'config', 'apis', 'UserService'];

    function LogoutCtrl($rootScope, $filter, $constant, $apis, UserService) {
        // Call to dispose session
        $apis.logoff.delete()
            .then(function () {
                UserService.deleteCurrentUser();
                $rootScope.$state.go($constant.ROUTER.DEFAULT, { reload: true });
            })
            .catch(function () {
                // Redirect to error page
                console.error('Cannot Log Out');
            });
    }
})(window.angular);
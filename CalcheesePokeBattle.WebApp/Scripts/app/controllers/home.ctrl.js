﻿(function (document, angular, $) {
    'use strict';

    angular
        .module('CalcheeseFrontSite')
        .controller('HomePageCtrl', HomePageCtrl);

    HomePageCtrl.$inject = ['$scope', '$uibModal', 'url', 'BaseCtrlAs', 'UserService', 'CouponCodeService', 'MessageAlertService'];

    function HomePageCtrl($scope, $uibModal, url, BaseCtrlAs, UserService, CouponCodeService, MessageAlertService) {
        var ctrl = this;
        BaseCtrlAs.extend(ctrl, $scope, 'users');

        function showExchangeCodeErrorModal() {
            $uibModal.open({
                templateUrl: url.tmpl('/v2-modals/exchange_code_result_error_modal.html'),
                controller: "ExchangeCodeResultErrorModalCtrl as modalCtrl",
                size: "md",
                windowTopClass: "modal-khong-hop-le-window"
            });
        }

        ctrl.requestExchangeCouponCode = requestExchangeCouponCode;

        function requestExchangeCouponCode() {
            //angular.element('#Modal-Sign-In').modal('show');
            //return;
            if (!ctrl.CurrentUser) {
                // request login
                $uibModal.open({
                    templateUrl: url.tmpl('/v2-modals/login_modal_mobile.html'),
                    controller: "LoginModalCtrl as modalCtrl",
                    size: "lg",
                    windowTopClass: "modal-sign-in-window"
                });
            } else {
                if (angular.isUndefined(ctrl.userCouponCode) || ctrl.userCouponCode === '') {
                    MessageAlertService.showModalMessage({
                        message: 'Vui lòng nhập mã'
                    });
                    return;
                }
                CouponCodeService.redeemCodeForPoint({
                        Code: ctrl.userCouponCode
                    })
                    .then(function(codeRes) {
                        var codeData = codeRes.data;
                        if (codeData.Success) {
                            $uibModal.open({
                                templateUrl: url.tmpl('/v2-modals/exchange_code_result_ok_modal.html'),
                                controller: "ExchangeCodeResultOkModalCtrl as modalCtrl",
                                size: "md",
                                windowTopClass: "modal-nap-diem-window",
                                resolve: {
                                    MessageModel: {
                                        imageUrl: codeData.CardImageUrl,
                                        point: codeData.Point
                                    }
                                }
                            }).result.then(function() {
                                    UserService.refreshCurrentUser().then(function() {
                                        //ctrl._go(ctrl._router.HOME, ctrl._params, { reload: false });
                                    });
                                },
                                function() {
                                    UserService.refreshCurrentUser().then(function() {
                                        //ctrl._go(ctrl._router.HOME, ctrl._params, { reload: false });
                                    });
                                });
                        } else {
                            showExchangeCodeErrorModal();
                        }
                    })
                    .catch(function() {
                        showExchangeCodeErrorModal();
                    })
                    .finally(function() {
                        delete ctrl.userCouponCode;
                    });
            }
        }
    }

    // modal controller for exchange code result ok
    angular
        .module('CalcheeseFrontSite')
        .controller('ExchangeCodeResultOkModalCtrl', ExchangeCodeResultOkModalCtrl);

    ExchangeCodeResultOkModalCtrl.$inject = ['$uibModalInstance', 'MessageModel'];

    function ExchangeCodeResultOkModalCtrl($uibModalInstance, MessageModel) {

        var ctrl = this;

        ctrl.$onInit = function () {
            ctrl.model = MessageModel;
            if (!ctrl.model.imageUrl)
                ctrl.model.imageUrl = '/Content/images/card-chuc-mung-nap-diem.png';
        }

        ctrl.close = function () {
            $uibModalInstance.dismiss('cancel');
        }
    }

    // modal controller for exchange code result error
    angular
        .module('CalcheeseFrontSite')
        .controller('ExchangeCodeResultErrorModalCtrl', ExchangeCodeResultErrorModalCtrl);

    ExchangeCodeResultErrorModalCtrl.$inject = ['$uibModalInstance'];

    function ExchangeCodeResultErrorModalCtrl($uibModalInstance) {

        var ctrl = this;

        ctrl.close = function () {
            $uibModalInstance.dismiss('cancel');
        }
    }

})(window.document, window.angular, window.$);
﻿(function (document, angular, $) {
    'use strict';

    angular
        .module('CalcheeseFrontSite')
        .controller('PlayerInventoryModalCtrl', PlayerInventoryModalCtrl);

    PlayerInventoryModalCtrl.$inject = ['$scope', '$timeout', '$uibModalInstance', 'url', 'BaseCtrlAs', 'UserService'];

    function PlayerInventoryModalCtrl($scope, $timeout, $uibModalInstance, url, BaseCtrlAs, UserService) {

        var ctrl = this;

        ctrl.$onInit = function () {

            ctrl.luckyCodePopover = {
                templateUrl: 'luckyCodePopoverTemplate.html',
                title: 'pop title'
            };

            $timeout(function() {
                    $(document).ready(function() {
                        var windowWidth = $(window).width();
                        if (windowWidth > 767) {
                            var classScroll = document.getElementsByClassName('modal-body-scroll');
                            for (var i = 0; i < classScroll.length; i++) {
                                Ps.initialize(classScroll[i],
                                {
                                    wheelSpeed: 2,
                                    wheelPropagation: true,
                                    maxScrollbarLength: 110
                                });
                            };
                        } else {
                            angular.element('.modal-kho-chien-cong-window').css('z-index', 1051);
                            angular.element('#player-inventory-modal-body-container').css('z-index', 1051);
                        }
                    });
                },
                1000);

            BaseCtrlAs.extend(ctrl, $scope, 'users');
            UserService.getPlayerInventoryViewModel(ctrl.CurrentUser.Id).then(function (inventoryData) {
                ctrl.profileModel = inventoryData;
            });
        }

        ctrl.close = close;

        function close() {
            $uibModalInstance.dismiss('cancel');
        }

    }

})(window.document, window.angular, window.$);
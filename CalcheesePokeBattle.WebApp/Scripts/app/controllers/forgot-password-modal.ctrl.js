﻿(function (angular) {
    'use strict';

    angular
        .module('CalcheeseFrontSite')
        .controller('ForgotPasswordModalCtrl', ForgotPasswordModalCtrl);

    ForgotPasswordModalCtrl.$inject = ['$scope', '$uibModal', '$uibModalInstance', 'UserService', 'MessageAlertService'];

    function ForgotPasswordModalCtrl($scope, $uibModal, $uibModalInstance, UserService, MessageAlertService) {

        var modalCtrl = this;

        modalCtrl.$onInit = function () {
            modalCtrl.processing = false;
            modalCtrl.model = {};
        };

        modalCtrl.requestPasswordReset = requestPasswordReset;

        function requestPasswordReset() {
            UserService.requestPasswordReset(modalCtrl.model).then(function() {
                MessageAlertService.showModalInfo('Yêu cầu đặt lại mật khẩu đã gửi đến ' + modalCtrl.model.Email)
                    .result.then(modalCtrl.cancel, modalCtrl.cancel);
            });
        }

        modalCtrl.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        }
    }

})(window.angular);
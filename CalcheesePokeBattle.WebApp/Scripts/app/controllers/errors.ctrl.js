﻿(function (angular) {
    'use strict';

    angular
        .module('CalcheeseFrontSite')
        .controller('ErrorsCtrl',
        [
            '$rootScope', '$stateParams', 'config', function ($rootScope, $stateParams, config) {
                var ctrl = this;
                ctrl.hideErrorDetails = false;
                ctrl.init = function () {
                    // Redirect to dashboard if there is not errors
                    if ($stateParams.ErrorResponse) {
                        ctrl.response = $stateParams.ErrorResponse;
                        ctrl.router = config.ROUTER;
                    } else if ($stateParams.errorMsg) {
                        ctrl.response = {
                            data: {
                                ExceptionMessage: 'Generic error found',
                                Message: $stateParams.errorMsg
                            }
                        }
                    } else {
                        $rootScope.$state.go(config.ROUTER.HOME);
                    }
                };

                ctrl.toggleErrorDetails = function () {
                    ctrl.hideErrorDetails = !ctrl.hideErrorDetails;
                }
            }
        ]);

})(window.angular);
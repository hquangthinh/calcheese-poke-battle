﻿(function (angular) {
    'use strict';

    angular
        .module('CalcheeseFrontSite')
        .config([
            '$stateProvider', '$urlMatcherFactoryProvider', '$urlRouterProvider', '$locationProvider',
            'toastrConfig', 'config', routerConfig
        ]);

    function routerConfig($stateProvider,
        $urlMatcherFactoryProvider,
        $urlRouterProvider,
        $locationProvider,
        toastrConfig,
        config) {

        $urlMatcherFactoryProvider.caseInsensitive(true);
        $urlMatcherFactoryProvider.strictMode(false);

        // enable html5 mode
        $locationProvider.html5Mode(true);

        var basicResolve = {
            CurrentUser: [
                'UserService', function (UserService) {
                    return UserService.getCurrentUser();
                }
            ]
        };

        var defaultResolve = angular.extend({}, basicResolve);

        var noResolve = { Nothing: function () { } };

        function urlService() {
            return angular.element(document.body).injector().get('url');
        }

        // helper to make cleaner code
        // map route to controller and template
        function map(state, route, ctrl, tmpl, isDefaultRoute, label, parent, resolve, params) {
            if (isDefaultRoute === true) {
                $urlRouterProvider.otherwise(route);
            }

            $stateProvider.state(state,
                {
                    url: route,
                    controller: ctrl,
                    templateUrl: function () {
                        return urlService().tmpl(tmpl);
                    },
                    resolve: resolve || defaultResolve,
                    params: params
                });
        }

        function mapControllerAs(state, route, ctrl, tmpl, isDefaultRoute, label, parent, resolve, params) {
            map(state, route, ctrl ? ctrl + ' as ctrl' : null, tmpl, isDefaultRoute, label, parent, resolve, params);
        }

        // application routers
        mapControllerAs(config.ROUTER.DEFAULT, '/', 'HomePageCtrl', '/home/index.html', true, 'Trang Chủ', null, noResolve, {});
        mapControllerAs(config.ROUTER.HOME, '/trang-chu', 'HomePageCtrl', '/home/index.html', false, 'Trang Chủ', null, defaultResolve, {});
        map(config.ROUTER.LOGOUT, '/logout', 'LogoutCtrl', '/home/logout.html', false, 'Calcheese', null, {});
        mapControllerAs(config.ROUTER.ERRORS, '/errors?errorMsg', 'ErrorsCtrl', '/home/errors.html', false, 'Calcheese', null, {}, { ErrorResponse: null });
        mapControllerAs(config.ROUTER.ABOUT, '/about', 'AboutPageCtrl', '/about/about.html', true, 'About', null, noResolve, {});

        mapControllerAs(config.ROUTER.REDEEM, '/doi-diem', 'RedeemPageCtrl', '/redeem/redeem.html', true, 'Đổi Lượt Quay Số', null, defaultResolve, {});
        mapControllerAs(config.ROUTER.REDEEM_GIFT, '/doi-qua', 'RedeemPageCtrl', '/redeem/redeem-gift.html', true, 'Đổi Quà', null, defaultResolve, {});
        mapControllerAs(config.ROUTER.LEGAL_TC, '/the-le', 'LeagalTcPageCtrl', '/legal-tc/legal-tc-page.html', true, 'Thể Lệ Chương Trình', null, noResolve, {});
        mapControllerAs(config.ROUTER.GIFT_ITEMS, '/qua-tang', 'GiftItemPageCtrl', '/gift-item/gift-item.html', true, 'Quà Tặng', null, noResolve, {});
        mapControllerAs(config.ROUTER.POKE_CARDS, '/pokemons', 'PokeCardPageCtrl', '/poke-card/poke-card.html', true, 'Thẻ Pokemon', null, noResolve, {});
        mapControllerAs(config.ROUTER.POKE_CARD_GAME, '/san-choi', 'PokeCardGamePageCtrl', '/poke-card-game/poke-card-game.html', true, 'Sân Chơi Pokemon', null, defaultResolve, {});
        mapControllerAs(config.ROUTER.POKE_CARD_GAME_JOIN_GAME, '/tham-gia-san-choi', 'PokeCardGameJoinGamePageCtrl', '/poke-card-game/join-game-page.html', true, 'Sân Chơi Pokemon', null, defaultResolve, {});
        mapControllerAs(config.ROUTER.POKE_CARD_GAME_PLAYING, '/choi-game/:gameSessionId', 'PokeCardGamePlayingPageCtrl', '/poke-card-game/poke-card-game-playing.html', true, 'Sân Chơi Pokemon', null, defaultResolve, {});
        mapControllerAs(config.ROUTER.PLAYER_PROFILE, '/profile', 'PlayerProfileDetailCtrl', '/player-profile/player-profile-detail.html', true, 'Trang Cá Nhân', null, defaultResolve, {});

        // Toast default settings
        angular.extend(toastrConfig,
            {
                positionClass: 'toast-bottom-full-width'
            });
    }

})(window.angular);
﻿(function(angular) {
    'use strict';

    var app = angular
        .module('CalcheeseFrontSite');

    // Price format with color base on value
    app.directive('numPrice',
    [
        '$filter', function($filter) {
            return function(scope, element, attrs) {
                var data = attrs['numPrice'];
                var data_unit = attrs['numPriceUom'];
                var currSymbol = attrs["numPriceCurrency"];
                if (currSymbol) currSymbol += " ";
                // If number
                if (data && !isNaN(parseFloat(data)) && isFinite(data)) {
                    formatLabel(data, element);
                    var text = $filter('currency')(data, currSymbol, 2);
                    if (data_unit) text += ' ' + data_unit;
                    element.text(text); // fractionSize only available from 1.3.0
                } else {
                    element.hide();
                }
            };
        }
    ]);

    // Status format
    app.directive('labelColor',
        function() {
            return {
                restrict: 'A',
                scope: {
                    labelColor: '<'
                },
                link: function(scope, element, attrs) {
                    var val = scope.labelColor;
                    switch (val) {
                    case -3:
                    case "-3":
                    case "C":
                        break;
                    case 0:
                    case "0":
                    case "Saved":
                    case 'Not Yet Submitted':
                        element.addClass('label-default');
                        break;
                    case 1:
                    case 2:
                    case "1":
                    case "2":
                    case "Submitted":
                    case "Approval In Progress":
                        element.addClass('label-belizehole');
                        break;
                    case 3:
                    case "3":
                    case "Approved":
                        element.addClass('label-nephritis');
                        break;
                    case 4:
                    case 5:
                    case "4":
                    case "5":
                    case "H":
                    case "PO Created":
                    case "Purchase Order Created":
                    case "Ordered":
                    case "Received":
                    case 'Posted':
                        element.addClass('label-greensea');
                        break;
                    default:
                        break;
                    }
                }
            };
        });

    // Loading element in panel
    app.directive('comLoading',
    [
        '$timeout', function($timeout) {
            return {
                restrict: 'E',
                template:
                    '<div class="com-loading"><span class="com-loading-inner"><i class="fa fa-spinner fa-spin"></i> Loading</span></div>',
                link: function(scope, element, attrs) {
                    var timer = $timeout(function() {
                            element.children().css('display', 'block');
                        },
                        100); // delay for NOT display in reload

                    scope.$on('$destroy',
                        function() {
                            $timeout.cancel(timer);
                        });
                }
            }
        }
    ]);

    // iCheck directive
    // https://github.com/fronteed/iCheck/issues/62 by wajatimur
    app.directive('icheck',
    [
        '$timeout', function($timeout) {
            return {
                require: 'ngModel',
                link: function($scope, element, $attrs, ngModel) {
                    var elem;
                    var timer = $timeout(function() {
                        var value;
                        value = $attrs['value'];

                        $scope.$watch($attrs['ngModel'],
                            function(newValue) {
                                $(element).iCheck('update');
                            });

                        elem = $(element).iCheck({
                            checkboxClass: 'icheckbox_square-blue',
                            radioClass: 'icheckbox_square-blue'
                        });

                        elem.on('ifChanged',
                            function(event) {
                                if ($(element).attr('type') === 'checkbox' && $attrs['ngModel']) {
                                    $scope.$apply(function() {
                                        return ngModel.$setViewValue(event.target.checked);
                                    });
                                }
                                if ($(element).attr('type') === 'radio' && $attrs['ngModel']) {
                                    return $scope.$apply(function() {
                                        return ngModel.$setViewValue(value);
                                    });
                                }
                            });
                    });

                    $scope.$on('$destroy',
                        function() {
                            if (elem) elem.off();
                            $timeout.cancel(timer);
                        });
                }
            };
        }
    ]);

    // Focus field, used in modal dialog
    // http://stackoverflow.com/questions/14833326/how-to-set-focus-on-input-field by Mark Rajcok
    app.directive('focusMe',
    [
        '$timeout', '$parse', function($timeout, $parse) {
            return {
                //scope: true,   // optionally create a child scope
                link: function(scope, element, attrs) {
                    var model = $parse(attrs.focusMe);
                    scope.$watch(model,
                        function(value) {
                            if (value === true) {
                                $timeout(function() {
                                        element[0].focus();
                                    },
                                    100,
                                    false);
                            }
                        });
                    // to address @blesh's comment, set attribute value to 'false'
                    // on blur event:
                    element.bind('blur',
                        function() {
                            scope.$applyAsync(model.assign(scope, false));
                        });
                    scope.$on('$destroy',
                        function() {
                            element.off('blur');
                        });
                }
            };
        }
    ]);

    /**
     * Toggle class
     */
    app.directive('toggleClass', toggleClass);

    function toggleClass() {
        return {
            restrict: 'A',
            link: function(scope, element, attrs) {
                element.bind('click',
                    function() {
                        var targetObj = document.getElementById(attrs.toggleTarget);
                        var targetElement = angular.element(targetObj);
                        targetElement.toggleClass(attrs.toggleClass);
                    });
                scope.$on('$destroy',
                    function() {
                        element.off();
                    })
            }
        };
    }

    //limit input charaters of text field
    app.directive("limitTo",
    [
        function() {
            return {
                restrict: "A",
                require: "ngModel",
                link: function(scope, element, attrs, ngModelCtrl) {
                    var limit = parseInt(attrs.limitTo);
                    attrs.$set('maxlength', limit);
                    //paste value
                    var timer;
                    element.on("paste",
                        function() {
                            timer = setTimeout(function() {
                                    if (element.val().length > limit) {
                                        ngModelCtrl.$setViewValue(element.val().substring(0, limit));
                                        ngModelCtrl.$render();
                                    }
                                },
                                5);
                        });
                    scope.$on('$destroy',
                        function() {
                            clearTimeout(timer);
                            element.off();
                        })
                }
            }
        }
    ]);

    //truncate text and display tooltip 
    app.directive("truncateAt",
    [
        "$compile", function($compile) {
            return {
                restrict: "A",
                scope: {
                    text: "=truncateText"
                },
                link: function(scope, element, attrs) {
                    var limit = parseInt(attrs.truncateAt);
                    var limitText = scope.text;
                    if (scope.text.length > limit) {
                        limitText = limitText.substring(0, limit) + "...";
                        element.attr('uib-tooltip', scope.text);
                    }
                    element.removeAttr('truncate-at'); //remove the attribute to avoid indefinite loop
                    element.removeAttr('truncate-text');
                    element.removeAttr('ui-sref'); //for ui router link
                    element.text(limitText);
                    $compile(element)(scope);
                }
            }
        }
    ]);

    // resize image base on fixed width -> height and maintain w/h ratio
    app.directive("imageResize",
    [
        "$parse", function($parse) {
            return {
                link: function(scope, elm, attrs) {
                    return elm.one("load",
                        function() {
                            return resizeImage(scope, elm, attrs);
                        });
                }
            };

            function resizeImage(scope, elm, attrs) {
                var neededWidth = $parse(attrs.neededWidth)(scope);
                var neededHeight = neededWidth * elm[0].height / elm[0].width;
                var canvas = document.createElement("canvas");
                canvas.width = neededWidth;
                canvas.height = neededHeight;
                var ctx = canvas.getContext("2d");
                ctx.drawImage(elm[0], 0, 0, neededWidth, neededHeight);
                return elm.attr("src", canvas.toDataURL("image/jpeg"));
            }
        }
    ]);

})(window.angular);
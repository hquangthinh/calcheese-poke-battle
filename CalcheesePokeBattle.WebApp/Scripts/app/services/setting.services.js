(function (angular) {
    'use strict';

    angular
        .module('CalcheeseFrontSite')
        .service("SettingService", SettingService);

    SettingService.$inject = ["apis", "UserService", "R"];

    function SettingService(apis, UserService, R) {
        var service = this;
        var settingsApi = apis.settings;

        var Settings = {
            PointsNeedForRedeemLuckyDrawCode: 'PointsNeedForRedeemLuckyDrawCode'
        };

        /** @memberOf SettingService **/
        service.dataImportSupportedFileTypes = ["application/vnd.ms-excel", "text/csv"];

        /** @memberOf SettingService **/
        service.getSettingValue = getSettingValue;
        /** @memberOf SettingService **/
        service.getServerSettingValue = getServerSettingValue;
        /** @memberOf SettingService **/
        service.getPointsNeedForRedeemLuckyDrawCode = getPointsNeedForRedeemLuckyDrawCode;
        /** @memberOf SettingService **/
        service.SETTINGS = Settings;
        /** @memberOf SettingService **/
        service.getPreferredLocale = getPreferredLocale;

        var _settings = {};


        function getSettingValue(settingId, defaultValue) {
            // user must be logged in to get setting value
            if (!UserService.CurrentUser) return defaultValue;
            return _settings[settingId] || defaultValue;
        }

        function getServerSettingValue(settingId, defaultValue) {
            return settingsApi.getFromUri(settingId, "/value").then(function (res) {
                return res.data || defaultValue;
            });
        }

        var serverLocale = R.replace('-', '_', angular.element('html').data('locale'));

        function getPreferredLocale() {
            return serverLocale;
        }

        function getPointsNeedForRedeemLuckyDrawCode() {
            return 10;
        }
    }
})(window.angular);
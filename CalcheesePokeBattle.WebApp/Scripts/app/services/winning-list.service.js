﻿(function (angular) {
    'use strict';

    angular
        .module('CalcheeseFrontSite')
        .service('WinningListService', WinningListService);

    WinningListService.$inject = ['apis'];

    function WinningListService(apis) {
        var serviceApi = apis.winninlist;
        var service = this;
        service.initCurrentWeek = initCurrentWeek;
        service.getWinnerList = getWinnerList;
        service.getRedeemList = getRedeemList;

        function initCurrentWeek() {
            return serviceApi.path("InitData").then(function (result) {
                return result.data;
            });
        }

        function getWinnerList(weekNumber) {
            return serviceApi.path("GetWinnerList", {
                params: {
                    WeekNumber: weekNumber
                }
            }).then(function (result) {
                return result.data;
            });
        }

        function getRedeemList(request) {
            return serviceApi.path("GetRedeemList", {
                params: request.params
            }).then(function (result) {
                return result.data;
            });
        }
    }
})(window.angular);

﻿(function (angular) {
    'use strict';

    angular
        .module('CalcheeseFrontSite')
        .provider('siteBaseUrl',
            function siteBaseUrlProvider() {

                var baseUrl = angular.element('html').data('sitebaseurl');

                return {
                    setBaseUrl: function(url) {
                        baseUrl = url;
                    },
                    $get: function() {
                        return baseUrl;
                    }
                };
            });

    angular
        .module('CalcheeseFrontSite')
        .provider('webBaseUrl',
            function webBaseUrlProvider() {

                var baseUrl = angular.element('html').data('webbaseurl');
                if (window.location.host === 'www.calcheese.vn') {
                    baseUrl = 'http://www.calcheese.vn';
                }

                return {
                    setBaseUrl: function(url) {
                        baseUrl = url;
                    },
                    $get: function() {
                        return baseUrl;
                    }
                };
            });

    angular
        .module('CalcheeseFrontSite')
        .provider('apiBaseUrl',
            function apiBaseUrlProvider() {

                var baseUrl = angular.element('html').data('apibaseurl');

                return {
                    setBaseUrl: function(url) {
                        baseUrl = url;
                    },
                    getBaseUrl: function() {
                        return baseUrl;
                    },
                    $get: function() {
                        return baseUrl;
                    }
                };
            });

    angular
        .module('CalcheeseFrontSite').service('url',
        [
            '$location', 'webBaseUrl', function ($location, webBaseUrl) {
                var _this = this;

                //reload browser cache every new deploy
                var instanceID = angular.element('html').data('instanceid') || (new Date().getMilliseconds());
                var mode = angular.element('html').data('mode') || 'production';

                _this.partial = partial;
                _this.directives = directives;
                _this.components = components;
                _this.files = files;
                _this.tmpl = function(path) {
                    return /^partial:/.test(path)
                        ? partial(path.substr('partial:'.length))
                        : tmpl(path);
                };

                function getCacheId() {
                    return mode === 'dev'
                        ? new Date().getMilliseconds()
                        : instanceID;
                }

                function partial(path) {
                    return webBaseUrl + '/Scripts/app/partials' + path + "?" + getCacheId();
                }

                function directives(path) {
                    return webBaseUrl + '/Scripts/app/directives' + path + "?" + getCacheId();
                }

                function tmpl(path) {
                    return webBaseUrl + '/Scripts/app/views' + path + "?" + getCacheId();
                }

                function components(path) {
                    return webBaseUrl + '/Scripts/app/components' + path + "?" + getCacheId();
                }

                //template files
                function files(path) {
                    return webBaseUrl + '/Content/files/' + path;
                }
            }
        ]);

})(window.angular);
﻿(function (angular) {
    'use strict';

    angular
        .module('CalcheeseFrontSite')
        .service('PokemonCardService', PokemonCardService);

    PokemonCardService.$inject = ['apis'];

    function PokemonCardService(apis) {
        var service = this;
        var serviceApi = apis.pokemon_card;

        /** @memberOf PokemonCardService **/
        service.getAllPokemonCards = getAllPokemonCards;

        function getAllPokemonCards() {
            return serviceApi.all();
        }
    }
})(window.angular);

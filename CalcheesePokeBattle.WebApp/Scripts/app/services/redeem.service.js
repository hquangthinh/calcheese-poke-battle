﻿(function (angular) {
    'use strict';

    angular
        .module('CalcheeseFrontSite')
        .service('RedeemService', RedeemService);

    RedeemService.$inject = ['apis'];

    function RedeemService(apis) {
        var service = this;
        var serviceApi = apis.redeem;

        /** @memberOf RedeemService **/
        service.getViewModelForRedeemPage = getViewModelForRedeemPage;

        /** @memberOf RedeemService **/
        service.redeemLuckyDrawCode = redeemLuckyDrawCode;

        /** @memberOf RedeemService **/
        service.redeemGiftItem = redeemGiftItem;

        /** @memberOf RedeemService **/
        service.getLuckyDrawCandidateListForCurrentWeek = getLuckyDrawCandidateListForCurrentWeek;

        function getViewModelForRedeemPage() {
            return serviceApi.path('view-model');
        }

        function redeemLuckyDrawCode(redeemLuckyCodeModal) {
            return serviceApi.postToUri('points-for-lucky-code', redeemLuckyCodeModal);
        }

        function redeemGiftItem(redeemGiftItemModel) {
            //points-for-gift
            return serviceApi.postToUri('points-for-gift', redeemGiftItemModel);
        }

        function getLuckyDrawCandidateListForCurrentWeek(weekNumber, pageNumber) {
            return serviceApi.postToUri('lucky-draw-candidate-list', { Keyword: weekNumber, PageNumber: pageNumber || 1 }).then(function(res) {
                return res.data;
            });
        }
    }
})(window.angular);

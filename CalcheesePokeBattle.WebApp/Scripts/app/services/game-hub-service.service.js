﻿(function (angular) {
    'use strict';

    angular
        .module('CalcheeseFrontSite')
        .service('GameHubService', GameHubService);

    GameHubService.$inject = ['apis'];

    function GameHubService(apis) {
        var service = this;
        var gameHubApi = apis.game_hub;

        /** @memberOf GameHubService **/
        service.getAllOnlinePlayers = getAllOnlinePlayers;

        /** @memberOf GameHubService **/
        service.checkIfUserCanJoinGame = checkIfUserCanJoinGame;

        /** @memberOf GameHubService **/
        service.getGamePlayInfoFromGameSession = getGamePlayInfoFromGameSession;

        /** @memberOf GameHubService **/
        service.stopGameRequest = stopGameRequest;

        /** @memberOf GameHubService **/
        service.disconnectGameForUser = disconnectGameForUser;

        function getAllOnlinePlayers(searchParams) {
            //console.log('getAllOnlinePlayers -> ', searchParams);
            return gameHubApi.path('online-palyers', searchParams);
        }

        function checkIfUserCanJoinGame() {
            return gameHubApi.postToUri('/can-play-game');
        }

        function getGamePlayInfoFromGameSession(gameSessionId) {
            return gameHubApi.path('game-play-info', { params: { GameSessionId: gameSessionId } });
        }

        function stopGameRequest(gameSessionId) {
            return gameHubApi.path('stop-game', { params: { GameSessionId: gameSessionId } });
        }

        // called when player unload browser
        function disconnectGameForUser(userId) {
            return gameHubApi.postToUri('/disconnect-game', { RequestUserId: userId });
        }
    }
})(window.angular);

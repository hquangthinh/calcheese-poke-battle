﻿(function (angular) {
    'use strict';

    angular
        .module('CalcheeseFrontSite')
        .service('CouponCodeService', CouponCodeService);

    CouponCodeService.$inject = ['apis'];

    function CouponCodeService(apis) {
        var service = this;
        var serviceApi = apis.coupon_code;

        /** @memberOf CouponCodeService **/
        service.redeemCodeForPoint = redeemCodeForPoint;

        function redeemCodeForPoint(couponCodeModel) {
            return serviceApi.postToUri('redeem-code', couponCodeModel);
        }
    }
})(window.angular);

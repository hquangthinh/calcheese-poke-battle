﻿(function(angular) {
    'use strict';

    angular
        .module("CalcheeseFrontSite")
        .service("UploadService", UploadService);

    UploadService.$inject = ["$timeout", "toastr"];

    function UploadService($timeout, toastr) {

        var service = this;

        /** @memberOf UploadService **/
        service.options = {
            fileReaderSupported: false,
            uploadAction: "upload",
            supportFileTypes: ['image/png', 'image/jpg', 'image/jpeg', 'image/bmp', 'image/gif']
        };

        /** @memberOf UploadService **/
        service.api = null;

        /** @memberOf UploadService **/
        service.uploadedCounter = 0;

        /** @memberOf UploadService **/
        service.onFileSelect = onFileSelect;

        /** @memberOf UploadService **/
        service.start = start;

        /** @memberOf UploadService **/
        service.uploadOptionData = uploadOptionData;

        /** @memberOf UploadService **/
        service.uploadCompleted = function() {};

        function uploadOptionData() {
            return {}
        }

        function reset(model) {
            service.uploadedCounter = 0;
            model.selectedFiles = [];
            model.progress = []; // Import properties to support progress display
            model.progressData = [];
            if (model.upload && model.upload.length > 0) {
                for (var i = 0; i < model.upload.length; i++) {
                    if (model.upload[i] != null) {
                        model.upload[i].abort();
                    }
                }
            }
            model.upload = [];
            model.uploadResult = [];
            model.uploadInvalid = true;
            model.dataUrls = []; // Use to read image data
        }

        function onFileSelect(files, model) {
            reset(model);
            model.selectedFiles = files;
            if (files && files.length) {
                model.uploadInvalid = false;
                for (var i = 0; i < files.length; i++) {
                    readData(i, files[i], model);
                    model.progress[i] = -1;
                }
            }
        }

        function start(model) {
            for (var i = 0; i < model.selectedFiles.length; i++) {
                startUpload(i, model);
            }
        }

        function startUpload(index, model) {
            if (service.api == null) {
                toastr.error("System Error! Cannot upload file", 'Error', { closeButton: true, timeOut: 0 });
            } else {
                model.upload[index] = service.api().upload(4,
                    model.selectedFiles[index],
                    service.options.uploadAction,
                    service.uploadOptionData());
                model.upload[index].then(function(response) {
                        // file is uploaded successfully
                        model.progressData[index] = response.data;
                        // check upload progress
                        service.uploadedCounter++;
                        if (service.uploadedCounter === model.selectedFiles.length)
                            service.uploadCompleted();
                    },
                    function(response) {
                        // file failed to upload
                        console.log(response);
                    },
                    function(evt) {
                        // Math.min is to fix IE which reports 200% sometimes
                        model.progress[index] = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
                    });
            }
        }

        // Read local file data
        function readData(i, file, model) {
            service.options.fileReaderSupported = window.FileReader != null &&
                (window.FileAPI == null || FileAPI.html5 != false);
            if (service.options.fileReaderSupported && file.type.indexOf('image') > -1) {
                var fileReader = new FileReader();
                fileReader.readAsDataURL(file);
                var loadFile = function(fileReader, index) {
                    fileReader.onload = function(e) {
                        $timeout(function() {
                            model.dataUrls[index] = e.target.result;
                        });
                    };
                };
                loadFile(fileReader, i);
            }
        }
    }
})(window.angular);

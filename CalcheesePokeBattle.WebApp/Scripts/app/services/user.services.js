(function (angular) {
    'use strict';

    angular
        .module('CalcheeseFrontSite')
        .service("UserService", UserService);

    UserService.$inject = ["apis", "$rootScope", "Memoization"];

    function UserService(apis, $rootScope, Memoization) {
        var service = this;
        var userApi = apis.users;
        var playerApi = apis.player;

        /** @memberOf UserService **/
        service.CurrentUser = null;

        /** @memberOf UserService **/
        service.registerUser = registerUser;

        /** @memberOf UserService **/
        service.validateCaptcha = validateCaptcha;

        /** @memberOf UserService **/
        service.checkUserName = checkUserName;

        /** @memberOf UserService **/
        service.checkPhoneNumber = checkPhoneNumber;

        /** @memberOf UserService **/
        service.getCurrentUser = getCurrentUser;

        /** @memberOf UserService **/
        service.refreshCurrentUser = refreshCurrentUser;

        /** @memberOf UserService **/
        service.changePassword = changePassword;

        /** @memberOf UserService **/
        service.updateProfileMainDetails = updateProfileMainDetails;

        /** @memberOf UserService **/
        service.getUserDetail = getUserDetail;

        /** @memberOf UserService **/
        service.getUserProfileViewModel = getUserProfileViewModel;

        /** @memberOf UserService **/
        service.getPlayerInventoryViewModel = getPlayerInventoryViewModel;

        /** @memberOf UserService **/
        service.updateCurrentUser = updateCurrentUser;

        /** @memberOf UserService **/
        service.deleteCurrentUser = deleteCurrentUser;

        /** @memberOf UserService **/
        service.searchUsers = searchUsers;

        /** @memberOf UserService **/
        service.requestPasswordReset = requestPasswordReset;

        /** @memberOf UserService **/
        service.uploadProfileAvatar = uploadProfileAvatar;

        var CURRENT_USER_CACHE_DURATION = 60000; // 1 minute
        var currentUserMemoized = Memoization.memoize(getCurrentUserInternal, CURRENT_USER_CACHE_DURATION);
        function getCurrentUserInternal() {
            return userApi.id('current').then(function (result) {
                return updateCurrentUser(result.data);
            });
        }

        function getCurrentUser() {
            return currentUserMemoized()();
        }

        function refreshCurrentUser() {
            return getCurrentUserInternal();
        }

        function dataURItoBlob(dataURI) {
            // convert base64/URLEncoded data component to raw binary data held in a string
            var byteString;
            if (dataURI.split(',')[0].indexOf('base64') >= 0)
                byteString = atob(dataURI.split(',')[1]);
            else
                byteString = unescape(dataURI.split(',')[1]);

            // separate out the mime component
            var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

            // write the bytes of the string to a typed array
            var ia = new Uint8Array(byteString.length);
            for (var i = 0; i < byteString.length; i++) {
                ia[i] = byteString.charCodeAt(i);
            }

            return new Blob([ia], { type: mimeString });
        }

        function uploadProfileAvatar(base64ImageDataUrl) {
            //var blob = dataURItoBlob(base64ImageData);
            //var fd = new FormData();
            //fd.append('Base64ImageData', base64ImageDataUrl);
            return userApi.postToUri('/avatar', { Base64ImageData: base64ImageDataUrl});
        }

        function requestPasswordReset(passwordResetModel) {
            return userApi.postToUri('/forgot-password', passwordResetModel);
        }

        function changePassword(userDetailModel) {
            return userApi.postToUri('/password', userDetailModel);
        }

        function updateProfileMainDetails(model) {
            return userApi.postToUri('/profile', model);
        }

        function getUserDetail(userId) {
            return userApi.id(userId).then(function(result) {
                return result.data;
            });
        }

        function getUserProfileViewModel(userId) {
            return playerApi.id(userId).then(function (result) {
                return result.data;
            });
        }

        function getPlayerInventoryViewModel(userId) {
            return playerApi.getFromUri(userId, 'inventory').then(function (result) {
                return result.data;
            });
        }

        function updateCurrentUser(user) {
            $rootScope.CurrentUser = user;
            service.CurrentUser = user;
            return user;
        }

        function deleteCurrentUser() {
            delete $rootScope.CurrentUser;
            delete service.CurrentUser;
        }
        

        function searchUsers(active, query, findAll, permission, requisitionApprover) {
            var permissionId = permission ? permission : null;
            return userApi.all({params: {
                Keyword: query,
                Permission: permissionId,
                IsApprover: requisitionApprover,
                SearchMatchType: 'contains',
                Active: active,
                FindAll: findAll === true
            }}).then(function (result) {
                return result.data.ResultSet;
            });
        }

        function checkUserName(userNameToCheck) {
            return apis.users.postToUri('/validate-playername', { UserName: userNameToCheck});
        }

        function validateCaptcha(validateCaptchaModel) {
            return apis.users.postToUri('/validate-captcha', validateCaptchaModel);
        }

        function checkPhoneNumber(phoneNumberToCheck) {
            return apis.users.postToUri('/validate-phonenumber', { PhoneNumber: phoneNumberToCheck});
        }

        function registerUser(userRegistrationModel) {
            return apis.users.postToUri('/register', userRegistrationModel);
        }
    }
})(window.angular);

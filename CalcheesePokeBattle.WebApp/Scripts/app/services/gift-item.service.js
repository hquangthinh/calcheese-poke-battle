﻿(function (angular) {
    'use strict';

    angular
        .module('CalcheeseFrontSite')
        .service('GiftItemService', GiftItemService);

    GiftItemService.$inject = ['apis'];

    function GiftItemService(apis) {
        var service = this;
        var serviceApi = apis.gift_item;

        /** @memberOf GiftItemService **/
        service.getAllGiftItems = getAllGiftItems;

        function getAllGiftItems() {
            return serviceApi.all();
        }
    }
})(window.angular);

﻿(function(angular) {
    'use strict';
    angular
        .module('CalcheeseFrontSite')
        .factory('MessageAlertService', MessageAlertService);

    MessageAlertService.$inject = ['swal', 'toastr', '$uibModal', 'url'];

    function MessageAlertService(swal, toastr, $uibModal, url) {
        var service = this;

        var messageExpiryTime = 3000;
        var errorMessageExpiryTime = 5000;

        service.showConfirmationMessage = showConfirmationMessage;
        service.showInfoMessage = showInfoMessage;
        service.showErrorMessage = showErrorMessage;
        service.showToastrMessage = showToastrMessage;
        service.showToastrError = showToastrError;

        /**
         * Show game confirmation message using uibModal
         */
        service.showGameConfirmationMessage = showGameConfirmationMessage;

        service.showGameModalInfoWithRetry = showGameModalInfoWithRetry;

        service.showGameNotEnoughPointModal = showGameNotEnoughPointModal;

        service.showPlayerLeaveGameModal = showPlayerLeaveGameModal;

        /**
         * Show message using uibModal. Modal has image and message
         */
        service.showModalMessage = showModalMessage;

        /**
         * Show message using uibModal. Modal has message only
         */
        service.showModalError = showModalError;
        service.showModalInfo = showModalError; // alias as showModalInfo

        service.showWinGameModal = showWinGameModal;
        service.showLostGameModal = showLostGameModal;
        service.showGameTieModal = showGameTieModal;

        function showInfoMessage(message, messageExpiredAfterMs) {
            return swal({
                title: 'Information',
                type: 'info', // "warning", "error", "success" and "info"
                html: message,
                timer: messageExpiredAfterMs || messageExpiryTime
            });
        }

        function showErrorMessage(message) {
            //console.log(message);
            return swal({
                title: 'Error',
                type: 'error', // "warning", "error", "success" and "info"
                html: message,
                timer: errorMessageExpiryTime
            });
        }

        function showConfirmationMessage(title, message, onOkAction, onCancelAction) {
            swal({
                title: title || 'Information',
                type: 'info', // "warning", "error", "success" and "info"
                html: message,
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                cancelButtonText: 'Bỏ Qua',
                confirmButtonText: 'Đồng Ý'
            }).then(onOkAction, onCancelAction);
        }
        
        // show message toastr type: Error, Warning, Success
        function showToastrMessage(message, type) {
            if ('error' === type) {
                console.error(message);
                toastr.error(message, 'Error', { closeButton: true, timeOut: 0 });
            } else if (type === 'warning') {
                toastr.warning(message);
            } else {
                toastr.success(message);
            }
        }

        function showToastrError(message) {
            service.showToastrMessage(message, 'error');
        }

        function showGameConfirmationMessage(messageModel) {
            return $uibModal.open({
                templateUrl: url.tmpl('/poke-card-game/game-confirmation-modal.html'),
                controller: "GameConfirmationModalCtrl as modalCtrl",
                size: "md",
                windowTopClass: "modal-thach-dau-window",
                resolve: {
                    MessageModel: messageModel
                }
            });
        }

        function showModalMessage(messageModel) {
            return $uibModal.open({
                templateUrl: url.tmpl('/modals/general-info-message-modal.html'),
                controller: "GeneralInfoMessageModalCtrl as modalCtrl",
                size: "md",
                windowTopClass: "modal-khong-hop-le-window",
                resolve: {
                    MessageModel: messageModel
                }
            });
        }

        function showModalError(errMessage) {
            return $uibModal.open({
                templateUrl: url.tmpl('/modals/general-error-modal.html'),
                controller: "GeneralErrorModalCtrl as modalCtrl",
                size: "md",
                windowTopClass: "modal-khong-hop-le-window",
                resolve: {
                    MessageParams: { errorMessage: errMessage }
                }
            });
        }

        function showGameModalInfoWithRetry(message) {
            return $uibModal.open({
                templateUrl: url.tmpl('/poke-card-game/game-info-with-retry-modal.html'),
                controller: "GameInfoWithRetryModalCtrl as modalCtrl",
                size: "md",
                windowTopClass: "modal-not-enough-card-window",
                resolve: {
                    MessageModel: {
                        message: message
                    }
                }
            });
        }

        function showGameNotEnoughPointModal(message) {
            return $uibModal.open({
                templateUrl: url.tmpl('/poke-card-game/game-not-enough-point-modal.html'),
                controller: "GameNotEnoughPointModalCtrl as modalCtrl",
                size: "md",
                windowTopClass: "modal-thach-dau",
                resolve: {
                    MessageModel: {
                        message: message
                    }
                }
            });
        }

        function showPlayerLeaveGameModal(playerFullName) {
            return $uibModal.open({
                templateUrl: url.tmpl('/poke-card-game/modal-leave-game.html'),
                controller: "PlayerLeaveGameModalCtrl as modalCtrl",
                size: "md",
                windowTopClass: "modal-thach-dau-window",
                resolve: {
                    MessageModel: {
                        PlayerFullName: playerFullName
                    }
                }
            });
        }

        function showWinGameModal(messageModel) {
            return $uibModal.open({
                templateUrl: url.tmpl('/poke-card-game/announce-win-game-modal.html'),
                controller: "AnnounceWinGameModalCtrl as modalCtrl",
                size: "md",
                windowTopClass: "modal-game-add-point-window",
                resolve: {
                    MessageModel: messageModel
                }
            });
        }

        function showLostGameModal(messageModel) {
            return $uibModal.open({
                templateUrl: url.tmpl('/poke-card-game/announce-loose-game-modal.html'),
                controller: "AnnounceLooseGameModalCtrl as modalCtrl",
                size: "md",
                windowTopClass: "modal-game-non-add-point-window",
                resolve: {
                    MessageModel: messageModel
                }
            });
        }

        function showGameTieModal(messageModel) {
            return $uibModal.open({
                templateUrl: url.tmpl('/poke-card-game/announce-draw-game-modal.html'),
                controller: "AnnounceDrawGameModalCtrl as modalCtrl",
                size: "md",
                windowTopClass: "modal-game-add-point-window",
                resolve: {
                    MessageModel: messageModel
                }
            });
        }

        // test part
        service.showUibStandardModal = function() {
            return $uibModal.open({
                templateUrl: url.tmpl('/v2-modals/uib-standard-modal.html'),
                controller: "UibStandardModalCtrl as $ctrl",
                size: "md"
            });
        }

        return service;
    }

})(window.angular);
﻿(function (angular) {
    'use strict';

    angular
        .module("CalcheeseFrontSite")
        .factory("GameClientService", GameClientService);

    GameClientService.$inject = ['$rootScope', 'Hub', 'siteBaseUrl', 'MessageAlertService', 'UserService'];

    function GameClientService($rootScope, Hub, siteBaseUrl, MessageAlertService, UserService) {
        var service = this;

        /** @memberOf GameClientService **/
        service.CurrentUser = UserService.CurrentUser;

        /** @memberOf GameClientService **/
        //service.connections = [];

        /** @memberOf GameClientService **/
        service.sendNewGameRequest = sendNewGameRequest;

        /** @memberOf GameClientService **/
        service.findRandomOpponentPlayer = findRandomOpponentPlayer;

        /** @memberOf GameClientService **/
        service.submitPokemonCard = submitPokemonCard;

        /** @memberOf GameClientService **/
        service.stopGameRequest = stopGameRequest;

        var gameHub = new Hub("pokeCardGameHub", {

            //client side methods
            listeners: {
                "newConnection": function (connectionInfo) {
                    //console.log('newConnection -> ', connectionInfo);
                    //should notify other players to update online player list via event
                    //service.connections.push(connectionInfo);
                    //$rootScope.$apply();
                    $rootScope.$broadcast('game-hub:new-player-online', connectionInfo);
                },
                "removeConnection": function (connectionInfo) {
                    //console.log('removeConnection -> ', connectionInfo);
                    // notify player went offline
                    //service.connections.splice(service.connections.indexOf(connectionInfo), 1);
                    //$rootScope.$apply();
                    $rootScope.$broadcast('game-hub:player-disconnect', connectionInfo);
                },
                "failSendNewGameRequest": function (errorMessage) {
                    
                    $rootScope.$broadcast('game-hub:new-game-request-failed', errorMessage);
                },
                "unableToFindRandomOpponent": function (errorMessage) {
                    
                    $rootScope.$broadcast('game-hub:no-random-opponent', errorMessage);
                },
                "newGameRequestForYou": function(newGameRequestCommand) {

                    MessageAlertService.showGameConfirmationMessage(newGameRequestCommand)
                        .result.then(function() {
                                gameHub.confirmGameInvitation({
                                    PlayerUserId: service.CurrentUser.Id,
                                    PlayerUserName: service.CurrentUser.UserName,
                                    PlayerName: service.CurrentUser.FullName || service.CurrentUser.UserName,
                                    ToPlayerUserId: newGameRequestCommand.PlayerUserId,
                                    ToPlayerUserName: newGameRequestCommand.PlayerUserName,
                                    ToPlayerrName: newGameRequestCommand.PlayerName ||
                                        newGameRequestCommand.PlayerUserName,
                                    Ok: true
                                });
                            },
                            function() {
                                gameHub.confirmGameInvitation({
                                    PlayerUserId: service.CurrentUser.Id,
                                    PlayerUserName: service.CurrentUser.UserName,
                                    PlayerName: service.CurrentUser.FullName || service.CurrentUser.UserName,
                                    ToPlayerUserId: newGameRequestCommand.PlayerUserId,
                                    ToPlayerUserName: newGameRequestCommand.PlayerUserName,
                                    ToPlayerrName: newGameRequestCommand.PlayerName ||
                                        newGameRequestCommand.PlayerUserName,
                                    Ok: false
                                });
                            });
                },
                "confirmToStartGame": function (confirmGameInvitationCommand) {
                    //console.log('confirmToStartGame -> ', confirmGameInvitationCommand);
                    $rootScope.$broadcast('game-hub:game-start', confirmGameInvitationCommand);
                },
                "declineToStartGame": function (declineGameInvitationCommand) {
                    //console.log('declineToStartGame -> ', declineGameInvitationCommand);
                    $rootScope.$broadcast('game-hub:game-decline', declineGameInvitationCommand);
                },
                "notifyGameResult": function(gameResult) {
                    //console.log('game result arrive success-> ', gameResult);
                    $rootScope.$broadcast('game-hub:game-result-success', gameResult);
                },
                "notifySubmitCardError": function (error) {
                    //console.log('game result arrive error -> ', error);
                    $rootScope.$broadcast('game-hub:game-result-error', error);
                },
                "notifyGameHasBeenStopped": function() {
                    $rootScope.$broadcast('game-hub:game-end', {});
                }
            },

            //server side methods
            methods: ['sendNewGameRequest', 'findRandomOpponentPlayer', 'confirmGameInvitation', 'submitPokemonCard', 'stopGameRequest'],

            //query params sent on initial connection
            queryParams: {
            },

            //handle connection error
            errorHandler: function (error) {
                console.error(error);
            },
            hubDisconnected: function () {
                if (gameHub.connection.lastError) {
                    gameHub.connection.start();
                }
            },
            transport: 'webSockets',
            logging: false,

            //specify a non default root
            rootPath: siteBaseUrl + "/signalr",

            stateChanged: function (state) {
                switch (state.newState) {
                    case $.signalR.connectionState.connecting:
                        console.log('connecting');
                        break;
                    case $.signalR.connectionState.connected:
                        console.log('connected');
                        break;
                    case $.signalR.connectionState.reconnecting:
                        console.log('reconnecting');
                        break;
                    case $.signalR.connectionState.disconnected:
                        console.log('disconnected for user -> ', service.CurrentUser.Id);
                        break;
                }
            }
        });

        function sendNewGameRequest(newGameRequestCommand) {
            //console.log('client send new game request -> ', newGameRequestCommand);
            return gameHub.sendNewGameRequest(newGameRequestCommand);
        }

        function findRandomOpponentPlayer(newGameRequestCommand) {
            return gameHub.findRandomOpponentPlayer(newGameRequestCommand);
        }

        function submitPokemonCard(submitCardCommand) {
            return gameHub.submitPokemonCard(submitCardCommand);
        }

        // call server method to stop game session
        // on stop game success notify players that game has been stopped
        function stopGameRequest(stopGameReqCommand) {
            return gameHub.stopGameRequest(stopGameReqCommand);
        }

        return service;
    }
})(window.angular);
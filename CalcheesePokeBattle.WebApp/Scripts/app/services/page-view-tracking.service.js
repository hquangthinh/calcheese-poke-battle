﻿(function (angular) {
    'use strict';

    angular
        .module('CalcheeseFrontSite')
        .service('PageViewTrackingService', PageViewTrackingService);

    PageViewTrackingService.$inject = ['apis'];

    function PageViewTrackingService(apis) {
        var service = this;
        var serviceApi = apis.page_view_tracking;

        /** @memberOf PageViewTrackingService **/
        service.trackPageView = trackPageView;

        function trackPageView(trackPageModel) {
            return serviceApi.postToUri('track-page-view', trackPageModel);
        }
    }
})(window.angular);

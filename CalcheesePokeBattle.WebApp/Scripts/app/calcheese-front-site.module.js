﻿(function (angular, $) {
    'use strict';

    var app = angular
        .module('CalcheeseFrontSite',
        [
            'ngResource',
            'ngAnimate',
            'ngSanitize',
            'ui.bootstrap',
            'ui.router',
            'ngStorage',
            'angular-loading-bar',
            'ncy-angular-breadcrumb',
            'angularFileUpload',
            'ngImgCrop',
            'toastr',
            'angular-momentjs',
            'ngCookies',
            'vcRecaptcha',
            '720kb.socialshare',
            'angular-spinkit',
            'SignalR'
        ]);

    // Ramda & sweetalert support
    app.constant('R', window.R);
    app.constant('swal', window.swal);

    // Config
    app.config([
        '$httpProvider', '$uibTooltipProvider', 'config', 'apiBaseUrlProvider',
        '$uibModalProvider', '$compileProvider', '$qProvider', '$anchorScrollProvider','$sceDelegateProvider',
        function ($httpProvider,
            $tooltipProvider,
            config,
            apiBaseUrlProvider,
            $modalProvider,
            $compileProvider,
            $qProvider, $anchorScrollProvider, $sceDelegateProvider) {
            'use strict';
            // intercept for authorization
            var interceptor = [
                '$rootScope', 'config', '$q', 'R', function ($scope, $constant, $q, R) {
                    return {
                        'request': function (config) {
                            config.timeout = 60000;
                            return config;
                        },

                        'response': function (response) {
                            if (response.status === 204) { // No content
                                $scope.$state.go($constant.ROUTER.HOME);
                            }
                            return response;
                        },

                        'responseError': function (response) {
                            console.error('http response error', response);
                            // Replace error icon
                            angular.element('.loading').toggleClass('error');
                            var errorDetails = "";
                            // connection error , can not connect to server
                            if (response.status === 0 || response.status === -1) {
                                $("#Global-Modal-Error-Message").text("Không thể kết nối đến máy chủ. \r\n Vui lòng thử lại!");
                                $('#Global-Modal-Error').modal('show');
                            }
                            // bad request
                            else if (response.status === 400 && !response.config.ignoreError) {
                                if (response.data.ModelState) {
                                    errorDetails = R.join('\n', R.values(response.data.ModelState));
                                } else {
                                    errorDetails =
                                        (response.data.Message ? response.data.Message : "") +
                                        (response.data.ExceptionMessage ? response.data.ExceptionMessage : "") +
                                        (response.data.InnerException && response.data.InnerException.ExceptionMessage
                                            ? response.data.InnerException.ExceptionMessage
                                            : "");
                                }
                                // alert to notify
                                $("#Global-Modal-Error-Message").text(errorDetails);
                                $('#Global-Modal-Error').modal('show');
                            }
                            // unauthorized
                            else if (response.status === 401) {
                                swal({
                                    title: "Unauthorized",
                                    text: response.data.Message || "You are not authorised to perform this action.",
                                    type: "warning"
                                },
                                    function () {
                                        $scope.$state.go($constant.ROUTER.HOME);
                                    });
                            }
                            // forbidden
                            else if (response.status === 403) {
                                console.log('// redirect to home page');
                                if ($scope.$state.current.name !== $constant.ROUTER.DEFAULT) {
                                    $scope.$state.go($constant.ROUTER.DEFAULT, {}, { reload: true });
                                }
                            }
                            // 409: resource conflict
                            else if (response.status === 409) {
                                errorDetails =
                                    (response.data.Message ? response.data.Message : "") +
                                    (response.data.ExceptionMessage ? response.data.ExceptionMessage : "") +
                                    (response.data.InnerException && response.data.InnerException.ExceptionMessage
                                        ? response.data.InnerException.ExceptionMessage
                                        : "");
                                // alert to notify
                                swal({
                                    title: response.statusText,
                                    text: errorDetails,
                                    type: "error"
                                });
                                $scope.$state.go($scope.$state.current.name, $scope.$stateParams, { reload: true });
                                $scope.$emit($scope.$state.current.name + '_resource_conflict',
                                    $scope.$state,
                                    $scope.$stateParams);
                            }
                            // internal server error || method not allow
                            else if (response.status >= 500 || response.status === 405) {
                                $("#Global-Modal-Error-Message").text('Có lỗi xảy ra, vui lòng thử lại');
                                $('#Global-Modal-Error').modal('show');
                            }

                            return $q.reject(response);
                        }
                    };
                }
            ];

            $httpProvider.defaults.withCredentials = true;
            $httpProvider.interceptors.push(interceptor);

            // Tooltip config
            $tooltipProvider.options({
                popupDelay: 200,
                appendToBody: true
            });

            $modalProvider.options.animation = true;
            $modalProvider.options.size = "lg";

            $anchorScrollProvider.disableAutoScrolling();

            $sceDelegateProvider.resourceUrlWhitelist([
                // Allow same origin resource loads.
                'self',
                // Allow loading from our assets domain.  Notice the difference between * and **.
                'http://calcheese.vn/**',
                'http://www.calcheese.vn/**',
                'http://*.calcheese.vn/**'
            ]);

            // set to false if we don't want to see warning from framework
            $compileProvider.debugInfoEnabled(true);

            // turn off console error when a promise is rejected and there no handler for it
            $qProvider.errorOnUnhandledRejections(false);
        }
    ]);

    // Run
    app.run([
        '$rootScope', '$uibModalStack', '$state', '$stateParams', '$http', 'webBaseUrl', 'config',
        function ($rootScope,
            $modalStack,
            $state,
            $stateParams,
            $http,
            webBaseUrl,
            config) {

            // Anti-forgery token
            $http.defaults.headers.common['__RequestVerificationToken'] = angular.element('span#token-af')
                .attr('value');

            $rootScope.$state = $state;
            $rootScope.$stateParams = $stateParams;
            $rootScope.overlay = {
                show: false,
                content: 'loading...'
            };

            $rootScope.isInGiftPage = function() {
                return $state.current.name === config.ROUTER.GIFT_ITEMS;
            }

            $rootScope.isInJoinGamePage = function() {
                return $state.current.name === config.ROUTER.POKE_CARD_GAME_JOIN_GAME;
            }

            $rootScope.isInGamePlayGroundPage = function() {
                return $state.current.name === config.ROUTER.POKE_CARD_GAME;
            }

            $rootScope.isInPlayingGame = function() {
                return $state.current.name === config.ROUTER.POKE_CARD_GAME_PLAYING;
            }

            $rootScope.isProfilePage = function() {
                return $state.current.name === config.ROUTER.PLAYER_PROFILE;
            }

            $rootScope.isInPokeListPage = function() {
                return $state.current.name === config.ROUTER.POKE_CARDS;
            }

            // dismiss any active modal if current state changes
            $rootScope.$on('$stateChangeStart',
                function (event, toState, toParams, fromState, fromParams) {
                    var top = $modalStack.getTop();
                    if (top) $modalStack.dismiss(top.key);
                    //console.log('app run from -> ', fromState.name);
                    //console.log('app run to -> ', toState.name);
                });

            // Base url to used in view
            $rootScope.webUrl = webBaseUrl;
        }
    ]);

})(window.angular, window.$);
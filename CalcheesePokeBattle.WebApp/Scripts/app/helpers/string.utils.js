(function (angular) {
    'use strict';

    angular
        .module('CalcheeseFrontSite')
        .service('StringUtils', StringUtils);

    StringUtils.$inject = ['R'];

    function StringUtils(R) {
        var _this = this;

        /** @memberOf StringUtils **/
        _this.contains = R.curry(contains);

        /** @memberOf StringUtils **/
        _this.containsR = R.curry(function (second, first) {
            return contains(first, second);
        });

        /** @memberOf StringUtils **/
        _this.match = R.curry(match);

        /** @memberOf StringUtils **/
        _this.matchR = R.curry(function (second, first) {
            return match(first, second);
        });

        /** @memberOf StringUtils **/
        _this.isNullOrEmpty = isNullOrEmptyString;

        /** @memberOf StringUtils **/
        _this.isNotNullOrEmpty = R.complement(isNullOrEmptyString);

        function contains(first, second) {
            if (!first) return false;
            if (!second) return true;
            return first.toLowerCase().indexOf(second.toLowerCase()) >= 0;
        }

        function match(first, second) {
            if (!first) return false;
            if (!second) return true;
            var expr = second.toLowerCase().replace(/%/g, '.*');
            return first.toLowerCase().match(new RegExp(expr));
        }

        function isNullOrEmptyString (s) {
            return R.isNil(s) || R.trim(s) === '';
        }
    }
})(window.angular);
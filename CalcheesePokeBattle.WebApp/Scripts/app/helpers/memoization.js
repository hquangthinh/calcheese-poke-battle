(function (angular) {
    'use strict';

    angular
        .module('CalcheeseFrontSite')
        .service('Memoization', Memoization);

    Memoization.$inject = ['R', '$timeout'];

    function Memoization(R, $timeout) {
        var _this = this;

        var DEFAULT_TIMEOUT = 10000; // 10 seconds

        /**
         * Produces a timed memoized function to prevent memory leak.
         * The memoiser will be de-referenced after the timeout and will get a fresh instance for subsequent calls
         * @memberOf Memoization
         * */
        _this.memoize = memoize;

        function memoize(fn, timeout) {
            var memoized = null;

            return function () {
                if (!memoized) {
                    var p = $timeout(
                        function () {
                            memoized = null;
                            $timeout.cancel(p);
                        },
                        timeout || DEFAULT_TIMEOUT,
                        false
                    );
                    memoized = R.memoize(fn);
                }
                return memoized;
            };
        }
    }
})(window.angular);

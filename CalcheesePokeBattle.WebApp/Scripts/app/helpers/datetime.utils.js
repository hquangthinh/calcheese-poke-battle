(function (angular) {
    'use strict';

    angular
        .module('CalcheeseFrontSite')
        .service('DateUtils', DateUtils);

    DateUtils.$inject = ['R', 'SettingService', '$moment'];

    function DateUtils(R, SettingService, $moment) {
        var service = this;

        var DateFormats = {
            'ar_SA' : 'dd/MM/yy',
            'bg_BG' : 'dd.M.yyyy',
            'ca_ES' : 'dd/MM/yyyy',
            'zh_TW' : 'yyyy/M/d',
            'cs_CZ' : 'd.M.yyyy',
            'da_DK' : 'dd-MM-yyyy',
            'de_DE' : 'dd.MM.yyyy',
            'el_GR' : 'd/M/yyyy',
            'en_US' : 'M/d/yyyy',
            'fi_FI' : 'd.M.yyyy',
            'fr_FR' : 'dd/MM/yyyy',
            'he_IL' : 'dd/MM/yyyy',
            'hu_HU' : 'yyyy. MM. dd.',
            'is_IS' : 'd.M.yyyy',
            'it_IT' : 'dd/MM/yyyy',
            'ja_JP' : 'yyyy/MM/dd',
            'ko_KR' : 'yyyy-MM-dd',
            'nl_NL' : 'd-M-yyyy',
            'nb_NO' : 'dd.MM.yyyy',
            'pl_PL' : 'yyyy-MM-dd',
            'pt_BR' : 'd/M/yyyy',
            'ro_RO' : 'dd.MM.yyyy',
            'ru_RU' : 'dd.MM.yyyy',
            'hr_HR' : 'd.M.yyyy',
            'sk_SK' : 'd. M. yyyy',
            'sq_AL' : 'yyyy-MM-dd',
            'sv_SE' : 'yyyy-MM-dd',
            'th_TH' : 'd/M/yyyy',
            'tr_TR' : 'dd.MM.yyyy',
            'ur_PK' : 'dd/MM/yyyy',
            'id_ID' : 'dd/MM/yyyy',
            'uk_UA' : 'dd.MM.yyyy',
            'be_BY' : 'dd.MM.yyyy',
            'sl_SI' : 'd.M.yyyy',
            'et_EE' : 'd.MM.yyyy',
            'lv_LV' : 'yyyy.MM.dd.',
            'lt_LT' : 'yyyy.MM.dd',
            'fa_IR' : 'MM/dd/yyyy',
            'vi_VN' : 'dd/MM/yyyy',
            'hy_AM' : 'dd.MM.yyyy',
            'az_Latn-AZ' : 'dd.MM.yyyy',
            'eu_ES' : 'yyyy/MM/dd',
            'mk_MK' : 'dd.MM.yyyy',
            'af_ZA' : 'yyyy/MM/dd',
            'ka_GE' : 'dd.MM.yyyy',
            'fo_FO' : 'dd-MM-yyyy',
            'hi_IN' : 'dd-MM-yyyy',
            'ms_MY' : 'dd/MM/yyyy',
            'kk_KZ' : 'dd.MM.yyyy',
            'ky_KG' : 'dd.MM.yy',
            'sw_KE' : 'M/d/yyyy',
            'uz_Latn-UZ' : 'dd/MM yyyy',
            'tt_RU' : 'dd.MM.yyyy',
            'pa_IN' : 'dd-MM-yy',
            'gu_IN' : 'dd-MM-yy',
            'ta_IN' : 'dd-MM-yyyy',
            'te_IN' : 'dd-MM-yy',
            'kn_IN' : 'dd-MM-yy',
            'mr_IN' : 'dd-MM-yyyy',
            'sa_IN' : 'dd-MM-yyyy',
            'mn_MN' : 'yy.MM.dd',
            'gl_ES' : 'dd/MM/yy',
            'kok_IN' : 'dd-MM-yyyy',
            'syr_SY' : 'dd/MM/yyyy',
            'dv_MV' : 'dd/MM/yy',
            'ar_IQ' : 'dd/MM/yyyy',
            'zh_CN' : 'yyyy/M/d',
            'de_CH' : 'dd.MM.yyyy',
            'en_GB' : 'dd/MM/yyyy',
            'es_MX' : 'dd/MM/yyyy',
            'fr_BE' : 'd/MM/yyyy',
            'it_CH' : 'dd.MM.yyyy',
            'nl_BE' : 'd/MM/yyyy',
            'nn_NO' : 'dd.MM.yyyy',
            'pt_PT' : 'dd-MM-yyyy',
            'sr_Latn-CS' : 'd.M.yyyy',
            'sv_FI' : 'd.M.yyyy',
            'az_Cyrl-AZ' : 'dd.MM.yyyy',
            'ms_BN' : 'dd/MM/yyyy',
            'uz_Cyrl-UZ' : 'dd.MM.yyyy',
            'ar_EG' : 'dd/MM/yyyy',
            'zh_HK' : 'd/M/yyyy',
            'de_AT' : 'dd.MM.yyyy',
            'en_AU' : 'd/MM/yyyy',
            'es_ES' : 'dd/MM/yyyy',
            'fr_CA' : 'yyyy-MM-dd',
            'sr_Cyrl-CS' : 'd.M.yyyy',
            'ar_LY' : 'dd/MM/yyyy',
            'zh_SG' : 'd/M/yyyy',
            'de_LU' : 'dd.MM.yyyy',
            'en_CA' : 'dd/MM/yyyy',
            'es_GT' : 'dd/MM/yyyy',
            'fr_CH' : 'dd.MM.yyyy',
            'ar_DZ' : 'dd-MM-yyyy',
            'zh_MO' : 'd/M/yyyy',
            'de_LI' : 'dd.MM.yyyy',
            'en_NZ' : 'd/MM/yyyy',
            'es_CR' : 'dd/MM/yyyy',
            'fr_LU' : 'dd/MM/yyyy',
            'ar_MA' : 'dd-MM-yyyy',
            'en_IE' : 'dd/MM/yyyy',
            'es_PA' : 'MM/dd/yyyy',
            'fr_MC' : 'dd/MM/yyyy',
            'ar_TN' : 'dd-MM-yyyy',
            'en_ZA' : 'yyyy/MM/dd',
            'es_DO' : 'dd/MM/yyyy',
            'ar_OM' : 'dd/MM/yyyy',
            'en_JM' : 'dd/MM/yyyy',
            'es_VE' : 'dd/MM/yyyy',
            'ar_YE' : 'dd/MM/yyyy',
            'en_029' : 'MM/dd/yyyy',
            'es_CO' : 'dd/MM/yyyy',
            'ar_SY' : 'dd/MM/yyyy',
            'en_BZ' : 'dd/MM/yyyy',
            'es_PE' : 'dd/MM/yyyy',
            'ar_JO' : 'dd/MM/yyyy',
            'en_TT' : 'dd/MM/yyyy',
            'es_AR' : 'dd/MM/yyyy',
            'ar_LB' : 'dd/MM/yyyy',
            'en_ZW' : 'M/d/yyyy',
            'es_EC' : 'dd/MM/yyyy',
            'ar_KW' : 'dd/MM/yyyy',
            'en_PH' : 'M/d/yyyy',
            'es_CL' : 'dd-MM-yyyy',
            'ar_AE' : 'dd/MM/yyyy',
            'es_UY' : 'dd/MM/yyyy',
            'ar_BH' : 'dd/MM/yyyy',
            'es_PY' : 'dd/MM/yyyy',
            'ar_QA' : 'dd/MM/yyyy',
            'es_BO' : 'dd/MM/yyyy',
            'es_SV' : 'dd/MM/yyyy',
            'es_HN' : 'dd/MM/yyyy',
            'es_NI' : 'dd/MM/yyyy',
            'es_PR' : 'dd/MM/yyyy',
            'am_ET' : 'd/M/yyyy',
            'tzm_Latn-DZ' : 'dd-MM-yyyy',
            'iu_Latn-CA' : 'd/MM/yyyy',
            'sma_NO' : 'dd.MM.yyyy',
            'mn_Mong-CN' : 'yyyy/M/d',
            'gd_GB' : 'dd/MM/yyyy',
            'en_MY' : 'd/M/yyyy',
            'prs_AF' : 'dd/MM/yy',
            'bn_BD' : 'dd-MM-yy',
            'wo_SN' : 'dd/MM/yyyy',
            'rw_RW' : 'M/d/yyyy',
            'qut_GT' : 'dd/MM/yyyy',
            'sah_RU' : 'MM.dd.yyyy',
            'gsw_FR' : 'dd/MM/yyyy',
            'co_FR' : 'dd/MM/yyyy',
            'oc_FR' : 'dd/MM/yyyy',
            'mi_NZ' : 'dd/MM/yyyy',
            'ga_IE' : 'dd/MM/yyyy',
            'se_SE' : 'yyyy-MM-dd',
            'br_FR' : 'dd/MM/yyyy',
            'smn_FI' : 'd.M.yyyy',
            'moh_CA' : 'M/d/yyyy',
            'arn_CL' : 'dd-MM-yyyy',
            'ii_CN' : 'yyyy/M/d',
            'dsb_DE' : 'd. M. yyyy',
            'ig_NG' : 'd/M/yyyy',
            'kl_GL' : 'dd-MM-yyyy',
            'lb_LU' : 'dd/MM/yyyy',
            'ba_RU' : 'dd.MM.yy',
            'nso_ZA' : 'yyyy/MM/dd',
            'quz_BO' : 'dd/MM/yyyy',
            'yo_NG' : 'd/M/yyyy',
            'ha_Latn-NG' : 'd/M/yyyy',
            'fil_PH' : 'M/d/yyyy',
            'ps_AF' : 'dd/MM/yy',
            'fy_NL' : 'd-M-yyyy',
            'ne_NP' : 'M/d/yyyy',
            'se_NO' : 'dd.MM.yyyy',
            'iu_Cans-CA' : 'd/M/yyyy',
            'sr_Latn-RS' : 'd.M.yyyy',
            'si_LK' : 'yyyy-MM-dd',
            'sr_Cyrl-RS' : 'd.M.yyyy',
            'lo_LA' : 'dd/MM/yyyy',
            'km_KH' : 'yyyy-MM-dd',
            'cy_GB' : 'dd/MM/yyyy',
            'bo_CN' : 'yyyy/M/d',
            'sms_FI' : 'd.M.yyyy',
            'as_IN' : 'dd-MM-yyyy',
            'ml_IN' : 'dd-MM-yy',
            'en_IN' : 'dd-MM-yyyy',
            'or_IN' : 'dd-MM-yy',
            'bn_IN' : 'dd-MM-yy',
            'tk_TM' : 'dd.MM.yy',
            'bs_Latn-BA' : 'd.M.yyyy',
            'mt_MT' : 'dd/MM/yyyy',
            'sr_Cyrl-ME' : 'd.M.yyyy',
            'se_FI' : 'd.M.yyyy',
            'zu_ZA' : 'yyyy/MM/dd',
            'xh_ZA' : 'yyyy/MM/dd',
            'tn_ZA' : 'yyyy/MM/dd',
            'hsb_DE' : 'd. M. yyyy',
            'bs_Cyrl-BA' : 'd.M.yyyy',
            'tg_Cyrl-TJ' : 'dd.MM.yy',
            'sr_Latn-BA' : 'd.M.yyyy',
            'smj_NO' : 'dd.MM.yyyy',
            'rm_CH' : 'dd/MM/yyyy',
            'smj_SE' : 'yyyy-MM-dd',
            'quz_EC' : 'dd/MM/yyyy',
            'quz_PE' : 'dd/MM/yyyy',
            'hr_BA' : 'd.M.yyyy.',
            'sr_Latn-ME' : 'd.M.yyyy',
            'sma_SE' : 'yyyy-MM-dd',
            'en_SG' : 'd/M/yyyy',
            'ug_CN' : 'yyyy-M-d',
            'sr_Cyrl-BA' : 'd.M.yyyy',
            'es_US' : 'M/d/yyyy'
        };

        /** @memberOf DateUtils **/
        service.getDateFormat = getDateFormat;

        /** @memberOf DateUtils **/
        service.getDateTimeFormat = getDateTimeFormat;

        /** @memberOf DateUtils **/
        service.fromString = fromString;

        /** @memberOf DateUtils **/
        service.correctValues = R.curry(correctValues);

        /** @memberOf DateUtils **/
        service.isSameOrAfter = isSameOrAfter;

        function getDateFormat() {
            return DateFormats[SettingService.getPreferredLocale()];
        }

        function getDateTimeFormat() {
            return getDateFormat() + ' H:mm';
        }

        function fromString(value) {
            return value ? new Date(value) : null;
        }

        function correctValues(props, obj) {
            return R.reduce(function (agg, prop) {
                return R.assoc(prop, fromString(R.prop(prop, agg)), agg);
            }, obj, props);
        }

        /* check whether dateTo is same or after dateFrom*/
        function isSameOrAfter(dateFrom, dateTo) {
            var fromDate = $moment(dateFrom);
            var toDate = $moment(dateTo);
            return fromDate.isValid() && toDate.isValid() && (toDate.isSameOrAfter(fromDate));
        }
    }
})(window.angular);
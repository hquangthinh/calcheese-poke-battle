﻿(function (angular) {
    'use strict';

    angular
        .module('CalcheeseFrontSite')
        .service('FormValidationUtils', FormValidationUtils);

    FormValidationUtils.$inject = [];

    function FormValidationUtils() {
        var service = this;

        service.validateRequire = validateRequire;
        service.validateMatch = validateMatch;
        service.validateTrue = validateTrue;
        service.validateLength = validateLength;

        function validateRequire(fieldName, errorMessage, modelToValidate) {
            // Convention for Error field: FieldName + 'Error'
            if (angular.isUndefined(modelToValidate[fieldName]) || modelToValidate[fieldName] === null || modelToValidate[fieldName].toString().trim().length === 0) {
                modelToValidate[fieldName + 'Error'] = errorMessage || 'Value is required';
                return false;
            } else {
                delete modelToValidate[fieldName + 'Error'];
            }
            return true;
        };

        function validateMatch(fieldName1, fieldName2, errorMessage, modelToValidate) {
            if (modelToValidate[fieldName1] &&
                modelToValidate[fieldName2] &&
                modelToValidate[fieldName1] !== modelToValidate[fieldName2]) {
                modelToValidate[fieldName2 + 'MatchError'] =
                    errorMessage || 'Value of does [' + fieldName2 + '] not match [' + fieldName1 + ']';
                return false;
            } else {
                delete modelToValidate[fieldName2 + 'MatchError'];
            }
            return true;
        }

        function validateTrue(fieldName, errorMessage, modelToValidate) {
            if (!modelToValidate[fieldName]) {
                modelToValidate[fieldName + 'Error'] = errorMessage;
                return false;
            } else {
                delete modelToValidate[fieldName + 'Error'];
            }
            return true;
        }

        function validateLength(fieldName, minLength, maxLength, errorMessage, modelToValidate) {

            var lengthOfValueToCheck = modelToValidate[fieldName] && typeof (modelToValidate[fieldName]) === 'string'
                ? modelToValidate[fieldName].trim().length
                : -1;

            if (lengthOfValueToCheck === -1) return true;

            if (lengthOfValueToCheck < minLength || lengthOfValueToCheck > maxLength) {
                modelToValidate[fieldName + 'LengthError'] = errorMessage || 'Tối thiểu là ' + minLength + ' ký tự và tối đa ' + maxLength + ' ký tự';
                return false;
            } else {
                delete modelToValidate[fieldName + 'LengthError'];
            }
            return true;
        }
    }

})(window.angular);
﻿using System;
using System.Configuration;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using CalcheesePokeBattle.Common.Cryptography;
using Dapper;

namespace CalcheesePokeBattle.ReportGeneratorJob
{
    public class UsedCouponCodeReportGenerator
    {
        private const string UsedCouponCodeReportGeneratorLastRunSettingKey = "UsedCouponCodeReportGeneratorLastRunSettingKey";

        public void GenerateUsedCouponCodeReport()
        {
            var connString = ConfigurationManager.ConnectionStrings["CalcheeseAppDbContext"].ConnectionString;

            using (var db = new SqlConnection(connString))
            {
                db.Open();
                var reportGeneratorLastRunDateString =
                    db.QueryFirst<string>(
                        "SELECT TOP 1 SettingValueString FROM Setting WHERE SettingKey = @UsedCouponCodeReportGeneratorLastRunSettingKey", 
                            new
                            {
                                UsedCouponCodeReportGeneratorLastRunSettingKey = new DbString
                                {
                                    Value = UsedCouponCodeReportGeneratorLastRunSettingKey,
                                    IsAnsi = false,
                                    IsFixedLength = false
                                }
                            });

                if(string.IsNullOrEmpty(reportGeneratorLastRunDateString))
                    return;

                var getUsedCodeQuery =
                    @"SELECT uc.Id, uc.CouponCode, uc.CreatedDate, uc.UserId, uc.CodePoint, uc.ClientIpAddress, uc.ClientInfo,
                        p.UserName, p.FirstName, p.LastName, p.PhoneNumber as UserPhoneNumber, p.Address as UserAddress, p.Email as UserEmail
                            FROM UsedCouponCode uc INNER JOIN UserProfile p ON uc.UserId = p.Id
                                WHERE uc.CreatedDate >= @reportGeneratorLastRunDateString";

                var usedCodeListWithUserInfo = db.Query<UsedCouponCodeReportDTO>(getUsedCodeQuery, new
                    {
                        reportGeneratorLastRunDateString = DateTime.ParseExact(reportGeneratorLastRunDateString,
                            "yyyy-MM-dd hh:mm:ss", DateTimeFormatInfo.CurrentInfo)
                    })
                    .ToList();
                var hasher = new MD5DataEncryptionService();
                foreach (var codeReportDto in usedCodeListWithUserInfo)
                {
                    // get detail from
                    var encCode = hasher.GetHashString(codeReportDto.CouponCode);
                    var couponCodeDto = db.QueryFirst<CouponCodeDTO>(
                        "SELECT TOP 1 * FROM CouponCode WHERE Code = @encCode", new
                        {
                            encCode = new DbString
                            {
                                Value = encCode,
                                IsAnsi = false,
                                IsFixedLength = false
                            }
                        });
                    if(couponCodeDto == null)
                        continue;
                    
                    // query card info
                    var getCardQuery = "SELECT Id, CardType, CardName, ImageUrl FROM PokeCard WHERE Id = @CardId";
                    var pokeCardDto = db.QueryFirst<PokeCardDTO>(getCardQuery, new
                    {
                        CardId = couponCodeDto.CardId
                    });
                    if(pokeCardDto ==null)
                        continue;

                    codeReportDto.CardId = couponCodeDto.CardId;
                    codeReportDto.CardType = pokeCardDto.CardType;
                    codeReportDto.CardName = pokeCardDto.CardName;
                    codeReportDto.CardImageUrl = pokeCardDto.ImageUrl;

                    // insert entry to UsedCouponCodeReport
                    var insertReportSql = @"INSERT INTO [dbo].[UsedCouponCodeReport]
                                               ([CouponCode]
                                               ,[CreatedDate]
                                               ,[UserId]
                                               ,[UserName]
                                               ,[UserFullName]
                                               ,[UserPhoneNumber]
                                               ,[UserAddress]
                                               ,[UserEmail]
                                               ,[ClientIpAddress]
                                               ,[ClientInfo]
                                               ,[CardId]
                                               ,[CardType]
                                               ,[CardName]
                                               ,[CardImageUrl]
                                               ,[CodePoint])
                                         VALUES
                                               (@CouponCode, @CreatedDate, @UserId, @UserName, @UserFullName, @UserPhoneNumber
                                               ,@UserAddress, @UserEmail, @ClientIpAddress, @ClientInfo
                                               , @CardId, @CardName, @CardType, @CardImageUrl, @CodePoint)";

                    db.Execute(insertReportSql, new
                    {
                        CouponCode = new DbString
                        {
                            Value = codeReportDto.CouponCode,
                            IsAnsi = false,
                            IsFixedLength = false
                        },
                        CreatedDate = codeReportDto.CreatedDate,
                        UserId = new DbString
                        {
                            Value = codeReportDto.UserId,
                            IsAnsi = false,
                            IsFixedLength = false
                        },
                        UserName = new DbString
                        {
                            Value = codeReportDto.UserName,
                            IsAnsi = false,
                            IsFixedLength = false
                        },
                        UserFullName = new DbString
                        {
                            Value = string.IsNullOrEmpty(codeReportDto.UserFullName) ? string.Empty : codeReportDto.UserFullName,
                            IsAnsi = false,
                            IsFixedLength = false
                        },
                        UserPhoneNumber = new DbString
                        {
                            Value = string.IsNullOrEmpty(codeReportDto.UserPhoneNumber) ? string.Empty : codeReportDto.UserPhoneNumber,
                            IsAnsi = false,
                            IsFixedLength = false
                        },
                        UserAddress = new DbString
                        {
                            Value = string.IsNullOrEmpty(codeReportDto.UserAddress) ? string.Empty : codeReportDto.UserAddress,
                            IsAnsi = false,
                            IsFixedLength = false
                        },
                        UserEmail = new DbString
                        {
                            Value = string.IsNullOrEmpty(codeReportDto.UserEmail) ? String.Empty : codeReportDto.UserEmail,
                            IsAnsi = false,
                            IsFixedLength = false
                        },
                        ClientIpAddress = new DbString
                        {
                            Value = codeReportDto.ClientIpAddress,
                            IsAnsi = false,
                            IsFixedLength = false
                        },
                        ClientInfo = new DbString
                        {
                            Value = codeReportDto.ClientInfo,
                            IsAnsi = false,
                            IsFixedLength = false
                        },
                        CardId = couponCodeDto.CardId,
                        CardName = new DbString
                        {
                            Value = codeReportDto.CardName,
                            IsAnsi = false,
                            IsFixedLength = false
                        },
                        CardType = new DbString
                            {
                            Value = codeReportDto.CardType,
                            IsAnsi = false,
                            IsFixedLength = false
                        },
                        CardImageUrl = new DbString
                        {
                            Value = $"https://storagecalcheeseprod.blob.core.windows.net/images/{codeReportDto.CardImageUrl}",
                            IsAnsi = false,
                            IsFixedLength = false
                        },
                        CodePoint = codeReportDto.CodePoint
                    });
                }

                // update setting last run time for this job
                db.Execute("UPDATE Setting Set SettingValueString = @SettingValueString WHERE SettingKey = @SettingKey",
                    new
                    {
                        SettingValueString = new DbString
                        {
                            Value = $"{DateTime.UtcNow:yyyy-MM-dd hh:mm:ss}",
                            IsAnsi = false,
                            IsFixedLength = false
                        },
                        SettingKey = new DbString
                        {
                            Value = "UsedCouponCodeReportGeneratorLastRunSettingKey",
                            IsAnsi = false,
                            IsFixedLength = false
                        }
                    });
            }
        }
    }

    internal class PokeCardDTO
    {
        public int Id { get; set; }

        public string CardType { get; set; }

        public string CardName { get; set; }

        public string ImageUrl { get; set; }
    }

    internal class CouponCodeDTO
    {
        public int Id { get; set; }

        public string Code { get; set; }

        public string Description { get; set; }

        public bool Available { get; set; }

        public int Point { get; set; }

        public int? CardId { get; set; }

        public string CardImageUrl { get; set; }

        public string RedeemByUserName { get; set; }

        public DateTime? RedeemOn { get; set; }

        public string ClientIpAddress { get; set; }

        public string ClientInfo { get; set; }
    }

    internal class UsedCouponCodeReportDTO
    {
        public int Id { get; set; }

        public string CouponCode { get; set; }

        public DateTime CreatedDate { get; set; }

        public string UserId { get; set; }

        public string UserName { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string UserFullName
            => string.IsNullOrEmpty(FirstName) && string.IsNullOrEmpty(LastName) ? UserName : $"{FirstName} {LastName}";

        public string UserPhoneNumber { get; set; }

        public string UserAddress { get; set; }

        public string UserEmail { get; set; }

        public string ClientIpAddress { get; set; }

        public string ClientInfo { get; set; }

        public int? CodePoint { get; set; }

        public int? CardId { get; set; }

        public string CardType { get; set; }

        public string CardName { get; set; }

        public string CardImageUrl { get; set; }
    }
}

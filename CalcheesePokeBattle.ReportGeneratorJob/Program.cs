﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalcheesePokeBattle.ReportGeneratorJob
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Running...");
            var reportGeneratorForUsedCouponCode = new UsedCouponCodeReportGenerator();
            reportGeneratorForUsedCouponCode.GenerateUsedCouponCodeReport();
        }
    }
}

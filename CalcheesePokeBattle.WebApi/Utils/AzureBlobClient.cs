﻿using System;
using System.Configuration;
using CalcheesePokeBattle.Common.DTO;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;

namespace CalcheesePokeBattle.WebApi.Utils
{
    public class AzureBlobClient : IDisposable
    {
        public string ContainerName { get; set; }
        private CloudBlobClient blobClient;
        private CloudBlobContainer blobContainer;

        public void InitStorage()
        {
            var storageAccount = CloudStorageAccount.Parse(ConfigurationManager.ConnectionStrings["AzureWebJobsStorage"].ConnectionString);
            // Create a blob client for interacting with the blob service.
            blobClient = storageAccount.CreateCloudBlobClient();
            blobContainer = blobClient.GetContainerReference(ContainerName);
            blobContainer.CreateIfNotExists();
            blobContainer.SetPermissions(new BlobContainerPermissions
            {
                PublicAccess = BlobContainerPublicAccessType.Blob
            });
        }

        public BlobInformation UploadFile(byte[] data, string fileName)
        {
            var blob = blobContainer.GetBlockBlobReference(fileName);
            blob.UploadFromByteArray(data, 0, data.Length);
            var blobInfo = new BlobInformation
            {
                BlobUri = new Uri(blob.Uri.ToString())
            };
            return blobInfo;
        }

        public void Dispose()
        {
            blobContainer = null;
            blobClient = null;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using CalcheesePokeBattle.Authentication.Models;
using CalcheesePokeBattle.Common.DTO;
using CalcheesePokeBattle.DTO;
using Newtonsoft.Json;
using Microsoft.WindowsAzure.Storage.Queue;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.WindowsAzure.Storage;

namespace  CalcheesePokeBattle.WebApi.Utils
{
    public class CouponDataHelper
    {
        private const string dataImportBlobContainerName = "dataforimport";
        private const string importCouponCodeQueueName = "importcouponcoderequest";

        private static CloudQueueClient queueClient;
        private static CloudQueue dataImportQueue;

        private static CloudBlobClient blobClient;
        private static CloudBlobContainer blobContainer;

        public void InitStorage()
        {
            if (queueClient != null && blobClient != null)
                return;

            var storageAccount = CloudStorageAccount.Parse(ConfigurationManager.ConnectionStrings["AzureWebJobsStorage"].ConnectionString);
            // Create a blob client for interacting with the blob service.
            blobClient = storageAccount.CreateCloudBlobClient();
            blobContainer = blobClient.GetContainerReference(dataImportBlobContainerName);
            blobContainer.CreateIfNotExists();

            // Create a queue client
            queueClient = storageAccount.CreateCloudQueueClient();
            dataImportQueue = queueClient.GetQueueReference(importCouponCodeQueueName);
            dataImportQueue.CreateIfNotExists();
        }

        /// <summary>
        /// Upload file to the import blob
        /// </summary>
        /// <param name="filePaths"></param>
        /// <returns></returns>
        public void UploadDataForImport(List<string> filePaths)
        {
            foreach (var filePath in filePaths)
            {
                var fi = new FileInfo(filePath);
                var blob = blobContainer.GetBlockBlobReference(fi.Name);
                blob.UploadFromFile(fi.FullName);

                // send message to import queue
                var blobInfo = new BlobInformation
                {
                    BlobUri = new Uri(blob.Uri.ToString())
                };
                var queueMessage = new CloudQueueMessage(JsonConvert.SerializeObject(blobInfo));
                dataImportQueue.AddMessage(queueMessage);
            }
        }

        public string UploadFileForImport(string filePath)
        {
            var fi = new FileInfo(filePath);
            var blob = blobContainer.GetBlockBlobReference(fi.Name);
            blob.UploadFromFile(fi.FullName);
            return blob.Uri.ToString();
        }

        public void UploadDataForImport(string folderPath)
        {
            var files = Directory.GetFiles(folderPath).ToList();
            UploadDataForImport(files);
        }

        public void TriggerQueueMessageForImportCouponCode(UserProfileDTO currentUser, string clientIp)
        {
            if (currentUser?.UserRoles == null)
                return;

            // check user role
            if (currentUser.UserRoles.Any(r => r.Name.Equals(RoleDTO.Administrator)))
            {
                InitStorage();
                var queueMessage = new CloudQueueMessage(JsonConvert.SerializeObject(new BatchImportQueueMessage
                {
                    Message = currentUser.UserName,
                    IpAddress = clientIp
                }));
                dataImportQueue.AddMessage(queueMessage);
            }
        }
    }
}
﻿using CalcheesePokeBattle.Common.Monads;
using CalcheesePokeBattle.DTO;
using CalcheesePokeBattle.Service;
using CalcheesePokeBattle.ViewModel;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.ModelBinding;

namespace CalcheesePokeBattle.WebApi.Controllers
{

    /// <summary>
    /// Winner list user for public page
    /// </summary>
    [RoutePrefix("api/winnerlist")]
    public class WinnerListUserApiController : ApiControllerBase
    {
        public WinnerListUserApiController(IServiceFactory serviceFactory) : base(serviceFactory)
        {
        }
        [HttpGet]
        [Route("InitData")]
        [ResponseType(typeof(CurrentWeekViewModel))]
        public IHttpActionResult InitData()
        {
            return RespondWith(() => ServiceFactory.GetService<IWinnerListService>().GetWeeekDefinitions());
            
        }

        [HttpGet]
        [Route("GetWinnerList")]
        [ResponseType(typeof(IEnumerable<WinnerListDTO>))]
        public IHttpActionResult GetWinnerList(int weekNumber)
        {
            if (weekNumber == 0) return RespondWith(() => new WinnerListViewModel());
            return RespondWith(() => ServiceFactory.GetService<IWinnerListService>().GetWinnerListForPublicView(weekNumber));
        }

        [HttpGet]
        [Route("GetRedeemList")]
        [ResponseType(typeof(PagingResult<GiftRedeemHistoryDTO>))]
        public IHttpActionResult GetRedeemList([ModelBinder]SearchGiftRedeemHistoryCommand command)
        {
            return RespondWith(() => ServiceFactory.GetService<IWinnerListService>().GetRedeemList(command));
        }

    }
}

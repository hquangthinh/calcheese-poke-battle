﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.ModelBinding;
using CalcheesePokeBattle.Common.DTO;
using CalcheesePokeBattle.Common.Monads;
using CalcheesePokeBattle.DTO;
using CalcheesePokeBattle.Service;
using CalcheesePokeBattle.WebApi.Utils;
using Dapper;

namespace CalcheesePokeBattle.WebApi.Controllers
{
    [Authorize]
    [RoutePrefix("api/page-view-tracking")]
    public class PageViewTrackingApiController : ApiControllerBase
    {
        public PageViewTrackingApiController(IServiceFactory serviceFactory) : base(serviceFactory)
        {
        }

        [HttpPost]
        [Route("all-page-view")]
        [ResponseType(typeof(PagingResult<PageViewTrackingDTO>))]
        public IHttpActionResult GetAllPageView([FromBody] PageViewReportQuery query)
        {
            var lastAccessDate = CalculateLastAccessDateFromLastAccessTypeAsDateTimeParam(query);
            var sql = @"select Id, PageTitle, PageUrl, DATEADD(hour,7,AccessDateTime) as AccessDateTime, 
                            ClientIpAddress, ClientInfo, UserName, UserId 
                        from PageViewTracking 
                        where AccessDateTime >= @lastAccessDate 
                        order by AccessDateTime desc";

            using (var db = new SqlConnection(CalcheeseAppDbContextConnectionString))
            {
                db.Open();
                var pageViewList = db.Query<PageViewTrackingDTO>(sql, new
                {
                    lastAccessDate = lastAccessDate
                }).ToList();
                return RespondWith(() => PagingResult<PageViewTrackingDTO>.AllResult(pageViewList));
            }
        }

        private DateTime CalculateLastAccessDateFromLastAccessTypeAsDateTimeParam(PageViewReportQuery query)
        {
            if (query == null)
                return DateTime.UtcNow.AddDays(-7);

            // 1-month, 1-week, 24-hours
            if ("1-month".Equals(query.LastAccessType))
                return DateTime.UtcNow.AddMonths(-1);

            if ("1-week".Equals(query.LastAccessType))
                return DateTime.UtcNow.AddDays(-7);

            return DateTime.UtcNow.AddDays(-7);
        }

        private string CalculateLastAccessDateFromLastAccessType(PageViewReportQuery query)
        {
            if (query == null)
                return DateTime.UtcNow.AddDays(-7).ToString("yyyy-MM-dd");

            // 1-month, 1-week, 24-hours
            if ("1-month".Equals(query.LastAccessType))
                return DateTime.UtcNow.AddMonths(-1).ToString("yyyy-MM-dd");

            if ("1-week".Equals(query.LastAccessType))
                return DateTime.UtcNow.AddDays(-7).ToString("yyyy-MM-dd");

            return DateTime.UtcNow.AddDays(-7).ToString("yyyy-MM-dd");
        }

        [HttpPost]
        [Route("page-view-report-detail")]
        [ResponseType(typeof(List<PageViewTrackingDTO>))]
        public IHttpActionResult GetPageViewReportDetail([FromBody] PageViewReportQuery query)
        {
            var sql = "select * from PageViewTracking where PageTitle = @PageTitle";
            using (var db = new SqlConnection(CalcheeseAppDbContextConnectionString))
            {
                db.Open();
                var pageViewList = db.Query<PageViewTrackingDTO>(sql, new
                    {
                        PageTitle = new DbString {Value = query.PageTitle, IsFixedLength = false, IsAnsi = false}
                    })
                    .ToList();
                return RespondWith(() => pageViewList);
            }
        }

        [HttpPost]
        [Route("page-view-report-summary-by-page")]
        [ResponseType(typeof(PagingResult<PageViewTrackingSummaryByPageDTO>))]
        public IHttpActionResult GetPageViewReportSummaryByPage()
        {
            var sql =
                "select PageTitle, PageUrl, count(*) as TotalVisit from PageViewTracking group by PageTitle, PageUrl";

            using (var db = new SqlConnection(CalcheeseAppDbContextConnectionString))
            {
                db.Open();
                var pageViewList = db.Query<PageViewTrackingSummaryByPageDTO>(sql).ToList();
                return RespondWith(() => PagingResult<PageViewTrackingSummaryByPageDTO>.AllResult(pageViewList));
            }
        }

        private List<PageViewTrackingSummaryByUserDTO> GetPageViewReportSummaryByUserInternal(PageViewTrackingDTO command)
        {
            var sql =
                @"select UserName, PageTitle, PageUrl, WeekNumber, count(*) as TotalVisit 
                    from vwAllPageViewTracking 
                    where (@PageTitle is null or @PageTitle = '' or PageTitle = @PageTitle) and (@WeekNumber is null or WeekNumber = @WeekNumber)
                    group by UserName, PageTitle, PageUrl, WeekNumber
                    order by WeekNumber";

            using (var db = new SqlConnection(CalcheeseAppDbContextConnectionString))
            {
                db.Open();
                var pageViewList = db.Query<PageViewTrackingSummaryByUserDTO>(sql, new
                    {
                        PageTitle = new DbString
                        {
                            IsFixedLength = false,
                            IsAnsi = false,
                            Length = 50,
                            Value = command.PageTitle
                        },
                        WeekNumber = command.WeekNumber
                    })
                    .ToList();
                return pageViewList;
            }
        }

        [HttpPost]
        [Route("page-view-report-summary-by-username")]
        [ResponseType(typeof(PagingResult<PageViewTrackingSummaryByUserDTO>))]
        public IHttpActionResult GetPageViewReportSummaryByUser([FromBody]PageViewTrackingDTO command)
        {
            var pageViewList = GetPageViewReportSummaryByUserInternal(command);
            return RespondWith(() => PagingResult<PageViewTrackingSummaryByUserDTO>.AllResult(pageViewList));
        }

        [HttpGet]
        [Route("export-page-view-report-summary-by-username")]
        public HttpResponseMessage ExportPageViewReportSummaryByUserName()
        {
            var pageViewList = GetPageViewReportSummaryByUserInternal(new PageViewTrackingDTO());

            var exportPath = GetPhysicalExportPath();

            var headers = new List<string>
            {
                "Tên Đăng Nhập",
                "Page",
                "Url",
                "Tuần",
                "Lượt Truy Cập"
            };

            var exportFileName =
                $"page-view-report-summary-by-username-exported-{DateTime.UtcNow.AddHours(7):yyyy-mm-dd-hh-mm-ss}.xlsx";

            var fullExportPath = Path.Combine(exportPath, exportFileName);

            CreateExcelFile.CreateExcelDocument(headers, pageViewList, fullExportPath);
            var result = Request.CreateResponse(HttpStatusCode.OK);
            var stream = new FileStream(fullExportPath, FileMode.Open);
            result.Content = new StreamContent(stream);
            result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
            result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
            {
                FileName = Path.GetFileName(fullExportPath)
            };

            return result;
        }

        [HttpPost]
        [Route("track-page-view")]
        public IHttpActionResult AddTrackPageView([FromBody]PageViewTrackingDTO command)
        {
            if (command == null)
                return Ok("true");

            command = PopulateRequestInfo<PageViewTrackingDTO>(command);
            var insertSql = @"INSERT INTO [dbo].[PageViewTracking]
                               ([PageTitle]
                               ,[PageUrl]
                               ,[AccessDateTime]
                               ,[ClientIpAddress]
                               ,[ClientInfo]
                               ,[UserName]
                               ,[UserId])
                            VALUES
                               (@PageTitle, @PageUrl, @AccessDateTime, @ClientIpAddress, @ClientInfo, @UserName, @UserId)";

            using (var db = new SqlConnection(CalcheeseAppDbContextConnectionString))
            {
                try
                {
                    db.Open();
                    db.Execute(insertSql, new
                    {
                        PageTitle = new DbString {Value = command.PageTitle, IsAnsi = false, IsFixedLength = false},
                        PageUrl = new DbString { Value = command.PageUrl, IsAnsi = false, IsFixedLength = false },
                        AccessDateTime = DateTime.UtcNow,
                        ClientIpAddress = new DbString { Value = command.ClientIpAddress, IsAnsi = false, IsFixedLength = false },
                        ClientInfo = new DbString { Value = command.ClientInfo, IsAnsi = false, IsFixedLength = false },
                        UserName = new DbString { Value = command.UserName, IsAnsi = false, IsFixedLength = false },
                        UserId = new DbString { Value = command.UserId, IsAnsi = false, IsFixedLength = false }
                    });
                }
                catch
                {
                    // ignored
                }
            }

            return Ok("true");
        }
    }

    public class PageViewReportQuery : KeywordSearchCommand
    {
        public string PageTitle { get; set; }

        public string LastAccessType { get; set; } // 1-month, 1-week, 24-hours

        public DateTime LastAccessDateTime { get; set; }
    }
}
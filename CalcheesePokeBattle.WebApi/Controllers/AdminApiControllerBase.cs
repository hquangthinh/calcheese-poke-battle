﻿using System.Web;
using CalcheesePokeBattle.Service;
using CalcheesePokeBattle.WebApi.Attributes;

namespace CalcheesePokeBattle.WebApi.Controllers
{
    [ApiAuthorization(Roles = "Administrator,LuckyDrawManager")]
    public abstract class AdminApiControllerBase: ApiControllerBase
    {
        protected AdminApiControllerBase(IServiceFactory serviceFactory) : base(serviceFactory)
        {
        }
    }
}
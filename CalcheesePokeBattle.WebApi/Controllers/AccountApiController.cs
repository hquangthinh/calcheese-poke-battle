﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.ModelBinding;
using CalcheesePokeBattle.Authentication;
using CalcheesePokeBattle.Authentication.Models;
using CalcheesePokeBattle.Common.Monads;
using CalcheesePokeBattle.DTO;
using CalcheesePokeBattle.Service;
using CalcheesePokeBattle.WebApi.Attributes;
using CalcheesePokeBattle.WebApi.Model;
using CalcheesePokeBattle.WebApi.Utils;

namespace CalcheesePokeBattle.WebApi.Controllers
{
    /// <summary>
    /// api for normal users to access their profile
    /// </summary>
    [ApiAuthorization]
    [RoutePrefix("api/account")]
    public class AccountApiController : ApiControllerBase
    {
        private ApplicationUserManager UserManager { get; }
        private IAuthenticator Authenticator => ServiceFactory.GetService<IAuthenticator>();

        public AccountApiController(IServiceFactory serviceFactory, ApplicationUserManager userManager) : base(serviceFactory)
        {
            UserManager = userManager;
        }

        [HttpPost]
        [Route("validate-captcha")]
        [ResponseType(typeof(ResponseDTO))]
        [AllowAnonymous]
        public async Task<IHttpActionResult> ValidateCaptcha([FromBody]ValidateCaptchaCommand command)
        {
            command.RemoteIp = GetClientIp(Request);
            return await RespondAsync(() => ServiceFactory.GetService<IAccountService>().ValidateCaptcha(command));
        }

        [HttpPost]
        [Route("validate-playername")]
        [ResponseType(typeof(ResponseDTO))]
        [AllowAnonymous]
        public IHttpActionResult ValidatePlayerName([FromBody]UpdateUserCommand command)
        {
            return RespondWith(() => ServiceFactory.GetService<IAccountService>().ValidatePlayerName(command));
        }

        [HttpPost]
        [Route("validate-phonenumber")]
        [ResponseType(typeof(ResponseDTO))]
        [AllowAnonymous]
        public IHttpActionResult ValidatePhoneNumber([FromBody]UpdateUserCommand command)
        {
            return RespondWith(() => ServiceFactory.GetService<IAccountService>().ValidatePhoneNumber(command));
        }

        [HttpPost]
        [Route("register")]
        [AllowAnonymous]
        public IHttpActionResult RegisterUser([FromBody]UpdateUserCommand command)
        {
            return RespondWith(() => ServiceFactory.GetService<IAccountService>().RegisterPlayer(command)
                .OnFailureRaiseError(msg => new ApplicationException(msg))
                .Value);
        }

        // Search user profiles
        [HttpGet]
        [Route("")]
        [ResponseType(typeof(PagingResult<UserProfileDTO>))]
        public IHttpActionResult All([ModelBinder] SearchUserCommand command)
        {
            return RespondWith(() => ServiceFactory.GetService<IUserProfileService>().SearchUserProfiles(command));
        }

        // Get user profile detail
        [HttpGet]
        [Route("{UserId}")]
        [ResponseType(typeof(UserProfileDTO))]
        public IHttpActionResult Detail([ModelBinder] UserCommandBase command)
        {
            if ("current".Equals(command.UserId, StringComparison.OrdinalIgnoreCase))
            {
                return RespondWith(() => CurrentProfile);
            }

            if ("new".Equals(command.UserId, StringComparison.OrdinalIgnoreCase))
            {
                return RespondWith(() => new UserProfileDTO
                {
                    UserRoles = new List<RoleDTO>()
                });
            }

            return RespondWith(() => 
                        ServiceFactory.GetService<IUserProfileService>()
                            .GetUserProfileDetail(command)
                            .OnFailureRaiseError(msg => new ApplicationException(msg))
                            .Value);
        }

        // Create or Update user profile
        [HttpPost]
        [Route("")]
        [ResponseType(typeof(UserProfileDTO))]
        public IHttpActionResult Update([FromBody] UpdateUserCommand command)
        {
            return RespondWith(() => 
                        ServiceFactory.GetService<IUserProfileService>().Update(command)
                            .OnFailureRaiseError(msg => new ApplicationException(msg))
                            .Value);
        }

        /// <summary>
        /// Only perform update main details for profile do not create new profile
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("profile")]
        [ResponseType(typeof(UserProfileDTO))]
        public IHttpActionResult UpdateProfileMainDetails([FromBody] UpdateUserCommand command)
        {
            return RespondWith(() =>
                        ServiceFactory.GetService<IUserProfileService>().UpdateProfileMainDetails(command)
                            .OnFailureRaiseError(msg => new ApplicationException(msg))
                            .Value);
        }

        [HttpPost]
        [Route("password")]
        [ResponseType(typeof(ResponseDTO))]
        public async Task<IHttpActionResult> ChangePassword([FromBody] ChangePasswordCommand command)
        {
            if (command == null || CurrentProfile == null)
                return Ok(ResponseDTO.ErrorResult("000", "Yêu cầu không hợp lệ"));

            if(string.IsNullOrEmpty(command.Password) || string.IsNullOrEmpty(command.PasswordConfirmation))
                return Ok(ResponseDTO.ErrorResult("001", "Yêu cầu không hợp lệ"));

            if(!command.Password.Equals(command.PasswordConfirmation))
                return Ok(ResponseDTO.ErrorResult("002", "Yêu cầu không hợp lệ"));

            var loginResult = await DoLogin(new LoginViewModel
            {
                UserName = CurrentProfile.UserName,
                Password = command.CurrentPassword,
                RememberMe = false
            });

            if(loginResult == LoginStatus.Unauthorized)
                return Ok(ResponseDTO.ErrorResult("003", "Yêu cầu không hợp lệ"));

            await Authenticator.ChangePasswordAsync(CurrentProfile.UserName, command.Password);

            return Ok(ResponseDTO.SuccessResult());
        }

        private async Task<LoginStatus> DoLogin(LoginViewModel model)
        {
            var user = await UserManager.FindAsync(model.UserName, model.Password);
            if (user != null && !user.LockoutEnabled)
            {
                if (await UserManager.HasApplicationLoginAsync(user.Id))
                {
                    await Authenticator.SignInAsync(user, model.RememberMe);
                    return LoginStatus.Success;
                }
            }
            return LoginStatus.Unauthorized;
        }

        [HttpPost]
        [Route("avatar")]
        [ResponseType(typeof(ResponseDTO))]
        public IHttpActionResult UpdatePlayerAvatar([FromBody] AvatarUploadCommand command)
        {
            var base64Data = command.Base64ImageData.Split(',');
            var data = Convert.FromBase64String(base64Data[1]);
            using (var blobClient = new AzureBlobClient {ContainerName = "avatars"})
            {
                var fileName = $"{CurrentProfile.UserName}_avatar.png";
                blobClient.InitStorage();
                var blobInfo = blobClient.UploadFile(data, fileName);
                ServiceFactory.GetService<IUserProfileService>().UpdateProfileAvatarUrl(blobInfo, CurrentProfile);
            }
            return Ok("ok");
        }
    }

    public class AvatarUploadCommand
    {
        public string Base64ImageData { get; set; }
    }
}
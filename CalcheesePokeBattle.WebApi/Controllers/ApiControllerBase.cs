﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Results;
using CalcheesePokeBattle.Common.Extensions;
using CalcheesePokeBattle.DTO;
using CalcheesePokeBattle.Service;
using CalcheesePokeBattle.WebApi.Filters;
using Castle.Core.Logging;
using System.Collections.Specialized;
using System.Configuration;
using System.IO;
using CalcheesePokeBattle.Common.Utils;
using Microsoft.ApplicationInsights;

namespace CalcheesePokeBattle.WebApi.Controllers
{
    [CustomExceptionFilter]
    public abstract class ApiControllerBase : ApiController
    {
        public ILogger Logger { get; set; }

        public IServiceFactory ServiceFactory { get; }

        protected string CurrentUserName => User.Identity.Name;

        protected string CalcheeseAppDbContextConnectionString = ConfigurationManager
            .ConnectionStrings["CalcheeseAppDbContext"]
            .ConnectionString;

        private TelemetryClient telemetryClient = new TelemetryClient();

        private UserProfileDTO _currentProfile;
        protected UserProfileDTO CurrentProfile
        {
            private set { _currentProfile = value; }
            get
            {
                if (_currentProfile == null) throw new ApplicationException($"Current User {CurrentUserName} is invalid.");
                return _currentProfile;
            }
        }

        protected ApiControllerBase(IServiceFactory serviceFactory)
        {
            ServiceFactory = serviceFactory;
            Logger = serviceFactory.GetService<ILogger>();

            if (!string.IsNullOrEmpty(CurrentUserName)) CheckCurrentUserSession();
        }

        protected string GetPhysicalExportPath()
        {
            var tempExportPath = HttpContext.Current.Server.MapPath("~/App_Data");
            return tempExportPath;
        }

        protected IHttpActionResult RespondWith(Action action, params Permission[] permissions)
        {
            var result = RespondWith(
                () =>
                {
                    action();
                    return true;
                },
                permissions);

            if (result is OkNegotiatedContentResult<bool>) return Ok();
            return result;
        }

        protected IHttpActionResult RespondWith<T>(Func<T> action, params Permission[] permissions)
        {
            if (CurrentUserHasPermissions(permissions))
            {
                try
                {
                    var result = action();
                    return Ok(result);
                }
                catch (Exception ex)
                {
                    telemetryClient.TrackException(ex);
                    throw;
                }
            }

            return Unauthorized();
        }

        protected async Task<IHttpActionResult> RespondAsync<T>(Func<Task<T>> action, params Permission[] permissions)
        {
            if (CurrentUserHasPermissions(permissions))
            {
                try { return Ok(await action()); }
                catch (Exception e) { e.ReThrow(); }
            }

            return Unauthorized();
        }

        protected T PopulateRequestInfo<T>(ITrackableDTO dto) where T : class, ITrackableDTO
        {
            dto.ClientIpAddress = GetClientIp(Request);
            dto.ClientInfo = GetClientInfo(Request);
            return dto as T;
        }

        protected string GetClientIp(HttpRequestMessage request)
        {

            if (request.Properties.ContainsKey("MS_HttpContext"))
            {
                return ((HttpContextWrapper)request.Properties["MS_HttpContext"]).Request.UserHostAddress;
            }

            if (HttpContext.Current != null)
            {
                return HttpContext.Current.Request.UserHostAddress;
            }

            return null;
        }

        protected string GetClientInfo(HttpRequestMessage request)
        {
            return request.Headers.UserAgent.ToString();
        }

        private void CheckCurrentUserSession()
        {
            try
            {
                CurrentProfile =
                    ServiceFactory.GetService<IUserProfileService>()
                        .GetUserProfileDetail(UserCommandBase.FromUserName(CurrentUserName)).Value;
            }
            catch (ArgumentException e)
            {
                var message = $"Error when loading user {CurrentUserName}";
                Logger.Error(message, e);
            }
        }

        private bool CurrentUserHasPermissions(Permission[] permissions)
        {
            return true;
        }

        protected async Task<List<ResponseDTO>> HandleFileUpload(Action<NameValueCollection, MultipartFileData, List<ResponseDTO>> handleFile)
        {
            if (!Request.Content.IsMimeMultipartContent())
                throw new ArgumentException("Must submit with enctype as multipart/form-data");

            var tempUploadPath = HttpContext.Current.Server.MapPath("~/Content/Upload");

            if (!Directory.Exists(tempUploadPath))
            {
                Directory.CreateDirectory(tempUploadPath);
            }

            var provider = new CustomMultipartFormDataStreamProvider(tempUploadPath);
            await Request.Content.ReadAsMultipartAsync(provider);

            var response = new List<ResponseDTO>();

            foreach (var file in provider.FileData)
            {
                handleFile(provider.FormData, file, response);
            }

            return response;
        }

        public async Task<IHttpActionResult> HandleFileUploadApi(Action<NameValueCollection, MultipartFileData, List<ResponseDTO>> handleFile)
        {
            return Ok(await HandleFileUpload(handleFile));
        }

    }
}
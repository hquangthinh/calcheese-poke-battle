﻿using System.Web.Http;
using System.Web.Http.Description;
using CalcheesePokeBattle.Common.Monads;
using CalcheesePokeBattle.DTO;
using CalcheesePokeBattle.Service;

namespace CalcheesePokeBattle.WebApi.Controllers
{
    [Authorize]
    [RoutePrefix("api/coupon")]
    public class CouponApiController : ApiControllerBase
    {
        public CouponApiController(IServiceFactory serviceFactory) : base(serviceFactory)
        {
        }

        [HttpPost]
        [Route("redeem-code")]
        [ResponseType(typeof(CouponCodeResult))]
        public IHttpActionResult RedeemCodeForPoint([FromBody] CouponCodeDTO codeDto)
        {
            codeDto = PopulateRequestInfo<CouponCodeDTO>(codeDto);
            return
                RespondWith(
                    () =>
                    {
                        var codeResult =
                            ServiceFactory.GetService<ICouponCodeService>().RedeemCodeForPoint(codeDto, CurrentProfile);
                        if (codeResult.IsFailure)
                            return new CouponCodeResult {Success = false};
                        return codeResult.Map(code =>
                            new CouponCodeResult
                            {
                                Success = true,
                                Point = code.Point,
                                CardImageUrl = code.CardImageUrl
                            }).Value;
                    });
        }
    }
}
﻿using System;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.ModelBinding;
using CalcheesePokeBattle.Common.Monads;
using CalcheesePokeBattle.Service;
using CalcheesePokeBattle.ViewModel;
using CalcheesePokeBattle.WebApi.Attributes;

namespace CalcheesePokeBattle.WebApi.Controllers
{
    [ApiAuthorization]
    [RoutePrefix("api/player")]
    public class PlayerProfileApiController : ApiControllerBase
    {
        public PlayerProfileApiController(IServiceFactory serviceFactory) : base(serviceFactory)
        {
        }

        [HttpGet]
        [Route("{UserId}")]
        [ResponseType(typeof(UserProfileViewModel))]
        public IHttpActionResult GetUserProfileViewModel([ModelBinder] UserCommandBase command)
        {
            return RespondWith(() =>
                        ServiceFactory.GetService<IUserProfileService>()
                            .GetUserProfileViewModel(command)
                            .OnFailureRaiseError(msg => new ApplicationException(msg))
                            .Value);
        }

        [HttpGet]
        [Route("{UserId}/inventory")]
        [ResponseType(typeof(UserInventoryViewModel))]
        public IHttpActionResult GetPlayerInventoryViewModel([ModelBinder] UserCommandBase command)
        {
            return RespondWith(() =>
                        ServiceFactory.GetService<IUserProfileService>()
                            .GetPlayerInventoryViewModel(command)
                            .OnFailureRaiseError(msg => new ApplicationException(msg))
                            .Value);
        }
    }
}
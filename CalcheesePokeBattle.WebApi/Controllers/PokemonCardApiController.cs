﻿using System.Web.Http;
using System.Web.Http.Description;
using CalcheesePokeBattle.Service;
using CalcheesePokeBattle.ViewModel;
using CalcheesePokeBattle.Common.Monads;
using System.Web.Http.ModelBinding;
using System.Collections.Generic;

namespace CalcheesePokeBattle.WebApi.Controllers
{
    /// <summary>
    /// Use for public page - anonymous user
    /// </summary>
    [RoutePrefix("api/pokemon-card")]
    public class PokemonCardApiController : ApiControllerBase
    {
        public PokemonCardApiController(IServiceFactory serviceFactory) : base(serviceFactory)
        {
        }

        [HttpGet]
        [Route("")]
        [ResponseType(typeof(PagingResult<PokeCardViewModel>))]
        public IHttpActionResult All([ModelBinder] SearchPokeCardCommand command)
        {
            return RespondWith(() => ServiceFactory.GetService<IPokeCardService>().SearchPokeCards(command));
        }

       
    }
}
﻿using System;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.ModelBinding;
using CalcheesePokeBattle.Common.Monads;
using CalcheesePokeBattle.DTO;
using CalcheesePokeBattle.Service;
using CalcheesePokeBattle.ViewModel;

namespace CalcheesePokeBattle.WebApi.Controllers
{
    [RoutePrefix("api/poke-card-game-hub")]
    [Authorize]
    public class PokeCardGameHubApiController : ApiControllerBase
    {
        public PokeCardGameHubApiController(IServiceFactory serviceFactory) : base(serviceFactory)
        {
        }

        [HttpGet]
        [Route("online-palyers")]
        [ResponseType(typeof(PagingResult<OnlinePlayerDTO>))]
        public IHttpActionResult GetAllOnlinePlayers([ModelBinder]ServerInfoCommand command)
        {
            return RespondWith(() 
                    => ServiceFactory.GetService<IPokeCardGameServerService>().GetAllOnlinePlayers(command, CurrentProfile)
                        .OnFailureRaiseError(msg => new ApplicationException(msg))
                        .Value);
        }

        [HttpPost]
        [Route("can-play-game")]
        [ResponseType(typeof(ResponseDTO<string>))]
        public IHttpActionResult CheckIfUserCanJoinGame()
        {
            return RespondWith(()
                    => ServiceFactory.GetService<IPokeCardGameLogicService>().CheckIfUserCanSendNewGameInvitation(CurrentProfile.Id)
                        .OnFailure(msg => ResponseDTO<string>.SuccessResult(msg))
                        .Value);
        }

        [HttpGet]
        [Route("game-play-info")]
        [ResponseType(typeof(GamePlayingPageViewModel))]
        public IHttpActionResult GetGamePlayInfoFromGameSession([ModelBinder] GameSessionQuery query)
        {
            return RespondWith(()
                    => ServiceFactory.GetService<IPokeCardGameServerService>().GetGamePlayInfoFromGameSession(query, CurrentProfile)
                        .OnFailureRaiseError(msg => new ApplicationException(msg))
                        .Value);
        }

        [HttpGet]
        [Route("stop-game")]
        [ResponseType(typeof(ResponseDTO))]
        public IHttpActionResult StopGameRequest([ModelBinder] GameSessionQuery query)
        {
            //TODO: this should be POST
            return RespondWith(()
                => ServiceFactory.GetService<IPokeCardGameServerService>().StopGameRequest(query)
                    .OnFailure(msg => ResponseDTO.ErrorResult("000", msg))
                    .Value);
        }

        [HttpPost]
        [Route("disconnect-game")]
        [ResponseType(typeof(ResponseDTO))]
        public IHttpActionResult DisconnectGame([FromBody] GameSessionQuery query)
        {
            return RespondWith(()
                => ServiceFactory.GetService<IPokeCardGameServerService>().DisconnectGame(query)
                    .OnFailure(msg => ResponseDTO.ErrorResult("000", msg))
                    .OnSuccess(() => ResponseDTO.SuccessResult()));
        }

        /// <summary>
        /// This endpoint for testing the game
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("lavigne-logan")]
        [ResponseType(typeof(ResponseDTO))]
        public IHttpActionResult ResumeGameSession([ModelBinder] ResumeGameSessionQuery query)
        {
            if (query == null || !"3BF650A1-4BE3-46B7-BD17-54B032584CD2".Equals(query.XKey))
                return Ok("ko");

            var gameSessionInfo = new GameSessionInfo
            {
                Id = query.GameSessionId,
                PlayerUserId1 = query.PlayerUserId1,
                PlayerUserName1 = query.PlayerUserName1,
                PlayerUserId2 = query.PlayerUserId2,
                PlayerUserName2 = query.PlayerUserName2
            };
            return RespondWith(()
                    => ServiceFactory.GetService<IGameSessionService>().StartNewGameSession(gameSessionInfo)
                        .OnFailure(msg => ResponseDTO.ErrorResult("999", $"Cannot resume game session {query.GameSessionId}"))
                        .OnSuccess(() => ResponseDTO.SuccessResult()));
        }

        [HttpGet]
        [Route("lavigne-logan-berry")]
        [ResponseType(typeof(ResponseDTO))]
        public IHttpActionResult GetAllGameSessions([ModelBinder] ResumeGameSessionQuery query)
        {
            if (query == null || !"3BF650A1-4BE3-46B7-BD17-54B032584CD2".Equals(query.XKey))
                return Ok("ko");

            return RespondWith(()
                => ServiceFactory.GetService<IGameSessionService>().GetAllGameSessionInfos());
        }
    }
}
﻿using System.Threading.Tasks;
using System.Web.Http;
using CalcheesePokeBattle.Authentication;
using CalcheesePokeBattle.Authentication.Models;
using CalcheesePokeBattle.Service;
using CalcheesePokeBattle.WebApi.Attributes;
using CalcheesePokeBattle.WebApi.Model;

namespace CalcheesePokeBattle.WebApi.Controllers
{
    [ApiAuthorization]
    [RoutePrefix("api/auth")]
    public class AuthApiController : ApiControllerBase
    {
        private ApplicationUserManager UserManager { get; }
        private IAuthenticator Authenticator => ServiceFactory.GetService<IAuthenticator>();

        public AuthApiController(IServiceFactory serviceFactory, ApplicationUserManager userManager) : base(serviceFactory)
        {
            UserManager = userManager;
        }

        [HttpPost]
        [Route("login")]
        [AllowAnonymous]
        public async Task<IHttpActionResult> Login(LoginViewModel model)
        {
            var result = await DoLogin(model);
            return Ok(result == LoginStatus.Success || result == LoginStatus.Redirect ? "ok" : "Invalid username or password");
        }

        private async Task<LoginStatus> DoLogin(LoginViewModel model)
        {
            var user = await UserManager.FindAsync(model.UserName, model.Password);
            if (user != null && !user.LockoutEnabled)
            {
                if (await UserManager.HasApplicationLoginAsync(user.Id))
                {
                    await Authenticator.SignInAsync(user, model.RememberMe);
                    return LoginStatus.Redirect;
                }
            }
            return LoginStatus.Unauthorized;
        }

        [HttpDelete]
        [Route("logoff")]
        public IHttpActionResult Logoff()
        {
            Authenticator.SignOut();
            return Ok();
        }

        [HttpPost]
        [Route("forgot-password")]
        [AllowAnonymous]
        public IHttpActionResult ForgotPasswordRequest(ForgotPasswordRequestCommand command)
        {
            ServiceFactory.GetService<IAccountService>().ForgotPasswordRequest(command);
            return Ok();
        }
    }
}
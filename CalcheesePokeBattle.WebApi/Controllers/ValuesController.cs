﻿using System;
using System.Collections.Generic;
using System.Web.Http;
using CalcheesePokeBattle.Dependency;
using CalcheesePokeBattle.DTO;
using CalcheesePokeBattle.Model;
using CalcheesePokeBattle.Service;
using Swashbuckle.Swagger.Annotations;

namespace CalcheesePokeBattle.WebApi.Controllers
{
    [RoutePrefix("api/values")]
    public class ValuesController : ApiController
    {
        [Route("")]
        [SwaggerOperation("GetAll")]
        public IEnumerable<string> Get()
        {
            return new [] { "value1", "value2" };
        }

        [Route("time-info")]
        public IHttpActionResult GetServerDateTimeNowInfo()
        {
            return Ok(ServerDateTimeInfo.Now());
        }
    }

    public class ServerDateTimeInfo
    {
        public DateTime LocalNow { get; set; }
        public DateTime Today { get; set; }
        public DateTime UctNow { get; set; }
        public DateTime VNNow { get; set; }
        public TimeZoneInfo LocalTimeInfoZone { get; set; }
        public TimeZoneInfo UtcTimeInfoZone { get; set; }
        public WeekDefinitionDTO CurrentWeekAsSystemDef { get; set; }
        public WeekDefinitionDTO WeekInfoForInputDate { get; set; }
        public string DatabaseName { get; set; }
        public string Build { get; set; }
        public string Hash { get; set; }

        public static ServerDateTimeInfo Now()
        {
            return new ServerDateTimeInfo
            {
                LocalNow = DateTime.Now,
                Today = DateTime.Today,
                UctNow = DateTime.UtcNow,
                VNNow = DateTime.Now.AddHours(7),
                LocalTimeInfoZone = TimeZoneInfo.Local,
                UtcTimeInfoZone = TimeZoneInfo.Utc,
                CurrentWeekAsSystemDef = IocManager.Instance.GetService<ISettingService>()
                    .GetWeekFromCurrentDate()
                    .Value,
                WeekInfoForInputDate = IocManager.Instance.GetService<ISettingService>()
                    .GetWeekFromDate(new DateTime(2017, 5, 1, 6, 0, 0))
                    .Value,
                DatabaseName = new CalcheeseAppDbContext().Database.Connection.Database,
                Build = "2017-06-12-1002am",
                Hash = "e3b9152e672f3172e698b98db304fedcf85ec2c4" //last git commit hash
            };
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.ModelBinding;
using CalcheesePokeBattle.DTO;
using CalcheesePokeBattle.Service;
using System.Threading.Tasks;
using CalcheesePokeBattle.WebApi.Utils;

namespace CalcheesePokeBattle.WebApi.Controllers.Admin
{
    [RoutePrefix("api/CouponUpload")]
    public class CouponUploadApiController : AdminApiControllerBase
    {
        public CouponUploadApiController(IServiceFactory serviceFactory) : base(serviceFactory)
        {
        }

        [HttpGet]
        [Route("")]
        public IHttpActionResult Detail([ModelBinder] GiftItemCommandBase command)
        {
            return RespondWith(() => new UploadFileDTO());
        }

        [HttpPost]
        [Route("upload")]
        [ResponseType(typeof(ResponseDTO))]
        public async Task<IHttpActionResult> Upload()
        {

            return await RespondAsync(() =>
            {
                return HandleFileUpload((form, file, response) =>
                {
                    //Handle update storage cloud
                    var dto = new UploadFileDTO();
                    var helper = new CouponDataHelper();
                    helper.InitStorage();
                    dto.FileName = helper.UploadFileForImport(file.LocalFileName);
                    dto.PokeCardId = Convert.ToInt32(form["PokeCardId"]);
                    ServiceFactory.GetService<IUploadFileService>().Update(dto);
                });
            }, (Permission)1);
        }

        [HttpGet]
        [Route("all-upload-info")]
        [ResponseType(typeof(List<UploadFileDTO>))]
        public IHttpActionResult GetAllUploadInfo()
        {
            return RespondWith(() => ServiceFactory.GetService<IUploadFileService>().GetAllUploadInfo());
        }

        [HttpPost]
        [Route("trigger-queue")]
        [ResponseType(typeof(ResponseDTO))]
        public IHttpActionResult TriggerQueue([FromBody]UploadFileDTO fileInfoDto)
        {
            return RespondWith(() =>
            {
                var currentUserWithRole =
                    ServiceFactory.GetService<IUserProfileService>()
                        .GetUserProfileDetail(UserCommandBase.FromUserName(CurrentUserName))
                        .Value;
                var helper = new CouponDataHelper();
                helper.TriggerQueueMessageForImportCouponCode(currentUserWithRole, GetClientIp(Request));
                return "ok";
            });
        }

        [HttpPost]
        [Route("delete-file")]
        [ResponseType(typeof(ResponseDTO))]
        public IHttpActionResult DeleteFileInfo([FromBody]UploadFileDTO fileInfoDto)
        {
            return RespondWith(() => ServiceFactory.GetService<IUploadFileService>().DeleteFile(fileInfoDto));
        }
    }
}

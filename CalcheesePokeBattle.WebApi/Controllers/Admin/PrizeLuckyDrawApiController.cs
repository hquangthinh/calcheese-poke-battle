﻿using CalcheesePokeBattle.Common.Monads;
using CalcheesePokeBattle.DTO;
using CalcheesePokeBattle.Service;
using CalcheesePokeBattle.ViewModel;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.ModelBinding;
using CalcheesePokeBattle.WebApi.Utils;
using Dapper;

namespace CalcheesePokeBattle.WebApi.Controllers.Admin
{
    [RoutePrefix("api/PrizeLuckyDraw")]
    public class PrizeLuckyDrawApiController : AdminApiControllerBase
    {
        public PrizeLuckyDrawApiController(IServiceFactory serviceFactory) : base(serviceFactory)
        {
        }

        // Get list of user coupon code at current week
        [HttpGet]
        [Route("")]
        [ResponseType(typeof(PagingResult<UserLuckyDrawCodeViewModel>))]
        public IHttpActionResult GetCurrentUserLuckyDrawCode([ModelBinder] SearchUserLuckyDrawCodeCommand command)
        {
            return RespondWith(() => ServiceFactory.GetService<IUserLuckyDrawCodeService>().SearchUserLuckyDrawCode(command));
        }

        [HttpGet]
        [Route("GetPrizeHistory")]
        [ResponseType(typeof(PagingResult<PrizeLuckyDrawHistoryViewModel>))]
        public IHttpActionResult GetPrizeHistory([ModelBinder] SearchPrizeLuckyDrawHistoryCommand command)
        {
            return RespondWith(() => ServiceFactory.GetService<IPrizeLuckyDrawHistoryService>().SearchPrizeLuckyDrawHistory(command));
        }

        [HttpGet]
        [Route("GetWinnerUserList")]
        [ResponseType(typeof(IEnumerable<WinnerListUserDTO>))]
        public IHttpActionResult GetWinnerUserList([ModelBinder] SearchPrizeLuckyDrawHistoryCommand command)
        {
            return RespondWith(() => ServiceFactory.GetService<IWinnerListService>().GetWinnerUserList(command));
        }

        [HttpPost]
        [Route("SetPrize")]
        [ResponseType(typeof(Result<WinnerListUserDTO>))]
        public IHttpActionResult SetPrize([FromBody] SetPrizeCommand command)
        {
            return RespondWith(() => ServiceFactory.GetService<IPrizeLuckyDrawHistoryService>().SetPrize(command)
                .OnFailureRaiseError(msg => new ApplicationException(msg))
                .Value);
        }


        [HttpGet]
        [Route("GetCurrentWeekData")]
        [ResponseType(typeof(CurrentWeekPrizeDataDTO))]
        public IHttpActionResult GetCurrentWeekData()
        {
            return RespondWith(() => ServiceFactory.GetService<IUserLuckyDrawCodeService>().GetCurrentWeekData());
        }


        [HttpPost]
        [Route("set-show-prize-result")]
        public IHttpActionResult SetShowPrizeResultToFrontSite([FromBody]SetPrizeCommand command)
        {
            return
                RespondWith(
                    () =>
                        ServiceFactory.GetService<ISettingService>()
                            .UpdateShowLuckyDrawPrizeOnFrontSiteForWeek(command.WeekNumber)
                            .IsSuccess);
        }

        [HttpPost]
        [Route("confirm-prize")]
        public IHttpActionResult ConfirmPrize([FromBody]ConfirmPrizeCommand command)
        {
            return
                RespondWith(
                    () => ServiceFactory.GetService<IPrizeLuckyDrawHistoryService>().ConfirmPrize(command).IsSuccess);
        }

        [HttpPost]
        [Route("reset-prize")]
        public IHttpActionResult ResetPrize([FromBody]ConfirmPrizeCommand command)
        {
            command = PopulateRequestInfo<ConfirmPrizeCommand>(command);
            command.UserName = CurrentUserName;
            return
                RespondWith(
                    () => ServiceFactory.GetService<IPrizeLuckyDrawHistoryService>().ResetPrize(command).IsSuccess);
        }

        /// <summary>
        /// Export data, store to azure storage, return blob url
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("export-result-excel")]
        public HttpResponseMessage ExportResultToExcel([ModelBinder]ExportResultCommand command)
        {
            return ExportAndDownloadFile(command);
        }

        private Result<string> ExportResultToExcelInternal(ExportResultCommand command)
        {
            if (command == null)
                return Result.Success("");

            var exportPath = GetPhysicalExportPath();

            var candidateListHeaders = new List<string>
            {
                "Tuần",
                "Tên Đăng Nhập",
                "Tên",
                "Họ",
                "Địa Chỉ",
                "Điện Thoại",
                "Mã May Mắn",
                "Ngày Nhập"
            };

            var winnerListHeaders = new List<string>
            {
                "Tên Đăng Nhập",
                "Tên",
                "Họ",
                "Địa Chỉ",
                "Điện Thoại",
                "Mã May Mắn",
                "Gải",
                "Trạng Thái"
            };

            // query data
            var connString = ConfigurationManager.ConnectionStrings["CalcheeseAppDbContext"].ConnectionString;
            var sqlGetCandidateDrawList =
                @"select WeekNumber, UserName, FirstName, LastName, Address, PhoneNumber, LuckyDrawCode, DATEADD(hour, 7, CreatedDate) as CreatedDate
                            from vwAllUserLuckyDrawCode
                                where WeekNumber = @WeekNumber
                        order by id ";

            var sqlGetWinnerList = @"select wu.UserName, wu.FirstName, wu.LastName, wu.Address, wu.PhoneNumber, wu.LuckyDrawCode, wl.PriceDescription as PrizeDescription, wu.Status
	                                    from WinnerList wl inner join
		                                    WinnerListUser wu on wl.Id = wu.WinnerListId
			                                    where wl.WeekNumber = @WeekNumber
                                                order by wl.GiftItemID";

            using (var db = new SqlConnection(connString))
            {
                db.Open();
                if ("winner".Equals(command.Type))
                {
                    var exportFileName =
                        $"danh-sach-thang-giai-tuan-{command.WeekNumber}-exported-{DateTime.UtcNow.AddHours(7):yyyy-mm-dd-hh-mm-ss}.xlsx";

                    var fullExportPath = Path.Combine(exportPath, exportFileName);

                    var winnerListWithUserInfo =
                        db.Query<WinnerListWithUserInfo>(sqlGetWinnerList, new
                            {
                                WeekNumber = command.WeekNumber
                            })
                            .ToList();
                    CreateExcelFile.CreateExcelDocument(winnerListHeaders, winnerListWithUserInfo, fullExportPath);

                    return Result.Success(fullExportPath);
                }
                else
                {
                    var exportFileName =
                        $"danh-sach-du-thuong-tuan-{command.WeekNumber}-exported-{DateTime.UtcNow.AddHours(7):yyyy-mm-dd-hh-mm-ss}.xlsx";

                    var fullExportPath = Path.Combine(exportPath, exportFileName);

                    var luckyDrawCandidateListWithUserInfo =
                        db.Query<LuckyDrawCodeListWithUserInfo>(sqlGetCandidateDrawList, new
                            {
                                WeekNumber = command.WeekNumber
                            })
                            .ToList();

                    CreateExcelFile.CreateExcelDocument(candidateListHeaders, luckyDrawCandidateListWithUserInfo, fullExportPath);

                    return Result.Success(fullExportPath);
                }
            }
        }

        private HttpResponseMessage ExportAndDownloadFile(ExportResultCommand command)
        {
            var fullExportPath = ExportResultToExcelInternal(command).Value;
            var result = Request.CreateResponse(HttpStatusCode.OK);

            var stream = new FileStream(fullExportPath, FileMode.Open);
            result.Content = new StreamContent(stream);
            result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
            result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
            {
                FileName = Path.GetFileName(fullExportPath)
            };

            return result;
        }
    }

    internal class LuckyDrawCodeListWithUserInfo
    {
        public int WeekNumber { get; set; }

        public string UserName { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Address { get; set; }

        public string PhoneNumber { get; set; }

        public string LuckyDrawCode { get; set; }

        public DateTime CreatedDate { get; set; }
    }

    internal class WinnerListWithUserInfo
    {
        public string UserName { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Address { get; set; }

        public string PhoneNumber { get; set; }

        public string LuckyDrawCode { get; set; }

        public string PrizeDescription { get; set; }

        public string Status { get; set; }
    }
}

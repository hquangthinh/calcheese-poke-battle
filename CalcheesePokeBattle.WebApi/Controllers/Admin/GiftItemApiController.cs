﻿using CalcheesePokeBattle.Common.Monads;
using CalcheesePokeBattle.DTO;
using CalcheesePokeBattle.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.ModelBinding;

namespace CalcheesePokeBattle.WebApi.Controllers.Admin
{
    [RoutePrefix("api/giftItem")]
    public class GiftItemApiController : AdminApiControllerBase
    {
        public GiftItemApiController(IServiceFactory serviceFactory) : base(serviceFactory)
        {
        }
        // Search user  GifItem
        [HttpGet]
        [Route("")]
        [ResponseType(typeof(PagingResult<GiftItemDTO>))]
        public IHttpActionResult All([ModelBinder] SearchGiftItemCommand command)
        {
            return RespondWith(() => ServiceFactory.GetService<IGiftItemService>().SearchGiftItems(command));
        }

        // Create or Update gift item
        [HttpPost]
        [Route("")]
        [ResponseType(typeof(GiftItemDTO))]
        public IHttpActionResult Update([FromBody] UpdateGiftItemCommand command)
        {
            return RespondWith(() =>
                        ServiceFactory.GetService<IGiftItemService>().Update(command)
                            .OnFailureRaiseError(msg => new ApplicationException(msg))
                            .Value);
        }

        // Delete gift item
        [HttpDelete]
        [Route("")]
        [ResponseType(typeof(ResponseDTO))]
        public IHttpActionResult Destroy([FromUri] GiftItemCommandBase command)
        {
            return RespondWith(() => ServiceFactory.GetService<IGiftItemService>().Delete(command));
        }

        // Get user profile detail
        [HttpGet]
        [Route("{Id}")]
        [ResponseType(typeof(GiftItemDTO))]
        public IHttpActionResult Detail([ModelBinder] GiftItemCommandBase command)
        {
            if (command.Id == 0)
            {
                return RespondWith(() => new GiftItemDTO
                {
                    IsRedeem=true
                });
            }
            return RespondWith(() =>
                        ServiceFactory.GetService<IGiftItemService>()
                            .GetGiftItemDetail(command)
                            .OnFailureRaiseError(msg => new ApplicationException(msg))
                            .Value);
        }

    }
}

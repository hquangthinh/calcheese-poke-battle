﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.ModelBinding;
using CalcheesePokeBattle.Common.Monads;
using CalcheesePokeBattle.DTO;
using CalcheesePokeBattle.Service;
using CalcheesePokeBattle.ViewModel;
using CalcheesePokeBattle.WebApi.Utils;

namespace CalcheesePokeBattle.WebApi.Controllers.Admin
{
    [RoutePrefix("api/reports")]
    public class ReportApiController : AdminApiControllerBase
    {
        public ReportApiController(IServiceFactory serviceFactory) : base(serviceFactory)
        {
        }

        [HttpGet]
        [Route("coupon-import-job-status")]
        [ResponseType(typeof(List<ImportJobStatusDTO>))]
        public IHttpActionResult GetAllImportJobStatus([ModelBinder] ReportQuery query)
        {
            return RespondWith(() => ServiceFactory.GetService<IReportService>().GetAllImportJobStatus());
        }

        [HttpGet]
        [Route("gift-summary-by-week")]
        [ResponseType(typeof(GiftSummaryByWeekViewModel))]
        public IHttpActionResult GetGiftSummaryByWeek([ModelBinder] ReportQuery query)
        {
            return RespondWith(() => ServiceFactory.GetService<IReportService>().GetGiftSummaryByWeek(query.WeekNumber));
        }

        [HttpGet]
        [Route("gift-redeem-by-week")]
        [ResponseType(typeof(PagingResult<AllGiftRedeemHistoryViewModel>))]
        public IHttpActionResult GetAllGiftRedeemByWeek([ModelBinder] ReportQuery query)
        {
            return RespondWith(() => ServiceFactory.GetService<IReportService>().GetAllGiftRedeemByWeek(query));
        }

        [HttpGet]
        [Route("export-gift-redeem-list-excel")]
        public HttpResponseMessage ExportAllGiftRedeemByWeek([ModelBinder]ReportQuery query)
        {
            return ExportAndDownloadFile(query);
        }

        private HttpResponseMessage ExportAndDownloadFile(ReportQuery query)
        {
            var giftRedeemList =
                ServiceFactory.GetService<IReportService>().GetAllGiftRedeemForExportByWeek(query);

            var exportPath = GetPhysicalExportPath();

            var headers = new List<string>
            {
                "Tuần",
                "Quà",
                "Tên Đăng Nhập",
                "Họ",
                "Tên",
                "Điện Thoại",
                "Địa Chỉ",
                "Địa Chỉ Nhận Quà",
                "Số Nhà",
                "Đường",
                "Phường/Xã",
                "Quận/Huyện",
                "Tỉnh/Thành Phố"
            };

            var exportFileName =
                        $"danh-sach-doi-qua-tuan-{query.WeekNumber}-exported-{DateTime.UtcNow.AddHours(7):yyyy-mm-dd-hh-mm-ss}.xlsx";

            var fullExportPath = Path.Combine(exportPath, exportFileName);

            CreateExcelFile.CreateExcelDocument(headers, giftRedeemList, fullExportPath);
            var result = Request.CreateResponse(HttpStatusCode.OK);
            var stream = new FileStream(fullExportPath, FileMode.Open);
            result.Content = new StreamContent(stream);
            result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
            result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
            {
                FileName = Path.GetFileName(fullExportPath)
            };

            return result;
        }

        [HttpGet]
        [Route("coupon-statistic")]
        [ResponseType(typeof(UserCouponCodeStatisticModel))]
        public IHttpActionResult UserCouponCodeStatistic()
        {
            return RespondWith(() => ServiceFactory.GetService<IReportService>().UserCouponCodeStatistic());
        }
    }
}
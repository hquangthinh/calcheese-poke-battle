﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using CalcheesePokeBattle.Service;
using System.Web.Http;
using System.Web.Http.Description;
using CalcheesePokeBattle.DTO;
using System.Web.Http.ModelBinding;
using CalcheesePokeBattle.Common.DTO;
using CalcheesePokeBattle.Common.Monads;
using CalcheesePokeBattle.WebApi.Utils;

namespace CalcheesePokeBattle.WebApi.Controllers.Admin
{
    [RoutePrefix("api/usedcouponcodereport")]
    public class UsedCouponCodeReportApiController : AdminApiControllerBase
    {
        public UsedCouponCodeReportApiController(IServiceFactory serviceFactory) : base(serviceFactory)
        {
        }

        [HttpGet]
        [Route("")]
        [ResponseType(typeof(PagingResult<UsedCouponCodeReportDTO>))]
        public IHttpActionResult All([ModelBinder] UsedCouponCodeReportCommand command)
        {
            return RespondWith(() => ServiceFactory.GetService<IUsedCouponCodeReportService>().SearchUsedCouponCodeReport(command));
        }

        [HttpGet]
        [Route("summary-by-date")]
        [ResponseType(typeof(PagingResult<UsedCouponCodeSummaryByDateReportDTO>))]
        public IHttpActionResult UsedCouponCodeReportSummaryByDate([ModelBinder] KeywordSearchCommand command)
        {
            return RespondWith(() => ServiceFactory.GetService<IUsedCouponCodeReportService>().GetDateForUsedCouponCodeSummaryByDateReport(command));
        }

        [HttpGet]
        [Route("export-card-release-summary-by-date")]
        public HttpResponseMessage ExportCardReleaseSummaryByDate()
        {
            var exportData =
                ServiceFactory.GetService<IUsedCouponCodeReportService>()
                    .GetDateForUsedCouponCodeSummaryByDateReport(new KeywordSearchCommand())
                    .ResultSet.ToList();

            var exportPath = GetPhysicalExportPath();

            var headers = new List<string>
            {
                "Ngày",
                "Tổng Số Thẻ"
            };

            var exportFileName =
                $"tong-so-the-nhap-theo-ngay-exported-{DateTime.UtcNow.AddHours(7):yyyy-mm-dd-hh-mm-ss}.xlsx";

            var fullExportPath = Path.Combine(exportPath, exportFileName);

            CreateExcelFile.CreateExcelDocument(headers, exportData, fullExportPath);
            var result = Request.CreateResponse(HttpStatusCode.OK);
            var stream = new FileStream(fullExportPath, FileMode.Open);
            result.Content = new StreamContent(stream);
            result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
            result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
            {
                FileName = Path.GetFileName(fullExportPath)
            };

            return result;
        }
    }
}
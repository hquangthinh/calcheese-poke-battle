﻿using CalcheesePokeBattle.Common.Monads;
using CalcheesePokeBattle.DTO;
using CalcheesePokeBattle.Service;
using CalcheesePokeBattle.ViewModel;
using System;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.ModelBinding;

namespace CalcheesePokeBattle.WebApi.Controllers.Admin
{
    [RoutePrefix("api/pokecard")]
    public class PokeCardApiController : AdminApiControllerBase
    {
        public PokeCardApiController(IServiceFactory serviceFactory) : base(serviceFactory)
        {
        }

        [HttpGet]
        [Route("")]
        [ResponseType(typeof(PagingResult<PokeCardViewModel>))]
        public IHttpActionResult All([ModelBinder] SearchPokeCardCommand command)
        {
            return RespondWith(() => ServiceFactory.GetService<IPokeCardService>().SearchPokeCards(command));
        }

        // Create or Update gift item
        [HttpPost]
        [Route("")]
        [ResponseType(typeof(PokeCardDTO))]
        public IHttpActionResult Update([FromBody] UpdatePokeCardCommand command)
        {
            return RespondWith(() =>
                        ServiceFactory.GetService<IPokeCardService>().Update(command)
                            .OnFailureRaiseError(msg => new ApplicationException(msg))
                            .Value);
        }

        // Delete gift item
        [HttpDelete]
        [Route("")]
        [ResponseType(typeof(ResponseDTO))]
        public IHttpActionResult Destroy([FromUri] PokeCardCommandBase command)
        {
            return RespondWith(() => ServiceFactory.GetService<IPokeCardService>().Delete(command));
        }

        // Get user profile detail
        [HttpGet]
        [Route("{Id}")]
        [ResponseType(typeof(PokeCardDTO))]
        public IHttpActionResult Detail([ModelBinder] PokeCardCommandBase command)
        {
            if (command.Id == 0)
            {
                return RespondWith(() => new PokeCardDTO());
            }
            return RespondWith(() =>
                        ServiceFactory.GetService<IPokeCardService>()
                            .GetPokeCardDetail(command)
                            .OnFailureRaiseError(msg => new ApplicationException(msg))
                            .Value);
        }

        [HttpGet]
        [Route("AllCardRelease")]
        [ResponseType(typeof(IEnumerable<PokeCardReleaseViewModel>))]
        public IHttpActionResult AllCardRelease()
        {
            return RespondWith(() => ServiceFactory.GetService<IPokeCardService>().AllCardRelease());
        }

    }
}

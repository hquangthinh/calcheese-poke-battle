﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.ModelBinding;
using CalcheesePokeBattle.Authentication;
using CalcheesePokeBattle.Authentication.Models;
using CalcheesePokeBattle.Common.Monads;
using CalcheesePokeBattle.DTO;
using CalcheesePokeBattle.Service;
using CalcheesePokeBattle.ViewModel;
using CalcheesePokeBattle.WebApi.Model;

namespace CalcheesePokeBattle.WebApi.Controllers.Admin
{
    [RoutePrefix("api/pikachu-secret")]
    public class AdminAccountApiController : AdminApiControllerBase
    {
        private ApplicationUserManager UserManager { get; }
        private IAuthenticator Authenticator => ServiceFactory.GetService<IAuthenticator>();

        public AdminAccountApiController(IServiceFactory serviceFactory, ApplicationUserManager userManager) : base(serviceFactory)
        {
            UserManager = userManager;
        }

        /// <summary>
        /// Back door endpoint to create admin user
        /// POST http://localhost:50000/api/account/initadmin
        /// Secret: 274FB09A-86D0-40EC-8A20-748F88426882
        /// </summary>
        /// <param name="command"></param>
        /// <returns>
        /// Admin user: admin - 123qaz456
        /// </returns>
        [HttpPost]
        [Route("initadmin")]
        [AllowAnonymous]
        public IHttpActionResult InitAdmin([FromBody]InitAdminAccountCommand command)
        {
            return RespondWith(() => ServiceFactory.GetService<IAccountService>().InitAdminAccount(command));
        }

        // Search user profiles
        [HttpGet]
        [Route("")]
        [ResponseType(typeof(PagingResult<UserProfileDTO>))]
        public IHttpActionResult All([ModelBinder] SearchUserCommand command)
        {
            return RespondWith(() => ServiceFactory.GetService<IUserProfileService>().SearchUserProfiles(command));
        }

        // Get user profile detail
        [HttpGet]
        [Route("{UserId}")]
        [ResponseType(typeof(UserProfileDTO))]
        public IHttpActionResult Detail([ModelBinder] UserCommandBase command)
        {
            if ("current".Equals(command.UserId, StringComparison.OrdinalIgnoreCase))
            {
                return RespondWith(() => CurrentProfile);
            }

            if ("new".Equals(command.UserId, StringComparison.OrdinalIgnoreCase))
            {
                return RespondWith(() => new UserProfileDTO
                {
                    UserRoles = new List<RoleDTO>()
                });
            }

            return RespondWith(() =>
                        ServiceFactory.GetService<IUserProfileService>()
                            .GetUserProfileDetail(command)
                            .OnFailureRaiseError(msg => new ApplicationException(msg))
                            .Value);
        }

        // get user game history
        [HttpGet]
        [Route("{UserId}/games")]
        [ResponseType(typeof(List<UserGameHistoryViewModel>))]
        public IHttpActionResult GetUserGameHistoryViewModel([ModelBinder] UserCommandBase command)
        {
            return RespondWith(() =>
                        ServiceFactory.GetService<IUserProfileService>()
                            .GetUserGameHistoryViewModel(command)
                            .OnFailureRaiseError(msg => new ApplicationException(msg))
                            .Value);
        }

        // get user activity histories
        [HttpPost]
        [Route("activity-histories")]
        [ResponseType(typeof(List<UserActivityHistoryDTO>))]
        public IHttpActionResult GetUserActivityHistories([FromBody] UserCommandBase command)
        {
            return RespondWith(() =>
                        ServiceFactory.GetService<IUserProfileService>().GetUserActivityHistories(command)
                            .OnFailureRaiseError(msg => new ApplicationException(msg))
                            .Value);
        }

        // get user used code history
        [HttpPost]
        [Route("used-coupon-codes")]
        [ResponseType(typeof(List<UsedCouponCodeReportDTO>))]
        public IHttpActionResult GetUserUsedCouponCodes([FromBody] UserCommandBase command)
        {
            return RespondWith(() =>
                        ServiceFactory.GetService<IUserProfileService>().GetUserUsedCouponCodes(command)
                            .OnFailureRaiseError(msg => new ApplicationException(msg))
                            .Value);
        }


        // Create or Update user profile
        [HttpPost]
        [Route("")]
        [ResponseType(typeof(UserProfileDTO))]
        public IHttpActionResult Update([FromBody] UpdateUserCommand command)
        {
            return RespondWith(() =>
                        ServiceFactory.GetService<IUserProfileService>().UpdateUserDetailsWithRoles(command)
                            .OnFailureRaiseError(msg => new ApplicationException(msg))
                            .Value);
        }

        // Delete user profile
        [HttpDelete]
        [Route("")]
        [ResponseType(typeof(ResponseDTO))]
        public IHttpActionResult Destroy([FromUri] UserCommandBase command)
        {
            return RespondWith(() => ServiceFactory.GetService<IUserProfileService>().Delete(command));
        }

        [HttpPost]
        [Route("password")]
        [ResponseType(typeof(ResponseDTO))]
        public async Task<IHttpActionResult> ChangePassword([FromBody] ChangePasswordCommand command)
        {
            if (command == null || CurrentProfile == null)
                return Ok(ResponseDTO.ErrorResult("000", "Yêu cầu không hợp lệ"));

            if (string.IsNullOrEmpty(command.Password) || string.IsNullOrEmpty(command.PasswordConfirmation))
                return Ok(ResponseDTO.ErrorResult("001", "Yêu cầu không hợp lệ"));

            if (!command.Password.Equals(command.PasswordConfirmation))
                return Ok(ResponseDTO.ErrorResult("002", "Yêu cầu không hợp lệ"));

            if (!CurrentProfile.UserPermission.IsSiteAdmin)
                return Ok(ResponseDTO.ErrorResult("003", "Yêu cầu không hợp lệ"));

            var loginResult = await DoLogin(new LoginViewModel
            {
                UserName = CurrentProfile.UserName,
                Password = command.CurrentPassword,
                RememberMe = false
            });

            if (loginResult == LoginStatus.Unauthorized)
                return Ok(ResponseDTO.ErrorResult("003", "Yêu cầu không hợp lệ"));

            if (command.ChangePasswordForUser)
            {
                await Authenticator.ChangePasswordAsync(command.UserNameToChangePassword, command.Password);
            }
            else
            {
                await Authenticator.ChangePasswordAsync(CurrentProfile.UserName, command.Password);
            }

            return Ok(ResponseDTO.SuccessResult());
        }

        private async Task<LoginStatus> DoLogin(LoginViewModel model)
        {
            var user = await UserManager.FindAsync(model.UserName, model.Password);
            if (user != null && !user.LockoutEnabled)
            {
                if (await UserManager.HasApplicationLoginAsync(user.Id))
                {
                    await Authenticator.SignInAsync(user, model.RememberMe);
                    return LoginStatus.Success;
                }
            }
            return LoginStatus.Unauthorized;
        }
    }
}
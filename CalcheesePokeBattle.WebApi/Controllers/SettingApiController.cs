﻿using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Description;
using CalcheesePokeBattle.DTO;
using CalcheesePokeBattle.Service;
using Swashbuckle.Swagger.Annotations;

namespace CalcheesePokeBattle.WebApi.Controllers
{
    [RoutePrefix("api/setting")]
    public class SettingApiController : ApiControllerBase
    {
        public SettingApiController(IServiceFactory serviceFactory) : base(serviceFactory)
        {
        }

        // GET api/setting
        [SwaggerOperation("GetAll")]
        [Route("")]
        [ResponseType(typeof(List<SettingDTO>))]
        public IHttpActionResult GetAll()
        {
            return Ok(ServiceFactory.GetService<ISettingService>().GetAll());
        }
    }
}
﻿using System.Threading.Tasks;
using System.Web.Http;
using CalcheesePokeBattle.Authentication;
using CalcheesePokeBattle.Authentication.Models;
using CalcheesePokeBattle.DTO;
using CalcheesePokeBattle.Service;
using CalcheesePokeBattle.WebApi.Model;

namespace CalcheesePokeBattle.WebApi.Controllers
{
    [RoutePrefix("api/admin-auth")]
    public class AdminAuthApiController : AdminApiControllerBase
    {
        private ApplicationUserManager UserManager { get; }
        private IAuthenticator Authenticator => ServiceFactory.GetService<IAuthenticator>();

        public AdminAuthApiController(IServiceFactory serviceFactory, ApplicationUserManager userManager) : base(serviceFactory)
        {
            UserManager = userManager;
        }

        [HttpPost]
        [Route("login")]
        [AllowAnonymous]
        public async Task<IHttpActionResult> Login(LoginViewModel model)
        {
            var result = await DoLoginAndCheckRole(model);
            return Ok(result == LoginStatus.Success || result == LoginStatus.Redirect ? "ok" : "Invalid username or password");
        }

        private async Task<LoginStatus> DoLoginAndCheckRole(LoginViewModel model)
        {
            var user = await UserManager.FindAsync(model.UserName, model.Password);
            if (user != null && !user.LockoutEnabled)
            {
                var isAdmin = await UserManager.IsInRoleAsync(user.Id, RoleDTO.Administrator);
                var isLuckyManager = await UserManager.IsInRoleAsync(user.Id, RoleDTO.LuckyDrawManager);
                var auditService = ServiceFactory.GetService<IUserActivityService>();
                if (!isAdmin && !isLuckyManager)
                {
                    auditService.RecordUserActivity(new UserActivityHistoryDTO
                    {
                        Action = "AdminFailedToLogin",
                        ActionDetail = $"Admin failed to login with user id {model.UserName}",
                        UserName = GetUserName(model.UserName),
                        ClientInfo = GetClientInfo(Request),
                        ClientIpAddress = GetClientIp(Request)
                    });
                    return LoginStatus.Unauthorized;
                }

                if (await UserManager.HasApplicationLoginAsync(user.Id))
                {
                    await Authenticator.SignInAsync(user, model.RememberMe);
                    auditService.RecordUserActivity(new UserActivityHistoryDTO
                    {
                        Action = "AdminLoginOk",
                        ActionDetail = $"Admin login with user id {model.UserName}",
                        UserName = GetUserName(model.UserName),
                        ClientInfo = GetClientInfo(Request),
                        ClientIpAddress = GetClientIp(Request)
                    });
                    return LoginStatus.Redirect;
                }
            }
            return LoginStatus.Unauthorized;
        }

        private string GetUserName(string modelUserName)
        {
            return !string.IsNullOrEmpty(User?.Identity?.Name)
                ? User.Identity.Name
                : modelUserName;
        }

        [HttpDelete]
        [Route("logoff")]
        public IHttpActionResult Logoff()
        {
            Authenticator.SignOut();
            return Ok();
        }

        [HttpPost]
        [Route("signout")]
        public IHttpActionResult SignOut()
        {
            Authenticator.SignOut();
            return Ok("ok");
        }
    }
}
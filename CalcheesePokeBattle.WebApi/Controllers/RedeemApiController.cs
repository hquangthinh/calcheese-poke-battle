﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Description;
using CalcheesePokeBattle.Common.DTO;
using CalcheesePokeBattle.Common.Monads;
using CalcheesePokeBattle.DTO;
using CalcheesePokeBattle.Service;
using CalcheesePokeBattle.ViewModel;

namespace CalcheesePokeBattle.WebApi.Controllers
{
    [Authorize]
    [RoutePrefix("api/redeem")]
    public class RedeemApiController : ApiControllerBase
    {
        public RedeemApiController(IServiceFactory serviceFactory) : base(serviceFactory)
        {
        }

        [HttpGet]
        [Route("view-model")]
        [ResponseType(typeof(RedeemPageViewModel))]
        public IHttpActionResult GetViewModelForRedeemPage()
        {
            return RespondWith(() =>
                ServiceFactory.GetService<IRedeemService>().GetViewModelForRedeemPage()
                    .OnFailureRaiseError(msg => new ApplicationException(msg))
                    .Value);
        }

        [HttpPost]
        [Route("points-for-gift")]
        [ResponseType(typeof(ResponseDTO<string>))]
        public IHttpActionResult RedeemPointsForGift([FromBody] RedeemPointToGiftCommand command)
        {
            command = PopulateRequestInfo<RedeemPointToGiftCommand>(command);
            return RespondWith(() =>
                ServiceFactory.GetService<IRedeemService>().RedeemPointsForGift(command, CurrentProfile)
                    .OnFailureRaiseError(msg => new ApplicationException(msg))
                    .Value);
        }

        [HttpPost]
        [Route("points-for-lucky-code")]
        [ResponseType(typeof(ResponseDTO<string>))]
        public IHttpActionResult RedeemPointsForLuckyDrawCode([FromBody] RedeemPointToGiftCommand command)
        {
            command = PopulateRequestInfo<RedeemPointToGiftCommand>(command);
            return RespondWith(() =>
                ServiceFactory.GetService<IRedeemService>().RedeemPointsForLuckyDrawCode(command, CurrentProfile)
                    .OnFailureRaiseError(msg => new ApplicationException(msg))
                    .Value);
        }

        [HttpPost]
        [Route("lucky-draw-candidate-list")]
        [ResponseType(typeof(PagingResult<UserLuckyDrawCodeViewModel>))]
        public IHttpActionResult GetLuckyDrawCandidateListForCurrentWeek([FromBody] KeywordSearchCommand cmd)
        {
            var command = new SearchUserLuckyDrawCodeCommand
            {
                WeekNumber = Convert.ToInt32(cmd.Keyword),
                PageSize = 100,
                PageNumber = cmd.PageNumber
            };

            return RespondWith(() => ServiceFactory.GetService<IUserLuckyDrawCodeService>()
                .SearchUserLuckyDrawCode(command));
        }
    }
}
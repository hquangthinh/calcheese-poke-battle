﻿namespace CalcheesePokeBattle.WebApi.Model
{
    public enum LoginStatus
    {
        Redirect,
        Unauthorized,
        Success
    }
}
﻿using System.Reflection;
using System.Threading.Tasks;
using CalcheesePokeBattle.Common.Monads;
using CalcheesePokeBattle.Service;
using log4net;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;

namespace CalcheesePokeBattle.WebApi.SignalRHubs
{
    [HubName("pokeCardGameHub")]
    [Authorize]
    public class PokemonCardGameHub : Hub
    {
        private readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        private readonly IPokeCardGameHubBroadcaster _gameHubBroadcaster;

        public PokemonCardGameHub() : this(EventBusGameHubBroadcaster.Instance)
        {
        }

        public PokemonCardGameHub(IPokeCardGameHubBroadcaster gameHubBroadcaster)
        {
            _gameHubBroadcaster = gameHubBroadcaster;
        }

        public override Task OnDisconnected(bool stopCalled)
        {
            _gameHubBroadcaster.OnClientDisconnected(Context);
            return base.OnDisconnected(stopCalled);
        }

        public override Task OnConnected()
        {
            _gameHubBroadcaster.OnNewClientConnected(Context);
            return base.OnConnected();
        }

        public override Task OnReconnected()
        {
            _gameHubBroadcaster.OnClientReconnected(Context);
            return base.OnReconnected();
        }

        public Task SendNewGameRequest(NewGameRequestCommand command)
        {
            _log.Info($"New game request from {command.PlayerUserId}-{command.PlayerName} send to {command.InvitedPlayerUserId}-{command.InvitedPlayerUserName}");
            return Task.Run(() => _gameHubBroadcaster.SendNewGameRequest(Context, command)
                .OnFailure(err => _log.Error(err)));
        }

        public Task FindRandomOpponentPlayer(NewGameRequestCommand command)
        {
            _log.Info($"New game request from {command.PlayerUserId}-{command.PlayerName} send to {command.InvitedPlayerUserId}-{command.InvitedPlayerUserName}");
            return Task.Run(() => _gameHubBroadcaster.FindRandomOpponentPlayer(Context)
                .OnFailure(err => _log.Error(err)));
        }

        public Task ConfirmGameInvitation(ConfirmGameInvitationCommand command)
        {
            _log.Info($"Player {command.PlayerUserName} has confirmed game invitation from {command.ToPlayerUserName}");
            return Task.Run(() => _gameHubBroadcaster.ConfirmGameInvitation(Context, command)
                .OnFailure(err => _log.Error(err)));
        }

        public Task SubmitPokemonCard(SubmitPokemonCardCommand command)
        {
            _log.Info($"Player {command.PlayerName} submit card {command.PlayerCardId}");
            return Task.Run(() => _gameHubBroadcaster.SubmitPokemonCard(Context, command)
                .OnFailure(err => _log.Error(err)));
        }

        public Task StopGameRequest(GameSessionQuery gameSessionQuery)
        {
            _log.Info($"Player requests to stop game session {gameSessionQuery.GameSessionId}");
            return Task.Run(() => _gameHubBroadcaster.StopGameRequest(gameSessionQuery)
                .OnFailure(err => _log.Error(err)));
        }
    }
}
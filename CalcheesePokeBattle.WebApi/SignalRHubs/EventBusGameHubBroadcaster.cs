﻿using System;
using System.Linq;
using CalcheesePokeBattle.Common.Monads;
using CalcheesePokeBattle.Dependency;
using CalcheesePokeBattle.DTO;
using CalcheesePokeBattle.Service;
using CalcheesePokeBattle.SignalRHubs;
using CalcheesePokeBattle.WebApi.SignalRHubs.Extensions;
using Castle.Core.Logging;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;

namespace CalcheesePokeBattle.WebApi.SignalRHubs
{
    public class EventBusGameHubBroadcaster : IEventBaseGameHubBroadcaster
    {
        private static readonly Lazy<EventBusGameHubBroadcaster> _instance = new Lazy<EventBusGameHubBroadcaster>(
            () => new EventBusGameHubBroadcaster(GlobalHost.ConnectionManager.GetHubContext<PokemonCardGameHub>().Clients));

        private static readonly ConnectionMapping<string> _connections = new ConnectionMapping<string>();

        private IHubConnectionContext<dynamic> Clients { get; }
        public ILogger Logger { get; }

        public static EventBusGameHubBroadcaster Instance => _instance.Value;

        public EventBusGameHubBroadcaster(IHubConnectionContext<dynamic> clients)
        {
            Clients = clients;
            Logger = IocManager.Instance.GetService<ILogger>();
        }

        public Result OnNewClientConnected(HubCallerContext context)
        {
            var playerConnectionInfo = CreatePlayerConnectionInfoFromContext(context);
            AddNewConnectionInternal(playerConnectionInfo);
            return Result.Success();
        }

        public Result OnClientReconnected(HubCallerContext context)
        {
            var name = context.User.Identity.Name;
            if (!_connections.GetConnections(name).Contains(context.ConnectionId))
            {
                var playerConnectionInfo = CreatePlayerConnectionInfoFromContext(context);
                var gameServerService = IocManager.Instance.GetService<IPokeCardGameServerService>();
                gameServerService.PlayerReconnect(playerConnectionInfo, new UserProfileDTO
                {
                    Id = playerConnectionInfo.UserId,
                    UserName = playerConnectionInfo.UserName
                });
            }
            return Result.Success();
        }

        private static PlayerConnectionInfoDTO CreatePlayerConnectionInfoFromContext(HubCallerContext context)
        {
            var playerConnectionInfo = new PlayerConnectionInfoDTO
            {
                ServerName = string.Empty,
                ServerIpAddress = string.Empty,
                UserId = context.User.Identity.GetUserId(),
                UserName = context.User.Identity.Name,
                ConnectionId = context.ConnectionId,
                ConnectedAt = DateTime.UtcNow,
                DeviceInfo = context.GetDeviceInfo(),
                ClientIpAddress = context.GetIpAddress(),
                Status = PlayerConnectionStatus.Online.ToString()
            };
            return playerConnectionInfo;
        }

        private void AddNewConnectionInternal(PlayerConnectionInfoDTO playerConnectionInfo)
        {
            _connections.Add(playerConnectionInfo.UserName, playerConnectionInfo.ConnectionId);
            var gameServerService = IocManager.Instance.GetService<IPokeCardGameServerService>();
            gameServerService.NewPlayerOnline(playerConnectionInfo, new UserProfileDTO
            {
                Id = playerConnectionInfo.UserId,
                UserName = playerConnectionInfo.UserName
            });
            Clients.All.newConnection(playerConnectionInfo);
        }

        public Result OnClientDisconnected(HubCallerContext context)
        {
            var name = context.User.Identity.Name;
            var playerConnectionInfo = CreatePlayerConnectionInfoFromContext(context);
            var gameServerService = IocManager.Instance.GetService<IPokeCardGameServerService>();
            gameServerService.PlayerDisconnect(playerConnectionInfo, new UserProfileDTO
                {
                    Id = playerConnectionInfo.UserId,
                    UserName = playerConnectionInfo.UserName
                })
                .OnSuccess(disconnectInfo =>
                {
                    _connections.Remove(name, context.ConnectionId);
                    Clients.All.removeConnection(new PlayerDisconnectInfo
                    {
                        UserId = playerConnectionInfo.UserId,
                        UserName = name,
                        ConnectionId = context.ConnectionId,
                        AffectedUserIds = disconnectInfo.AffectedUserIds
                    });
                });

            return Result.Success();
        }

        /// <summary>
        /// A player send a game request to another player
        /// </summary>
        /// <param name="context"></param>
        /// <param name="command"></param>
        /// <returns></returns>
        public Result SendNewGameRequest(HubCallerContext context, NewGameRequestCommand command)
        {
            var playerConnectionInfo = CreatePlayerConnectionInfoFromContext(context);
            var gameLogic = IocManager.Instance.GetService<IPokeCardGameLogicService>();
            return gameLogic.ValidateNewGameRequest(playerConnectionInfo, command)
                .OnSuccess(() =>
                {
                    // notify invited user
                    var connectionIds = _connections.GetConnections(command.InvitedPlayerUserName).ToArray();
                    Clients.Clients(connectionIds).newGameRequestForYou(command);
                })
                .OnFailure(msg =>
                {
                    // notify requestor if the request fail
                    //var invitedPlayerConnectionIds = _connections.GetConnections(command.InvitedPlayerUserName).ToArray();
                    var requestorConnectionIds = _connections.GetConnections(command.PlayerUserName).ToArray();
                    Clients.Clients(requestorConnectionIds).failSendNewGameRequest(msg);
                });
        }

        public Result FindRandomOpponentPlayer(HubCallerContext context)
        {
            var playerConnectionInfo = CreatePlayerConnectionInfoFromContext(context);
            var gameLogic = IocManager.Instance.GetService<IPokeCardGameLogicService>();
            return gameLogic.FindRandomOpponentPlayer(ServerInfoCommand.Default(), playerConnectionInfo)
                .OnSuccess(invitedPlayer =>
                {
                    // notify invited user
                    var connectionIds = _connections.GetConnections(invitedPlayer.UserName).ToArray();
                    var newGameRequestData = new NewGameRequestCommand
                    {
                        PlayerName = playerConnectionInfo.UserName,
                        PlayerUserName = playerConnectionInfo.UserName,
                        PlayerUserId = playerConnectionInfo.UserId,
                        InvitedPlayerUserId = invitedPlayer.UserId,
                        InvitedPlayerUserName = invitedPlayer.UserName,
                        InvitedPlayerName = invitedPlayer.FullName
                    };
                    Clients.Clients(connectionIds).newGameRequestForYou(newGameRequestData);
                })
                .OnFailure(msg =>
                {
                    var requestorConnectionIds = _connections.GetConnections(playerConnectionInfo.UserName).ToArray();
                    Clients.Clients(requestorConnectionIds).unableToFindRandomOpponent(msg);
                });
        }

        public Result ConfirmGameInvitation(HubCallerContext context, ConfirmGameInvitationCommand command)
        {
            var gameLogic = IocManager.Instance.GetService<IPokeCardGameLogicService>();
            return gameLogic.ConfirmGameInvitation(command)
                .OnSuccess(confirmResult =>
                {
                    var connectionIds = _connections.GetConnections(command.ToPlayerUserName).ToArray();
                    if (confirmResult.ConfirmStart)
                    {
                        var requestorConnectionIds = _connections.GetConnections(command.PlayerUserName).ToArray();
                        // send message to both players to notify game start
                        Clients.Clients(connectionIds.Concat(requestorConnectionIds).ToArray())
                            .confirmToStartGame(confirmResult);
                    }
                    else
                    {
                        // send message to game requestor to notify invited player has declined
                        Clients.Clients(connectionIds).declineToStartGame(command);
                    }
                });
        }

        public Result SubmitPokemonCard(HubCallerContext context, SubmitPokemonCardCommand command)
        {
            var gameLogic = IocManager.Instance.GetService<IPokeCardGameLogicService>();
            return gameLogic.SubmitPokemonCard(command)
                .OnSuccess(gameRes =>
                {
                    if (gameRes.HasWinner || gameRes.GameTie)
                    {
                        // send game result to clients
                        var wonConnectionIds = _connections.GetConnections(gameRes.WonUserName);
                        var lostConnectionIds = _connections.GetConnections(gameRes.LostUserName);
                        Clients.Clients(wonConnectionIds.Concat(lostConnectionIds).ToArray())
                            .notifyGameResult(gameRes);

                    }
                    // still waiting for game result from 1 among 2 players
                })
                .OnFailure(errMsg =>
                {
                    var connectionIds = _connections.GetConnections(command.PlayerUserName).ToArray();
                    Clients.Clients(connectionIds).notifySubmitCardError(errMsg);
                });
        }

        public Result StopGameRequest(GameSessionQuery gameSessionQuery)
        {
            return IocManager.Instance.GetService<IPokeCardGameServerService>().StopGameRequest(gameSessionQuery)
                .OnSuccess(gameSessionInfo =>
                {
                    if (gameSessionInfo.PlayerUserId1.Equals(gameSessionQuery.RequestUserId))
                    {
                        // notify player 2
                        var player2ConnectionIds = _connections.GetConnections(gameSessionInfo.PlayerUserName2);
                        Clients.Clients(player2ConnectionIds.ToArray()).notifyGameHasBeenStopped();
                    }
                    else if (gameSessionInfo.PlayerUserId2.Equals(gameSessionQuery.RequestUserId))
                    {
                        var player1ConnectionIds = _connections.GetConnections(gameSessionInfo.PlayerUserName1);
                        Clients.Clients(player1ConnectionIds.ToArray()).notifyGameHasBeenStopped();
                    }
                });
        }

        public void RegisterEventHandlers()
        {
            throw new NotImplementedException();
        }
    }
}
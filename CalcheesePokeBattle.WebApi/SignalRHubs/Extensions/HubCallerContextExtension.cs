﻿using System;
using Microsoft.AspNet.SignalR.Hubs;

namespace CalcheesePokeBattle.WebApi.SignalRHubs.Extensions
{
    public static class HubCallerContextExtension
    {
        public static string GetIpAddress(this HubCallerContext context)
        {
            return GetValueFromContext(context, "server.RemoteIpAddress");
        }

        public static string GetDeviceInfo(this HubCallerContext context)
        {
            return context.Request.Headers.Get("User-Agent ");
        }

        private static string GetValueFromContext(HubCallerContext context, string varableName)
        {
            var value = string.Empty;
            try
            {
                object tempObject;
                context.Request.Environment.TryGetValue(varableName, out tempObject);
                if (tempObject != null)
                {
                    value = (string)tempObject;
                }
                else
                {
                    value = string.Empty;
                }
            }
            catch (Exception)
            {
                // ignored
            }

            return value;
        }
    }
}
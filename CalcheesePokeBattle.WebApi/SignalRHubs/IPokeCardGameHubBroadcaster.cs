﻿using System.Threading.Tasks;
using CalcheesePokeBattle.Common.Monads;
using CalcheesePokeBattle.Service;
using Microsoft.AspNet.SignalR.Hubs;

namespace CalcheesePokeBattle.WebApi.SignalRHubs
{
    public interface IPokeCardGameHubBroadcaster
    {
        Result OnNewClientConnected(HubCallerContext context);

        Result OnClientReconnected(HubCallerContext context);

        Result OnClientDisconnected(HubCallerContext context);

        Result SendNewGameRequest(HubCallerContext context, NewGameRequestCommand command);

        Result FindRandomOpponentPlayer(HubCallerContext context);

        Result ConfirmGameInvitation(HubCallerContext context, ConfirmGameInvitationCommand command);

        Result SubmitPokemonCard(HubCallerContext context, SubmitPokemonCardCommand command);

        Result StopGameRequest(GameSessionQuery gameSessionQuery);
    }

    public interface ITimerBaseGameHubBroadcaster : IPokeCardGameHubBroadcaster
    {
        /// <summary>
        /// Start polling db to monitor for new notifications will be invoked when application starts
        /// </summary>
        void StartAppNotificationPolling();

        /// <summary>
        /// Stop polling db for new notifications will be invoked when application shutdowns
        /// </summary>
        void StopAppNotificationPolling();
    }

    public interface IEventBaseGameHubBroadcaster : IPokeCardGameHubBroadcaster
    {
        void RegisterEventHandlers();
    }
}
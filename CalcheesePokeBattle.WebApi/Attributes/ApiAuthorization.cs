﻿using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Controllers;

namespace CalcheesePokeBattle.WebApi.Attributes
{
    public class ApiAuthorization : AuthorizeAttribute
    {
        protected override void HandleUnauthorizedRequest(HttpActionContext actionContext)
        {
            actionContext.Response = new HttpResponseMessage(HttpStatusCode.Forbidden);
            actionContext.Response.Headers.Add("X-LoginUrl", actionContext.RequestContext.VirtualPathRoot + "/Account/RemoteLogin");
        }
    }
}
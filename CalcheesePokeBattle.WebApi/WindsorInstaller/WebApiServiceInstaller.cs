﻿using CalcheesePokeBattle.Authentication;
using CalcheesePokeBattle.Model;
using CalcheesePokeBattle.Service;
using CalcheesePokeBattle.WebApi.Owin.Security;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using Microsoft.Owin.Security;
using System.Web;
using CalcheesePokeBattle.Common.Cryptography;
using CalcheesePokeBattle.Dependency;

namespace CalcheesePokeBattle.WebApi.WindsorInstaller
{
    public class WebApiServiceInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            var serviceFactory = new WindsorServiceFactory(container);
            IocManager.Instance.ServiceFactory = serviceFactory;

            container.Register(
                Component.For<IServiceFactory>().Instance(serviceFactory)
                , Component.For<IDbFactory>().ImplementedBy<DefaultDbFactory>().LifeStyle.HybridPerWebRequestPerThread()
                , Component.For<CalcheeseAppDbContext>().LifeStyle.HybridPerWebRequestPerThread()
                , Component.For<IAuthenticationManager>().UsingFactoryMethod(() => HttpContext.Current != null ? HttpContext.Current.GetOwinContext().Authentication : new NonHttpContextAuthenticationManager()).LifeStyle.HybridPerWebRequestPerThread()
                , Component.For<ApplicationIdentityDbContext>().LifeStyle.HybridPerWebRequestPerThread()
                , Component.For<ApplicationRoleStore>().LifeStyle.HybridPerWebRequestPerThread()
                , Component.For<ApplicationUserStore>().LifeStyle.HybridPerWebRequestPerThread()
                , Component.For<ApplicationRoleManager>().LifeStyle.HybridPerWebRequestPerThread()
                , Component.For<ApplicationUserManager>().LifeStyle.HybridPerWebRequestPerThread()

                , Component.For<IAuthenticator>().ImplementedBy<DefaultAuthenticator>().LifeStyle.HybridPerWebRequestPerThread()
                , Component.For<ISettingService>().ImplementedBy<DefaultSettingService>().LifeStyle.HybridPerWebRequestPerThread()
                , Component.For<IAccountService>().ImplementedBy<DefaultAccountService>().LifeStyle.HybridPerWebRequestPerThread()
                , Component.For<IUserProfileService>().ImplementedBy<DefaultUserProfileService>().LifeStyle.HybridPerWebRequestPerThread()
                , Component.For<IPokeCardGameServerService>().ImplementedBy<DefaultPokeCardGameServerService>().LifeStyle.HybridPerWebRequestPerThread()
                , Component.For<IPokeCardGameLogicService>().ImplementedBy<DefaultPokeCardGameLogicService>().LifeStyle.HybridPerWebRequestPerThread()
                , Component.For<IGameSessionService>().ImplementedBy<InMemoryGameSessionService>().LifeStyle.Singleton
                , Component.For<IGameRuleService>().ImplementedBy<DefaultGameRuleService>().LifeStyle.Singleton
                , Component.For<ICouponCodeService>().ImplementedBy<DefaultCouponCodeService>().LifeStyle.HybridPerWebRequestPerThread()
                , Component.For<IDataEncryptionService>().ImplementedBy <DefaultAesDataEncryptionService>().LifeStyle.HybridPerWebRequestPerThread()
                , Component.For<IOneWayEncryptionService>().ImplementedBy<MD5DataEncryptionService>().LifeStyle.HybridPerWebRequestPerThread()
                , Component.For<IRedeemService>().ImplementedBy<DefaultRedeemService>().LifeStyle.HybridPerWebRequestPerThread()
                , Component.For<ILuckyDrawCodeService>().ImplementedBy<DefaultLuckyDrawCodeService>().LifeStyle.HybridPerWebRequestPerThread()
                , Component.For<IWinnerListService>().ImplementedBy<DefaultWinnerListService>().LifeStyle.HybridPerWebRequestPerThread()
                , Component.For<IGiftItemService>().ImplementedBy<GiftItemService>().LifeStyle.HybridPerWebRequestPerThread()
                , Component.For<IPokeCardService>().ImplementedBy<DefaultPokeCardService>().LifeStyle.HybridPerWebRequestPerThread()
                , Component.For<IPrizeLuckyDrawHistoryService>().ImplementedBy<PrizeLuckyDrawHistoryService>().LifeStyle.HybridPerWebRequestPerThread()
                , Component.For<IUserActivityService>().ImplementedBy<DefaultUserActivityService>().LifeStyle.HybridPerWebRequestPerThread()
                , Component.For<IUploadFileService>().ImplementedBy<UploadFileService>().LifeStyle.HybridPerWebRequestPerThread()
                , Component.For<IUserLuckyDrawCodeService>().ImplementedBy<UserLuckyDrawCodeService>().LifeStyle.HybridPerWebRequestPerThread()
                , Component.For<ILuckyDrawCodeServiceFactory>().ImplementedBy<DefaultLuckyDrawCodeServiceFactory>().LifeStyle.HybridPerWebRequestPerThread()
                , Component.For<ILuckyDrawCodeGenerator>().ImplementedBy<LuckyDrawCodeGeneratorForWeek1>().Named("ILuckyDrawCodeGeneratorForWeek1").LifeStyle.HybridPerWebRequestPerThread()
                , Component.For<ILuckyDrawCodeGenerator>().ImplementedBy<LuckyDrawCodeGeneratorForWeek2>().Named("ILuckyDrawCodeGeneratorForWeek2").LifeStyle.HybridPerWebRequestPerThread()
                , Component.For<ILuckyDrawCodeGenerator>().ImplementedBy<LuckyDrawCodeGeneratorForWeek3>().Named("ILuckyDrawCodeGeneratorForWeek3").LifeStyle.HybridPerWebRequestPerThread()
                , Component.For<ILuckyDrawCodeGenerator>().ImplementedBy<LuckyDrawCodeGeneratorForWeek4>().Named("ILuckyDrawCodeGeneratorForWeek4").LifeStyle.HybridPerWebRequestPerThread()
                , Component.For<ILuckyDrawCodeGenerator>().ImplementedBy<LuckyDrawCodeGeneratorForWeek5>().Named("ILuckyDrawCodeGeneratorForWeek5").LifeStyle.HybridPerWebRequestPerThread()
                , Component.For<ILuckyDrawCodeGenerator>().ImplementedBy<LuckyDrawCodeGeneratorForWeek6>().Named("ILuckyDrawCodeGeneratorForWeek6").LifeStyle.HybridPerWebRequestPerThread()
                , Component.For<ILuckyDrawCodeGenerator>().ImplementedBy<LuckyDrawCodeGeneratorForWeek7>().Named("ILuckyDrawCodeGeneratorForWeek7").LifeStyle.HybridPerWebRequestPerThread()
                , Component.For<ILuckyDrawCodeGenerator>().ImplementedBy<LuckyDrawCodeGeneratorForWeek8>().Named("ILuckyDrawCodeGeneratorForWeek8").LifeStyle.HybridPerWebRequestPerThread()
                , Component.For<ILuckyDrawCodeGenerator>().ImplementedBy<LuckyDrawCodeGeneratorForWeek9>().Named("ILuckyDrawCodeGeneratorForWeek9").LifeStyle.HybridPerWebRequestPerThread()
                , Component.For<ILuckyDrawCodeGenerator>().ImplementedBy<LuckyDrawCodeGeneratorForWeek10>().Named("ILuckyDrawCodeGeneratorForWeek10").LifeStyle.HybridPerWebRequestPerThread()
                , Component.For<ILuckyDrawCodeGenerator>().ImplementedBy<LuckyDrawCodeGeneratorForWeek11>().Named("ILuckyDrawCodeGeneratorForWeek11").LifeStyle.HybridPerWebRequestPerThread()
                , Component.For<ILuckyDrawCodeGenerator>().ImplementedBy<LuckyDrawCodeGeneratorForWeek12>().Named("ILuckyDrawCodeGeneratorForWeek12").LifeStyle.HybridPerWebRequestPerThread()
                , Component.For<ILuckyDrawCodeGenerator>().ImplementedBy<LuckyDrawCodeGeneratorForWeek13>().Named("ILuckyDrawCodeGeneratorForWeek13").LifeStyle.HybridPerWebRequestPerThread()
                
                , Component.For<IReportService>().ImplementedBy<DefaultReportService>().LifeStyle.HybridPerWebRequestPerThread()
                 , Component.For<IUsedCouponCodeReportService>().ImplementedBy<DefaultUsedCouponCodeReportService>().LifeStyle.HybridPerWebRequestPerThread()
            );
        }
    }
}
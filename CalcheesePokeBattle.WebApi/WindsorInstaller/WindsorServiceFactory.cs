﻿using CalcheesePokeBattle.Service;
using Castle.Windsor;

namespace CalcheesePokeBattle.WebApi.WindsorInstaller
{
    public class WindsorServiceFactory : DefaultWindsorServiceFactory
    {
        public WindsorServiceFactory(IWindsorContainer container) : base(container)
        {
        }
    }
}
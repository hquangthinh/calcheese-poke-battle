﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.Owin.Security;

namespace CalcheesePokeBattle.WebApi.Owin.Security
{
    public class NonHttpContextAuthenticationManager : IAuthenticationManager
    {
        public IEnumerable<AuthenticationDescription> GetAuthenticationTypes()
        {
            throw new NotImplementedException("Use HttpContext.Current.GetOwinContext().Authentication if HttpContext is available");
        }

        public IEnumerable<AuthenticationDescription> GetAuthenticationTypes(Func<AuthenticationDescription, bool> predicate)
        {
            throw new NotImplementedException("Use HttpContext.Current.GetOwinContext().Authentication if HttpContext is available");
        }

        public Task<AuthenticateResult> AuthenticateAsync(string authenticationType)
        {
            throw new NotImplementedException("Use HttpContext.Current.GetOwinContext().Authentication if HttpContext is available");
        }

        public Task<IEnumerable<AuthenticateResult>> AuthenticateAsync(string[] authenticationTypes)
        {
            throw new NotImplementedException("Use HttpContext.Current.GetOwinContext().Authentication if HttpContext is available");
        }

        public void Challenge(AuthenticationProperties properties, params string[] authenticationTypes)
        {
            throw new NotImplementedException("Use HttpContext.Current.GetOwinContext().Authentication if HttpContext is available");
        }

        public void Challenge(params string[] authenticationTypes)
        {
            throw new NotImplementedException("Use HttpContext.Current.GetOwinContext().Authentication if HttpContext is available");
        }

        public void SignIn(AuthenticationProperties properties, params ClaimsIdentity[] identities)
        {
            throw new NotImplementedException("Use HttpContext.Current.GetOwinContext().Authentication if HttpContext is available");
        }

        public void SignIn(params ClaimsIdentity[] identities)
        {
            throw new NotImplementedException("Use HttpContext.Current.GetOwinContext().Authentication if HttpContext is available");
        }

        public void SignOut(AuthenticationProperties properties, params string[] authenticationTypes)
        {
            throw new NotImplementedException("Use HttpContext.Current.GetOwinContext().Authentication if HttpContext is available");
        }

        public void SignOut(params string[] authenticationTypes)
        {
            throw new NotImplementedException("Use HttpContext.Current.GetOwinContext().Authentication if HttpContext is available");
        }

        public ClaimsPrincipal User
        {
            get { throw new NotImplementedException("Use HttpContext.Current.GetOwinContext().Authentication if HttpContext is available"); }
            set { throw new NotImplementedException("Use HttpContext.Current.GetOwinContext().Authentication if HttpContext is available"); }
        }

        public AuthenticationResponseGrant AuthenticationResponseGrant
        {
            get { throw new NotImplementedException("Use HttpContext.Current.GetOwinContext().Authentication if HttpContext is available"); }
            set { throw new NotImplementedException("Use HttpContext.Current.GetOwinContext().Authentication if HttpContext is available"); }
        }

        public AuthenticationResponseChallenge AuthenticationResponseChallenge
        {
            get { throw new NotImplementedException("Use HttpContext.Current.GetOwinContext().Authentication if HttpContext is available"); }
            set { throw new NotImplementedException("Use HttpContext.Current.GetOwinContext().Authentication if HttpContext is available"); }
        }

        public AuthenticationResponseRevoke AuthenticationResponseRevoke
        {
            get { throw new NotImplementedException("Use HttpContext.Current.GetOwinContext().Authentication if HttpContext is available"); }
            set { throw new NotImplementedException("Use HttpContext.Current.GetOwinContext().Authentication if HttpContext is available"); }
        }
    }
}
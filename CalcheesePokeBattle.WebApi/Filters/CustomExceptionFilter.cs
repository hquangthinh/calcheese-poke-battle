﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http.Filters;
using System.Web.Http.ModelBinding;
using CalcheesePokeBattle.Exceptions;
using ExceptionHelper = CalcheesePokeBattle.Exceptions.ExceptionHelper;

namespace CalcheesePokeBattle.WebApi.Filters
{
    public class CustomExceptionFilter : ExceptionFilterAttribute
    {
        protected static readonly log4net.ILog Logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public override void OnException(HttpActionExecutedContext actionExecutedContext)
        {
            if (actionExecutedContext.Exception is UnauthorizedAccessException)
            {
                actionExecutedContext.Response = actionExecutedContext.Request.CreateErrorResponse(
                    HttpStatusCode.Unauthorized, actionExecutedContext.Exception.Message);
            }
            else if (actionExecutedContext.Exception is ResourceModifiedException)
            {
                actionExecutedContext.Response = actionExecutedContext.Request.CreateErrorResponse(
                    HttpStatusCode.Conflict, actionExecutedContext.Exception.Message);
            }
            else
            {
                var errorDetails = ExceptionHelper.GetExeptionDetails(actionExecutedContext.Exception);
                switch (errorDetails.PresentationStyle)
                {
                    case ErrorPresentationStyle.Aggregated:
                        var modelStateDict = new ModelStateDictionary();
                        foreach (var e in errorDetails.Exceptions)
                        {
                            modelStateDict.AddModelError(e.GetHashCode().ToString(), e.Message);
                        }
                        actionExecutedContext.Response = actionExecutedContext.Request.CreateErrorResponse(HttpStatusCode.BadRequest, modelStateDict);
                        break;

                    case ErrorPresentationStyle.Warning:
                        actionExecutedContext.Response = actionExecutedContext.Request.CreateErrorResponse(HttpStatusCode.BadRequest, errorDetails.ErrorMessage);
                        break;

                    case ErrorPresentationStyle.Error:
                        actionExecutedContext.Response = actionExecutedContext.Request.CreateErrorResponse(
                            HttpStatusCode.InternalServerError, errorDetails.ErrorMessage, errorDetails.Exceptions.First()
                        );
                        break;
                }
            }

            base.OnException(actionExecutedContext);

            var exception = actionExecutedContext.Exception;
            // If this is not an HTTP 500 (for example, if somebody throws an HTTP 404 from an action method),
            // ignore it.
            if (new HttpException(null, exception).GetHttpCode() != 500) return;

            var requestUri = actionExecutedContext.ActionContext.ControllerContext.Request.RequestUri.ToString();
            Logger.Error($"Exception occurred when processing request {requestUri}", exception);
        }
    }
}
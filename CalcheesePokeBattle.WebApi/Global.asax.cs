﻿using System;
using System.Web.Http;
using System.Web.Http.Controllers;
using CalcheesePokeBattle.Dependency;
using CalcheesePokeBattle.Service;
using CalcheesePokeBattle.WebApi.WindsorInstaller;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.Resolvers.SpecializedResolvers;
using Castle.Windsor;

namespace CalcheesePokeBattle.WebApi
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        private static IWindsorContainer _container;

        protected void Application_Start()
        {
            WindsorInit();
            // Disconnect all clients on app start up to ensure fresh app state
            IocManager.Instance.GetService<IPokeCardGameServerService>().DisconnectAllPlayers();
        }

        private void WindsorInit()
        {
            _container = new WindsorContainer();
            _container.Register(
                Classes.FromThisAssembly().BasedOn<IHttpController>().WithServiceSelf().LifestyleScoped()
            );

            _container.Kernel.Resolver.AddSubResolver(new CollectionResolver(_container.Kernel));
            _container.Install(new WebApiServiceInstaller(), new LoggerInstaller());
            GlobalConfiguration.Configuration.DependencyResolver = new CastleDependencyResolver(_container.Kernel);
        }

        protected void Application_End(object sender, EventArgs e)
        {
        }
    }
}
